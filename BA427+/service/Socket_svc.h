//*****************************************************************************
//
// Source File: Socket_svc.h
// Description: socket service
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 08/03/18     Jerry.W        Created file.
// 09/28/18     Jerry.W        add SSL certificate enable/diable function.
// 10/17/18     Jerry.W        add Socket reset function.
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/driver/wifi_driver.h#2 $
// $DateTime: 2017/12/18 16:17:54 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************


#ifndef SOCKET_SERVICE_H
#define SOCKET_SERVICE_H

#include "../driver/wifi_driver.h"

typedef struct{
	uint8_t index;
	uint8_t status;
	uint16_t port;
	uint8_t * pair;								//server name for TCP, or from
	void (*evn_cb)(void* , uint8_t );				//event notification
	void (*rcv_cb)(void* , uint8_t *, uint16_t);		//receive callback
	void *reserve;
}Socket_t;


typedef enum{
	Soc_idle,
	Soc_TCP_connecting,
	Soc_TCP_connected,
	Soc_TCP_connect_fail,
	Soc_TCP_disconnected,

	Soc_UDP_opened,

}SOC_STATE_t;

typedef enum{
	CERT_server,
	CERT_client
}CERT_t;


#define SOCKET_DEBUG	1


#define TCP_CONN_MAX	5

/*************************************************************************************************
 * @ Name		: Socket_reset
 * @ Brief 		: reset socket service.
 *************************************************************************************************/
void Socket_reset();


/*************************************************************************************************
 * @ Name		: Socket_svc_init
 * @ Brief 		: Initial socket service.
 *************************************************************************************************/
void Socket_svc_init();


/*************************************************************************************************
 * @ Name		: TCP_connect
 * @ Brief 		: TCP socket connect to server.
 * @ Parameter 	: soc -- socket data structure, setup by this API.
 * 				  secure -- true : TLS connection.  false : normal connection.
 * @ Return		: success or fail.
 *************************************************************************************************/
bool TCP_connect(Socket_t* soc,  bool secure );


/*************************************************************************************************
 * @ Name		: TCP_disconnect
 * @ Brief 		: disconnect socket connection.
 * @ Parameter 	: soc -- socket structure of to be disconnected.
 *************************************************************************************************/
void TCP_disconnect(Socket_t* soc);


/*************************************************************************************************
 * @ Name		: TCP_send
 * @ Brief 		: disconnect socket connection.
 * @ Parameter 	: soc -- socket structure of to be disconnected.
 *************************************************************************************************/
void TCP_send(Socket_t* soc, uint8_t* data, uint16_t len);


/*************************************************************************************************
 * @ Name		: CERT_set
 * @ Brief 		: setting server/client certificate
 * @ Parameter 	: type -- server or client refer CERT_t type.
 * @ Parameter 	: en -- true: enable, false: disable.
 *************************************************************************************************/
void CERT_set(CERT_t type, bool en);


#endif
