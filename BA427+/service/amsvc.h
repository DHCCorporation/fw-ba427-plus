//*****************************************************************************
//
// Source File: amsvc.h
// Description: Functions to access voltage meter data form amper meter accessory.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 01/12/16     Vincent.T      Created file.
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/service/amsvc.h#5 $
// $DateTime: 2017/09/22 10:27:40 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#ifndef _AMSVC_H_
#define _AMSVC_H_

#include "..\driver/am.h"

void GetAM(float *pfAMvol, uint32_t *ADC);

#endif /* SERVICE_AMSVC_H_ */
