//*****************************************************************************
//
// Source File: wordbanksvc.h
// Description: The wordbanksvc function to access wordbank.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 10/29/15     Vincent.T      Created file.
// 03/14/16     Vincent.T      1. New func. PrinterGetDataBarCode(); to get barcode data.
// 03/15/16     Vincent.T      1. New func. PrintSetDataBarCode(); to print barcode.
// 04/14/16     Vincent.T      1. Add data into WORDBANK
// 04/27/16     Vincent.T      1. Add data into WORDBANK
// 09/27/17		Jerry.W		   1. add WB of { } ~
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/service/wordbanksvc.h#6 $
// $DateTime: 2017/09/28 18:05:18 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#ifndef _WORDBANKSVC_H_
#define _WORDBANKSVC_H_

typedef enum
{
	wb_space = 0x20,		//work bank index:
	wb_exclamation,			//work bank index: !
	wb_dquote,				//work bank index: "
	wb_number,				//work bank index: #
	wb_dollar,				//work bank index: $
	wb_percent,				//work bank index: %
	wb_and,					//work bank index: &
	wb_quote,				//work bank index: '
	wb_leftparenthesis,		//work bank index: (
	wb_Rightparenthesis,	//work bank index: )
	wb_asterisk,			//work bank index: *
	wb_plus,				//work bank index: +
	wb_comma,				//work bank index: ,
	wb_minus,				//work bank index: -
	wb_dot,					//work bank index: .
	wb_forwardslash,		//work bank index: /
	wb_0 = 0x30,			//word bank index: 0
	wb_1,					//word bank index: 1
	wb_2,					//word bank index: 2
	wb_3,					//word bank index: 3
	wb_4,					//word bank index: 4
	wb_5,					//word bank index: 5
	wb_6,					//word bank index: 6
	wb_7,					//word bank index: 7
	wb_8,					//word bank index: 8
	wb_9,					//word bank index: 9
	wb_colon,				//work bank index: :
	wb_semicolon,			//work bank index: ;
	wb_lessthan,			//work bank index: <
	wb_equal,				//work bank index: =
	wb_greaterthan,			//work bank index: >
	wb_questmark,			//work bank index: ?
	wb_AT,					//work bank index: @
	wb_A = 'A',				//word bank index: A
	wb_B, 		 			//word bank index: B
	wb_C,					//word bank index: C
	wb_D,					//word bank index: D
	wb_E,					//word bank index: E
	wb_F,					//word bank index: F
	wb_G,					//word bank index: G
	wb_H,					//word bank index: H
	wb_I,					//word bank index: I
	wb_J,					//word bank index: J
	wb_K,					//word bank index: K
	wb_L,					//word bank index: L
	wb_M,					//word bank index: M
	wb_N,					//word bank index: N
	wb_O,					//word bank index: O
	wb_P,					//word bank index: P
	wb_Q,					//word bank index: Q
	wb_R,					//word bank index: R
	wb_S,					//word bank index: S
	wb_T,					//word bank index: T
	wb_U,					//word bank index: U
	wb_V,					//word bank index: V
	wb_W,					//word bank index: W
	wb_X,					//word bank index: X
	wb_Y,					//word bank index: Y
	wb_Z,					//word bank index: Z
	wb_a = 'a',				//word bank index: a
	wb_b, 		 			//word bank index: b
	wb_c,					//word bank index: c
	wb_d,					//word bank index: d
	wb_e,					//word bank index: e
	wb_f,					//word bank index: f
	wb_g,					//word bank index: g
	wb_h,					//word bank index: h
	wb_i,					//word bank index: i
	wb_j,					//word bank index: j
	wb_k,					//word bank index: k
	wb_l,					//word bank index: l
	wb_m,					//word bank index: m
	wb_n,					//word bank index: n
	wb_o,					//word bank index: o
	wb_p,					//word bank index: p
	wb_q,					//word bank index: q
	wb_r,					//word bank index: r
	wb_s,					//word bank index: s
	wb_t,					//word bank index: t
	wb_u,					//word bank index: u
	wb_v,					//word bank index: v
	wb_w,					//word bank index: w
	wb_x,					//word bank index: x
	wb_y,					//word bank index: y
	wb_z,					//word bank index: z
	wb_Bigleftparenthesis,	//word bank index: {
	wb_ohm, 				//word bank index: Ohm
	wb_BigRightparenthesis,	//word bank index: }
	wb_hyphen,				//word bank index: ~
	wb_doC,					//word bank index: doC
	wb_doF					//word bank index: doF
}WORDBANK;

uint16_t PrinterGetDataWB(WORDBANK WBindex, uint8_t ui8WBSeg);
void PrinterSetDataWB(uint8_t *pui8Src, uint16_t *pui16Dest, uint8_t ui8WBseg);
uint16_t PrinterGetDataBarCode(uint8_t BarCodeindex);
void PrintSetDataBarCode(uint8_t *pui8Src, uint16_t *pui16Dest);

#endif /* _WORDBANKSVC_H_ */
