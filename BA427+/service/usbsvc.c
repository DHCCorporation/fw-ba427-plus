//*****************************************************************************
//
// Source File: usbsvc.c
// Description: Service Function of
//				(1). USBSyncTestRecToPC()
//					- Synchronize Tester Record to PC Software via USB
//				(2). USBLCDScreenUpdate()
//					- Language Index
//					- Screen Index
//					- Full screen Hex Code
//				(3). USBPrinterFieldUpdate()
//					- Language Index
//					- Printer Field Index
//					- Each printer field Hex code
//				(4). USBMapAutoTestProg()
//					- Bi-direction
//						- PC send command to Device for CCA load-on test (to simulate Button(key-press) operation) via USB.
//						- Tester Device report the CCA/SOC/SOH/MAP Judge result back to PC tool via USB.
//				(5). USBUIFlowTestProg() - Option
//					- Bi-direction
//						- PC send command to Device for UI flow Auto-Test (to simulate Button(key-press) operation) via USB.
//						- Tester Device report the UI screen Hex code back to PC tool via USB.
//				(6). USBMfcVoltCalibration() �VOption
//				(7). USBMfcCCACalibration() �VOption
//				(8). USBMfcVMCalibration() �VOption
//				(9). USBMfcAMCalibration() �VOption
//				(10). USBMfcTempCalibration() �VOption
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 07/15/15     William.H      Created file.
// 10/01/15     William.H      Re-arrange/remove Global varaible's usage due to we've used stack to behave the manu level selection.
//                             Respective global varaible by the respective widget file to be maintained by their own.
//                             To integrate all the BUTTON_PRESSED event: SELECT_BUTTON/UP_BUTTON/DOWN_BUTTON/LEFT_BUTTON/RIGHT_BUTTON, and respective control function into Widget Manager.c file.
// 10/05/15     William.H      Check in BT2020 project which branched from LCDFontMaker(Pre-BT2020) by modifed related GPIOs.
// 10/26/15     Vincent.T      1. Created function structure(USBPCModeSvc).
//                             2. add USB shakehand func.
//                             3. add USB printout update.
//                             4. integrate lcd update
// 11/18/15     Vincent.T      1. Add Tested records upload function
// 12/23/15     Vincent.T      1. Modify some bugs when allocateing/freeing memory
//                             2. Add USB printout read func.
// 12/29/15     Vincent.T      1. Add USB DFU function.
//                             2. Extern globla variable g_ui32SysClock
// 01/06/16     Vincent.T      1. Modified USBTestRecordUpload();
//                             2. #include "eepromsvc.h"
// 01/28/16     Vincent.T      1. Show USB icon when usb is plugged in.
// 03/08/16     Vincent.T      1. Add Vbus Detec func.
//                             2. global variables g_MainMenuItem, g_MenuLvlStack
//                             3. #include "..\service/stacksvc.h"
//                             4. #include "..\app/Mainmenuwidget.h"
// 03/16/16     Vincent.T     1. Modify some function input paremeters. (see printersvc.c/.h)
// 03/24/16     Henry.Y        1. #include "..\service/battcoresvc.h"
//                             2. Add external voltage calibration func.
//                             3. Add internal voltage calibration func.
//                             4. Global variables: voltage calibration related. 
// 03/22/16     Vincent.T      1. Add VM Kmode func.
//                             2. #include "vmsvc.h"
//                             3. Add VM global variables.
// 03/26/16     Vincent.T      1. Add AM Kmode func.
//                             2. #include "amsvc.h"
//                             3. Add AM global variables.
// 03/31/16     Vincent.T      1. Replace g_LangSelectIdx by g_LangSelectIdx;
//                             2. New global variable g_LangSelectIdx to get language index.
// 04/15/16     Vincent.T      1. Add IR Vload Kmode routine.
//                             2. global variables of IR test, such as: gVLoadVL, gVLoadVH, gVLoadADL and gVLoadADH.
// 06/14/16     Vincent.T      1. Modify AM calibration procedure.
// 06/27/16     Vincent.T      1. Modify USB upload data protocol.(USBPCModeSvc(), USBTestRecordUpload())
// 06/28/16     Vincent.T      1. New mode: TRDEL(Test record delete)
//                             2. Modify USBEchoPCMode(); for TRDEL
//                             3. global variable gui16TotalTestRecord.
// 07/05/16     Vincent.T      1. #include "..\service/tempsvc.h"
// 08/10/16     Vincent.T      1. global variable BodyTempCoe and ObjTempCoe.
//                             2. global variable gfIRCalCoe6V and gfIRCalCoe12V
//                             3. Modify AM and VM calbration mode and all related protocol.(more easier to K)
//                             4. New Vload IR and CCA calbration mode and all related protocol.(more easier to K)
//                             5. New calbration mode of temperature.(Make temperature can be K in usb mode)
// 08/24/16     Vincent.T      1. Modify IR K-Mode(if IR over range, show --.--)
// 09/06/16     Vincent.T      1. global variable gui16TestRecordOffset
//                             2. Test Record Delete: reset gui16TestRecordOffset
//                             3. Modify USBTestRecordUpload(); to implement cycled Test Records upload.
//                             4. Modify USBSendByte(); delay 3ms to stablize PC-K-tool
// 11/15/16     Ian.C          1. Modfiy USBPCModeSvc(); if(ui8USBModeSub == SUSB_LCD_STRART)... change the variable to make UARTprintf correct.
// 11/21/16     Vincent. T     1. global variable g_SNInfo.
//                             2. Modify USBPCModeSvc(); add new mode to read S/N.
//                             3. Modify DFU Mode for PCB_CHECK_MODE.(wirte S/N before DFU)
//                             4. Modify USBReceviedByte(); for new mode.(S/N Write)
//                             5. Modify USBEchoPCMode(); for S/N write mode.
//                             6. Modify USBSNPC(); to return S/N to PC.
// 12/02/16     Vincent.T      1. global variables for 3-wire compensation.(gf12IRH, gf12IRL, gf12IRH_COM and gf12IRL_COM)
//                             2. Modify Internal Resistor K-mode for 3-wire compensation(under USB_KIR)
//                             3. Modify 12V clamp setting(14.5 -> 15.5 to 15.5 -> 16.0)
//                             4. Modify USBReceviedByte(); for 3-wire compensation;
// 12/08/16     Vincent.T      1. Modify USB remove detection in USBPCModeSvc();
//                             2. Modify KIR mode to show 6V IR value after three-wire calibration.
// 12/30/16     Vincent.T      1. global variables: gVLoadVL_PLUS/MINS, gVLoadVH_PLUS/MINS, gVLoadADL_PLUS/MINS and gVLoadADH_PLUS/MINS
//                             2. Modify Load Resistor Voltage KMode(single point -> two point)
// 12/02/16     Vincent.T      1. global variables for 3-wire compensation.(gf6IRH, gf6IRL, gf6IRH_COM and gf6IRL_COM)
//                             2. Isolate S/N write mode(still under PCB_CHECK_MODE)
//                             3. Modify USBEchoPCMode();(new S/N wire mode 'S')
//                             4. Modify 6V Internal Resistor K-mode for 3-wire compensation(under USB_KIR)
//                             5. Modify USBPCShakeHand(); to know current F/W via USB
// 01/13/17     Vincent.T      1. Modify USBTestRecordUpload(); date length 64 -> 128
// 01/19/17     Vincent.T      1. Modify USBPCModeSvc(); make sure release correct memory address
//                             2. Modify USBSendByte(); check space before transmition.
// 02/06/17     Henry.Y        1. global variable:*ui8TesterName
//                             2. Enlarge the calibration tolerance of temp from 0.1 to 1.
//                             3. Modify object temp calibration formula.
//                             4. Modify the re-k loop of temperature(body and object) calibration.
//                             5. Modify USBPCShakeHand(): predefined symbol to seperate *ui8TesterName.
// 02/10/17     Vincent.T      1. Modify KCCA Mode(for 12V Three-Wire CCA Compensation)
//                             2. global variables CCA_Adjustment_12V_Low and CCA_Adjustment_12V_High
// 02/17/17     Vincent.T      1. global variables CCA_TestV_Low and CCA_TestV_High
//                             2. Modify KCCA mode(to access CCA_TestV_Low and CCA_TestV_High)
//                             3. Modify USBStorageAllocater();(for 3-wire 12V cca Kmode)
// 03/01/17     Vincent.T      1. Remove code related 3-wire CCA part.(No necessary) 
// 03/22/17     Vincent.T      1. #include "extrtcsvc.h"
//                             2. #include "..\driver/btuart_iocfg.h"
//                             3. new global variables for bluetooth check mode: blUsbBTchk and info->BTChkTimes
//                             4. Modify USBPCModeSvc();(add clock-IC check and bluetooth check)
//                             5. Modify USBStorageAllocater();(add clock-IC check and bluetooth check)
//                             6. Modify USBEchoPCMode(); (add clock-IC check and bluetooth check)
//                             7. new function: USBTime(); -> upload clock-IC data(second only)
// 03/24/17     Vincent.T      1. Modify USBPCModeSvc(); for BT check mode and clock check mode
// 04/05/17     Vincent.T      1. Modify USB Connection function.
// 06/01/17     Henry          FW version: BT2100_WW_V01.a
//                             Fix Bug: USB plug-in screen shift to main menu screen if internal battery is low before USB plug in.
// 11/06/17     Henry.Y	       1. Hide useless variables:ui8USBConnStat
//                             2. Add transition time 3sec when USB serial -> DFU.
//                             3. Apply BT2100_WW_V02.b USB modification(check releasenote or readme)
// 09/11/18     Henry.Y        Use systeminfo data struct to replace the global variables.
//                             Add bootloader: Jump_to_booloader_DFU
//                             Use system driver interface to replace global variable.
// 11/19/18     Henry.Y        Fix bug: wrong voltage channel setup by command. Add WIFI and scanner command length return.
// 08/13/20     David.Wang     1) BAT TEST : Add PACK TEST NUM register
//                             2) SYS TEST : Modify UPLOAD DATA
// 09/03/20     David.Wang     Fix little Kevin PC software bug
//
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/service/usbsvc.c#2 $
// $DateTime: 2017/11/27 17:53:57 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#include "usbsvc.h"
#include "../driver/delaydrv.h"
#include "SystemInfosvs.h"
#include "WIFI_service.h"



//*****************************************************************************
//
// Variables tracking transmit and receive counts.
//
//*****************************************************************************
volatile uint32_t g_ui32UARTTxCount = 0;
volatile uint32_t g_ui32UARTRxCount = 0;
#ifdef DEBUG
uint32_t g_ui32UARTRxErrors = 0;
#endif
//*****************************************************************************
//
// Global system tick counter
//
//*****************************************************************************
volatile uint32_t g_ui32SysTickCount = 0;

char *g_pcStatus;





//bool blDataRecComplete = false;
//bool blDataEndEvent = false;
void (*USB_cb)(uint8_t*, uint32_t) = NULL;





void Jump_to_booloader_DFU(void){
	USBDCDTerm(0);	//release USB device, so USB can be used by DFU.
	HWREG(NVIC_DIS0) = 0xffffffff;
	HWREG(NVIC_DIS1) = 0xffffffff;
	wifi_config(false, NULL);
	UARTprintf("Jump to Bootloader DFU\n");
	DELAY_ZERO_ZERO_ZERO_FIVE_SECOND;
	(*((void (*)(void))(*(unsigned long *)0x2C)))(); // jump to boot loader.
}

void USB_svc_set_CB(void(*cb)(uint8_t*, uint32_t)){
	USB_cb = cb;
}

uint8_t USBIRCCACalibration(USB_MODE ui8USBMode, uint8_t* CMD)
{
	uint8_t error = 0;
	uint32_t ui32buf;
	float fVBuf,fCCA,fIR;
	float* tmep_ptr;

	TEST_DATA* TD = Get_test_data();
	SYS_INFO* info = Get_system_info();

	switch(ui8USBMode){
		case USB_KCCA:
		{
			//*****************************
			//Cold Cranking Amper Kmode
			//*****************************
			if((!memcmp("C1",CMD,2) || !memcmp("C3",CMD,2)) && CMD[6] == '@'){
				fVBuf = ((CMD[2] - 0x30) * 1000) + ((CMD[3] - 0x30) * 100) + ((CMD[4] - 0x30) * 10) + ((CMD[5] - 0x30) * 1);
				
				if((CMD[1] == '1' && (fVBuf < 100 || fVBuf > 1500)) || (CMD[1] == '3' && (fVBuf < 100 || fVBuf > 1500))){
					error = 1;
					break;
				}
				
				TD->SYSTEM_CHANNEL = (CMD[1] == '1')? MAP_6V:MAP_12V;
				tmep_ptr = (CMD[1] == '1')? &info->CCA_Adjustment_6V:&info->CCA_Adjustment_12V;
				if(!BattCoreGetCCA(NULL, &fCCA, 3)){
					error = 1;
					break;
				}
				(*tmep_ptr) = (*tmep_ptr)*(fVBuf / fCCA);
				save_system_info(tmep_ptr, sizeof(float));
			}
			else if(!memcmp("c@",CMD,2)){
				//
				// Scan A/D Channel first
				//
				BattCoreScanVoltage(&fVBuf);
				if(TD->SYSTEM_CHANNEL == MAP_24V){
					error = 1;
					break;
				}
				// Get CCA Coe
				ui32buf = (TD->SYSTEM_CHANNEL == MAP_6V)? (info->CCA_Adjustment_6V*1000):(info->CCA_Adjustment_12V*1000);
				
				if(!BattCoreGetCCA(NULL, &fCCA, 3)){
					error = 1;
					break;
				}
				
				char buf[15] = {0x00};
				snprintf(buf, sizeof(buf),"%04d,%04d@",(uint16_t)fCCA,ui32buf);			
				USBSendByte((uint8_t*)buf, strlen(buf));
				return 0;
			}
			else error = 1;

			break;
		}
		case USB_KIR:
		{
			//*****************************
			//Internal Resistor Kmode
			//
			//	- 12V IR Calibration
			//	[dual load calibration only]
			//
			//*****************************
			if((!memcmp("R1",CMD,2)|| !memcmp("R3",CMD,2)) && CMD[6] == '@'){
				fVBuf = ((CMD[2] - 0x30) * 10) + ((CMD[3] - 0x30) * 1) + ((CMD[4] - 0x30) * 0.1) + ((CMD[5] - 0x30) * 0.01);

				if((CMD[1] == '1' && (fVBuf < 0.8 || fVBuf >99)) || (CMD[1] == '3' && (fVBuf < 0.8 || fVBuf >8))){
					error = 1;
					break;
				}
				
				TD->SYSTEM_CHANNEL = (CMD[1] == '1')? MAP_6V:MAP_12V;
				tmep_ptr = (CMD[1] == '1')? &info->IRCalCoe6V:&info->IRCalCoe12V;
				
				if(!BattCoreGetIR(&fIR, 6, dual_load_with_result)){
					error = 1;
					break;
				}
				(*tmep_ptr) = (*tmep_ptr)*(fVBuf / fIR);
				save_system_info(tmep_ptr, sizeof(float));			
			}
			else if(!memcmp("c@",CMD,2)){
				//
				// Scan A/D Channel first
				//
				BattCoreScanVoltage(&fVBuf);
				if(TD->SYSTEM_CHANNEL == MAP_24V){
					error = 1;
					break;
				}

				if(TD->SYSTEM_CHANNEL == MAP_6V){
					if(!BattCoreGetIR(&fIR, 3, dual_load_with_result)){
						error = 1;
						break;
					}
				}
				else{
					if(!BattCoreGetIR(&fVBuf, 1, dual_load_with_result)){
						error = 1;
						break;
					}
					
					if(!BattCoreGetIR(&fIR, 3, (fVBuf>ThreewireTh)? single_load_with_result:dual_load_with_result)){
						error = 1;
						break;
					}					
				}
				if(fIR > 99){
					error = 1;
					break;
				}
				// Get IR Coe
				ui32buf = (TD->SYSTEM_CHANNEL == MAP_6V)? (info->IRCalCoe6V*1000):(info->IRCalCoe12V*1000);
				fIR = fIR*100;
				
				char buf[15] = {0x00};
				snprintf(buf, sizeof(buf),"%04d,%04d@",(uint16_t)fIR,ui32buf);			
				USBSendByte((uint8_t*)buf, strlen(buf));
				return 0;	
			}
			else error = 1;
			
			break;
		}
	}
	
	if(error == 1){
		USBSendByte("!@", 2);
	}
	else{
		USBSendByte("#@", 2);
	}
	
	return error;
}


uint8_t USBVoltageCalibration(USB_MODE ui8USBMode, uint8_t* CMD)
{
	//**************************************************************************************
	// 6/12/24/VM/AM/load/temp Voltage calibration
	// Internal battery calibration
	//**************************************************************************************

	unsigned int uiVoltage_show =0;

	uint32_t voltage_scale = 0;
	float voltage_from_PC = 0;
	float fEmode_voltage_test = 0;

	uint8_t ui8ShowBuffer[5] = {0x00};
	uint8_t error = 0;

	TEST_DATA* TD = Get_test_data();
	SYS_INFO* info = Get_system_info();

	switch(ui8USBMode){
		case USB_EXTV_CAL:
		{
			if((CMD[0] == '1' || CMD[0] == '3' || CMD[0] == '4') &&	(CMD[1] == 'l' || CMD[1] == 'L' || CMD[1] == 'h' || CMD[1] == 'H') && CMD[7] == '@'){
				voltage_from_PC = (CMD[2] - 0x30)*10 + (CMD[3]-0x30)*1 + (CMD[4]-0x30)*0.1 + (CMD[5]-0x30)*0.01 + (CMD[6]-0x30)*0.001;
				
				TD->SYSTEM_CHANNEL = (CMD[0] == '1')? MAP_6V:(CMD[0] == '3')? MAP_12V:MAP_24V;
				BattCoreGetVoltage(&voltage_scale, &fEmode_voltage_test);
				if(voltage_scale == 4095){
					error = 1;
					break;
				}

				if(CMD[0] == '1'){					
					if(CMD[1] == 'l'||CMD[1] == 'L'){
						if(voltage_from_PC >= 1.5 && voltage_from_PC <= 2.0){
							info->V6_L = voltage_from_PC;
							save_system_info(&info->V6_L, sizeof(info->V6_L));
					
							info->AD_1_5_6 = voltage_scale;
							save_system_info(&info->AD_1_5_6, sizeof(info->AD_1_5_6));
						}
						else error = 1;
					}
					else{
						if(voltage_from_PC >= 6.500 && voltage_from_PC <= 7.500){
							info->V6_H = voltage_from_PC;
							save_system_info(&info->V6_H, sizeof(info->V6_H));
					
							info->AD_7 = voltage_scale;
							save_system_info(&info->AD_7, sizeof(info->AD_7));
						}
						else error = 1;
					}
				}
				else if(CMD[0] == '3'){
					if(CMD[1] == 'l'||CMD[1] == 'L'){
						if(voltage_from_PC >= 1.500 && voltage_from_PC <= 2.000){
							info->V12_L = voltage_from_PC;
							save_system_info(&info->V12_L, sizeof(info->V12_L));
					
							info->AD_1_5_12 = voltage_scale;
							save_system_info(&info->AD_1_5_12, sizeof(info->AD_1_5_12));
						}
						else error = 1;
					}
					else{
						if(voltage_from_PC >= 15.000 && voltage_from_PC <= 15.500){
							info->V12_H = voltage_from_PC;
							save_system_info(&info->V12_H, sizeof(info->V12_H));
					
							info->AD_15 = voltage_scale;
							save_system_info(&info->AD_15, sizeof(info->AD_15));
						}
						else error = 1;
					}
				}
				else{
					if(CMD[1] == 'l'||CMD[1] == 'L'){
						if(voltage_from_PC >= 1.500 && voltage_from_PC <= 2.000){
							info->V24_L = voltage_from_PC;
							save_system_info(&info->V24_L, sizeof(info->V24_L));
					
							info->AD_1_5_24 = voltage_scale;
							save_system_info(&info->AD_1_5_24, sizeof(info->AD_1_5_24));
						}
						else error = 1;
					}
					else{
						if(voltage_from_PC >= 29.500 && voltage_from_PC <= 30.500){
							info->V24_H = voltage_from_PC;
							save_system_info(&info->V24_H, sizeof(info->V24_H));
					
							info->AD_31 = voltage_scale;
							save_system_info(&info->AD_31, sizeof(info->AD_31));
						}
						else error = 1;
					}
				}			
			}
			else if(CMD[0] == 's' && (CMD[1] == '1' || CMD[1] == '6' || CMD[1] == 'c' || CMD[1] == 'C') && CMD[2] == '@'){//Show voltage
				if(CMD[1] == '1'){
					BattCoreScanVoltage(&fEmode_voltage_test);
				}
				else{
					TD->SYSTEM_CHANNEL = (CMD[1] == '6')? MAP_6V:MAP_12V;
					BattCoreGetVoltage(&voltage_scale, &fEmode_voltage_test);
				}

				uiVoltage_show = fEmode_voltage_test*1000;
				ui8ShowBuffer[0] = (uiVoltage_show/10000)+0x30;
				ui8ShowBuffer[1] = (uiVoltage_show%10000)/1000+0x30;
				ui8ShowBuffer[2] = (uiVoltage_show%1000)/100+0x30;
				ui8ShowBuffer[3] = (uiVoltage_show%100)/10+0x30;
				ui8ShowBuffer[4] = (uiVoltage_show%10)/1+0x30;
				USBSendByte(ui8ShowBuffer, 5);
				USBSendByte("@", 1);
				return 0;
			}
			else error = 1;
			break;
		}
		case USB_INTV_CAL:
		{
			BattCoreChkIntBatt(&voltage_scale ,&fEmode_voltage_test);
			if(CMD[0] == 'a' && CMD[5] == '@'){
				if(voltage_scale < 3000 || voltage_scale > 4000){
					error = 1;
					break;
				}
				
				voltage_from_PC = (CMD[1] - 0x30)*1 + (CMD[2] - 0x30)*0.1 +	(CMD[3] - 0x30)*0.01 + (CMD[4] - 0x30)*0.001;
				
				if(voltage_from_PC >= 7.5 && voltage_from_PC <= 9.0){					
					info->ADC_to_BAT_voltage = info->ADC_to_BAT_voltage*(voltage_from_PC/fEmode_voltage_test);			
					save_system_info(&info->ADC_to_BAT_voltage, sizeof(info->ADC_to_BAT_voltage));
				
					//Show
					BattCoreChkIntBatt(&voltage_scale ,&fEmode_voltage_test);
					uiVoltage_show = fEmode_voltage_test*1000;			
					ui8ShowBuffer[0] = (uiVoltage_show/1000)+0x30;
					ui8ShowBuffer[1] = (uiVoltage_show%1000)/100+0x30;
					ui8ShowBuffer[2] = (uiVoltage_show%100)/10+0x30;
					ui8ShowBuffer[3] = (uiVoltage_show%10)/1+0x30;			
					USBSendByte(ui8ShowBuffer, 4);		
					USBSendByte("@", 1);
					return 0;
				}
				else error = 1;
			}
			else if(!memcmp("s@",CMD,2)){
				uiVoltage_show = fEmode_voltage_test*1000;
				ui8ShowBuffer[0] = (uiVoltage_show/1000)+0x30;
				ui8ShowBuffer[1] = (uiVoltage_show%1000)/100+0x30;
				ui8ShowBuffer[2] = (uiVoltage_show%100)/10+0x30;
				ui8ShowBuffer[3] = (uiVoltage_show%10)/1+0x30;
				USBSendByte(ui8ShowBuffer, 4);			
				USBSendByte("@", 1);
				return 0;
			}
			else error = 1;
			break;
		}
		case USB_KVM:
		{
			if(CMD[0] == 'V' && (CMD[1] == 'l'||CMD[1] == 'L'||CMD[1] == 'h'||CMD[1] == 'H') && CMD[7] == '@'){
				if(CMD[1] == 'l'||CMD[1] == 'L'){
					//*******************************
					//			Low Voltage
					// 1. Check Voltage 4.5v ~ 5.5v
					//*******************************
					voltage_from_PC = (CMD[2] - 0x30) * 10 + (CMD[3] - 0x30) * 1 + (CMD[4] - 0x30) * 0.1 + (CMD[5] - 0x30) * 0.01 + (CMD[6] - 0x30) * 0.001;
					if(voltage_from_PC >= 4.5 && voltage_from_PC <= 5.5){
						info->VMCalLvol = voltage_from_PC;
						VMGetAD(&info->VMCalLAD, 100);
						//VM AD low voltage
						save_system_info(&info->VMCalLAD, sizeof(info->VMCalLAD));
	
						//VM Vol low voltage
						save_system_info(&info->VMCalLvol, sizeof(info->VMCalLvol));
					}
					else error = 1;	
				}
				else{//CMD[1] == 'h'||CMD[1] == 'H'
					//*******************************
					//			High Voltage
					// 1. Check Voltage 29.5v ~ 30.5v
					//*******************************
					voltage_from_PC  = ((CMD[2] - 0x30) * 10) + ((CMD[3] - 0x30) * 1) + ((CMD[4] - 0x30) * 0.1) + ((CMD[5] - 0x30) * 0.01) + ((CMD[6] - 0x30) * 0.001);
					if(voltage_from_PC >= 29.5 && voltage_from_PC <= 30.5){
						info->VMCaHLvol = voltage_from_PC;
						VMGetAD(&info->VMCaHLAD, 100);
						//VM AD High voltage
						save_system_info(&info->VMCaHLAD, sizeof(info->VMCaHLAD));

						//VM Vol High voltage
						save_system_info(&info->VMCaHLvol, sizeof(info->VMCaHLvol));
					}
					else error = 1;
				}
			}			
			else if(!memcmp("c@",CMD,2)){
				//*****************
				// Return VM value
				//*****************
				GetVM(&fEmode_voltage_test,&voltage_scale);
				uiVoltage_show = fEmode_voltage_test*1000;
				ui8ShowBuffer[0] = (uiVoltage_show/10000) + 0x30;
				ui8ShowBuffer[1] = ((uiVoltage_show%10000)/1000) + 0x30;
				ui8ShowBuffer[2] = ((uiVoltage_show%1000)/100) + 0x30;
				ui8ShowBuffer[3] = (uiVoltage_show%100)/10 + 0x30;
				ui8ShowBuffer[4] = (uiVoltage_show%10)/1 + 0x30;
				USBSendByte(ui8ShowBuffer, 5);	
				USBSendByte("@", 1);
				return 0;
			}
			else error = 1;
			break;	
		}
		case USB_KAM:
		{
			//*************************************************
			//
			// Check clamp voltage first!!(for activation -5V)
			//
			//*************************************************			
			BattCoreScanVoltage(&fEmode_voltage_test);
			if(fEmode_voltage_test <= 9){
				error = 1;
				break;
			}

			if(CMD[0] == 'A' && (CMD[1] == 'l'||CMD[1] == 'L'||CMD[1] == 'h'||CMD[1] == 'H') && CMD[7] == '@'){
				voltage_from_PC = ((CMD[2] - 0x30) * 100) + ((CMD[3] - 0x30) * 10) + ((CMD[4] - 0x30) * 1) + ((CMD[5] - 0x30) * 0.1) + ((CMD[6] - 0x30) * 0.01);
				
				if(CMD[1] == 'l'||CMD[1] == 'L'){	
					//*******************************
					//			Low Voltage
					// 1. Check Voltage 0v
					//*******************************
					if(voltage_from_PC <= 50){
						info->AMCalLvol = 0;
						AM_NEG5V_PIN_ON;
						//for(ui32LopIdx = 0; ui32LopIdx < 1000; ui32LopIdx++){
							AMGetAD(&info->AMCalLAD, 250);
							//ui64Buffer = ui64Buffer + info->AMCalLAD;
						//}						
						AM_NEG5V_PIN_OFF;
						
						//AM AD low voltage
						//info->AMCalLAD = (float)ui64Buffer / 1000;
						save_system_info(&info->AMCalLAD, sizeof(info->AMCalLAD));
						//AM Vol low voltage
						save_system_info(&info->AMCalLvol, sizeof(info->AMCalLvol));
					}
					else error = 1;
				}
				else{//CMD[1] == 'h'||CMD[1] == 'H'
					//*******************************
					//			High Voltage
					// 1. Check Voltage 475mv ~ 525mv
					//*******************************				
					if(voltage_from_PC >= 500 && voltage_from_PC <= 580){
						info->AMCalHvol = voltage_from_PC;
						AM_NEG5V_PIN_ON;
						//ui64Buffer = 0;
						//for(ui32LopIdx = 0; ui32LopIdx < 1000; ui32LopIdx++){
							AMGetAD(&info->AMCalHAD, 250);
							//ui64Buffer = ui64Buffer + info->AMCalHAD;
						//}
						AM_NEG5V_PIN_OFF;
					
						//AM AD High voltage
						//info->AMCalHAD = (float)ui64Buffer / 1000;
						save_system_info(&info->AMCalHAD, sizeof(info->AMCalHAD));
					
						//AM Vol High voltage
						save_system_info(&info->AMCalHvol, sizeof(info->AMCalHvol));
					}
					else error = 1;
				}
			}
			else if(!memcmp("c@",CMD,2)){

				//ui64Buffer = 0;
				//for(ui32LopIdx = 0; ui32LopIdx < 1000; ui32LopIdx++){
					GetAM(&fEmode_voltage_test,&voltage_scale);//mV to A(500mV = 500A)
					
					//ui64Buffer = ui64Buffer + fVBuf;
				//}
				// due to sample 1000 times -> total value / 1000 = average
				//fVBuf = ui64Buffer / 1000;
				ui8ShowBuffer[0] = ((uint32_t)fEmode_voltage_test/100) + 0x30;
				ui8ShowBuffer[1] = (((uint32_t)fEmode_voltage_test%100)/10) + 0x30;
				ui8ShowBuffer[2] = (((uint32_t)fEmode_voltage_test%10)/1) + 0x30;			
				USBSendByte(ui8ShowBuffer, 3);
				USBSendByte("@", 1);
				return 0;
			}
			else error = 1;

			
			//ROM_GPIOPinWrite(NEG5V_BASE, NEG5V_PIN, 0xFF);//off
			AM_NEG5V_PIN_OFF;
			break;
		}		
		case USB_KVL:
		{
			if(CMD[0] == 'L' && (CMD[8] == '@')){
				voltage_from_PC = ((CMD[3] - 0x30) * 10) + ((CMD[4] - 0x30) * 1) + ((CMD[5] - 0x30) * 0.1) + ((CMD[6] - 0x30) * 0.01) + ((CMD[7] - 0x30) * 0.001);
				if(!memcmp("L1L",CMD,3)){
					if( (voltage_from_PC >= 1.5 ) && (voltage_from_PC <= 2.0) ){
						info->VLoadVL_MINS = voltage_from_PC;
						BattCoreGetVLoadAD_MINS(&info->VLoadADL_MINS, 15);
					
						//VM AD low voltage
						save_system_info(&info->VLoadADL_MINS, sizeof(info->VLoadADL_MINS));
					
						//VM Vol low voltage
						save_system_info(&info->VLoadVL_MINS, sizeof(info->VLoadVL_MINS));
					}
					else error = 1;
				}
				else if(!memcmp("L1H",CMD,3)){
					if( (voltage_from_PC >= 6.5 ) && (voltage_from_PC <= 7.5) ){
						info->VLoadVH_MINS = voltage_from_PC;
						BattCoreGetVLoadAD_MINS(&info->VLoadADH_MINS, 15);
					
						//VM AD High voltage
						save_system_info(&info->VLoadADH_MINS, sizeof(info->VLoadADH_MINS));
					
						//VM Vol High voltage
						save_system_info(&info->VLoadVH_MINS, sizeof(info->VLoadVH_MINS));
					}
					else error = 1;
				}
				else if(!memcmp("L3L",CMD,3)){
					if( (voltage_from_PC >= 1.5 ) && (voltage_from_PC <= 2.0) ){
						info->VLoadVL_PLUS = voltage_from_PC;
						BattCoreGetVLoadAD_PLUS(&info->VLoadADL_PLUS, 15);
					
						//VM AD low voltage
						save_system_info(&info->VLoadADL_PLUS, sizeof(info->VLoadADL_PLUS));
					
						//VM Vol low voltage
						save_system_info(&info->VLoadVL_PLUS, sizeof(info->VLoadVL_PLUS));
					}
					else error = 1;
				}
				else if(!memcmp("L3H",CMD,3)){
					if( (voltage_from_PC >= 15.0 ) && (voltage_from_PC <= 15.5) ){
						info->VLoadVH_PLUS = voltage_from_PC;					
						BattCoreGetVLoadAD_PLUS(&info->VLoadADH_PLUS, 15);
						
						//VM AD High voltage
						save_system_info(&info->VLoadADH_PLUS, sizeof(info->VLoadADH_PLUS));
					
						//VM Vol High voltage
						save_system_info(&info->VLoadVH_PLUS, sizeof(info->VLoadVH_PLUS));
					}
					else error = 1;
				}
				else error = 1;
			}
			else if(!memcmp("c1@",CMD,3) || !memcmp("c3@",CMD,3)){
				if(CMD[1] == '1'){
					BattCoreGetVLoad_MINS(&fEmode_voltage_test, 15);			
				}
				else{
					BattCoreGetVLoad_PLUS(&fEmode_voltage_test, 15);
				}

				uiVoltage_show = fEmode_voltage_test * 1000;
				ui8ShowBuffer[0] = (uiVoltage_show/10000) + 0x30;
				ui8ShowBuffer[1] = ((uiVoltage_show%10000)/1000) + 0x30;
				ui8ShowBuffer[2] = ((uiVoltage_show%1000)/100) + 0x30;
				ui8ShowBuffer[3] = (uiVoltage_show%100)/10 + 0x30;
				ui8ShowBuffer[4] = (uiVoltage_show%10) + 0x30;
				USBSendByte(ui8ShowBuffer, 5);
				USBSendByte("@", 1);
				return 0;
			}
			else error = 1;			

			break;
		}
		case USB_KTEMP:
		{
			float fTemp,fbody;
			//*************************************************
			//
			// Check clamp voltage first!!(for activation -5V)
			//
			//*************************************************
			BattCoreScanVoltage(&fEmode_voltage_test);
			if(fEmode_voltage_test <= 9){
				error = 1;
				break;
			}
			if((!memcmp("TB",CMD,2) || !memcmp("TO",CMD,2)) && CMD[6] == '@'){
				voltage_from_PC = (CMD[2] - 0x30)*10 + (CMD[3] - 0x30)*1 + (CMD[4] - 0x30)*0.1 + (CMD[5] - 0x30)*0.01;
				if(CMD[1] == 'B'){
					if(voltage_from_PC > 10 && voltage_from_PC <= 40){
						//
						//Get Temperature as initial value
						//
						info->BodyTempCoe = BodyTempCoe_Default;
						GetBodyTemp(&fEmode_voltage_test);
						info->BodyTempCoe = info->BodyTempCoe * (voltage_from_PC / fEmode_voltage_test);
						// Write BodyTempCoe into internal eeprom
						save_system_info(&info->BodyTempCoe, sizeof(info->BodyTempCoe));
					}					
					else error = 1; 		
					
				}
				else{// if(CMD[1] == 'O')
					if(voltage_from_PC > 10 && voltage_from_PC <= 40){
						//
						//Get Temperature as initial value
						//
						info->ObjTempCoe = ObjTempCoe_Default;
						GetObjTemp(&fTemp,&fbody);						
						
						float obj_vol_1 = get_obj_vol(fbody, fTemp);
						float obj_vol_2 = get_obj_vol(fbody, voltage_from_PC);
						info->ObjTempCoe += (obj_vol_2-obj_vol_1);
						
						//
						// Write ObjTempCoe into internal eeprom
						//
						save_system_info(&info->ObjTempCoe, sizeof(info->ObjTempCoe));
					}
					else error = 1; 		
				}
			}
			else if(!memcmp("c@",CMD,2))
			{
				float fTemp,fbody;
				char buf[15] = {0x00};
				//*******************************************
				// Return Temperature value(Body and Object)
				//*******************************************
				//
				//Get Temperature
				//
				GetObjTemp(&fTemp,&fbody);
				fTemp = fTemp*100;
				fbody = fbody*100;
				snprintf(buf,sizeof(buf),"B%04d,O%04d@",(int16_t)fbody,(int16_t)fTemp);
				
				USBSendByte((uint8_t*)buf, strlen(buf));				
				return 0;
			}
			else error = 1;
			break;
		}
		default:
		{
			error = 1;
			break;
		}	
	}

	if(error == 1){
		USBSendByte("!@", 2);
	}
	else{
		USBSendByte("#@", 2);
	}
	
	return error;
}






uint16_t USBStorageAllocater(uint8_t ui8USBMode, uint8_t ui8USBModeSub)
{
	uint16_t malloc_size;
	//***********
	//Mode Select
	//***********
	if(ui8USBMode == USB_READY){
		malloc_size = USB_READY_LEN;
	}
	else if(ui8USBMode == USB_PRT_UP){
		//*******************************
		//main mode: usb printout up data
		//*******************************
		switch(ui8USBModeSub){
		case SUSB_PRT_START:
			malloc_size = USB_PRT_START_LEN;
			break;
		case SUSB_PRT_DATA:
			malloc_size = USB_PRT_UPDATE_LEN;
			break;
		case SUSB_PRT_END:
		case SUSB_PRT_READY:
		default:
			malloc_size = USB_READY_LEN;
			break;
		}
	}
	else if(ui8USBMode == USB_PRT_READ)
	{
		//*******************************
		//main mode: usb printout up data
		//*******************************
		switch(ui8USBModeSub){
		case SUSB_PRT_START:
			malloc_size = USB_PRT_START_LEN;
			break;
		case SUSB_PRT_READY:		
		case SUSB_PRT_DATA:
		case SUSB_PRT_END:
		default:
			malloc_size = USB_READY_LEN;
			break;
		}
	}
	else if(ui8USBMode == USB_LCD_UP)
	{
		//*******************************
		//main mode: usb printout up data
		//*******************************
		switch(ui8USBModeSub)
		{
		case SUSB_LCD_STRART:
			malloc_size = USB_LCD_START_LEN;
			break;
		case SUSB_LCD_DATA:
			malloc_size = USB_LCD_UPDATE_LEN;
			break;
		case SUSB_LCD_READY:
		case SUSB_LCD_END:
		default:
			malloc_size = USB_READY_LEN;
			break;
		}
	}
	else if(ui8USBMode == USB_LCD_READ)
	{
		//*******************************
		//main mode: usb printout up data
		//*******************************
		switch(ui8USBModeSub)
		{
		case SUSB_LCD_STRART:
			malloc_size = USB_LCD_START_LEN;
			break;
		case SUSB_LCD_READY:
		case SUSB_LCD_DATA:
		case SUSB_LCD_END:
		default:
			malloc_size = USB_READY_LEN;
			break;
		}
	}
	else if(ui8USBMode == USB_SEND_DAT)
	{		
		malloc_size = USB_READY_LEN;
	}
	else if(ui8USBMode == USB_EXTV_CAL || ui8USBMode == USB_INTV_CAL)
	{
		malloc_size = USB_CAL_LEN;
	}
	else if(ui8USBMode == USB_KVM)
	{
		malloc_size = VMPOINT_LEN;
	}
	else if(ui8USBMode == USB_KAM)
	{
		malloc_size = AMPOINT_LEN;
	}
	else if(ui8USBMode == USB_KVL)
	{
		malloc_size = VLPOINT_LEN;
	}
	else if(ui8USBMode == USB_KTEMP)
	{
		malloc_size = TMPOINT_LEN;
	}
	else if(ui8USBMode == USB_KCCA)
	{
		malloc_size = CCAPOINT_LEN;
	}
	else if(ui8USBMode == USB_KIR)
	{
		malloc_size = IRPOINT_LEN;
	}
	else if(ui8USBMode == USB_SN)
	{
		if(ui8USBModeSub == USB_SN_READY)	malloc_size = SN_LEN_READY;
		else if(ui8USBModeSub == USB_SN_C_WRITE) malloc_size = SNC_LEN;
		else if(ui8USBModeSub == USB_SN_P_WRITE) malloc_size = SNP_LEN;
		else{
			UARTprintf("USB_SN melloc error");
			while(1);
		}
	}
	else if(ui8USBMode == USB_TIME)
	{
		malloc_size = USB_READY_LEN;
	}
	else if(ui8USBMode == USB_BUTTON || ui8USBMode == USB_DETECT_VOLTAGE)
	{
		malloc_size = USB_READY_LEN;
	}
	else if(ui8USBMode == USB_WIFI)
	{
		malloc_size = WIFI_FUNC;
	}
	else if(ui8USBMode == USB_SCANNER){
		malloc_size = SCANNER_CMD_LEN;
	}
    else if(ui8USBMode == USB_FLASH){
        malloc_size = FLASH_SECTOR_SIZE;
    }
	else
	{
		//********
		//Trap M4
		//********
		while(1);//debug
	}

	return malloc_size;	
}

void USBSendByte(uint8_t *pui8DataBuff, uint32_t ui8DataLen)
{
	/*UARTprintf("USB send ");
	uint32_t i;
	for(i= 0; i<ui8DataLen; i++){
		UARTprintf("%c ",pui8DataBuff[i]);
	}
	UARTprintf("\n");*/
	uint32_t send_len;

	while(ui8DataLen != 0){
		send_len = USBBufferSpaceAvailable((tUSBBuffer *) &g_sTxBuffer);
	
		if(send_len){
			if(send_len > ui8DataLen) send_len = ui8DataLen;
	
			USBBufferWrite((tUSBBuffer *) &g_sTxBuffer, (const unsigned char *)pui8DataBuff, send_len);
			ui8DataLen -= send_len;
			pui8DataBuff += send_len;
		}
	
	}

	
}

uint8_t ASCII2HEX(uint8_t *ui8ASCII, uint8_t ui8DataLen)
{
	uint8_t ui8DatIdx = 0;
	uint8_t ui8DataBuf = 0;
	UARTprintf("ASCII2HEX()...\n");
	if(ui8DataLen > 2)
	{
		UARTprintf("Error!! ui8DataLen > 2( 2 Max)!!...\n");
	}

	for(ui8DatIdx = 0; ui8DatIdx < ui8DataLen; ui8DatIdx++)
	{
		//**************
		//ui8ASCII 0 ~ 9
		//**************
		if((ui8ASCII[ui8DatIdx] >= 0x30) && (ui8ASCII[ui8DatIdx] <= 0x39))
		{
			ui8ASCII[ui8DatIdx] = ui8ASCII[ui8DatIdx] - 0x30;
		}
		//**************
		//ui8ASCII A ~ F
		//**************
		else if((ui8ASCII[ui8DatIdx] >= 0x41) && (ui8ASCII[ui8DatIdx] <= 0x46))
		{
			ui8ASCII[ui8DatIdx] = ui8ASCII[ui8DatIdx] - 0x37;
		}
		else
		{
			UARTprintf("Error!! Input Data Error(Data > 0xF)!!...\n");
		}
		ui8DataBuf = ui8DataBuf + (ui8ASCII[ui8DatIdx]* pow(16, (1 - ui8DatIdx)));
	}
	return ui8DataBuf;
}

void USBDataBufASCII2HEX(uint8_t *pui8DatBuffSrc, uint8_t *pui8DatBuffDest, uint32_t ui32DatLen)
{
	uint32_t ui8LoopIndex = 0;
	uint32_t ui8DataIndex = 0;
	uint8_t ui8DataBuf = 0;

	UARTprintf("USBDataBufASCII2HEX()...\n");
	for(ui8LoopIndex = 0, ui8DataIndex = 0; ui8DataIndex < ui32DatLen; ui8LoopIndex = ui8LoopIndex + 2, ui8DataIndex++)
	{
		ui8DataBuf = ASCII2HEX(&pui8DatBuffSrc[ui8LoopIndex], 2);
		pui8DatBuffDest[ui8DataIndex] = ui8DataBuf;
	}
}

void USBDataMove(uint8_t *ui8DatBuffSrc, uint8_t *ui8DatBuffDest, uint32_t ui32DatLen)
{
	uint32_t ui32DatLenIndex = 0;
	UARTprintf("USBDataMove()...\n");
	for(ui32DatLenIndex = 0; ui32DatLenIndex < ui32DatLen; ui32DatLenIndex++)
	{
		ui8DatBuffDest[ui32DatLenIndex] = ui8DatBuffSrc[ui32DatLenIndex];
	}
}

void USBPrtExtMemICUpdate(uint8_t *ui8DatBuff, uint8_t langIdx, uint8_t ui8PrtFldIdx)
{
	UARTprintf("USBPrtExtMemICUpdate()...\n");
	if(ExtFlsPrtUpdData(ui8DatBuff, (LANGUAGE)langIdx, (PRT_FIELD_SEL)ui8PrtFldIdx))
	{
		UARTprintf("USBPrtExtMemICUpdate() Success!...\n");
	}
	else
	{
		UARTprintf("USBPrtExtMemICUpdate() Failed!...\n");
	}
}

void USBLCDScreenUpdate(LANGUAGE langIdx, uint8_t ui8ScrnIdx, uint8_t *lcdScrnBuff, uint32_t bufLen)
{
	// Clears the screen
	ST7565ClearScreen(BUFFER0);

	// Refresh the screen to see the results
	ST7565Refresh(BUFFER0);
	ExtFlsLCDUpdData(langIdx, ui8ScrnIdx, lcdScrnBuff, bufLen);
	LcdDrawScreen( ui8ScrnIdx, BUFFER0);
}

void USBLCDScreenRead(LANGUAGE langIdx, uint8_t ui8ScrnIdx)
{
	// Clears the screen
	ST7565ClearScreen(BUFFER0);

	// Refresh the screen to see the results
	ST7565Refresh(BUFFER0);

	LcdDrawScreen( ui8ScrnIdx, BUFFER0);
}

void USBTestRecordUpload(void)
{
	uint8_t ui8TestRecord[4] = {0x00};
	uint8_t ui8RecordBuf[128] = {0x00};
	uint8_t ui8RecordBuf_sting[128];
	uint8_t ui8DatBuf = 0;
	uint16_t ui16TestRecordIdx = 0;
	uint16_t ui16TRCounter = 0;
	uint32_t ui32TestRecord = 0;
	SYS_INFO* info = Get_system_info();

	//*************************************
	//             Total Number
	//         Read forme InitEEprom
	//**************************************
	UARTprintf("USBTestRecordUpload()...\n");
	ui8DatBuf = 'a';
	USBSendByte(&ui8DatBuf, 1);


	//Read test record counter
	ui32TestRecord = info->TotalTestRecord + info->TotalSYSrecord;

	if(ui32TestRecord != 0){
		ui8TestRecord[0] = ((ui32TestRecord / 1000) % 10) + 0x30;
		ui8TestRecord[1] = ((ui32TestRecord / 100) % 10)+ 0x30;
		ui8TestRecord[2] = ((ui32TestRecord % 100) / 10) + 0x30;
		ui8TestRecord[3] = (ui32TestRecord % 10) + 0x30;

		//**************************************
		//          Test Records Upload
		//**************************************
		USBSendByte(ui8TestRecord, 4);

		//***********************
		//          Separate
		//***********************
		ui8DatBuf = ',';
		USBSendByte(&ui8DatBuf, 1);

		//*****************************************************
		//             Read Bat Test Data From  ExtEEprom
		//*****************************************************
		for(ui16TestRecordIdx = info->TestRecordOffset; ui16TestRecordIdx < (info->TotalTestRecord + info->TestRecordOffset); ui16TestRecordIdx++)
		{
			TestRecordRead((ui16TestRecordIdx % MAX_BTSSTESTCOUNTER), ui8RecordBuf);

			TEST_RECORD_T* rec = (TEST_RECORD_T*) ui8RecordBuf;

			char end[2] = {0x13, 0x00};

			switch(rec->Test_Type){
			case Start_Stop_Test:
			case Battery_Test :{
				BATTERY_SARTSTOP_RECORD_T* BT_rec = (BATTERY_SARTSTOP_RECORD_T*)ui8RecordBuf;
				sprintf((char*)ui8RecordBuf_sting, "%d,%d,%d,%d,%04.0f,%04d,%04d,%d,%03d,%03d,%04.0f,%s,%s,",
						BT_rec->Test_Type, BT_rec->Battery_Type, BT_rec->SSBattery_Type, BT_rec->Ratting, (BT_rec->Voltage)*100,
						BT_rec->CCA_Capacity, BT_rec->CCA_Measured, BT_rec->Judgement, BT_rec->SOH, BT_rec->SOC,
						BT_rec->Temperature, BT_rec->Test_code, BT_rec->JIS );

			    if(BT_rec->IR > 99.99){
			        sprintf((char*)ui8RecordBuf_sting + strlen((char*)ui8RecordBuf_sting), "----,");
			    }
                else{
				    sprintf((char*)ui8RecordBuf_sting + strlen((char*)ui8RecordBuf_sting), "%04.0f,", (BT_rec->IR)*100);
			    }

			    sprintf((char*)ui8RecordBuf_sting + strlen((char*)ui8RecordBuf_sting),"%04d/%02d/%02d/%02d:%02d:%02d,%s",
						BT_rec->Test_Time.year,	BT_rec->Test_Time.month, BT_rec->Test_Time.day,
						BT_rec->Test_Time.hour,	BT_rec->Test_Time.minute, BT_rec->Test_Time.second,end );

				/* no used data
				---------------------------
				,%d = BT_rec->PackTestNum,
				,%d = BT_rec->PackTestCount,
                ,% 5.0f = ((BT_rec->Temperature)*100*9/5) + 32,
                ,%s = BT_rec->RO,
                ,%s = BT_rec->VIN,
                ,%d = BT_rec->RC_judegment,
				*/

				USBSendByte(ui8RecordBuf_sting, strlen((char*)ui8RecordBuf_sting));

			}
			break;
			case System_Test :{
				SYSTEM_TEST_RECORD_T* SYS_rec = (SYSTEM_TEST_RECORD_T*)ui8RecordBuf;
				sprintf((char*)ui8RecordBuf_sting, "%d,%d,",
						SYS_rec->Test_Type, SYS_rec->Crank_result);

				if(SYS_rec->Crank_result == NO_DETECT){
					sprintf((char*)ui8RecordBuf_sting + strlen((char*)ui8RecordBuf_sting), "----,");
				}
				else{
					sprintf((char*)ui8RecordBuf_sting + strlen((char*)ui8RecordBuf_sting), "%04.0f,", (SYS_rec->Crank_voltage)*100);
				}

				sprintf((char*)ui8RecordBuf_sting + strlen((char*)ui8RecordBuf_sting), "%d,%04.0f,%d,",
						SYS_rec->Idle_result, (SYS_rec->Idle_voltage)*100, SYS_rec->Ripple_result);

				if(SYS_rec->Ripple_result == NO_DETECT){
					sprintf((char*)ui8RecordBuf_sting + strlen((char*)ui8RecordBuf_sting), "----,");
				}
				else{
					sprintf((char*)ui8RecordBuf_sting + strlen((char*)ui8RecordBuf_sting), "%04.0f,", (SYS_rec->Ripple_voltage)*100);
				}

				sprintf((char*)ui8RecordBuf_sting + strlen((char*)ui8RecordBuf_sting), "%d,%04.0f,%04.0f,%04d/%02d/%02d/%02d:%02d:%02d,%s",
				        SYS_rec->Loaded_result, (SYS_rec->Loaded_voltage)*100,
						(SYS_rec->Loaded_voltage)*100,
						SYS_rec->Test_Time.year, SYS_rec->Test_Time.month, SYS_rec->Test_Time.day,
						SYS_rec->Test_Time.hour, SYS_rec->Test_Time.minute, SYS_rec->Test_Time.second, end );


				USBSendByte(ui8RecordBuf_sting, strlen((char*)ui8RecordBuf_sting));
			}
				break;
			default :
				UARTprintf("test record type error.\n");
				break;
			}

			if(((ui16TRCounter + 1) % 250) == 0)
			{
				ui8DatBuf = '$';
				USBSendByte(&ui8DatBuf, 1);
			}
			ui16TRCounter++;
		}

        //*****************************************************
        //             Read SYS Test Data From  ExtEEprom
        //*****************************************************

        for(ui16TestRecordIdx = info->SYSrecordOffset; ui16TestRecordIdx < (info->TotalSYSrecord + info->SYSrecordOffset); ui16TestRecordIdx++)
        {
            TestRecordSYSRead((ui16TestRecordIdx % MAX_SYSTESTCOUNTER), ui8RecordBuf);

            char end[2] = {0x13, 0x00};

                SYSTEM_TEST_RECORD_T* SYS_rec = (SYSTEM_TEST_RECORD_T*)ui8RecordBuf;

                sprintf((char*)ui8RecordBuf_sting, "%d,%d,",
                        SYS_rec->Test_Type, SYS_rec->Crank_result);

                if(SYS_rec->Crank_result == NO_DETECT){
                    sprintf((char*)ui8RecordBuf_sting + strlen((char*)ui8RecordBuf_sting), "----,");
                }
                else{
                    sprintf((char*)ui8RecordBuf_sting + strlen((char*)ui8RecordBuf_sting), "%04.0f,", (SYS_rec->Crank_voltage)*100);
                }

                sprintf((char*)ui8RecordBuf_sting + strlen((char*)ui8RecordBuf_sting), "%d,%04.0f,%d,",
                        SYS_rec->Idle_result, (SYS_rec->Idle_voltage)*100, SYS_rec->Ripple_result);

                if(SYS_rec->Ripple_result == NO_DETECT){
                    sprintf((char*)ui8RecordBuf_sting + strlen((char*)ui8RecordBuf_sting), "----,");
                }
                else{
                    sprintf((char*)ui8RecordBuf_sting + strlen((char*)ui8RecordBuf_sting), "%04.0f,", (SYS_rec->Ripple_voltage)*100);
                }

                sprintf((char*)ui8RecordBuf_sting + strlen((char*)ui8RecordBuf_sting), "%d,%04.0f,%04d/%02d/%02d/%02d:%02d:%02d%s",
                        SYS_rec->Loaded_result, (SYS_rec->Loaded_voltage)*100,
                        SYS_rec->Test_Time.year, SYS_rec->Test_Time.month, SYS_rec->Test_Time.day,
                        SYS_rec->Test_Time.hour, SYS_rec->Test_Time.minute, SYS_rec->Test_Time.second, end );


                USBSendByte(ui8RecordBuf_sting, strlen((char*)ui8RecordBuf_sting));

            if(((ui16TRCounter + 1) % 250) == 0)
            {
                ui8DatBuf = '$';
                USBSendByte(&ui8DatBuf, 1);
            }
            ui16TRCounter++;
        }

	}
	ui8DatBuf = '@';
	USBSendByte(&ui8DatBuf, 1);	
}

//
//
//Read S/N Number -> PC
//
//
void USBSNPC(uint8_t *serial_number,uint8_t len)
{
	uint8_t ui8LopIdx = 0;

	for(ui8LopIdx = 0; ui8LopIdx < len; ui8LopIdx++){
		if(serial_number[ui8LopIdx] == '@'){
			break;
		}
	}
	USBSendByte(serial_number, ui8LopIdx+1);
}

//
//
// Read time and upload
//
//
void USBTime(void)
{
	//**********************
	//
	// Date & Time variables
	//
	//**********************
	uint16_t year= 0;
	uint8_t  month = 0;
	uint8_t  date = 0;
	uint8_t  day = 0;
	uint8_t  hour = 0;
	uint8_t  min = 0;
	uint8_t  sec = 0;
	uint8_t ui8DatBuf[2] = {0x00};

	UARTprintf("USBTime()...\n");

	ExtRTCRead( &year, &month, &date, &day, &hour, &min, &sec);

	ui8DatBuf[0] = (sec / 10) + 0x30;
	ui8DatBuf[1] = (sec % 10) + 0x30;

	USBSendByte(ui8DatBuf, 2);
}


uint8_t ascii2hex(uint8_t msb, uint8_t lsb)
{
	uint8_t hex;
	
 	if((0x2F < msb) && (msb < 0x3A))		// check if numeric 0 - 9
 	{
 		msb -= 0x30;			// if so subtract 48
 	}
 	else
 	{
 		if((0x40 < msb) && (msb < 0x47))	// otherwise check if alpha A - F
 		{
 			msb -= 0x37;		// if so subtract 55
 		}
 		else
 		{
 			return 0;			// just return 0 if outside value
 		}
 	}
 	hex = msb * 0x10;			// multiply by 16 (rotate left 4) and store
 
 	if((0x2F < lsb) && (lsb < 0x3A))
 	{
 		lsb -= 0x30;
 	}
 	else
 	{
 		if((0x40 < lsb) && (lsb < 0x47))
 		{
 			lsb -= 0x37;
 		}
 		else
 		{
 			return 0;
 		}
 	}
 	hex += lsb;
 
 	return hex;
}


//*****************************************************************************
//
// Interrupt handler for the system tick counter.
//
//*****************************************************************************
void
SysTickIntHandler(void)
{
    //
    // Update our system time.
    //
    g_ui32SysTickCount++;	
}



//*****************************************************************************
//
// Set the state of the RS232 RTS and DTR signals.
//
//*****************************************************************************
static void
SetControlLineState(uint16_t ui16State)
{
    //
    // TODO: If configured with GPIOs controlling the handshake lines,
    // set them appropriately depending upon the flags passed in the wValue
    // field of the request structure passed.
    //
}





//*****************************************************************************
//
// This function sets or clears a break condition on the redirected UART RX
// line.  A break is started when the function is called with \e bSend set to
// \b true and persists until the function is called again with \e bSend set
// to \b false.
//
//*****************************************************************************
static void
SendBreak(bool bSend)
{
    //
    // Are we being asked to start or stop the break condition?
    //
    if(!bSend)
    {
        //
        // Remove the break condition on the line.
        //
        //ROM_UARTBreakCtl(USB_UART_BASE, false);
        g_bSendingBreak = false;
    }
    else
    {
        //
        // Start sending a break condition on the line.
        //
        //ROM_UARTBreakCtl(USB_UART_BASE, true);
        g_bSendingBreak = true;
    }
}


//*****************************************************************************
//
// Handles CDC driver notifications related to control and setup of the device.
//
// \param pvCBData is the client-supplied callback pointer for this channel.
// \param ui32Event identifies the event we are being notified about.
// \param ui32MsgValue is an event-specific value.
// \param pvMsgData is an event-specific pointer.
//
// This function is called by the CDC driver to perform control-related
// operations on behalf of the USB host.  These functions include setting
// and querying the serial communication parameters, setting handshake line
// states and sending break conditions.
//
// \return The return value is event-specific.
//
//*****************************************************************************
uint32_t
ControlHandler(void *pvCBData, uint32_t ui32Event,
               uint32_t ui32MsgValue, void *pvMsgData)
{	
	static tLineCoding setting = {.ui32Rate = 115200, .ui8Stop = USB_CDC_STOP_BITS_1, .ui8Parity = USB_CDC_PARITY_NONE , .ui8Databits = 8};
	uint32_t ui32IntsOff;

    //
    // Which event are we being asked to process?
    //
    switch(ui32Event)
    {
        //
        // The host has disconnected.
        //
        case USB_EVENT_DISCONNECTED:
        {
        	//ui8USBConnStat = USB_DISCONNECTED;
            //g_bUSBConfigured = false;
            ui32IntsOff = ROM_IntMasterDisable();
            g_pcStatus = "Disconnected";

            if(!ui32IntsOff)
            {
                ROM_IntMasterEnable();
            }
            break;
        }

        //
        // Return the current serial communication parameters.
        //
        case USBD_CDC_EVENT_GET_LINE_CODING:
        {
			tLineCoding * temp = (tLineCoding*)pvMsgData;
			temp->ui32Rate = setting.ui32Rate;
			temp->ui8Databits = setting.ui8Databits;
			temp->ui8Parity = setting.ui8Parity;
			temp->ui8Stop = setting.ui8Stop;
            break;
        }        

        //
        // Set the current serial communication parameters.
        //
        case USBD_CDC_EVENT_SET_LINE_CODING:
        {
			tLineCoding * temp = (tLineCoding*)pvMsgData;
			setting.ui32Rate = temp->ui32Rate;
			setting.ui8Databits = temp->ui8Databits;
			setting.ui8Parity = temp->ui8Parity;
			setting.ui8Stop = temp->ui8Stop;            
            break;
        }

        //
        // Set the current serial communication parameters.
        //
        case USBD_CDC_EVENT_SET_CONTROL_LINE_STATE:
            SetControlLineState((uint16_t)ui32MsgValue);
            break;

        //
        // Send a break condition on the serial line.
        //
        case USBD_CDC_EVENT_SEND_BREAK:
            SendBreak(true);
            break;

        //
        // Clear the break condition on the serial line.
        //
        case USBD_CDC_EVENT_CLEAR_BREAK:
            SendBreak(false);
            break;

        //
        // Ignore SUSPEND and RESUME for now.
        //
        case USB_EVENT_SUSPEND:
        case USB_EVENT_RESUME:
            break;

            
		//
		// We are connected to a host and communication is now possible.
		//
		case USB_EVENT_CONNECTED:
		{
			//g_bUSBConfigured = true;
			//ui8USBConnStat = USB_CONNECTED;
		
			//
			// Flush our buffers.
			//
			USBBufferFlush(&g_sTxBuffer);
			USBBufferFlush(&g_sRxBuffer);
		
			//
			// Tell the main loop to update the display.
			//
			ui32IntsOff = ROM_IntMasterDisable();
			g_pcStatus = "Connected";
			if(!ui32IntsOff)
			{
				ROM_IntMasterEnable();
			}
			break;
		}
			

        //
        // We don't expect to receive any other events.  Ignore any that show
        // up in a release build or hang in a debug build.
        //
        default:
            break;
    }

    return(0);
}

//*****************************************************************************
//
// Handles CDC driver notifications related to the transmit channel (data to
// the USB host).
//
// \param ui32CBData is the client-supplied callback pointer for this channel.
// \param ui32Event identifies the event we are being notified about.
// \param ui32MsgValue is an event-specific value.
// \param pvMsgData is an event-specific pointer.
//
// This function is called by the CDC driver to notify us of any events
// related to operation of the transmit data channel (the IN channel carrying
// data to the USB host).
//
// \return The return value is event-specific.
//
//*****************************************************************************
uint32_t
TxHandler(void *pvCBData, uint32_t ui32Event, uint32_t ui32MsgValue,
          void *pvMsgData)
{
    //
    // Which event have we been sent?
    //
    switch(ui32Event)
    {
        case USB_EVENT_TX_COMPLETE:
            //
            // Since we are using the USBBuffer, we don't need to do anything
            // here.
            //
            break;
        default:
            break;

    }
    return(0);
}

//*****************************************************************************
//
// Take as many bytes from the transmit buffer as we have space for and move
// them into the USB UART's transmit FIFO.
//
//*****************************************************************************
/*
static void
USBUARTPrimeTransmit(uint32_t ui32Base)
{
    uint32_t ui32Read;
    uint8_t ui8Char;

    while(1)
    {
        //
        //Read Data
        //
        ui32Read = USBBufferRead((tUSBBuffer *)&g_sRxBuffer, &ui8Char, 1);
        if(ui32Read)
        {
            //
            //End Symbol Check
            //
           if(USBRecEndSymbolCheck(ui8Char, &ui8USBMode, &ui8USBModeSub))
           {
        	   //
        	   //Change global variables status
        	   //
        	   blDataEndEvent = true;
        	   ui8DataLenRec = 0;
            }
            //
            //Data Buffer Check
            //
            else if(USBReceviedByte(ui8USBMode, ui8USBModeSub, &ui8Char, pui8USBUpdataBuf))
            {
            	//
            	//Change global variables status(in function)
            	//
            }
        }
        else
        {
            return;
        }
    }

}
*/

//*****************************************************************************
//
// Handles CDC driver notifications related to the receive channel (data from
// the USB host).
//
// \param ui32CBData is the client-supplied callback data value for this channel.
// \param ui32Event identifies the event we are being notified about.
// \param ui32MsgValue is an event-specific value.
// \param pvMsgData is an event-specific pointer.
//
// This function is called by the CDC driver to notify us of any events
// related to operation of the receive data channel (the OUT channel carrying
// data from the USB host).
//
// \return The return value is event-specific.
//
//*****************************************************************************
uint32_t
RxHandler(void *pvCBData, uint32_t ui32Event, uint32_t ui32MsgValue,
          void *pvMsgData)
{
    uint32_t len;
    //uint32_t ui32Count;

    //
    // Which event are we being sent?
    //
    switch(ui32Event)
    {
        //
        // A new packet has been received.
        //
        case USB_EVENT_RX_AVAILABLE:
        {
            //
            // Feed some characters into the UART TX FIFO and enable the
            // interrupt so we are told when there is more space.
            //
            //USBUARTPrimeTransmit(USB_UART_BASE);
            //ROM_UARTIntEnable(USB_UART_BASE, UART_INT_TX);
			
			len = USBBufferDataAvailable(&g_sRxBuffer);
			uint8_t *buf = malloc(len);
			len = USBBufferRead(&g_sRxBuffer, buf, len);
			
			if(USB_cb) USB_cb(buf, len);
			free(buf);
            
            break;
        }

        //
        // We are being asked how much unprocessed data we have still to
        // process. We return 0 if the UART is currently idle or 1 if it is
        // in the process of transmitting something. The actual number of
        // bytes in the UART FIFO is not important here, merely whether or
        // not everything previously sent to us has been transmitted.
        //
        case USB_EVENT_DATA_REMAINING:
        /*{
            //
            // Get the number of bytes in the buffer and add 1 if some data
            // still has to clear the transmitter.
            //
            ui32Count = ROM_UARTBusy(USB_UART_BASE) ? 1 : 0;
            return(ui32Count);
        }*/


        //
        // We don't expect to receive any other events.  Ignore any that show
        // up in a release build or hang in a debug build.
        //
        default:
        	break;
    }

    return(0);
}

bool USBConnect(void)
{
	uint16_t count = 0;

	while(ROM_GPIOPinRead(GPIO_PORTB_BASE,GPIO_PIN_1)){
		count++;
		if(count>500) return true;
	}

	return false;
}



