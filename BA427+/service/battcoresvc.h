
//*****************************************************************************
//
// Source File: battcoresvc.h
// Description: Battery Tester Core Functions' Definition.
//				
//
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 10/07/15     William.H      Created file.
// 10/19/15     William.H      Its porting the CCA Load-On and BT/SS test related APIs from SBT1_SC_V00 project to BT2020 service/battcoresvc.c.
// 11/03/15     Vincent.T      1. New function to integrate all soc functions
//                             2. Include mapsvc.h
//                             3. Mask BattCoreCalcSocConv6V()/BattCoreCalcSocConvEFB()/BattCoreCalcSocVRLA_AGM_F_6V()
// 11/04/15     Vincent.T      1. Past CCA rating conversion form .c
// 11/05/15     Vincent.T      1. New function to convert any CCA value among different CCA ratings
//                             2. #include "../app\Selratingwidget.h" to access SELRATING
// 01/21/16     William.H      Add cranking volt detect function - BattCoreCrankingVolt() in system test.
// 02/02/16     Vincent.T      1. New enum SYSRANK
//                             2. define for cranking volt level
// 02/03/16     Vincent.T      1. New func. AltIdleVoltTest(); to test Alt. Idle voltage.
// 02/04/16     Vincent.t      1. Add SysRipTest(float *pfvol) to test ripple voltage while doing system test.
//                             2. Add AltLoadVoltTest() func. to test ALT. load voltage.
// 03/09/16     Vincent.T      1. Add BattCoreGetVLoad(); declaraction.
//                             2. Add BattCoreGetIR(); declaraction.
// 03/24/16     Henry.Y        1. Modify BattCoreChkIntBatt(): Add adc and voltage test parameter.
//                             2. Modify BattCoreGetVoltage(): Add adc test parameter.
// 04/15/16     Vincent.T      1. New BattCoreGetVLoadAD(); to get Vload AD(K-IR)
// 06/14/16     Vincent.T      1. New BattCoreChkIntBatFast(); to get internal battery voltage quickly.
// 12/30/16     Vincent.T      1. Modify BattCoreGetVLoad(); -> BattCoreGetVLoad_PLUS();, BattCoreGetVLoad_MINS(); and BattCoreGetVLoad();
//                             2. Modify BattCoreGetVLoadAD(); -> BattCoreGetVLoadAD_PLUS(); and BattCoreGetVLoadAD_MINS();
// 02/17/17     Vincent.T      1. Declaraction of BattCoreGetVoltageCCA();
// 09/28/17 	Jerry.W		   Move BATTERYTYPE define into this file
// 11/06/17     Henry.Y	       1. Set IR/CCA test parameter.
//                             2. Hide useless function: BattCoreGetVoltageCCA(),BattCoreChkCable()
// 03/22/18     William.H      Add CLAMPCONNMETH and CARMODEL enumeration.
// 05/03/18     Jerry.W        add EN1 and EN2.
// 09/11/18     Henry.Y        Add car brand/model/version data struct.
//                             Add EN1 coefficient.
//                             Add service API: resistance_compensation()
// 03/20/19     Henry.Y        Add new warning threshold in BattCoreChkIntBatt().
// 20201204     DW             MODIFY SET CCA RANGE FOR BT2010
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/service/battcoresvc.h#1 $
// $DateTime: 2017/11/27 10:38:22 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#ifndef __BATTCORESVC_H__
#define __BATTCORESVC_H__

#include "mapsvc.h"

typedef enum {
    DIN = 0,	/* 0: DIN    */
    IEC,		/* 1: IEC    */
    SAE,    	/* 2: SAE    */
    MCA,        /* 3: SAE    */
    CA_MCA,		/* 4: CA/MCA */
    EN2,	    /* 5: EN2    */
	JIS,		/* 6: JIS    */
	EN1,		/* 7: EN1    */
	SELRATING_num,
} SELRATING;


typedef enum{
	NO_DETECT = 0,		//No detected
	LOW,				//Low
	NORMAL,				//Normal
	HIGH				//High
}SYSRANK;


typedef enum {
    FLOODED = 0,    /* 0: FLOODED        */
    AGM_FLAT_PLATE,	/* 1: AGM FLAT PLATE */
    AGM_SPIRAL,		/* 2: AGM SPIRAL     */
	VRLA_GEL,	    /* 3: VRLA / GEL     */
	EFB,            /* 4: efb            */
} BATTERYTYPE;



typedef enum {
	DIRECT_CONNECT = 0,
	JUMPER_POST	
}CLAMPCONNMETH;



typedef struct{
	char *version;
	float cable_resistance;
}
tVersion;



typedef struct{
	char *model_name;
	uint8_t version_num;
	const tVersion version_list[10];
}
tModel;



typedef struct{
	char *brand_name;
	uint8_t model_num;
	const tModel model_list[10];
}
tBrand;







#define MAX_SETCCA_VAL_SAE 3000
#define MAX_SETCCA_VAL_EN  2830
#define MAX_SETCCA_VAL_EN2 2830
#define MAX_SETCCA_VAL_DIN 1685
#define MAX_SETCCA_VAL_IEC 1985
#define MAX_SETCCA_VAL_CA  3600      // 20201016 DW ADD CA/MCA

//#define MIN_SETCCA_VAL 25
#define MAX_SETRC_VAL 300
#define MIN_SETRC_VAL 30
#define DEFAULT_SETRC 150

#define MIN_SETCCA_VAL_SAE 40
#define MIN_SETCCA_VAL_EN  40
#define MIN_SETCCA_VAL_EN2 40
#define MIN_SETCCA_VAL_DIN 25
#define MIN_SETCCA_VAL_IEC 30
#define MIN_SETCCA_VAL_CA_MCA 50

#define FLOODED_SURFACE_VOL 12.90
#define AGMF_SURFACE_VOL 13.00
#define AGMS_SURFACE_VOL 13.10
#define VRLA_SURFACE_VOL 13.00
#define EFB_SURFACE_VOL AGMF_SURFACE_VOL

//*****************************************************************************
//
// Define Alternator Idle Volt. level
//
//*****************************************************************************
#define ALT_IDLE_12V_VOL_LOW_ABOVE_0        13.0//13.400
#define ALT_IDLE_12V_VOL_NORMAL_ABOVE_0     15.5//14.800
#define ALT_IDLE_12V_VOL_LOW_BELOW_0        ALT_IDLE_12V_VOL_LOW_ABOVE_0//13.400
#define ALT_IDLE_12V_VOL_NORMAL_BELOW_0     ALT_IDLE_12V_VOL_NORMAL_ABOVE_0//15.500

#define ALT_IDLE_24V_VOL_LOW_ABOVE_0        (ALT_IDLE_12V_VOL_LOW_ABOVE_0*2)
#define ALT_IDLE_24V_VOL_NORMAL_ABOVE_0     (ALT_IDLE_12V_VOL_NORMAL_ABOVE_0*2)
#define ALT_IDLE_24V_VOL_LOW_BELOW_0        (ALT_IDLE_12V_VOL_LOW_BELOW_0*2)
#define ALT_IDLE_24V_VOL_NORMAL_BELOW_0     (ALT_IDLE_12V_VOL_NORMAL_BELOW_0*2)
//*****************************************************************************
//
// Define Ripple Volt. level
//
//*****************************************************************************
#define RIPPLE_12V_ND       0.030
#define RIPPLE_12V_NORMAL   0.34//0.347
#define RIPPLE_24V_ND       (RIPPLE_12V_ND*2)
#define RIPPLE_24V_NORMAL   (RIPPLE_12V_NORMAL*2)

//*****************************************************************************
//
// Define Alternator Load Volt. level
//
//*****************************************************************************
#define ALT_LOAD_12V_VOL_LOW_ABOVE_0        12.8//12.500
#define ALT_LOAD_12V_VOL_NORMAL_ABOVE_0     15.0//14.800
#define ALT_LOAD_12V_VOL_LOW_BELOW_0        ALT_LOAD_12V_VOL_LOW_ABOVE_0//12.500
#define ALT_LOAD_12V_VOL_NORMAL_BELOW_0     ALT_LOAD_12V_VOL_NORMAL_ABOVE_0//15.500

#define ALT_LOAD_24V_VOL_LOW_ABOVE_0        (ALT_LOAD_12V_VOL_LOW_ABOVE_0*2)
#define ALT_LOAD_24V_VOL_NORMAL_ABOVE_0     (ALT_LOAD_12V_VOL_NORMAL_ABOVE_0*2)
#define ALT_LOAD_24V_VOL_LOW_BELOW_0        (ALT_LOAD_12V_VOL_LOW_BELOW_0*2)
#define ALT_LOAD_24V_VOL_NORMAL_BELOW_0     (ALT_LOAD_12V_VOL_NORMAL_BELOW_0*2)

//*****************************************************************************
//
// Define ADC input channel(6V/12V/24V channel) sampling counter for averaging.
//
//*****************************************************************************
#define ADC_sample_counter	200

//	1. BattCoreGetVoltage() { @ start
//*****************************************************************************
//
// Define internal battery's ADC sampling counter for averaging.
//
//*****************************************************************************
#define ADC_BAT_sample	100	// for counting 9V bat. ADC

//*****************************************************************************
//
// Define internal battery's voltage threshold.
// - Low: 7.8, 99 times available printout.
//*****************************************************************************
#define Bat_threshold_low	7.9 //6.5//7.5
#define Bat_threshold_warn	8.0
#define Bat_threshold_Print	5.8
#define printing_warning_vol 11.5

//*****************************************************************************
//
// Define Cranking Volt. level
//
//*****************************************************************************
#define CRANKVOLT_12V 9.6
#define CRANKVOLT_24V 19.2

//	2. BattCoreChkClamp() { @ start
//*****************************************************************************
//
//	CCA load error condition.
//
//*****************************************************************************
#define delta_voltage_CCA	0.05

//*****************************************************************************
//
//	Check clamp threshold
//
//*****************************************************************************
#define check_clamp_v	0.8
//	} @ end

//*****************************************************************************
//
//	Check clamp threshold
//
//*****************************************************************************
#define POWER_SAVE_THRESHOLD		9.6

//*****************************************************************************
//
//	Voltage diode parameter for CCA load on circuit.
//
//*****************************************************************************
#define VDIODE				0.795

//*****************************************************************************
//
//	 Cable length coefficient for 6V/12V battery test.
//
//*****************************************************************************
#define FECTALL_6V			13.74404386317856
//#define FECTALL_12V			27.15647726909253
#define FECTALL_12V 23.6361	
//	} @ end

//*****************************************************************************
//
//	Delay time definition
//
//*****************************************************************************
#define DELAY_ZERO_ZERO_ZERO_ZERO_TWO_FIVE_SECOND	ROM_SysCtlDelay(get_sys_clk()/3/4000)	// Delay 0.00025 second = 0.25ms
#define DELAY_ZERO_ZERO_ZERO_ZERO_FIVE_SECOND		ROM_SysCtlDelay(get_sys_clk()/3/2000)	// Delay 0.0005 second = 0.5ms
#define DELAY_ZERO_ZERO_ZERO_ONE_SECOND				ROM_SysCtlDelay(get_sys_clk()/3/1000)	// Delay 0.001 second = 1ms
#define DELAY_ZERO_ZERO_ZERO_ONE_FIVE_SECOND		ROM_SysCtlDelay(get_sys_clk()/2000)	// Delay 0.0015 second = 1.5ms
#define DELAY_ZERO_ZERO_ZERO_TWO_SECOND				ROM_SysCtlDelay(get_sys_clk()/3/500)	// Delay 0.002 second = 2ms
#define DELAY_ZERO_ZERO_ZERO_FIVE_SECOND			ROM_SysCtlDelay(get_sys_clk()/3/200)	// Delay 0.005 second = 5ms
#define DELAY_ZERO_ZERO_ONE_SECOND					ROM_SysCtlDelay(get_sys_clk()/3/100)	// Delay 0.01 second = 10ms
#define DELAY_ZERO_ZERO_FIVE_SECOND					ROM_SysCtlDelay(get_sys_clk()/3/20)	// Delay 0.05 second = 50ms
#define DELAY_ZERO_ONE_SECOND						ROM_SysCtlDelay(get_sys_clk()/3/10)	// Delay 0.1 second = 100ms
#define DELAY_ZERO_TWO_FIVE_SECOND					ROM_SysCtlDelay(get_sys_clk()/3/2/2)	// Delay 0.25 second = 250ms
#define DELAY_ZERO_FIVE_SECOND						ROM_SysCtlDelay(get_sys_clk()/3/2)		// Delay 0.5 second = 500ms
#define DELAY_ONE_SECOND							ROM_SysCtlDelay(get_sys_clk()/3)		// Delay 1 second
#define DELAY_TWO_SECOND							ROM_SysCtlDelay(get_sys_clk()*2/3)		// Delay 2 second

//*****************************************************************************
//
// Define a boolean type.
//
//*****************************************************************************
typedef unsigned char tBoolean;

//*****************************************************************************
//
//	CCA unit conversion
//
//*****************************************************************************
/******UNIT  TRANS******/
#define SAE_to_CA		1.2
#define SAE_to_EN2		0.943
#define SAE_to_EN1		1.1
#define SAE_to_IEC		0.662
#define SAE_to_DIN		0.561
#define SAE_to_CCA		1
#define VRLA_FLOODED	1
#define AGMF_AGMS_EFB	0.8

//
// BT2100 IR coefficient
// 
#define IR_6V_COMP_C  1    //0.98//4.528889418899493
#define IR_12V_COMP_C 1    //0.95//4.816005482612083

//
// BT2100 6V CCA coefficient
// 
#define CCA_6V_COMP_C 0.486

//
// 3WIRE THREAD THRESHOLD
// this value almost equals to IRbat+Rclamp- 
// K-connection:
// SINGLE LOAD FOR LOW CCA/HIGH IR BATTERY
// 
#define ThreewireTh 10.00


//
// BT2100 -> standard value CCA formula CCA = 3077.5 * IR^-1.001
//
#define EXP_a 3077.5
#define EXP_pow -1.001

//For battery switch control
enum{
    dual_load = 0,
    single_load = 1,
    dual_load_with_result = 10,
    single_load_with_result = 11,
};

extern void BattCoreScanVoltage(float *voltage);
extern int BattCoreChkIntBatt(uint32_t *bat_voltage_acc, float *bat_voltage);
bool BattCoreChkIntBatFast(void);
extern void BattCoreGetVoltage(uint32_t *voltage_acc, float *voltage);
extern int BattCoreChkClamp(void);
extern int BattCoreGetCCA(float *IR, float *CCA,uint8_t Load_Times);
extern int BattCoreJudgeRipple(float *delta_voltage_Ripple);

extern void BattCoreCCAUnitTransform(float *CCA, uint32_t bat_type, uint32_t rating, uint8_t JisDinNum);
extern void BattCoreCalcSOH(float *SOH, float  CCA);
float BattCoreCalSoc(MAP_BATTERYTYPE Battype, float fvol, VOL_CH volch);
void CCAValConvert(SELRATING CCARatingSrc, SELRATING CCARatingDest, uint16_t *pui16CCAVal);
extern int BattCoreCrankingVolt(float *cranking_voltage, float *vmin);
int AltIdleVoltTest(float *fVol);
int SysRipTest(float *pfvol);
int AltLoadVoltTest(float *pfVol);
void BattCoreGetVLoad_PLUS(float *pfVload, uint8_t ui8SampleNum);
void BattCoreGetVLoad_MINS(float *pfVload, uint8_t ui8SampleNum);
void BattCoreGetVLoad(float *pfVload_P, float *pfVload_M, uint8_t ui8SampleNum);
void BattCoreGetVLoadAD_PLUS(float *pfVload, uint8_t ui8SampleNum);
void BattCoreGetVLoadAD_MINS(float *pfVload, uint8_t ui8SampleNum);
bool BattCoreGetIR(float *pfIR, uint8_t ui8SampleNum, uint8_t ui8Switch);
void resistance_compensation(float val);
bool Surface_charge_detect(BATTERYTYPE Battype, VOL_CH ch, float fvol);
bool BattCoreCChkCable(uint8_t ui8SampleNum);
void CCATempComp(float *CCA, float fTemp);
void IRTempComp(float *IR, float fTemp);

#endif

