//*****************************************************************************
//
// Source File: lcdsvc.h
// Description: Prototype for functions to Draw LCD Screen which data is read 
//				from Flash.
//
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 06/30/15     William.H      Created file.
// 10/05/15     William.H      Check in BT2020 project which branched from LCDFontMaker(Pre-BT2020) by modifed related GPIOs.
// 02/02/16     Vincent.T      1. #include "battcoresvc.h"
//                             2. New function LCDDrawCrankingVolt(); to draw cranking vol. waveform on display.
// 02/04/16     Vincent.T      1. New func. LCDDrawRipVolt() to display waveform.
// 12/07/18     Henry.Y        Move LCDDrawCrankingVolt()/LCDDrawRipVolt()/DrawPageInfo()/YNAntiWhiteItem() to app layer.
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/service/lcdsvc.h#5 $
// $DateTime: 2017/09/22 10:27:40 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#ifndef __LCDSVC_H__
#define __LCDSVC_H__

#include <stdbool.h>
#include <stdint.h>
#include "flashsvc.h"
#include "battcoresvc.h"
#include "..\driver/st7565_lcm.h"


extern void LcdDrawScreen( uint8_t ui8ScrnIdx, MOVEINBUFFER MoveInBufIdx);
extern void LcdDrawScreenBuffer( uint8_t ui8ScrnIdx, MOVEINBUFFER MoveInBufIdx);
extern void LcdDrawScreenBuffer_ReverseType( uint8_t ui8ScrnIdx, MOVEINBUFFER MoveInBufIdx);
extern void LcdDrawScreen_ReverseType(uint8_t ui8ScrnIdx, MOVEINBUFFER MoveInBufIdx);



#endif // __LCDSVC_H__
