//*****************************************************************************
//
// Source File: UTF8_WBsvc.c
// Description: UTF8 character images.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 10/12/18     Jerry.W        Created file.
// 11/03/18     Jerry.W        add Greek & Cyrillic.
// 11/27/18     Jerry.W        add UTF8_show function.
// 12/06/18     Jerry.w        add printer function.
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/driver/wifi_driver.h#2 $
// $DateTime: 2017/12/18 16:17:54 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************


#include "UTF8_WBsvc.h"
#include "UTF8_word_bank\font_graph.h"
#include "UTF8_word_bank\Latin_12.h"
#include "UTF8_word_bank\Greek_12.h"
#include "UTF8_word_bank\Cyrillic_12.h"
#include "..\driver\utilities.h"
#include "..\driver\st7565_lcm.h"
#include "UTF8_word_bank/ASSIC_12.h"
#include "printersvc.h"
#include "printersvc.h"
#include "UTF8_word_bank\Latin_18.h"
#include "UTF8_word_bank\Greek_18.h"
#include "UTF8_word_bank\Cyrillic_18.h"
#include "UTF8_word_bank/ASSIC_18.h"

static const font_table_dsc_t* CHAR_SEET_12[] = {&ASSIC_12, &Latin_12, &Greek_12, &Cyrillic_12};
static const font_table_dsc_t* CHAR_SEET_18[] = {&ASSIC_18, &Latin_18, &Greek_18, &Cyrillic_18};

bool UTF8_get_word(uint8_t *word, const font_glyph_dsc_t** word_dsc, const uint8_t **bitmap,uint8_t *height_start, uint8_t *height_end , FONTS_T font);

void UTF8_LCM_put_word(uint32_t x, uint32_t y, const font_glyph_dsc_t *word_dsc, const uint8_t *bitmap, int8_t start, uint8_t end, bool transparent){
	int8_t i, j;
	uint8_t buf[(end - start +8)/8];
	uint8_t mask = 0x80;
	uint8_t cul = ((word_dsc->w_px-1)/8)+1;

	for(i = 0; i < word_dsc->w_px; i++){
		memset(buf, 0, (end - start +8)/8);
		for(j = start; j <= end ; j++){
			if( j >= 0 && (bitmap[j*cul+(i/8)] & mask)){
				set_bit(&buf, j - start, 1);
			}
		}
		ST7565V128x64x1LinePrintV(0, x+i , y, end - start +1, (uint8_t*)&buf, transparent);
		mask >>= 1;
		if(mask == 0) mask = 0x80;
	}
}


void UTF8_printf(uint32_t x, uint32_t y, char* string){
	const font_glyph_dsc_t *word_dsc;
	const uint8_t *bitmap;
	uint16_t width = 0;

	while(*string){
		if(UTF8_get_word((uint8_t*)string, &word_dsc, &bitmap, 0, 0, font_12)){
			UTF8_LCM_put_word(x,y,word_dsc,bitmap, 0, 11, false);
			x += (word_dsc->w_px +1);
			width += (word_dsc->w_px +1);
			string += strlen(word_dsc->word);
		}
		else{
			UARTprintf("unknown character!\n");
			string++;
		}
	}
}

bool UTF8_get_word(uint8_t *word, const font_glyph_dsc_t** word_dsc, const uint8_t **bitmap,uint8_t *pixel_H, uint8_t *pixel_L , FONTS_T font){
	uint8_t i;
	uint8_t k,j,P_H = 5,P_L = 5; 	//5 -> in case of space
	uint8_t width, height;
	char word_buf[5];
	memset(word_buf, 0, 5);
	uint16_t up = 0;
	uint16_t low = 0;
	int32_t cmp;
	uint8_t font_num;
	const font_table_dsc_t *CHAR_temp;
	if(*word < 127){//1byte character
		memcpy(word_buf, word, 1);
	}else if(*word < 0xE0){//2byte character
		memcpy(word_buf, word, 2);
	}
	else if(*word < 0xF0){//3byte character
		memcpy(word_buf, word, 3);
	}
	else{//4byte character
		memcpy(word_buf, word, 4);
	}

	if(font == font_12){
		font_num = sizeof(CHAR_SEET_12)/4;
	}
	else{
		font_num = sizeof(CHAR_SEET_18)/4;
	}

	for(i = 0 ; i < font_num ; i++){
		if(font == font_12){
			CHAR_temp = CHAR_SEET_12[i];
		}
		else{
			CHAR_temp = CHAR_SEET_18[i];
		}

		height = CHAR_temp->height;
		if(strcmp(word_buf,CHAR_temp->dsc[0].word)>=0
				&& strcmp(word_buf,CHAR_temp->dsc[CHAR_temp->numb_word-1].word)<=0){
			up = CHAR_temp->numb_word;
			while(low != up){
				cmp = strcmp(word_buf, CHAR_temp->dsc[(up+low)/2].word);
				if(cmp == 0){ //matched!!
					if(word_dsc) *word_dsc = &CHAR_temp->dsc[(up+low)/2];
					if(bitmap) *bitmap = &CHAR_temp->bitmap[(*word_dsc)->glyph_index];
					width = (*word_dsc)->w_px;
					for(k = 0; k < height ; k++){
						for(j = 0; j*8 < width; j++){
							if(CHAR_temp->bitmap[((*word_dsc)->glyph_index) + j + k*((width-1)/8 + 1)] != 0){
								P_H = k;
								j = k = 0xfe;	//to leave the for loop
							}
						}
					}
					for(k = height-1; k != 0 ; k--){
						for(j = 0; j*8 < width; j++){
							if(CHAR_temp->bitmap[((*word_dsc)->glyph_index) + j + k*((width-1)/8 +1)] != 0){
								P_L = k;
								j = 0xfe;	//to leave the for loop
								k = 1;		//to leave the for loop
							}
						}
					}
					if(pixel_H) *pixel_H = P_H;
					if(pixel_L) *pixel_L = P_L;
					return true;
				}
				else if(cmp > 0){
					low = (up+low)/2;
				}
				else{
					up = (up+low)/2;
				}
			}


		}
	}

	return false;
}

uint32_t UTF8_str_size(const char *string){
	const char *temp = string;
	while(*temp){
		if(*temp < 127){//1byte character
			temp++;
		}else if(*temp < 0xE0){//2byte character
			temp += 2;
		}
		else if(*temp < 0xF0){//3byte character
			temp += 3;
		}
		else{//4byte character
			temp += 4;
		}
	}
	return (temp - string);
}


uint16_t get_line_UTF8(char *buf, char *string, uint16_t limit_width, uint16_t *width, uint8_t *pixel_H, uint8_t *pixel_L, FONTS_T font, uint8_t distance){
	char* start_line = string, *end_line, *last_end_line;
	uint16_t total_width = 0, last_total_width;
	uint8_t P_H = 12, P_L = 0, P_H_last, P_L_last, P_H_temp, P_L_temp;
	const font_glyph_dsc_t *word_dsc;

	//while(*start_line == ' ') start_line++;
	if(*start_line == '\n'){
		*buf = '\0';
		return start_line-string+1;
	}
	if(*start_line == '\0'){
		*buf = '\0';
		return start_line-string;
	}

	end_line = start_line;
	do{
		last_end_line = end_line;
		last_total_width = total_width;
		P_H_last = P_H;
		P_L_last = P_L;

		if(*end_line == '\0' || *end_line == '\n'){
			break;
		}
		else if(*end_line == ' '){
			UTF8_get_word((uint8_t*)end_line, &word_dsc, 0, 0, 0, font);
			end_line += strlen(word_dsc->word);
			total_width += (word_dsc->w_px +distance);
		}
		while(*end_line != ' ' && *end_line != '\n' && *end_line != '\0'){
			if(UTF8_get_word((uint8_t*)end_line, &word_dsc, 0, &P_H_temp, &P_L_temp, font)){
				if(P_H > P_H_temp) P_H = P_H_temp;
				if(P_L < P_L_temp) P_L = P_L_temp;
				end_line += strlen(word_dsc->word);
				total_width += (word_dsc->w_px +distance);
			}
			else{
				UARTprintf("unknown character!\n");
				end_line++;
			}

		}
	}while(total_width <= limit_width);

	if(width) *width = last_total_width;

	if(pixel_L) *pixel_L = P_L_last;
	if(pixel_H) *pixel_H = P_H_last;

	memcpy(buf, start_line, last_end_line - start_line);
	buf[last_end_line - start_line] = '\0';
	return last_end_line - string+1;
}


uint8_t UTF8_LCM_show(uint32_t x, uint32_t y, char* string, uint16_t limit_width, SHOW_profile_t *profile){
	uint32_t str_len, len_temp = 0;
	uint16_t width_temp, height;
	uint8_t pixel_H, pixel_L;
	const font_glyph_dsc_t *word_dsc;
	const uint8_t *bitmap;
	uint32_t x_temp, y_temp;
	uint16_t x_max = 0, x_min = 0xffff;
	char buf[100];
	char* buf_temp;
	bool first_row = true;
	str_len = UTF8_str_size(string);

	y_temp = y;

	while(len_temp < str_len){
		len_temp += get_line_UTF8(buf, string+len_temp, limit_width, &width_temp, &pixel_H, &pixel_L, font_12, 1);

		switch(profile->side){
		case side_left:
			x_temp = x;
			break;
		case side_right:
			x_temp = x + (limit_width - width_temp);
			break;
		case side_middle:
			x_temp = x + (limit_width - width_temp)/2;
			break;
		}

		if(x_min > x_temp) x_min = x_temp;

		buf_temp = buf;
		while(*buf_temp){
			if(UTF8_get_word((uint8_t*)buf_temp, &word_dsc, &bitmap, 0, 0, font_12)){
				UTF8_LCM_put_word(x_temp, y_temp, word_dsc, bitmap,(first_row)? pixel_H : (pixel_H - profile->row_gap), pixel_L, profile->transparent);
				height = (first_row)? (pixel_L - pixel_H + 1): (pixel_L - pixel_H + 1 + profile->row_gap);
				x_temp += (word_dsc->w_px+1);
				buf_temp += strlen(word_dsc->word);
				if(profile->transparent == false && *buf_temp){
					uint8_t zero[(height +8)/8];
					memset(zero, 0 , (height +8)/8);
					ST7565V128x64x1LinePrintV(0, x_temp -1, y_temp, height, zero, false);
				}
			}
			else{
				UARTprintf("unknown character!\n");
				buf_temp++;
			}
		}
		x_temp--;
		y_temp += height;
		if(x_max < x_temp) x_max = x_temp;
		first_row = false;
	}
	profile->region.y1 = y;
	profile->region.y2 = y_temp -1;
	profile->region.x1 = x_min;
	profile->region.x2 = x_max;

	return y_temp - y;
}


void UTF8_print_row(char* string, uint8_t row, uint16_t x_start, SHOW_profile_t *profile){
	uint8_t buf[MP205_PRINTOUT_SIZE];
	const font_glyph_dsc_t *word_dsc;
	const uint8_t *bitmap;
	uint8_t *temp;
	uint8_t i,j;
	uint8_t cul;
	uint8_t repeat = profile->Print_spec.pixel_H;
	uint8_t print_pixel = profile->Print_spec.pixel_W;

	memset(buf, 0 , MP205_PRINTOUT_SIZE);

	while(*string && x_start < 384){
		if(UTF8_get_word((uint8_t*)string, &word_dsc, &bitmap, 0, 0, font_18)){
			cul = ((word_dsc->w_px-1)/8)+1;
			temp = (uint8_t*)bitmap + (cul*row);
			for(i = 0; i < word_dsc->w_px ; i++){
				if(get_bit_reverse(temp, i)){
					UARTprintf("@");
					for(j = 0 ; j < print_pixel; j++){
						uint8_t A = (x_start+j)%8;
						uint8_t B = 7 - A;
						uint16_t C = x_start+j - A + B;
						set_bit(buf, C, 1);
					}
				}
				else{
					UARTprintf("_");
				}
				x_start += print_pixel;
			}
			string += strlen(word_dsc->word);
		}
		else{
			UARTprintf("unknown character!\n");
			string++;
		}
		x_start += print_pixel;	//distance between characters.
	}
	UARTprintf("|\n");

	PrinterSet(buf, STB_1_2_3_4_5_6, 1.5, 1, repeat);
}


void UTF8_Printer_show(char* string, SHOW_profile_t *profile){
	uint32_t str_len, len_temp = 0;
	uint16_t width_temp;
	uint8_t pixel_H, pixel_L, i;
	uint32_t x_temp;
	uint16_t x_max = 0, x_min = 0xffff;
	char buf[100];
	uint8_t print_pixel = profile->Print_spec.pixel_W;

	bool first_row = true;
	str_len = UTF8_str_size(string);

	PrinterPrintSpace(1);

	while(len_temp < str_len){
		len_temp += get_line_UTF8(buf, string+len_temp, MP205_PRINTOUT_SIZE*8/print_pixel, &width_temp, &pixel_H, &pixel_L, font_18, print_pixel);

		width_temp *= print_pixel;

		switch(profile->side){
		case side_left:
			x_temp = 0;
			break;
		case side_right:
			x_temp = (MP205_PRINTOUT_SIZE*8 - width_temp);
			break;
		case side_middle:
			x_temp = (MP205_PRINTOUT_SIZE*8 - width_temp)/2;
			break;
		}

		if(x_min > x_temp) x_min = x_temp;

		if(first_row == false){
			PrinterPrintSpace(profile->row_gap);
		}
		for(i = pixel_H ; i <= pixel_L; i++){
			UTF8_print_row(buf, i , x_temp, profile);
		}

		x_temp += width_temp;
		if(x_max < x_temp) x_max = x_temp;

		first_row = false;
	}
	profile->region.x1 = x_min;
	profile->region.x2 = x_max;
	profile->region.y1 = 0;
	profile->region.y2 = 0;

}


