//*****************************************************************************
//
// Source File: cloud_tranform.c
// Description: transform informations to sting type
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 01/23/18     Jerry.W        Created file.
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/driver/wifi_driver.h#2 $
// $DateTime: 2017/12/18 16:17:54 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************


#include "cloud_tranform.h"




typedef struct {
	SELRATING bat_rate;
	char bat_string[10];
}BAT_R;

typedef struct {
	BATTERYTYPE bat_type;
	char bat_string[10];
}BAT_T;

const BAT_R Batter_Rating[] = 	{	{SAE,"sae"},
									{EN1,"en"},
									{EN2,"en2"},
									{IEC,"iec"},
									{JIS,"jis"},
									{DIN,"din"},
									{CA_MCA,"ca/mca"},
									{MCA,"mca"},};

const BAT_T Batter_Type[5] = 	{	{FLOODED,"flooded"},
									{AGM_FLAT_PLATE,"agm_f"},
									{AGM_SPIRAL,"agm_s"},
									{VRLA_GEL,"vrla"},
									{EFB,"efb"},};




SELRATING string_to_bat_rating(const char* bat_r){
	uint8_t i, loop =  sizeof(Batter_Rating) / sizeof(Batter_Rating[1]);
	uint8_t temp = 0xff;

	for(i = 0; i<loop; i++){
		if(strcmp(Batter_Rating[i].bat_string, bat_r) == 0) break;
	}

	if(i < loop) temp = Batter_Rating[i].bat_rate;
	else{
		UARTprintf("Cloud transform: cloud rating error, @%d\n", __LINE__);
	}
	return (SELRATING)temp;
}




BATTERYTYPE string_to_bat_type(const char* bat_t){
	uint8_t i, loop =  sizeof(Batter_Type) / sizeof(Batter_Type[1]);
	uint8_t temp = 0xff;

	for(i = 0; i<loop; i++){
		if(strcmp(Batter_Type[i].bat_string, bat_t) == 0) break;
	}

	if(i < loop) temp = Batter_Type[i].bat_type;
	else{
		UARTprintf("Cloud transform: cloud Bat. type error, @%d\n", __LINE__);
	}
	return (BATTERYTYPE)temp;
}



bool TIME_TO_STRING(TIME_T* time, char* time_string){

	if((time->year >= 3000)||(time->year < 2000))	goto FALSE;
	else if(time->month >12) goto FALSE;
	else if(time->day >31) goto FALSE;
	else if(time->hour >23) goto FALSE;
	else if(time->minute >60) goto FALSE;
	else if(time->second >60) goto FALSE;

	time_string[0] = ((time->year/1000)%10 + '0');
	time_string[1] = ((time->year/100)%10 + '0');
	time_string[2] = ((time->year/10)%10 + '0');
	time_string[3] = ((time->year)%10 + '0');

	time_string[4] = '.';

	time_string[5] = ((time->month/10)%10 + '0');
	time_string[6] = ((time->month)%10 + '0');

	time_string[7] = '.';

	time_string[8] = ((time->day/10)%10 + '0');
	time_string[9] = ((time->day)%10 + '0');

	time_string[10] = '.';

	time_string[11] = ((time->hour/10)%10 + '0');
	time_string[12] = ((time->hour)%10 + '0');

	time_string[13] = '.';

	time_string[14] = ((time->minute/10)%10 + '0');
	time_string[15] = ((time->minute)%10 + '0');

	time_string[16] = '.';

	time_string[17] = ((time->second/10)%10 + '0');
	time_string[18] = ((time->second)%10 + '0');

	time_string[19] = '\0';

	return true;

FALSE:
	UARTprintf("Cloud transform: time error, @%d\n", __LINE__);
	return false;
}

bool test_type_to_string(TEST_TYPE type, char* sting){
	char* temp;
	switch(type){
	case 	Start_Stop_Test :
		temp = "start-stop test";
	break;
	case 	Battery_Test	:
		temp = "battery test";
	break;
	case	System_Test		:
		temp = "system test";
	break;

	default :
		UARTprintf("Cloud transform: test type error, @%d\n", __LINE__);
		return false;
	}

	strcpy(sting, temp);
	return true;
}

bool Bat_type_to_sting(BATTERYTYPE type, char* sting){
	uint8_t i;
	for(i=0; i< sizeof(Batter_Type)/sizeof(Batter_Type[0]);i++){
		if(type == Batter_Type[i].bat_type){
			strcpy(sting, Batter_Type[i].bat_string);
			return true;
		}
	}
	UARTprintf("Cloud transform: Bar. type error, @%d\n", __LINE__);
	return false;
}


bool Rating_to_sting(SELRATING rating, char* sting){
	uint8_t i;
	for(i=0; i< sizeof(Batter_Rating)/sizeof(Batter_Rating[0]);i++){
		if(rating == Batter_Rating[i].bat_rate){
			strcpy(sting, Batter_Rating[i].bat_string);
			return true;
		}
	}
	UARTprintf("Cloud transform: rating error, @%d\n", __LINE__);
	return false;
}

bool Judgement_to_sting(TEST_RESULT judge, char* sting){
	char* temp;
	switch(judge){
	case 	TR_GOODnPASS:
		temp = "good&pass";
	break;
	case 	TR_GOODnRRCHARGE:
		temp = "good&recharge";
	break;
	case 	TR_BADnREPLACE:
		temp = "bad&replace";
	break;
	case 	TR_CAUTION:
		temp = "caution";
	break;
	case 	TR_RECHARGEnRETEST:
		temp = "recharge&retest";
	break;
	case 	TR_BAD_CELL_REPLACE:
		temp = "badcellreplace";
	break;

	default:
		UARTprintf("Cloud transform: judge error, @%d\n", __LINE__);
		return false;
	}
	strcpy(sting, temp);
	return true;
}

bool RC_judge_to_string(RC_RESULT judge, char* sting){
	char* temp;
	switch(judge){
	case 	RC_GOOD:
		temp = "good";
	break;
	case 	RC_LOW:
		temp = "low";
	break;
	case 	RC_POOR:
		temp = "poor";
	break;
	default:
		UARTprintf("Cloud transform: RC judge error, @%d\n", __LINE__);
		return false;
	}

	strcpy(sting, temp);
	return true;
}


bool SYSRANK_to_string(SYSRANK rank, char* sting){
	char* temp;
	switch(rank){
	case NO_DETECT:
		temp = "no detect";
	break;
	case LOW:
		temp = "low";
	break;
	case NORMAL:
		temp = "normal";
	break;
	case HIGH:
		temp = "high";
	break;

	default :
		UARTprintf("Cloud transform: SYSRANK error, @%d\n", __LINE__);
		return false;
	}
	strcpy(sting, temp);
	return true;
}



