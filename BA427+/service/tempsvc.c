//*****************************************************************************
//
// Source File: tempsvc.c
// Description: Functions to access temperature sensor(tp-337a) data.
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 12/01/15     Vincent.T      Created file.
// 01/22/16     Vincent.T      fix Temp. Compensate coefficent
// 03/11/16     Vincent.T      1. Redefine CCATempComp(); to get testing temperature value.
// 06/21/16     Vincent.T      1. Cancel Temperature here.(PointToBatClick(); instead)
// 07/05/16     Vincent.T      1. Modify sample number of GetBodyTemp(); and GetObjTemp();
// 08/10/16     Vincent.T      1. New func. IRTempComp(); to implement temperature compensation of IR.
// 09/28/18     Jerry.W        modify temperature sensor function for new OPA and HW config.
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/service/tempsvc.c#5 $
// $DateTime: 2017/09/22 10:27:40 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#include "tempsvc.h"
#include <math.h>
#include "SystemInfosvs.h"

void GetTemp(float *pfBodyTemp, float *pfObjTemp)
{
	//Get Value
	//GetBodyTemp(pfBodyTemp);
	GetObjTemp(pfObjTemp,pfBodyTemp);
}

void GetBodyTemp(float *pfTemp)
{
	uint16_t ui16BodyTempAD = 0;
	SYS_INFO* info = Get_system_info();

	ui16BodyTempAD = TP337ABodyTempAD(200);

	double temp_f = (double)ui16BodyTempAD;

	double temp_e[6];

	temp_e[5] = (-0.000000000000000834773192126935 ) * temp_f*temp_f*temp_f*temp_f*temp_f;
	temp_e[4] = (0.000000000009151914882708730000 * temp_f*temp_f*temp_f*temp_f);
	temp_e[3] = (-0.000000040920868960134400000000 * temp_f*temp_f*temp_f);
	temp_e[2] = (0.0000937593246839643 * temp_f*temp_f);
	temp_e[1] = (-0.133239463192304 * temp_f);
	temp_e[0] = 125.273259093355;

	*pfTemp = temp_e[5]+temp_e[4]+temp_e[3]+temp_e[2]+temp_e[1]+temp_e[0];
			//-5E-15x^5 + 4E-11x^4 - 1E-07x^3 + 0.0002x^2 - 0.2098x + 105.5

	*pfTemp = *pfTemp * info->BodyTempCoe;
}

float OBJ_table[9][13] = {
		//-20	-10		0		10		20		30		40		50		60		70		80		90		100		C for OBJ
		{0, 	0.154, 	0.323, 	0.509, 	0.712, 	0.932, 	1.172, 	1.432, 	1.712, 	3.014, 	2.340, 	2.689, 	3.063},	//-20
		{-0.154,	0, 	0.169, 	0.355, 	0.558, 	0.778, 	1.018, 	1.278, 	1.558, 	1.860, 	2.186, 	2.535, 	2.909},	//-10
		{-0.323,-0.169, 	0, 	0.186, 	0.388, 	0.609, 	0.849, 	1.108, 	1.389/**/, 	1.691, 	2.016, 	2.365, 	2.739},	//0
		{-0.509,-0.355, -0.186, 	0, 	0.203, 	0.423, 	0.663, 	0.923, 	1.203, 	1.506, 	1.831, 	2.180, 	2.554},	//10
		{-0.712,-0.558, -0.388, -0.203, 	0, 	0.221, 	0.460, 	0.720, 	1.001, 	1.303, 	1.628, 	1.977, 	2.351},	//20
		//{-0.820,-0.666, -0.496, -0.311, -0.108, 0.113, 	0.352, 	0.612, 	0.893, 	1.195, 	1.520, 	1.869, 	2.243},	//25
		{-0.932,-0.778, -0.609, -0.423, -0.221, 	0, 	0.240, 	0.499, 	0.780, 	1.082, 	1.407, 	1.756, 	2.130},	//30
		{-1.172,-1.018, -0.849, -0.663, -0.460, -0.240,		0, 	0.260, 	0.540, 	0.842, 	1.168, 	1.517, 	1.891},	//40
		{-1.432,/**/-1.278, -1.108, -0.923, -0.720, -0.499,	-0.260, 	0, 	0.281, 	0.583, 	0.908, 	1.257, 	1.631},	//50
		{-1.712,-1.558, -1.389, -1.203, -1.001, -0.780, -0.540, -0.281,		0, 	0.302, 	0.628, 	0.977, 	1.351},	//60
};




void GetObjTemp(float *pfTemp, float *pfbody)
{
	uint16_t ui16ObjTempAD = 0;
	SYS_INFO* info = Get_system_info();
	uint8_t i, j;

	float Obj_V;
	float BodyTemp;
	float *uppper,*lower;
	uint8_t table_index;

	float x1, x2;

	static bool init = false;

	if(!init){
		//initial the OBJ_table by OPA configuration
		init = true;
		for(i = 0; i < 9 ; i++){
			for(j = 0 ; j < 13 ; j++){
				OBJ_table[i][j] = (OBJ_table[i][j]*2.003/3)+1.25;
			}
		}
	}


	//Object Temp. Decision
	GetBodyTemp(&BodyTemp);
	*pfbody = BodyTemp;

	ui16ObjTempAD = TP337AObjTempAD(200);
	Obj_V = ui16ObjTempAD;
	Obj_V = Obj_V*2.5/4096;

	Obj_V += info->ObjTempCoe;

	if(BodyTemp <= -20){
		uppper = OBJ_table[0];
		lower = OBJ_table[0];
	}
	else if(BodyTemp >= 60){
		uppper = OBJ_table[8];
		lower = OBJ_table[8];
	}
	else{
		table_index = (BodyTemp+20)/10;
		uppper = OBJ_table[table_index+1];
		lower = OBJ_table[table_index];
	}

	for(i = 0; i < 12 ; i++){
		x1 = uppper[i]*(BodyTemp-(table_index*10)+20)/10 + lower[i]*(10-(BodyTemp-(table_index*10)+20))/10;
		x2 = uppper[i+1]*(BodyTemp-(table_index*10)+20)/10 + lower[i+1]*(10-(BodyTemp-(table_index*10)+20))/10;

		if(i == 0 && Obj_V < x1 && Obj_V < x2){
			*pfTemp = -20;
			return;
	}

		if(i == 11 && Obj_V > x1 && Obj_V > x2){
			*pfTemp = 100;
			return;
		}

		if(Obj_V <= x2 && Obj_V >= x1){
			*pfTemp = ( ((float)i*10 - 20.0)*(x2 - Obj_V)	+ ((float)i*10 - 10.0)*(Obj_V - x1) )/(x2 - x1);
			break;
		}
	}

}

float get_obj_vol(float body, float object){
	float *uppper,*lower;
	float obj_V[13], result;
	uint8_t table_index, temp_index, i;

	if(body <= -20){
		uppper = OBJ_table[0];
		lower = OBJ_table[0];
	}
	else if(body >= 60){
		uppper = OBJ_table[8];
		lower = OBJ_table[8];
	}
	else{
		table_index = (body+20)/10;
		uppper = OBJ_table[table_index+1];
		lower = OBJ_table[table_index];
	}

	for(i = 0 ; i < 13; i++){
		obj_V[i] = uppper[i]*(body-(table_index*10)+20)/10 + lower[i]*(10-(body-(table_index*10)+20))/10;
	}

	temp_index = (object+20) / 10;
	result = obj_V[temp_index+1] * (object+20 - (temp_index*10)) / 10 + obj_V[temp_index] * ((temp_index*10)-object-10) / 10;

	return result;
}


