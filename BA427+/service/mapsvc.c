//*****************************************************************************
//
// Source File: mapsvc.c
// Description: Prototype for functions to know battery good or bad.
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 10/23/15     Vincent.T      Created file.
// 10/28/15     Vincent.T      1. Modify some tested result.
//                             2. Add 6v map.
// 11/04/15     Vincent.T      1. New input parameter of BatJudgeMap()
//                             2. Add BattCoreCalcSOH() into BatJudgeMap()
//                             3. #include "battcoresvc.h"
// 11/05/15     Vincent.T      1. Modified one soh judgment value in AGMFnS MAP
// 04/27/16     Vincent.T      1. Modify BatJudgeMap_FOOLDED();, BatJudgeMap_AGMFnS();, BatJudgeMap_VRLAnGEL(); and BatJudgeMap_EFB(); due to adjust caution Y-range.
// 09/11/18     Henry.Y        Applied BT2100_WW edit history: 2018/08/01~03 D.W.'s modification.
//                             Modify BatJudgeMap_FOOLDED judgment lower voltage: good&pass and caution.
// 10/24/18     Henry.Y        (__BO__)Modify judgment map "TR_CAUTION" to "TR_GOODnPASS".
// 03/29/19     Henry.Y        Add the identifier(__BO__) to make the map different with the DHC standard.
// 08/07/20     David.Wang     Change to BT2200_HD_MAX MAP
// 09/08/20     David.Wang     BT2200_HD_BO must has caution , So mask the program
// 20201205     David.Wang     Modify judge map to same as BT2010
// 12/26/20     Henry.Y        Add the identifier(__BA427__): BT(JM025V01) SS(JM032V01)
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/service/mapsvc.c#5 $
// $DateTime: 2017/09/22 10:27:40 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#include "mapsvc.h"
#include "battcoresvc.h"
#include "../driver/uartstdio.h"
#include "SystemInfosvs.h"
#define __BA427__ 1

TEST_RESULT BatJudgeMap(MAP_BATTERYTYPE Battype, float fvol, float *pfcca, float *pfsoh, VOL_CH volch)
{
	uint8_t ui8TestResult = 0;
	TEST_DATA* TD = Get_test_data();
	SYS_PAR* par = Get_system_parameter();

	UARTprintf("BatJudgeMap(). batType = %d.\n", Battype);
	UARTprintf("BatJudgeMap(). voltage channel = %d.\n", volch);

	switch(volch)
	{
	case MAP_6V:
		fvol = fvol*2;
		break;
	case MAP_12V:
		fvol = fvol;
		break;
	default:
		UARTprintf("BatJudgeMap() ---> Error!\n");
		break;
	}

	//**********************************************
	//    Get Coefficicent Depend on Battery type
	//**********************************************
	if(par->WHICH_TEST == Start_Stop_Test){
	    (*pfcca) = (*pfcca) * 0.8;             // JM032V01 BT521 START STOP (AGM & EFB) content
	}
	else{
	    switch(Battype){
	    case MAP_FLOODED:
	        (*pfcca) = (*pfcca) / MAP_FLOODED_CCA;
		    break;
	    case MAP_AGM_FLAT_PLATE:
		    (*pfcca) = (*pfcca) / MAP_AGMFnS_CCA;
		    break;
	    case MAP_AGM_SPIRAL:
		    (*pfcca) = (*pfcca) / MAP_AGMFnS_CCA;
		    break;
	    case MAP_VRLA_GEL:
		    (*pfcca) = (*pfcca) / MAP_VRLAnGEL_CCA;
		    break;
	    case MAP_EFB:
		    (*pfcca) = (*pfcca) / MAP_EFB_CCA;
		    break;
	    default:
		    UARTprintf("Error!!!!! BatJudgeMap(). batType = %d.\n", Battype);
		    while(1);//debug
	    }
	}

	//*************************************
	//         SOH calculation
	//*************************************
	BattCoreCalcSOH(pfsoh, (*pfcca));

	//*************************************
	//         MAP Judgment
	//*************************************
	switch(Battype){
	    case MAP_FLOODED:
	        ui8TestResult = BatJudgeMap_FOOLDED(fvol, pfsoh);
		    break;
	    case MAP_AGM_FLAT_PLATE:
		    ui8TestResult = BatJudgeMap_AGMFnS(fvol, pfsoh);
		    break;
	    case MAP_AGM_SPIRAL:
		    ui8TestResult = BatJudgeMap_AGMFnS(fvol, pfsoh);
		    break;
	    case MAP_VRLA_GEL:
		    ui8TestResult = BatJudgeMap_AGMFnS(fvol, pfsoh);
		    break;
	    case MAP_EFB:
		    ui8TestResult = BatJudgeMap_FOOLDED(fvol, pfsoh);
		    break;
	    default:
		    UARTprintf("Error!!!!! BatJudgeMap(). batType = %d.\n", Battype);
		    while(1);//debug
	}

	//**********************************************
	//      BAD CELL REPLACE HANDLER(LEFT & UPPER)
	//**********************************************
	if((*pfsoh) == 0)
	{
		(*pfcca) = (*pfsoh);
	}	
	return (TEST_RESULT) ui8TestResult;
}

//=======================================================//
//======== 20200807 DW EDIT==============================//
//=======================================================//
TEST_RESULT BatJudgeMap_FOOLDED(float fvol, float *pfsoh)
{
	MapCoordinates p1 = {12.4, 80};
	MapCoordinates p2 = {11.8, 20};
	MapCoordinates p3 = {11.2, 20};

	float fsohtemph = 0;
	//float fsohtempl = 0; no used mask

	// map to check map
	if(fvol >= p1.x){
	    if((*pfsoh) >= p1.y){
	        return TR_GOODnPASS;
	    }
		else{
			return TR_BADnREPLACE;
		}
	}
	else if(fvol >= p2.x){
	    fsohtemph = ((p1.y - p2.y)/(p1.x*100 - p2.x*100))*(fvol*100 - p2.x*100) + p2.y;

	    if((*pfsoh) >= fsohtemph){
	        return TR_GOODnRRCHARGE;
		}
		else{
		    return TR_ASK_CUSTOM;
		}
	}
	else if(fvol >= p3.x){
        if((*pfsoh) >= p3.y){
            return TR_GOODnRRCHARGE;
        }
        else{
            return TR_ASK_CUSTOM;
        }
    }
	else{
	    if((*pfsoh) >= p3.y){
			//force tested cca = 0
			(*pfsoh) = 0;
	            return TR_BAD_CELL_REPLACE;
	    }
		else{
	            return TR_ASK_CUSTOM;
		}
	}
} // FLLODED MAP END

//=======================================================//
//======== 20200807 DW EDIT==============================//
//=======================================================//
TEST_RESULT BatJudgeMap_AGMFnS(float fvol, float *pfsoh)
{
#if (__BA427__)
    MapCoordinates p1 = {12.6, 80};
#else
    MapCoordinates p1 = {12.6, 85};
#endif

    MapCoordinates p2 = {11.8, 20};
    MapCoordinates p3 = {11.2, 20};

    float fsohtemph = 0;
    //float fsohtempl = 0; no used mask

    // map to check map
    if(fvol >= p1.x){
        if((*pfsoh) >= p1.y){
            return TR_GOODnPASS;
        }
        else{
            return TR_BADnREPLACE;
        }
    }
    else if(fvol >= p2.x){
        fsohtemph = ((p1.y - p2.y)/(p1.x*100 - p2.x*100))*(fvol*100 - p2.x*100) + p2.y;

        if((*pfsoh) >= fsohtemph){
            return TR_GOODnRRCHARGE;
        }
        else{
            return TR_ASK_CUSTOM;
        }
    }
    else if(fvol >= p3.x){
        if((*pfsoh) >= p3.y){
            return TR_GOODnRRCHARGE;
        }
        else{
            return TR_ASK_CUSTOM;
        }
    }
    else{
        if((*pfsoh) >= p3.y){
            //force tested cca = 0
            (*pfsoh) = 0;
                return TR_BAD_CELL_REPLACE;
        }
        else{
                return TR_ASK_CUSTOM;
        }
    }
}

//=======================================================//
//======== 20200807 DW EDIT==============================//
//=======================================================//
/*
TEST_RESULT BatJudgeMap_AGMSpiral(float fvol, float *pfsoh)
{
    MapCoordinates p1 = {12.69, 85};
    MapCoordinates p2 = {12.69, 80};
    MapCoordinates p3 = {11.2, 20};

    float fsohtemph = 0;
    //float fsohtempl = 0;

    //start judge form right to left(voltage form high to low)
    if(fvol >= p1.x){
        //start form high to low(soh from high to low)
        if((*pfsoh) >= p1.y){
            return TR_GOODnPASS;
        }
        else if((*pfsoh) >= p2.y){
            return TR_CAUTION;
        }
        else{
            return TR_BADnREPLACE;
        }
    }
    else if(fvol >= p3.x){

        fsohtemph = ((p1.y - p3.y)/(p1.x*100 - p3.x*100))*(fvol*100 - p3.x*100) + p3.y;

        if((*pfsoh) >= fsohtemph){
            return TR_GOODnRRCHARGE;
        }
        else{
            return TR_ASK_CUSTOM;
        }
    }
    else{
        if((*pfsoh) >= p3.y){
            //force tested cca = 0
            (*pfsoh) = 0;
                return TR_BAD_CELL_REPLACE;
        }
        else{
                return TR_ASK_CUSTOM;
        }
    }
}
//=======================================================//
//======== 20180801 DW EDIT==============================//
//=======================================================//
TEST_RESULT BatJudgeMap_VRLAnGEL(float fvol, float *pfsoh)
{
    MapCoordinates p1 = {12.65, 85};
    MapCoordinates p2 = {12.65, 80};
    MapCoordinates p3 = {11.2, 20};

    float fsohtemph = 0;
    //float fsohtempl = 0;

    //start judge form right to left(voltage form high to low)
    if(fvol >= p1.x){
        //start form high to low(soh from high to low)
        if((*pfsoh) >= p1.y){
            return TR_GOODnPASS;
        }
        else if((*pfsoh) >= p2.y){
            return TR_CAUTION;
        }
        else{
            return TR_BADnREPLACE;
        }
    }
    else if(fvol >= p3.x){

        fsohtemph = ((p1.y - p3.y)/(p1.x*100 - p3.x*100))*(fvol*100 - p3.x*100) + p3.y;

        if((*pfsoh) >= fsohtemph){
            return TR_GOODnRRCHARGE;
        }
        else{
            return TR_ASK_CUSTOM;
        }
    }
    else{
        if((*pfsoh) >= p3.y){
            //force tested cca = 0
            (*pfsoh) = 0;
                return TR_BAD_CELL_REPLACE;
        }
        else{
                return TR_ASK_CUSTOM;
        }
    }
}

//=======================================================//
//======== 20180801 DW EDIT==============================//
//=======================================================//
TEST_RESULT BatJudgeMap_EFB(float fvol, float *pfsoh)
{
	//normal

    MapCoordinates p1 = {12.7, 80};
	MapCoordinates p2 = {12.5, 85};
	MapCoordinates p2_1 = {12.5, 80};
	MapCoordinates p3 = {12.4, 40};
	MapCoordinates p4 = {12.0, 0};
	MapCoordinates p5 = {11.8, 25};
	MapCoordinates p6 = {11.0, 40};
	MapCoordinates p7 = {11.0, 25};
	MapCoordinates p8 = {8.0,  20};

	float fsohtemph = 0;
	float fsohtempl = 0;

	//start judge form right to left(voltage form high to low)
    //start judge form right to left(voltage form high to low)
        if(fvol >= p1.x)                         // Limit=1
	{
		//start form high to low(soh from high to low)
            if((*pfsoh) >= p2.y)                 // 20180730 DW EDIT p[0].y > p[1].y
		{
			return TR_GOODnPASS;
		}
            else if((*pfsoh) >= p1.y)            // 20180730 DW EDIT p[1].y > p[0].y
		{
			return TR_CAUTION;
		}
		else
		{
			return TR_BADnREPLACE;
		}
	}
        else if(fvol >= p2.x)                      // Limit=2
        {                                          // 20180730 DW EDIT
            if((*pfsoh) >= p2.y)                   // 20180730 DW EDIT
            {                                      // 20180730 DW EDIT
                return TR_GOODnPASS;               // 20180730 DW EDIT
            }                                      // 20180730 DW EDIT
            else if((*pfsoh) >= p2_1.y)            // 20180730 DW EDIT
            {                                      // 20180730 DW EDIT
                return TR_CAUTION;                 // 20180730 DW EDIT
            }                                      // 20180730 DW EDIT
            else                                   // 20180730 DW EDIT
	{
              fsohtempl = ((p1.y - p3.y)/(p1.x*100 - p3.x*100))*(fvol*100 - p3.x*100) + p3.y;

              if((*pfsoh) >= fsohtempl)
		{
			return TR_ASK_CUSTOM;
		}
		else
		{
			return TR_BADnREPLACE;
		}
	}
        }

        else if(fvol >= p3.x)      // Limit=2.5  20180731 dw edit
	{
             fsohtemph = ((p2_1.y - p5.y)/(p2_1.x*100 - p5.x*100))*(fvol*100 - p5.x*100) + p5.y;   // 20180731 DW EDIT
             fsohtempl = ((p1.y - p3.y)/(p1.x*100 - p3.x*100))*(fvol*100 - p3.x*100) + p3.y;       // 20180731 DW EDIT

		if((*pfsoh) >= fsohtemph)
		{
			return TR_GOODnRRCHARGE;
		}
		else if((*pfsoh) >= fsohtempl)
		{
			return TR_ASK_CUSTOM;
		}
		else
		{
			return TR_BADnREPLACE;
		}
	}

        else if(fvol >= p4.x)      // Limit=3
        {                                                                                         // 20180730 DW EDIT
            fsohtemph = ((p2_1.y - p5.y)/(p2_1.x*100 - p5.x*100))*(fvol*100 - p5.x*100) + p5.y;   // 20180730 DW EDIT
            fsohtempl = ((p3.y - p4.y)/(p3.x*100 - p4.x*100))*(fvol*100 - p4.x*100) + p4.y;       // 20180730 DW EDIT

		if((*pfsoh) >= fsohtemph)
		{
			return TR_GOODnRRCHARGE;
		}
		else if((*pfsoh) >= fsohtempl)
		{
			return TR_ASK_CUSTOM;
		}
		else
		{
			return TR_BADnREPLACE;
		}
	}
        else if(fvol >= p5.x)     // Limit=4
        {
            fsohtempl = ((p2_1.y - p5.y)/(p2_1.x*100 - p5.x*100))*(fvol*100 - p5.x*100) + p5.y;  // 20180730 DW EDIT

            if((*pfsoh) >= fsohtempl)
	{
                return TR_GOODnRRCHARGE;           // 20180730 DW EDIT
            }
            else
            {
                return TR_ASK_CUSTOM;              // 20180730 DW EDIT
            }
        }
        else if(fvol >= p6.x)                     // Limit=5
        {
            if((*pfsoh) >= p7.y)                 // 20180730 DW EDIT
		{
                return TR_GOODnRRCHARGE;           // 20180730 DW EDIT
		}
            else
		{
			return TR_ASK_CUSTOM;
		}
        }
        else if(fvol >= p8.x)                        // Limit=6
            {
                fsohtempl = ((p6.y - p8.y)/(p6.x*100 - p8.x*100))*(fvol*100 - p8.x*100) + p8.y;  // 20180730 DW EDIT

                if((*pfsoh) >= fsohtempl)                 // 20180730 DW EDIT
                {                                         // 20180730 DW EDIT
                    //force tested cca = 0
                    (*pfsoh) = 0;                         // 20180730 DW EDIT
                    return TR_BAD_CELL_REPLACE;           // 20180730 DW EDIT
                }
		else
		{
                    return TR_ASK_CUSTOM;
		}
	}
	else
	{
		if((*pfsoh) >= p8.y)
		{
			//force tested cca = 0
			(*pfsoh) = 0;
                return TR_BAD_CELL_REPLACE;               // 20180730 DW EDIT
		}
		else
		{
                return TR_ASK_CUSTOM;                    // 20180730 DW EDIT
		}
	}
}
*/


// end
