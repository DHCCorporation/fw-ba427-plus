//*****************************************************************************
//
// Source File: cloud_config.h
// Description: the configuration for cloud service.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 10/08/18     Jerry.W        Created file.
// 08/07/20     David.Wang     close registered function
//
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/driver/wifi_driver.h#2 $
// $DateTime: 2017/12/18 16:17:54 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************


#ifndef SERVICE_CLOUD_CONFIG_H_
#define SERVICE_CLOUD_CONFIG_H_

//1-> connect to AWS IoT, 0-> connect to EC2
#define AWS_IOT_SERVICE 1              //20200807 dw Edit close registered function 0>1
#define RD_MODE         0

#if !AWS_IOT_SERVICE
#define CLOUD_ACCOUNT	"service@dhc.com.tw"
//#define CLOUD_ACCOUNT	"fcatest@fcatest.com"
//#define CLOUD_ACCOUNT	"test@tridon.com"
//#define CLOUD_ACCOUNT	"test@test.com"
#endif

#if RD_MODE
#define URL_UPLOAD_NORMAL   "/topics/uploadRecord/dev?qos=1"
#define URL_DEVICE_REG      "/topics/registerDevice/dev?qos=1"
#else
#define URL_UPLOAD_NORMAL   "/topics/ba427plus/uploadRecordBT?qos=1"
#define URL_DEVICE_REG      "/topics/ba427plus/registerDevice?qos=1"
#endif

#define URL_OTA_RESULT      "/topics/ba427plus/otaResult?qos=1"
#define URL_CHECK_VERSION   "/topics/ba427plus/checkVersion?qos=1"
#define URL_SYSTEM_RECORD   "/topics/ba427plus/uploadRecordST?qos=1"

#endif /* SERVICE_CLOUD_CONFIG_H_ */


