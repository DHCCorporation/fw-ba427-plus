//*****************************************************************************
//
// Source File: dlinklistsvc.h
// Description: Prototype for functions of double linking list data structure.
//				
//
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 09/08/15     William.H      Created file.
// 10/01/15     William.H      Re-arrange/remove Global varaible's usage due to we've used stack to behave the manu level selection.
//                             Respective global varaible by the respective widget file to be maintained by their own.
// 10/05/15     William.H      Check in BT2020 project which branched from LCDFontMaker(Pre-BT2020) by modifed related GPIOs.
// 11/02/17     Jerry.w		   1. add const in tJISBatteryType type definition for fewer memory usage
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/service/dlinklistsvc.h#7 $
// $DateTime: 2017/11/02 09:22:22 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#ifndef __DLINKLISTSVC_H__
#define __DLINKLISTSVC_H__

typedef struct
{
	char *batString;
	uint16_t itemNum;
	uint16_t capVal;
}
tJISBatteryType;


//
//	JIS Battery Type Node Data Structure
//
struct node
{
	tJISBatteryType psJISBatType;
	struct node *next;
	struct node *prev;
};

typedef struct node JISNode;


//
//	JIS BAT Type Linked List
//
typedef struct
{
	JISNode *head;
	JISNode *currNode;
}
JISBATDLList;


extern void JISDLlist_Init(JISBATDLList *linklist);

extern JISNode *JISDLlist_Gen_New_Node(tJISBatteryType JISData);

extern void JISDLlist_Travel(JISBATDLList *linklist);

extern void JISDLlist_Del(JISBATDLList *linklist);

extern void JISDLlist_Insert_In_Front(JISBATDLList *linklist, JISNode *newNode);

extern void JISDLlist_Insert(JISBATDLList *linklist, JISNode *nodeInList, JISNode *newNode);

extern JISNode *JISDLlist_Get_Last_Node(JISBATDLList * linklist);

extern void JISDLlist_Insert_In_Rear(JISBATDLList *linklist, JISNode *newNode);

extern void JISDLlist_Init_All(JISBATDLList *linklist,  const tJISBatteryType *dataArray, uint16_t tNum);

extern JISNode *JISDLlist_Search_by_ItemNum(JISBATDLList *linklist, uint16_t value);

extern JISNode *JISDLlist_Search_by_BATTypeString(JISBATDLList *linklist, char *searchStr);

#endif

