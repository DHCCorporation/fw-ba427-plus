//*****************************************************************************
//
// Source File: cloud_tranform.h
// Description: transform informations to sting type
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 01/23/18     Jerry.W        Created file.
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/driver/wifi_driver.h#2 $
// $DateTime: 2017/12/18 16:17:54 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************


#ifndef __CLOUD_TRANSFORM_H__
#define __CLOUD_TRANSFORM_H__

#include "cloud.h"

SELRATING string_to_bat_rating(const char* bat_r);

BATTERYTYPE string_to_bat_type(const char* bat_t);

bool TIME_TO_STRING(TIME_T* time, char* time_string);

bool test_type_to_string(TEST_TYPE type, char* sting);

bool Bat_type_to_sting(BATTERYTYPE type, char* sting);

bool Rating_to_sting(SELRATING rating, char* sting);

bool Judgement_to_sting(TEST_RESULT judge, char* sting);

bool RC_judge_to_string(RC_RESULT judge, char* sting);

bool SYSRANK_to_string(SYSRANK rank, char* sting);



#endif
