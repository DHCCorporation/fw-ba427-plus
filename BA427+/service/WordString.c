//*****************************************************************************
//
// Source File: WordString.c
// Description: new grab string function
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 08/12/20     DW             Create file.
//
// =============================================================================
//
//
// $Header$
// $DateTime$
// $Author$
//
//
// Copyright (c) 2020 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#include "WordString.h"

//*** WORDS ********************************************//
char* EN_string_table[]={"TRUCK/GROUP 31?", "YES", "NO"};
char* FR_string_table[]={"TRUCK/GROUP 31?", "YES", "NO"};
char* SP_string_table[]={"TRUCK/GROUP 31?", "YES", "NO"};

//*** LANGUAGE *****************************************//
char** string_table[]={EN_string_table, FR_string_table, SP_string_table};

typedef struct{
    int number_of_language;
    int number_of_string;
    char ***string_table;
}STRING_TABLE_T;

STRING_TABLE_T str_table_spe = {sizeof(string_table)/sizeof(char**), 5, string_table};

int Get_language_number(){
    return str_table_spe.number_of_language;
}

int Get_String_number(){
    return str_table_spe.number_of_string;
}

char* Get_String(int lang, STRING_WORD_T str_idx){
    if(lang > Get_language_number()) return 0;
    if(str_idx > Get_String_number()) return 0;

    return str_table_spe.string_table[lang][str_idx];
}











