//*****************************************************************************
//
// Source File: UTF8_WBsvc.h
// Description: UTF8 character images.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 10/12/18     Jerry.W        Created file.
// 11/27/18     Jerry.W        add UTF8_show function.
// 12/06/18     Jerry.w        add printer function.
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/driver/wifi_driver.h#2 $
// $DateTime: 2017/12/18 16:17:54 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#ifndef SERVICE_UTF8_WBSVC_H_
#define SERVICE_UTF8_WBSVC_H_

#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "UTF8_word_bank\font_graph.h"

typedef enum{
	font_12,
	font_18,
}FONTS_T;

typedef enum{
	side_left,
	side_middle,
	side_right,
}SHOW_side;

typedef struct{
	uint16_t x1;
	uint16_t x2;
	uint16_t y1;
	uint16_t y2;
}SOHW_region;

typedef struct{
	uint8_t pixel_W;
	uint8_t pixel_H;
	float	HeatTime;
}PRINT_spec_t;

typedef struct{
	SHOW_side side;		//�a���a�k�ܤ�
	uint8_t row_gap;	//��Z
	bool 		transparent;
	PRINT_spec_t 	Print_spec;
	SOHW_region region;		//get the display region from UTF8_show function
}SHOW_profile_t;

/*************************************************************************************************
 * @ Name		: UTF8_printf
 * @ Brief 		: display UTF8 string on LCM.
 * @ parameter 	: x, y - indicating the location to display.
 * 				  string - the UTF8 string to be displayed.
 *************************************************************************************************/
void UTF8_printf(uint32_t x, uint32_t y, char* string);

/*************************************************************************************************
 * @ Name		: UTF8_show
 * @ Brief 		: display UTF8 string on LCM with extended features.
 * @ parameter 	: x, y - indicating the location to display.
 * 				  string - the UTF8 string to be displayed.
 * 				  limit_width - the space of LCM for displaying characters.
 * 				  profile - the profile for extended features.
 * @ return		: the height of string.
 *************************************************************************************************/
uint8_t UTF8_LCM_show(uint32_t x, uint32_t y, char* string, uint16_t limit_width, SHOW_profile_t *profile);


/*************************************************************************************************
 * @ Name		: UTF8_Printer_show
 * @ Brief 		: display UTF8 string on LCM with extended features.
 * @ parameter 	: string - the UTF8 string to be displayed.
 * 				  profile - the profile for extended features.
 *************************************************************************************************/
void UTF8_Printer_show(char* string, SHOW_profile_t *profile);


/*************************************************************************************************
 * @ Name		: UTF8_str_size
 * @ Brief 		: count the string length.
 * @ parameter 	: string - the UTF8 string.
 * @ return		: string length.
 *************************************************************************************************/
uint32_t UTF8_str_size(const char *string);

/*************************************************************************************************
 * @ Name		: get_line_UTF8
 * @ Brief 		: get a string can be put in limit width.
 * @ parameter 	: buf - the memory space to store output string(please make sure it has enough memory).
 * 				  string - the UTF8 string.
 * 				  limit_width - the space of LCM for displaying characters.
 * 				  width - the memory to output the width of the string stored in buf.
 * 				  pixel_H - the memory to output the highest pixel of the string stored in buf.
 * 				  pixel_L - the memory to output the lowest pixel of the string stored in buf.
 * @ return		: string length.
 *************************************************************************************************/
uint16_t get_line_UTF8(char *buf, char *string, uint16_t limit_width, uint16_t *width, uint8_t *pixel_H, uint8_t *pixel_L , FONTS_T font, uint8_t distance);


#endif /* SERVICE_UTF8_WBSVC_H_ */
