//*****************************************************************************
//
// Source File: AWS_IoT_svc.c
// Description: service for communication with AWS IoT.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 10/02/18     Jerry.W        Created file.
// 11/19/18     Henry.Y        AWS_IoT_Upload_test_record() add UPLOAD_MODE_t mode.
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/driver/wifi_driver.h#2 $
// $DateTime: 2017/12/18 16:17:54 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#ifndef SERVICE_AWS_IOT_SVC_H_
#define SERVICE_AWS_IOT_SVC_H_

#include "cloud.h"
#include "SystemInfosvs.h"
#include "cloud_tranform.h"
#include "Json/jansson.h"
#include "wifi_service.h"
#include "HTTP_service.h"
#include "extrtcsvc.h"

typedef enum{
	register_success = 0,
	register_fail_database_read = 1,
	register_fail_database_write = 3,
	register_fail_wrong_account = 6,
	register_fail_registered = 7,	//this means register success!!!
	register_fail_registered_by_other = 8,
	register_fail_response_err,
	register_fail_network_err,
	register_fail_unknown,
}REGIST_FAIL_REASON;



#define SHADOW_RESEND_PERIOD	2 //second,


#define AWS_IoT_DEBUG	1

/*************************************************************************************************
 * @ Name		: AWS_IoT_Upload_test_record
 * @ Brief 		: upload test result through AWS IoT
 * @ Parameter 	: rec -- test record.
 * 				  rslt -- callback function to inform the app of the upload result.
 * 				  mode -- uploading mode, normal or MFC.
 *************************************************************************************************/
bool AWS_IoT_Upload_test_record(TEST_RECORD_T *rec, void(*rslt)(bool ,uint8_t), UPLOAD_MODE_t mode);



/*************************************************************************************************
 * @ Name		: AWS_IoT_FW_check.
 * @ Brief 		: check new FW available.
 * @ parameter 	: version -- current FW version.
 * 				  rslt -- callback function on cloud response.
 * 				  		  please refer CHECK_OTA_RESULT definition for result in callcback function.
 *************************************************************************************************/
bool AWS_IoT_FW_check(uint8_t *version, void(*rslt)(uint8_t FW_result, OTA_FW_t *FW, uint8_t FLS_result, OTA_FW_t *FLS));


/*************************************************************************************************
 * @ Name		: AWS_IoT_send_Upgrade_result.
 * @ Brief 		: send the OTA result to AWS IoT.
 * @ parameter 	: OLD_version -- old version.
 * 				  NEW_version -- new version.
 * 				  success -- OAT success or not.
 *************************************************************************************************/
bool AWS_IoT_send_Upgrade_result(uint8_t *OLD_version, uint8_t *NEW_version, bool success, void(*rslt)(bool result));


/*************************************************************************************************
 * @ Name		: AWS_IoT_poll.
 * @ Brief 		: processing resent work.
 *************************************************************************************************/
void AWS_IoT_poll();


/*************************************************************************************************
 * @ Name		: AWS_IoT_device_register.
 * @ Brief 		: register device by scanned data.
 * @ parameter 	: data -- register data.
 * 				  rslt -- callback function on cloud response.
 *************************************************************************************************/
bool AWS_IoT_device_register(uint8_t *data, void(*rslt)(bool result, uint8_t reason));

#endif /* SERVICE_AWS_IOT_SVC_H_ */
