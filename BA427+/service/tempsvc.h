//*****************************************************************************
//
// Source File: tempsvc.h
// Description: Declare all the functions to access temperature sensor(tp-337a) data.
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 12/01/15     Vincent.T      Created file.
// 03/11/16     Vincent.T      1. Redefine CCATempComp(); to get testing temperature value.
// 08/10/16     Vincent.T      1. New func. IRTempComp(); to implement temperature compensation of IR. 
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/service/tempsvc.h#5 $
// $DateTime: 2017/09/22 10:27:40 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#include "../driver\tp337a_temp.h"

#ifndef _TEMPSVC_H_
#define _TEMPSVC_H_

void GetTemp(float *pfBodyTemp, float *pfObjTemp);
void GetBodyTemp(float *pfTemp);
void GetObjTemp(float *pfTemp, float *pfbody);
float get_obj_vol(float body, float object);	//the get the voltage of object sensor for smartlab


#endif /* _TEMPSVC_H_ */
