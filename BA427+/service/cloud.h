//*****************************************************************************
//
// Source File: cloud.h
// Description: software timer
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 01/12/18     Jerry.W        Created file.
// 03/22/18     Jerry.W        add Time_adjust(), TIME_cb() for time adjustment.
// 11/19/18     Henry.Y        (Jerry)add set_upload_recod_mode() and get_upload_recod_mode().
// 12/07/18     Jerry.W        modify FW check function for FLASH update.
// 08/13/20     David.Wang     add bat test register PackTestNum
//
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/driver/wifi_driver.h#2 $
// $DateTime: 2017/12/18 16:17:54 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************


#ifndef __CLOUD_H__
#define __CLOUD_H__


#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include "../driver/uartstdio.h"
#include "driverlib/interrupt.h"
#include "../driver/mem_protect.h"
#include "../driver/soft_timer.h"
#include "battcoresvc.h"
#include "testrecordsvc.h"
#include "flashsvc.h"
#include "Json/jansson.h"
#include "cloud_config.h"


typedef struct{
	TEST_TYPE test_type;
	BATTERYTYPE bat_type;
	SELRATING rating;
	uint16_t CCA;
	uint16_t RC;
}BATTERY_INFO_T;


#pragma pack(push)
#pragma pack(4)
typedef struct{
	uint16_t year;
	uint8_t month;
	uint8_t day;
	uint8_t hour;
	uint8_t minute;
	uint8_t second;
}TIME_T;

typedef struct{
	TEST_TYPE 		Test_Type;
	TIME_T 			Test_Time;
	uint8_t			VIN[18];
	uint8_t 		RO[9];
	BATTERYTYPE		Battery_Type;
	uint8_t         SSBattery_Type;
	float			Voltage;
	SELRATING		Ratting;
	uint16_t		CCA_Capacity;
	uint16_t		CCA_Measured;
	float           IR;
	TEST_RESULT		Judgement;
	uint8_t			SOH;
	uint8_t			SOC;
	uint16_t		RC_Set;
	uint16_t		RC_Measured;
	RC_RESULT		RC_judegment;
	uint8_t			Test_code[20];
	uint8_t         JIS[20];
	float			Temperature;
	uint8_t 		IsBatteryCharged;
	uint8_t         TestInVecicle;
	uint8_t         PackTestNum;         //for HD version pack test
    uint8_t         PackTestCount;       //for HD version pack test
    char            Email[60];

}BATTERY_SARTSTOP_RECORD_T;

typedef struct{
	TEST_TYPE 		Test_Type;
	TIME_T 			Test_Time;
	uint8_t			VIN[VININ_LEN1+1];
	uint8_t 		RO[ROIN_LEN+1];
	SYSRANK			Crank_result;
	float 			Crank_voltage;
	SYSRANK			Idle_result;
	float			Idle_voltage;
	SYSRANK			Ripple_result;
	float			Ripple_voltage;
	SYSRANK			Loaded_result;
	float			Loaded_voltage;
	float			Temperature;
	char            Email[60];
}SYSTEM_TEST_RECORD_T;

typedef struct{
	TEST_TYPE 		Test_Type;
}TEST_RECORD_T;

typedef struct{
	TEST_TYPE 		Test_Type;
	TIME_T 			Test_Time;
	float 			IR_Vol;
	float			Measured_IR;
	float			Temperature;
}IR_TEST_RECORD_T;

#pragma pack(pop)

typedef enum{
	CK_OTA_demand_yes,
	CK_OTA_demand_no,
	CK_OTA_demand_must,
	CK_OTA_demand_internet_fail,

}CHECK_OTA_RESULT;


typedef struct{
	uint8_t *version;
	uint8_t *URL;
	uint8_t *Note;
	uint32_t FW_len;
	uint8_t CRC;
}OTA_FW_t;

typedef struct{
	uint8_t number;
	OTA_FW_t *FW[];
}OTA_FW_LIST_t;


typedef enum{
	normal_upload,
	MFC_upload,
}UPLOAD_MODE_t;

typedef enum{
	Fail_reason_NULL,
	Fail_reason_data_read = 1,
	Fail_reason_data_write = 3,
	Fail_reason_not_register = 4,
	Fail_reason_network_err,
	Fail_reason_response_err,
}FAIL_REASON_T;


extern uint8_t Jason_header[];


/*************************************************************************************************
 * @ Name		: GET_BAT_BY_VIN
 * @ Brief 		: Get battery information by VIN
 * @ Parameter 	: VIN -- 0 = VIN code.
				  VIN_len -- VIN code length.
				  Bat_info -- battery information will returned in this data structure.
				  wait_time -- the time waiting server response.
 * @ Return		: 0 - successfully get battery information.
				  1 - timeout & no response.
				  2 - no battery information with this VIN.
 *************************************************************************************************/
uint8_t GET_BAT_BY_VIN(uint8_t* VIN, uint8_t VIN_len, BATTERY_INFO_T* Bat_info, uint8_t wait_time);


/*************************************************************************************************
 * @ Name		: set_upload_recod_mode
 * @ Brief 		: setting upload mode used by user or MFC.
 * @ Parameter 	: mode -- set upload mode.
 *************************************************************************************************/
void set_upload_recod_mode(UPLOAD_MODE_t mode);

/*************************************************************************************************
 * @ Name		: get_upload_recod_mode
 * @ Brief 		: getting upload mode used by user or MFC.
 * @ Parameter 	: void
 *************************************************************************************************/
UPLOAD_MODE_t get_upload_recod_mode(void);


/*************************************************************************************************
 * @ Name		: upload_saved_recod
 * @ Brief 		: if there is records to be upload, upload all record in background.
				  otherwise, do noting.
 * @ Parameter 	: state -- callback function to notify upload succeeded or failed.
 *************************************************************************************************/
bool upload_recod(void(*state)(bool ,uint8_t));



/*************************************************************************************************
 * @ Name		: FirmwareDownload
 * @ Brief 		: download FW from Internet.
 *************************************************************************************************/
bool download_request(uint8_t *server, uint8_t* FW_URL, uint32_t start, uint32_t end, void(*done_cb)(bool, uint16_t , uint8_t*));


/*************************************************************************************************
 * @ Name		: Time_adjust.
 * @ Brief 		: adjust system by Internet.
 * @ Parameter 	: TZ -- time zone.
 *************************************************************************************************/
void Time_adjust(int16_t TZ);



/*************************************************************************************************
 * @ Name		: get_saved_record_count.
 * @ Brief 		: get the number of records to be uploaded.
 *************************************************************************************************/
uint16_t get_saved_record_count(void);



/*************************************************************************************************
 * @ Name		: account_ident.
 * @ Brief 		: identify account with cloud.
 * @ parameter 	: version -- current FW version.
 * 				  SN -- Device SN number.
 * 				  rslt -- callback function on cloud response.
 * 				  		  please refer CHECK_OTA_RESULT definition for result in callcback function.
 *************************************************************************************************/
bool FW_check(uint8_t *version, uint8_t *SN, void(*rslt)(uint8_t FW_result, OTA_FW_t* OTA_FW,uint8_t FLS_result, OTA_FW_t* OTA_FLS));


/*************************************************************************************************
 * @ Name		: get_FW_list.
 * @ Brief 		: get FW list from cloud.
 * @ parameter 	: version -- current FW version.
 * 				  SN -- Device SN number.
 * 				  rslt -- callback function on cloud response.
 *************************************************************************************************/
bool get_FW_list(uint8_t *version, uint8_t *SN, void(*rslt)(bool result, OTA_FW_LIST_t* FW_list));


/*************************************************************************************************
 * @ Name		: get_Upgrade_ID.
 * @ Brief 		: get upgrade ID from cloud.
 * @ parameter 	: version -- current FW version.
 * 				  SN -- Device SN number.
 * 				  Time_zone -- time zone (signed number).
 * 				  rslt -- callback function on cloud response.
 *************************************************************************************************/
bool get_Upgrade_ID(uint8_t *version, uint8_t *SN, int16_t Time_zone, void(*rslt)(bool result, UPGRADE_ID_t* ID));


/*************************************************************************************************
 * @ Name		: send_Upgrade_ID.
 * @ Brief 		: send upgrade id back to cloud.
 * @ parameter 	: ID -- update ID.
 * 				  Time_zone -- time zone (signed number).
 * 				  rslt -- callback function on cloud response.
 *************************************************************************************************/
bool send_Upgrade_ID(UPGRADE_ID_t *ID, int16_t Time_zone, void(*rslt)(bool result));

#endif
