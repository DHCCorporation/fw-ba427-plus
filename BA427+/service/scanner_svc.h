//*****************************************************************************
//
// Source File: scanner_svc.h
// Description: printer function.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 01/03/18     Henry.Y	       Created file.
// 09/11/18     Henry.Y        Add trigger pin on/ff definition.
// 11/19/18     Henry.Y        Add set_scanner_cb.
// 02/24/20     Henry.Y        Scanner control modification.
// =============================================================================
//
//
// $Header$
// $DateTime$
// $Author$
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#ifndef SERVICE_SCANNER_SVC_H_
#define SERVICE_SCANNER_SVC_H_

#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "driverlib/rom.h"
#include "../driver/uartstdio.h"

#include "..\driver/scanner_driver.h"
#include "..\driver/delaydrv.h"

#define SCAN_ON scanner_switch(1);
#define SCAN_OFF scanner_switch(0);
#define SCAN_PWR_CYL scanner_switch(2);


#define SCANNER_BUZZ BUZZER_ON; delay_ms(100); BUZZER_OFF;


typedef enum{
	trigger,
	reboot,
	save_config,
	get_rd_mode,
	set_rd_mode,
	get_result,
	set_default,	
	get_version,
	idle = 99,
}scanner_set;


#define SCANNER_RX_BUFFER_SIZE 20

// return data receiving length
uint8_t scanner_send_cmd(scanner_set cmd);
void scanner_send(const char *pui8Buffer, uint8_t ui32Count);


//
// barcode scanner receive callback
//
void set_scanner_cb(void (*scan_cb)(bool, uint8_t));

bool scanner_msg_to_string(scanner_set cmd, uint8_t* data, uint8_t str_size);

#endif /* SERVICE_SCANNER_SVC_H_ */
