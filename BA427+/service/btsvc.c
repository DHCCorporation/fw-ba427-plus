//*****************************************************************************
//
// Source File: btsvc.c
// Description: Bluetooth service functions' definition.
//				
//
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 01/13/16     William.H      Created file.
// 01/27/16     Henry.Y        1. #include "stacksvc.h", #include "Lcdsvc.h", #include "..\driver/st7565_lcm.h", #include "..\app\Widgetmgr.h"
//                             2. Remove global variables: g_fCCA_Set_Capacity. 
//                             3. Add global variable: g_SetCapacityVal, g_bCheck_clamps,
//                                                     g_BatteryType, g_SelectRating,
//                                                     Internal_bat_low, g_BluetoothEnabled,
//                                                     g_MenuLvlStack
//                             4. New BTClear_Status(); func. to clear test stacks and push 'BLUETOOTHSET' entry in stack.
//                             5. Modify BTUART6IntHandler() to process a new arrived command from mobile(APP).
// 02/17/16     Vincent.T      1. global variables for start-stop test
//                             2. #include "..\app\Battypeselwidget.h"
//                             3. #include "testrecordsvc.h"
//                             4. #include "..\app\Selratingwidget.h"
// 02/18/16     Vincent.T      1. When receving 0x55, 0x56 && in system test routine, back to main menu
// 11/15/16     Ian.C          1. extern g_LangSelectIdx for multi-language.
//                             2. Modify BTClear_Status(); to BTClear_Status(g_LangSelectIdx); for multi-language.
// 03/22/17     Vincent.T      1. global variables for bluetooth-check func.: blUsbBTchk, *pui8USBUpdataBuf and blDataRecComplete
//                             2. Modify BTUART6IntHandler(); due to bluetooth-check func.;
// 06/01/17     Henry          FW version: BT2100_WW_V01.a
//                             1. BTUARTSend(): add UART space check before each byte sent.
//                             2. BTUART6IntHandler(): add protocol 0x09, 0xff~0xff to check firmware version.
// 09/28/17 	Jerry.W		   1. Rewrite Widget for new software architecture
//							   2. move all BT command process to app layer
// 11/06/17     Henry.Y	       1. Hide useless variables:g_ucPre_check_cable,voltage_over_32
// 11/10/17     Jerry.W        1. Modify BT service layer for more flexibile application
// 11/23/17     Jerry.W        1. add delay at sending data for baud rate shifting
// 12/01/17     Jerry.W        1. Add command print out function for debugging
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/service/btsvc.c#3 $
// $DateTime: 2017/12/18 16:12:45 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#include "btsvc.h"
#include "../driver/delaydrv.h"







tBoolean g_bACK_Status = false;
tBoolean g_bService_status = false;

//*****************************************************************************
//
// Declare Bluetooth UART Tx/Rx buffers and parameters.
//
//*****************************************************************************
char RX_buffer[20] = {0x00};
uint8_t g_ucRX_index = 0;


//*****************************************************************************
//
// Declare CCA capacity parameter receive from APP via Bluetooth UART Rx buffers.
//
//*****************************************************************************
//uint16_t g_fCCA_Set_Capacity = 0;




//*****************************************************************************
// USB Bluetooth function check
//*****************************************************************************
//extern bool blUsbBTchk;
//extern uint8_t *pui8USBUpdataBuf;
//extern bool blDataRecComplete;



void (*BTcommand_callback)(const char *) = NULL;
bool (*BTcommand_match)(const char *,uint8_t) = NULL;



void BTUARTSend(const char *pui8Buffer, uint32_t ui32Count)
{
	uint8_t i;
	//
	// Loop while there are more characters to send.
	//
	BT_DEBUG("Event : ");
	for(i = 0;i < ui32Count ;i++){
		BT_DEBUG(" %02x",pui8Buffer[i]);
	}
	BT_DEBUG("\n");

	while(ui32Count != 0)
	{
		while(!ROM_UARTSpaceAvail(BTUART_BASE));//busy?
		
		//
		// Write the next character to the UART.
		//
		ROM_UARTCharPutNonBlocking(BTUART_BASE, *pui8Buffer++);
		ROM_SysCtlDelay(get_sys_clk()/3/1000/5);
		ui32Count--;
	}


}



void BTUART6IntHandler(void)
{
	unsigned long ui32Status;


	//
	// Get the interrrupt status.
	//
	ui32Status = ROM_UARTIntStatus(BTUART_BASE, true);

	//
	// Clear the asserted interrupts.
	//
	ROM_UARTIntClear(BTUART_BASE, ui32Status);

	//
	// Loop while there are characters in the receive FIFO.
	//
	while(ROM_UARTCharsAvail(BTUART_BASE))
	{
		//
		// Read the next character from the UART and write it back to the UART.
		//
		RX_buffer[g_ucRX_index] = (unsigned char)ROM_UARTCharGetNonBlocking(BTUART_BASE);


		/*if(blUsbBTchk == true)
		{
			pui8USBUpdataBuf[g_ucRX_index] = RX_buffer[g_ucRX_index];

			if(g_ucRX_index == 0 && (pui8USBUpdataBuf[0] == '@' || pui8USBUpdataBuf[0] == '!'))
			{
				blDataRecComplete = true;
			}
			else
			{
				g_ucRX_index = 0;
			}
		}*/
		g_ucRX_index++;
		if(BTcommand_match){
			if(BTcommand_match(RX_buffer,g_ucRX_index))
			{

				BTcommand_callback(RX_buffer);
				g_ucRX_index = 0;
			}
		}
	}

	if((ui32Status == UART_INT_RT)&&(g_ucRX_index != 0)){
		g_ucRX_index = 0;
	}
}

void Bluetooth_buffer_flush(){
	g_ucRX_index = 0;
}

void set_BTcommand_callback(void (*com_callback)(const char*), bool (*command_match)(const char*,uint8_t)){
	BTcommand_callback = com_callback;
	BTcommand_match = command_match;
}
