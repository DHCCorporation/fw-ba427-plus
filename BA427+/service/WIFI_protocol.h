//*****************************************************************************
//
// Source File: \WIFI_protocol.h
// Description: wifi module protocol.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// ../../..     Jerry.W        Created file.
// 11/19/18     Henry.Y        (Jerry) Add new policy: WIFI_com_get_version\WIFI_com_loopback\WIFI_evnt_resp_version
// ============================================================================
//
//
// $Header$
// $DateTime$
// $Author$
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************


#ifndef WIFI_PROTOCOL_H
#define WIFI_PROTOCOL_H




typedef enum{
	WIFI_com_conn_policy = 	0x10,
	WIFI_com_scan =			0x11,
	WIFI_com_get_status = 	0x12,
	WIFI_com_get_version =  0x13,

	WIFI_com_loopback = 	0x1a,

	WIFI_evnt_conn_status = 0x20,
	WIFI_evnt_conn_policy = 0x21,
	WIFI_evnt_scan_result = 0x22,
	WIFI_evnt_resp_version= 0x23,

	TCP_com_conn_to_ser	=	0x30,
	TCP_com_disconn	=		0x31,

	TCP_evnt_status = 		0x40,

	HTTP_com_request =		0x50,

	HTTP_evnt_response =	0x60,
	
	SNTP_com_request = 		0x70,
	SNTP_com_response = 	0x71,

	//for v2.0 socket protocol
	TCP_com_conn_to_ser_v2 =	0x80,
	TCP_com_disconn_v2 = 		0x81,
	TCP_evnt_status_v2 = 		0x82,
	TCP_com_send = 				0x83,
	TCP_evnt_receive = 			0x84,

	UDP_com_open = 				0x90,
	UDP_com_close = 			0x91,
	UDP_evnt_status = 			0x92,
	UDP_com_send = 				0x93,
	UDP_evnt_receive = 			0x94,

	CERT_server_set = 			0xa0,
	CERT_client_set = 			0xa1,

}PROTOCOL_T;


typedef enum{
	connected = 				0x00,
	connect_fail = 				0x01,
	disconnected = 				0x02,
	connect_to_service_fail = 	0x03,
	gettingIP = 				0x04,
}PROTOCOL_conn;




#define WIFI_SERVICE_CHANN		50

#define SOCKET_SERVICE_CHANN	90







#endif
