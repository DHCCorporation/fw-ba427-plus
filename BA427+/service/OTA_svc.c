//*****************************************************************************
//
// Source File: OTA_svc.c
// Description: OTA status handling service.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 11/19/18     Jerry.W        Created file.
// 01/14/19     Jerry.W        add more error handling functions.
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/driver/wifi_driver.h#2 $
// $DateTime: 2017/12/18 16:17:54 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "OTA_svc.h"
#include "flashsvc.h"
#include "cloud.h"
#include "AWS_IoT_svc.h"
#include "../driver/battcore_iocfg.h"
#include "../driver/delaydrv.h"
#include "usblib/usblib.h"
#include "usblib/usbcdc.h"
#include "usblib/usb-ids.h"
#include "usblib/device/usbdevice.h"
#include "usblib/device/usbdcdc.h"

extern uint8_t *strFWver;

#define FLS_COPY_LEN	4096
#define DOWNLOAD_SIZE 1024*4

#define _OTA_DEBUG_		1

#if _OTA_DEBUG_
#define 	OTA_DEBUG(x)	UARTprintf("OTA: %s @ %d\n", x, __LINE__)
#else
	OTA_DEBUG(x)
#endif

typedef enum{
	DL_dile,
	DL_preparing,
	DL_downloading,
	DL_verifying,
	DL_complete
}DOWNLOAD_status_t;


OTA_FW_t *New_FW = NULL;
OTA_FW_t *New_FLS = NULL;

OTA_Progress_t OTA_progress = {.status = OTA_status_unchecked};
FlashHeader_t *FLS_header;

IMAGE_VER_t *NEW_ver = NULL;
IMAGE_VER_t *OLD_ver = NULL;

uint32_t header_modify_time = 0;

LAYOUT_INDEX_t FLS_layout;

static bool service_initailed = false;

void image_copy_handler();
void imaga_new_ver_init();
void image_updating();
void get_new_LAYOUT();
void DL_cb(bool success, uint16_t len, uint8_t*data);
static void Jump_to_booloader_OTA(void);
void get_update_ID_handle(bool result, UPGRADE_ID_t* ID);

void(*event_callback)(OTA_EVENT_T);
int32_t image_update_index;
uint8_t updating_section; //0->LCM, 1->printer
char *FW_CHECK_RESULT[4] = {"yes","no","must","internet issue"};

uint32_t LCM_SECTION_SIZE = 0, Printer_SECTION_SIZE = 0, LCM_image_size = 0, Printer_image_size = 0;

int32_t image_find_dif(){
	uint16_t i;
	for(i = 0; i < LCM_SECTION_SIZE / LCM_image_size ; i++){
		uint8_t temp1 = NEW_ver->LCM[i];
		uint8_t temp2 = OLD_ver->LCM[i];
		temp1 = temp1;
		temp2 = temp2;
		if(NEW_ver->LCM[i] != 0 && (NEW_ver->LCM[i] != OLD_ver->LCM[i])){
			updating_section = 0;
			return i;
		}
	}
	for(i = 0; i < Printer_SECTION_SIZE / Printer_image_size ; i++){
		if(NEW_ver->Printer[i] != 0 && (NEW_ver->Printer[i] != OLD_ver->Printer[i])){
			updating_section = 1;
			return i;
		}
	}
	return -1;
}

void image_updating_handler(bool success, uint16_t len, uint8_t*data){
	static uint8_t retry = 0;
	uint32_t add, update_len, i;
	static uint8_t ver_modify = 0;
	uint8_t CRC = 0;
	LAYOUT_INDEX_t* old_layout = ExtFls_get_layout();

	if(success){

		add = (FLS_header->image_mode == 0)? old_layout->IMAGE2_add : old_layout->IMAGE_add;
		add += (updating_section == 0)? LCM_image_size * image_update_index : old_layout->IMAGE_sector_LCM_len + (Printer_image_size * image_update_index);
		update_len = (updating_section == 0)? LCM_image_size : Printer_image_size;

		for(i = 0 ; i < update_len ; i++){
			CRC ^= data[i];
		}
		CRC = ((CRC&0xf0) >> 4) ^ (CRC & 0x0f);

		if(updating_section == 0){
			if(len != LCM_image_size){
				OTA_DEBUG("download image length error");
				goto RETRY;
			}
			if((NEW_ver->LCM[image_update_index] & 0x0f) != CRC){
				OTA_DEBUG("download image CRC error");
				goto RETRY;
			}
			OLD_ver->LCM[image_update_index] = NEW_ver->LCM[image_update_index];
			OTA_progress.LCM_to_update--;
		}
		else if(updating_section == 1){
			if(len != Printer_image_size){
				OTA_DEBUG("download image error");
				return;
			}
			if((NEW_ver->Printer[image_update_index] & 0x0f) != CRC){
				OTA_DEBUG("download image CRC error");
				UARTprintf("%d, %d \n", NEW_ver->Printer[image_update_index], CRC);
				goto RETRY;
			}
			OLD_ver->Printer[image_update_index] = NEW_ver->Printer[image_update_index];
			OTA_progress.Printer_to_update--;
		}
		else{
			OTA_DEBUG("Image update error");
		}

		if(ver_modify > 10){
			ver_modify = 0;
			ExtFls_set_image_ver();
		}
		else ver_modify++;

		UARTprintf("save image to 0x%x\n", add);
		ExtFlsWrite(add, data, update_len );
		image_updating();
	}
	else{
RETRY:
		retry++;
		if(retry <= 3){
			image_updating();
		}
		else{
			OTA_DEBUG("download image fail");
			OTA_progress.status = OTA_status_error;
			event_callback(OTA_event_internet_unstable);
		}
	}
}

void image_updating(){
	uint8_t *server_temp = (uint8_t*)strstr((char*)New_FLS->URL, "//")+2;
	uint8_t *URL = (uint8_t*)strstr((char*)server_temp, "/");
	uint8_t server[60];
	uint32_t add, len;
	SYS_INFO* info = Get_system_info();

	memcpy(server, server_temp, URL - server_temp);
	server[URL - server_temp] = '\0';

	image_update_index = image_find_dif();

	if(image_update_index >= 0){
		UARTprintf("find index %d to update\n", image_update_index);
		add = (updating_section == 0)? FLS_layout.IMAGE_add + (LCM_image_size*image_update_index):
									FLS_layout.IMAGE_add+FLS_layout.IMAGE_sector_LCM_len+(Printer_image_size*image_update_index);
		len = (updating_section == 0)? LCM_image_size : Printer_image_size;
		download_request(server, URL,add ,add + len -1, image_updating_handler);
	}
	else{
		//when all the images are updated!
		ExtFls_set_image_ver();
		if((OTA_progress.LCM_to_update == 0) && (OTA_progress.Printer_to_update == 0)){
			FLS_header->OTA_FLS_IMAGE_status.status = DL_complete;
			ExtFls_set_header(FLS_header, sizeof(FlashHeader_t));
#if AWS_IOT_SERVICE
			OTA_progress.status = OTA_status_ready;
			event_callback(OTA_event_ready_to_upgrade);
#else
			get_Upgrade_ID(strFWver, info->SNP, info->Time_zone, get_update_ID_handle);
#endif
		}
		OTA_DEBUG("OTA ready");
	}
}

void imaga_new_ver_init_handler(bool success, uint16_t len, uint8_t*data){
	static uint8_t retry = 0;
	uint32_t i;
	if(success){
		if(len != (LCM_SECTION_SIZE / LCM_image_size + Printer_SECTION_SIZE / Printer_image_size)){
			OTA_DEBUG("check new image version fail");
			goto RETRY;
		}
		memcpy(NEW_ver->LCM, data, LCM_SECTION_SIZE / LCM_image_size);
		memcpy(NEW_ver->Printer, data+(LCM_SECTION_SIZE/LCM_image_size), Printer_SECTION_SIZE / Printer_image_size);

		OTA_progress.LCM_to_update = 0;
		OTA_progress.Printer_to_update = 0;

		for(i = 0; i < LCM_SECTION_SIZE / LCM_image_size ; i++){
			if(NEW_ver->LCM[i] != 0 && (NEW_ver->LCM[i] != OLD_ver->LCM[i])){
				OTA_progress.LCM_to_update++;
			}
		}
		for(i = 0; i < Printer_SECTION_SIZE / Printer_image_size ; i++){
			if(NEW_ver->Printer[i] != 0 && (NEW_ver->Printer[i] != OLD_ver->Printer[i])){
				OTA_progress.Printer_to_update++;
			}
		}
		OTA_progress.LCM_upload_total = OTA_progress.LCM_to_update;
		OTA_progress.Printer_upload_total = OTA_progress.Printer_to_update;
#if _OTA_DEBUG_
		UARTprintf("\n==GET NEW FLASH IMAGE VERSION==\n");
		UARTprintf("to be updated: %d, %d\n", OTA_progress.LCM_upload_total, OTA_progress.Printer_upload_total);
		UARTprintf("==GET NEW FLASH IMAGE VERSION==\n\n");
#endif

		image_updating();

	}
	else{
RETRY:
		retry++;
		if(retry <= 3){
			imaga_new_ver_init();
		}
		else{
			OTA_DEBUG("get new image version fail");
			OTA_progress.status = OTA_status_error;
			event_callback(OTA_event_internet_unstable);
		}
	}
}

void imaga_new_ver_init(){
	uint8_t *server_temp = (uint8_t*)strstr((char*)New_FLS->URL, "//")+2;
	uint8_t *URL = (uint8_t*)strstr((char*)server_temp, "/");
	uint8_t server[60];
	if(NEW_ver == NULL){
		NEW_ver = malloc(sizeof(IMAGE_VER_t));
		if(NEW_ver == NULL) goto ERROR;
		NEW_ver->LCM = malloc(LCM_SECTION_SIZE/LCM_image_size);
		NEW_ver->Printer = malloc(Printer_SECTION_SIZE/Printer_image_size);
		if(NEW_ver->LCM == NULL || NEW_ver->Printer == NULL) goto ERROR;
	}

	memcpy(server, server_temp, URL - server_temp);
	server[URL - server_temp] = '\0';

	download_request(server, URL,IMAGE_HEADER ,IMAGE_HEADER + (LCM_SECTION_SIZE/LCM_image_size + Printer_SECTION_SIZE/Printer_image_size) - 1, imaga_new_ver_init_handler);

	return;

ERROR:
	if(NEW_ver){
		if(NEW_ver->LCM) free(NEW_ver->LCM);
		if(NEW_ver->Printer) free(NEW_ver->Printer);
		free(NEW_ver);
	}
	OTA_DEBUG("malloc fail");
}

void Layout_handler(bool success, uint16_t len, uint8_t*data){
	static uint8_t retry = 0;
	if(success){
		if(len != sizeof(LAYOUT_INDEX_t)){
			OTA_DEBUG("check new LAYOUT length error");
			goto RETRY;
		}
		memcpy(&FLS_layout, data, sizeof(FLS_layout));
		LCM_SECTION_SIZE = FLS_layout.IMAGE_sector_LCM_len;
		Printer_SECTION_SIZE = FLS_layout.IMAGE_sector_Printer_len;
		LCM_image_size = FLS_layout.IMAGE_LCM_len;
		Printer_image_size = FLS_layout.IMAGE_Printer_len;
#if _OTA_DEBUG_
		UARTprintf("\n==GET NEW FLASH LAYOUT==\n");
		UARTprintf("IMAGE ADD: 0x%x\n", FLS_layout.IMAGE_add);
		UARTprintf("LCM section: 0x%x\n", LCM_SECTION_SIZE);
		UARTprintf("LCM size: 0x%x\n", LCM_image_size);
		UARTprintf("Printer section: 0x%x\n", Printer_SECTION_SIZE);
		UARTprintf("Printer size: 0x%x\n", Printer_image_size);
		UARTprintf("==GET NEW FLASH LAYOUT==\n\n");
#endif
		OLD_ver = ExtFls_get_image_ver();
		imaga_new_ver_init();
	}
	else{
RETRY:
		retry++;
		if(retry <= 3){
			get_new_LAYOUT();
		}
		else{
			OTA_DEBUG("get new LAYOUT fail");
			OTA_progress.status = OTA_status_error;
			event_callback(OTA_event_internet_unstable);
		}

	}
}

void get_new_LAYOUT(){
	uint8_t *server_temp = (uint8_t*)strstr((char*)New_FLS->URL, "//")+2;
	uint8_t *URL = (uint8_t*)strstr((char*)server_temp, "/");
	uint8_t server[60];

	memcpy(server, server_temp, URL - server_temp);
	server[URL - server_temp] = '\0';

	download_request(server, URL,LAYOUT_INDEX_HEADER ,LAYOUT_INDEX_HEADER + (sizeof(LAYOUT_INDEX_t)) - 1, Layout_handler);
}


void download_process(){

	uint8_t *server_temp = (uint8_t*)strstr((char*)New_FW->URL, "//")+2;
	uint8_t *URL = (uint8_t*)strstr((char*)server_temp, "/");
	uint32_t download_idx = FLS_header->OTA_FW_status.index;
	uint8_t server[60];
	memcpy(server, server_temp, URL - server_temp);
	server[URL - server_temp] = '\0';

	OTA_progress.FW_downloaded = download_idx;
	OTA_progress.FW_to_download = FLS_header->OTA_FW_status.new_size;

	download_request(server, URL,download_idx ,download_idx + DOWNLOAD_SIZE - 1, DL_cb);
}


void DL_cb(bool success, uint16_t len, uint8_t*data){
	static uint8_t retry = 0;
	SYS_INFO* info = Get_system_info();
	LAYOUT_INDEX_t* old_layout = ExtFls_get_layout();

	if(success){
		retry = 0;

		if(len){
			ExtFlsWrite(old_layout->OTA_BUF_ADD + FLS_header->OTA_FW_status.index, data, len);
			header_modify_time++;
		}


		FLS_header->OTA_FW_status.index += len;
		UARTprintf( GREEN_TEXT("download %d K.. \n")  , FLS_header->OTA_FW_status.index/1024);

		if(FLS_header->OTA_FW_status.index != FLS_header->OTA_FW_status.new_size) download_process();
		else{
			OTA_progress.FW_downloaded = FLS_header->OTA_FW_status.index;
			UARTprintf("download %d byte complete, to be verified!\n",FLS_header->OTA_FW_status.index);
			FLS_header->OTA_FW_status.status = DL_verifying;
			FLS_header->OTA_FW_status.index = 0;
			ExtFls_set_header(FLS_header, sizeof(FlashHeader_t));
			header_modify_time = 0;
		}

	}
	else{
		retry++;
		if(retry<3){
			download_process();
		}
		else{
			OTA_progress.status = OTA_status_error;
			event_callback(OTA_event_internet_unstable);
			UARTprintf("download_err\n");
		}
	}
}



void FW_check_rslt_handle(uint8_t FW_result, OTA_FW_t *FW, uint8_t FLS_result, OTA_FW_t *FLS){
	bool to_saved = false;
	SYS_INFO* info = Get_system_info();

	if(FW_result == CK_OTA_demand_internet_fail){
	    OTA_progress.status = OTA_status_check_fail;
	    event_callback(OTA_event_internet_unstable);
	}
	else if(FW_result == CK_OTA_demand_no && FLS_result == CK_OTA_demand_no){
	    OTA_progress.status = OTA_status_lastest;
	    event_callback(OTA_event_FW_up_to_date);
	}
	else{
	    OTA_progress.status = OTA_status_preparing;

	    if(New_FW){
	        free(New_FW->version);
	        free(New_FW->URL);
	        free(New_FW->Note);
	        free(New_FW);
	        New_FW = NULL;
	    }


	    UARTprintf("FW result = %s\n", FW_CHECK_RESULT[FW_result]);

	    if(FW_result == CK_OTA_demand_yes || FW_result == CK_OTA_demand_must){
	        UARTprintf("New version : %s\nURL : %s\nNote : %s\nCRC : %d\nlen : %d\n",  FW->version, FW->URL, FW->Note, FW->CRC, FW->FW_len);
	        New_FW = FW;
	        if(FLS_header->OTA_FW_status.status == DL_dile){
	            FW_INIT:
	            FLS_header->OTA_FW_status.status = DL_downloading;
	            FLS_header->OTA_FW_status.index = 0;
	            FLS_header->OTA_FW_status.new_size = FW->FW_len;
	            FLS_header->OTA_FW_status.CRC = FW->CRC;
	            memset(FLS_header->OTA_FW_status.new_ver, 0 , sizeof(FLS_header->OTA_FW_status.new_ver));
	            memcpy(FLS_header->OTA_FW_status.new_ver, FW->version, strlen((char*)FW->version));
	            memset(FLS_header->OTA_FW_status.old_ver, 0 , sizeof(FLS_header->OTA_FW_status.old_ver));
	            memcpy(FLS_header->OTA_FW_status.old_ver, strFWver, strlen((char*)strFWver));
	            to_saved = true;
	            download_process();
	        }
	        else{
	            if(strcmp((char*)FLS_header->OTA_FW_status.new_ver, (char*)FW->version) != 0){
	                goto FW_INIT;
	            }
	            if(FLS_header->OTA_FW_status.status == DL_downloading) download_process();
	            if(FLS_header->OTA_FW_status.status != DL_verifying && FLS_header->OTA_FW_status.status != DL_complete) goto FW_INIT;
	        }
	    }
	    else if(FW_result == CK_OTA_demand_no){
	        if(FLS_header->OTA_FW_status.status != DL_dile){
	            FLS_header->OTA_FW_status.status = DL_dile;
	            to_saved = true;
	        }
	    }

	    UARTprintf("FLS result = %s\n", FW_CHECK_RESULT[FLS_result]);
	    if(New_FLS){
	        free(New_FLS->version);
	        free(New_FLS->URL);
	        free(New_FLS->Note);
	        free(New_FLS);
	        New_FLS = NULL;
	    }

	    if(FLS_result == CK_OTA_demand_yes || FLS_result == CK_OTA_demand_must){
	        UARTprintf("New version : %s\nURL : %s\nNote : %s\nCRC : %d\nlen : %d\n",  FLS->version, FLS->URL, FLS->Note, FLS->CRC, FLS->FW_len);
	        New_FLS = FLS;
	        if(FLS_header->OTA_FLS_IMAGE_status.status == DL_dile){
	            FLS_INIT:
	            FLS_header->OTA_FLS_IMAGE_status.status = DL_preparing;
	            FLS_header->OTA_FLS_IMAGE_status.index = 0;
	            memset(FLS_header->OTA_FLS_IMAGE_status.new_ver, 0 , sizeof(FLS_header->OTA_FLS_IMAGE_status.new_ver));
	            memcpy(FLS_header->OTA_FLS_IMAGE_status.new_ver, FLS->version, strlen((char*)FLS->version));
	            memset(FLS_header->OTA_FLS_IMAGE_status.old_ver, 0 , sizeof(FLS_header->OTA_FLS_IMAGE_status.old_ver));
	            memcpy(FLS_header->OTA_FLS_IMAGE_status.old_ver, FLS_header->version, strlen((char*)FLS_header->version));
	            to_saved = true;
	            image_copy_handler();
	        }
	        else{
	            if(strcmp((char*)FLS_header->OTA_FLS_IMAGE_status.new_ver, (char*)FLS->version) != 0){
	                goto FLS_INIT;
	            }
	            if(FLS_header->OTA_FLS_IMAGE_status.status == DL_preparing){
	                image_copy_handler();
	            }
	            else if(FLS_header->OTA_FLS_IMAGE_status.status == DL_downloading){
	                if(FLS_header->OTA_FW_status.status == DL_complete){
	                    get_new_LAYOUT();
	                }
	            }
	            else if(FLS_header->OTA_FLS_IMAGE_status.status == DL_complete){
	                if(FLS_header->OTA_FW_status.status == DL_complete){
#if AWS_IOT_SERVICE
	                    OTA_progress.status = OTA_status_ready;
	                    event_callback(OTA_event_ready_to_upgrade);
#else
	                    if(FLS_header->update_ID.ID1 == 0 || FLS_header->update_ID.ID2 == 0){
	                        get_Upgrade_ID(strFWver, info->SNP, info->Time_zone, get_update_ID_handle);
	                    }
	                    else{
	                        OTA_progress.status = OTA_status_ready;
	                        event_callback(OTA_event_ready_to_upgrade);
	                    }
#endif
	                }
	            }
	        }
	    }
	    else if(FW_result != CK_OTA_demand_no){
	        if(FLS_header->OTA_FLS_IMAGE_status.status != DL_dile){
	            FLS_header->OTA_FLS_IMAGE_status.status = DL_dile;
	            to_saved = true;
	        }
	        if(FLS_header->OTA_FW_status.status == DL_complete){
#if AWS_IOT_SERVICE
	            OTA_progress.status = OTA_status_ready;
	            event_callback(OTA_event_ready_to_upgrade);
#else
	            if(FLS_header->update_ID.ID1 == 0 || FLS_header->update_ID.ID2 == 0){
	                get_Upgrade_ID(strFWver, info->SNP, info->Time_zone, get_update_ID_handle);
	            }
	            else{
	                OTA_progress.status = OTA_status_ready;
	                event_callback(OTA_event_ready_to_upgrade);
	            }
#endif
	        }
	    }

	    if(OTA_progress.status == OTA_status_preparing){
	        event_callback(OTA_event_new_FW_available);
	    }

	    if(to_saved){
	        ExtFls_set_header(FLS_header, sizeof(FlashHeader_t));
	    }
	}
}


void image_copy_handler(){
	uint32_t from,to;
	static bool copying = false;
	static uint32_t copy_len = 0;
	LAYOUT_INDEX_t* old_layout = ExtFls_get_layout();

	if(FLS_header->OTA_FLS_IMAGE_status.status == DL_preparing){
		uint8_t buf[FLS_COPY_LEN];
		if(copying){
			header_modify_time++;
			FLS_header->OTA_FLS_IMAGE_status.index += copy_len;
			if(FLS_header->OTA_FLS_IMAGE_status.index == (old_layout->IMAGE_sector_LCM_len+ old_layout->IMAGE_sector_Printer_len)){
				FLS_header->OTA_FLS_IMAGE_status.status = DL_downloading;
				FLS_header->OTA_FLS_IMAGE_status.index = 0;
				copying = false;
				OTA_DEBUG("image copying finished");
				ExtFls_set_header(FLS_header, sizeof(FlashHeader_t));
				header_modify_time = 0;
				if(FLS_header->OTA_FW_status.status == DL_complete){
					get_new_LAYOUT();
				}
				return;
			}
		}

		copy_len = FLS_COPY_LEN;

		from = (FLS_header->image_mode == 0)? old_layout->IMAGE_add : old_layout->IMAGE2_add;
		to = (FLS_header->image_mode == 0)? old_layout->IMAGE2_add : old_layout->IMAGE_add;

		ExtFlsRead(from + FLS_header->OTA_FLS_IMAGE_status.index, buf, copy_len);
		copying = ExtFlsWrite_nonblocking(to + FLS_header->OTA_FLS_IMAGE_status.index,  buf, copy_len, image_copy_handler);
		if(copying == false){
			OTA_DEBUG("image copy fail");
		}
	}

}

void OTA_service_init(void(*event_cb)(OTA_EVENT_T)){
	if(OTA_progress.status == OTA_status_unchecked){
	SYS_INFO* info = Get_system_info();
	service_initailed = true;
	event_callback = event_cb;
	//read flash to check previous status
	//check FW version again, in case there is newer version to be download.
	FLS_header = ExtFls_get_header();
	OTA_progress.status = OTA_status_checking;
	memset(&FLS_layout, 0, sizeof(FLS_layout));
#if AWS_IOT_SERVICE
			AWS_IoT_FW_check(strFWver, FW_check_rslt_handle);
#else
			FW_check(strFWver, info->SNP, FW_check_rslt_handle );
#endif
}
}

void OTA_service_poll(){
	static uint8_t CRC = 0;
	static uint32_t CRC_index = 0;
	uint16_t i;
	LAYOUT_INDEX_t* old_layout = ExtFls_get_layout();
	SYS_INFO* info = Get_system_info();

	if(service_initailed == false) return;

	if(header_modify_time > 20){	//this is to reduce the access of flash
		ExtFls_set_header(FLS_header, sizeof(FlashHeader_t));
		header_modify_time = 0;
	}

	if(FLS_header->OTA_FW_status.status == DL_verifying){	//verifying new FW
		if(CRC_index != FLS_header->OTA_FW_status.new_size){
			uint8_t buf[1024];
			uint32_t len = ((CRC_index+1024) <= FLS_header->OTA_FW_status.new_size)? 1024:(FLS_header->OTA_FW_status.new_size-CRC_index);
			ExtFlsRead(old_layout->OTA_BUF_ADD + CRC_index, buf, len);
			for(i = 0; i < len ; i++){
				CRC ^= buf[i];
			}
			CRC_index += len;
		}
		else{
			if(CRC == FLS_header->OTA_FW_status.CRC){
				FLS_header->OTA_FW_status.status = DL_complete;
				FLS_header->OTA_New_FW_add = old_layout->OTA_BUF_ADD;
				FLS_header->OTA_New_FW_size = FLS_header->OTA_FW_status.new_size;
				OTA_DEBUG("FW CRC verified!");
				if(FLS_header->OTA_FLS_IMAGE_status.status == DL_dile){
#if AWS_IOT_SERVICE
					OTA_progress.status = OTA_status_ready;
					event_callback(OTA_event_ready_to_upgrade);
#else
					if(FLS_header->update_ID.ID1 == 0 || FLS_header->update_ID.ID2 == 0){
						get_Upgrade_ID(strFWver, info->SNP, info->Time_zone, get_update_ID_handle);
					}
					else{
						OTA_progress.status = OTA_status_ready;
						event_callback(OTA_event_ready_to_upgrade);
					}
#endif
				}
				else if(FLS_header->OTA_FLS_IMAGE_status.status == DL_downloading){
					if(OTA_progress.status == OTA_status_preparing) get_new_LAYOUT();
				}
			}
			else{
				//download fail
				FLS_header->OTA_FW_status.status = DL_dile;
				//it will start to download again, at next boot time.
				char buf[100];
				sprintf(buf, "FW CRC error %d <-> %d", CRC, FLS_header->OTA_FW_status.CRC);
				OTA_DEBUG(buf);
				OTA_progress.status = OTA_status_error;
				event_callback(OTA_event_internet_unstable);
			}
			ExtFls_set_header(FLS_header, sizeof(FlashHeader_t));
			header_modify_time = 0;
		}
	}
	else{
		CRC_index = 0;
		CRC = 0;
	}
}

void OTA_service_refresh(){
	SYS_INFO* info = Get_system_info();
	if(OTA_progress.status == OTA_status_lastest || OTA_progress.status == OTA_status_error || OTA_progress.status == OTA_status_check_fail){
		//check FW version again
#if AWS_IOT_SERVICE
		AWS_IoT_FW_check(strFWver, FW_check_rslt_handle);
#else
		FW_check(strFWver, info->SNP, FW_check_rslt_handle );
#endif
	}
}

bool OTA_service_upgrade_start(){
	FlashHeader_t* FLS_header = ExtFls_get_header();
	LAYOUT_INDEX_t* LAYOUT = ExtFls_get_layout();
	if(OTA_progress.status == OTA_status_ready){
		//open internal BAT switch
		//IntBat_SW_ON;
		//set flags and update flash header
		if(FLS_header->OTA_FLS_IMAGE_status.status == DL_complete){
			FLS_header->image_mode = (FLS_header->image_mode == 0)? 1:0;
			UARTprintf("change image mode to %d\n", FLS_header->image_mode);
			memcpy(FLS_header->version, FLS_header->OTA_FLS_IMAGE_status.new_ver, sizeof(FLS_header->version));
		}
		FLS_header->OTA_update_flag = 1;
		FLS_header->update_ID_send_flag = 1;
		FLS_header->OTA_New_FW_size = FLS_header->OTA_FW_status.new_size;
		FLS_header->OTA_New_FW_add = LAYOUT->OTA_BUF_ADD;
		memset(&FLS_header->OTA_FW_status, 0 , sizeof(FLS_header->OTA_FW_status));
		memset(&FLS_header->OTA_FLS_IMAGE_status, 0 , sizeof(FLS_header->OTA_FLS_IMAGE_status));

		ExtFls_set_header(FLS_header, sizeof(FlashHeader_t));
		//jump to boot-loader
		Jump_to_booloader_OTA();
		return true;
	}
	return false;
}

OTA_Progress_t* OTA_service_get_progress(){
	return &OTA_progress;
}

void Jump_to_booloader_OTA(void){
	SYS_INFO* info = Get_system_info();
	if(!info->LcdBacklightConfig) info->LcdBacklightConfig = info->main_LcdBacklightConfig;
	save_system_info(&info->LcdBacklightConfig, sizeof(info->LcdBacklightConfig));

	USBDCDTerm(0);	//release USB device, so USB can be used by DFU.
	HWREG(NVIC_DIS0) = 0xffffffff;
	HWREG(NVIC_DIS1) = 0xffffffff;
	wifi_config(false, NULL);
	UARTprintf("Jump to Bootloader\n");
	delay_ms(5);
	(*((void (*)(void))(*(unsigned long *)0x30)))(); // jump to boot loader.
}

void get_update_ID_handle(bool result, UPGRADE_ID_t* ID){
	if(result){
		FlashHeader_t* FH = ExtFls_get_header();
		UARTprintf("update ID : %u, %u\n", ID->ID1, ID->ID2);
		//FH->OTA_update_flag = 1;
		FH->update_ID = *ID;
		//FH->update_ID_send_flag = 1;
		ExtFls_set_header(FH, sizeof(FlashHeader_t));
		free(ID);
		OTA_progress.status = OTA_status_ready;
		event_callback(OTA_event_ready_to_upgrade);
	}
	else{
		OTA_DEBUG("get upgrade ID fail");
		OTA_progress.status = OTA_status_error;
		event_callback(OTA_event_internet_unstable);
	}
}

char* get_bootloader_version(){
	char* (*get_ver)(void);
	uint32_t* temp = (uint32_t *)0x1C;
	char* ver = NULL;
	get_ver = (char*(*)(void))(*temp);

	if(*get_ver){
		ver = get_ver();
		UARTprintf(ver);
	}
	else{
		UARTprintf("bootloader version NULL\n");
	}
	return ver;
}

