//*****************************************************************************
//
// Source File: SystemInfosvs.h
// Description: system parameter, mode, and error informations.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 02/27/18     Jerry.W	       Created file.
// 10/17/18     Henry.Y        Add battery test AGM JIS parameter.
// 11/19/18     Henry.Y        Add MFC test data record and function.
// 03/20/19     Henry.Y        Add new LCM contrast parameter adjustment(match HW bias1/9, \5x booster).
// 03/20/19     Henry.Y        Add test record in system parameter data struct and pre_printed in system mode.
// 03/29/19     Henry.Y        Change the contrast default from 40 to 36.
// =============================================================================
//
//
// $Header: //depot/Tester/pb/service.h#0 $
// $DateTime: 2017/09/26 17:08:19 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#ifndef SERVICE_SYSTEMINFOSVS_H_
#define SERVICE_SYSTEMINFOSVS_H_


#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>
#include "driverlib/sysctl.h"
#include "driverlib/interrupt.h"
#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "driverlib/rom.h"
#include "driverlib/eeprom.h"
#include "driverlib/timer.h"
#include <string.h>
#include "../driver/utilities.h"
#include "../driver/st7565_lcm.h"
#include "cloud.h"
#include "testrecordsvc.h"


#define SNC_LEN 12
#define SNP_LEN 20



#define SYSTEM_INFO_MAP_ADD 0
#define SYSTEM_PAR_MAP_ADD 600
#define SYSTEM_QR_MAP_ADD 1024
#define SYSTEM_MFC_ADD 1536



#define Voltage_Coefficient_AD6L	778		//6V LOW POINT 1.5V AD
#define Voltage_Coefficient_AD6H	3667	//8V HIGH POINT 7V AD
#define Voltage_Coefficient_V6L		1.511	//6V LOW 1.5V
#define Voltage_Coefficient_V6H		7.128	//6V HIGH 7V

#define Voltage_Coefficient_AD8L	638		//8V LOW POINT 1.5V AD
#define Voltage_Coefficient_AD8H	3677	//8V HIGH POINT 9V AD
#define Voltage_Coefficient_V8L		1.500	//8V LOW 1.5V
#define Voltage_Coefficient_V8H		9.000	//8V HIGH 9V

#define Voltage_Coefficient_AD12L	387		//12V LOW POINT 1.5V AD
#define Voltage_Coefficient_AD12H	3972	//12V HIGH POINT 15 AD
#define Voltage_Coefficient_V12L	1.511	//12V LOW 1.5V
#define Voltage_Coefficient_V12H	15.531	//12V HIGH 15V

#define Voltage_Coefficient_AD24L	192		//24V LOW POINT 1.5V AD
#define Voltage_Coefficient_AD24H	3867	//24V HIGH POINT 31 AD
#define Voltage_Coefficient_V24L	1.511	//24V LOW 15V
#define Voltage_Coefficient_V24H	30.304	//24V HIGH 31.0V

#define Voltage_Coefficient_AIN16	0.002438	//Preset voltage calibration value(9V battery)
#define CCA_Coefficient_6V			1.000	//CCA default calibration value (6V)
#define CCA_Coefficient_8V			1.000	//CCA default calibration value (8V)
#define CCA_Coefficient_12V			1.000	//CCA default calibration value (12V)

#define BodyTempCoe_Default			1.026	//Temperature Sensor body: default calibration value
#define ObjTempCoe_Default			0.03	//Temperature Sensor obj:  default calibration value

#define VMCalCoeADL					522		//Voltage meter AD low voltage 1.5V
#define VMCalCoeADH					2884	//Voltage meter AD high voltage 40V

#define VMCalCoeVL					5.461	//Voltage meter vol low voltage 1.5V
#define VMCalCoeVH					30.140	//Voltage meter vol high voltage 40V

#define AMCalCoeVL                  0		//Amper Meter low voltage 5mv
#define AMCalCoeVH                  500     //Amper Meter high voltage 100mv

#define AMCalCoeADL                 2042		//Amper Meter low AD
#define AMCalCoeAdH                 2861     //Amper Meter high AD

#define IRCalCoe					1.000	//Internal Resistance Calibration

#define LCDContrast					20		//LCD Contrast default value

#define bias9bter5x_LCDcontrast_default	\
									36		//bias9 5x booster LCD Contrast default value

#define LCDRRatio					0x05	//LCD Contrast default value

#define VLoadLowV					1.569	//VLoad LOW 1.5V
#define VLoadHighV					15.908	//VLoad LOW 15V

#define VLoadLowVAD					398		//VLoad LOW 1.5V AD
#define VLoadHighVAD				4032	//VLoad LOW 15V AD

#define TROffset					0		//Test Records Offset
#define SetCapacity_default			500

#define SOFT_T_IDLE_COUNTER				120	//SOFT TIMER COUNTER 120 * 1SEC
#define SOFT_T_BUTTON_COUNTER			3	//SOFT TIMER COUNTER 3 * 1SEC


#define TESTCODE_LEN 16


#define SSID_LEN 32
#define PW_LEN 32


#pragma pack(push)
#pragma pack(4)

typedef struct{
	uint8_t		Version[20];
	uint8_t		Language_Select;
	uint8_t		SNC[SNC_LEN];
	uint8_t		SNP[SNP_LEN];
	uint8_t		LCDRratio;
	uint8_t 	LCDcontrast;
	uint8_t		LcdBacklightConfig;// for bootloader

	uint8_t		WIFIPR;
	uint8_t 	pre_test_state;
	uint8_t		BTChkTimes;
	int16_t		Time_zone;

	uint16_t	AD_1_5_6;
	uint16_t 	AD_7;
	float		V6_L;
	float		V6_H;
	uint16_t	AD_1_5_8;
	uint16_t	AD_9;
	float		V8_L;
	float		V8_H;
	uint16_t	AD_1_5_12;
	uint16_t	AD_15;
	float		V12_L;
	float		V12_H;
	uint16_t	AD_1_5_24;
	uint16_t	AD_31;
	float		V24_L;
	float		V24_H;
	float		ADC_to_BAT_voltage;
	float		CCA_Adjustment_6V;
	float		CCA_Adjustment_12V;
	float 		CCA_Adjustment_12V_Low;
	float		CCA_Adjustment_12V_High;
	float		CCA_TestV_Low;
	float		CCA_TestV_High;
	float		BodyTempCoe;
	float 		ObjTempCoe;

	float		VMCalLAD;
	float		VMCaHLAD;
	float		VMCalLvol;
	float		VMCaHLvol;
	float		AMCalLAD;
	float		AMCalHAD;
	float		AMCalLvol;
	float		AMCalHvol;

	float		IRCalCoe6V;
	float		IRCalCoe12V;

	float		VLoadVL_PLUS;
	float		VLoadVH_PLUS;
	float		VLoadADL_PLUS;
	float		VLoadADH_PLUS;
	float		VLoadVL_MINS;
	float		VLoadVH_MINS;
	float		VLoadADL_MINS;
	float		VLoadADH_MINS;

	//for 3-wire compensation
	float		_3W_12IRH;
	float		_3W_12IRL;
	float		_3W_12IRH_COM;
	float		_3W_12IRL_COM;

	float		_3W_6IRH;
	float		_3W_6IRL;
	float		_3W_6IRH_COM;
	float		_3W_6IRL_COM;

	uint16_t	SSTestCounter;
	uint16_t	BatTestCounter;
	uint16_t 	SysTestCounter;
	uint16_t	TotalTestRecord;
	uint16_t	TestRecordOffset;
	uint16_t	TotalSYSrecord;
	uint16_t	SYSrecordOffset;
	uint16_t	IRTestCounter;

	char 	    VkybdTextEditStr[5][17];
	uint8_t     Has_DIY;
	uint8_t		main_LcdBacklightConfig;// for main function

	uint8_t 	device_registered;	//0-> no, 1-> yes.
	uint8_t 	bias9bter5x_LCDcontrast; //main version V03.A and after LCM contrast setting(with bias1/9 and \5x booster)

}SYS_INFO;

typedef struct{
	uint8_t		BatteryType;
	uint8_t		SelectRating;
	uint8_t		SelectRating_last;
	uint16_t	BTSetCapacityVal;
	uint16_t	BTJIS_RgBatSelect;
	uint16_t    BTJIS_AGMBatSelect;
	uint16_t	BTSetRCVal;
    uint8_t     PackTestNum;
    uint8_t     PackTestCount;
    uint8_t     Temp_Range;

}Batter_test_t;

typedef struct{
	uint8_t		SSBatteryType;
	uint8_t		SSSelectRating;
	uint8_t		SSSelectRating_last;
	uint16_t	SS_JIS_EFB_Select;
	uint16_t	SS_JIS_AGM_Select;
	uint16_t	SSSetCapacityVal;
	uint16_t 	SSSetRCVal;
}SS_test_t;

typedef struct{
	uint8_t		Version[20];

	Batter_test_t	BAT_test_par;

	SS_test_t		SS_test_par;

	uint8_t			VIN_str[VININ_LEN1];
	uint8_t			RO_str[ROIN_LEN];

	uint8_t			MainMenu;
	uint8_t 		WHICH_TEST;


	uint8_t 		upload_rslt_seqID;	    //uploading test result sequence ID (with AWS IoT shadow)
	uint8_t 		FW_check_seqID;			//new FW check sequence ID (with AWS IoT shadow)
	uint8_t 		OTA_rslt_seqID;			//upload OTA result sequence ID (with AWS IoT shadow)
	uint8_t 		register_rslt_seqID;	//registering sequence ID (with AWS IoT shadow)
	uint8_t			JIS_BAT_STRING[10];
	union{
	    BATTERY_SARTSTOP_RECORD_T   Bat_SS_test;
	    SYSTEM_TEST_RECORD_T        System_test;
		IR_TEST_RECORD_T			IR_test;
	}record;
}SYS_PAR;

/******************************************************************************
*	struct name : QR_data
*	struct items : set :  0: all blank, 1: QRcode saved.
*	struct items : data_width & data_height => data[data_height][data_width]
*******************************************************************************/
typedef struct{
	uint8_t set;
	uint8_t data_width;
	uint8_t data_height;
}QR_data;


#pragma pack(pop)


#pragma pack(push)
#pragma pack(1)
typedef struct{
	uint8_t		start_up;//0: fail, 1:pass, 99:no test yet
	uint8_t 	switch_left;
	uint8_t 	switch_up;
	uint8_t 	switch_right;
	uint8_t 	switch_down;
	uint8_t 	switch_enter;
	uint8_t 	backlight_check_blue;
	uint8_t 	backlight_check_green;
	uint8_t 	backlight_check_orange;
	uint8_t 	backlight_check_red;
	uint8_t 	voltage_6V;
	uint8_t 	voltage_12V;
	uint8_t 	voltage_24V;
	uint8_t 	voltage_int;
	uint8_t 	voltage_VM;
	uint8_t 	voltage_AM;
	uint8_t 	clock_IC;
		//uint8_t flash_SPI;
	uint8_t 	step_motor;
	uint8_t 	load_on;
	uint8_t		SSID[SSID_LEN];
	uint8_t		SSID_PW[PW_LEN];
}MFC_TD;
#pragma pack(pop)



typedef struct{
	uint16_t		SetCapacityVal;
	uint8_t			TestInVehicle;
	uint8_t 		Isa6VBattery;
	uint8_t 		IsBatteryCharged;
	uint8_t			SYSTEM_CHANNEL;
	uint8_t			Code_test;
	
	uint8_t 		SelClampConnMeth;
	uint8_t 		Brand_sel;
	uint8_t 		Model_sel;
	uint8_t 		Version_sel;

	bool 			Surface_charge;
	bool 			System_stop;

	uint16_t		SetRCVal;
	uint8_t			RCJudge_Result;
	uint16_t		Measured_RC;
	int16_t			JISCapSelect;

	float 			IRvoltage;
	float 			IR;
	float 			CrankVol;
	float 			Crank_vmin;
	uint8_t			CrankVResult;
	uint8_t 		AltLoadVResult;
	float 			IdleVol;
	uint8_t 		AltIdleVResult;
	float 			ripple;
	uint8_t 		RipVResult;
	float 			LoadVol;

	float			ObjTestingTemp;
	float			BodyTestingTemp;
	float			SysObjTemp;

	float 			battery_test_voltage;
	float			SOH;
	float			SOC;
	float			Measured_CCA;
	uint8_t			Judge_Result;

	uint8_t			TestCode[TESTCODE_LEN];

	uint16_t 		year;
	uint8_t  		month;
	uint8_t 	 	date;
	uint8_t  		day;
	uint8_t  		hour;
	uint8_t  		min;
	uint8_t  		sec;

	uint8_t         PackTestNum;         // 20200814 DW EDIT
	uint8_t         PackTestCount;       // 20200817 DW EDIT


}TEST_DATA;





typedef enum {
	//USB_mode_err 	= 0x00000001,
	No_paper 		= 0x00000001 << 1,
	Clamp_err		= 0x00000001 << 2,
	Power_low		= 0x00000001 << 3, 
	Voltage_low		= 0x00000001 << 4,
	Voltage_High	= 0x00000001 << 5,
	load_error		= 0x00000001 << 6,
	Memory_error	= 0x00000001 << 7,
	Parameter_error = 0x00000001 << 8,
	Voltage_low_mem	= 0x00000001 << 9,
	Data_uninstall	= 0x00000001 << 10,
	Temp_over_range = 0x00000001 << 11,
	printer_error	= 0x00000001 << 12,
	power_warning	= 0x00000001 << 13,
}ERROR_Type;


typedef enum {
	manual_test,
	WiFiSet,
	USB_mode,
	engineer_mode,
	MFC,
	pre_tested,
	pre_printed,
	bootup_check,
	
}System_Mode;



/*************************************************************************************************
 * @ Name		: Get_system_info
 * @ Brief 		: Get system information pointer.
 *************************************************************************************************/
SYS_INFO* Get_system_info(void);

/*************************************************************************************************
 * @ Name		: save_system_parameter
 * @ Brief 		: save changed parameter into EEPROM.
 * @ Return 	: 0 for success, 1 for power error, 2 for memory error, 3 for parameter error
 * @ example 	:
				SYS_INFO* temp = Get_system_info();
				temp->Static_Battery_IS_Recharge++;

				save_system_info(&temp->Static_Battery_IS_Recharge, sizeof(temp->Static_Battery_IS_Recharge));
 *************************************************************************************************/
bool save_system_info(void* par, uint32_t par_len);


/*************************************************************************************************
 * @ Name		: Get_test_data
 * @ Brief 		: Get shared testing data pointer.
 *************************************************************************************************/
TEST_DATA* Get_test_data(void);


/*************************************************************************************************
 * @ Name		: Get_system_parameter
 * @ Brief 		: Get system parameter pointer.
 * @ Return 	: 0 for success, 1 for power error, 2 for memory error
 *************************************************************************************************/
bool system_parameter_init();



/*************************************************************************************************
 * @ Name		: Get_system_parameter
 * @ Brief 		: Get system parameter pointer.
 *************************************************************************************************/
SYS_PAR* Get_system_parameter(void);

/*************************************************************************************************
 * @ Name		: save_system_parameter
 * @ Brief 		: save changed parameter into EEPROM.
 * @ Return 	: 0 for success, 1 for power error, 2 for memory error, 3 for parameter error
 * @ example 	: 
				SYS_PAR* temp = Get_system_parameter();
				temp->Language_Select = Japanese;
				
				save_system_parameter(&temp->Language_Select, sizeof(temp->Language_Select));
 *************************************************************************************************/
bool save_system_parameter(void* par, uint32_t par_len);


/*
bool save_QRcode(QR_data *QR_setting, uint8_t *data);

QR_data* get_QRcode_format(void);

bool get_QRcode_row(uint8_t row, uint8_t *buf);
*/

/*************************************************************************************************
 * @ Name		: Get_MFC_TEST_DATA
 * @ Brief 		: Get MFC test data pointer.
 *************************************************************************************************/
MFC_TD* Get_MFC_TEST_DATA(void);

/*************************************************************************************************
 * @ Name		: save_MFC_TEST_DATA
 * @ Brief 		: save MFC test data into EEPROM.
 * @ Return 	: 0 for success, 1 for power error, 2 for memory error, 3 for parameter error
 * @ example 	: 
				MFC_TD* temp = Get_MFC_TEST_DATA();
				temp->start_up = 0;// 0: fail, 1: pass, 99: not test yet
				
				save_MFC_TEST_DATA(&temp->start_up, sizeof(temp->start_up));
 *************************************************************************************************/
bool save_MFC_TEST_DATA(void* par, uint32_t par_len);

void set_system_mode(System_Mode mode);
System_Mode get_system_mode(void);



void set_system_error(ERROR_Type err);
void clear_system_error(ERROR_Type err);
ERROR_Type get_system_error(void);



bool MFC_clear_eeprom_version(void);


#endif /* SERVICE_SYSTEMINFOSVS_H_ */
