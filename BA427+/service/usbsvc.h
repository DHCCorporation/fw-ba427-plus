//*****************************************************************************
//
// Source File: usbsvc.h
// Description: Prototype of Service Function
//				(1). USBSyncTestRecToPC()
//					- Synchronize Tester Record to PC Software via USB
//				(2). USBLCDScreenUpdate()
//					- Language Index
//					- Screen Index
//					- Full screen Hex Code
//				(3). USBPrinterFieldUpdate()
//					- Language Index
//					- Printer Field Index
//					- Each printer field Hex code
//				(4). USBMapAutoTestProg()
//					- Bi-direction
//						- PC send command to Device for CCA load-on test (to simulate Button(key-press) operation) via USB.
//						- Tester Device report the CCA/SOC/SOH/MAP Judge result back to PC tool via USB.
//				(5). USBUIFlowTestProg() - Option
//					- Bi-direction
//						- PC send command to Device for UI flow Auto-Test (to simulate Button(key-press) operation) via USB.
//						- Tester Device report the UI screen Hex code back to PC tool via USB.
//				(6). USBMfcVoltCalibration() �VOption
//				(7). USBMfcCCACalibration() �VOption
//				(8). USBMfcVMCalibration() �VOption
//				(9). USBMfcAMCalibration() �VOption
//				(10). USBMfcTempCalibration() �VOption
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 07/15/15     William.H      Created file.
// 10/01/15     William.H      Re-arrange/remove Global varaible's usage due to we've used stack to behave the manu level selection.
//                             Respective global varaible by the respective widget file to be maintained by their own.
//                             To integrate all the BUTTON_PRESSED event: SELECT_BUTTON/UP_BUTTON/DOWN_BUTTON/LEFT_BUTTON/RIGHT_BUTTON, and respective control function into Widget Manager.c file.
// 10/05/15     William.H      Check in BT2020 project which branched from LCDFontMaker(Pre-BT2020) by modifed related GPIOs.
// 10/19/15     Vincent.T      1. Add LCD service function call;
//                             2. Add printout update
//                             3. add lots of functions(focus on lcd & printout).
// 11/18/15     Vincent.T      1. Add Tested records upload function
// 12/23/15     Vincent.T      1. Modify some bugs when allocateing/freeing memory
//                             2. Add USB printout read func.
// 12/29/15     Vincent.T      1. Add USB DFU function.
// 03/24/16     Henry.Y        1. Add external voltage calibration func.
//                             2. Add internal voltage calibration func.
//                             3. Global variables: voltage calibration related. 
// 03/22/16     Vincent.T      1. Add VM, AM auto-Kmode
// 04/15/16     Vincent.T      1. Add IR Vload auto-Kmode
// 06/28/16     Vincent.T      1. New mode USB_TRDEL(Test Record delete) and new enum USB_TRDEL_MODE
// 08/10/16     Vincent.T      1. New mode: KCCA, KTEMP and KBLOAD(and all related sub mode)
// 11/21/16     Vincent.T      1. New mode: USB_SN, USB_SN_WRITE(write S/N into internal EEPROM)
//                             2. Define maximum length of Serial Number SN_LEN as 20
//                             3. New Func. USBSNPC(); to return S/N to PC
// 12/30/16     Vincent.T      1. Modify marco of VLPOINT_LEN 8 -> 9(due to H/L K-Point);
// 03/22/17     Vincent.T      1. Modify USB_MODE();(clock-ic check and bluetooth check)
//                             2. new function: USBTime(); -> read clock-ic data(second)
// 11/19/18     Henry.Y        Add WIFI and scanner command length return.
// 29/07/20     David Wang     1. add usb_flash uprade
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/service/usbsvc.h#5 $
// $DateTime: 2017/09/22 10:27:40 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#ifndef __USBSVC_H__
#define __USBSVC_H__


#include <math.h>
#include <stdbool.h>
#include <stdint.h>
#include "stdlib.h"
#include "stdio.h"
#include "driverlib/rom.h"
#include "driverlib/usb.h"
#include "../driver/uartstdio.h"
#include "..\driver/a25L032_fls.h"
#include "..\driver/st7565_lcm.h"
#include "..\driver/btuart_iocfg.h"
#include "..\driver/mem_protect.h"
#include "..\driver/usb_serial_structs.h"
#include "lcdsvc.h"
#include "extrtcsvc.h"
#include "flashsvc.h"
#include "vmsvc.h"
#include "amsvc.h"
#include "testrecordsvc.h"
#include "printersvc.h"
#include "battcoresvc.h"
#include "tempsvc.h"
#include "cloud.h"
#include "WordString.h"
extern uint8_t *strFWver;


//*****************************************************************************
//USB MODE
//*****************************************************************************
typedef enum
{
	USB_READY = 0,	//Ready
	USB_PRT_UP,		//Printout database update
	USB_PRT_READ,	//Printout database read
	USB_LCD_UP,		//LCD Database update
	USB_LCD_READ,	//LCD Database read
	USB_SEND_DAT,	//Test records upload
	USB_EXTV_CAL,   //External voltage calibration
	USB_INTV_CAL,   //Internal voltage calibration
	USB_DFU,		//Device Firmware Update
	USB_KVM,		//Voltage Meter Calibration
	USB_KAM,		//Amper Meter Calibration
	USB_KVL,		//Vload Calibration
	USB_TRDEL,		//Test Record Delete
	USB_KTEMP,		//Temperature Calibration
	USB_KCCA,		//CCA Calibration
	USB_KIR,		//IR Calibration
	USB_SN,		//DHC smart lab Serial-Number Flash
	USB_TIME,		//Read Time
	USB_BUTTON,		//BUTTON OPERATION CHECK
	USB_DETECT_VOLTAGE,	//SCAN EXTERNAL VOLTAGE
	USB_WIFI,		//WIFI FUNCTION
	USB_SCANNER,    //SCANNER FUNCTION
    USB_FLASH,      //External flash
}USB_MODE;

//*****************************************************************************
//PRINTOUT SUB MODE
//*****************************************************************************
typedef enum
{
	SUSB_PRT_READY = 0, 	//Printout update ready
	SUSB_PRT_START,			//Printout update Start
	SUSB_PRT_DATA,			//Printout update Data
	SUSB_PRT_END			//Printout update End
}USB_PC_PRT_MODESUB;

//*****************************************************************************
//LCD SUB MODE
//*****************************************************************************
typedef enum
{
	SUSB_LCD_READY = 0,		//Lcd update ready
	SUSB_LCD_STRART,		//Lcd update start
	SUSB_LCD_DATA,			//Lcd update data
	SUSB_LCD_END			//Lcd update end
}USB_PC_LCD_MODESUB;



//*****************************************************************************
//USB CONNECTION STATUS
//*****************************************************************************
typedef enum
{
	USB_DISCONNECTED = 0,	//usb removed
	USB_CONNECTED			//usb plug-in
}USB_CONNECTION_STATUS;


typedef enum
{
	USB_SN_READY = 0,	//Serail-Number Ready
	USB_SN_C_WRITE,		//DHC Serail-Number Ready WRITE-IN
	USB_SN_P_WRITE		//PRODUCT Serail-Number WRITE-IN
}USB_SN_MODE;

typedef enum
{
	USB_KTIME_READY = 0,	//Time upload
	USB_KTIME_END
}USB_TIME_MODE;

typedef enum
{
	USB_WIFI_FUNCTION_CHECK_READY = 0,
	USB_WIFI_VER,
	USB_WIFI_LOOPBACK,
	USB_SSID_PW_WRITE
}USB_WIFI_MODE;

typedef enum
{
	USB_SCANNER_READY = 0,
	USB_SCANNER_TRiGGER,
	USB_SCANNER_GET_READING_MODE,
	USB_SCANNER_SCAN
}USB_SCANNER_MODE;


//***************
//USB DATA LENGTH
//***************
//USB MODE
#define USB_READY_LEN		30   // 1 > 30 20200729 dw edit usb upgart modify

//USB -> PRINTER MODE
#define USB_PRT_START_LEN	6	//'$'(Start) + 2(Language) + 2(Printerout Field Index) + '!'(End)
#define USB_PRT_UPDATE_LEN	98//'$'(Start) + 96(Content) + '!'(End)

//USB -> LCD MODE
#define USB_LCD_START_LEN	6	//'$'(Start) + 2(Language) + 2(LCD Screen Index) + '!'(End)
#define USB_LCD_UPDATE_LEN	2050//'$'(Start) + 2048(Content) + '!'(End)

//USB -> CALIBRATION MODE
#define USB_CAL_LEN 8 //SBT-1 format:9 -> BT2100 format: 1+8
//************************
//USB -> FLASH DATA LENGTH
//************************

#define FLASH_EIGHT_PAGE	2048
#define FLASH_FOUR_PAGE		1024

#define FLASH_SECTOR_SIZE        1024*4 //200729 DW DIT USB FLASH UPGRATE

//************************
//USB Voltage Meter Kmode
//************************
#define VMPOINT_LEN			8	//VL12345@

//************************
//USB Amper Meter Kmode
//************************
#define AMPOINT_LEN			8	//AL12345@

//************************
//USB Temperature Kmode
//************************
#define TMPOINT_LEN			7	//TB2455@

//********************************
//USB Load Resistor Voltage Kmode
//********************************
#define VLPOINT_LEN			9	//L3L01500@

//********************************
//USB Cold Cranking Amper Kmode
//********************************
#define CCAPOINT_LEN		7	//C10500@

//********************************
//Internal Resistor Kmode
//********************************
#define IRPOINT_LEN		7	//R15000@

//********************************
//Serial Number Flash
//********************************
#define SN_LEN_READY		2

//********************************
// WIFI function check
//********************************
#define WIFI_FUNC		32

//********************************
// scanner function check
//********************************
#define SCANNER_CMD_LEN	10


uint16_t USBStorageAllocater(uint8_t ui8USBMode, uint8_t ui8USBModeSub);
void USBSendByte(uint8_t *pui8DataBuff, uint32_t ui8DataLen);
uint8_t ASCII2HEX(uint8_t *ui8ASCII, uint8_t ui8DataLen);	//Note: DataLen Max

/**************************/
/* PRINTER UPDATE SERVICE */
/**************************/
void USBDataBufASCII2HEX(uint8_t *pui8DatBuffSrc, uint8_t *pui8DatBuffDest, uint32_t ui32DatLen);	//96Byte(ASCII) -> 48 Byte(HEX)
void USBDataMove(uint8_t *ui8DatBuffSrc, uint8_t *ui8DatBuffDest, uint32_t ui32DatLen);
void USBPrtExtMemICUpdate(uint8_t *ui8DatBuff, uint8_t langIdx, uint8_t ui8PrtFldIdx);

/**********************/
/* LCD UPDATE SERVICE */
/**********************/
void USBLCDScreenUpdate(LANGUAGE langIdx, uint8_t ui8ScrnIdx, uint8_t *lcdScrnBuff, uint32_t bufLen);
void USBLCDScreenRead(LANGUAGE langIdx, uint8_t ui8ScrnIdx);

/************************/
/* TESTED RECORD UPLOAD */
/************************/
void USBTestRecordUpload(void);

/************************/
/* VOLTAGE CALIBRATION */
/************************/
uint8_t USBVoltageCalibration(USB_MODE ui8USBMode, uint8_t* CMD);
uint8_t USBIRCCACalibration(USB_MODE ui8USBMode, uint8_t* CMD);

//************************
//
//Serial Number Return
//
//************************
void USBSNPC(uint8_t *serial_number, uint8_t len);

//**************************
//
// Read Time(s) and upload
//
//**************************
void USBTime(void);

void Jump_to_booloader_DFU(void);

void USB_svc_set_CB(void(*cb)(uint8_t*, uint32_t)); //set callback function replace old one

bool USBConnect(void);


#endif // __USBSVC_H__
