//*****************************************************************************
//
// Source File: battcoresvc.c
// Description: Battery Tester Core Functions' Definition.
//				
//
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 10/07/15     William.H      Created file.
// 10/08/15     Henry.Y        Remove 8V channel.
// 10/19/15     William.H      It��s porting the CCA Load-On and BT/SS test related APIs from SBT1_SC_V00 project to BT2020 service/battcoresvc.c.
// 10/21/15     Henry.Y        1. Replace the loop count by g_ui32SysClock in ROM_SysCtlDelay().
//                             2. Remove SYS_6V/SYS_12V/SYS_24V flag, use SYSTEM_CHANNEL to discriminate system voltage status.
//                             3. Get CCA and check clamps will use load on 2.
// 10/23/15     Henry.Y        To separate Check Clamps(BattCoreChkClamp) and Check Cable(BattCoreChkCable).
// 11/03/15     Vincent.T      1. Add new global variable to access cca setting
//                             2. Modified all SOC calculation coefficient
// 11/04/15     Vincent.T      1. Add stable CCA procedure
//                             2. SYSTEM_CHANNEL -> all refer to VOL_CH
//                             3. CCA Rating conversion marcos .c -> .h
// 11/05/15     Vincent.T      1. Add CCAValConvert() body;
// 11/06/15     Vincent.T      1. Integrate CCA convert(battery type) in BattCoreCCAUnitTransform() into BatJudgeMap()
// 01/07/16     Vincent.T      1. Re-arrange BattCoreScanVoltage()
// 01/19/16     William.H      1. Add ACK response in BattCoreScanVoltage() func if voltage is high.
//                             2. Porting BattCheckCable() func from SBT-1.
//                             3. Add ACK resp in BattCoreChkClamp() func about the status of check_clamp & check_cable.
// 01/19/16     Vincent.T      1. Add JIS in BattCoreCCAUnitTransform();
// 01/21/16     William.H      Add cranking volt detect function - BattCoreCrankingVolt() in system test.
// 01/27/16     Henry.Y        1. Add global variable: g_bCheck_clamps
//                             2. Modify variable 'CCA_show' declared type 'uint8_t' to 'uint16_t'.
// 02/02/16     Vincent.T      Modify BattCoreCrankingVolt();
// 02/03/16     Vincent.T      Add void int AltIdleVoltTest(float *fVol); body.
// 02/04/16     Vincent.t      1. Add SysRipTest(float *pfvol) to test ripple voltage while doing system test.
//                             2. Add AltLoadVoltTest() func. to test ALT. load voltage.
// 02/18/16     Vincent.T      1. Add Bluetooth + cranking volt func.
// 03/09/16     Vincent.T      1. Add BattCoreGetVLoad(); body.
//                             2. Add void BattCoreGetIR(); body.
//                             3. Add new global variables gfVloadCalCoe, gfIRCalCoe.
// 03/24/16     Henry.Y        1. Modify BattCoreScanVoltage(): Transform variable type when calculate the voltage.
//                             2. Modify BattCoreChkIntBatt(): Add adc and voltage test parameter. 
//                             3. Modify BattCoreGetVoltage(): Add adc test parameter.
// 04/14/16     Vincent.T      1. Modify sample number for more preciser.
//                             2. Modify BattCoreGetIR();
// 04/15/16     Vincent.T      1. New global variables gVLoadVL, gVLoadVH, gVLoadADL and gVLoadADH to correct Vload.
//                             2. Add BattCoreGetVLoadAD() body to get Vload AD(K-IR)
// 04/27/16     Vincent.T      1. Modify ADC_sample_counter
//                             2. Modify FECTALL_6V, FECTALL_12V
//                             3. Modify BattCoreScanVoltage(); for linearly voltage performance.
// 05/19/16     Vincent.T      1. global variable gbSysTemp
// 05/20/16     Vincent.T      1. Modify AltIdleVoltTest(); & AltLoadVoltTest; due to measure testing temperature.
// 06/14/16     Vincent.T      1. Modify Cable length coefficient
//                             2. New BattCoreChkIntBatFast(); to get internal voltage quickly.
//                             3. Modyfy BattCoreGetCCA();(2400 -> 3600 -> Load Error)
//                             4. Modify IR coefficent.
// 06/21/16     Vincent.T      1. New marcos Bat_threshold_Print to set "Internal Battery low" threshold when printing.
//                             2. Modify BattCoreChkIntBatFast(); voltage threshold.
// 06/22/16     Vincent.T      1. Cancel check cable func.(avoid 24V load on)
// 06/24/16     Vincent.T      1. Modify Threshold value of internal battery voltage low and printing powe low voltage value.
// 08/10/16     Vincent.T      1. #include <math.h>
//                             2. Modify BattCoreGetCCA(); using new cca equation.
// 08/24/16     Vincent.T      1. Modify CCA & IR Coe.(New PCB)
//                             2. Make 6V and 12V both coefficients are related.
// 12/02/16     Vincent.T      1. global variables for 3-wire compensation.(gf12IRH, gf12IRL, gf12IRH_COM and gf12IRL_COM)
//                             2. Modify BattCoreGetCCA(); 1. Average more times of IR calculation; 2. IR linear compensation.
//                             3. Modify BattCoreGetIR(); for 3-wire compensation;(Diffenent K-method)
// 12/08/16     Vincent.T      1. Modify BattCoreGetCCA();(6V CCA coefficient)
// 12/30/16     Vincent.T      1. Modify BattCoreGetCCA(); reduce average times.
//                             2. new global variables Vload Plus and Minus.
//                             3. Modify BattCoreGetVLoad() -> BattCoreGetVLoad_PLUS();, BattCoreGetVLoad_MINS(); and BattCoreGetVLoad();
//                             4. Modify BattCoreGetIR(); due to Vload_PLUS and Vload_MINS.
// 01/06/17     Vincent.T      1. global variables for 3-wire compensation.(gf6IRH, gf6IRL, gf6IRH_COM and gf6IRL_COM)
//                             2. Modify BattCoreGetCCA(); for 3-wire compensation;(6v focus)
// 02/10/17     Vincent.T      1. New marco to delay 0.25s
//                             2. ADC_sample_counter 100 -> 200(voltage more stable while load on)
//                             3. New global variables CCA_Adjustment_12V_Low and CCA_Adjustment_12V_High(12V Three-Wire CCA Compensation)
//                             4. Modify BattCoreGetCCA(); due to 12V Three-Wire CCA Compensation
// 02/17/17     Vincent.T      1. New global variables CCA_TestV_Low and CCA_TestV_High(for 3-wire 12V cca compensattion)
//                             2. Body of new function BattCoreGetVoltageCCA();
//                             3. Modify BattCoreGetCCA(); for 3-wire 12V cca compensation.
// 03/01/17     Vincent.T      1. Re-dfein FECTALL_12V
//                             2. Modify average times of BattCoreGetVoltageCCA();
//                             3. Modify BattCoreGetCCA();(3-wire)
// 03/30/17     Vincent.T      1. Modify BattCoreScanVoltage(); add voltage maximum value.
//                             2. Add CCA max. value.
// 09/26/17		Jerry.W		   1. remove the use of gbSysTemp to reduce the usage of extern variable
//							   2. fix declaration error of fSysObjTemp. (change type from bool to flaot)
// 09/28/17		Jerry.W		   remove BT command process
// 11/06/17     Henry.Y	       1. Hide useless variables: CCA_Adjustment_8V/voltage_over_32/g_ucPre_check_cable
//                             2. Modify 24V voltage test value upper bound to 32 in BattCoreScanVoltage().
//                             3. Remove surface charge judgment in BattCoreGetVoltage().
//                             4. Hide useless function BattCoreGetVoltageCCA()
//                             5. Arrange BattCoreChkClamp().
//                             6. BattCoreGetCCA(): apply single load/dual load test, add new load error condition IR<0.8
//                             7. BattCoreGetIR(): add new load error condition Vload_MINS >= Vload_PLUS, set 3-wire and kelvin connection formula.
// 11/22/17     Henry.Y        Move test range limitation after temperature compensation.
// 11/23/17     Jerry.W        1. change Bat_threshold_Print to privent printer mode from power low shut down
// 02/14/18     Henry.Y	       BattCoreCalcSOH() modified: force trans variable types when calculating.
// 05/03/18     Henry.Y        add EN1 and EN2.
// 09/11/18     Henry.Y        Use systeminfo data struct to replace the global variables.
//                             Use system driver interface to replace global variable.
// 10/17/18     Henry.Y 	   Decrease delay time in BattCoreJudgeRipple().
// 03/20/19     Henry.Y        Add new warning threshold in BattCoreChkIntBatt().
// 03/29/19     Henry.Y        Add error flag setup if the internal power under the threshold.
// 07/31/20     David Wang     AltLoadVoltTest vol specification modify 12.5v > 13.4v
// 20201016     David Wang     Rating add CA/MCA in BattCoreCCAUnitTransform()
// 20201127     David Wang     VNoDetect = 0.03v to change 0.00v for bosch need diesel enginge
//
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/service/battcoresvc.c#1 $
// $DateTime: 2017/11/27 10:38:22 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#include <stdbool.h>
#include <stdint.h>
#include <math.h>
#include "inc/hw_memmap.h"
#include "driverlib/adc.h"
#include "driverlib/rom.h"
#include "driverlib/timer.h"
#include "../driver/uartstdio.h"
#include "battcoresvc.h"
#include "..\driver/battcore_iocfg.h"
#include "..\driver/btuart_iocfg.h"
#include "../driver/delaydrv.h"
#include "SystemInfosvs.h"


//*****************************************************************************
//
// Declare Timer2A global timeout flag(30 sec) for cranking voltage detect in system test.
//
//*****************************************************************************
extern bool g_bCranking_timeout;

// CABLE RESISTACE FOR IR COMPENSATION
float cable_resistance = 0;

//*****************************************************************************
//
// surface charge threshold definition
//
//*****************************************************************************
typedef struct{
    BATTERYTYPE bat_type;
    float surface_voltage;
}SURFACE_DEFINE_T;

static const SURFACE_DEFINE_T battery_SurfVol[] = { {FLOODED, FLOODED_SURFACE_VOL},
                                                                   {AGM_FLAT_PLATE, AGMF_SURFACE_VOL},
                                                                   {AGM_SPIRAL, AGMS_SURFACE_VOL},
                                                                   {VRLA_GEL, VRLA_SURFACE_VOL},
                                                                   {EFB,EFB_SURFACE_VOL} };

bool Surface_charge_detect(BATTERYTYPE Battype, VOL_CH ch, float fvol)
{
    uint16_t _size = sizeof(battery_SurfVol)/sizeof(battery_SurfVol[0]);
    if(ch > MAP_12V || Battype >= _size){
        UARTprintf("Surface_charge_detect para err\n");
        while(1);
    }
    uint8_t i;
    float sc_vol = 0;
    for(i = 0; i < _size; i++){
        if(battery_SurfVol[i].bat_type == Battype){
            sc_vol = battery_SurfVol[i].surface_voltage;
            break;
        }
    }

    if(ch == MAP_6V) sc_vol /= 2;

    return ((fvol >= sc_vol)? 1:0);
}


//*****************************************************************************
//
// IR/CCA temperature compensation definition
//
//*****************************************************************************
typedef struct{
	float Temp_threshold;
    float compensation;
}TemperatureCopm_t;

static const TemperatureCopm_t TempCom[] = { { 10.0, 1.00},
                                             {  2.0, 1.03},
                                             { -2.0, 1.09},
                                             { -7.0, 1.13},
                                             {-12.0, 1.17},
                                             {-17.0, 1.20},
                                             {-99.0, 1.23} };

void CCATempComp(float *CCA, float fTemp)
{
	/* BA327_2200_CL */
    if(fTemp <= 2){
        (*CCA) *= 1.2;
    }
    else if(fTemp>2 && fTemp <=4){
        (*CCA) *= 1.1;
    }
    else{
        // LECEL5~LEVEL11 don't care
    }


    /* BA427_2200_CL
    uint8_t Sequence_num = sizeof(TempCom)/sizeof(TemperatureCopm_t);
    uint8_t i;

	for(i = 0; i < Sequence_num; i++){
		if(fTemp >= TempCom[i].Temp_threshold){
            (*CCA) *= TempCom[i].compensation;
            break;
		}

	// Clore CCA compensate to DHC
    (*CCA) *= 1.06;

	}
	*/
	return;
}

void IRTempComp(float *IR, float fTemp)
{
	uint8_t Sequence_num = sizeof(TempCom)/sizeof(TemperatureCopm_t);
    uint8_t i;

	for(i = 0; i < Sequence_num; i++){
		if(fTemp >= TempCom[i].Temp_threshold){
            (*IR) /= TempCom[i].compensation;
            break;
		}
	}
	return;
}


void BattCoreScanVoltage(float *voltage)
{
	uint32_t voltage_buffer[3] = {0};
	uint32_t voltage_acc[3] = {0};
	uint16_t loop_index = 0;
	TEST_DATA* TD = Get_test_data();
	SYS_INFO* info = Get_system_info();

	//UARTprintf("BattCoreScanVoltage()\n");

	//
	// 6V CHANNEL
	//
	ROM_ADCSequenceDisable(ADC0_BASE, 1);
	ROM_ADCSequenceConfigure(ADC0_BASE, 1, ADC_TRIGGER_PROCESSOR, 0);
	ROM_ADCSequenceStepConfigure(ADC0_BASE, 1, 0, ADC_CTL_CH13 );	// 6V
	ROM_ADCSequenceStepConfigure(ADC0_BASE, 1, 1, ADC_CTL_CH14 );	// 12V
	ROM_ADCSequenceStepConfigure(ADC0_BASE, 1, 2, ADC_CTL_CH15 | ADC_CTL_IE | ADC_CTL_END);	//24V
	ROM_ADCSequenceEnable(ADC0_BASE, 1);

	for(loop_index = 0; loop_index < ADC_sample_counter; loop_index++)
	{
		ROM_ADCIntClear(ADC0_BASE, 1);
		ROM_ADCProcessorTrigger(ADC0_BASE, 1);

		while(!ROM_ADCIntStatus(ADC0_BASE, 1, false));

		ROM_ADCSequenceDataGet(ADC0_BASE, 1, voltage_buffer);

		//Data handler
		//6V
		voltage_acc[0] += voltage_buffer[0];
		//12V
		voltage_acc[1] += voltage_buffer[1];
		//24V
		voltage_acc[2] += voltage_buffer[2];

		//ROM_SysCtlDelay(1);
	}

	//Channel Selection
	voltage_buffer[0] = (voltage_acc[0] / ADC_sample_counter );
	voltage_buffer[1] = (voltage_acc[1] / ADC_sample_counter );
	voltage_buffer[2] = (voltage_acc[2] / ADC_sample_counter );

	//6V
	if(voltage_buffer[0] <= 4000)
	{
		TD->SYSTEM_CHANNEL = MAP_6V;
		*voltage = ((info->V6_H-info->V6_L)/(info->AD_7 - info->AD_1_5_6))*((float)*voltage_buffer - info->AD_1_5_6) + info->V6_L;
	}
	//12V
	else if(voltage_buffer[1] <= 4000)
	{
		TD->SYSTEM_CHANNEL = MAP_12V;
		*voltage = ((info->V12_H-info->V12_L)/(info->AD_15 - info->AD_1_5_12))*((float)*(voltage_buffer + 1) - info->AD_1_5_12) + info->V12_L;
	}
	//24V
	else
	{
		TD->SYSTEM_CHANNEL = MAP_24V;
		*voltage = ((info->V24_H-info->V24_L)/(info->AD_31 - info->AD_1_5_24))*((float)*(voltage_buffer + 2) - info->AD_1_5_24) + info->V24_L;
		if(*voltage > 32.0)
		{
			*voltage = 32.00;
		}
	}




}


int BattCoreChkIntBatt(uint32_t *bat_voltage_acc, float *bat_voltage)
{
	uint32_t bat_voltage_buffer = 0x00;
	uint8_t loop_index = 0;
	SYS_INFO* info = Get_system_info();

	*bat_voltage_acc = 0x00;
	*bat_voltage = 0;

	ROM_ADCSequenceDisable(ADC0_BASE, 1);
	ROM_ADCSequenceStepConfigure(ADC0_BASE, 1, 0, ADC_CTL_CH5 | ADC_CTL_IE | ADC_CTL_END);
	ROM_ADCSequenceEnable(ADC0_BASE, 1);

	for(loop_index = 0; loop_index < ADC_BAT_sample; loop_index++)
	{
		ROM_ADCIntClear(ADC0_BASE, 1);
		ROM_ADCProcessorTrigger(ADC0_BASE, 1);

		while(!ROM_ADCIntStatus(ADC0_BASE, 1, false));

		ROM_ADCSequenceDataGet(ADC0_BASE, 1, &bat_voltage_buffer);
		*bat_voltage_acc += bat_voltage_buffer;
		bat_voltage_buffer = 0;
	}

	*bat_voltage_acc = *bat_voltage_acc/ADC_BAT_sample;
	*bat_voltage = *bat_voltage_acc*info->ADC_to_BAT_voltage;

	if(*bat_voltage < Bat_threshold_low){
		set_system_error(Power_low);
		return 1;
	}
//	else if(*bat_voltage < Bat_threshold_warn){
//		if((power_warning & get_system_error()) == false) set_system_error(power_warning);
//		return 2;
//	}
	else{
		if(Power_low & get_system_error()) clear_system_error(Power_low);
		if(power_warning & get_system_error()) clear_system_error(power_warning);
		return 0;
	}
}

bool BattCoreChkIntBatFast(void)
{
	float fIntBat = 0.0;

	uint32_t ui32BatBuf = 0x00;
	uint32_t ui32BatBufAcc = 0x00;
	uint8_t loop_index = 0;
	SYS_INFO* info = Get_system_info();

	ROM_ADCSequenceDisable(ADC0_BASE, 3);
	ROM_ADCSequenceConfigure(ADC0_BASE, 3, ADC_TRIGGER_PROCESSOR, 0);
	ROM_ADCSequenceStepConfigure(ADC0_BASE, 3, 0, ADC_CTL_CH5 | ADC_CTL_IE | ADC_CTL_END);
	ROM_ADCSequenceEnable(ADC0_BASE, 3);

	for(loop_index = 0; loop_index < 1; loop_index++)
	{
		ROM_ADCIntClear(ADC0_BASE, 3);
		ROM_ADCProcessorTrigger(ADC0_BASE, 3);
		while(!ROM_ADCIntStatus(ADC0_BASE, 3, false));
		ROM_ADCSequenceDataGet(ADC0_BASE, 3, &ui32BatBuf);
		ui32BatBufAcc += ui32BatBuf;
	}

	ui32BatBufAcc = ui32BatBufAcc/1;
	fIntBat = ui32BatBufAcc*info->ADC_to_BAT_voltage;

	if(fIntBat < Bat_threshold_Print)
	{
		return true;
	}
	else
	{
		return false;
	}
}


void BattCoreGetVoltage(uint32_t *voltage_acc, float *voltage)
{
	uint32_t voltage_buffer = 0x00;
	uint16_t loop_index = 0;
	float voltage_accu = 0;
	TEST_DATA* TD = Get_test_data();
	SYS_INFO* info = Get_system_info();

	*voltage = 0;

	//
	// 6V channel 
	//
	if(TD->SYSTEM_CHANNEL == MAP_6V)
	{	
		ROM_ADCSequenceStepConfigure(ADC0_BASE, 1, 0, ADC_CTL_CH13 | ADC_CTL_IE | ADC_CTL_END);
		ROM_ADCSequenceEnable(ADC0_BASE, 1);

		for(loop_index = 0; loop_index < ADC_sample_counter; loop_index++)
		{
			ROM_ADCIntClear(ADC0_BASE, 1);
			ROM_ADCProcessorTrigger(ADC0_BASE, 1);

			while(!ROM_ADCIntStatus(ADC0_BASE, 1, false));

			ROM_ADCSequenceDataGet(ADC0_BASE, 1, &voltage_buffer);
			ROM_SysCtlDelay(1);

			voltage_accu += voltage_buffer;
		}

		voltage_accu = (voltage_accu/ADC_sample_counter);
		*voltage = ((info->V6_H-info->V6_L)/(info->AD_7 - info->AD_1_5_6))*(voltage_accu - info->AD_1_5_6) + info->V6_L;

	}	
	//
	// 12V channel
	//
	else if(TD->SYSTEM_CHANNEL == MAP_12V)
	{
		ROM_ADCSequenceStepConfigure(ADC0_BASE, 1, 0, ADC_CTL_CH14 | ADC_CTL_IE | ADC_CTL_END);
		ROM_ADCSequenceEnable(ADC0_BASE, 1);

		for(loop_index = 0; loop_index < ADC_sample_counter; loop_index++)
		{
			ROM_ADCIntClear(ADC0_BASE, 1);
			ROM_ADCProcessorTrigger(ADC0_BASE, 1);

			while(!ROM_ADCIntStatus(ADC0_BASE, 1, false));

			ROM_ADCSequenceDataGet(ADC0_BASE, 1, &voltage_buffer);
			ROM_SysCtlDelay(1);

			voltage_accu += voltage_buffer;
		}

		voltage_accu = (voltage_accu/ADC_sample_counter);
		*voltage = ((info->V12_H-info->V12_L)/(info->AD_15 - info->AD_1_5_12))*(voltage_accu - info->AD_1_5_12) + info->V12_L;

	}
	//
	// 24V channel
	//
	else if(TD->SYSTEM_CHANNEL == MAP_24V)
	{
		ROM_ADCSequenceStepConfigure(ADC0_BASE, 1, 0, ADC_CTL_CH15 | ADC_CTL_IE | ADC_CTL_END);
		ROM_ADCSequenceEnable(ADC0_BASE, 1);

		for(loop_index = 0; loop_index < ADC_sample_counter; loop_index++)
		{
			ROM_ADCIntClear(ADC0_BASE, 1);
			ROM_ADCProcessorTrigger(ADC0_BASE, 1);

			while(!ROM_ADCIntStatus(ADC0_BASE, 1, false));

			ROM_ADCSequenceDataGet(ADC0_BASE, 1, &voltage_buffer);
			ROM_SysCtlDelay(1);

			voltage_accu += voltage_buffer;
		}

		voltage_accu = (voltage_accu/ADC_sample_counter);
		*voltage = ((info->V24_H-info->V24_L)/(info->AD_31 - info->AD_1_5_24))*(voltage_accu - info->AD_1_5_24) + info->V24_L;
	}
	else
		while(1);//debug

	*voltage_acc = voltage_accu;
}



int BattCoreChkClamp(void)
{
	float check_v = 0.00;
	//char strc[4] = {0x00};
	//uint32_t vol = 0;
	uint32_t ui32Voltage_scale = 0;

	//
	// check clamp voltage
	//
	BattCoreGetVoltage(&ui32Voltage_scale, &check_v);


	if(check_v < check_clamp_v)
	{
		//UARTprintf("Check Clamps, External Voltage = %d%d.%d%dV\n",strc[0],strc[1],strc[2],strc[3]);
		set_system_error(Clamp_err);
		return 1;
	}
	else
	{
		clear_system_error(Clamp_err);
		return 0;
	}
}

int BattCoreGetCCA(float *IR, float *CCA,uint8_t Load_Times)//0:load error
{
	UARTprintf("BattCoreGetCCA(). Load_Times = %d.\n", Load_Times);

	uint8_t ui8LopIdx = 0;
	uint8_t load_switch = dual_load_with_result;
	float fIR = 0;
	float fIRsum = 0;
	TEST_DATA* TD = Get_test_data();
	SYS_INFO* info = Get_system_info();

    /*if(TD->SYSTEM_CHANNEL == MAP_12V){
        if(BattCoreGetIR(&fIR, 1, load_switch)){
            if(fIR > ThreewireTh){
                load_switch = single_load_with_result;
            }
        }
        else return 0;
    }*/

	for(ui8LopIdx = 0; ui8LopIdx < Load_Times; ui8LopIdx++){
		if(BattCoreGetIR(&fIR, 1, load_switch)){
			fIRsum = fIRsum + fIR;
		}
		else return 0;
	}

	fIR = fIRsum / (float)Load_Times;
    if(IR) (*IR) = fIR;

	//
	// IR lower limitation
	//
	if(fIR < 0.8)//IR lower bound 0.8 mini ohm
	{
		// Just in case
		return 0;
	}
	else
	{
		if(TD->SYSTEM_CHANNEL == MAP_6V)
		{
			*CCA = EXP_a* (info->CCA_Adjustment_6V) * pow(fIR, EXP_pow);
			*CCA = (*CCA) * CCA_6V_COMP_C;
		}
		else if (TD->SYSTEM_CHANNEL == MAP_12V)
		{
			*CCA = EXP_a * (info->CCA_Adjustment_12V) * pow(fIR, EXP_pow);
		}
		else
		{
			return 0;
		}
	}

	/*if(*CCA > 3600)
	{
		return 0;
	}*/

	return 1;
}



int BattCoreJudgeRipple(float *delta_voltage_Ripple)
{
	float voltage_temp = 0;
	float voltage_max = 0.00;
	float voltage_min = 0.00;
	uint8_t ripple_judge_loop;
	uint32_t ui32Voltage_scale = 0;
	TEST_DATA* TD = Get_test_data();
	SYS_PAR* par = Get_system_parameter();

	if(TD->SYSTEM_CHANNEL == MAP_6V)
		voltage_min = 8.00;
	else if(TD->SYSTEM_CHANNEL == MAP_12V)
		voltage_min = 16.00;
	else if(TD->SYSTEM_CHANNEL == MAP_24V)
		voltage_min = 32.00;

	for(ripple_judge_loop = 0; ripple_judge_loop < 50; ripple_judge_loop++)
	{
		BattCoreGetVoltage(&ui32Voltage_scale, &voltage_temp);

		if(voltage_temp > voltage_max)	//GET MAX
		{
			voltage_max = voltage_temp;
		}


		if(voltage_temp  <voltage_min)	//GET MIN
		{
			voltage_min = voltage_temp;
		}

		delay_ms(2);
	}
	*delta_voltage_Ripple = voltage_max - voltage_min;


	// BT/SS/GC/ST-cranking monitor the stability of system
	if(par->WHICH_TEST == Battery_Test||par->MainMenu == Start_Stop_Test)
	{
		if((TD->SYSTEM_CHANNEL == MAP_6V && *delta_voltage_Ripple >= 0.1)||(TD->SYSTEM_CHANNEL == MAP_12V && *delta_voltage_Ripple >= 0.1)||(TD->SYSTEM_CHANNEL == MAP_24V && *delta_voltage_Ripple >= 0.2))
			return 1;
		else
			return 0;
	}
	else    // system test ripple test
	{
		//
		// delta_voltage > 0.03V(12V)0.06V(24V)? 	/* Use 10 for testing */
		//
		if((TD->SYSTEM_CHANNEL == MAP_6V && *delta_voltage_Ripple >= 0.06) || (TD->SYSTEM_CHANNEL == MAP_12V && *delta_voltage_Ripple >= 0.06) || (TD->SYSTEM_CHANNEL == MAP_24V && *delta_voltage_Ripple >= 0.12))
			return 1;
		else
			return 0;
	}
}


void BattCoreCCAUnitTransform(float *CCA, uint32_t bat_type, uint32_t rating, uint8_t JisDinNum)
{
    TEST_DATA* TD = Get_test_data();

    float CCA_temp = (*CCA);
	switch(rating){
	case SAE:
		*CCA = CCA_temp * SAE_to_CCA;
		UARTprintf("Rating: SAE\n");
		break;
	case EN1:
		*CCA = CCA_temp * SAE_to_EN1;
		UARTprintf("Rating: EN1\n");
		break;
	case EN2:
		*CCA = CCA_temp * SAE_to_EN2;
		UARTprintf("Rating: EN2\n");
		break;
	case IEC:
		*CCA = CCA_temp * SAE_to_IEC;
		UARTprintf("Rating: IEC\n");
		break;
	case JIS:
	    if(JisDinNum > 60 && JisDinNum < 69){
	        *CCA = CCA_temp * SAE_to_DIN;
	        UARTprintf("Rating: DIN\n");
	    }
		break;
	case DIN:
		*CCA = CCA_temp * SAE_to_DIN;
		UARTprintf("Rating: DIN\n");
		break;
	case CA_MCA:
    case MCA:
        *CCA = CCA_temp * SAE_to_CA;
        UARTprintf("Rating: CA\n");
        break;
	default:
		break;
	}
}

void BattCoreCalcSOH(float *SOH, float  CCA)
{
	uint16_t ui16temp = CCA;
	TEST_DATA* TD = Get_test_data();

	 if ( TD->PackTestNum > 1 && TD->PackTestCount == 0){

	     TD->SetCapacityVal = TD->SetCapacityVal * TD->PackTestNum ;
	 }

	*SOH = (float)ui16temp/TD->SetCapacityVal;
	*SOH = (*SOH) * 100;

	//***********************
	// Get cca setting value
	//***********************

	if((*SOH) > 100)
	{
		*SOH = 100;
	}
}

float BattCoreCalSoc(MAP_BATTERYTYPE Battype, float fvol, VOL_CH volch)
{
	float soch = 0.0;
//	float socm = 0.0;
	float socl = 0.0;
//	float soc_scale = 0.0;
//	float delta_vol = 0.0;
//	float sohoffset = 0;

	UARTprintf("BattCoreCalSoc(). batType = %d.\n", Battype);

	//************************
	//  Set soch, socm, socl
	//***********************
	switch(Battype)
	{
	case MAP_FLOODED:
		soch = 12.6;
//		socm = 12.2;
		socl = 11.8;
		break;
	case MAP_AGM_FLAT_PLATE:
		soch = 12.8; //12.9;
//		socm = 12.3;
		socl = 11.8;
		break;
	case MAP_AGM_SPIRAL:
		soch = 13.06; //12.9;
//		socm = 12.23;
		socl = 11.39; //11.8;
		break;
	case MAP_VRLA_GEL:
		soch = 12.8; //12.9;
//		socm = 12.3;
		socl = 11.8;
		break;
	case MAP_EFB:
		soch = 12.6; //12.8;
//		socm = 12.2;
		socl = 11.8;
		break;
	default:
		UARTprintf("Error!!!!! BattCoreCalSoc(). batType = %d.\n", Battype);
		while(1);//debug
	}

	//*****************
	// volch decition
	//*****************
	switch(volch)
	{
	case MAP_6V:
		fvol = fvol * 2;
		break;
	case MAP_12V:
		fvol = fvol * 1;
		break;
	default:
		UARTprintf("BattCoreCalSoc() ---> volch Error!\n");
		break;

	}

	if(fvol >= soch )
	{
		return 100;
	}
	else if(fvol <= socl)
	{
		return 0;
	}
	else
	{
	    return ((fvol-socl)/(soch-socl))*100;

	    /*
	    if(fvol >= socm)
		{
			//
			// SOC calculation procedure
			//
			switch(Battype)
			{
			case MAP_FLOODED:
				soc_scale = (50/(soch - socm));
				delta_vol = fvol - socm;
				sohoffset = 50;
				break;
			case MAP_AGM_FLAT_PLATE:
				soc_scale = (75/(soch - socm));
				delta_vol = fvol - socm;
				sohoffset = 25;
				break;
			case MAP_AGM_SPIRAL:
				soc_scale = (75/(soch - socm));
				delta_vol = fvol - socm;
				sohoffset = 25;
				break;
			case MAP_VRLA_GEL:
				soc_scale = (75/(soch - socm));
				delta_vol = fvol - socm;
				sohoffset = 25;
				break;
			case MAP_EFB:
				soc_scale = (50/(soch - socm));
				delta_vol = fvol - socm;
				sohoffset = 50;
				break;
			default:
				UARTprintf("Error!!!!! BattCoreCalSoc(). batType = %d.\n", Battype);
				while(1);//debug
			}
			return ((delta_vol * soc_scale) + sohoffset);
		}
		else
		{
			//
			// SOC calculation procedure
			//
			switch(Battype)
			{
			case MAP_FLOODED:
				soc_scale = (50/(socm - socl));
				delta_vol = fvol - socl;
				sohoffset = 0;
				break;
			case MAP_AGM_FLAT_PLATE:
				soc_scale = (25/(socm - socl));
				delta_vol = fvol - socl;
				sohoffset = 0;
				break;
			case MAP_AGM_SPIRAL:
				soc_scale = (25/(socm - socl));
				delta_vol = fvol - socl;
				sohoffset = 0;
				break;
			case MAP_VRLA_GEL:
				soc_scale = (25/(socm - socl));
				delta_vol = fvol - socl;
				sohoffset = 0;
				break;
			case MAP_EFB:
				soc_scale = (50/(socm - socl));
				delta_vol = fvol - socl;
				sohoffset = 0;
				break;
			default:
				UARTprintf("Error!!!!! BattCoreCalSoc(). batType = %d.\n", Battype);
				while(1);//debug
			}
			return ((delta_vol * soc_scale) + sohoffset);
		}
        */
	}

}

#if 1
static float CCA_rating_gain[] = {SAE_to_CCA, \
                                    SAE_to_CA, \
                                    SAE_to_EN2, \
                                    SAE_to_IEC, \
                                    SAE_to_CCA/*JIS*/,\
                                    SAE_to_DIN,\
                                    SAE_to_EN1,\
                                    SAE_to_CA};

void CCAValConvert(SELRATING CCARatingSrc, SELRATING CCARatingDest, uint16_t *pui16CCAVal)
{
	uint8_t Sequence_num = sizeof(CCA_rating_gain)/sizeof(float);
    uint8_t err = 0;
	if(Sequence_num != SELRATING_num) err = 1;
    if(pui16CCAVal == NULL) err = 2;
	if(CCARatingSrc >= SELRATING_num) err = 3;
	if(CCARatingSrc == JIS) err = 4;
	if(CCARatingDest >= SELRATING_num) err = 5;
	if(CCARatingDest == JIS) err = 6;
    if(err){
        UARTprintf("CCAValConvert err%d\n",err);
        while(err);
    }
    if(CCARatingSrc == CCARatingDest) return;
	
	float CCA_temp = (*pui16CCAVal);
	float gain = CCA_rating_gain[CCARatingDest];
	float divisor = CCA_rating_gain[CCARatingSrc];

	CCA_temp *= (gain/divisor);
	(*pui16CCAVal) = CCA_temp;
}


#else
void CCAValConvert(SELRATING CCARatingSrc, SELRATING CCARatingDest, uint16_t *pui16CCAVal)
{
	//******************************
	//     CCA Rating Src Handler
	//******************************
	UARTprintf("CCAValConvert()...\n");
	if(CCARatingSrc == SAE)
	{
		//******************************
		//     CCA Rating Dest Handler
		//******************************
		switch(CCARatingDest)
		{
		case SAE:
			//(*pui16CCAVal) = (*pui16CCAVal);
			break;
		case EN1:
			(*pui16CCAVal) = (float)(*pui16CCAVal) * SAE_to_EN1;
			break;
		case EN2:
			(*pui16CCAVal) = (float)(*pui16CCAVal) * SAE_to_EN2;
			break;
		case IEC:
			(*pui16CCAVal) = (float)(*pui16CCAVal) * SAE_to_IEC;
			break;
		case DIN:
			(*pui16CCAVal) = (float)(*pui16CCAVal) * SAE_to_DIN;
			break;
		case JIS:
			UARTprintf("CCAValConvert()...JIS Error!!!!!!!!!!!\n");
			break;
		default:
			UARTprintf("CCAValConvert()...Rating Error!!!!!!!!!!!\n");
			break;
		}
	}
	else if(CCARatingSrc == EN1)
	{
		//******************************
		//     CCA Rating Dest Handler
		//******************************
		switch(CCARatingDest)
		{
		case SAE:
			(*pui16CCAVal) = (float)(*pui16CCAVal) / SAE_to_EN1;
			break;
		case EN1:
			//(*pui16CCAVal) = (float)(*pui16CCAVal);
			break;
		case EN2:
			(*pui16CCAVal) = (float)(*pui16CCAVal) / SAE_to_EN1;
			(*pui16CCAVal) = (float)(*pui16CCAVal) * SAE_to_EN2;
			break;
		case IEC:
			(*pui16CCAVal) = (float)(*pui16CCAVal) / SAE_to_EN1;
			(*pui16CCAVal) = (float)(*pui16CCAVal) * SAE_to_IEC;
			break;
		case DIN:
			(*pui16CCAVal) = (float)(*pui16CCAVal) / SAE_to_EN1;
			(*pui16CCAVal) = (float)(*pui16CCAVal) * SAE_to_DIN;
			break;
		case JIS:
			UARTprintf("CCAValConvert()...JIS Error!!!!!!!!!!!\n");
			break;
		default:
			UARTprintf("CCAValConvert()...Rating Error!!!!!!!!!!!\n");
			break;
		}
	}
	else if(CCARatingSrc == EN2)
	{
		//******************************
		//     CCA Rating Dest Handler
		//******************************
		switch(CCARatingDest)
		{
		case SAE:
			(*pui16CCAVal) = (float)(*pui16CCAVal) / SAE_to_EN2;
			break;
		case EN1:
			(*pui16CCAVal) = (float)(*pui16CCAVal) / SAE_to_EN2;
			(*pui16CCAVal) = (float)(*pui16CCAVal) * SAE_to_EN1;
			break;
		case EN2:
			//(*pui16CCAVal) = (float)(*pui16CCAVal);
			break;
		case IEC:
			(*pui16CCAVal) = (float)(*pui16CCAVal) / SAE_to_EN2;
			(*pui16CCAVal) = (float)(*pui16CCAVal) * SAE_to_IEC;
			break;
		case DIN:
			(*pui16CCAVal) = (float)(*pui16CCAVal) / SAE_to_EN2;
			(*pui16CCAVal) = (float)(*pui16CCAVal) * SAE_to_DIN;
			break;
		case JIS:
			UARTprintf("CCAValConvert()...JIS Error!!!!!!!!!!!\n");
			break;
		default:
			UARTprintf("CCAValConvert()...Rating Error!!!!!!!!!!!\n");
			break;
		}
	}
	else if(CCARatingSrc == IEC)
	{
		//******************************
		//     CCA Rating Dest Handler
		//******************************
		switch(CCARatingDest)
		{
		case SAE:
			(*pui16CCAVal) = (float)(*pui16CCAVal) / SAE_to_IEC;
			break;
		case EN1:
			(*pui16CCAVal) = (float)(*pui16CCAVal) / SAE_to_IEC;
			(*pui16CCAVal) = (float)(*pui16CCAVal) * SAE_to_EN1;
			break;
		case EN2:
			(*pui16CCAVal) = (float)(*pui16CCAVal) / SAE_to_IEC;
			(*pui16CCAVal) = (float)(*pui16CCAVal) * SAE_to_EN2;
			break;
		case IEC:
			//(*pui16CCAVal) = (float)(*pui16CCAVal);
			break;
		case DIN:
			(*pui16CCAVal) = (float)(*pui16CCAVal) / SAE_to_IEC;
			(*pui16CCAVal) = (float)(*pui16CCAVal) * SAE_to_DIN;
			break;
		case JIS:
			UARTprintf("CCAValConvert()...JIS Error!!!!!!!!!!!\n");
			break;
		default:
			UARTprintf("CCAValConvert()...Rating Error!!!!!!!!!!!\n");
			break;
		}
	}
	else if(CCARatingSrc == JIS)
	{
		UARTprintf("CCAValConvert()...JIS Error!!!!!!!!!!!\n");
	}
	else if(CCARatingSrc == DIN)
	{
		//******************************
		//     CCA Rating Dest Handler
		//******************************
		switch(CCARatingDest)
		{
		case SAE:
			(*pui16CCAVal) = (float)(*pui16CCAVal) / SAE_to_DIN;
			break;
		case EN1:
			(*pui16CCAVal) = (float)(*pui16CCAVal) / SAE_to_DIN;
			(*pui16CCAVal) = (float)(*pui16CCAVal) * SAE_to_EN1;
			break;
		case EN2:
			(*pui16CCAVal) = (float)(*pui16CCAVal) / SAE_to_DIN;
			(*pui16CCAVal) = (float)(*pui16CCAVal) * SAE_to_EN2;
			break;
		case IEC:
			(*pui16CCAVal) = (float)(*pui16CCAVal) / SAE_to_DIN;
			(*pui16CCAVal) = (float)(*pui16CCAVal) * SAE_to_IEC;
			break;
		case DIN:
			//(*pui16CCAVal) = (float)(*pui16CCAVal);
			break;
		case JIS:
			UARTprintf("CCAValConvert()...JIS Error!!!!!!!!!!!\n");
			break;
		default:
			UARTprintf("CCAValConvert()...Rating Error!!!!!!!!!!!\n");
			break;
		}
	}
	else
	{
		UARTprintf("CCAValConvert()...Rating Error!!!!!!!!!!!\n");
	}
}
#endif


int BattCoreCrankingVolt(float *cranking_voltage, float *vmin)
{
	float delta_voltage1 = 0, delta_voltage2 = 0, delta_voltage3 = 0;
	float voltage1 = 0, voltage2 = 0, voltage0 = 0, voltage_min = 0, voltage_max = 0, voltage_temp = 0, voltage_V50 = 0;
	float cranking_volt_buffer = 0;
	unsigned char cranking_loop = 0;
	uint32_t ui32Voltage_scale = 0;
	TEST_DATA* TD = Get_test_data();

	//****************************************************************
	//
	// Reset g_bCranking_timeout to make sure every test must meet 30s
	// Reset g_bSystem_stop to make sure won't stop test every new test
	//
	//****************************************************************
	ROM_TimerDisable(TIMER2_BASE, TIMER_A);	// DISABLE TIMERA
	g_bCranking_timeout = 0;
	TD->System_stop = 0;

	// voltage threshold
	float delta_V1 = (TD->SYSTEM_CHANNEL == MAP_24V)? 0.2:0.1;
	float delta_V2 = (TD->SYSTEM_CHANNEL == MAP_24V)? 3.0:1.5;
	float delta_V4 = (TD->SYSTEM_CHANNEL == MAP_24V)? 0.3:0.15;
	*vmin = (TD->SYSTEM_CHANNEL == MAP_24V)? 32:16;
	voltage_min = (TD->SYSTEM_CHANNEL == MAP_24V)? 32:16;

	UARTprintf("BattCoreCrankingVolt()...\n");

	ROM_TimerEnable(TIMER2_BASE, TIMER_A);

	Cranking_test_redo:
	while(!g_bCranking_timeout)
	{
		if(TD->System_stop) // cancel by user
			break;

		BattCoreGetVoltage(&ui32Voltage_scale, &voltage1);	//* Get V1  
		DELAY_ZERO_ZERO_ZERO_TWO_SECOND;	// 2ms
		BattCoreGetVoltage(&ui32Voltage_scale, &voltage2);	//* Get V2  
		if(voltage2 < *vmin) *vmin = voltage2;

		if(voltage1 > voltage2)
			delta_voltage1 = (voltage1 - voltage2);
		else
			delta_voltage1 = 0;

		if(delta_voltage1 > delta_V1)	// condition1: delta_voltage>0.1V?
		{
			voltage0 = voltage1;	// record V0

			//get Vmin
			for(cranking_loop = 0; cranking_loop < 200; cranking_loop++)	// 0.25ms*200 = 50ms
			{
				BattCoreGetVoltage(&ui32Voltage_scale, &voltage_temp);

				if(voltage_temp < voltage_min)
					voltage_min = voltage_temp;

				DELAY_ZERO_ZERO_ZERO_ZERO_TWO_FIVE_SECOND;	// 0.00025s = 0.25ms
			}
			*vmin = voltage_min;

			if(voltage0 > voltage_min)
				delta_voltage2 = (voltage0 - voltage_min);
			else
				delta_voltage2 = 0;

			if(delta_voltage2 > delta_V2)	//condition2: V0-Vmin>1.5?
			{
				voltage_V50 = 0;	// reset

				// DELAY_ZERO_ZERO_FIVE_SECOND;	// 0.05s=50ms
				BattCoreGetVoltage(&ui32Voltage_scale, &voltage_V50);

				if(voltage_V50 > voltage_min){	// rising?				
					delta_voltage3 = (voltage_V50 - voltage_min);
				}				
				else{
					*vmin = voltage_V50;
					goto Cranking_test_redo;
				}

				if(delta_voltage2 > delta_voltage3)	// condition3: V0-Vmin>V50-Vmin
				{
					voltage_temp = 0;
					cranking_volt_buffer = 0;

					// delay 150ms
					delay_ms(150);

					if(TD->SYSTEM_CHANNEL == MAP_24V)
						voltage_min = 32.00;
					else
						voltage_min = 16.00;

					// get MAX & MIN *
					for(cranking_loop = 0; cranking_loop < 200; cranking_loop++)
					{
						BattCoreGetVoltage(&ui32Voltage_scale, &voltage_temp);

						if(voltage_temp > voltage_max)
						{
							voltage_max = voltage_temp;
						}

						if(voltage_temp < voltage_min)
						{
							voltage_min = voltage_temp;
						}

						cranking_volt_buffer += voltage_temp;

						delay_ms(1);
					}

					if((voltage_max > voltage_min) && ((voltage_max - voltage_min) > delta_V4))	// condition4: ripple>0.15V
					{
						cranking_volt_buffer = (cranking_volt_buffer/200);
						*cranking_voltage = cranking_volt_buffer;

						ROM_TimerDisable(TIMER2_BASE, TIMER_A);	//DISABLE TIMERA

						g_bCranking_timeout = false;

						//
						// Normoal or Low>
						//
						if(TD->SYSTEM_CHANNEL == MAP_24V)
						{
							if(*cranking_voltage >= CRANKVOLT_24V)
							{
								return NORMAL;
							}
							else
							{
								return LOW;
							}
						}
						else	//MAP_12V:
						{
							if(*cranking_voltage >= CRANKVOLT_12V)
							{
								return NORMAL;
							}
							else
							{
								return LOW;
							}
						}
					}
				}
			}			
		}
	}

	ROM_TimerDisable(TIMER2_BASE, TIMER_A);	// DISABLE TIMERA

	g_bCranking_timeout = false;
	*cranking_voltage = 0;
	return NO_DETECT;	// no detect
}


int AltIdleVoltTest(float *pfVol)
{
	UARTprintf("AltIdleVoltTest()...\n");

	//
	// Boundary defination
	//
	uint32_t ui32Voltage_scale = 0;
	TEST_DATA* TD = Get_test_data();

	float  VLow;
	float  VNormal;
    
    if(TD->SYSTEM_CHANNEL == MAP_24V){
        VLow = (TD->SysObjTemp >= 0)? ALT_IDLE_24V_VOL_LOW_ABOVE_0:ALT_IDLE_24V_VOL_LOW_BELOW_0;
        VNormal = (TD->SysObjTemp >= 0)? ALT_IDLE_24V_VOL_NORMAL_ABOVE_0:ALT_IDLE_24V_VOL_NORMAL_BELOW_0;
    }
    else{
        VLow = (TD->SysObjTemp >= 0)? ALT_IDLE_12V_VOL_LOW_ABOVE_0:ALT_IDLE_12V_VOL_LOW_BELOW_0;
        VNormal = (TD->SysObjTemp >= 0)? ALT_IDLE_12V_VOL_NORMAL_ABOVE_0:ALT_IDLE_12V_VOL_NORMAL_BELOW_0;
    }

	BattCoreGetVoltage(&ui32Voltage_scale, pfVol);

	//
	// Ranking
	//
	if(*pfVol >= VNormal)
	{
		return HIGH;
	}
	else if(*pfVol >= VLow)
	{
		return NORMAL;
	}
	else
	{
		return LOW;
	}
}

int SysRipTest(float *pfvol)
{
	float voltage_temp = 0;
	float voltage_max = 0.00;
	float voltage_min = 0.00;
	uint8_t ripple_judge_loop;
	uint32_t ui32Voltage_scale = 0;
	TEST_DATA* TD = Get_test_data();

	//************************
	//
	//     Set Boundary
	//
	//************************
	float VNoDetect;
	float VNormal;

	if(TD->SYSTEM_CHANNEL == MAP_6V)
	{
		voltage_min = 8.00;
		VNoDetect = RIPPLE_12V_ND;
		VNormal = RIPPLE_12V_NORMAL;
	}
	else if(TD->SYSTEM_CHANNEL == MAP_12V)
	{
		voltage_min = 16.00;
		VNoDetect = RIPPLE_12V_ND;
		VNormal = RIPPLE_12V_NORMAL;
	}
	else if(TD->SYSTEM_CHANNEL == MAP_24V)
	{
		voltage_min = 32.00;
		VNoDetect = RIPPLE_24V_ND;
		VNormal = RIPPLE_24V_NORMAL;
	}

	for(ripple_judge_loop = 0; ripple_judge_loop < 50; ripple_judge_loop++)
	{
		BattCoreGetVoltage(&ui32Voltage_scale, &voltage_temp);

		if(voltage_temp > voltage_max)	//GET MAX
		{
			voltage_max = voltage_temp;
		}


		if(voltage_temp  <voltage_min)	//GET MIN
		{
			voltage_min = voltage_temp;
		}

		delay_ms(5);
	}
	*pfvol = voltage_max - voltage_min;

	//
	// delta_voltage > 0.03V(12V)0.06V(24V)? 	/* Use 10 for testing */
	//
	//if((TD->SYSTEM_CHANNEL == MAP_6V && *pfvol >= 0) || (TD->SYSTEM_CHANNEL == MAP_12V && *pfvol >= 0) || (TD->SYSTEM_CHANNEL == MAP_24V && *pfvol >= 0))
	//{
		if((*pfvol) <= VNoDetect)
		{
			*pfvol = 0.0;
			return NO_DETECT;
		}
		else if((*pfvol) <= VNormal)
		{
			return NORMAL;
		}
		else
		{
			return HIGH;
		}
	//}
	//else
	//{
	//	*pfvol = 0.0;
	//	return NO_DETECT;
	//}
}

int AltLoadVoltTest(float *pfVol)
{
	UARTprintf("AltIdleVoltTest()...\n");

	//
	// Boundary defination
	//
	float  VLow;
	float  VNormal;
	uint32_t ui32Voltage_scale = 0;
	TEST_DATA* TD = Get_test_data();

	if(TD->SYSTEM_CHANNEL == MAP_24V){
        VLow = (TD->SysObjTemp >= 0)? ALT_LOAD_24V_VOL_LOW_ABOVE_0:ALT_LOAD_24V_VOL_LOW_BELOW_0;
        VNormal = (TD->SysObjTemp >= 0)? ALT_LOAD_24V_VOL_NORMAL_ABOVE_0:ALT_LOAD_24V_VOL_NORMAL_BELOW_0;        
	}
	else{
        VLow = (TD->SysObjTemp >= 0)? ALT_LOAD_12V_VOL_LOW_ABOVE_0:ALT_LOAD_12V_VOL_LOW_BELOW_0;
        VNormal = (TD->SysObjTemp >= 0)? ALT_LOAD_12V_VOL_NORMAL_ABOVE_0:ALT_LOAD_12V_VOL_NORMAL_BELOW_0;    
	}

	BattCoreGetVoltage(&ui32Voltage_scale, pfVol);

	//
	// Ranking
	//
	if(*pfVol >= VNormal)
	{
		return HIGH;
	}
	else if(*pfVol >= VLow)
	{
		return NORMAL;
	}
	else
	{
		return LOW;
	}
}

//*****************************************************************************
//
// Get differential voltage between load
//
//*****************************************************************************
void BattCoreGetVLoad_PLUS(float *pfVLoad, uint8_t ui8SampleNum)
{
	UARTprintf("BattCoreGetVLoad()...\n");

	uint32_t ui32VLoad = 0;
	uint32_t ui32VLoadSum = 0;
	uint8_t ui8LopIdx = 0;
	SYS_INFO* info = Get_system_info();

	ROM_ADCSequenceDisable(ADC0_BASE, 3);
	ROM_ADCSequenceConfigure(ADC0_BASE, 3, ADC_TRIGGER_PROCESSOR, 0);
	ROM_ADCSequenceStepConfigure(ADC0_BASE, 3, 0, ADC_VLOAD_PLUS_CH | ADC_CTL_IE | ADC_CTL_END);
	ROM_ADCSequenceEnable(ADC0_BASE, 3);

	//Acc
	for(ui8LopIdx = 0; ui8LopIdx < ui8SampleNum; ui8LopIdx++)
	{
		ROM_ADCIntClear(ADC0_BASE, 3);
		ROM_ADCProcessorTrigger(ADC0_BASE, 3);
		while(!ROM_ADCIntStatus(ADC0_BASE, 3, false));
		ROM_ADCSequenceDataGet(ADC0_BASE, 3, &ui32VLoad);
		ui32VLoadSum = ui32VLoadSum + ui32VLoad;
	}

	//Average
	ui32VLoadSum = ui32VLoadSum / ui8SampleNum;

	//
	// - AD to V
	//
	(*pfVLoad) = (((info->VLoadVH_PLUS - info->VLoadVL_PLUS) / (info->VLoadADH_PLUS -  info->VLoadADL_PLUS)) * (ui32VLoadSum - info->VLoadADL_PLUS)) + info->VLoadVL_PLUS;
}

void BattCoreGetVLoad_MINS(float *pfVload, uint8_t ui8SampleNum)
{
	UARTprintf("BattCoreGetVLoad()...\n");

	uint32_t ui32VLoad = 0;
	uint32_t ui32VLoadSum = 0;
	uint8_t ui8LopIdx = 0;
	SYS_INFO* info = Get_system_info();

	ROM_ADCSequenceDisable(ADC0_BASE, 3);
	ROM_ADCSequenceConfigure(ADC0_BASE, 3, ADC_TRIGGER_PROCESSOR, 0);
	ROM_ADCSequenceStepConfigure(ADC0_BASE, 3, 0, ADC_VLOAD_MINS_CH | ADC_CTL_IE | ADC_CTL_END);
	ROM_ADCSequenceEnable(ADC0_BASE, 3);

	//Acc
	for(ui8LopIdx = 0; ui8LopIdx < ui8SampleNum; ui8LopIdx++)
	{
		ROM_ADCIntClear(ADC0_BASE, 3);
		ROM_ADCProcessorTrigger(ADC0_BASE, 3);
		while(!ROM_ADCIntStatus(ADC0_BASE, 3, false));
		ROM_ADCSequenceDataGet(ADC0_BASE, 3, &ui32VLoad);
		ui32VLoadSum = ui32VLoadSum + ui32VLoad;
	}

	//Average
	ui32VLoadSum = ui32VLoadSum / ui8SampleNum;

	//
	// - AD to V
	//
	(*pfVload) = (((info->VLoadVH_MINS- info->VLoadVL_MINS) / (info->VLoadADH_MINS -  info->VLoadADL_MINS)) * (ui32VLoadSum - info->VLoadADL_MINS)) + info->VLoadVL_MINS;
}

void BattCoreGetVLoad(float *pfVload_P, float *pfVload_M, uint8_t ui8SampleNum)
{
	UARTprintf("BattCoreGetVLoad()...\n");

	uint32_t ui32VLoad[4] = {0x00};
	uint32_t ui32VLoadSum[4] = {0x00};
	uint8_t ui8LopIdx = 0;
	SYS_INFO* info = Get_system_info();

	ROM_ADCSequenceDisable(ADC0_BASE, 2);
	ROM_ADCSequenceConfigure(ADC0_BASE, 2, ADC_TRIGGER_PROCESSOR, 0);
	ROM_ADCSequenceStepConfigure(ADC0_BASE, 2, 0, ADC_VLOAD_MINS_CH);
	ROM_ADCSequenceStepConfigure(ADC0_BASE, 2, 1, ADC_VLOAD_PLUS_CH | ADC_CTL_IE | ADC_CTL_END);
	ROM_ADCSequenceEnable(ADC0_BASE, 2);

	//Acc
	for(ui8LopIdx = 0; ui8LopIdx < ui8SampleNum; ui8LopIdx++)
	{
		ROM_ADCIntClear(ADC0_BASE, 2);
		ROM_ADCProcessorTrigger(ADC0_BASE, 2);
		while(!ROM_ADCIntStatus(ADC0_BASE, 2, false));
		ROM_ADCSequenceDataGet(ADC0_BASE, 2, ui32VLoad);

		ui32VLoadSum[0] = ui32VLoadSum[0] + ui32VLoad[0];
		ui32VLoadSum[1] = ui32VLoadSum[1] + ui32VLoad[1];
		ui32VLoadSum[2] = ui32VLoadSum[2] + ui32VLoad[2];
		ui32VLoadSum[3] = ui32VLoadSum[3] + ui32VLoad[3];
	}

	//Average
	ui32VLoadSum[0] = ui32VLoadSum[0] / ui8SampleNum;
	ui32VLoadSum[1] = ui32VLoadSum[1] / ui8SampleNum;
	ui32VLoadSum[2] = ui32VLoadSum[2] / ui8SampleNum;
	ui32VLoadSum[3] = ui32VLoadSum[2] / ui8SampleNum;

	//
	// - AD to V
	//
	(*pfVload_M) = (((info->VLoadVH_MINS- info->VLoadVL_MINS) / (info->VLoadADH_MINS -  info->VLoadADL_MINS)) * (ui32VLoadSum[0] - info->VLoadADL_MINS)) + info->VLoadVL_MINS;
	(*pfVload_P) = (((info->VLoadVH_PLUS- info->VLoadVL_PLUS) / (info->VLoadADH_PLUS -  info->VLoadADL_PLUS)) * (ui32VLoadSum[1] - info->VLoadADL_PLUS)) + info->VLoadVL_PLUS;
}

void BattCoreGetVLoadAD_PLUS(float *pfVload, uint8_t ui8SampleNum)
{
	UARTprintf("BattCoreGetVLoadAD()...\n");

	uint32_t ui32VLoad = 0;
	uint32_t ui32VLoadSum = 0;
	uint8_t ui8LopIdx = 0;

	ROM_ADCSequenceDisable(ADC0_BASE, 3);
	ROM_ADCSequenceConfigure(ADC0_BASE, 3, ADC_TRIGGER_PROCESSOR, 0);
	ROM_ADCSequenceStepConfigure(ADC0_BASE, 3, 0, ADC_VLOAD_PLUS_CH | ADC_CTL_IE | ADC_CTL_END);
	ROM_ADCSequenceEnable(ADC0_BASE, 3);

	//Acc
	for(ui8LopIdx = 0; ui8LopIdx < ui8SampleNum; ui8LopIdx++)
	{
		ROM_ADCIntClear(ADC0_BASE, 3);
		ROM_ADCProcessorTrigger(ADC0_BASE, 3);
		while(!ROM_ADCIntStatus(ADC0_BASE, 3, false));
		ROM_ADCSequenceDataGet(ADC0_BASE, 3, &ui32VLoad);
		ui32VLoadSum = ui32VLoadSum + ui32VLoad;
	}

	//Average
	ui32VLoadSum = ui32VLoadSum / ui8SampleNum;
	//float temp = ui32VLoadSum;
	*pfVload = ui32VLoadSum;
}

void BattCoreGetVLoadAD_MINS(float *pfVload, uint8_t ui8SampleNum)
{
	UARTprintf("BattCoreGetVLoadAD()...\n");

	uint32_t ui32VLoad = 0;
	uint32_t ui32VLoadSum = 0;
	uint8_t ui8LopIdx = 0;

	ROM_ADCSequenceDisable(ADC0_BASE, 3);
	ROM_ADCSequenceConfigure(ADC0_BASE, 3, ADC_TRIGGER_PROCESSOR, 0);
	ROM_ADCSequenceStepConfigure(ADC0_BASE, 3, 0, ADC_VLOAD_MINS_CH | ADC_CTL_IE | ADC_CTL_END);
	ROM_ADCSequenceEnable(ADC0_BASE, 3);

	//Acc
	for(ui8LopIdx = 0; ui8LopIdx < ui8SampleNum; ui8LopIdx++)
	{
		ROM_ADCIntClear(ADC0_BASE, 3);
		ROM_ADCProcessorTrigger(ADC0_BASE, 3);
		while(!ROM_ADCIntStatus(ADC0_BASE, 3, false));
		ROM_ADCSequenceDataGet(ADC0_BASE, 3, &ui32VLoad);
		ui32VLoadSum = ui32VLoadSum + ui32VLoad;
	}

	//Average
	ui32VLoadSum = ui32VLoadSum / ui8SampleNum;
	//float temp = ui32VLoadSum;

	*pfVload = ui32VLoadSum;
}

//*****************************************************************************
//
// Application layer call this API to set test parameter: calble resistance compensation
//
//*****************************************************************************
void resistance_compensation(float val){
	uint8_t rc = val*10;
	UARTprintf("***resistance_compensation = %d\n", rc);
	cable_resistance = val;	
}

//*****************************************************************************
//
// Get differential voltage between load
// 20170808 add uint8_t ui8Switch
// 0: Dual load switch
// 10: Dual load switch with IR calculation(Kelvin connection)/linear mapping compensation(3-wire applied)
// 1: Single load 1 switch
// 11: Single load 1 switch with single load compensation(3-wire applied) 
//
//*****************************************************************************
bool BattCoreGetIR(float *pfIR, uint8_t ui8SampleNum, uint8_t ui8Switch)
{    
	UARTprintf("BattCoreGetIR(). Load_Times = %d.\n", ui8SampleNum);
	uint32_t ui32Voltage_scale = 0;
	uint8_t ui8LopIdx = 0;
	float Vmin = 0.0;
	float Vmax = 0.0;
	float Vload_PLUS = 0.0;
	float Vload_MINS = 0.0;
	float IRsum = 0.0;
	float f_current = 0.0;
	TEST_DATA* TD = Get_test_data();
	SYS_INFO* info = Get_system_info();

	ROM_IntMasterDisable();

	for(ui8LopIdx = 0; ui8LopIdx < ui8SampleNum; ui8LopIdx++)
	{
		//*****************************************************
		//
		//                 Vmin Handler
		//
		//*****************************************************

		//
		// load on protect off, load on pin on
		//
		switch(ui8Switch)
		{
			case dual_load:
			case dual_load_with_result:
				CCA_load_1_protect_OFF;
				CCA_load_2_protect_OFF;
				CCA_load_1_ON;
				CCA_load_2_ON;
				break;
			case single_load:
			case single_load_with_result:
				CCA_load_1_protect_OFF;
				CCA_load_1_ON;
				break;
		}
		
		DELAY_ZERO_ZERO_ZERO_ONE_FIVE_SECOND;
		BattCoreGetVoltage(&ui32Voltage_scale, &Vmin);

		//*****************************************************
		//
		//               Delta Vload Handler
		//
		//*****************************************************
		BattCoreGetVLoad(&Vload_PLUS, &Vload_MINS,15);

		//*****************************************************
		//
		//                 Vmax Handler
		//
		//*****************************************************

		//
		// load on pin off, load on protect on
		//
		switch(ui8Switch)
		{
			case dual_load:
			case dual_load_with_result:
				CCA_load_1_OFF;
				CCA_load_2_OFF;
				CCA_load_1_protect_ON;
				CCA_load_2_protect_ON;
				break;
			case single_load:
			case single_load_with_result:
				CCA_load_1_OFF;
				CCA_load_1_protect_ON;
				break;
		}
		DELAY_ZERO_ZERO_ZERO_TWO_SECOND;//2ms
		BattCoreGetVoltage(&ui32Voltage_scale, &Vmax);// Get Voltage_max

		//*****************************************************
		//
		//              Load Error Conditions?
		//
		//*****************************************************
		if(Vmin>= Vmax)
		{
			ROM_IntMasterEnable();
			return false;
		}
		else if( (Vmax - Vmin) < delta_voltage_CCA )
		{
			ROM_IntMasterEnable();
			return false;
		}
		else if(Vload_MINS >= Vload_PLUS)
		{
			ROM_IntMasterEnable();
			return false;
		}

		//*****************************************************
		//
		//                     IR Handler
		//
		//*****************************************************
		switch(ui8Switch)
		{
			case dual_load:
			case dual_load_with_result:
				f_current = (Vload_PLUS - Vload_MINS)/0.025; //0.05 = 100m
				IRsum = IRsum + ((Vmax - Vmin) / f_current);
				break;
			case single_load:
			case single_load_with_result:
				f_current = (Vload_PLUS - Vload_MINS)/0.05;   // 0.1 = 100m
				IRsum = IRsum + ((Vmax - Vmin) / f_current);
				break;
		}
		//*****************************************************
		//
		//                   LOAD TAKE A REST
		//
		//*****************************************************
		DELAY_ZERO_FIVE_SECOND;
	}
	ROM_IntMasterEnable();
	//
	//  IR average
	//
	(*pfIR) = IRsum / ui8SampleNum;

	//
	//  Ohm to mini Ohm
	//
	(*pfIR) = (*pfIR) * 1000;

	if(TD->SYSTEM_CHANNEL == MAP_6V)
	{		
		switch(ui8Switch)
		{
			case dual_load_with_result:
			case single_load_with_result:
			    (*pfIR) = (*pfIR) * info->IRCalCoe6V * IR_6V_COMP_C;// * gfIRCalCoe12V * gfIRCalCoe12V * IR_6V_COMP_C;
				break;
		}
	}
	else if(TD->SYSTEM_CHANNEL == MAP_12V)
	{		
		switch(ui8Switch)
		{
			case dual_load_with_result:
			case single_load_with_result:
                (*pfIR) = (*pfIR) * info->IRCalCoe12V * IR_12V_COMP_C;
                break;
		}
	}
	else if(TD->SYSTEM_CHANNEL == MAP_24V)
	{
		//************************
		//        Error
		//************************
	}

	*pfIR -= cable_resistance;
	return true;
}

bool BattCoreCChkCable(uint8_t ui8SampleNum)
{    
	uint8_t ui8LopIdx = 0;
	float Vmin = 0.0;
	float Vmax = 0.0;
	float Vload_PLUS = 0.0;
	float Vload_MINS = 0.0;
    bool ret = true;
    if(!ui8SampleNum) ui8SampleNum = 1;

	ROM_IntMasterDisable();
	for(ui8LopIdx = 0; ui8LopIdx < ui8SampleNum; ui8LopIdx++){
        CCA_load_1_protect_OFF;
        CCA_load_2_protect_OFF;
        CCA_load_1_ON;
        CCA_load_2_ON;
        DELAY_ZERO_ZERO_ZERO_ONE_FIVE_SECOND;
        
        BattCoreScanVoltage(&Vmin);
        BattCoreGetVLoad(&Vload_PLUS, &Vload_MINS,15);

        CCA_load_1_OFF;
        CCA_load_2_OFF;
        CCA_load_1_protect_ON;
        CCA_load_2_protect_ON;
		DELAY_ZERO_ZERO_ZERO_TWO_SECOND;
        BattCoreScanVoltage(&Vmax);

		if(Vmin>= Vmax || (Vmax - Vmin) < delta_voltage_CCA || Vload_MINS >= Vload_PLUS){
            ret = false;
            break;
        }
		DELAY_ZERO_FIVE_SECOND;
	}
	ROM_IntMasterEnable();
	return ret;
}

