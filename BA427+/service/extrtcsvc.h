//*****************************************************************************
//
// Source File: extrtcsvc.h
// Description: Prototype of the function to accecc/write S35190A Clock Drive.
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 11/19/15     Vincnet.T      Created file.
// 08/05/20     David.Wang     Clock time rtc to manual operation
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/service/extrtcsvc.h#5 $
// $DateTime: 2017/09/22 10:27:40 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#include "../driver\s35190a_clk.h"


#ifndef _EXTRTCSVC_H_
#define _EXTRTCSVC_H_

//**********************************
//Example:
//2015/12/31/4/9:00:00
//Year:  2015
//Month: December
//Date:  31th
//Day:   Thursday
//Hour:  9
//Min:   0
//Sec:   0
//**********************************
void ExtRTCRead(uint16_t *pu16Year, uint8_t *pui8Month, uint8_t *pui8Date, uint8_t *pui8Day, uint8_t *pui8Hour, uint8_t *pui8Min, uint8_t *pui8Sec);
void ExtRTCWrite(uint16_t pu16Year, uint8_t pui8Month, uint8_t pui8Date, uint8_t pui8Day, uint8_t pui8Hour, uint8_t pui8Min, uint8_t pui8Sec);
uint8_t ExtRTCDecode(uint8_t ui8Byte);
uint8_t ExtRTCDEncode(uint8_t ui8Byte);
void ExtRTCInit(void);

#endif /* _EXTRTCSVC_H_ */
