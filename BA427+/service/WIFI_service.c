//*****************************************************************************
//
// Source File: wifi_service.c
// Description: wifi module service layer.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 12/28/17     Jerry.W        Created file.
// 02/14/18     Henry.Y        add HTTP status.
// 03/22/18     Jerry.W        add Get_internet_time() to get time with Internet.
// 05/18/18     Jerry.W        modify for changing of HTTP protocol with wifi module.
//                             add wifi status blinking function.
// 09/28/18     Jerry.W        remove the function of old architecture.
// 09/28/18     Jerry.W        reset socket when wifi power off.
// 10/24/18     Jerry.W        add protocol: get wifi module version, wifi module test.
// 12/07/18     Jerry.W        Fix bug: connect to AP and disconnect, backlight should be flashing.
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/driver/wifi_driver.h#2 $
// $DateTime: 2017/12/18 16:17:54 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#include "cloud.h"
#include "wifi_service.h"
#include "Socket_svc.h"
#include "WIFI_protocol.h"
#include "../driver/st7565_lcm.h"
#include "../driver/delaydrv.h"

#define WIFI_SERVICE_DEBUG 0

#if WIFI_SERVICE_DEBUG
	#define MEM_DEBUG {UARTprintf("MEM FAIL in FUNCTION : %S\n", __func__); \
					while(1){};}  //doing nothing now, could modify the definition for debugging


	#define VARNAME(var) #var
	#define WIFI_SERVICE_MALLOC(mem,size) { UARTprintf("Malloc(%s) at line : %d\n", VARNAME(mem),__LINE__); \
										mem = malloc(size);}
	#define WIFI_SERVICE_FREE(mem) 	{ UARTprintf("Free(%s) at line : %d\n", VARNAME(mem),__LINE__); \
										free(mem);}
#else
	#define MEM_DEBUG return
	#define WIFI_SERVICE_MALLOC(mem,size) { mem = malloc(size); }
	#define WIFI_SERVICE_FREE(mem) {free(mem);}
#endif				
					



/****************************************************************************
 *  					State machine declaration							*
 ****************************************************************************/
typedef enum{
	WIFI_svc_off,
	WIFI_svc_disconnect,
	WIFI_svc_connected,
}WIFI_SERVICE_state_T;

static volatile WIFI_SERVICE_state_T WIFI_SERVICE_state = WIFI_svc_off;

static CONN_POLICY_T WIFI_connection_policy = conn_auto;

/****************************************************************************
 *  							 Work buffer								*
 ****************************************************************************/
typedef enum{
	work_SNTP_response = 			(0x00000001 << 6),
	work_VER_response = 			(0x00000001 << 7),
	work_loopback_response = 		(0x00000001 << 8),
	
	event_report_state_change = 	(0x00000001 << 10),
	event_report_conn_fail = 		(0x00000001 << 11),
	event_report_internet_fail = 	(0x00000001 << 12),
	event_report_AP_scan_done = 	(0x00000001 << 13),
	event_report_getting_IP = 		(0x00000001 << 16),
	
	error_report_com_type = 		(0x00000001 << 20),
	error_report_com_fromat = 		(0x00000001 << 21),
	error_report_module_fail = 		(0x00000001 << 22),
}WORK_T;




static WORK_T Remained_work_flag = (WORK_T)NULL;

static SSID_list_T* SSID_list = NULL;
static uint8_t * AP_SSID = NULL; 
static uint8_t * AP_PW = NULL;

static uint8_t * AP_connected = NULL;

static uint8_t * version_s = NULL;
static uint8_t* loopback_data = NULL;
static uint16_t loopback_len = 0;


static uint8_t Time_counter;
void (*Time_out_cb)(void);
static Timer_t* Timer_handle = NULL;

static uint8_t *Date_sting = NULL;

static bool blink_enable = true;

#define PORTOCOL_DEBUG(flag) Remained_work_flag |= flag; ////doing nothing now, could modify the definition for debugging


/****************************************************************************
 *  						Callback function declaration					*
 ****************************************************************************/
static void (*WIFI_connection_event_cb)(uint32_t );
static void (*WIFI_AP_SCAN_cb)(SSID_list_T *);
static void (*SNTP_cb)(uint8_t *);
static void (*version_cb)(uint8_t *);
static void (*module_loopback_cb)(uint16_t ,uint8_t *);

static void driver_error_handler(uint8_t err);
static void command_handler(uint8_t *command, uint16_t len);

static void WIFI_status_blinker(void);

Timer_t *WIFI_blink_timer = NULL;

void wifi_config(uint8_t no_off,void (*evn_cb)(uint32_t )){
	static bool cb_registered = false;
	if(WIFI_blink_timer != NULL){
		unregister_timer(WIFI_blink_timer);
		WIFI_blink_timer = NULL;
		ST7650Purple_blink(false);
	}

	if(no_off){
		if(evn_cb){
			WIFI_connection_event_cb = evn_cb;
			if(blink_enable){
				WIFI_blink_timer = register_timer(20, WIFI_status_blinker); 		
				start_timer(WIFI_blink_timer);
			}
		}
		
		if(WIFI_SERVICE_state == WIFI_svc_off){
			WIFI_SERVICE_state = WIFI_svc_disconnect;
			WIFI_connection_policy = conn_auto;
		}
	}
	else{
		WIFI_SERVICE_state = WIFI_svc_off;
		WIFI_connection_event_cb = NULL;
		Socket_reset();
	}

	wifi_driver_init(no_off);
	if(!cb_registered){
		wifi_driver_regist_com_cb(WIFI_SERVICE_CHANN, command_handler);
		wifi_driver_regist_error_cb(driver_error_handler);
		cb_registered = true;
	}
}





void wifi_connnect_policy(CONN_POLICY_T policy){
	uint8_t * data;
	WIFI_connection_policy = policy;
	WIFI_SERVICE_state = WIFI_svc_disconnect;

	WIFI_SERVICE_MALLOC(data,2);
	if(!data) MEM_DEBUG;
	data[0] = WIFI_com_conn_policy;
	data[1] = policy;

	if(wifi_driver_send_command(WIFI_SERVICE_CHANN,data,2)&&WIFI_connection_event_cb){
		WIFI_connection_event_cb(WIFI_TX_buffer_over_flow);
	}
}





void wifi_connnect_AP(uint8_t *SSID, uint8_t *PW){
	uint8_t len_s,len_p;
	uint8_t* data;
	WIFI_connection_policy = conn_designate;
	WIFI_SERVICE_state = WIFI_svc_disconnect;
	
	len_s = strlen((char*)SSID);
	len_p = strlen((char*)PW);
	WIFI_SERVICE_MALLOC(data, len_s + len_p + 4);
	if(!data) MEM_DEBUG;
	memset(data,0,len_s + len_p + 4);
	data[0] = WIFI_com_conn_policy;
	data[1] = conn_designate;
	memcpy(data + 2, SSID , len_s);
	memcpy(data + 3 + len_s, PW , len_p);
	
	if(blink_enable){
		if(WIFI_blink_timer != NULL) unregister_timer(WIFI_blink_timer);
		ST7650Purple_blink(false);
		WIFI_blink_timer = register_timer(20, WIFI_status_blinker);
		start_timer(WIFI_blink_timer);
	}

	if(wifi_driver_send_command(WIFI_SERVICE_CHANN,data,len_s + len_p + 4)&&WIFI_connection_event_cb){
		WIFI_connection_event_cb(WIFI_TX_buffer_over_flow);
	}

	
	//save SSID & Password
	if(AP_SSID){
		WIFI_SERVICE_FREE(AP_SSID);
		AP_SSID = NULL;
	}
	if(AP_PW){
		WIFI_SERVICE_FREE(AP_PW);
		AP_PW = NULL;
	}
	WIFI_SERVICE_MALLOC(AP_SSID, len_s+1);
	if(!AP_SSID) MEM_DEBUG;
	WIFI_SERVICE_MALLOC(AP_PW, len_p+1);
	if(!AP_PW){
		WIFI_SERVICE_FREE(AP_SSID);
		AP_SSID = NULL;;
		MEM_DEBUG;
	}
	memcpy(AP_SSID, SSID , len_s+1);
	memcpy(AP_PW, SSID , len_p+1);
		
}





void wifi_scan_AP(void (*scn_bc)(SSID_list_T *)){
	uint8_t *data;
	WIFI_AP_SCAN_cb = scn_bc;
	WIFI_SERVICE_MALLOC(data, 1);
	if(!data) MEM_DEBUG;
	data[0] = WIFI_com_scan;
	if(wifi_driver_send_command(WIFI_SERVICE_CHANN,data,1)&&WIFI_connection_event_cb){
		WIFI_connection_event_cb(WIFI_TX_buffer_over_flow);
	}
}




	
	



void wifi_service_poll(void){ //process the work to be done and asynchronous callback

	if(WIFI_SERVICE_state == WIFI_svc_off) return;
	
	//report error condition
	if(Remained_work_flag & error_report_com_type){
		if(WIFI_connection_event_cb) WIFI_connection_event_cb(WIFI_receive_command_error_type);
		Remained_work_flag &= ~error_report_com_type;
	}
	if(Remained_work_flag & error_report_com_fromat){
		if(WIFI_connection_event_cb) WIFI_connection_event_cb(WIFI_receive_command_error_format);
		Remained_work_flag &= ~error_report_com_fromat;
	}
	if(Remained_work_flag & error_report_module_fail){
		if(WIFI_connection_event_cb) WIFI_connection_event_cb(WIFI_module_fail);
		Remained_work_flag &= ~error_report_module_fail;
	}
	
	//report connection status 
	if(Remained_work_flag & event_report_state_change){
		if(WIFI_blink_timer != NULL){
			unregister_timer(WIFI_blink_timer);
			WIFI_blink_timer = NULL;
			ST7650Purple_blink(false);
		}

		if(wifi_get_status() != WIFI_connected){
			if(blink_enable){
				WIFI_blink_timer = register_timer(20, WIFI_status_blinker); 		
				start_timer(WIFI_blink_timer);
			}
		}

		if(WIFI_connection_event_cb) WIFI_connection_event_cb(wifi_get_status());
		Remained_work_flag &= ~event_report_state_change;
	}
	if(Remained_work_flag & event_report_conn_fail){
		if(WIFI_connection_event_cb) WIFI_connection_event_cb(WIFI_connect_fail);
		Remained_work_flag &= ~event_report_conn_fail;
		if(WIFI_blink_timer == NULL){
			WIFI_blink_timer = register_timer(20, WIFI_status_blinker);
			start_timer(WIFI_blink_timer);
		}
	}
	if(Remained_work_flag & event_report_internet_fail){
		if(WIFI_blink_timer != NULL){
			unregister_timer(WIFI_blink_timer);
			WIFI_blink_timer = NULL;
			ST7650Purple_blink(false);
		}
		if(WIFI_connection_event_cb) WIFI_connection_event_cb(WIWI_connected_AP_internet_fail);
		Remained_work_flag &= ~event_report_internet_fail;
	}
	if(Remained_work_flag & event_report_AP_scan_done){
		if(WIFI_AP_SCAN_cb&&SSID_list) WIFI_AP_SCAN_cb(SSID_list );
		Remained_work_flag &= ~event_report_AP_scan_done;
	}

	if(Remained_work_flag & event_report_getting_IP){
		if(WIFI_connection_event_cb) WIFI_connection_event_cb(WIFI_getting_IP);
		Remained_work_flag &= ~event_report_getting_IP;
	}
	
	
	
	//doing buffered works
	
	

	if(Remained_work_flag & work_SNTP_response){
		if(SNTP_cb) SNTP_cb(Date_sting);
		WIFI_SERVICE_FREE(Date_sting);
		Date_sting = NULL;
		Remained_work_flag &= ~work_SNTP_response;
	}
	
	if(Remained_work_flag & work_VER_response){
		if(version_cb && version_s) version_cb(version_s);
		WIFI_SERVICE_FREE(version_s);
		version_s = NULL;
		Remained_work_flag &= ~work_VER_response;
	}
	
	if(Remained_work_flag & work_loopback_response){
		if(module_loopback_cb&&loopback_data) module_loopback_cb(loopback_len, loopback_data);
		WIFI_SERVICE_FREE(loopback_data);
		loopback_data = NULL;
		Remained_work_flag &= ~work_loopback_response;
	}
}





WIFI_CONN_T wifi_get_status(void){
	WIFI_CONN_T temp = WIFI_off;
	switch(WIFI_SERVICE_state){
	case WIFI_svc_off :
		temp = WIFI_off;
		break;
	case WIFI_svc_disconnect :
		temp = WIFI_disconnect;
		break;
	case WIFI_svc_connected :
		temp = WIFI_connected;
		break;
	}
	return temp;
}



bool wifi_get_connected_AP(uint8_t *SSID_buf, uint8_t buf_len){
	if(wifi_get_status() == WIFI_connected){
		strncpy((char*)SSID_buf, (const char*)AP_connected, buf_len);
		
		return true;
	}
	else{
		return false;
	}
}








static void driver_error_handler(uint8_t err){
	switch(err){
		case module_no_response :
			PORTOCOL_DEBUG(error_report_module_fail);
		break;
	}
}



static void command_handler(uint8_t *command, uint16_t len){
	uint8_t n;
	uint8_t *temp;
	switch(command[0]){
	case WIFI_evnt_conn_status :
		if((len != 2)&&(command[1] != 0x00)&&(command[1] != 0x03)){
			PORTOCOL_DEBUG(error_report_com_fromat);
			return;
		}
		switch(command[1]){
		case 0x00 :
		case 0x03 :
			temp = (command + 2);
			while(*temp) {
				temp++;
				if((temp - command) >= len){
					PORTOCOL_DEBUG(error_report_com_fromat);
					return;
				}
			}
			temp++;
			if((temp - command) != len){
				PORTOCOL_DEBUG(error_report_com_fromat);
				return;
			}

			if(WIFI_SERVICE_state == WIFI_svc_disconnect){
				WIFI_SERVICE_state = WIFI_svc_connected;
				if(command[1] == 0x00){
					Remained_work_flag |= event_report_state_change;
				}
				else{
					Remained_work_flag |= event_report_internet_fail;
				}
			}
			if(AP_connected){
				WIFI_SERVICE_FREE(AP_connected);
			}
			WIFI_SERVICE_MALLOC(AP_connected,len-2);
			memcpy(AP_connected, command + 2, len-2);

			break;
		case 0x01 :
			WIFI_SERVICE_state = WIFI_svc_disconnect;
			Remained_work_flag |= event_report_conn_fail;

			break;
		case 0x02 :
			if(wifi_get_status() == WIFI_connected){
				WIFI_SERVICE_state = WIFI_svc_disconnect;
				Remained_work_flag |= event_report_state_change;
			}
			break;
		case 0x04 :
			Remained_work_flag |= event_report_getting_IP;
			break;
		default :
			PORTOCOL_DEBUG(error_report_com_type);
			break;
		}

		break;
	case WIFI_evnt_conn_policy :
		if(len != 2){
			PORTOCOL_DEBUG(error_report_com_fromat);
			return;
		}
		if(command[1] > 0x02){
			PORTOCOL_DEBUG(error_report_com_type);
		}
		else if(command[1] != WIFI_connection_policy){
			if(WIFI_connection_policy == conn_designate){
				wifi_connnect_AP(AP_SSID, AP_PW);
			}
			else{
				wifi_connnect_policy(WIFI_connection_policy);
			}
		}

		break;
	case WIFI_evnt_scan_result :
		if(len<2){
			PORTOCOL_DEBUG(error_report_com_fromat);
			return;
		}
		n = command[1];

		if(len != (n*sizeof(AP_info_T)+2)){
					PORTOCOL_DEBUG(error_report_com_fromat);
					return;
				}
		
		if(SSID_list) WIFI_SERVICE_FREE(SSID_list);
		
		WIFI_SERVICE_MALLOC(SSID_list, len-1);
		if(!SSID_list) MEM_DEBUG;
		memcpy(SSID_list, command+1, len-1);
		Remained_work_flag |= event_report_AP_scan_done;

		break;
	case SNTP_com_response:
		if(Date_sting){
			WIFI_SERVICE_FREE(Date_sting);
		}
		WIFI_SERVICE_MALLOC(Date_sting, len);
		if(!Date_sting) MEM_DEBUG;

		memcpy(Date_sting, command+1, len-1);
		Date_sting[len-1] = 0;

		Remained_work_flag |= work_SNTP_response;

		break;
	case WIFI_evnt_resp_version:
		if(version_s) WIFI_SERVICE_FREE(version_s);
		WIFI_SERVICE_MALLOC(version_s, len-1);
		if(version_s == 0) MEM_DEBUG;
		memcpy(version_s,command+1, len-1 );
		Remained_work_flag |= work_VER_response;
		break;
	case WIFI_com_loopback:
		if(loopback_data) WIFI_SERVICE_FREE(loopback_data);
		WIFI_SERVICE_MALLOC(loopback_data, len-1);
		if(loopback_data == 0) MEM_DEBUG;
		memcpy(loopback_data,command+1, len-1 );
		loopback_len = len-1;
		Remained_work_flag |= work_loopback_response;
		break;
	default :
		PORTOCOL_DEBUG(error_report_com_type);
		break;
	}
}





void Time_coundown(){
	if(Time_counter){
		Time_counter--;
	}
	else{
		Time_out_cb();
		unregister_timer(Timer_handle);
		Timer_handle = NULL;
	}
}

void set_network_timout(uint8_t wait_time, void (*time_cb)(void)){
	if(Timer_handle) unregister_timer(Timer_handle);
	Time_counter = wait_time-1;
	Time_out_cb = time_cb;
	Timer_handle = register_timer(10, Time_coundown);
	start_timer(Timer_handle);
}



void Get_internet_time(int8_t TZ, void (*time_cb)(uint8_t *)){
	uint8_t *data;
	if(TZ>12 || TZ < -11) TZ = 8;
	SNTP_cb = time_cb;
	WIFI_SERVICE_MALLOC(data,  2);
	if(!data) MEM_DEBUG;
	data[0] = SNTP_com_request;
	data[1] = TZ;
	wifi_driver_send_command(WIFI_SERVICE_CHANN,data, 2);
}


static void WIFI_status_blinker(void){
	static bool light = false;

	ST7650Purple_blink(light);

	light ^= 1;
}

bool Get_module_version(void (*ver_cb)(uint8_t *)){
	uint8_t *data;
	if(WIFI_SERVICE_state == WIFI_svc_off) return false;
	version_cb = ver_cb;
	WIFI_SERVICE_MALLOC(data,1);
	data[0] = WIFI_com_get_version;
	wifi_driver_send_command(WIFI_SERVICE_CHANN,data, 1);
	return true;
}

bool wifi_module_loopback(uint16_t len, uint8_t *data, void (*loop_cb)(uint16_t data, uint8_t *)){
	uint8_t *temp;
	if(WIFI_SERVICE_state == WIFI_svc_off) return false;
	module_loopback_cb = loop_cb;

	WIFI_SERVICE_MALLOC(temp,len+1);

	temp[0] = WIFI_com_loopback;

	memcpy(temp+1, data, len);

	wifi_driver_send_command(WIFI_SERVICE_CHANN,temp, len+1);
	return true;

}


void set_WIFI_blink(bool on_off){

	blink_enable = on_off;
}

