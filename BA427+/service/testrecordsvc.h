//*****************************************************************************
//
// Source File: testrecordsvc.h
// Description: Prototype of the function to Read/Write Test Record
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 11/13/15     Vincnet.T      Created file.
// 11/18/15     Vincent.T      New function TestRecordRead() to read tested records
// 01/05/16     Vincent.T      1.Define Max. # of test counter
//                             2.BTTestCounterInc() to save battery test counter
// 01/29/16     Vincent.T      1. Add function BTTestCounterInc(); declaraction
// 02/15/16     Vincent.T      1. Add function SYSTestCounterInc(); declaraction
// 04/11/16     Vincent.T      1. New IR_Test in TEST_TYPE
// 06/28/16     Vincent.T      1. Switch volt. and cca rating in BTTestRecord(); and SSTestRecord();
//                             2. Add Temperature and Date Time as input parameters for SysTestRecord(), BTTestRecord(); and SSTestRecord();
// 09/06/16     Vincent.T      1. Modify MAX_TESTCOUNTER 1500 -> 1000
// 01/13/17     Vincent.T      1. Modify MAX_TESTCOUNTER 1000 -> 500
//                             2. New function IRTestCounterInc();
//                             3. Modify declaraction of BTTestRecord(); and SSTestRecord();
// 09/11/18     Henry.Y        Add MAX_SYSTESTCOUNTER definition.
// 11/19/18     Henry.Y        Move enum{...}TESTCOUNTERTYPE from app layer to here.
// 07/28/20     David Wang     Adjust the order of the menu => TEST_TYPE, TESTCOUNTERTYPE
// 09/06/20     David.Wang     TEST COUNTER ENUM IR & SYS TEST NO USED TO MASK
//
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/service/testrecordsvc.h#3 $
// $DateTime: 2017/12/26 10:41:41 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#ifndef _TESTRECORDSVC_H_
#define _TESTRECORDSVC_H_

#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include "exteprsvc.h"
#include "battcoresvc.h"

#define MAX_BTSSTESTCOUNTER 500
#define MAX_SYSTESTCOUNTER 200

#define ROIN_LEN 8
#define VININ_LEN1 17
#define TestCode_LEN 17
#define JIS_LEN 17

//***************
//   Test Type
//***************
typedef enum
{
    Start_Stop_Test,        // Start-stop Test
	Battery_Test,		    // Battery Test
	System_Test, 			// System Test
	IR_Test					// IR Test
}TEST_TYPE;


typedef enum{
	Wait_uplaod = 0xff,
	Uploaded_record = 0x5F,
	No_upload = 0x00,
}RECORD_STATUS;

typedef enum
{
	BATTEST = 0,        /* Battery test counter */
	SYSTEST,		    /* System test counter */
	PRINT,			    /* Print Test Conter */
	//IRTESTCOUNTER,	/* IR test counter */
	//SSTEST	        /* Start stop test counter */
}TESTCOUNTERTYPE;


void SaveTestRecords(TEST_TYPE test_type, void* rec, uint8_t len, RECORD_STATUS status );

uint8_t record_status(uint16_t ui16TestNum);
void set_uploaded(uint16_t ui16TestNum);

void TestRecordRead(uint16_t ui16TestNum, uint8_t *ui8RecordBuf);
void counter_increase(uint16_t *count);

void TestRecordSYSRead(uint16_t ui16TestNum, uint8_t *ui8RecordBuf);

#endif /* _TESTRECORDSVC_H_ */
