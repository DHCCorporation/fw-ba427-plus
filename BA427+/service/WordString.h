//*****************************************************************************
//
// Source File: WordString.h
// Description: new grab string function
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 08/12/20     DW             Create file.
//
// =============================================================================
//
//
// $Header$
// $DateTime$
// $Author$
//
//
// Copyright (c) 2020 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#ifndef SERVICE_WORDSTRING_H_
#define SERVICE_WORDSTRING_H_

#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

typedef enum{
    TruckGroup31 = 0,
    YES,
    NO,
}STRING_WORD_T;

int Get_language_number();

int Get_String_number();


//return pointer to the string.  return 0, if parameter error.
char* Get_String(int lang, STRING_WORD_T str_idx);



#endif /* SERVICE_WORDSTRING_H_ */
