//*****************************************************************************
//
// Source File: vmsvc.h
// Description: Functions to access voltage meter data form voltage meter accessory.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 01/07/16     Vincent.T      Created file.
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/service/vmsvc.h#5 $
// $DateTime: 2017/09/22 10:27:40 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#ifndef _VMSVC_H_
#define _VMSVC_H_

#include "../driver\vm.h"

void GetVM(float *pfVMvol, uint32_t *ADC);


#endif /* _VMSVC_H_ */
