//*****************************************************************************
//
// Source File: Socket_svc.c
// Description: socket service
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 08/03/18     Jerry.W        Created file.
// 09/28/18     Jerry.W        add SSL certificate enable/diable function.
// 10/17/18     Jerry.W        add Socket reset function.
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/driver/wifi_driver.h#2 $
// $DateTime: 2017/12/18 16:17:54 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************


#include "WIFI_protocol.h"
#include "WIFI_service.h"
#include "Socket_svc.h"

#if SOCKET_DEBUG
#define 	SOC_DEBUG(bug_info)		UARTprintf("SOC:%s, @%d\n", bug_info, __LINE__)
#else
#define 	SOC_DEBUG(bug_info)
#endif

void TCP_status_update(uint8_t index, uint8_t status);
void TCP_recieve_handler(uint8_t index, uint8_t *data, uint16_t len);



static uint8_t TCP_conn_count = 0;
static Socket_t* Soc_rec[TCP_CONN_MAX];
static uint8_t soc_idx_count = 0;






void Socket_protocal_handler(uint8_t *data,uint16_t len){
	switch(data[0]){
	case TCP_evnt_status_v2:{
		uint8_t status = (data[2] == connected)? Soc_TCP_connected:
						 (data[2] == connect_fail)? Soc_TCP_connect_fail:
						 (data[2] == disconnected)? Soc_TCP_disconnected:Soc_idle;

		TCP_status_update(data[1] ,  status);
	}
		break;
	case TCP_evnt_receive:
		TCP_recieve_handler(data[1], data+2, len-2);
		break;
	case UDP_evnt_status:

		break;
	case UDP_evnt_receive:

		break;
	default:
		SOC_DEBUG("unknown event");
		break;
	}
}





void Socket_svc_init(){
	static bool cb_registered = false;


	if(!cb_registered){
		wifi_driver_regist_com_cb(SOCKET_SERVICE_CHANN, Socket_protocal_handler);
		cb_registered = true;
	}

}


bool TCP_connect(Socket_t* soc,  bool secure ){
	uint8_t i, comm_len, *comm;

	if(wifi_get_status() != WIFI_connected){
		SOC_DEBUG("wifi disabled");
		for(i = 0; i < TCP_CONN_MAX ; i++){
			if(Soc_rec[i]){
				if(Soc_rec[i]->status != Soc_idle){
					Soc_rec[i]->evn_cb(Soc_rec[i]->reserve, Soc_TCP_connect_fail);
				}
				Soc_rec[i]->status = Soc_idle;
				Soc_rec[i] = 0;
			}
		}
		return false;
	}

	if(TCP_conn_count == TCP_CONN_MAX || soc == NULL){
		SOC_DEBUG("TCP connection fulled");
		return false;
	}

	if(soc->status != Soc_idle){
		SOC_DEBUG("socket status error");
		return false;
	}

	for(i = 0; i < TCP_CONN_MAX; i++){
			if(Soc_rec[i] == soc){
				break;
			}
	}

	if(i == TCP_CONN_MAX){
		for(i = 0; i < TCP_CONN_MAX; i++){
		if(Soc_rec[i] == NULL){
			Soc_rec[i] = soc;
				TCP_conn_count++;
			break;
		}
	}

	if(i == TCP_CONN_MAX){
		SOC_DEBUG("Soc_rec error");
		return false;
	}
	}

	soc->status = Soc_TCP_connecting;
	soc->index = soc_idx_count;
	soc_idx_count++;
	comm_len = strlen((char*)soc->pair) + 6;

	comm = malloc(comm_len);

	if(comm == NULL){
		soc->status = Soc_idle;
		SOC_DEBUG("malloc fail");
		return false;
	}

	comm[0] = TCP_com_conn_to_ser_v2;
	comm[1] = soc->index;
	comm[2] = soc->port % 256;
	comm[3] = soc->port / 256;
	comm[4] = (secure)? 1:0;
	memcpy(comm+5, soc->pair, comm_len-5);

	if(wifi_driver_send_command(SOCKET_SERVICE_CHANN, comm, comm_len)){
		SOC_DEBUG("send TCP connect command fail");
		soc->status = Soc_idle;
		return false;
	}

	return true;

}

void TCP_disconnect(Socket_t* soc){
	uint8_t i, *comm;

	for(i = 0; i < TCP_CONN_MAX; i++){
		if(Soc_rec != 0 && Soc_rec[i] == soc){
			if(soc->status != Soc_TCP_connected){
				SOC_DEBUG("disconnect unconnected socket");
			}
			soc->status = Soc_idle;
			comm = malloc(2);
			if(comm == NULL){
				soc->status = Soc_idle;
				SOC_DEBUG("malloc fail");
				return;
			}
			comm[0] = TCP_com_disconn_v2;
			comm[1] = soc->index;
			if(wifi_driver_send_command(SOCKET_SERVICE_CHANN, comm, 2)){
				SOC_DEBUG("send TCP disconnect command fail");
			}
			//Soc_rec[i] = NULL;
			//TCP_conn_count--;
			return;
		}
	}

	SOC_DEBUG("disconnect unknown socket");
}

void TCP_send(Socket_t* soc, uint8_t* data, uint16_t len){
	uint8_t i, *comm;
	for(i = 0 ; i < TCP_CONN_MAX ; i++){
		if(Soc_rec[i] != 0 && soc == Soc_rec[i]){
			break;
		}
	}

	if(i == TCP_CONN_MAX){
		SOC_DEBUG("TCP send fail : unknown socket");
		return;
	}

	comm = malloc(len+2);
	if(comm == NULL){
		soc->status = Soc_idle;
		SOC_DEBUG("malloc fail");
		return;
	}

	comm[0] = TCP_com_send;
	comm[1] = soc->index;
	memcpy(comm+2, data, len);
	if(wifi_driver_send_command(SOCKET_SERVICE_CHANN, comm, len+2)){
		SOC_DEBUG("send TCP data command fail");
	}
}


void TCP_status_update(uint8_t index, uint8_t status){
	uint8_t i;

	for(i = 0; i < TCP_CONN_MAX; i++){
		if(Soc_rec[i] != 0 && Soc_rec[i]->index == index){
			Soc_rec[i]->status = (status == Soc_TCP_connect_fail)? Soc_idle:
								(status == Soc_TCP_disconnected)? Soc_idle:status;
			if(Soc_rec[i]->evn_cb) Soc_rec[i]->evn_cb(Soc_rec[i]->reserve,status);
			if(Soc_rec[i]->status == Soc_idle){
				Soc_rec[i] = NULL;
				TCP_conn_count--;
			}
			break;
		}
	}

	if(i == TCP_CONN_MAX){
		SOC_DEBUG("TCP status update : unregistered index");
	}
}

void TCP_recieve_handler(uint8_t index, uint8_t *data, uint16_t len){
	uint8_t i;

	for(i = 0; i < TCP_CONN_MAX; i++){
		if(Soc_rec[i] != 0 && Soc_rec[i]->index == index){
			if(Soc_rec[i]->rcv_cb) Soc_rec[i]->rcv_cb(Soc_rec[i]->reserve, data, len);
			break;
		}
	}

	if(i == TCP_CONN_MAX){
		SOC_DEBUG("TCP receive : unregistered index");
	}
}

void CERT_set(CERT_t type, bool en){
	uint8_t *comm = malloc(2);
	comm[0] = (type == CERT_server)? CERT_server_set : CERT_client_set;
	comm[1] = (en)? 1 : 0;
	wifi_driver_send_command(SOCKET_SERVICE_CHANN, comm, 2);
}

void Socket_reset(){
	uint8_t i;
	for(i = 0; i < TCP_CONN_MAX; i++){
		if(Soc_rec[i] != 0){
			if(Soc_rec[i]->status != Soc_idle){
				Soc_rec[i]->status = Soc_idle;
				if(Soc_rec[i]->evn_cb) Soc_rec[i]->evn_cb(Soc_rec[i]->reserve, Soc_TCP_disconnected);
			}
			Soc_rec[i] = NULL;
		}
	}
	TCP_conn_count = 0;
}
