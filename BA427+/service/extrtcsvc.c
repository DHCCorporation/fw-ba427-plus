//*****************************************************************************
//
// Source File: extrtcsvc.c
// Description: Prototype of the function to accecc/write S35190A Clock Drive.
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 11/19/15     Vincnet.T      Created file.
// 08/05/20     David.Wang     Clock time rtc to manual operation
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/service/extrtcsvc.c#5 $
// $DateTime: 2017/09/22 10:27:40 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#include "extrtcsvc.h"

void ExtRTCRead(uint16_t *pu16Year, uint8_t *pui8Month, uint8_t *pui8Date, uint8_t *pui8Day, uint8_t *pui8Hour, uint8_t *pui8Min, uint8_t *pui8Sec)
{
	//***********************************
	//Get RTC data form external clock ic
	//ui8Time[0]: Year
	//ui8Time[1]: Month
	//ui8Time[2]: Date
	//ui8Time[3]: Day
	//ui8Time[4]: Hour
	//ui8Time[5]: Min
	//ui8Time[6]: Sec
	//************************************
	uint8_t ui8Time[7] = {0x00};
	uint8_t ui8DatBuf = 0x00;

	//Read Data
	CLK_S35190ARead(RealTime_Read, ui8Time, 7);

	//***************************************
	//Start Converting bit to valuable number
	//***************************************
	//year
	ui8DatBuf =  ExtRTCDecode(ui8Time[0]);
	(*pu16Year) = 2000 +  ui8DatBuf;

	//month
	ui8DatBuf =  ExtRTCDecode(ui8Time[1]);
	(*pui8Month) = ui8DatBuf;

	//date
	ui8DatBuf =  ExtRTCDecode(ui8Time[2]);
	(*pui8Date) = ui8DatBuf;

	//day
	ui8DatBuf =  ExtRTCDecode(ui8Time[3]);
	(*pui8Day) = ui8DatBuf;

	//hour
	ui8DatBuf =  ExtRTCDecode(ui8Time[4]);
	if(ui8DatBuf >= 40)
	{
		ui8DatBuf = ui8DatBuf - 40;
	}
	(*pui8Hour) = ui8DatBuf;

	//min
	ui8DatBuf =  ExtRTCDecode(ui8Time[5]);
	(*pui8Min) = ui8DatBuf;

	//sec
	ui8DatBuf = ExtRTCDecode(ui8Time[6]);
	 (*pui8Sec) = ui8DatBuf;
}

void ExtRTCWrite(uint16_t pu16Year, uint8_t pui8Month, uint8_t pui8Date, uint8_t pui8Day, uint8_t pui8Hour, uint8_t pui8Min, uint8_t pui8Sec)
{
	//***********************************
	//Set RTC data for external clock ic
	//ui8Time[0]: Year
	//ui8Time[1]: Month
	//ui8Time[2]: Date
	//ui8Time[3]: Day
	//ui8Time[4]: Hour
	//ui8Time[5]: Min
	//ui8Time[6]: Sec
	//************************************
	uint8_t ui8Time[7] = {0x00};

	//Set Data
	//Year
	ui8Time[0] = ExtRTCDEncode(pu16Year - 2000);
	//Month
	ui8Time[1] = ExtRTCDEncode(pui8Month);
	//Date
	ui8Time[2] = ExtRTCDEncode(pui8Date);
	//Day
	ui8Time[3] = ExtRTCDEncode(pui8Day);
	//Hour
	ui8Time[4] = ExtRTCDEncode(pui8Hour);
	//Min
	ui8Time[5] = ExtRTCDEncode(pui8Min);
	//Sec
	ui8Time[6] = ExtRTCDEncode(pui8Sec);

	 //Write Data
	 CLK_S35190ASend(RealTime_Write, ui8Time, 7);

}

uint8_t ExtRTCDecode(uint8_t ui8Byte)
{
	uint8_t ui8RetVal = 0x00;
	uint8_t B1 = 0;
	uint8_t B2 = 0;
	uint8_t B4 = 0;
	uint8_t B8 = 0;
	uint8_t B10 = 0;
	uint8_t B20 = 0;
	uint8_t B40 = 0;
	uint8_t B80 = 0;

	B1 = ((ui8Byte >> (7)) & 0x01);
	B2 = ((ui8Byte >> (6)) & 0x01);
	B4 = ((ui8Byte >> (5)) & 0x01);
	B8 = ((ui8Byte >> (4)) & 0x01);
	B10 = ((ui8Byte >> (3)) & 0x01);
	B20 = ((ui8Byte >> (2)) & 0x01);
	B40 = ((ui8Byte >> (1)) & 0x01);
	B80 = ((ui8Byte >> (0)) & 0x01);

	ui8RetVal = (B1 * 1) + (B2 * 2) + (B4 * 4) + (B8 * 8) + (B10 * 10) + (B20 * 20) + (B40 * 40) + (B80 * 80);
	return ui8RetVal;
}

uint8_t ExtRTCDEncode(uint8_t ui8Byte)
{
	uint8_t ui8LoopIdx = 0;
	uint8_t ui8DatBuf = 0;
	uint8_t ui8RetVal = 0;
	for(ui8LoopIdx = 0; ui8LoopIdx < 8; ui8LoopIdx++)
	{
		ui8DatBuf = 0;
		switch((7 -ui8LoopIdx))
		{
		case 0:
			//B1
			if(ui8Byte >= 1)
			{
				ui8DatBuf = 1;
				ui8Byte = ui8Byte - 1;
			}
			break;
		case 1:
			//B2
			if(ui8Byte >= 2)
			{
				ui8DatBuf = 1;
				ui8Byte = ui8Byte - 2;
			}
			break;
		case 2:
			//B4
			if(ui8Byte >= 4)
			{
				ui8DatBuf = 1;
				ui8Byte = ui8Byte - 4;
			}
			break;
		case 3:
			//B8
			if(ui8Byte >= 8)
			{
				ui8DatBuf = 1;
				ui8Byte = ui8Byte - 8;
			}
			break;
		case 4:
			//B10
			if(ui8Byte >= 10)
			{
				ui8DatBuf = 1;
				ui8Byte = ui8Byte - 10;
			}
			break;
		case 5:
			//B20
			if(ui8Byte >= 20)
			{
				ui8DatBuf = 1;
				ui8Byte = ui8Byte - 20;
			}
			break;
		case 6:
			//B40
			if(ui8Byte >= 40)
			{
				ui8DatBuf = 1;
				ui8Byte = ui8Byte - 40;
			}
			break;
		case 7:
			//B80
			if(ui8Byte >= 80)
			{
				ui8DatBuf = 1;
				ui8Byte = ui8Byte - 80;
			}
			break;
		default:
			//error!!!!
			break;
		}
		ui8DatBuf = (ui8DatBuf << (ui8LoopIdx));
		ui8RetVal = ui8RetVal | ui8DatBuf;
	}
	return ui8RetVal;
}

void ExtRTCInit(void)
{
	uint16_t year= 0;
	uint8_t  month = 0;
	uint8_t  date = 0;
	uint8_t  day = 0;
	uint8_t  hour = 0;
	uint8_t  min = 0;
	uint8_t  sec = 0;
	ExtRTCRead( &year, &month, &date, &day, &hour, &min, &sec);
	if(year == 2000)
	{
		year= 2016;
		month = 3;
		date = 1;
		day = 2;
		hour = 0;
		min = 0;
		sec = 0;
		ExtRTCWrite( year, month, date, day, hour, min, sec);
	}
}
