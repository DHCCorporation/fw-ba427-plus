//*****************************************************************************
//
// Source File: OTA_svc.h
// Description: OTA status handling service.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 11/19/18     Jerry.W        Created file.
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/driver/wifi_driver.h#2 $
// $DateTime: 2017/12/18 16:17:54 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#ifndef SERVICE_OTA_SVC_H_
#define SERVICE_OTA_SVC_H_

typedef enum{
	OTA_status_unchecked,	//haven't checked version with cloud.
	OTA_status_checking,
	OTA_status_check_fail,	//connect to cloud fail.
	OTA_status_lastest,		//nothing to be updated.
	OTA_status_preparing,	//will start to download at background.
	OTA_status_error,
	OTA_status_ready,		//download complete, ready to upgrade.
}OTA_STATUS_T;

typedef enum{
	OTA_event_new_FW_available,
	OTA_event_ready_to_upgrade,
	OTA_event_internet_unstable,
	OTA_event_FW_up_to_date,
}OTA_EVENT_T;

typedef struct{
	OTA_STATUS_T status;
	uint32_t 	 FW_to_download;
	uint32_t 	 FW_downloaded;
	uint32_t	 LCM_upload_total;
	uint32_t	 LCM_to_update;
	uint32_t	 Printer_upload_total;
	uint32_t	 Printer_to_update;
}OTA_Progress_t;


/*************************************************************************************************
 * @ Name		: OTA_service_init.
 * @ Brief 		: start the OTA service.
 * @ parameter	: event_cb -- callback function to notify application.
 *************************************************************************************************/
void OTA_service_init(void(*event_cb)(OTA_EVENT_T));

/*************************************************************************************************
 * @ Name		: OTA_service_refresh.
 * @ Brief 		: double check the FW version with cloud while the previous checking indicating
 * 				  the FW is latest.
 *************************************************************************************************/
void OTA_service_refresh();

/*************************************************************************************************
 * @ Name		: OTA_service_poll.
 * @ Brief 		: polling OTA service.
 *************************************************************************************************/
void OTA_service_poll();

/*************************************************************************************************
 * @ Name		: OTA_service_upgrade_start.
 * @ Brief 		: start the upgrade process.
 *************************************************************************************************/
bool OTA_service_upgrade_start();

/*************************************************************************************************
 * @ Name		: OTA_service_get_progress.
 * @ Brief 		: get OTA progress details.
 *************************************************************************************************/
OTA_Progress_t* OTA_service_get_progress();

/*************************************************************************************************
 * @ Name		: get_bootloader_version.
 * @ Brief 		: get boot loader version.
 *************************************************************************************************/
char* get_bootloader_version();

#endif /* SERVICE_OTA_SVC_H_ */
