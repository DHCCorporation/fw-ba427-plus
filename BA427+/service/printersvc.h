//*****************************************************************************
//
// Source File: printersvc.h
// Description: Prototype for functions to control printer
//
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 10/26/15     Vincent.T     Created file.
// 10/30/15     Vincent.T     1. Add #include wordbanksvc.h
//                            2. Add PrinterStartWB(printersvc access database in wordbank)
// 03/08/16     Vincnet.T     1. add PrintErrorHanlder() declaration.
// 03/16/16     Vincent.T     1. Re-define some functions.(More ezer to use)
//                            2. New func. PrinterPrintSeperate(); to print seperate line.
// 03/17/16     Vincent.T     1. New func. TestCodeGenerater(); to generate TestCode
//                            2. New func. ASCII2TestCode(); to search corresponding TestCode.
// 06/21/16     Vincent.T     1. Re-define PrinterSet();, PrinterStart();, PrinterStartWB(); and PrintBarCode(); to support float number heating time.
// 08/10/16     Vincent.T     1. Re-define PrinterSet() and PrinterStart(); to support flaot step-motor pulse width.
// 12/07/18     Henry.Y        New display api.
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/service/printersvc.h#5 $
// $DateTime: 2017/09/22 10:27:40 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#ifndef _PRINTERSVC_H_
#define _PRINTERSVC_H_

#include <stdbool.h>
#include <stdint.h>
#include "flashsvc.h"
#include "wordbanksvc.h"
#include "..\driver/mp205_prt.h"
#include "WordString.h"

/****************/
/*PRINTOUT STEP */
/****************/
typedef enum
{
	PTR_READY = 0,	/* Printer Ready Flag */
	PTR_START,		/* Field start flag */
	PTR_END			/* Field end flag */
}PRT_STATUS;

void PowerOn(void);
void PowerOff(void);
void FeedPaper(void);
void DataASCII2Byte(uint8_t *ui8DataSrc, uint8_t *ui8DataDest, uint8_t ui8DataLenSrc, uint8_t ui8DataLenDest);
void PrinterStepMotorControl(uint8_t ui8SMPhase, uint8_t ui8Delayms);
void PrinterSet(uint8_t *ui8DataBuffer, uint8_t ui8STBgroup, float HeatingTimems, float fSMTimens, uint8_t ui8Repeat);
void PrinterStart( uint8_t ui8PrtFldIdx, uint8_t ui8STBgroup, float HeatingTimems, float fSMTimens, uint8_t ui8Repeat);
void PrinterPrintSpace(uint32_t ui32SpaceNum);
void PrinterStartWB(uint8_t *pui8Src, uint8_t ui8STBgroup, float HeatingTimems, uint8_t ui8SMTimens, uint8_t ui8Repeat);
bool PrintErrorHanlder(void);
void PrintBarCode(uint8_t *pui8Src, uint8_t ui8STBgroup, float HeatingTimems, uint8_t ui8SMTimens, uint8_t ui8Repeat);
void PrinterPrintSeperate(uint32_t ui32Width);

void Printer_BT_Percent(uint32_t percent);

enum{
    arrow_left,
    arrow_middle,
    arrow_right
};
void Printer_SYS_Percent(uint32_t percent, uint8_t arrow);

void Printer_Dashed_line(uint16_t start, uint16_t end);

enum{
    Printe_align_right,
    Printe_align_left,
    Printe_align_central
};

void Printer_string(char* string, uint8_t repeat, uint8_t align);
void Printer_string2(char* string, uint8_t repeat, uint8_t align, uint8_t spaces);

#endif /* _PRINTERSVC_H_ */
