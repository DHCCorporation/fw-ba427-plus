//*****************************************************************************
//
// Source File: lcdsvc.c
// Description: Functions to Draw LCD Screen which data is read from Flash.
//				
//
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 06/30/15     William.H      Created file.
// 10/05/15     William.H      Check in BT2020 project which branched from LCDFontMaker(Pre-BT2020) by modifed related GPIOs.
// 02/02/16     Vincent.T      1. Ceate Different data for cranking voltage wave
//                             2. New LCDDrawCrankingVolt(); body
// 02/04/16     Vincent.T      1. New func. LCDDrawRipVolt() to display waveform.
// 09/11/18     Henry.Y        Add DrawPageInfo and YNAntiWhiteItem.
// 12/07/18     Henry.Y        Move LCDDrawCrankingVolt()/LCDDrawRipVolt()/DrawPageInfo()/YNAntiWhiteItem() to app layer.
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/service/lcdsvc.c#5 $
// $DateTime: 2017/09/22 10:27:40 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "lcdsvc.h"

uint8_t FlsReadBuf[1024] = {0};

void LcdDrawScreen( uint8_t ui8ScrnIdx, MOVEINBUFFER MoveInBufIdx)
{
	ExtFlsReadLCDScrnData( ui8ScrnIdx, FlsReadBuf, sizeof(FlsReadBuf)/sizeof(FlsReadBuf[0]));
	ST7565DrawScreen(FlsReadBuf, MoveInBufIdx);

	// Refresh the screen to see the results
    ST7565Refresh(MoveInBufIdx);
}

void LcdDrawScreenBuffer( uint8_t ui8ScrnIdx, MOVEINBUFFER MoveInBufIdx)
{
	ExtFlsReadLCDScrnData( ui8ScrnIdx, FlsReadBuf, sizeof(FlsReadBuf)/sizeof(FlsReadBuf[0]));
	ST7565DrawScreen(FlsReadBuf, MoveInBufIdx);
}

void LcdDrawScreen_ReverseType( uint8_t ui8ScrnIdx, MOVEINBUFFER MoveInBufIdx)
{
	
	ExtFlsReadLCDScrnData( ui8ScrnIdx, FlsReadBuf, sizeof(FlsReadBuf)/sizeof(FlsReadBuf[0]));
	ST7565DrawScreenInvertType(FlsReadBuf, MoveInBufIdx);

	// Refresh the screen to see the results
    ST7565Refresh(MoveInBufIdx);	
}

void LcdDrawScreenBuffer_ReverseType( uint8_t ui8ScrnIdx, MOVEINBUFFER MoveInBufIdx)
{
	ExtFlsReadLCDScrnData( ui8ScrnIdx, FlsReadBuf, sizeof(FlsReadBuf)/sizeof(FlsReadBuf[0]));
	ST7565DrawScreenInvertType(FlsReadBuf, MoveInBufIdx);
}



