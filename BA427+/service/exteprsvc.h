//*****************************************************************************
//
// Source File: exteprsvc.h
// Description: Functions to Read/Write/Chip Erase/Deep Power Down of 25LC512
//              EEPROM IC.
//
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 10/14/15     Vincent.T      Created file.
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/service/exteprsvc.h#5 $
// $DateTime: 2017/09/22 10:27:40 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#ifndef BT2100_WW_V00_L_SERVICE_EXTEPRSVC_H_
#define BT2100_WW_V00_L_SERVICE_EXTEPRSVC_H_

#include <stdbool.h>
#include <stdint.h>
#include "../driver/25lc512_epr.h"


// -CS Control
#define EERPOM_IC_CS_HIGH ROM_GPIOPinWrite(EPR_25LC512_BASE_LAT, EPR_25LC512_CS_PIN, 0xFF)
#define EERPOM_IC_CS_LOW ROM_GPIOPinWrite(EPR_25LC512_BASE_LAT, EPR_25LC512_CS_PIN, 0x00)

//25LC512 Cmd
#define EPR_WRITE		0x02
#define EPR_READ		0x03
#define EPR_WRDI		0x04
#define EPR_RDSR		0x05
#define EPR_WREN 		0x06
#define EPR_PAGE_ERASE	0x42
#define EPR_CHIP_ERASE	0xC7
#define EPR_SEC_ERASE	0xD8
#define EPR_DPD			0xB9
#define EPR_RDPD		0xAB

void ExtEprWriteSeriesData(uint8_t *ui8DataBuff, uint16_t ui16Addr, uint8_t ui8DataLen);
void ExtEprReadSeriesData(uint8_t *ui8DataBuff, uint16_t ui16Addr, uint8_t ui8DataLen);
void ExtEprReadStatReg(uint8_t *ui8StatReg);
void ExtEprWriteWREN(void);
void ExtEprWriteWRDI(void);
void ExtEprWaitBusy(void);
void ExtEprChkWel(void);
void ExtEprChipErase(void);
void ExtEprSectorErase(uint16_t ui16SectorNum);
void ExtEprPageErase(uint16_t ui16PageNum);
void ExtEprDPDMode(void);
void ExtEprRelDPDMode(void);

#endif /* BT2100_WW_V00_L_SERVICE_EXTEPRSVC_H_ */
