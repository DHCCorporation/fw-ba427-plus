//*****************************************************************************
//
// Source File: flashsvc.h
// Description: Prototype for functions to Read/Write A25L032 Flash memory for
//				LCD screen / Printer field data access.
//
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 06/25/15     William.H      Created file.
// 10/05/15     William.H      Check in BT2020 project which branched from LCDFontMaker(Pre-BT2020) by modifed related GPIOs.
// 10/13/15     Vincent.T      Add more flash services(focus on printer).
// 10/26/15     Vincent.T      Modify GetPrtOffset in Flash.
// 10/30/15     Vincent.T      1. Add PRT_MEASURED field in printout
// 01/06/16     William.H      Add "Portuguese" language.
// 01/30/16     Vincent.T      Add PRT_SSAGMF
// 02/16/16     Vincent.T      Add Priout field: RO, VIN
// 09/06/16     Vincent.T      Add printout field: PRT_IRTEST
// 11/15/16     Ian.C          1. Modify language address to make it work fine on multi-language.
// 01/13/17     Vincent.T      1. Modify PRT_FIELD_SEL(add PRT_PBDHC)
// 05/18/18     Jerry.W        add flash header structure and functions. 
// 09/11/18     Henry.Y        Add set_language/get_language_setting.
//                             Seperate battery test+start stop test and system test memory start address.
// 11/19/18     Henry.Y        typedef struct{...}FlashHeader_t add old_FW_ver.
// 11/20/18     Jerry          add non-blocking flash write function.
// 12/07/18     Jerry.W        modify flash layout.
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/service/flashsvc.h#5 $
// $DateTime: 2017/09/22 10:27:40 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#ifndef __FLASHSVC_H__
#define __FLASHSVC_H__

#include <stdbool.h>
#include <stdint.h>
#include "../driver/a25L032_fls.h"
#include "langaugesvc.h"


#define FLASH_SECTOR		4096
#define FLASH_PAGE			256


#define RECORD_UNIT_LENGTH	256
#define FLSAH_TOTAL_SIZE	16*1024*1024	//16M




#define LAYOUT_INDEX_HEADER			0x000400
#define IMAGE_HEADER				0x000800







typedef struct{
	uint8_t *LCM;			//0 -> unused, 0x57 -> version: 5, CRC: 7
	uint8_t *Printer;		//version = 1 ~ f
}IMAGE_VER_t;

typedef struct{
	uint32_t IMAGE_add;
	uint32_t IMAGE2_add;
	uint32_t IMAGE_sector_LCM_len;
	uint32_t IMAGE_LCM_len;
	uint32_t IMAGE_sector_Printer_len;
	uint32_t IMAGE_Printer_len;

	uint32_t TEST_RESULT_ADD;
	uint32_t TEST_RESULT_sector_len;

	uint32_t OTA_BUF_ADD;
	uint32_t OTA_BUF_sector_len;

}LAYOUT_INDEX_t;


typedef struct{
	uint32_t ID1;
	uint32_t ID2;
}UPGRADE_ID_t;

typedef struct {
	uint8_t status;
	uint32_t new_size;
	uint8_t old_ver[30];
	uint8_t	new_ver[30];
	uint8_t CRC;
	uint32_t index;
}OTA_status_t;

//flash header structure
typedef struct{
	uint8_t version[20];
	uint8_t OTA_update_flag;		//1 -> bootloader will start to upgrade FW.
	uint32_t OTA_New_FW_add;		//for bootloader to know where the new FW stored.
	uint32_t OTA_New_FW_size;		//for bootloader to know the length of new FW.
	UPGRADE_ID_t update_ID;			//for EC2 architecture only, AWS IoT didn't use this variable.
	uint8_t update_ID_send_flag;	//0 nothing, 1 needs send ID back to EC2 or send success/fail to IoT

	uint8_t image_mode;				//for image of section selection, 0 -> section1, 1 -> section2

	OTA_status_t OTA_FW_status;

	OTA_status_t OTA_FLS_IMAGE_status;

}FlashHeader_t;





typedef struct
{
	uint8_t		lanHandle;
	LANGUAGE	lanSel;
}
tLanguageSet;

/* Printerout Zone Selection */
/* BATTERY TEST */
typedef enum{
	PRT_LOGO = 0,		/* Logo */
	PRT_TESTREPORT = 2,		/* TEST REPORT */
	PRT_BATTERYTEST,	/* =BATTERY TEST= */
	PRT_STARTSTOP,		/* =START STOP= */
	PRT_STARTERTEST,	/* =STARTER TEST= */
	PRT_TESTCOUNTER,	/* TEST COUNTER */
	PRT_FLOODED,		/* FLOODED */
	PRT_AGMF,			/* AGMF */
	PRT_AGMS,			/* AGMS */
	PRT_VRLAGEL,		/* VRLA/GEL */
	PRT_EFB,			/* EFB */
	PRT_DIN,			/* DIN */
	PRT_EN,				/* EN */
	PRT_IEC,			/* IEC */
	PRT_JIS,			/* JIS */
	PRT_SAE,			/* SAE */
	PRT_VOLTAGE,		/* VOLTAGE: */
	PRT_RATED,			/* RATED: */
	PRT_TEMP,			/* TEMP: */
	PRT_GnP,			/* GOOD & PASS */
	PRT_GnR,			/* GOOD & RECHARGE */
	PRT_BnR,			/* BAD & REPLACE */
	PRT_CAU,			/* CAUTION */
	PRT_RnR,			/* RECHARGE & RETEST */
	PRT_BCR,			/* BAD CELL REPLACE */
	PRT_SOH,			/* STATE OF HEALTH */
	PRT_SOC,			/* STATE OF CHARGE */
	PRT_PERCENT_0,		/* BAR GRAPH 0% */
	PRT_PERCENT_10,		/* BAR GRAPH 10% */
	PRT_PERCENT_20,		/* BAR GRAPH 20% */
	PRT_PERCENT_30,		/* BAR GRAPH 30% */
	PRT_PERCENT_40, 	/* BAR GRAPH 40% */
	PRT_PERCENT_50, 	/* BAR GRAPH 50% */
	PRT_PERCENT_60, 	/* BAR GRAPH 60% */
	PRT_PERCENT_70, 	/* BAR GRAPH 70% */
	PRT_PERCENT_80,	 	/* BAR GRAPH 80% */
	PRT_PERCENT_90,	 	/* BAR GRAPH 90% */
	PRT_PERCENT_100,	/* BAR GRAPH 100% */
	PRT_0PA_100PA,		/* 0%        100% */
	PRT_CODE,			/* CODE */
	PRT_CLIENT,			/* CLIENT: */
	PRT_TESTDATE,		/* TEST DATE: */
	PRT_BY,				/* BY: */
	PRT_MEASURED,		/* MEASURED: */
	PRT_CHARGINGTEST,	/* =CHARGING TEST= */
	PRT_NOLOAD,			/* NO LOAD: */
	PRT_LOAD,			/* LOAD: */
	PRT_MINMAX,			/* MIN       MAX */
	PRT_LOADOFF,		/* LOAD OFF: */
	PRT_LOADON,			/* LOAD ON: */
	PRT_DIODERIPPLE,	/* DIODE RIPPLE: */
	PRT_HIGH,			/* HIGH */
	PRT_NORMAL,			/* NORMAL */
	PRT_LOW,			/* LOW */
	PRT_NODETECT,		/* NO DETECT */
	PRT_SSTEST,			/* SS. TEST: */
	PRT_BATTEST,		/* BAT. TEST: */
	PRT_SYSTEST,		/* SYS. TEST: */
	PRT_SSAGMF,			/* AGM FLAT PLATE for SS */
	PRT_RO,				/* RO NUMBER */
	PRT_VIN,			/* VIN_NUMBER */
	PRT_IRTEST,			/* ==IR TEST== */
	PRT_IRTESTCOUNT,	/* IR TEST: */
	PRT_PBDHC,			/* POWERED BY DHC */
}PRT_FIELD_SEL;

//
// Printer index is <BASE ADDRESS>
// Table of Language Culture Names
//




void ExtFlsWrite(uint32_t ui32SectAddr, uint8_t *ui8DatBuff, uint32_t ui32BufLen);
bool ExtFlsWrite_nonblocking(uint32_t ui32SectAddr, uint8_t *ui8DatBuff, uint32_t ui32BufLen, void(*done_cb)(void));
#define ExtFlsRead A25L032ReadData
//void A25L032ReadData(uint32_t ui32Addr, uint8_t *pui8RxBuf, uint32_t ui32Len)
void ExtFlsPoll();

extern void ExtFlsLCDUpdData(LANGUAGE langIdx, uint8_t ui8ScrnIdx, uint8_t *lcdScrnBuff, uint32_t bufLen);
extern void	ExtFlsReadLCDScrnData(uint8_t ui8ScrnIdx, uint8_t *lcdScrnBuff, uint32_t bufLen);

extern void ExtFlsReadPrtFldData( PRT_FIELD_SEL ui8PrtFldIdx, uint32_t ui32offset, uint8_t *ui8Databuf, uint32_t ui32DataLen);
extern bool ExtFlsPrtUpdData(uint8_t *ui8DatBuff, LANGUAGE langIdx, PRT_FIELD_SEL ui8PrtFldIdx);

void ExtFlsWriteRecord(uint32_t Addr,uint16_t idx, uint8_t* buf, uint32_t len);

//rewrite without erase
void ExtFlsReWriteRecord(uint32_t Addr,uint16_t idx, uint8_t* buf, uint32_t len);

void ExtFlsReadRecord(uint32_t Addr,uint16_t idx, uint8_t* buf, uint8_t offset, uint32_t len);

IMAGE_VER_t* ExtFls_get_image_ver(void);
void ExtFls_set_image_ver();

LAYOUT_INDEX_t* ExtFls_get_layout(void);
void ExtFls_set_layout();

FlashHeader_t* ExtFls_get_header(void);
void ExtFls_set_header(void* data, uint32_t length);



void ExtFls_header_init();

#endif // __FLASHSVC_H__

