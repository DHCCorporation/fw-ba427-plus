//*****************************************************************************
//
// Source File: cloud.c
// Description: software timer
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 01/12/18     Jerry.W        Created file.
// 02/14/18     Henry.Y        add HTTP status.
// 03/22/18     Jerry.W        add Time_adjust(), TIME_cb() for time adjustment.
// 05/18/18     Jerry.W        modify for changing of HTTP protocol with wifi module.
// 11/19/18     Henry.Y        (Jerry) Add MFC_upload mode. FW_check() add CLOUD_ACCOUNT upload.
// 12/07/18     Jerry.W        modify FW check function for FLASH update.
// 01/14/19     Jerry.W        add notifacation in "download_request()" while the function return false
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/driver/wifi_driver.h#2 $
// $DateTime: 2017/12/18 16:17:54 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#include "cloud.h"
#include "Json/jansson.h"
#include "wifi_service.h"
#include "HTTP_service.h"
#include "cloud_tranform.h"
#include "extrtcsvc.h"
#include "SystemInfosvs.h"
#include "AWS_IoT_svc.h"
								
/****************************************************************
*																*
*				down load battery information functions			*
*																*
****************************************************************/

enum HTTP_status_code{
	HTTP_OK = 200,
	HTTP_partial = 206,
	HTTP_sudo_OK = 209,
	HTTP_not_found = 404,
	HTTP_bad_req = 400,

};




bool timeout = false;
bool Batt_info_responsed = false;
BATTERY_INFO_T* Battery_info = NULL;
bool Batt_info_get;
uint8_t *VIN_get = NULL;
uint8_t VIN_length = 0;
uint16_t HTTP_GET_status;

uint16_t uploading_idx = 0;
bool uploading = false;

UPLOAD_MODE_t upload_mode = normal_upload;

const uint8_t AWS_EC2[] = "dhc1976.ddns.net";//"192.168.1.60";//

uint8_t Jason_header[] = "Content-Type: application/json; charset=utf-8\r\n";

static void timeout_handle(){
	timeout = true;
	UARTprintf("_TIME_OUT_\n");
}


static void response_handle(uint16_t status, uint16_t length, uint8_t *data){
	json_t *root = NULL, *temp;
	json_error_t error;

	if((!Battery_info)||(!VIN_get)) goto FAIL;
	
	HTTP_GET_status = status;

	if((!timeout)&&(!Batt_info_responsed)){
		Batt_info_responsed = true;
		if(status == HTTP_OK){
			Batt_info_get = true;
			json_set_alloc_funcs(prtect_malloc, protect_free);
			root = json_loads((char*)data, 0, &error);
			if(!json_is_object(root)){
				goto FAIL;
			}
			else{
				//get return Result
				temp = json_object_get(root, "result");
				if(!json_is_string(temp)){
					goto FAIL;
				}
				
				if(memcmp(json_string_value(temp),"success", 7 )){
					UARTprintf("response : %s\n", json_string_value(temp));
					goto FAIL;
				}
				
				//get return VIN
				temp = json_object_get(root, "vin");
				if(!json_is_string(temp)){
					goto FAIL;
				}
				//compare VIN with original request
				if(memcmp(json_string_value(temp),VIN_get, VIN_length )){
					goto FAIL;
				}
				
				//get battery type
				temp = json_object_get(root, "batteryType");
				if(!json_is_string(temp)){
					goto FAIL;
				}
				Battery_info->bat_type = string_to_bat_type(json_string_value(temp));

				Battery_info->test_type = (Battery_info->bat_type == EFB)? Start_Stop_Test:Battery_Test;
				
				//get rating
				temp = json_object_get(root, "rating");
				if(!json_is_string(temp)){
					goto FAIL;
				}
				Battery_info->rating = string_to_bat_rating(json_string_value(temp));
				
				//get CCA
				temp = json_object_get(root, "capacity");
				if(!json_is_number(temp)){
					goto FAIL;
				}
				Battery_info->CCA = json_number_value(temp);
				
				//get RC
				temp = json_object_get(root, "rc");
				if(!json_is_number(temp)){
					goto FAIL;
				}
				Battery_info->RC = json_number_value(temp);
			}
		}
		else{
			goto FAIL;
		}
		json_decref(root);
		return;
	}
	
FAIL :	
	Batt_info_get = false;
	if(root) json_decref(root);
	Battery_info = NULL;
	VIN_get = NULL;
	VIN_length = 0;
}



uint8_t GET_BAT_BY_VIN(uint8_t* VIN, uint8_t VIN_len, BATTERY_INFO_T* Bat_info, uint8_t wait_time){

	timeout = Batt_info_responsed = false;
	Battery_info = Bat_info;
	VIN_get = VIN;
	VIN_length = VIN_len;
	
	
	uint8_t *get_URL = malloc(VIN_len + 6);
	if(get_URL == 0){
		return 1;
	}
	
	memcpy(get_URL, "/vin/", 5 );
	memcpy(get_URL + 5, VIN, VIN_len);
	get_URL[VIN_len + 5] = 0x00;
	
	
	HTTP_request(GET, (uint8_t *)AWS_EC2, 443, get_URL,  Jason_header, NULL, true, false, response_handle);
	
	free(get_URL);
	
	set_network_timout(wait_time, timeout_handle);
	

	
	//wait response or timeout 
	while(!timeout && !Batt_info_responsed){
		wifi_service_poll();
	}
	
	if(Batt_info_responsed){
		if(HTTP_GET_status != HTTP_not_found){
		if(Batt_info_get == true){
			return 0;
		}
		else{
			return 2;
		}
	}
	else{
		return 1;
	}
}
	else{
		return 1;
	}
}



/****************************************************************
*																*
*					upload test records functions				*
*																*
****************************************************************/
void (*post_callback)(bool , uint8_t);

void post_response_cb(uint16_t status, uint16_t len, uint8_t *data){
	if(post_callback) post_callback((status == HTTP_OK)||(status == HTTP_sudo_OK), Fail_reason_response_err);

}



bool Upload_test_record(TEST_RECORD_T *rec, char* account, void(*done_cb)(bool ,uint8_t)){
	json_set_alloc_funcs(prtect_malloc, protect_free);
	if((rec->Test_Type == Start_Stop_Test)||(rec->Test_Type == Battery_Test)){
		BATTERY_SARTSTOP_RECORD_T* record = (BATTERY_SARTSTOP_RECORD_T*) rec;
		char test_type[20];
		char test_time[20];
		char batt_type[10];
		char rating[10];
		char judgement[20];
		char RC_judgement[10];
		
		if(!test_type_to_string(record->Test_Type, test_type)) return false;
		if(!Bat_type_to_sting(record->Battery_Type, batt_type)) return false;
		if(!TIME_TO_STRING(&record->Test_Time, test_time)) return false;
		if(!Rating_to_sting(record->Ratting, rating)) return false;
		if(!Judgement_to_sting(record->Judgement, judgement)) return false;
		if(!RC_judge_to_string(record->RC_judegment, RC_judgement)) return false;
		
		json_t* root = json_object();
		json_object_set_new(root,"testType",json_string(test_type));
		json_object_set_new(root,"testTime",json_string(test_time));
		json_object_set_new(root,"vin",json_string((char*)record->VIN));
		json_object_set_new(root,"ro",json_string((char*)record->RO));
		
		json_object_set_new(root,"batteryType",json_string(batt_type));
		json_object_set_new(root,"voltage",json_real(record->Voltage));
		json_object_set_new(root,"rating",json_string(rating));
		json_object_set_new(root,"ccaCapacity",json_integer(record->CCA_Capacity));
		json_object_set_new(root,"measuredCca",json_integer(record->CCA_Measured));
		json_object_set_new(root,"judgement",json_string(judgement));
		json_object_set_new(root,"soh",json_integer(record->SOH));
		json_object_set_new(root,"soc",json_integer(record->SOC));
		json_object_set_new(root,"setRc",json_integer(record->RC_Set));
		json_object_set_new(root,"measuredRc",json_integer(record->RC_Measured));
		json_object_set_new(root,"rcJudgement",json_string(RC_judgement));
		json_object_set_new(root,"testCode",json_string((char*)record->Test_code));
		
		json_object_set_new(root,"temperature",json_real(record->Temperature));
		json_object_set_new(root,"account",json_string(account));
		
		if(record->IsBatteryCharged <= 1){	//0 or 1
			json_object_set_new(root,"batteryCharged",json_integer(record->IsBatteryCharged));
		}
		
		post_callback = done_cb;
		
		if(!HTTP_request(POST, (uint8_t*)AWS_EC2, 443, "/record", Jason_header, (uint8_t*)json_dumps(root, JSON_INDENT(1)), true, false, post_response_cb)){
			if(post_callback) post_callback(false, Fail_reason_network_err);
		}
		json_decref(root);
	}
	else if(rec->Test_Type == System_Test){
		SYSTEM_TEST_RECORD_T* record = (SYSTEM_TEST_RECORD_T*) rec;
		char test_type[20];
		char test_time[20];
		char crank_rlt[15];
		char idle_rlt[15];
		char ripple_rlt[15];
		char loaded_rlt[15];
		
		if(!test_type_to_string(record->Test_Type, test_type)) return false;
		if(!TIME_TO_STRING(&record->Test_Time, test_time)) return false;
		if(!SYSRANK_to_string(record->Crank_result, crank_rlt)) return false;
		if(!SYSRANK_to_string(record->Idle_result, idle_rlt)) return false;
		if(!SYSRANK_to_string(record->Ripple_result, ripple_rlt)) return false;
		if(!SYSRANK_to_string(record->Loaded_result, loaded_rlt)) return false;
		
		json_t* root = json_object();
		json_object_set_new(root,"testType",json_string(test_type));
		json_object_set_new(root,"testTime",json_string(test_time));
		json_object_set_new(root,"vin",json_string((char*)record->VIN));
		json_object_set_new(root,"ro",json_string((char*)record->RO));
		
		json_object_set_new(root,"crankResult",json_string(crank_rlt));
		json_object_set_new(root,"crankVoltage",json_real(record->Crank_voltage));
		json_object_set_new(root,"idleResult",json_string(idle_rlt));
		json_object_set_new(root,"idleVoltage",json_real(record->Idle_voltage));
		json_object_set_new(root,"rippleResult",json_string(ripple_rlt));
		json_object_set_new(root,"rippleVoltage",json_real(record->Ripple_voltage));
		json_object_set_new(root,"loadedResult",json_string(loaded_rlt));
		json_object_set_new(root,"loadedVoltage",json_real(record->Loaded_voltage));
		
		json_object_set_new(root,"temperature",json_real(record->Temperature));
		json_object_set_new(root,"account",json_string(account));
		
		post_callback = done_cb;
		if(!HTTP_request(POST, (uint8_t*)AWS_EC2, 443, "/record", Jason_header, (uint8_t*)json_dumps(root, JSON_INDENT(1)), true, false, post_response_cb)){
			if(post_callback) post_callback(false, Fail_reason_network_err);
			json_decref(root);
			return false;
		}
		json_decref(root);
	}
	else{
		return false;
	}
	
	return true;
}
	
uint16_t find_first_to_upload(uint16_t max, uint16_t min){
	uint16_t mid, lower;
	uint8_t mid_s, lower_s;
	if((min+1) == max) return max;
	
	mid = (min+max)/2;
	mid_s = record_status(mid);
	if(mid_s == Uploaded_record) return find_first_to_upload(max, mid);
	else if(mid_s == Wait_uplaod) return find_first_to_upload(mid, min);
	
	lower = mid-1;
	while(lower != min){
		lower_s = record_status(lower);
		if(lower_s != No_upload) break;
		lower--;
	}
	
	if((lower == min)||(lower_s == Uploaded_record)) return find_first_to_upload(max, mid);
	else return find_first_to_upload(lower, min);
	
}

uint16_t get_saved_record_count(void){
	uint16_t last_idx, first_idx, upload_idx;
	uint16_t upload_counter = 0;
	uint16_t total_record;
	SYS_INFO* info = Get_system_info();
	total_record = info->TotalTestRecord + info->TestRecordOffset;

	if(total_record == 0) return 0;
	last_idx = total_record-1;
CHECK_LAST:
	if(record_status(last_idx) == Uploaded_record){
		return 0;
	}
	else if(record_status(last_idx) == No_upload){
		if(last_idx != 0){
			last_idx--;
			goto CHECK_LAST;
		}
		else{
			return 0;
		}
	}

	first_idx = (total_record>MAX_BTSSTESTCOUNTER)? total_record-MAX_BTSSTESTCOUNTER : 0;
	if(record_status(first_idx) == Wait_uplaod){
		upload_idx = first_idx;
	}
	else{
		upload_idx = find_first_to_upload(last_idx, first_idx);
	}

	while(upload_idx <= last_idx){
		if(record_status(upload_idx) == Wait_uplaod) upload_counter++;
		upload_idx++;
	}
	return upload_counter;
}

void (*upload_state)(bool , uint8_t );

void update_cb(bool succeed, uint8_t reason){
	if(succeed&uploading){
		uploading = false;
		UARTprintf("_______________________________________upload %d succeed!\n",uploading_idx);
		set_uploaded(uploading_idx);
		upload_recod(upload_state);
	}
	else{
		UARTprintf("_______________________________________upload %d FAIL!!!\n",uploading_idx);
		uploading = false;
	}

	if(upload_state) upload_state(succeed, reason);
}


bool upload_recod(void(*state)(bool ,uint8_t)){
	uint16_t last_idx, first_idx;
	uint8_t last_record_status;
	uint16_t total_record;
	SYS_INFO* info = Get_system_info();
	total_record = info->TotalTestRecord + info->TestRecordOffset;
	
	if(uploading) return false;
	
	upload_state = state;

	if(total_record == 0) return false;
	last_idx = total_record-1;
CHECK_LAST:
	last_record_status = record_status(last_idx);
	if(last_record_status == Uploaded_record){
		UARTprintf("_______________________________________nothing to be uploaded\n");
		return false;
	}
	else if(last_record_status == No_upload){
		last_idx--;
		goto CHECK_LAST;
	}
	
	first_idx = (total_record>MAX_BTSSTESTCOUNTER)? total_record-MAX_BTSSTESTCOUNTER : 0;
	if(record_status(first_idx) == Wait_uplaod){
		uploading_idx = first_idx;
	}
	else{
		uploading_idx = find_first_to_upload(last_idx, first_idx);
	}
	//upload uploading_idx
	uint8_t ui8RecordBuf[RECORD_UNIT_LENGTH];
	TestRecordRead(uploading_idx, ui8RecordBuf);
	uploading = true;

	UARTprintf("_______________________________________uploading : %d \n",uploading_idx);
#if AWS_IOT_SERVICE
	if(!AWS_IoT_Upload_test_record((TEST_RECORD_T *)ui8RecordBuf,  update_cb, upload_mode)){
#else
	bool upload_result;
	if(upload_mode == normal_upload){
		upload_result = Upload_test_record((TEST_RECORD_T *)ui8RecordBuf, CLOUD_ACCOUNT, update_cb);
	}
	else{
		upload_result = AWS_IoT_Upload_test_record((TEST_RECORD_T *)ui8RecordBuf,  update_cb, upload_mode);
	}
	if(!upload_result){
#endif
		//handle record format error here.
		uploading = false;
		return false;
	}
	
	return true;
}

/****************************************************************
*																*
*						download  functions						*
*																*
****************************************************************/
bool downloading = false;
void (*download_result_cb)(bool, uint16_t , uint8_t*) = NULL;

void download_cb(uint16_t status, uint16_t len, uint8_t *data){
	UARTprintf("[download] s = %d\n", status);
	if(downloading){
		downloading = false;
		if((status>=200)&&(status < 300)){
			if(download_result_cb) download_result_cb(true, len, data);
		}
		else if(status == 416){	//while file length = 1024*n
			if(download_result_cb) download_result_cb(true, 0, NULL);
		}
		else{
			if(download_result_cb) download_result_cb(false, len, data);
		}
	}
}

bool download_request(uint8_t *server, uint8_t* FW_URL, uint32_t start, uint32_t end, void(*done_cb)(bool, uint16_t , uint8_t*)){
	uint8_t D_header[40];
	
	if(downloading){
		done_cb(false, 0, 0);
		return false;
	}
	
	download_result_cb = done_cb;
	

	sprintf((char*)D_header, "Range: bytes=%d-%d\r\n", start, end);

	downloading = true;
	if(!HTTP_request(GET, server, 443, FW_URL, D_header, NULL, true, false, download_cb)){
		downloading = false;
		done_cb(false, 0, 0);
		return false;
	}
	return true;
}



/****************************************************************
*																*
*							SNTP functions						*
*																*
****************************************************************/
int16_t time_adjust;
void TIME_cb(uint8_t* time_string){
	uint16_t year, month , day, hour, minut, secend;
	char* temp = (char*)time_string;
	UARTprintf("get time : %s\n", time_string);
	year = atoi(temp);

	temp = (strstr(temp, "/") +1);
	month = atoi(temp);

	temp = (strstr(temp, "/") +1);
	day = atoi(temp);

	temp = (strstr(temp, " ") +1);
	hour = atoi(temp);

	temp = (strstr(temp, ".") +1);
	minut = atoi(temp);

	temp = (strstr(temp, ".") +1);
	secend = atoi(temp);



	int8_t adj_minute = time_adjust % 60;
	if(adj_minute != 0){
		if(adj_minute > 0){
			minut += adj_minute;
			if(minut >= 60){
				minut -= 60;
				hour++;
				if(hour >= 24){
					hour = 0;
					day++;
					switch(month){
					case 1:
					case 3:
					case 5:
					case 7:
					case 8:
					case 10:
					case 12:
						if(day > 31){
							day = 1;
							month++;
						}
						break;
					case 2:
						if(year%4 == 0){
							if(day > 29){
								day = 1;
								month++;
							}
						}
						else{
							if(day > 28){
								day = 1;
								month++;
							}
						}

						break;
					case 4:
					case 6:
					case 9:
					case 11:
						if(day > 30){
							day = 1;
							month++;
						}
						break;
					}

					if(month > 12){
						month = 1;
						year++;
					}
				}
			}
		}
		else{	// adj_minute < 0
			if((adj_minute + (int8_t) minut)<0){
				minut = minut+adj_minute+60;
				if(hour == 0){
					hour = 23;
					if(day == 1){
						month--;
						if(month == 0){
							month = 12;
							year--;
						}
						switch(month){
						case 1:
						case 3:
						case 5:
						case 7:
						case 8:
						case 10:
						case 12:
							day = 31;
							break;
						case 2:
							if(year%4 == 0){
								day = 29;
							}
							else{
								day = 28;
							}
							break;
						case 4:
						case 6:
						case 9:
						case 11:
							day = 30;
							break;
						}
					}
					else{
						day--;
					}
				}
				else{
					hour--;
				}
			}
			else{
				minut += adj_minute;
			}
		}
	}

	UARTprintf("%d/%d/%d %d.%d.%d\n", year, month , day, hour, minut, secend);

	ExtRTCWrite(year, month, day, 0, hour, minut, secend);
}

void Time_adjust(int16_t TZ){
	time_adjust = TZ;
	Get_internet_time(time_adjust/60, TIME_cb);
}



/****************************************************************
*																*
*						FW version check functions				*
*																*
****************************************************************/
const char *item[3] = {"newVersion","url","note"};
#if !AWS_IOT_SERVICE
void (*FW_check_result_cb)(uint8_t FW_result, OTA_FW_t* OTA_FW,uint8_t FLS_result, OTA_FW_t* OTA_FLS);
static bool FW_checking = false;

void FW_check_response_cb(uint16_t status, uint16_t len,uint8_t *data){
	json_t *root = NULL, *temp, *leaf;
	json_error_t error;
	uint8_t i,result_FW, result_FLS, len_temp, **des_temp;
	OTA_FW_t *New_FW = NULL;
	OTA_FW_t *New_FLS = NULL;

	FW_checking = false;

	UARTprintf("%s\n",data);

	if(status == HTTP_OK){

		json_set_alloc_funcs(prtect_malloc, protect_free);

		root = json_loads((char*)data, 0, &error);

		/*FW version processing*/
		leaf = json_object_get(root, "fw");
		if(!json_is_object(leaf)){
			goto FAIL;
		}

		temp = json_object_get(leaf, "update");
		if(!json_is_string(temp)){
			goto FAIL;
		}

		if(memcmp(json_string_value(temp),"yes", 3 ) == 0){
			result_FW = CK_OTA_demand_yes;

		}
		else if(memcmp(json_string_value(temp),"no", 2 ) == 0){
			result_FW = CK_OTA_demand_no;
			result_FLS = CK_OTA_demand_no;
			goto CB;
		}
		else if(memcmp(json_string_value(temp),"must", 4 ) == 0){
			result_FW = CK_OTA_demand_must;
		}
		else{
			goto FAIL;
		}


		New_FW = malloc(sizeof(OTA_FW_t));
		memset(New_FW, 0, sizeof(OTA_FW_t));


		for(i = 0; i < 3; i++){
			temp = json_object_get(leaf, item[i]);
			if(!json_is_string(temp)){
				goto FAIL;
			}
			len_temp = json_string_length(temp)+1;

			des_temp = (i == 0)? &New_FW->version :
					   (i == 1)? &New_FW->URL : &New_FW->Note;
			*des_temp = malloc(len_temp);
			memcpy(*des_temp, json_string_value(temp), len_temp);
		}

		temp = json_object_get(leaf, "crc");
		if(!json_is_integer(temp)){
			goto FAIL;
		}
		New_FW->CRC = json_integer_value(temp);



		temp = json_object_get(leaf, "length");
		if(!json_is_integer(temp)){
			goto FAIL;
		}
		New_FW->FW_len = json_integer_value(temp);

		/*FLS version processing*/
		leaf = json_object_get(root, "fls");
		if(!json_is_object(leaf)){
			goto FAIL;
		}

		temp = json_object_get(leaf, "update");
		if(!json_is_string(temp)){
			goto FAIL;
		}

		if(memcmp(json_string_value(temp),"yes", 3 ) == 0){
			result_FLS = CK_OTA_demand_yes;

		}
		else if(memcmp(json_string_value(temp),"no", 2 ) == 0){
			result_FLS = CK_OTA_demand_no;
			goto CB;
		}
		else if(memcmp(json_string_value(temp),"must", 4 ) == 0){
			result_FLS = CK_OTA_demand_must;
		}
		else{
			goto FAIL;
		}


		New_FLS = malloc(sizeof(OTA_FW_t));
		memset(New_FLS, 0, sizeof(OTA_FW_t));


		for(i = 0; i < 3; i++){
			temp = json_object_get(leaf, item[i]);
			if(!json_is_string(temp)){
				goto FAIL;
			}
			len_temp = json_string_length(temp)+1;

			des_temp = (i == 0)? &New_FLS->version :
					(i == 1)? &New_FLS->URL : &New_FLS->Note;
			*des_temp = malloc(len_temp);
			memcpy(*des_temp, json_string_value(temp), len_temp);
		}


		goto CB;
	}


FAIL :
	if(FW_check_result_cb) FW_check_result_cb(CK_OTA_demand_internet_fail, NULL,CK_OTA_demand_internet_fail, NULL);
	if(root) json_decref(root);
	if(New_FW){
		if(New_FW->version) free(New_FW->version);
		if(New_FW->URL) free(New_FW->URL);
		if(New_FW->Note) free(New_FW->Note);
		free(New_FW);
	}
	if(New_FLS){
		if(New_FLS->version) free(New_FLS->version);
		if(New_FLS->URL) free(New_FLS->URL);
		if(New_FLS->Note) free(New_FLS->Note);
		free(New_FLS);
	}
	return;

CB:
	if(FW_check_result_cb) FW_check_result_cb(result_FW, New_FW, result_FLS, New_FLS);
	if(root) json_decref(root);

}


bool FW_check(uint8_t *version, uint8_t *SN, void(*rslt)(uint8_t FW_result, OTA_FW_t* OTA_FW,uint8_t FLS_result, OTA_FW_t* OTA_FLS) ){

	if(version == NULL || SN == NULL || rslt == NULL){
		return false;
	}

	if(FW_checking) return false;

	FlashHeader_t* FH = ExtFls_get_header();

	FW_checking = true;


	FW_check_result_cb = rslt;

	json_set_alloc_funcs(prtect_malloc, protect_free);
	json_t* root = json_object();

	json_object_set_new(root,"fwVersion",json_string((char*)version));
	json_object_set_new(root,"flsVersion",json_string((char*)FH->version));
	json_object_set_new(root,"sn",json_string((char*)SN));
	json_object_set_new(root,"account",json_string(CLOUD_ACCOUNT));

	if(!HTTP_request(POST, (uint8_t*)AWS_EC2, 443, "/ota/check", Jason_header, (uint8_t*)json_dumps(root, JSON_INDENT(1)), true, false, FW_check_response_cb)){
		if(FW_check_result_cb) FW_check_result_cb(CK_OTA_demand_internet_fail, NULL,CK_OTA_demand_internet_fail, NULL);
		FW_checking = false;
	}
	json_decref(root);

	return true;
}
#endif


/****************************************************************
*																*
*						get FW list functions					*
*																*
****************************************************************/
void (*get_FW_list_result_cb)(bool result, OTA_FW_LIST_t* OTA_FW_t);

void get_FW_list_response_cb(uint16_t status, uint16_t len,uint8_t *data){
	json_t *root = NULL, *temp, *FW_o, *FW_list_o;
	json_error_t error;
	uint8_t i, j, len_temp, **des_temp, len_list;
	OTA_FW_LIST_t *FW_list = NULL;
	OTA_FW_t *New_FW = NULL;

	if(status == HTTP_OK){

		json_set_alloc_funcs(prtect_malloc, protect_free);
		root = json_loads((char*)data, 0, &error);
		if(!json_is_object(root)){
			goto FAIL;
		}

		FW_list_o = json_object_get(root, "fwList");
		if(!json_is_array(FW_list_o)){
			goto FAIL;
		}

		len_list = json_array_size(FW_list_o);

		FW_list = malloc(sizeof(OTA_FW_LIST_t) + len_list*4);
		memset(FW_list, 0, sizeof(OTA_FW_LIST_t) + len_list*4);

		FW_list->number = len_list;

		for(j = 0; j < len_list ; j++){
			FW_o = json_array_get(FW_list_o, j);
			if(!json_is_object(FW_o)){
				goto FAIL;
			}

			New_FW = malloc(sizeof(OTA_FW_t));
			memset(New_FW, 0, sizeof(OTA_FW_t));
			FW_list->FW[j] = New_FW;


			for(i = 0; i < 3; i++){
				temp = json_object_get(FW_o, item[i]);
				if(!json_is_string(temp)){
					goto FAIL;
				}
				len_temp = json_string_length(temp)+1;

				des_temp = (i == 0)? &New_FW->version :
						(i == 1)? &New_FW->URL : &New_FW->Note;
				*des_temp = malloc(len_temp);
				memcpy(*des_temp, json_string_value(temp), len_temp);
			}
		}
		goto CB;
	}


FAIL :
	if(get_FW_list_result_cb) get_FW_list_result_cb(false, NULL);
	if(root) json_decref(root);
	if(FW_list){
		for(j = 0; j < FW_list->number ; j++){
			if(FW_list->FW[j]){
				if(FW_list->FW[j]->version) free(FW_list->FW[j]->version);
				if(FW_list->FW[j]->URL) free(FW_list->FW[j]->URL);
				if(FW_list->FW[j]->Note) free(FW_list->FW[j]->Note);
				free(FW_list->FW[j]);
			}
			else break;
		}
		free(FW_list);
	}
	return;

CB:
	if(get_FW_list_result_cb) get_FW_list_result_cb(true, FW_list);
	if(root) json_decref(root);

}

bool get_FW_list(uint8_t *version, uint8_t *SN, void(*rslt)(bool result, OTA_FW_LIST_t* FW_list)){
	if(version == NULL || SN == NULL || rslt == NULL){
		return false;
	}


	get_FW_list_result_cb = rslt;

	json_set_alloc_funcs(prtect_malloc, protect_free);
	json_t* root = json_object();

	json_object_set_new(root,"fwVersion",json_string((char*)version));
	json_object_set_new(root,"sn",json_string((char*)SN));

	if(!HTTP_request(POST, (uint8_t *)AWS_EC2, 443, "/ota/list", Jason_header, (uint8_t*)json_dumps(root, JSON_INDENT(1)), true, false, get_FW_list_response_cb)){
		if(get_FW_list_result_cb) get_FW_list_result_cb(false, NULL);
	}
	json_decref(root);

	return true;
}


/****************************************************************
*																*
*						get upgrade ID functions				*
*																*
****************************************************************/
void (*get_Upgrade_ID_result_cb)(bool result, UPGRADE_ID_t* ID);

void get_Upgrade_ID_response_cb(uint16_t status, uint16_t len,uint8_t *data){
	json_t *root = NULL, *id1, *id2;
	json_error_t error;

	if(status == HTTP_OK){

		json_set_alloc_funcs(prtect_malloc, protect_free);
		root = json_loads((char*)data, 0, &error);
		if(!json_is_object(root)){
			goto FAIL;
		}

		id1 = json_object_get(root, "updateId1");
		if(!json_is_integer(id1)){
			goto FAIL;
		}
		id2 = json_object_get(root, "updateId2");
		if(!json_is_integer(id2)){
			goto FAIL;
		}

		UPGRADE_ID_t *ID = malloc(sizeof(UPGRADE_ID_t));
		ID->ID1 = (uint32_t)json_integer_value(id1);
		ID->ID2 = (uint32_t)json_integer_value(id2);

		if(get_Upgrade_ID_result_cb) get_Upgrade_ID_result_cb(true, ID);
		if(root) json_decref(root);

		return;

	}

FAIL :
	if(root) json_decref(root);
	if(get_Upgrade_ID_result_cb) get_Upgrade_ID_result_cb(false, NULL);

}


bool get_Upgrade_ID(uint8_t *version, uint8_t *SN,  int16_t Time_zone, void(*rslt)(bool result, UPGRADE_ID_t* ID)){
	if(version == NULL || SN == NULL || rslt == NULL){
		return false;
	}

	char time[30];
	uint16_t year;
	uint8_t  month;
	uint8_t  date;
	uint8_t  day;
	uint8_t  hour;
	uint8_t  min;
	uint8_t  sec;

	ExtRTCRead( &year, &month, &date, &day, &hour, &min, &sec);

	sprintf(time, "%4d-%02d-%02dT%02d:%02d:%02d%+02d:%02d", year, month, date, hour, min, sec , Time_zone/60, Time_zone%60);

	get_Upgrade_ID_result_cb = rslt;

	json_set_alloc_funcs(prtect_malloc, protect_free);
	json_t* root = json_object();

	json_object_set_new(root,"fwVersion",json_string((char*)version));
	json_object_set_new(root,"sn",json_string((char*)SN));
	json_object_set_new(root,"time",json_string((char*)time));



	if(!HTTP_request(POST, (uint8_t *)AWS_EC2, 443, "/ota/downloadFinish", Jason_header, (uint8_t*)json_dumps(root, JSON_INDENT(1)), true, false, get_Upgrade_ID_response_cb)){
		if(get_Upgrade_ID_result_cb) get_Upgrade_ID_result_cb(false, NULL);
	}
	json_decref(root);

	return true;
}



/****************************************************************
*																*
*						send upgrade ID functions				*
*																*
****************************************************************/
void (*send_Upgrade_ID_result_cb)(bool result);

void send_Upgrade_ID_response_cb(uint16_t status, uint16_t len,uint8_t *data){
	json_t *root = NULL, *temp;
	json_error_t error;
	bool result;

	if(status == HTTP_OK){

		json_set_alloc_funcs(prtect_malloc, protect_free);
		root = json_loads((char*)data, 0, &error);
		if(!json_is_object(root)){
			goto FAIL;
		}

		temp = json_object_get(root, "response");
		if(!json_is_string(temp)){
			goto FAIL;
		}

		if(memcmp(json_string_value(temp),"success", 3 ) == 0){
			result = true;
		}
		else{
			result = false;
		}

		if(send_Upgrade_ID_result_cb) send_Upgrade_ID_result_cb(result);
		if(root) json_decref(root);
		return;
	}


FAIL :
	if(root) json_decref(root);
	if(send_Upgrade_ID_result_cb) send_Upgrade_ID_result_cb(false);

}

bool send_Upgrade_ID(UPGRADE_ID_t *ID, int16_t Time_zone, void(*rslt)(bool result)){
	if(ID == NULL || rslt == NULL){
		return false;
	}

	char time[30];
	uint16_t year;
	uint8_t  month;
	uint8_t  date;
	uint8_t  day;
	uint8_t  hour;
	uint8_t  min;
	uint8_t  sec;

	ExtRTCRead( &year, &month, &date, &day, &hour, &min, &sec);

	sprintf(time, "%4d-%02d-%02dT%02d:%02d:%02d%+02d:%02d", year, month, date, hour, min, sec , Time_zone/60, Time_zone%60);

	send_Upgrade_ID_result_cb = rslt;

	json_set_alloc_funcs(prtect_malloc, protect_free);
	json_t* root = json_object();

	json_object_set_new(root,"updateId1",json_integer((json_int_t)ID->ID1));
	json_object_set_new(root,"updateId2",json_integer((json_int_t)ID->ID2));
	json_object_set_new(root,"time",json_string((char*)time));


	if(!HTTP_request(POST, (uint8_t *)AWS_EC2, 443, "/ota/updateFinish", Jason_header, (uint8_t*)json_dumps(root, JSON_INDENT(1)), true, false, send_Upgrade_ID_response_cb)){
		if(send_Upgrade_ID_result_cb) send_Upgrade_ID_result_cb(false);
	}
	json_decref(root);

	return true;
}


void set_upload_recod_mode(UPLOAD_MODE_t mode){
	upload_mode = mode;
	if(mode == normal_upload){
		set_WIFI_blink(true);
	}
	else{
		set_WIFI_blink(false);
	}
}

UPLOAD_MODE_t get_upload_recod_mode(void){
	return upload_mode;
}


