//*****************************************************************************
//
// Source File: scanner_svc.c
// Description: Scanner UART service API.
//
//
// Edit History:
//
// when 		who 		   what, where, why  
// -------- 	----------	   ------------------------------------------------
// 01/13/18 	Henry.Y 	   Create file.
// 03/23/18     Jerry.W        optimize buffering mechanism, make it can receive asynchronous data.
// 03/30/18     Jerry.W        fix QRcode over length issue.
// 11/19/18     Henry.Y        Add set_scanner_cb. Move callback function to app layer.
// 02/24/20     Henry.Y        Scanner control modification.
// =============================================================================
//
//
// $Header$
// $DateTime$
// $Author$
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#include <stdio.h>
#include "scanner_svc.h"

#if 1//1: disable UARTprintf
#undef UARTprintf
#define UARTprintf(fmt, ...)
#endif


void (*scanner_cb)(bool, uint8_t);

// Champtek MIR3+ setting command
// CMD protocol
//							  	STX| CMD| ETX| CHS
const char MIR3P_CMD_GetFWver[4] = 		{0x02,0x85,0x03,0x84};
//const char MIR3P_CMD_Trigger[4] = 		{0x02,0x82,0x03,0x83};
const char MIR3P_CMD_Reboot[4] = 		{0x02,0xA1,0x03,0xA0};
const char MIR3P_CMD_GetRdMode[6] = 	{0x02,0x21,0x00,0xA6,0x03,0x86};
const char MIR3P_CMD_SAVECONFIG[4] = 	{0x02,0xA7,0x03,0xA6};
const char MIR3P_CMD_SETGRDMODE[7] = 	{0x02,0x12,0x01,0x15,0x01,0x03,0x06};// good-read off
//const char MIR3P_CMD_SETTRDMODE[7] = 	{0x02,0x12,0x01,0x15,0x00,0x03,0x07};// Trigger on/off mode
const char MIR3P_RSP_GetRdMode_chk[] = {0x06, 0x02, 0x22, 0x01, 0xA6, 0x01, 0x03, 0x85};
const char MIR3P_CMD_FWver_head_chk[] = {0x06, 0x02, 0x06, 0x1d};
const char MIR3P_CMD_FWver_chk_1[] = "RZAV1.01.01-1084605";
const char MIR3P_CMD_FWver_chk_2[] = "RZAV1.01.01-1084606";

// 

/*
 Honeywell N3680 setting command
//const char N3680_CMD_GetFWver[4] = 		{};
 //const char N3680_CMD_Trigger[4] = 		{0x16, 0x54, 0x0d};
//const char N3680_CMD_Reboot[4] = 		{};
//const char N3680_CMD_SAVECONFIG[4] = 	{};

 CONFIG:
 ALLENA1;//Decode all the symbologies allowable
 RSLENA0;//GS1 DATABAR LIMITED 
 MAXENA1;//MAXICODE 
 MPDENA1;//MICROPDF417 
 PDFDCP3;//PDF417 CODE PAGE Special Characters in PDF417 / Code Page CP437
 N25ENA0;//NEC 2 OF 5
 X25ENA1;//MATRIX 2 OF 5
 I25ENA0;//INTERLEAVED 2 OF 5
 E13ISB1;//ISBN TRANSLATE
 UPAENA0;//CONVERT UPC-A TO EAN-13
 GS1ENA0;//GS1-128 DECODING
 //const char N3680_CMD_CODE_DEF[] =		 "RSLENA0;MAXENA1;MPDENA1;PDFDCP3;N25ENA0;X25ENA1;I25ENA0;E13ISB1;UPAENA0;GS1ENA0";
 //const char N3680_CMD_STORAGE[] = 	 "."; // Must be add after configuration commands

 */

const char N3680_CMD_PREFIX[] = 		{0x16,0x4d,0x0D};//3
const char N3680_CMD_GetRdMode[] = 		"TRGMOD?";//7
const char N3680_CMD_SETDEFALT[] = 		"DEFALT";//6
const char N3680_CMD_MANUTRG[] = 		"TRGMOD0";//7
const char N3680_CMD_RS232_INTF[] = 	"TERMID0;232BAD9;232WRD2";//23
const char N3680_CMD_REV_IMG[] = 		"VIDREV2";//7
const char N3680_CMD_SOFT_PART_NUM[] = 	"REV_WA?";//7
const char N3680_CMD_SOFT_V02[] = 	"REV_WA: CZ000169BAA";
const char N3680_CMD_SOFT_V01[] = 	"REV_WA: CZ000089BAA";


const char N3680_CMD_SYMB_ALL[] = 		"ALLENA1";//7
const char N3680_CMD_SUFFIX[] = 		"SUFBK2990A0D";//12

volatile bool RX_busy = false;
volatile bool dump_data = false;

uint32_t get_triger_level(){
	uint32_t RX_flag, TX_flag;
	UARTFIFOLevelGet(SCANNER_UART_BASE, &TX_flag, &RX_flag);
	switch(RX_flag){
	case UART_FIFO_RX1_8: return 16/8;
	case UART_FIFO_RX2_8: return (16*2)/8;
	case UART_FIFO_RX4_8: return (16*4)/8;
	case UART_FIFO_RX6_8: return (16*6)/8;
	case UART_FIFO_RX7_8: return (16*7)/8;
	default : return 8;
	}
}


void SCANNERUARTIntHandler(void){
	 uint32_t ui32Status, loop;
	 uint8_t buf;

	 ui32Status = UARTIntStatus(SCANNER_UART_BASE, true);
 
	 UARTIntClear(SCANNER_UART_BASE, ui32Status);
 
	 if(ui32Status & UART_INT_RX){
	 
		 RX_busy = true;
		 loop = get_triger_level() -1;	//reserve 1 byte to ensure RX timeout must be triggered.
		 while(loop){
			 buf = (unsigned char)ROM_UARTCharGetNonBlocking(SCANNER_UART_BASE);
			 if(!dump_data){
				UARTprintf("barcode send[RX] [%c][0x%02x]\n", buf,buf);
			 	if(scanner_cb) scanner_cb(0,buf);
			 }
			 loop--;
		 } 
	 }

	 if(ui32Status & UART_INT_RT){	// UART timeout
		 while(UARTCharsAvail(SCANNER_UART_BASE)){
			 buf = (unsigned char)ROM_UARTCharGetNonBlocking(SCANNER_UART_BASE);
			 if(!dump_data){
				 UARTprintf("barcode send[RT] [%c][0x%02x]\n", buf,buf);
				 if(scanner_cb) scanner_cb(0,buf);
			 }
		 }
		 if(scanner_cb) scanner_cb(1,' ');
		 RX_busy = false;
		 dump_data = false;
	 } 
 }


uint8_t scanner_send_cmd(scanner_set cmd)
{
	uint8_t len = 0,cmd_len;
	const char* cmd_ptr = NULL;
	char device_cmd_buf[200] = {0};
	HWVer_t hw_ver = get_hardware_version();
	
	 //make sure no incoming data
	 if(RX_busy){
		 dump_data = true;	//if there is data incoming, dump it.
	 }
	 while(RX_busy);	//wait data receiving finished.

	switch(cmd){
	case get_version:
		{
			dump_data = false;
			if(hw_ver == hw_v01){
				memcpy(device_cmd_buf, N3680_CMD_PREFIX, sizeof(N3680_CMD_PREFIX));
				snprintf(device_cmd_buf+sizeof(N3680_CMD_PREFIX), sizeof(device_cmd_buf),"%s.",N3680_CMD_SOFT_PART_NUM);
				cmd_len = sizeof(N3680_CMD_PREFIX) + (strlen(N3680_CMD_SOFT_PART_NUM)) + 1;
				cmd_ptr = device_cmd_buf;
				len = 21;
			}
			else{
				memcpy(device_cmd_buf, MIR3P_CMD_GetFWver, sizeof(MIR3P_CMD_GetFWver));
				cmd_len = sizeof(MIR3P_CMD_GetFWver);
				cmd_ptr = device_cmd_buf;
				len = 30;
			}
			break;
		}
	case reboot:
		{
			dump_data = true;
			if(hw_ver == hw_v00){
				cmd_ptr = MIR3P_CMD_Reboot;
				cmd_len = sizeof(MIR3P_CMD_Reboot);
			}
			else{
				SCAN_PWR_CYL;
			}
		break;
		}
	case save_config:
		{
			dump_data = false;
			if(hw_ver == hw_v00){
				cmd_ptr = MIR3P_CMD_SAVECONFIG;
				cmd_len = sizeof(MIR3P_CMD_SAVECONFIG);
			}
		break;
		}
	case get_rd_mode:
		{
			dump_data = false;
			if(hw_ver == hw_v01){
				memcpy(device_cmd_buf, N3680_CMD_PREFIX, sizeof(N3680_CMD_PREFIX));
				snprintf(device_cmd_buf+sizeof(N3680_CMD_PREFIX), sizeof(device_cmd_buf),"%s.",N3680_CMD_GetRdMode);
				cmd_len = sizeof(N3680_CMD_PREFIX) + (strlen(N3680_CMD_GetRdMode)) + 1;
				cmd_ptr = device_cmd_buf;
				len = 9;
			}
			else{
				memcpy(device_cmd_buf, MIR3P_CMD_GetRdMode, sizeof(MIR3P_CMD_GetRdMode));
				cmd_len = sizeof(MIR3P_CMD_GetRdMode);
				cmd_ptr = device_cmd_buf;
				len = sizeof(MIR3P_RSP_GetRdMode_chk);
			}
		break;
		}
	case set_rd_mode:
		{
			dump_data = true;
			if(hw_ver == hw_v01){
				memcpy(device_cmd_buf, N3680_CMD_PREFIX, sizeof(N3680_CMD_PREFIX));
				snprintf(device_cmd_buf+sizeof(N3680_CMD_PREFIX), sizeof(device_cmd_buf),"%s;%s;%s;%s;%s.",N3680_CMD_MANUTRG,\
																		N3680_CMD_RS232_INTF,\
																		N3680_CMD_REV_IMG,\
																		N3680_CMD_SUFFIX,\
																		N3680_CMD_SYMB_ALL);\
				cmd_len = sizeof(N3680_CMD_PREFIX) + (strlen(N3680_CMD_MANUTRG));\
				cmd_len += (strlen(N3680_CMD_RS232_INTF));\
				cmd_len += (strlen(N3680_CMD_REV_IMG));\
				cmd_len += (strlen(N3680_CMD_SUFFIX));\
				cmd_len += (strlen(N3680_CMD_SYMB_ALL));\
				cmd_len += 5;
				cmd_ptr = device_cmd_buf;
			}
			else{
				memcpy(device_cmd_buf, MIR3P_CMD_SETGRDMODE, sizeof(MIR3P_CMD_SETGRDMODE));
				cmd_len = sizeof(MIR3P_CMD_SETGRDMODE);
				cmd_ptr = device_cmd_buf;
			}
		break;
	}
	case set_default:
		{
			dump_data = false;
			if(hw_ver == hw_v01){
				memcpy(device_cmd_buf, N3680_CMD_PREFIX, sizeof(N3680_CMD_PREFIX));
				snprintf(device_cmd_buf+sizeof(N3680_CMD_PREFIX), sizeof(device_cmd_buf),"%s.",N3680_CMD_SETDEFALT);
				cmd_len = sizeof(N3680_CMD_PREFIX) + (strlen(N3680_CMD_GetRdMode)) + 1;
				cmd_ptr = device_cmd_buf;
			}
			else{
				UARTprintf("Not defined operation\n");
			}
			break;
		}
	default: return len;
	}
	if(cmd_ptr != NULL){
		scanner_send(cmd_ptr, cmd_len);
	}
	return len;
}

 void scanner_send(const char *pui8Buffer, uint8_t ui32Count){
 
	 //uint8_t i;
	 uint8_t temp = ui32Count;
	 while(temp != 0)
	 {
		 while(!ROM_UARTSpaceAvail(SCANNER_UART_BASE));//busy?
		 
		 //
		 // Write the next character to the UART.
		 //
		 ROM_UARTCharPutNonBlocking(SCANNER_UART_BASE, *pui8Buffer++);
		 
		 temp--;
	 }
 }

 
		
 void set_scanner_cb(void (*scan_cb)(bool, uint8_t)){
	 scanner_cb = scan_cb;
 }
 

bool scanner_msg_to_string(scanner_set cmd, uint8_t* data, uint8_t str_size)
{
	HWVer_t hw_ver = get_hardware_version();
	bool ret = false;
	char buf[20] = {0};
	if(cmd == get_rd_mode){
		if(hw_ver == hw_v01){
			if(!memcmp(data, N3680_CMD_MANUTRG, strlen(N3680_CMD_MANUTRG))){
				snprintf((char*)data, str_size, "OK");
				ret = true;
		}
		else{
				snprintf((char*)data, str_size, "Wrong resp.");
		}
	}
		else{
			// MIR3+
			//Function ID: 0xA6(get reading mode), param: 0x01(good-read off), checksum: 0x85
			//Function ID: 0xA6(get reading mode), param: 0x00(trigger on/off), checksum: 0x84
			if(!memcmp(data, MIR3P_RSP_GetRdMode_chk,sizeof(MIR3P_RSP_GetRdMode_chk))){
				snprintf((char*)data, str_size, "OK");
				ret = true;
			}
			else{
				snprintf((char*)data, str_size, "Wrong resp.");
			}
 }
 }
	else if(cmd == get_version){
		if(hw_ver == hw_v01){
			if(!memcmp(data, N3680_CMD_SOFT_V01, strlen(N3680_CMD_SOFT_V01))){
				snprintf((char*)data, str_size, "N3680SR-W1-TTL");
				ret = true;
			}
			else if(!memcmp(data, N3680_CMD_SOFT_V02, strlen(N3680_CMD_SOFT_V02))){
				snprintf((char*)data, str_size, "N3680SR-W1-TTL-02");
				ret = true;
			}
			else{				
				if(!memcmp(data, "REV_WA:", 7)){
					memcpy(buf, data+7, 12);
					snprintf((char*)data, str_size, "KNOWN:%s",buf);
					ret = true;
				}
				else{
					snprintf((char*)data, str_size, "Wrong resp.");
				}
			}
		}
		else{
			// MIR3+
			if(!memcmp(data, MIR3P_CMD_FWver_head_chk, sizeof(MIR3P_CMD_FWver_head_chk))){
				memcpy(buf, data+sizeof(MIR3P_CMD_FWver_head_chk), sizeof(buf));
				snprintf((char*)data, str_size, "%s",buf);
			}
			else{
				snprintf((char*)data, str_size, "Wrong resp.");
			}
		}
	}
	else{
		snprintf((char*)data, str_size, "ERROR");
	}	
	return ret;
}




