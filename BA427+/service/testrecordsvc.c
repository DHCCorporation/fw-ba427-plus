//*****************************************************************************
//
// Source File: testrecordsvc.c
// Description: Prototype of the function to Read/Write Test Record
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 11/13/15     Vincnet.T      Created file.
// 11/18/15     Vincent.T      New function TestRecordRead() to read tested records
// 11/23/15     Vincent.T      Modified: ui8TestResult -> ui8TestResult + 0x30;
// 01/05/16     Vincent.T      1. New global variables to save test record: gui16SSTestCounter, gui16BatTestCounter, gui16SysTestCounter
//                             2. Add function BTTestCounterInc();
// 01/29/16     Vincent.T      1. Add function BTTestCounterInc(); body
// 02/15/16     Vincent.T      1. Add function SYSTestCounterInc(); body
//                             2. Add BT/SS/Sys Counter acc func. in BTTestCounterInc();/BTTestCounterInc();/SYSTestCounterInc();
// 06/27/16     Vincent.T      1. Modified SSTestRecord();(worng content for cca rating)
//                             2. Modified SysTestRecord();(tested voltage * 100)
// 06/28/16     Vincent.T      1. Switch volt. and cca rating in BTTestRecord(); and SSTestRecord();
//                             2. Add Temperature and Date Time as input parameters for SysTestRecord(), BTTestRecord(); and SSTestRecord();
// 09/06/16     Vincent.T      1. New global varibale gui16TestRecordOffset
//                             2. Modif BTTestRecord(); SSTestRecord(); and SysTestRecord(); to implement cycled test record upload 
// 0/13/17      Vincent.T      1. New global variable gui16IRTestCounter
//                             2. New func. IRTestCounterInc();
//                             3. Modify BTTestRecord(); and SSTestRecord(); due to add License#, VIN/RO# and Test Code
//                             4. Modify SysTestRecord(); and IRTestRecord(); due to data length changed.
// 01/19/17     Vincent.T      1. Modify IRTestRecord();(first byte 4(0x34))
// 03/08/17     Vincent.T      1. Modify SSTestRecord() and BTTestRecord(); due to RO#/VIN# are expanded.
// 03/22/17     Vincent.T      1. #include "battcoresvc.h"
//                             2. Modify SysTestRecord(); to save "----" as "NO DETECTED"
// 03/30/17     Vincent.T      1. Modify BTTestRecord();, SSTestRecord();, SysTestRecord(); and IRTestRecord();(add doF info. to EEPROM)
// 06/01/17     Henry.Y        FW version: BT2100_WW_V01.a
//                             Fix bug: temperature celsius positive value record in testrecord function: BTTestRecord(),SSTestRecord(),SysTestRecord(),IRTestRecord()
// 11/06/17     Henry.Y        1. Modify IR test result lower bound in IRTestRecord().
// 11/22/17     Henry.Y        1. Adjust BTTestRecord() and SSTestRecord() format of storage.
// 12/21/17     Jerry.W        1. Adjust BTTestRecord() and SSTestRecord() format of storage, remove ui8TestRC.
// 09/11/18     Henry.Y        SaveTestRecords() make battery test+start stop test and system test seperated.
// 12/07/18     Jerry.W        modify for flash layout changing.
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/service/testrecordsvc.c#3 $
// $DateTime: 2017/12/26 10:41:34 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#include "testrecordsvc.h"
#include "SystemInfosvs.h"
#include "flashsvc.h"

void SaveTestRecords(TEST_TYPE test_type,void* rec, uint8_t len, RECORD_STATUS status ){
    if(len >= RECORD_UNIT_LENGTH){
        UARTprintf("ERR: Record size\n");
        while(1){}
    }
	uint8_t buf[RECORD_UNIT_LENGTH] = {0x00};
	uint16_t max_counter;
	SYS_INFO* info = Get_system_info();
	LAYOUT_INDEX_t* layout = ExtFls_get_layout();
	uint32_t RECORD_START_ADDRESS = layout->TEST_RESULT_ADD;
	//uint32_t RECORD_SYSTEM_TEST_ADDRESS = RECORD_START_ADDRESS + (layout->TEST_RESULT_sector_len/2);

	buf[0] = status;
	memcpy(buf+1, rec, len);

	/********************* this change is for the application save both Battery & System test record in the same zone  *********************/
	//uint32_t save_address = (test_type == System_Test)? RECORD_SYSTEM_TEST_ADDRESS:RECORD_START_ADDRESS;
	//uint16_t idx = (test_type == System_Test)? (info->TotalSYSrecord + info->SYSrecordOffset):(info->TotalTestRecord + info->TestRecordOffset);
	uint32_t save_address = RECORD_START_ADDRESS;
	uint16_t idx =(info->TotalTestRecord + info->TestRecordOffset);

	if(status == Wait_uplaod){
		UARTprintf("to be upload : %d \n",idx);
	}

	//max_counter = (test_type == System_Test)? MAX_SYSTESTCOUNTER:MAX_BTSSTESTCOUNTER;
	max_counter = MAX_BTSSTESTCOUNTER;
	idx %= max_counter;
	
	ExtFlsWriteRecord(save_address,idx, buf, RECORD_UNIT_LENGTH);

//	if(test_type == System_Test){
//
//		if(info->TotalSYSrecord == max_counter){
//			info->SYSrecordOffset++;
//			info->SYSrecordOffset %= max_counter;
//			save_system_info(&info->SYSrecordOffset, sizeof(info->SYSrecordOffset));
//		}
//		else{
//			info->TotalSYSrecord++;
//			save_system_info(&info->TotalSYSrecord, sizeof(info->TotalSYSrecord));
//		}
//	}
//	else{
		if(info->TotalTestRecord == max_counter){
			info->TestRecordOffset++;
			info->TestRecordOffset %= max_counter;
			save_system_info(&info->TestRecordOffset, sizeof(info->TestRecordOffset));
		}
		else{
			info->TotalTestRecord++;
			save_system_info(&info->TotalTestRecord, sizeof(info->TotalTestRecord));
		
		}
//	}
}


uint8_t record_status(uint16_t ui16TestNum){
	uint8_t buf;
	LAYOUT_INDEX_t* layout = ExtFls_get_layout();
	uint32_t RECORD_START_ADDRESS = layout->TEST_RESULT_ADD;
	ExtFlsReadRecord(RECORD_START_ADDRESS, ui16TestNum %MAX_BTSSTESTCOUNTER, &buf, 0, 1);
	return buf;
}

void set_uploaded(uint16_t ui16TestNum){
	uint8_t buf = Uploaded_record;
	LAYOUT_INDEX_t* layout = ExtFls_get_layout();
	uint32_t RECORD_START_ADDRESS = layout->TEST_RESULT_ADD;
	ExtFlsReWriteRecord(RECORD_START_ADDRESS,ui16TestNum %MAX_BTSSTESTCOUNTER, &buf, 1);
}



void TestRecordRead(uint16_t ui16TestNum, uint8_t *ui8RecordBuf)
{
	LAYOUT_INDEX_t* layout = ExtFls_get_layout();
	uint32_t RECORD_START_ADDRESS = layout->TEST_RESULT_ADD;
	ExtFlsReadRecord(RECORD_START_ADDRESS,ui16TestNum %MAX_BTSSTESTCOUNTER, ui8RecordBuf, 1, RECORD_UNIT_LENGTH-1);
}



void counter_increase(uint16_t *count){
    if(*count < 1000){
        (*count)++;
    }
}

void TestRecordSYSRead(uint16_t ui16TestNum, uint8_t *ui8RecordBuf)
{
    LAYOUT_INDEX_t* layout = ExtFls_get_layout();
    uint32_t RECORD_START_ADDRESS = layout->TEST_RESULT_ADD;
    uint32_t RECORD_SYSTEM_TEST_ADDRESS = RECORD_START_ADDRESS + (layout->TEST_RESULT_sector_len/2);
    ExtFlsReadRecord(RECORD_SYSTEM_TEST_ADDRESS,ui16TestNum %MAX_SYSTESTCOUNTER, ui8RecordBuf, 1, 128);
}


