//*****************************************************************************
//
// Source File: wifi_service.h
// Description: wifi module service layer.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 12/28/17     Jerry.W        Created file.
// 03/22/18     Jerry.W        add Get_internet_time() to get time with Internet.
// 05/18/18     Jerry.W        modify for changing of HTTP protocol with wifi module.
// 09/28/18     Jerry.W        remove the function of old architecture.
// 10/24/18     Jerry.W        add protocol: get wifi module version, wifi module test.
// 11/19/18     henry.Y        (Jerry) Add new connection policy: conn_disconnect.
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/driver/wifi_driver.h#2 $
// $DateTime: 2017/12/18 16:17:54 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#ifndef __WIFI_SERVICE_H__
#define __WIFI_SERVICE_H__

#include "../driver/wifi_driver.h"


#define SSID_LEN 32



typedef enum{
	conn_auto = 0,		//auto connection
	conn_smart = 1,		//smartconfig
	conn_designate = 2,	//designate AP
	conn_disconnect = 3,
}CONN_POLICY_T;


typedef enum{
	WIFI_connected,
	//WIFI_connecting,
	WIFI_disconnect,
	WIFI_off,
	
	//WIFI connection event type used in callback function to notice application layer
	WIFI_connect_fail,	//connect to AP fail, wrong password maybe,
	WIWI_connected_AP_internet_fail,
	WIFI_getting_IP,
	
	
	//error type used in callback function to notice application layer
	WIFI_module_fail = 100,
	WIFI_TX_buffer_over_flow,
	WIFI_receive_command_error_format,
	WIFI_receive_command_error_type,
}WIFI_CONN_T;


typedef enum _auth_mode {
	AUTH_OPEN = 0,
	AUTH_WEP,
	AUTH_WPA_PSK,
	AUTH_WPA2_PSK,
	AUTH_WPA_WPA2_PSK
}AUTH_MODE;

#pragma pack(push)
#pragma pack(1)
typedef struct AP_info {
	uint8_t bssid[6];
	uint8_t ssid[SSID_LEN];
	uint8_t ssid_len;
	uint8_t channel;
	int8_t rssi;
	uint32_t authmode;
	uint8_t is_hidden; // SSID of current AP is hidden or not.
}AP_info_T;


typedef struct{
	uint8_t AP_number;
	AP_info_T AP[];
}SSID_list_T;
#pragma pack(pop)



/*************************************************************************************************
 * @ Name		: wifi_config
 * @ Brief 		: configure service layer data structure.
 * @ Parameter 	: no_off -- 0 = off, 1 = on.
 * 				  evn_cb -- callback function pointer to notice server layer of receiving command.
 *************************************************************************************************/
void wifi_config(uint8_t no_off,void (*evn_cb)(uint32_t ));


/*************************************************************************************************
 * @ Name		: wifi_connnect_policy
 * @ Brief 		: set WIFI module connection policy.
 * @ Parameter 	: policy -- connection policy, refer to CONN_POLICY_T definition.
 *************************************************************************************************/
void wifi_connnect_policy(CONN_POLICY_T policy);


/*************************************************************************************************
 * @ Name		: wifi_connnect_AP
 * @ Brief 		: set WIFI module connect to a designate AP.
 * @ Parameter 	: SSID -- SSID of designate AP.
 * @ Parameter 	: PW -- password of designate AP.
 *************************************************************************************************/
void wifi_connnect_AP(uint8_t *SSID, uint8_t *PW);


/*************************************************************************************************
 * @ Name		: wifi_scan_AP
 * @ Brief 		: scan AP.
 * @ Parameter 	: scn_bc -- callback function pointer to notice server layer of scanned AP.
 *************************************************************************************************/
void wifi_scan_AP(void (*scn_bc)(SSID_list_T *));


/*************************************************************************************************
 * @ Name		: wifi_service_poll
 * @ Brief 		: processing the service layer functionality(process the work to be done and asynchronous callback).
 *************************************************************************************************/
void wifi_service_poll(void);


/*************************************************************************************************
 * @ Name		: wifi_get_status
 * @ Brief 		: get WIFI connection status.
 * @ Return		: WIFI connection status, refer to WIFI_CONN_T.
 *************************************************************************************************/
WIFI_CONN_T wifi_get_status(void);



/*************************************************************************************************
 * @ Name		: wifi_get_connected_AP
 * @ Brief 		: get WIFI connected AP.
 * @ Parameter	: SSID_buf -- address to be put in SSID.
				  buf_len -- buffer length.
 * @ Return		: false -- didn't connect to any AP.
 *************************************************************************************************/
bool wifi_get_connected_AP(uint8_t *SSID_buf, uint8_t buf_len);



/*************************************************************************************************
 * @ Name		: Get_internet_time
 * @ Brief 		: get time by SNTP service. the result will be returned by
 * @ Parameter 	: TZ -- time zone.
 * @ Parameter 	: time_cb -- callback function to get result.
 *************************************************************************************************/
void Get_internet_time(int8_t TZ, void (*time_cb)(uint8_t *));


/*************************************************************************************************
 * @ Name		: set_network_timout
 * @ Brief 		: set timer for upper layer needs to know if timeout occurs
 * @ Parameter 	: wait_time -- timeout period in second.
 *				  time_cb -- callback function triggered at times up
 *************************************************************************************************/
void set_network_timout(uint8_t wait_time, void (*time_cb)(void));


/*************************************************************************************************
 * @ Name		: Get_module_version
 * @ Brief 		: get the FW version of wifi module.
 * @ Parameter 	: ver_cb -- callback function to get result.
 * @ Return 	: 1 while error occur.
 *************************************************************************************************/
bool Get_module_version(void (*ver_cb)(uint8_t *));


/*************************************************************************************************
 * @ Name		: wifi_module_loopback
 * @ Brief 		: loop-back the sent data to test the wifi module.
 * @ Parameter 	: len -- data length.
 * 				  data -- data to send.
 * 				  loop_cb -- callback function to check the loop-backed data.
 * @ Return 	: 1 while error occur.
 *************************************************************************************************/
bool wifi_module_loopback(uint16_t len, uint8_t *data, void (*loop_cb)(uint16_t data, uint8_t *));

/*************************************************************************************************
 * @ Name		: set_WIFI_blink
 * @ Brief 		: enable/disable WIFI back-light blinking.
 * @ Parameter 	: on_off -- on or off.
 *************************************************************************************************/
void set_WIFI_blink(bool on_off);

#endif
