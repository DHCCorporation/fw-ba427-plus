//*****************************************************************************
//
// Source File: HTTP_service.c
// Description: HTTP service
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 08/10/18     Jerry.W        Created file.
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/driver/wifi_driver.h#2 $
// $DateTime: 2017/12/18 16:17:54 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#include "HTTP_service.h"
#include "wifi_service.h"
#include <stdio.h>
#include "../driver/delaydrv.h"


#if HTTP_DEBUG
#define 	H_DEBUG(bug_info)		UARTprintf(BLUE_TEXT("http:%s, @%d\n"), bug_info, __LINE__); //while(HTTP_bug_block){}; HTTP_bug_block = true;
#else
#define 	H_DEBUG(bug_info)
#endif



typedef enum{
	HTTP_idle,
	HTTP_wait_response,
}HTTP_connection_state;



typedef struct request_node{
	REQUEST_TYPE type;
	uint8_t *URL;
	uint8_t *header;
	uint8_t *data;
	void (*evn_cb)(uint16_t , uint16_t ,uint8_t *);
	struct request_node* next_req;
}request_node_t;


typedef struct response{
	uint16_t status;
	uint16_t len;
	uint8_t *data;
	void (*evn_cb)(uint16_t , uint16_t ,uint8_t *);
	struct response* next_resp;
}response_node_t;

typedef struct RX_node{
	uint16_t len;
	uint8_t *data;
	struct RX_node* next_RX_node;
}RX_node_t;

typedef struct{
	HTTP_connection_state state;
	Socket_t soc;
	bool secure;
	bool certificate;
	bool blocked;
	request_node_t *req_n;
	RX_node_t *RX_buf;
	response_node_t *resp_n;
}connection_t;


void HTTP_send_request(connection_t *conn);
void HTTP_connect(connection_t *conn);
bool add_new_request(connection_t *conn, REQUEST_TYPE type, uint8_t *URL, uint8_t *header,
		uint8_t *data, void (*evn_cb)(uint16_t , uint16_t ,uint8_t *));
bool pop_request(connection_t *conn);
bool pop_response(connection_t *conn);
int8_t new_conn(uint8_t *server, bool secure, uint16_t port, bool certificate);
int8_t find_conn_by_server(uint8_t *server, bool secure, uint16_t port);



connection_t *conn_list[MAX_HTTP_CONN];
bool HTTP_bug_block = true;
static bool _HTTP_lock = false;

bool HTTP_lock(){
	if(_HTTP_lock == false){
		_HTTP_lock = true;
		return true;
	}
	else{
		return false;
	}
}

void HTTP_unlock(){
	uint8_t i;
	_HTTP_lock = false;
	for(i = 0; i < MAX_HTTP_CONN; i++){
		if(conn_list[i] != NULL){
			if(conn_list[i]->blocked == true){
				conn_list[i]->blocked = false;
				if(conn_list[i]->soc.status == Soc_TCP_connected){
					conn_list[i]->state = HTTP_wait_response;
					HTTP_send_request(conn_list[i]);
				}
				else if(conn_list[i]->soc.status == Soc_idle){
					HTTP_connect(conn_list[i]);
				}
				return;
			}
		}
	}
}


char *GET_s = "GET";
char *POST_s = "POST";
char *CONTENT_LEN_s = "Content-Length:";
char *CONTENT_LEN_s_L = "content-length:";

void request_to_respons(connection_t* conn, uint16_t status, uint16_t len,uint8_t *data){
	response_node_t* temp_res = malloc(sizeof(response_node_t));
	if(temp_res == NULL){
		H_DEBUG("malloc fail");
		return;
	}

	temp_res->status = status;		//HTTP response status ex: 200, 400...
	temp_res->len = len;
	temp_res->data = data;
	temp_res->evn_cb = conn->req_n->evn_cb;
	temp_res->next_resp = 0;

	if(conn->resp_n == NULL){
		conn->resp_n = temp_res;
	}
	else{
		response_node_t* temp = conn->resp_n;
		while(temp->next_resp != NULL){
			temp = temp->next_resp;
		}
		temp->next_resp = temp_res;
	}

	temp_res = temp_res;
	temp_res ++;
	temp_res = temp_res;

	pop_request(conn);
	if(conn->req_n == NULL){
		conn->state = HTTP_idle;
	}
	else{
		if(conn->soc.status == Soc_TCP_connected) HTTP_send_request(conn);
	}
}


void RX_resolving(connection_t* conn){
	char header[MAX_HEADER_LENGTH];
	uint16_t index = 0;
	uint16_t cpy_len;
	uint16_t total_len = 0;
	RX_node_t* temp = conn->RX_buf;

	while(temp != NULL && memcmp(temp->data, "HTTP", 4)){
		char *err = strstr((char*)temp->data, "<Error>");
		if(err){
			UARTprintf("http: drop error package\n");
		}
		else{
			H_DEBUG("header err, drop 1 RX node");
		}
		conn->RX_buf = conn->RX_buf->next_RX_node;
		free(temp->data);
		free(temp);
		temp = conn->RX_buf;
	}

	while(temp != NULL && index != MAX_HEADER_LENGTH){
		cpy_len = ((index + temp->len) <= MAX_HEADER_LENGTH)? temp->len :  (MAX_HEADER_LENGTH - index);
		memcpy(header + index, temp->data, cpy_len);
		index += cpy_len;
		total_len += temp->len;
		temp = temp->next_RX_node;
	}

	while(temp != NULL){
		total_len += temp->len;
		temp = temp->next_RX_node;
	}

	char* header_end;
	header[index] = '\0';
	header_end = strstr(header, "\r\n\r\n");

	if(header_end != NULL){
		char* content_s = strstr(header, CONTENT_LEN_s);
		if(content_s == NULL) content_s = strstr(header, CONTENT_LEN_s_L);
		uint16_t content_len = (content_s != NULL)? atoi(content_s+strlen(CONTENT_LEN_s))  : 0 ;
		uint16_t header_len = header_end - header +4;

		header[header_len] = '\0';

		if(total_len >= (content_len + header_len)){
			uint16_t status = atoi(header+8);
			uint8_t *data = NULL;

			if(content_len != 0){
				data = malloc(content_len+1);
				if(data == NULL){
					H_DEBUG("malloc fail");
					return;
				}
				data[content_len] = '\0';
			}

			//find the start of body
			temp = conn->RX_buf;
			cpy_len = 0;
			while(header_len){
				if(header_len >= temp->len ){
					header_len -= temp->len;
				}
				else{
					if(content_len == 0){
						H_DEBUG("content len err");
					}
					cpy_len += temp->len - header_len;
					memcpy(data, temp->data + header_len, cpy_len);
					header_len = 0;
				}
				RX_node_t* last = temp;
				temp = temp->next_RX_node;
				free(last->data);
				free(last);
			}


			while(content_len > cpy_len){
				if((content_len - cpy_len) < temp->len){
					memcpy(data + cpy_len, temp->data, content_len - cpy_len);
				}
				else{
				memcpy(data + cpy_len, temp->data, temp->len);
				}
				cpy_len += temp->len;

				RX_node_t* last = temp;
				temp = temp->next_RX_node;
				free(last->data);
				free(last);
			}
			conn->RX_buf = temp;

			//UARTprintf("%s\n", header);

			request_to_respons( conn,  status, content_len, data);
		}

	}
	else{
		if(index == MAX_HEADER_LENGTH){
			H_DEBUG("header too long!");
		}
	}


}


int8_t check_conn(void* conn){
	uint8_t i;
	for(i = 0; i < MAX_HTTP_CONN ; i++){
		if(conn == conn_list[i]){
			return i;
		}
	}
	return -1;
}

void conn_evnt_handler(void* conn, uint8_t event){
	int8_t i;

	i = check_conn(conn);

	if(i == -1){
		H_DEBUG("evnt cb : error conn");
		return;
	}

	connection_t* conn_temp = (connection_t*)conn;

	switch(event){
	case Soc_idle:
		// if has request, reconnect
		// else do nothing
		conn_temp->state = HTTP_idle;
		if(conn_temp->req_n != NULL){
			HTTP_connect(conn_temp);
		}

		break;
	case Soc_TCP_connected:
		// if has request, send it.
		if(conn_temp->state != HTTP_idle){
			H_DEBUG("evnt cb : HTTP state err");
		}

		if(conn_temp->req_n != NULL){
			conn_temp->state = HTTP_wait_response;
			HTTP_send_request(conn_temp);
		}
		else{
			H_DEBUG("evnt cb : connected & no request?");
		}

		break;
	case Soc_TCP_connect_fail:
	case Soc_TCP_disconnected:
		// if has request, response 404
		conn_temp->state = HTTP_idle;
		while(conn_temp->req_n != NULL){
			request_to_respons(conn_temp, 404, 0,0);
		}

		break;
	}
}


void conn_rcv_handler(void* conn, uint8_t *data, uint16_t len){
	int8_t i;

	i = check_conn(conn);

	if(i == -1){
		H_DEBUG("rcv cb : error conn");
		return;
	}
	connection_t* conn_temp = (connection_t*)conn;

	if(conn_temp->state == HTTP_idle){
		UARTprintf("rcv cb : idle state");
		return;
	}

	RX_node_t *RX_node = malloc(sizeof(RX_node_t));
	uint8_t *RX_buf = malloc(len);

	if(RX_node == NULL || RX_buf == NULL){
		H_DEBUG("malloc fail");
		free(RX_buf);
		free(RX_node);
		return;
	}

	RX_node->len = len;
	memcpy(RX_buf, data, len);
	RX_node->data = RX_buf;
	RX_node->next_RX_node = NULL;

	if(conn_temp->RX_buf == NULL){
		conn_temp->RX_buf = RX_node;
	}
	else{
		RX_node_t *temp = conn_temp->RX_buf;
		while(temp->next_RX_node != NULL){
			temp = temp->next_RX_node;
		}
		temp->next_RX_node = RX_node;
	}

	RX_resolving(conn_temp);

}


void HTTP_service_poll(void){
	uint8_t i;
	for(i = 0; i < MAX_HTTP_CONN ; i++){
		if(conn_list[i] != NULL){
			//check response list
			while(conn_list[i]->resp_n != NULL){
				conn_list[i]->resp_n->evn_cb(conn_list[i]->resp_n->status, conn_list[i]->resp_n->len, conn_list[i]->resp_n->data);
				pop_response(conn_list[i]);
			}

		}
	}
}

int8_t find_conn_by_server(uint8_t *server, bool secure, uint16_t port){
	uint8_t i;
	for(i = 0; i < MAX_HTTP_CONN ; i++){
		if(conn_list[i] != NULL){
			if(strcmp((char*)conn_list[i]->soc.pair, (char*)server) == 0 &&
				conn_list[i]->secure == secure && conn_list[i]->soc.port == port){
				return i;
			}
		}
	}
	return -1;
}

int8_t new_conn(uint8_t *server, bool secure, uint16_t port, bool certificate){
	//find space and setup
	uint8_t i;
	for(i = 0; i < MAX_HTTP_CONN ; i++){
		if(conn_list[i] == NULL){
			conn_list[i] = malloc(sizeof(connection_t));
			uint8_t *temp = malloc(strlen((char*)server)+1);
			if(conn_list[i] == NULL || temp == NULL){
				free(temp);
				free(conn_list[i]);
				H_DEBUG("malloc fail");
				return -1;
			}

			memset(conn_list[i], 0, sizeof(connection_t));
			memcpy(temp, server, strlen((char*)server)+1);
			conn_list[i]->state = HTTP_idle;
			conn_list[i]->soc.status = Soc_idle;
			conn_list[i]->soc.evn_cb = conn_evnt_handler;
			conn_list[i]->soc.rcv_cb = conn_rcv_handler;
			conn_list[i]->soc.pair = temp;
			conn_list[i]->soc.reserve = conn_list[i];
			conn_list[i]->secure = secure;
			conn_list[i]->certificate = certificate;
			conn_list[i]->blocked = false;
			conn_list[i]->soc.port = port;
			return i;
		}
	}
	return -1;
}

bool add_new_request(connection_t *conn, REQUEST_TYPE type, uint8_t *URL, uint8_t *header,
		uint8_t *data, void (*evn_cb)(uint16_t , uint16_t ,uint8_t *)){

	ROM_IntMasterDisable();
	request_node_t *temp = conn->req_n;
	request_node_t *req_n = malloc(sizeof(request_node_t));
	if(req_n == NULL ){
		H_DEBUG("malloc fail");
		ROM_IntMasterEnable();
		return false;
	}

	req_n->type = type;
	req_n->URL = malloc(strlen((char*)URL)+1);
	if(header != NULL){
		req_n->header = malloc(strlen((char*)header)+1);
	}
	else{
		req_n->header = NULL;
	}

	req_n->data = data;
	req_n->evn_cb = evn_cb;
	req_n->next_req = NULL;

	if((header && req_n->header == NULL) || req_n->URL == NULL ){
		H_DEBUG("malloc fail");
		free(req_n->header);	//free(NULL) is safe!
		free(req_n->URL);
		free(req_n);
		ROM_IntMasterEnable();
		return false;
	}

	memcpy(req_n->URL , URL , strlen((char*)URL)+1);
	if(header) memcpy(req_n->header , header , strlen((char*)header)+1);

	if(temp == NULL){
		conn->req_n = req_n;
	}
	else{
		while(temp->next_req != NULL){
			temp = temp->next_req;
		}
		temp->next_req = req_n;
	}
	ROM_IntMasterEnable();
	return true;
}

bool pop_request(connection_t *conn){
	request_node_t *first;

	first = conn->req_n;
	if(first == NULL){
		H_DEBUG("pop req fail");
		return false;
	}

	conn->req_n = conn->req_n->next_req;

	free(first->URL);
	free(first->data);
	free(first->header);
	free(first);

	return true;
}

bool pop_response(connection_t *conn){
	response_node_t *first;

	first = conn->resp_n;
	if(first == NULL){
		H_DEBUG("pop resp fail");
		return false;
	}

	conn->resp_n = conn->resp_n->next_resp;

	free(first->data);
	free(first);

	HTTP_unlock();

	return true;
}





void HTTP_send_request(connection_t *conn){

	//20190123 Jerry : esp8266 can handle two TCP connection, but ran out of RAM when two are sending at same time
	if(HTTP_lock() == false){
		conn->blocked = true;
		conn->state = HTTP_idle;
		return;	
	}


	if(conn->req_n == NULL){
		conn->state = HTTP_idle;
		return;
	}

	request_node_t* req = conn->req_n;
	uint8_t buf[1024];
	uint16_t header_len;
	uint16_t data_len = 0;
	uint8_t *send_buf;
	char content_len[32] = "";
	char query_s[60] = "";

	if(req->type == POST){
		data_len = strlen((char*) req->data);
		sprintf(content_len , "Content-Length: %d\r\n", data_len);

	}
	else{
		if(req->data) sprintf(query_s , "?%s", req->data);
	}

	sprintf((char*)buf,"%s %s%s HTTP/1.1\r\n"
				"Host: %s:%d\r\n"
				"Connection: keep-alive\r\n"
				"User-Agent: ESP8266\r\n"
				"%s"
				"%s"
				"\r\n",
				(req->type == GET)? GET_s : POST_s,
				req->URL, query_s,
				conn->soc.pair, conn->soc.port ,
				(req->header != NULL)? req->header : "",
				content_len);

	header_len = strlen((char*)buf);

	send_buf = malloc(header_len + data_len+1);

	if(send_buf == NULL){
		H_DEBUG("malloc fail");
		conn->state = HTTP_idle;
		return;
	}

	memcpy(send_buf, buf , header_len);
	if(data_len) memcpy(send_buf+header_len , req->data , data_len);

	send_buf[header_len + data_len] = 0;
	UARTprintf("%s\n\n",  send_buf );

	TCP_send(&conn->soc, send_buf, header_len + data_len);
	free(send_buf);
}

void HTTP_connect(connection_t *conn){
	bool cert_b = conn->certificate;
	CERT_set(CERT_server, cert_b);
	CERT_set(CERT_client, cert_b);
delay_ms(1);
	TCP_connect(&conn->soc,  conn->secure );
}


bool HTTP_request(REQUEST_TYPE type, uint8_t *server, uint16_t port, uint8_t *URL, uint8_t *header,
		uint8_t *data, bool secure, bool certificate, void (*evn_cb)(uint16_t , uint16_t ,uint8_t *)){
	if(wifi_get_status() != WIFI_connected){
		return false;
	}

	int8_t conn_i;
	conn_i = find_conn_by_server(server, secure, port);
	if(conn_i == -1){
		conn_i = new_conn(server, secure, port, certificate);
	}

	if(conn_i == -1){
		H_DEBUG("add conn fail");
		return false;
	}

	if(!add_new_request(conn_list[conn_i], type, URL, header, data, evn_cb)){
		H_DEBUG("add req fail");
		return false;
	}

	if(conn_list[conn_i]->state == HTTP_idle){
		if(conn_list[conn_i]->soc.status == Soc_TCP_connected){
			conn_list[conn_i]->state = HTTP_wait_response;
			HTTP_send_request(conn_list[conn_i]);
		}
		else if(conn_list[conn_i]->soc.status == Soc_idle){	//do nothing for Soc_TCP_connecting
			HTTP_connect(conn_list[conn_i]);
		}
	}
	return true;
}

void HTTP_disconnect(uint8_t *server, bool secure, uint16_t port){
	int8_t conn_i;
	conn_i = find_conn_by_server(server, secure, port);
	if(conn_i == -1){
		H_DEBUG("disconnect unknown connection");
		return;
	}
	TCP_disconnect(&conn_list[conn_i]->soc);
}


