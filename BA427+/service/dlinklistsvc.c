//*****************************************************************************
//
// Source File: dlinklistsvc.c
// Description: Functions to construct double linking list data structure.
//				
//
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 09/08/15     William.H      Created file.
// 10/05/15     William.H      Check in BT2020 project which branched from LCDFontMaker(Pre-BT2020) by modifed related GPIOs.
// 11/02/17     Jerry.w		   1. add const in tJISBatteryType type definition for fewer memory usage
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/service/dlinklistsvc.c#6 $
// $DateTime: 2017/11/02 09:22:07 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>    // for malloc
#include <stdint.h>
#include <string.h>
#include "../driver/uartstdio.h"
#include "dlinklistsvc.h"
#include "../driver/mem_protect.h"

void JISDLlist_Init(JISBATDLList *linklist) 
{
	linklist->head = NULL;
}


/*******************************************
	Method :JISDLlist_Gen_New_Node
*******************************************/
// generate a new node / return NULL if anything wrong
JISNode *JISDLlist_Gen_New_Node(tJISBatteryType JISData) 
{
	JISNode *newNode = (JISNode *)malloc(sizeof(JISNode));

	newNode->psJISBatType.batString = JISData.batString;
	newNode->psJISBatType.capVal = JISData.capVal;
	newNode->psJISBatType.itemNum = JISData.itemNum;
	
	newNode->prev = NULL;
	newNode->next = NULL;

	//
  	//	Note: LinkList module have to free this node(free()) in somewhere mothod
  	//
	return newNode;		
}

/*******************************************
	Method :llist_travel
*******************************************/
// To print out all node in the list
void JISDLlist_Travel(JISBATDLList *linklist) 
{
	JISNode * tempNodePtr ;
	tempNodePtr = linklist->head ;
	int index = 1 ;
	
	if(tempNodePtr == NULL)
	{
		UARTprintf("List is empty!!\n") ;
		return ;  // empty list
	}
	
	while(tempNodePtr != NULL)
	{
		UARTprintf("Node %d, batString = %s, itemNum = %d, capVal = %d.\n",index++,tempNodePtr->psJISBatType.batString, tempNodePtr->psJISBatType.itemNum, tempNodePtr->psJISBatType.capVal);
		
		tempNodePtr = tempNodePtr->next ;			
	}
}

/*******************************************
	Method :JISDLlist_Del
*******************************************/
// Free all node
void JISDLlist_Del(JISBATDLList *linklist) 
{
	JISNode *tempNodePtr;
	JISNode *nodeToDel;

	UARTprintf("JISDLlist_Del(). Free all node.\n");
	
	tempNodePtr = linklist->head;
	
	if(tempNodePtr == NULL)
	{
		UARTprintf("Empty List. THIS SHOULD NOT HAPPEN!!\n");
		return;  // empty list
	}
	
	while(tempNodePtr != NULL)
	{
		nodeToDel = tempNodePtr;
		tempNodePtr = tempNodePtr->next;
		
		free(nodeToDel);
	}
		
	linklist->head = NULL;

	UARTprintf("JISDLlist_Del(). Free list mem done.\n");
}

// Add new note in the front of linked list
void JISDLlist_Insert_In_Front(JISBATDLList *linklist, JISNode *newNode) 
{
	  JISNode *head = linklist->head;
	  
		if(linklist->head != NULL)
		{
			(linklist->head)->prev = newNode;
		}
		
		newNode->next = head;
		newNode->prev = NULL;
		
		linklist->head = newNode;	
}

/*******************************************
	Method :llist_insert
*******************************************/
// Add the new node behind someone node.
void JISDLlist_Insert(JISBATDLList *linklist, JISNode *nodeInList, JISNode *newNode) 
{
	// To check if nodeInList is indeed existing in the list
	// TODO

	//JISNode *head = linklist->head;
	
	if(nodeInList->next != NULL)
	{
		(nodeInList->next)->prev = newNode;
	}
	
	newNode->next = nodeInList->next ;
	newNode->prev = nodeInList ;
	nodeInList->next = newNode ;
	
}

/*******************************************
	Method : JISDLlist_Get_Last_Node
*******************************************/
// To get the last node address in this linked list.
JISNode *JISDLlist_Get_Last_Node(JISBATDLList * linklist) 
{
	JISNode *tempNodePtr;
	JISNode *currentNodePtr;
 
	tempNodePtr = linklist->head;
 
	while(tempNodePtr != NULL)
	{
		currentNodePtr = tempNodePtr; 	
 		tempNodePtr = tempNodePtr->next;
	}

	UARTprintf("JISDLlist_Get_Last_Node(). batString = %s, itemNum = %d.\n", currentNodePtr->psJISBatType.batString, currentNodePtr->psJISBatType.itemNum);

	return currentNodePtr;	
}



/*******************************************
	Method : llist_insert_in_rear
*******************************************/
// Add the new node into the rear of this list
void JISDLlist_Insert_In_Rear(JISBATDLList *linklist, JISNode *newNode) 
{
	if(linklist->head == NULL)
	{
		linklist->head = newNode;
	}
	else
	{	
		JISNode *last_node = JISDLlist_Get_Last_Node(linklist);
	
		JISDLlist_Insert(linklist,last_node,newNode);
	}
 
 // by pointer access
 /*
 last_node->next = newNode ;
 newNode->next = NULL ;
 */
}


/*******************************************
	Method : JISDLlist_Init_All
*******************************************/
// Init all Node of JIS Battery Type
void JISDLlist_Init_All(JISBATDLList *linklist, const tJISBatteryType *dataArray, uint16_t tNum)
{
	JISNode *newNode;	
	uint16_t i;
  
 	JISDLlist_Init(linklist);
 
	for(i=0; i<tNum; i++)
	{
		newNode = JISDLlist_Gen_New_Node(dataArray[i]);
		//JISDLlist_Insert_In_Front(linklist,newNode);
		JISDLlist_Insert_In_Rear(linklist, newNode);
 	}

}

/*******************************************
	Method : llist_search
*******************************************/
//	To search someone node which conatins the specific item number. 
//	YES: return 1,   NO: return 0
JISNode *JISDLlist_Search_by_ItemNum(JISBATDLList *linklist, uint16_t value) 
{
	JISNode *tempNodePtr;
  
	tempNodePtr = linklist->head;
 
	while(tempNodePtr != NULL)
	{
		if(tempNodePtr->psJISBatType.itemNum == value)
		{
			return tempNodePtr;	
		}
		tempNodePtr = tempNodePtr->next;
	}

	return NULL ;
}

JISNode *JISDLlist_Search_by_BATTypeString(JISBATDLList *linklist, char *searchStr)
{
	JISNode *tempNodePtr;
  
	tempNodePtr = linklist->head;

	while(tempNodePtr != NULL)
	{
		if(strstr(tempNodePtr->psJISBatType.batString, searchStr) != NULL)
		{
			return tempNodePtr;	
		}
		tempNodePtr = tempNodePtr->next;
	}

	return NULL ;
}


