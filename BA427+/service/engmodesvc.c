//*****************************************************************************
//
// Source File: engmodesvc.c
// Description: Functions to do all k-coe funcs.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 03/03/16     Vincent.T      Created file.
// 03/21/16     Vincent.T      1. Add LCD contrast Adj func.
// 04/07/16     Vincent.T      1. Add LCD contrast Adj func.(big jump)
//                             2. Add global variable gui8LCDRRatio
//                             3. Skip VM, AM here.
// 04/14/16     Vincent.T      1. Add IR Adj func. & related variables.
// 06/14/16     Vincent.T      1. Modify IR x-axis on screen.
// 06/21/16     Vincent.T      1. BT2100_WW_V00.a -> BT2100_WW_v00.b
//                             2. LCD boundary >=7 to > 7
// 06/24/16     Vincent.T      1. BT2100_WW_V00.b -> BT2100_WW_V00.c
// 06/27/16     Vincent.T      1. BT2100_WW_V00.c -> BT2100_WW_V01.a
// 08/10/16     Vincent.T      1. Disable CCA calbration, IR calbration and temperature calbration.
// 08/24/16     Vincent.T      1. BT2100_WW_V01.a -> BT2100_WW_v01.b
// 08/25/16     Vincent.T      1. BT2100_WW_V01.b -> BT2100_WW_v00.d
// 09/06/16     Vincent.T      1. BT2100_WW_V00.d -> BT2100_WW_v00.e.
// 10/13/16     Ian.C          1. BT2100_WW_V00.e -> BT2100_WW_v00.f
// 11/15/16     Ian.C          1. BT2100_WW_v00.f -> BT2100_WW_v00.g
// 03/01/17     Vincent.T      1. #include "..\app/Battypeselwidget.h"
//                             2. #include "lcdsvc.h"
//                             3. Modify EngMode(); present "heavy load LCD screen" when adjust LCD contrast(small jump)
// 03/08/17     Vincent.T      1. #include "printersvc.h"
//                             2. #include "..\app/Datetimesetwidget.h"
//                             3. Modify FW version BT2100_WW_V00.x -> BT2100_WW_V00.x and yyyymmdd
//                             4. Modify EngMode(); to quick check btns, schedule-ic and printer
// 03/22/17     Vincent.T      1. Modify EngMode();(version#, skip big jump and fix small jump range.
// 03/24/17     Vincent.T      1. Modify FWver "i -> g" and remove data info.
// 03/27/17     Vincent.T      1. Add FWver Date info.
// 03/30/17     Vincent.T      1. Modify EngMode();(FW ver.)
// 04/05/17     Vincent.T      1. Modify EngMode();(FW ver.)
// 06/01/17     Henry          FW version: BT2100_WW_V01.a
//                             1. Change strFWver[] to global variable, location at main.c.
// 11/06/17     Henry.Y	       1. Hide useless variables:CCA_Adjustment_8V
//                             2. arrange engineering mode flow(apply ver.:BT2100_WW_V02.b)
// 09/11/18     Henry.Y        Use systeminfo data struct to replace the global variables.
// 09/28/18     Jerry.W        modify temperature sensor calibrate function for new OPA and HW config.
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/service/engmodesvc.c#6 $
// $DateTime: 2017/09/22 10:56:10 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************


#include "engmodesvc.h"
#include "SystemInfosvs.h"










uint8_t EngMode(void)
{

	//
	// Read Key
	// Left(only) -> Left + Right -> Right(only)
	//
	//
	if(MAP_GPIOPinRead(BUTTONS_GPIO_BASE, LEFT_BUTTON | SELECT_BUTTON | RIGHT_BUTTON) == (SELECT_BUTTON | RIGHT_BUTTON))
	{
		if(LeftBtn(0))
		{
			if(MAP_GPIOPinRead(BUTTONS_GPIO_BASE, LEFT_BUTTON | SELECT_BUTTON | RIGHT_BUTTON) == (LEFT_BUTTON | SELECT_BUTTON))
			{
				if(RightBtn(0))
				{
					return 1;					
				}
			}
		}
	}
	return 0;
}
void TempEngMode(void)
{
	char strTempEngMode[] = "<Temperature K-mode>";
	char strTempBody[] = "Body:";
	char strTempObject[] = "Object:";
	char strTempUL[] = "-------------------------";
	char strTempCoe[] = "Coe:";
	
	uint8_t l,sLen;
	uint8_t ui8DatBuf[10] = {0x00};
	uint8_t ui8TempDatBuf[5] = {0x00};

	int32_t int_buffer = 0;

	uint32_t ui32TempBodyCoe = 0;
	int32_t  i32TempObjCoe = 0;
	uint32_t ui32TempBuf = 0;
	SYS_INFO* info = Get_system_info();

	//float fDatBuf = 0.0;
	float ObjTemp = 0;
	float BodyTemp = 0;


	//************************************************************************
	//
	// Temperature Calibration Mode
	// - Body Temperature
	// - Object Temperature
	//
	//************************************************************************
	// Refresh the screen to see the results
	ST7565ClearScreen(BUFFER0);
	ST7565Refresh(BUFFER0);
	sLen = strlen(strTempEngMode);//"<Temperature K-mode>"
	for(l = 0; l < sLen; l++)
	{
		ST7565Draw6x8Char(0 + l*6, 0, strTempEngMode[l], Font_System5x8);
	}
	sLen = strlen(strTempBody);//"Body:"
	for(l = 0; l < sLen; l++)
	{
		ST7565Draw6x8Char(0 + l*6, 16, strTempBody[l], Font_System5x8);
	}
	sLen = strlen(strTempUL);//"-------------------------"
	for(l = 0; l < sLen; l++)
	{
		ST7565Draw6x8Char(0 + l*6, 32, strTempUL[l], Font_System5x8);
	}
	sLen = strlen(strTempObject);//"Object:"
	for(l = 0; l < sLen; l++)
	{
		ST7565Draw6x8Char(0 + l*6, 40, strTempObject[l], Font_System5x8);
	}
	sLen = strlen(strTempCoe);//"Coe:"
	for(l = 0; l < sLen; l++)
	{
		ST7565Draw6x8Char(0 + l*6, 24, strTempCoe[l], Font_System5x8);
		ST7565Draw6x8Char(0 + l*6, 48, strTempCoe[l], Font_System5x8);
	}
	
	//
	// - Get Body Temperature Coe
	// - Get Object Temperature Coe
	//
	//**************************************************************
	//Temperature Coefficient
	//**************************************************************
	//Body
	int_buffer = info->BodyTempCoe*1000;
	ui32TempBodyCoe = int_buffer;
	
	sprintf((char*)ui8DatBuf, "%5d", int_buffer);
	
	sLen = strlen((char*)ui8DatBuf);
	for(l = 0; l < sLen; l++)
	{
		ST7565Draw6x8Char(30 + l*6, 24, ui8DatBuf[l], Font_System5x8);
	}
	
	//Object
	int_buffer = info->ObjTempCoe*1000;
	i32TempObjCoe = int_buffer;
	
	
	sprintf((char*)ui8DatBuf, "%5d", int_buffer);

	sLen = strlen((char*)ui8DatBuf);
	for(l = 0; l < sLen; l++)
	{
		ST7565Draw6x8Char(30 + l*6, 48, ui8DatBuf[l], Font_System5x8);
	}
	
	int_buffer = ui32TempBodyCoe;
	while(!SelectBtn(0)){
		if(RightBtn(2)){
			int_buffer = (int_buffer == TEMPMAX_B)? TEMPMIN_B:int_buffer+1;
		}
		else if(LeftBtn(2)){		
			int_buffer = (int_buffer == TEMPMIN_B)? TEMPMAX_B:int_buffer-1;
		}

		sprintf((char*)ui8DatBuf, "%5d", int_buffer);

		info->BodyTempCoe = (float)int_buffer / 1000;
		
		sLen = strlen((char*)ui8DatBuf);
		for(l = 0; l < sLen; l++)
		{
			ST7565Draw6x8Char(30 + l*6, 24, ui8DatBuf[l], Font_System5x8);
		}
	
		GetBodyTemp(&BodyTemp);
	
	
		if(BodyTemp < 0)
		{
			ui8TempDatBuf[0] = '-';
			BodyTemp = (-1) * BodyTemp;
		}
		else
		{
			ui8TempDatBuf[0] = '+';
		}
	
		ui32TempBuf = BodyTemp * 100;
		ui8TempDatBuf[1] = (ui32TempBuf < 1000)? ' ':(ui32TempBuf / 1000) + 0x30;
		ui8TempDatBuf[2] = (ui32TempBuf < 100)? '0':((ui32TempBuf % 1000) / 100) + 0x30;
		ui8TempDatBuf[3] = '.';
		ui8TempDatBuf[4] = ((ui32TempBuf % 100) / 10) + 0x30;
	
		sLen = sizeof(ui8TempDatBuf)/sizeof(uint8_t);
		for(l = 0; l < sLen; l++)
		{
			ST7565Draw6x8Char(72 + l*6, 24, ui8TempDatBuf[l], Font_System5x8);
		}
		ST7565Draw6x8Char(78 + l*6, 24, 'C', Font_System5x8);
	}

	save_system_info(&info->BodyTempCoe, sizeof(info->BodyTempCoe));

	ST7565Draw6x8Char(72, 16, 'O', Font_System5x8);
	ST7565Draw6x8Char(78, 16, 'K', Font_System5x8);
	ST7565Draw6x8Char(84, 16, '!', Font_System5x8);
	
	
	int_buffer = i32TempObjCoe;
	while(!SelectBtn(0)){
		if(RightBtn(2)){
			int_buffer = (int_buffer == TEMPMAX_O)? TEMPMIN_O:int_buffer+1;
		}
		else if(LeftBtn(2)){			
			int_buffer = (int_buffer == TEMPMIN_O)? TEMPMAX_O:int_buffer-1;
		}

		sprintf((char*)ui8DatBuf, "%5d", int_buffer);		
		
		info->ObjTempCoe = (float)int_buffer / 1000;
		
		sLen = strlen((char*)ui8DatBuf);
		for(l = 0; l < sLen; l++)
		{
			ST7565Draw6x8Char(30 + l*6, 48, ui8DatBuf[l], Font_System5x8);
		}
	
		GetObjTemp(&ObjTemp,&BodyTemp);
	
		if(ObjTemp < 0)
		{
			ui8TempDatBuf[0] = '-';
			ObjTemp = (-1) * ObjTemp;
		}
		else
		{
			ui8TempDatBuf[0] = '+';
		}
	
		ui32TempBuf = ObjTemp * 100;
		ui8TempDatBuf[1] = (ui32TempBuf < 1000)? ' ':(ui32TempBuf / 1000) + 0x30;
		ui8TempDatBuf[2] = (ui32TempBuf < 100)? '0':((ui32TempBuf % 1000) / 100) + 0x30;
		ui8TempDatBuf[3] = '.';
		ui8TempDatBuf[4] = ((ui32TempBuf % 100) / 10) + 0x30;
		
		sLen = sizeof(ui8TempDatBuf)/sizeof(uint8_t);
		for(l = 0; l < sLen; l++)
		{
			ST7565Draw6x8Char(72 + l*6, 48, ui8TempDatBuf[l], Font_System5x8);
		}
		ST7565Draw6x8Char(78 + l*6, 48, 'C', Font_System5x8);
	}
	save_system_info(&info->ObjTempCoe ,sizeof(info->ObjTempCoe));

	ST7565Draw6x8Char(72, 40, 'O', Font_System5x8);
	ST7565Draw6x8Char(78, 40, 'K', Font_System5x8);
	ST7565Draw6x8Char(84, 40, '!', Font_System5x8);
	while(!SelectBtn(0));
}


void Check_backlight(void)
{
	uint8_t l,sLen;
	uint16_t ui16DatBuf[6];
	uint16_t year= 0;
	uint8_t  month = 0;
	uint8_t  date = 0;
	uint8_t  day = 0;
	uint8_t  hour = 0;
	uint8_t  min = 0;
	uint8_t  sec = 0;
	char ui8ScreenLine1[10];
	char ui8ScreenLine2[8];
	uint8_t ui8DisplayBuf[17] = {0x00};
	
	//****************************************************************
	//	Backlight Test: Blue -> Green -> Orange -> Red
	//****************************************************************
	//
	//Blue On!
	//
	ST7650Blue();
	ST7565ClearScreen(BUFFER0);
	ST7565Refresh(BUFFER0);
	SetDisplayBuf(ui8DisplayBuf, 'P', 'R', 'E', 'S', 'S', ' ', 'L', 'E', 'F', 'T', ' ', ' ', ' ', ' ', ' ', ' ');
	ST7565DrawStringx16(0, 0, ui8DisplayBuf, Font_System5x8);
	SetDisplayBuf(ui8DisplayBuf, 'C', 'O', 'L', 'O', 'R', ':', ' ', 'B', 'L', 'U', 'E', ' ', ' ', ' ', ' ', ' ');
	ST7565DrawStringx16(0, 16, ui8DisplayBuf, Font_System5x8);
	while(!LeftBtn(0));

	//
	//Green On!
	//
	ST7650Green();
	ST7565ClearScreen(BUFFER0);
	ST7565Refresh(BUFFER0);
	SetDisplayBuf(ui8DisplayBuf, 'P', 'R', 'E', 'S', 'S', ' ', 'U', 'P', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ');
	ST7565DrawStringx16(0, 0, ui8DisplayBuf, Font_System5x8);
	SetDisplayBuf(ui8DisplayBuf, 'C', 'O', 'L', 'O', 'R', ':', ' ', 'G', 'R', 'E', 'E', 'N', ' ', ' ', ' ', ' ');
	ST7565DrawStringx16(0, 16, ui8DisplayBuf, Font_System5x8);
	while(!UpBtn(0));

	//
	//Orange On!
	//
	ST7650Yellow();
	ST7565ClearScreen(BUFFER0);
	ST7565Refresh(BUFFER0);
	SetDisplayBuf(ui8DisplayBuf, 'P', 'R', 'E', 'S', 'S', ' ', 'R', 'I', 'G', 'H', 'T', ' ', ' ', ' ', ' ', ' ');
	ST7565DrawStringx16(0, 0, ui8DisplayBuf, Font_System5x8);
	SetDisplayBuf(ui8DisplayBuf, 'C', 'O', 'L', 'O', 'R', ':', ' ', 'O', 'R', 'A', 'N', 'G', 'E', ' ', ' ', ' ');
	ST7565DrawStringx16(0, 16, ui8DisplayBuf, Font_System5x8);
	while(!RightBtn(0));

	//
	//Red On!
	//
	ST7650Red();
	ST7565ClearScreen(BUFFER0);
	ST7565Refresh(BUFFER0);
	SetDisplayBuf(ui8DisplayBuf, 'P', 'R', 'E', 'S', 'S', ' ', 'D', 'O', 'W', 'N', ' ', ' ', ' ', ' ', ' ', ' ');
	ST7565DrawStringx16(0, 0, ui8DisplayBuf, Font_System5x8);
	SetDisplayBuf(ui8DisplayBuf, 'C', 'O', 'L', 'O', 'R', ':', ' ', 'R', 'E', 'D', ' ', ' ', ' ', ' ', ' ', ' ');
	ST7565DrawStringx16(0, 16, ui8DisplayBuf, Font_System5x8);
	while(!DownBtn(0));

	//
	//Return to Blue Color
	//
	//
	//Blue On!
	//
	ST7650Blue();
	ST7565ClearScreen(BUFFER0);
	ST7565Refresh(BUFFER0);
	SetDisplayBuf(ui8DisplayBuf, 'P', 'R', 'E', 'S', 'S', ' ', 'E', 'N', 'T', 'E', 'N', ' ', ' ', ' ', ' ', ' ');
	ST7565DrawStringx16(0, 0, ui8DisplayBuf, Font_System5x8);
	SetDisplayBuf(ui8DisplayBuf, 'T', 'O', ' ', 'T', 'E', 'S', 'T', ' ', 'P', 'R', 'I', 'N', 'T', 'E', 'R', ' ');
	ST7565DrawStringx16(0, 16, ui8DisplayBuf, Font_System5x8);
	while(!SelectBtn(0))
	{
		//extracted "ExtRTCShowDateTime" function

		ExtRTCRead( &year, &month, &date, &day, &hour, &min, &sec);
		
		ui16DatBuf[0] = year;
		ui16DatBuf[1] = month;
		ui16DatBuf[2] = date;
		ui16DatBuf[3] = hour;
		ui16DatBuf[4] = min;
		ui16DatBuf[5] = sec;
		
		//year
		ui8ScreenLine1[0] = (ui16DatBuf[0] / 1000) + 0x30;
		ui8ScreenLine1[1] = ((ui16DatBuf[0] % 1000) / 100) + 0x30;
		ui8ScreenLine1[2] = ((ui16DatBuf[0] % 100) / 10) + 0x30;
		ui8ScreenLine1[3] = (ui16DatBuf[0] % 10) + 0x30;
		ui8ScreenLine1[4] = '/';
		
		//month
		ui8ScreenLine1[5] = (ui16DatBuf[1] / 10) + 0x30;
		ui8ScreenLine1[6] = (ui16DatBuf[1] % 10) + 0x30;
		ui8ScreenLine1[7] = '/';
		
		//date
		ui8ScreenLine1[8] = (ui16DatBuf[2] / 10) + 0x30;
		ui8ScreenLine1[9] = (ui16DatBuf[2] % 10) + 0x30;
		
		//hour
		ui8ScreenLine2[0] = (ui16DatBuf[3] / 10) + 0x30;
		ui8ScreenLine2[1] = (ui16DatBuf[3] % 10) + 0x30;
		ui8ScreenLine2[2] = ':';
		
		//minite
		ui8ScreenLine2[3] = (ui16DatBuf[4] / 10) + 0x30;
		ui8ScreenLine2[4] = (ui16DatBuf[4] % 10) + 0x30;
		ui8ScreenLine2[5] = ':';
		
		//second
		ui8ScreenLine2[6] = (ui16DatBuf[5] / 10) + 0x30;
		ui8ScreenLine2[7] = (ui16DatBuf[5] % 10) + 0x30;
		
		
		sLen = sizeof(ui8ScreenLine1);
		for(l = 0; l < sLen; l++)
		{
			//*********************
			//
			//	- 8x16 character
			//
			//*********************
			ST7565Draw6x8Char(65 + l*6, 38, ui8ScreenLine1[l], Font_System5x8);
		}
		
		sLen = sizeof(ui8ScreenLine2);
		for(l = 0; l < sLen; l++)
		{
			ST7565Draw6x8Char(65 + l*6, 38 + 8, ui8ScreenLine2[l], Font_System5x8);
		}
		
	}


}

void CCAEngMode(VOL_CH Mode)
{
	bool blCCASecondStage = false;

	char strCCAEngMode[] = "< CCA Calibration >";
	char strCCA6V[] = "Plz connect 6V bat";
	//char strCCA8V[] = "Plz connect 8V bat";
	char strCCA12V[] = "Plz connect 12V bat";
	char strTesting[] = "Testing...";
	char strCCACoe[] = "Coe:";
	char *strCCABuf;

	uint8_t ui8DatBuf[4] = {0x00};
	uint8_t l,sLen, ui8loop;

	unsigned int int_buffer = 0;

	unsigned int ui16Coe = 0;

	float fCCASum = 0.0;
	float fCCA = 0.0;
	TEST_DATA* TD = Get_test_data();
	SYS_INFO* info = Get_system_info();

	bool blDisplay = true;
	//************************************************************************
	//
	// CCA Calibration Mode
	// - 6V
	// - 8V
	// - 12V
	//
	//************************************************************************
	// Clears the screen
	ST7565ClearScreen(BUFFER0);
	// Refresh the screen to see the results
	ST7565Refresh(BUFFER0);

	switch(Mode)
	{
	case MAP_6V:
		//
		// Read 6V CCA Coe
		// Assign 6V channel
		//
		TD->SYSTEM_CHANNEL = MAP_6V;
		ui16Coe = info->CCA_Adjustment_6V*1000;
		strCCABuf = strCCA6V;
		break;
		//case MAP_8V:
		//
		//  //
		//  // Read 8V CCA Coe
		//  // Assign 8V channel
		//  //
		//	SYSTEM_CHANNEL = MAP_8V;
		//	IntEepromSvcReadTwoByte(&int_buffer, 10, 2);
		//	strCCABuf = strCCA8V;
		//	break;
	case MAP_12V:
		//
		// Read 12V CCA Coe
		// Assign 12V channel
		//
		TD->SYSTEM_CHANNEL = MAP_12V;
		ui16Coe = info->CCA_Adjustment_12V*1000;
		strCCABuf = strCCA12V;
		break;
	default:
		//
		//Error
		//
		UARTprintf("CCAEngMode Error: %d\n", TD->SYSTEM_CHANNEL);
		while(1);//debug
	}

	if(ui16Coe > IR_CCACOEMAX || ui16Coe < IR_CCACOEMIN)
	{
		while(1);//debug
	}

	sLen = strlen(strCCAEngMode);
	for(l = 0; l < sLen; l++)
	{
		ST7565Draw6x8Char(10 + l*6, 8, strCCAEngMode[l], Font_System5x8);
	}
	sLen = strlen(strCCABuf);
	for(l = 0; l < sLen; l++)
	{
		ST7565Draw6x8Char(0 + l*6, 24, strCCABuf[l], Font_System5x8);
	}
	sLen = strlen(strCCACoe);
	for(l = 0; l < sLen; l++)
	{
		ST7565Draw6x8Char(0 + l*6, 32, strCCACoe[l], Font_System5x8);
	}

	//
	// wait select key event
	//
	//*****************************************
	//
	//		First stage(Combo Key event)
	//
	//*****************************************
	while(!SelectBtn(0))
	{
		if(RightBtn(1))
		{
			//
			// Increase Coe
			//
			ui16Coe++;
			if(ui16Coe > IR_CCACOEMAX)
			{
				ui16Coe = IR_CCACOEMIN;
			}
	        blDisplay = true;
		}
		else if(LeftBtn(1))
		{
			//
			// Decrease Coe
			//
			ui16Coe--;
			if(ui16Coe < IR_CCACOEMIN)
			{
				ui16Coe = IR_CCACOEMAX;
			}
	        blDisplay = true;
		}
		if(blDisplay == true)
		{
	        blDisplay = false;

			ui8DatBuf[0] = (ui16Coe / 1000) + 0x30;
			ui8DatBuf[1] = ((ui16Coe % 1000) / 100) + 0x30;
			ui8DatBuf[2] = ((ui16Coe % 100) / 10) + 0x30;
			ui8DatBuf[3] = (ui16Coe % 10) + 0x30;

			//
			// '0': 0x30 -> ' ': 0x20
			//
			if(ui16Coe < 1000)
			{
				ui8DatBuf[0] = ' ';
				if(ui16Coe < 100)
				{
					ui8DatBuf[1] = ' ';
					if(ui16Coe < 10)
					{
						ui8DatBuf[2] = ' ';
					}
				}
			}
	    }

		sLen = sizeof(ui8DatBuf)/sizeof(uint8_t);
		for(l = 0; l < sLen; l++)
		{
			ST7565Draw6x8Char(30 + l*6, 32, ui8DatBuf[l], Font_System5x8);
		}
	}



	//
	// Select -> Right -> Left -> Select
	//
	if(RightBtn(0))
	{
		if(LeftBtn(0))
		{
			if(SelectBtn(0))
			{
				blCCASecondStage = true;
	            blDisplay = true;

	            while(1)
				{
			//
	                // Button Event
			//
	                if(blCCASecondStage == true)
					{
				//
				// Back to Select CCA Coe
				//
	                    if(RightBtn(1))
				{
					//
					// Reset Variables
					//
					fCCASum = 0;

	                        blDisplay = true;
	                        blCCASecondStage = false;
	                        
					//
					// Refresh LCD
					//
					//
					// 3 time cca respectly
					//
					for(ui8loop = 0; ui8loop <3; ui8loop++)
					{
						sLen = sizeof(ui8DatBuf)/sizeof(uint8_t);
						for(l = ui8loop * (sLen + 2); l < ui8loop * (sLen + 2) + sLen; l++)
						{
									ST7565Draw6x8Char(8 + l*6, 40, ' ', Font_System5x8);
						}
					}
					//
					// CCA average
					//
					sLen = sizeof(ui8DatBuf)/sizeof(uint8_t);
					for(l = 0; l < sLen; l++)
					{
								ST7565Draw6x8Char(8 + l*6, 48, ' ', Font_System5x8);
							}
						}
	                    else if(SelectBtn(1))
	                    {
	                        break;
	                    }
					}
					else
					{
						//
						// CCA coe adjustment or step into draw testing value.
						//
						if(RightBtn(1))
						{
							//
							// Increase Coe
							//
							ui16Coe++;
							if(ui16Coe > IR_CCACOEMAX)
							{
								ui16Coe = IR_CCACOEMIN;
							}
	                        blDisplay = true;
						}
						else if(LeftBtn(1))
						{
							//
							// Decrease Coe
							//
							ui16Coe--;
							if(ui16Coe < IR_CCACOEMIN)
							{
								ui16Coe = IR_CCACOEMAX;
							}
	                        blDisplay = true;
						}
						else if(SelectBtn(1))
						{
							blCCASecondStage = true;
							blDisplay = true;
						}
					}
							//
	                // Display Event
							//
	                if(blDisplay == true)
							{
	                    blDisplay = false;
	                    if(blCCASecondStage == true)
								{
	                        //***********************************
	                        // Refresh CCA coefficient
	                        //***********************************
						switch(Mode)
						{
						case MAP_6V:
							// Set CCA calibration coefficients
								info->CCA_Adjustment_6V = ((float)ui16Coe / 1000);
							break;
							//case MAP_8V:
							// Set CCA calibration coefficients
								//CCA_Adjustment_8V = ((float)ui16Coe / 1000);
							//	break;
						case MAP_12V:
							// Set CCA calibration coefficients
								info->CCA_Adjustment_12V = ((float)ui16Coe / 1000);
							break;
						default:
							//
							//Error
							//
								UARTprintf("CCAEngMode Error: %d\n", TD->SYSTEM_CHANNEL);
							while(1);//debug
						}
							sLen = strlen(strTesting);
							for(l = 0; l < sLen; l++)
							{
								ST7565Draw6x8Char(60 + l*6, 32, strTesting[l], Font_System5x8);
							}

						//
						// Get CCA 3 times
						// 123  123  123 -> respectly
						//      123      -> average
						//

						//
						// 3 time cca respectly
						//
						for(ui8loop = 0; ui8loop <3; ui8loop++)
						{
							BattCoreGetCCA(NULL, &fCCA, 3);
							int_buffer = fCCA;

							ui8DatBuf[0] = (int_buffer / 1000) + 0x30;
							ui8DatBuf[1] = ((int_buffer % 1000) / 100) + 0x30;
							ui8DatBuf[2] = ((int_buffer % 100) / 10) + 0x30;
							ui8DatBuf[3] = (int_buffer % 10) + 0x30;

							//
							// '0': 0x30 -> ' ': 0x20
							//
							if(int_buffer < 1000)
							{
								ui8DatBuf[0] = ' ';
								if(int_buffer < 100)
								{
									ui8DatBuf[1] = ' ';
									if(int_buffer < 10)
									{
										ui8DatBuf[2] = ' ';
									}
								}
							}

							sLen = sizeof(ui8DatBuf)/sizeof(uint8_t);
							for(l = ui8loop * (sLen + 2); l < ui8loop * (sLen + 2) + sLen; l++)
							{
									ST7565Draw6x8Char(8 + l*6, 40, ui8DatBuf[l-(ui8loop * (sLen + 2))], Font_System5x8);
							}
							fCCASum = fCCASum + fCCA;
						}

						//
						// CCA average
						//
						int_buffer = (fCCASum / 3);
						ui8DatBuf[0] = (int_buffer / 1000) + 0x30;
						ui8DatBuf[1] = ((int_buffer % 1000) / 100) + 0x30;
						ui8DatBuf[2] = ((int_buffer % 100) / 10) + 0x30;
						ui8DatBuf[3] = (int_buffer % 10) + 0x30;

						//
						// '0': 0x30 -> ' ': 0x20
						//
						if(int_buffer < 1000)
						{
							ui8DatBuf[0] = ' ';
							if(int_buffer < 100)
							{
								ui8DatBuf[1] = ' ';
								if(int_buffer < 10)
								{
									ui8DatBuf[2] = ' ';
								}
							}
						}

						sLen = sizeof(ui8DatBuf)/sizeof(uint8_t);
						for(l = 0; l < sLen; l++)
						{
								ST7565Draw6x8Char(8 + l*6, 48, ui8DatBuf[l], Font_System5x8);
						}

						//
						// Clear Testing...
						//
						sLen = strlen(strTesting);
						for(l = 0; l < sLen; l++)
						{
							ST7565Draw6x8Char(60 + l*6, 32, ' ', Font_System5x8);
						}
					}
						else
						{
	                        //*****************************************
	                        // Show CCA Coe and adjustment is available.
	                        //*****************************************
							ui8DatBuf[0] = (ui16Coe / 1000) + 0x30;
							ui8DatBuf[1] = ((ui16Coe % 1000) / 100) + 0x30;
							ui8DatBuf[2] = ((ui16Coe % 100) / 10) + 0x30;
							ui8DatBuf[3] = (ui16Coe % 10) + 0x30;
							
							//
							// '0': 0x30 -> ' ': 0x20
							//
							if(ui16Coe < 1000)
							{
								ui8DatBuf[0] = ' ';
								if(ui16Coe < 100)
								{
									ui8DatBuf[1] = ' ';
									if(ui16Coe < 10)
									{
										ui8DatBuf[2] = ' ';
									}
								}
				}

							sLen = sizeof(ui8DatBuf)/sizeof(uint8_t);
							for(l = 0; l < sLen; l++)
							{
								ST7565Draw6x8Char(30 + l*6, 32, ui8DatBuf[l], Font_System5x8);
							}
						}
					}
				}
			//
			// Save Parameters
			//
			switch(Mode)
			{
			case MAP_6V:
				//	6V CCA coe
					save_system_info(&info->CCA_Adjustment_6V , sizeof(info->CCA_Adjustment_6V));
				break;
				//case MAP_8V:
				//	8V CCA coe
					//IntEepromSvcSaveTwoByte(&ui16Coe, 10, 2);
				//	break;
			case MAP_12V:
				//	12V CCA coe
					save_system_info(&info->CCA_Adjustment_12V , sizeof(info->CCA_Adjustment_12V));
				break;
			default:
				//
				//Error
				//
					UARTprintf("CCAEngMode Error: %d\n", TD->SYSTEM_CHANNEL);
				while(1);//debug
			}
		}
		}
	}

	}

void IREngMode(VOL_CH Mode)
{
	bool blIRSecondStage = false;

	char strIREngMode[] = "< IR Calibration >";
	char strIR6V[] = "Plz connect 6V bat";
	//char strIR8V[] = "Plz connect 8V bat";
	char strIR12V[] = "Plz connect 12V bat";
	char strTesting[] = "Testing...";
	char strIRCoe[] = "Coe:";
	char *strIRBuf;

	uint8_t ui8DatBuf[5] = {0x00};
	uint8_t l,sLen, ui8loop;

	unsigned int int_buffer = 0;
	unsigned int ui16Coe = 0;
	float fIRSum = 0.0;
	float fIR = 0.0;
	TEST_DATA* TD = Get_test_data();
	SYS_INFO* info = Get_system_info();

	bool blDisplay = true;
	//************************************************************************
	//
	// IR Calibration Mode
	// - 6V
	// - 8V
	// - 12V
	//
	//************************************************************************
	// Clears the screen
	ST7565ClearScreen(BUFFER0);
	// Refresh the screen to see the results
	ST7565Refresh(BUFFER0);

	TD->SYSTEM_CHANNEL = (Mode == MAP_6V)? MAP_6V:MAP_12V;
	ui16Coe = (Mode == MAP_6V)? info->IRCalCoe6V*1000:info->IRCalCoe12V*1000;
	strIRBuf = (Mode == MAP_6V)? strIR6V:strIR12V;

	

	if(ui16Coe > IR_CCACOEMAX || ui16Coe < IR_CCACOEMIN)
	{
		while(1);//debug
	}

	sLen = strlen(strIREngMode);
	for(l = 0; l < sLen; l++)
	{
		ST7565Draw6x8Char(10 + l*6, 8, strIREngMode[l], Font_System5x8);
	}
	sLen = strlen(strIRBuf);
	for(l = 0; l < sLen; l++)
	{
		ST7565Draw6x8Char(0 + l*6, 24, strIRBuf[l], Font_System5x8);
	}
	sLen = strlen(strIRCoe);
	for(l = 0; l < sLen; l++)
	{
		ST7565Draw6x8Char(0 + l*6, 32, strIRCoe[l], Font_System5x8);
	}

	//
	// wait select key event
	//
	//*****************************************
	//
	//		First stage(Combo Key event)
	//
	//*****************************************
	while(!SelectBtn(0))
	{	    
		if(RightBtn(1))
		{
			//
			// Increase Coe
			//
			ui16Coe++;
			if(ui16Coe > IR_CCACOEMAX)
			{
				ui16Coe = IR_CCACOEMIN;
			}
	        blDisplay = true;
		}
		else if(LeftBtn(1))
		{
			//
			// Decrease Coe
			//
			ui16Coe--;
			if(ui16Coe < IR_CCACOEMIN)
			{
				ui16Coe = IR_CCACOEMAX;
			}
	        blDisplay = true;
		}	    
		if(blDisplay == true)
	    {
	        blDisplay = false;

            ui8DatBuf[0] = (ui16Coe < 1000)? ' ':(ui16Coe / 1000) + 0x30;
            ui8DatBuf[1] = ((ui16Coe % 1000) / 100) + 0x30;
            ui8DatBuf[2] = ((ui16Coe % 100) / 10) + 0x30;
            ui8DatBuf[3] = (ui16Coe % 10) + 0x30;
            
            sLen = sizeof(ui8DatBuf)/sizeof(uint8_t);
            for(l = 0; l < sLen - 1; l++)
            {
                ST7565Draw6x8Char(30 + l*6, 32, ui8DatBuf[l], Font_System5x8);
            }	        
	    }
	}
	
	//
	// Select -> Right -> Left -> Select
	//
	if(RightBtn(0)){
		if(LeftBtn(0)){
			if(SelectBtn(0)){
	            blIRSecondStage = true;
	            blDisplay = true;

	            while(1)
				{
	                //
	                // Button Event
	                //
	                if(blIRSecondStage == true)
	                {
	                    //
	                    // Back to Select IR Coe
	                    //
	                    if(RightBtn(1))
	                    {
	                        // Reset Variables
	                        //
	                        fIRSum = 0;
	                        
	                        blDisplay = true;
	                        blIRSecondStage = false;
	                        
	                        //
	                        // Refresh LCD
	                        //
	                        //
	                        // 3 time IR respectly
	                        //
	                        for(ui8loop = 0; ui8loop <3; ui8loop++)
	                        {
	                            sLen = sizeof(ui8DatBuf)/sizeof(uint8_t);
	                            for(l = ui8loop * (sLen + 2); l < ui8loop * (sLen + 2) + sLen; l++)
	                            {
	                                ST7565Draw6x8Char(8 + l*6, 40, ' ', Font_System5x8);
	                            }
	                        }
	                        //
	                        // IR average
	                        //
	                        sLen = sizeof(ui8DatBuf)/sizeof(uint8_t);
	                        for(l = 0; l < sLen; l++)
	                        {
	                            ST7565Draw6x8Char(8 + l*6, 48, ' ', Font_System5x8);
	                        }
	                        
	                    }
	                    else if(SelectBtn(1))
	                    {
	                        break;
	                    }
	                }
	                else
	                {
	                    //
	                    // IR coe adjustment or step into draw testing value.
	                    //
	                    if(RightBtn(1))
	                    {
	                        ui16Coe++;
	                        if(ui16Coe > IR_CCACOEMAX)
	                        {
	                            ui16Coe = IR_CCACOEMIN;
	                        }
	                        blDisplay = true;
	                    }
	                    else if(LeftBtn(1))
	                    {
	                        ui16Coe--;
	                        if(ui16Coe < IR_CCACOEMIN)
	                        {
	                            ui16Coe = IR_CCACOEMAX;
	                        }
	                        blDisplay = true;
	                    }
	                    else if(SelectBtn(1))
	                    {
	                        blIRSecondStage = true;
	                        blDisplay = true;
	                    }
	                }
	                //
	                // Display Event
	                //
	                if(blDisplay == true)
	                {
	                    blDisplay = false;
	                    if(blIRSecondStage == true)
	                    {
	                        //***********************************
	                        // Refresh IR coefficient
	                        //***********************************
	                        switch(Mode)
	                        {
	                        case MAP_6V:
	                            // Set IR calibration coefficients
	                        	info->IRCalCoe6V = ((float)ui16Coe / 1000);
	                            break;
	                        case MAP_12V:
	                            // Set IR calibration coefficients
	                        	info->IRCalCoe12V = ((float)ui16Coe / 1000);
	                            break;
	                        }
	                        // Draw "TESTING..."
	                        sLen = strlen(strTesting);
	                        for(l = 0; l < sLen; l++)
	                        {
	                            ST7565Draw6x8Char(60 + l*6, 32, strTesting[l], Font_System5x8);
	                        }
	                        
	                        //
	                        // Get IR 3 times
	                        // 123  123  123 -> respectly
	                        // 123           -> average
	                        //
	                        // 201709 do dual load and single load judge
	                        //
							BattCoreGetIR(&fIR, 1, dual_load_with_result);                        
	                        
	                        //
	                        // 3 time IR respectly
	                        //
	                        for(ui8loop = 0; ui8loop <3; ui8loop++)
	                        {
	            				switch(Mode)
								{
									case MAP_6V:
                            			BattCoreGetIR(&fIR, 3, dual_load_with_result);
										break;
									default:
										if(fIR > ThreewireTh)
										{
											BattCoreGetIR(&fIR, 3, single_load_with_result);
										}
										else
										{
											BattCoreGetIR(&fIR, 3, dual_load_with_result);
										}
										break;
								}
                        
	                        	//
	                        	// IR upper limitation
	                        	//
	                            if(fIR > 99.99)
	                            {
	                                fIR = 99.99;
	                            }
	                        
	                            int_buffer = fIR * 100;
	                        
	                            ui8DatBuf[0] = (int_buffer < 1000)? ' ':(int_buffer / 1000) + 0x30;
	                            ui8DatBuf[1] = ((int_buffer % 1000) / 100) + 0x30;
	                            ui8DatBuf[2] = '.';
	                            ui8DatBuf[3] = ((int_buffer % 100) / 10) + 0x30;
	                            ui8DatBuf[4] = (int_buffer % 10) + 0x30;
	                        	                        
	                            sLen = sizeof(ui8DatBuf)/sizeof(uint8_t);
	                            for(l = ui8loop * (sLen + 2); l < ui8loop * (sLen + 2) + sLen; l++)
	                            {
	                                ST7565Draw6x8Char(8 + l*6, 40, ui8DatBuf[l-(ui8loop * (sLen + 2))], Font_System5x8);
	                            }
	                            fIRSum = fIRSum + fIR;
	                        }
	                        
	                        //
	                        // IR average
	                        //
	                        int_buffer = (fIRSum / 3) * 100;
	                        ui8DatBuf[0] = (int_buffer < 1000)? ' ':(int_buffer / 1000) + 0x30;
	                        ui8DatBuf[1] = ((int_buffer % 1000) / 100) + 0x30;
	                        ui8DatBuf[2] = '.';
	                        ui8DatBuf[3] = ((int_buffer % 100) / 10) + 0x30;
	                        ui8DatBuf[4] = (int_buffer % 10) + 0x30;
	                        	                        
	                        sLen = sizeof(ui8DatBuf)/sizeof(uint8_t);
	                        for(l = 0; l < sLen; l++)
	                        {
	                            ST7565Draw6x8Char(8 + l*6, 48, ui8DatBuf[l], Font_System5x8);
	                        }
	                        
	                        //
	                        // Clear Testing...
	                        //
	                        sLen = strlen(strTesting);
	                        for(l = 0; l < sLen; l++)
	                        {
	                            ST7565Draw6x8Char(60 + l*6, 32, ' ', Font_System5x8);
	                        }
	                    }
	                    else
	                    {
	                        //*****************************************
	                        // Show IR Coe and adjustment is available.
	                        //*****************************************
	                        
	                        ui8DatBuf[0] = (ui16Coe < 1000)? ' ':(ui16Coe / 1000) + 0x30;
	                        ui8DatBuf[1] = ((ui16Coe % 1000) / 100) + 0x30;
	                        ui8DatBuf[2] = ((ui16Coe % 100) / 10) + 0x30;
	                        ui8DatBuf[3] = (ui16Coe % 10) + 0x30;
	                        	                        
	                        sLen = sizeof(ui8DatBuf)/sizeof(uint8_t);
	                        for(l = 0; l < sLen - 1; l++)
	                        {
	                            ST7565Draw6x8Char(30 + l*6, 32, ui8DatBuf[l], Font_System5x8);
	                        }           
	                    }
	                }
				}
	            //
	            // Save Parameters
	            //
	            switch(Mode)
	            {
	            case MAP_6V:
	                //  6V IR coe
	                info->IRCalCoe6V = ui16Coe;
	                info->IRCalCoe6V = (info->IRCalCoe6V + 0.5)/1000;
	                save_system_info(&info->IRCalCoe6V, sizeof(info->IRCalCoe6V));
	                break;
	            case MAP_12V:
	                //  12V IR coe
	                info->IRCalCoe12V = ui16Coe;
	                info->IRCalCoe12V = (info->IRCalCoe12V + 0.5)/1000;
	                save_system_info(&info->IRCalCoe12V, sizeof(info->IRCalCoe12V));
	                break;
	            }           
	        }
	    }
	}
}


