//*****************************************************************************
//
// Source File: amsvc.c
// Description: Functions to access voltage meter data form amper meter accessory.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 01/12/16     Vincent.T      Created file.
// 03/03/16     Vincent.T      1. Add AM meter offset
// 03/23/16     Vincent.T      1. New global variables to K amper meter.
// 06/14/16     Vincent.T      1. Modify GetAM(); to get more samples.
// 03/30/17     Vincent.T      1. Modify GetAM(); new current calculation eq.
// 09/11/18     Henry.Y        Use systeminfo data struct to replace the global variables.
// ============================================================================


#include "amsvc.h"
#include "../driver/uartstdio.h"
#include "SystemInfosvs.h"

//***********************************************
//
// - flaot: gfAMCalAD, AD offset of Amper meter.
//
//************************************************
float gfAMCalAD = 0;

void GetAM(float *pfAMvol, uint32_t *ADC)
{
	SYS_INFO* info = Get_system_info();
	//UARTprintf("GetAM()\n");

	//AD
	float fDatBuf = 0.0;
	
	AM_NEG5V_PIN_ON;
	AMGetAD(&fDatBuf, 250);	
	AM_NEG5V_PIN_OFF;
	
	*ADC = fDatBuf;

	//AD to Amper
	(*pfAMvol) = ((info->AMCalHvol - info->AMCalLvol) / (info->AMCalHAD - info->AMCalLAD)) * (fDatBuf - info->AMCalLAD) + info->AMCalLvol;
	//(*pfAMvol) = (fDatBuf - gfAMCalLAD);
}

