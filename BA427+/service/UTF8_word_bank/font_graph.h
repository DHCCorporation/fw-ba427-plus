//*****************************************************************************
//
// Source File: font_graph.h
// Description: font_graph data structure definitions.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 10/18/18     Jerry.W        Created file. output from(https://littlevgl.com/ttf-font-to-c-array)
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/driver/wifi_driver.h#2 $
// $DateTime: 2017/12/18 16:17:54 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#ifndef SERVICE_UTF8_WORD_BANK_DEFINE_H_
#define SERVICE_UTF8_WORD_BANK_DEFINE_H_

typedef struct{
	char* word;
	uint16_t glyph_index;
	uint8_t w_px;
}font_glyph_dsc_t;

typedef struct{
	uint32_t numb_word;
	const uint8_t *bitmap;
	const font_glyph_dsc_t* dsc;
	uint8_t height;
	
}font_table_dsc_t;



#endif
