//*****************************************************************************
//
// Source File: engmodesvc.h
// Description: Functions to do all k-coe funcs.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 03/03/16     Vincent.T      Created file.
// 04/14/16     Vincent.T      1. New K-IR func.
// 06/14/16     Vincent.T      1. Modify coefficients for different test items.
// 09/28/18     Jerry.W        modify temperature sensor calibrate function for new OPA and HW config.
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/service/engmodesvc.h#5 $
// $DateTime: 2017/09/22 10:27:40 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#ifndef __ENGMODESVC__
#define __ENGMODESVC__

#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include "..\driver/buttons.h"
#include "..\driver/st7565_lcm.h"
#include "..\driver/scanner_driver.h"
#include "inc/hw_memmap.h"
#include "driverlib/rom.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/fpu.h"
#include "driverlib/rom_map.h"
#include "../driver/uartstdio.h"
#include "mapsvc.h"
#include "vmsvc.h"
#include "amsvc.h"
#include "battcoresvc.h"
#include "tempsvc.h"
#include "extrtcsvc.h"
#include "lcdsvc.h"
#include "scanner_svc.h"
#include "printersvc.h"

#define IR_CCACOEMAX 1300
#define IR_CCACOEMIN 700

#define TEMPMAX_B 1500//1100
#define TEMPMIN_B 1//800

#define TEMPMAX_O 200
#define TEMPMIN_O -200

//*****************************************************************************
//
// Engineer Mode
//
//*****************************************************************************
uint8_t EngMode(void);
void TempEngMode(void);
void CCAEngMode(VOL_CH Mode);
void IREngMode(VOL_CH Mode);
void Check_backlight(void);
void BARCODE_ENG_MODE(void);

#endif /* __ENGMODESVC__ */
