//*****************************************************************************
//
// Source File: printersvc.c
// Description: Prototype for functions to control printer
//
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 10/26/15     Vincent.T     Created file.
// 10/30/15     Vincent.T     1. Add #include wordbanksvc.h
//                            2. Add PrinterStartWB(printersvc access database in wordbank)
// 11/03/15     Vincent.T     1. Skip M4 lockout if no paper detected
// 03/08/16     Vincent.T     1. Add errora handler func. to deal with problems when printing.
//                            2. new global variables blNoPaper, blPrintError.
// 03/16/16     Vincent.T     1. Re-define some functions.(More ezer to use)
//                            2. New func. PrinterPrintSeperate(); to print seperate line.
// 03/17/16     Vincent.T     1. New func. TestCodeGenerater(); to generate TestCode
//                            2. New func. ASCII2TestCode(); to search corresponding TestCode.
// 03/24/16     Henry.Y        1. Modify BattCoreChkIntBatt(): Add adc and voltage test parameter.
// 04/06/16     Vincent.T     1. Modify PrintBarCode() due to shift barcode location 8 bits.
// 06/14/16     Vincent.T     1. New global blPowerLow
//                            2. Modidy printer setting of bar(STB: 3seg. -> 6 seg.)
//                            3. Detect internal battery voltage while printing.
// 06/21/16     Vincent.T     1. Modify PrinterStart();, PrinterStart(); PrinterStartWB(); and PrintBarCode();(support float number heating time)
//                            2. Modify PrinterPrintSpace(); to speed-up step motor.
// 07/04/16     Vincent.T     1. Modify PrinterPrintSeperate(); step-motor pulse time.
// 08/10/16     Vincent.T     1. New global variable blInitBatReplaceEvent to determine replace internal battery or not.
//                            2. Modify PrinterSet() to support float number pulse width step-motor control
//                            3. during printing test result, if internal battery voltage under 5.3 more than 3 times -> "Replace internal battery"
//                            4. Modify PrinterPrintSpace();(input parameters)
//                            5. Modify PrinterPrintSeperate() to print dot line.
// 12/08/16     Vincent.T     1. #include "usbsvc.h".
//                            2. global variable ui8USBConnStat
//                            3. Modify USB plug-in detection in PrintErrorHanlder();
// 01/19/17     Vincent.T     1. Modify TestCodeGenerater();(Temperature Code)
// 03/22/17     Vincent.T     1. Modify TestCodeGenerater();(Test CCA)
// 03/30/17     Vincent.T     1. Modify TestCodeGenerater();(Temperature boundary)
// 04/05/17     Vincent.T     1. Modify PrintErrorHanlder();(USB connection function)
// 06/01/17     Henry.Y       FW version: BT2100_WW_V01.a
//                            Fix bug: negative temperature value lose after TestCodeGenerater();
// 09/12/17		Jerry.W		  optimise PrinterStartWB function
// 11/06/17     Henry.Y	      1. Hide useless variables:ui8USBConnStat
//                            2. Modify BattCoreChkClamp()
// 11/22/17     Henry.Y       TestCodeGenerater() apply hyundai test code rule.
// 11/23/17     Jerry.W       1. remove disused varable
// 09/11/18     Henry.Y       Use system error flag to replace the global variables.
// 11/19/18     Henry.Y       Modified PrinterPrintSpace() step motor control.
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/service/printersvc.c#1 $
// $DateTime: 2017/11/27 10:38:22 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#include "driverlib/rom.h"
#include "printersvc.h"
#include "battcoresvc.h"
#include "usbsvc.h"
#include "..\driver/usb_serial_structs.h"
#include "SystemInfosvs.h"

bool blInitBatReplaceEvent = false;

//******************************
//
// Declare USB connected status.
//
//******************************
//extern uint8_t ui8USBConnStat;


void PowerOn(void)
{
	ROM_GPIOPinWrite(PRINTER_POWER_BASE, PRINTER_POWER_PIN, 0xFF);
}

void PowerOff(void)
{
	ROM_GPIOPinWrite(PRINTER_POWER_BASE, PRINTER_POWER_PIN, 0x00);
}

void FeedPaper(void)
{
	static bool paperdetected = false;
	uint32_t paper_length = 0;
	//Paper initial status
	paperdetected = MP205CoSensorDetect();
	if(!paperdetected)	//no paper in stock
	{
		paperdetected = MP205CoSensorDetect();
		if(paperdetected)	//No paper -> Paper Detected
		{
			//Step-Motor On
			//Printer Power On
			PowerOn();
			PrinterStepMotorControl(0, 1);
			for(paper_length = 0; paper_length <FEED_PAPER_LENGTH; paper_length++)
			{
				BA6845StepMotorControl((paper_length % 4) + 1, 6);
			}
			//Step-Motor Off
			//Printer Power Off
			PowerOff();
		}
	}
	else
	{
		//Paper in stock
		paperdetected = MP205CoSensorDetect();
	}
}

void DataASCII2Byte(uint8_t *ui8DataSrc, uint8_t *ui8DataDest, uint8_t ui8DataLenSrc, uint8_t ui8DataLenDest)
{
	uint8_t ui8LoopIndexSrc = 0;
	uint8_t ui8LoopIndexDest = 0;

	//******************************
	//Length Correct?
	//98 - 1 - 1 = 96Byte/2 = 48Byte
	//******************************
	if(((ui8DataLenSrc - 2) / 2) != ui8DataLenDest)
	{
		//****************
		//ERROR!
		//CHECK DATA ARRAY
		//TRAP M4
		//****************
		while(1);
	}
	//*******************
	//START ASCII TO BYTE
	//*******************
	ui8LoopIndexSrc = 1;
	for(ui8LoopIndexDest = 0; ui8LoopIndexDest < ui8DataLenDest; ui8LoopIndexDest++)
	{
		//****************
		//A ~ F to 10 ~ 16
		//****************
		if(ui8DataSrc[ui8LoopIndexSrc] > '9')
		{
			//A ~ F
			ui8DataSrc[ui8LoopIndexSrc] = (ui8DataSrc[ui8LoopIndexSrc] - 0x37) << 4;
		}
		else
		{
			// 0 ~ 9
			ui8DataSrc[ui8LoopIndexSrc] = (ui8DataSrc[ui8LoopIndexSrc] - 0x30) << 4;
		}
		if(ui8DataSrc[ui8LoopIndexSrc + 1] > '9')
		{
			//A ~ F
			ui8DataSrc[ui8LoopIndexSrc + 1] = (ui8DataSrc[ui8LoopIndexSrc + 1] - 0x37);
		}
		else
		{
			// 0 ~ 9
			ui8DataSrc[ui8LoopIndexSrc + 1] = (ui8DataSrc[ui8LoopIndexSrc + 1] - 0x30);
		}
		ui8DataDest[ui8LoopIndexDest] = (ui8DataSrc[ui8LoopIndexSrc] | ui8DataSrc[ui8LoopIndexSrc + 1]);
		ui8LoopIndexSrc = ui8LoopIndexSrc + 2;
	}

}

void PrinterStepMotorControl(uint8_t ui8SMPhase, uint8_t ui8Delayms)
{
	BA6845StepMotorControl(ui8SMPhase, ui8Delayms);
}

void PrinterSet(uint8_t *ui8DataBuffer, uint8_t ui8STBgroup, float HeatingTimems, float fSMTimens, uint8_t ui8Repeat)
{
	static uint8_t ui8SMPhase = 0;
	uint8_t ui8PrintRepeat = ui8Repeat;

	if(!(Power_low & get_system_error())){
		//Paper?
		if(!MP205CoSensorDetect())
		{
			//Trap MCU
			PowerOff();

			//
			// Set blNoPaper
			//
			set_system_error(No_paper);

			//while(1);
			return;
		}

		//Transmit data to printer
		MP205DataTransControl(ui8DataBuffer);
		while(ui8PrintRepeat > 0)
		{
			//2 lines in one act
			ui8PrintRepeat--;

			//Heating
			MP205ThermalHeadControl(ui8STBgroup, HeatingTimems);

			//Step-Motor Control
			BA6845StepMotorControl((ui8SMPhase % 4) + 1, fSMTimens);

			//Overflow?
			ui8SMPhase = (ui8SMPhase+1)%256;
		}
		//Reset data buffer
		MP205BufferReset(ui8DataBuffer, MP205_PRINTOUT_SIZE);
		//Transmit data to printer
		MP205DataTransControl(ui8DataBuffer);
	}
}


void PrinterStart( uint8_t ui8PrtFldIdx, uint8_t ui8STBgroup, float HeatingTimems, float fSMTimens, uint8_t ui8Repeat)
{
	uint8_t blPrtFlag = PTR_READY;
	uint8_t ui8FlsPrtData[MP205_PRINTOUT_SIZE] = {0x00};
	uint8_t ui8PrtFlagData[MP205_PRINTOUT_SIZE] = {0x00};
	uint32_t ui32PrtAddrOffset = 0;



	while(1)
	{
		//Get data from flash
		ExtFlsReadPrtFldData((PRT_FIELD_SEL)ui8PrtFldIdx, ui32PrtAddrOffset, ui8FlsPrtData, MP205_PRINTOUT_SIZE);
		ui32PrtAddrOffset = ui32PrtAddrOffset + MP205_PRINTOUT_SIZE;
		/******************************/
		/* Set Printer Start/End Flag */
		/******************************/
		memset(ui8PrtFlagData, 'U', MP205_PRINTOUT_SIZE);

		/******************/
		/* Start/End flag */
		/******************/
		if(memcmp(ui8FlsPrtData, ui8PrtFlagData, MP205_PRINTOUT_SIZE) == 0)
		{
			blPrtFlag++;
			if(blPrtFlag == PTR_END)
			{
				//
				//Reset PowerLowCounter
				//
				break;
			}
		}
		else
		{
			//Set printer -> Data Buffer, STB(Thermal Heads), Heating Time, SM Timens, Repeat times
			//
			//  Set Printout Length here!!!
			//
			PrinterSet(ui8FlsPrtData, ui8STBgroup, HeatingTimems, fSMTimens, ui8Repeat);
		}

	}
}

void PrinterPrintSpace(uint32_t ui32SpaceNum)
{
	/*uint8_t ui8DatBuf[MP205_PRINTOUT_SIZE] = {0x00};
	PrinterSet(ui8DatBuf, STB_ALL, 0, 1.5, ui32SpaceNum);*/

	uint32_t i;
	for(i = 0; i <ui32SpaceNum; i++){
		BA6845StepMotorControl(0, 1);
	}
}

void PrinterStartWB(uint8_t *pui8Src, uint8_t ui8STBgroup, float HeatingTimems, uint8_t ui8SMTimens, uint8_t ui8Repeat)
{
	uint8_t ui8WBSeg = 0;
	uint8_t ui8PrtDataWB[MP205_PRINTOUT_SIZE];

	memset(ui8PrtDataWB, 0, sizeof(ui8PrtDataWB));

	for(ui8WBSeg = 0; ui8WBSeg < 16; ui8WBSeg++)
	{
		//Get Data
		PrinterSetDataWB(pui8Src, (uint16_t*)ui8PrtDataWB, ui8WBSeg);

		//
		//  Set Printout Length here!!!
		//
		PrinterSet(ui8PrtDataWB, ui8STBgroup, HeatingTimems, ui8SMTimens, ui8Repeat);
	}
}

void Printer_string(char* string, uint8_t repeat, uint8_t align){
    uint32_t len = strlen(string);
    uint8_t ui8WBSeg = 0;
    uint8_t ui8PrtDataWB[MP205_PRINTOUT_SIZE];
    uint16_t start;

    if(len > 24){
        string[24] = '\0';
    }

    memset(ui8PrtDataWB, 0, sizeof(ui8PrtDataWB));

    switch(align){
    case Printe_align_right:
        start = (24-len)*2;
        break;
    case Printe_align_left:
        start = 0;
        break;
    case Printe_align_central:
        start = (24-len);
        break;
    default: while(1){}
    }

    for(ui8WBSeg = 0; ui8WBSeg < 16; ui8WBSeg++)
    {
        //Get character
        PrinterSetDataWB((uint8_t*)string, (uint16_t*)&ui8PrtDataWB[start], ui8WBSeg);

        PrinterSet(ui8PrtDataWB, STB_ALL, 2, 1, repeat);
    }

}

void Printer_string2(char* string, uint8_t repeat, uint8_t align, uint8_t space){
    uint32_t len = strlen(string);
    uint8_t ui8WBSeg = 0;
    uint8_t ui8PrtDataWB[MP205_PRINTOUT_SIZE];
    uint16_t start;

    if(len > 24){
        string[24] = '\0';
    }

    memset(ui8PrtDataWB, 0, sizeof(ui8PrtDataWB));

    switch(align){
    case Printe_align_right:
        start = (24-len)*2-space*2;
        break;
    case Printe_align_left:
        start = space*2;
        break;
    case Printe_align_central:
        start = (24-len);
        break;
    default: while(1){}
    }

    for(ui8WBSeg = 0; ui8WBSeg < 16; ui8WBSeg++)
    {
        //Get character
        PrinterSetDataWB((uint8_t*)string, (uint16_t*)&ui8PrtDataWB[start], ui8WBSeg);

        PrinterSet(ui8PrtDataWB, STB_ALL, 2, 1, repeat);
    }

}

bool PrintErrorHanlder(void)
{
	bool blError = false;

	//if(((Clamp_err | No_paper | Power_low) & get_system_error()) || USBConnect()) blError = true;

	if(Clamp_err & get_system_error()){
		blError = true;
	}
	else if(No_paper & get_system_error())
	{
		//
		// No paper?
		// NoPaperwidget
		//
		set_system_error(No_paper);
		blError = true;
	}
	else if(USBConnect())
    {
		//
		// USB?
		//
		blError = true;
	}
	else if(Power_low & get_system_error())
	{
		//
		// Internal battery low?
		//
		blError = true;
	}

	if(blError)
	{
		set_system_error(printer_error);
		return true;
	}
	else
	{
		return false;
	}
}

void PrintBarCode(uint8_t *pui8Src, uint8_t ui8STBgroup, float HeatingTimems, uint8_t ui8SMTimens, uint8_t ui8Repeat)
{
	uint8_t ui8Loopindex = 0;
	uint8_t ui8SubLoopindex = 0;
	uint8_t ui8widlop = 0;
	uint8_t ui8widsublop = 0;
	uint8_t ui8BarcodeSize = 0;
	uint8_t ui8PrtDataWB[MP205_PRINTOUT_SIZE] = {0x00};
	uint16_t ui16WBData = 0;
	uint16_t ui16PrtDataWB[(MP205_PRINTOUT_SIZE/2)] = {0x00};
	uint32_t ui32WBBateBuf = 0;
	uint32_t ui32WBBate = 0;

	//
	//  Set Barcode Length here!!!
	//
	while(ui8BarcodeSize < 42)
	{
		//Get Data
		PrintSetDataBarCode(pui8Src, ui16PrtDataWB);

		ui8SubLoopindex = 1;
		for(ui8Loopindex = 0; ui8Loopindex < (MP205_PRINTOUT_SIZE/2)/2; ui8Loopindex++)
		{
			ui16WBData = ui16PrtDataWB[ui8Loopindex];
			ui32WBBate = 0;

			for(ui8widsublop = 0, ui8widlop = 0; ui8widlop < 16; ui8widlop++, ui8widsublop = ui8widsublop + 2)
			{
				//
				//  double the width
				//
				ui32WBBateBuf = ui32WBBate | (ui16WBData >> 15 - ui8widlop);
				ui32WBBateBuf = ui32WBBateBuf & 0x01;

				ui32WBBate = ui32WBBate | (ui32WBBateBuf << 31 - ui8widsublop);
				ui32WBBate = ui32WBBate | (ui32WBBateBuf << 31 - (ui8widsublop + 1));
			}

			ui8PrtDataWB[ui8SubLoopindex] = (ui32WBBate >> 24);
			ui8PrtDataWB[ui8SubLoopindex + 1] = ((ui32WBBate >> 16) & 0xFF );
			ui8PrtDataWB[ui8SubLoopindex + 2] = ((ui32WBBate >> 8) & 0xFF );
			ui8PrtDataWB[ui8SubLoopindex + 3] = ((ui32WBBate >> 0) & 0xFF );
			ui8SubLoopindex = ui8SubLoopindex + 4;
		}
		//Set printer -> Data Buffer, STB(Thermal Heads), Heating Time, SM Timens, Repeat times

		//
		//  Set Printout Length here!!!
		//
		PrinterSet(ui8PrtDataWB, ui8STBgroup, HeatingTimems, ui8SMTimens, ui8Repeat);
		ui8BarcodeSize++;
	}
}

void PrinterPrintSeperate(uint32_t ui32Width)
{
	uint8_t ui8LopIdx = 0;
	uint8_t ui8DatBuf[MP205_PRINTOUT_SIZE] = {0x00};

	for(ui8LopIdx = 0; ui8LopIdx < MP205_PRINTOUT_SIZE; ui8LopIdx++)
	{
		ui8DatBuf[ui8LopIdx] = 0xAA;
	}
	while(ui32Width > 0)
	{
		PrinterSet(ui8DatBuf, STB_1_2_3_4_5_6, 1.5, 1, 2);
		ui32Width--;
	}
	PrinterPrintSpace(5);
}

void Printer_SYS_Percent(uint32_t percent, uint8_t arrow){

    if(percent>100) percent = 100;
    uint8_t ui8DatBuf[MP205_PRINTOUT_SIZE] = {0x00};
    uint32_t i, j;


    uint32_t center = (320*percent/100) + 32;

    PrinterPrintSpace(10);

    if(arrow == arrow_middle){
        for(i = 40 ; i > 0 ; i--){
            memset(ui8DatBuf, 0, sizeof(ui8DatBuf));
            for(j = center - (i/3); j < center + (i/3) ; j++){
                set_bit_reverse(ui8DatBuf, j, 1);
            }
            PrinterSet(ui8DatBuf, STB_12_34_56, 3, 2, 1);
        }
    }
    else if(arrow == arrow_left){
        for(i = 0 ; i < 24 ; i++){
            memset(ui8DatBuf, 0, sizeof(ui8DatBuf));
            for(j = 28; j >= 28 - (float)i/1.5 ; j--){
                set_bit_reverse(ui8DatBuf, j, 1);
            }
            PrinterSet(ui8DatBuf, STB_12_34_56, 3, 2, 1);
        }
        for( ; i > 1 ; i--){
            memset(ui8DatBuf, 0, sizeof(ui8DatBuf));
            for(j = 28; j >= 28 - (float)i/1.5 ; j--){
                set_bit_reverse(ui8DatBuf, j, 1);
            }
            PrinterSet(ui8DatBuf, STB_12_34_56, 3, 2, 1);
        }
    }
    else if(arrow == arrow_right){
        for(i = 0 ; i < 24 ; i++){
            memset(ui8DatBuf, 0, sizeof(ui8DatBuf));
            for(j = 352; j <= 352 + (float)i/1.5 ; j++){
                set_bit_reverse(ui8DatBuf, j, 1);
            }
            PrinterSet(ui8DatBuf, STB_12_34_56, 3, 2, 1);
        }
        for( ; i > 1 ; i--){
            memset(ui8DatBuf, 0, sizeof(ui8DatBuf));
            for(j = 352; j <= 352 + (float)i/1.5 ; j++){
                set_bit_reverse(ui8DatBuf, j, 1);
            }
            PrinterSet(ui8DatBuf, STB_12_34_56, 3, 2, 1);
        }
    }

    memset(ui8DatBuf, 0, sizeof(ui8DatBuf));
    for(i = 16 ; i < 368 ; i++){
        set_bit_reverse(ui8DatBuf, i, i%2);
    }

    PrinterSet(ui8DatBuf, STB_12_34_56, 3, 1, 7);

    memset(ui8DatBuf, 0, sizeof(ui8DatBuf));
    ui8DatBuf[4] = 0xff;
    ui8DatBuf[43] = 0xff;
    PrinterSet(ui8DatBuf, STB_12_34_56, 3, 1, 32);

    PrinterPrintSpace(10);
    Printer_string(" MIN                MAX ", 3, Printe_align_left);

}

void Printer_BT_Percent(uint32_t percent){
    uint8_t ui8DatBuf[MP205_PRINTOUT_SIZE] = {0x00};
    char word[25];
    uint32_t i, j;

    uint32_t center = (320*percent/100) + 32;
    uint32_t word_start = center/16;
    if(percent < 100) word_start -= 1;
    else word_start -= 2;

    memset(word, ' ', sizeof(word));
    sprintf(&word[word_start], "%d%%", percent);
    word[strlen(word)] = ' ';

    Printer_string(word, 3, Printe_align_left);

    PrinterPrintSpace(10);

    for(i = 40 ; i > 0 ; i--){
        memset(ui8DatBuf, 0, sizeof(ui8DatBuf));
        for(j = center - (i/3); j < center + (i/3) ; j++){
            set_bit_reverse(ui8DatBuf, j, 1);
        }
        PrinterSet(ui8DatBuf, STB_12_34_56, 3, 1, 1);
    }

    memset(ui8DatBuf, 0, sizeof(ui8DatBuf));
    for(i = 32 ; i < 352 ; i++){
        set_bit_reverse(ui8DatBuf, i, i%2);
    }

    PrinterSet(ui8DatBuf, STB_12_34_56, 3, 1, 7);

    memset(ui8DatBuf, 0, sizeof(ui8DatBuf));
    ui8DatBuf[4] = 0xff;
    ui8DatBuf[43] = 0xff;
    PrinterSet(ui8DatBuf, STB_12_34_56, 3, 1, 32);

    PrinterPrintSpace(10);
    Printer_string(" 0                  100 ", 3, Printe_align_left);

}

void Printer_Dashed_line(uint16_t start, uint16_t end){
    uint32_t i;
    uint8_t ui8DatBuf[MP205_PRINTOUT_SIZE] = {0x00};
    memset(ui8DatBuf, 0, sizeof(ui8DatBuf));
    for(i = start ; i < end ; i++){
        set_bit_reverse(ui8DatBuf, i, i%2);
    }

    PrinterSet(ui8DatBuf, STB_12_34_56, 3, 1, 8);
}


