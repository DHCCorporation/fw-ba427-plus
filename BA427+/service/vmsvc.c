//*****************************************************************************
//
// Source File: vmsvc.c
// Description: Functions to access voltage meter data form voltage meter accessory.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 01/07/16     Vincent.T      Created file.
// 06/14/16     Vincent.T      1. Modify GetVM();(for more samples)
// 09/11/18     Henry.Y        Use systeminfo data struct to replace the global variables.
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/service/vmsvc.c#5 $
// $DateTime: 2017/09/22 10:27:40 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#include "vmsvc.h"
#include "../driver/uartstdio.h"
#include "SystemInfosvs.h"


void GetVM(float *pfVMvol, uint32_t *ADC)
{
	SYS_INFO* info = Get_system_info();
	//UARTprintf("GetVM()\n");

	//AD
	float fDatBuf = 0.0;
	VMGetAD(&fDatBuf, 100);
	*ADC = fDatBuf;

	//AD to Voltage
	(*pfVMvol) = ((info->VMCaHLvol - info->VMCalLvol) / (info->VMCaHLAD - info->VMCalLAD)) * (fDatBuf - info->VMCalLAD) + info->VMCalLvol;
}
