//*****************************************************************************
//
// Source File: HTTP_service.h
// Description: HTTP service
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 08/10/18     Jerry.W        Created file.
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/driver/wifi_driver.h#2 $
// $DateTime: 2017/12/18 16:17:54 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#ifndef SERVICE_HTTP_SERVICE_H_
#define SERVICE_HTTP_SERVICE_H_

#include "Socket_svc.h"

#define MAX_HTTP_CONN	3

#define HTTP_DEBUG	1

#define MAX_HEADER_LENGTH	1024


typedef enum{
	GET,
	POST
}REQUEST_TYPE;





/*************************************************************************************************
 * @ Name		: HTTP_request
 * @ Brief 		: lodge a HTTP request.
 * @ Parameter 	: type -- method type.
 * @ Parameter 	: server -- server domain name.
 * @ Parameter 	: URL -- the URL to be post to.
 * @ Parameter 	: header -- HTTP header.
 * @ Parameter 	: data -- the data for POST or query for GET.
 * @ Parameter 	: evn_cb -- callback function pointer to notice APP layer the post operation succeed or not.
 * @ Return		: true -- service layer send or buffer this command successfully.
 * 				  false -- service can't handle this request now.
 *************************************************************************************************/
bool HTTP_request(REQUEST_TYPE type, uint8_t *server, uint16_t port, uint8_t *URL, uint8_t *header,
					uint8_t *data, bool secure, bool certificate, void (*evn_cb)(uint16_t , uint16_t ,uint8_t *));

/*************************************************************************************************
 * @ Name		: HTTP_disconnect
 * @ Brief 		: disconnect server connection to resume resources.
 * @ Parameter 	: server -- server domain name.
 *************************************************************************************************/
void HTTP_disconnect(uint8_t *server, bool secure, uint16_t port);


/*************************************************************************************************
 * @ Name		: HTTP_service_poll
 * @ Brief 		: processing the service layer functionality(process the work to be done and asynchronous callback).
 *************************************************************************************************/
void HTTP_service_poll(void);



#endif /* SERVICE_HTTP_SERVICE_H_ */
