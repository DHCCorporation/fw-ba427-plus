//*****************************************************************************
//
// Source File: langaugesvc.h
// Description: languages set.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 11/30/18     Jerry.W        Created file.
// 08/20/20     David.Wang     Language modification for BT2200_HD_BO_USA
//
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/driver/wifi_driver.h#2 $
// $DateTime: 2017/12/18 16:17:54 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#ifndef SERVICE_LANGAUGESVC_H_
#define SERVICE_LANGAUGESVC_H_

#include <stdint.h>
#include "language_string.h"


#if 0//defined(__WW__)
#define ENGLISH_en 					1
#define FRANCAIS_en 				1
#define DEUTSCH_en 					1
#define ESPANOL_en 					1
#define ITALIAN_en 					1
#define PORTUGUES_en 				1
/*
#elif defined(__BO__)
#define ENGLISH_en 					1
#define FRANCAIS_en 				1
#define DEUTSCH_en 					1
#define ESPANOL_en 					1
#define ITALIAN_en 					1
#define PORTUGUES_en 				1
*/
#else
#define ENGLISH_en 					1
#define FRANCAIS_en 				1
#define DEUTSCH_en 					1
#define ESPANOL_en 					1
#define ITALIAN_en 					1
#define PORTUGUES_en 				1
#define NEDERLANDS_en 				1
#define DANSK_en 					0   
#define NORSK_en 					0   
#define SOOMI_en 					0   
#define SVENSKA_en 					0   
#define CESTINA_en 					0   
#define LIETUVIU_K_en 				0   
#define POLSKI_en 					0   
#define SLOVENCINA_en 				0   
#define SLOVENSCINA_en 				0   
#define TURKU_en 					0   
#define EESTI_en 					0   
#define LATVIESU_en 				0   
#define LIMBA_ROMANA_en 			0   
#define PUSSIJI_en 					0   
#define EAAHNIKA_en 				0   
#endif


/* Language */
typedef enum {
#if ENGLISH_en
	en_US = 0,
#endif
#if DEUTSCH_en
    de_DE,
#endif
#if ESPANOL_en
    es_ES,
#endif
#if FRANCAIS_en
	fr_FR,
#endif
#if ITALIAN_en
	it_IT,
#endif
#if PORTUGUES_en
	pt_PT,
#endif
#if NEDERLANDS_en
	nd_ND,
#endif
#if DANSK_en
	dk_DK,
#endif
#if NORSK_en
	nl_NW,
#endif
#if SOOMI_en
	sm_FN,
#endif
#if SVENSKA_en
	sv_SW,
#endif
#if CESTINA_en
	cs_CZ,
#endif
#if LIETUVIU_K_en
	lt_LT,
#endif
#if POLSKI_en
	pl_PL,
#endif
#if SLOVENCINA_en
	sl_SL,
#endif
#if SLOVENSCINA_en
	sls_SLJ,
#endif
#if TURKU_en
	tk_TR,
#endif
#if EESTI_en
	ee_EE,
#endif
#if LATVIESU_en
	la_LA,
#endif
#if LIMBA_ROMANA_en
	lm_ML,
#endif
#if PUSSIJI_en
	pu_RS,
#endif
#if EAAHNIKA_en
	ea_GK,
#endif
	number_total_language

} LANGUAGE;

void set_language(LANGUAGE lan);
LANGUAGE get_language_setting();

char* get_Language_String(uint32_t idx);



#endif /* SERVICE_LANGAUGESVC_H_ */
