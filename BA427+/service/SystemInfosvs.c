//*****************************************************************************
//
// Source File: SystemInfosvs.c
// Description: system parameter, mode, and error informations.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 02/27/18     Jerry.W	       Created file.
// 04/11/18     Henry.Y        system_parameter_init() add set default value to judgment counter of each battery type.
// 10/17/18     Henry.Y        Add battery test AGM JIS parameter.
// 10/23/18     Henry.Y        Add main backlight setup parameter.
// 11/19/18     Henry.Y        Add MFC test data record and function.
// 12/07/18     Henry.Y        Replace total defined language number by number_total_language.
// 03/20/19     Henry.Y        Add new LCM contrast parameter adjustment(match HW bias1/9, \5x booster).
// =============================================================================
//
//
// $Header: //depot/Tester/PB_Series/PB firmware/E1_V00/service/SystemInfosvs.c#2 $
// $DateTime: 2018/04/20 15:38:54 $
// $Author: HenryY $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************


#include "SystemInfosvs.h"
#include "../driver/uartstdio.h"
#include "../driver/inEEPROMdrv.h"
#include "../driver/delaydrv.h"
#include "inc/hw_ints.h"
#include <stddef.h>
#include "flashsvc.h"
#include "battcoresvc.h"
#include "engmodesvc.h"

char* SYSTEM_INFO_VER = "SIV0";
char* SYSTEM_PAR_VER = "SPV1";

/*
#pragma pack(push)
#pragma pack(4)
typedef struct{
	QR_data setting;
	uint8_t data[][];
}QR_data_full;
#pragma pack(pop)


QR_data_full QRcode_data;
*/



SYS_PAR system_parameter;
SYS_INFO system_info;
ERROR_Type system_error;
System_Mode system_mode = manual_test;

TEST_DATA shared_test_data;
MFC_TD MFC_TEST_DATA; 


/*these variable is defined at APP layer now*/
extern const uint16_t NUM_JIS_BT;
extern const uint16_t NUM_JIS_SS_EFB;
extern const uint16_t NUM_JIS_SS_AGM;


bool system_parameter_init(void){
	uint8_t i;
	int32_t error;
	uint8_t init_retry = 10;

	ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_EEPROM0);

		//check datasheet later for details

	while(EEPROMInit() != EEPROM_INIT_OK && init_retry){
		init_retry--;
		delay_ms(100);
		if(init_retry == 0){
			error = 2;
			goto ERR;
		}
	}
	
	
	EEPROMRead((uint32_t *)&system_info.Language_Select , SYSTEM_INFO_MAP_ADD + offsetof(SYS_INFO, Language_Select), 4);
	if(system_info.Language_Select >= number_total_language){
		//if Language_Select = undefined value
		system_info.Language_Select = en_US;
	}

	set_language((LANGUAGE)system_info.Language_Select);


	error = EEPROM_bytes_read_safe((uint8_t *)&system_info, SYSTEM_INFO_MAP_ADD, sizeof(system_info));
	if(error) goto ERR;
	
	if(strcmp(SYSTEM_INFO_VER, (char*)system_info.Version )){
		//initial !!!
		memset(system_info.Version, 0, 20);
		memcpy(system_info.Version,SYSTEM_INFO_VER, strlen(SYSTEM_INFO_VER));
		system_info.Language_Select = en_US;

		memset(system_info.SNC, 0, SNC_LEN);
		memset(system_info.SNP, 0, SNP_LEN);
		system_info.SNC[0] = '@';
		system_info.SNP[0] = '@';

		system_info.LCDRratio = LCDRRatio;
		system_info.LCDcontrast = LCDContrast;
		system_info.LcdBacklightConfig = 0;
		system_info.main_LcdBacklightConfig = 50;

		system_info.WIFIPR = 0;
		system_info.pre_test_state = 0;
		system_info.BTChkTimes = 0;
		system_info.Time_zone = 480;	//+8:00

		system_info.AD_1_5_6 = Voltage_Coefficient_AD6L;
		system_info.AD_7 = Voltage_Coefficient_AD6H;
		system_info.V6_L = Voltage_Coefficient_V6L;
		system_info.V6_H = Voltage_Coefficient_V6H;
		system_info.AD_1_5_8 = Voltage_Coefficient_AD8L;
		system_info.AD_9 = Voltage_Coefficient_AD8H;
		system_info.V8_L = Voltage_Coefficient_V8L;
		system_info.V8_H = Voltage_Coefficient_V8H;
		system_info.AD_1_5_12 = Voltage_Coefficient_AD12L;
		system_info.AD_15 = Voltage_Coefficient_AD12H;
		system_info.V12_L = Voltage_Coefficient_V12L;
		system_info.V12_H = Voltage_Coefficient_V12H;
		system_info.AD_1_5_24 = Voltage_Coefficient_AD24L;
		system_info.AD_31 = Voltage_Coefficient_AD24H;
		system_info.V24_L = Voltage_Coefficient_V24L;
		system_info.V24_H = Voltage_Coefficient_V24H;
		system_info.ADC_to_BAT_voltage = Voltage_Coefficient_AIN16;
		system_info.CCA_Adjustment_6V = CCA_Coefficient_6V;
		system_info.CCA_Adjustment_12V = CCA_Coefficient_12V;
		system_info.CCA_Adjustment_12V_Low = CCA_Coefficient_12V;
		system_info.CCA_Adjustment_12V_High = CCA_Coefficient_12V;
		system_info.CCA_TestV_Low = 0.512;
		system_info.CCA_TestV_High = 1.024;
		system_info.BodyTempCoe = BodyTempCoe_Default;
		system_info.ObjTempCoe = ObjTempCoe_Default;

		system_info.VMCalLAD = VMCalCoeADL;
		system_info.VMCaHLAD = VMCalCoeADH;
		system_info.VMCalLvol = VMCalCoeVL;
		system_info.VMCaHLvol = VMCalCoeVH;
		system_info.AMCalLAD = AMCalCoeADL;
		system_info.AMCalHAD = AMCalCoeAdH;
		system_info.AMCalLvol = AMCalCoeVL;
		system_info.AMCalHvol = AMCalCoeVH;

		system_info.IRCalCoe6V = IRCalCoe;
		system_info.IRCalCoe12V = IRCalCoe;

		system_info.VLoadVL_PLUS = VLoadLowV;
		system_info.VLoadVH_PLUS = VLoadHighV;
		system_info.VLoadADL_PLUS = VLoadLowVAD;
		system_info.VLoadADH_PLUS = VLoadHighVAD;
		system_info.VLoadVL_MINS = VLoadLowV;
		system_info.VLoadVH_MINS = VLoadHighV;
		system_info.VLoadADL_MINS = VLoadLowVAD;
		system_info.VLoadADH_MINS = VLoadHighVAD;


		system_info._3W_12IRH = 6.93;
		system_info._3W_12IRL = 2.67;
		system_info._3W_12IRH_COM = 21.31;
		system_info._3W_12IRL_COM = 16.88;

		system_info._3W_6IRH = 9.46;
		system_info._3W_6IRL = 2.55;
		system_info._3W_6IRH_COM = 20.41;
		system_info._3W_6IRL_COM = 14.40;



		system_info.SSTestCounter = 0;
		system_info.BatTestCounter = 0;
		system_info.SysTestCounter = 0;
		system_info.TotalTestRecord = 0;
		system_info.TestRecordOffset = 0;
		system_info.TotalSYSrecord = 0;
		system_info.SYSrecordOffset = 0;
		system_info.IRTestCounter = 0;

		system_info.device_registered = 0;
		system_info.bias9bter5x_LCDcontrast = bias9bter5x_LCDcontrast_default;

		memset(system_info.VkybdTextEditStr , ' ' , sizeof(system_info.VkybdTextEditStr));
		system_info.Has_DIY = 0;

		
		if(save_system_info(&system_info, sizeof(system_info)) == false) return false;


	}
	else{
		bool to_be_save = false;
		
		if(system_info.Has_DIY != 1 && system_info.Has_DIY != 0){
		    system_info.Has_DIY = 0;
		    if(save_system_info(&system_info.Has_DIY, sizeof(system_info.Has_DIY)) == false) return false;
		}

		if(system_info.bias9bter5x_LCDcontrast > bias9bter5x_LCDcontrast_max ||
			system_info.bias9bter5x_LCDcontrast < bias9bter5x_LCDcontrast_min){
			
			system_info.bias9bter5x_LCDcontrast = bias9bter5x_LCDcontrast_default;
			if(save_system_info(&system_info.bias9bter5x_LCDcontrast, sizeof(system_info.bias9bter5x_LCDcontrast)) == false) return false;
		}

		if(system_info.LCDcontrast > LCDcontrast_max || system_info.LCDcontrast < LCDcontrast_min){					
			system_info.LCDcontrast = LCDContrast;
			if(save_system_info(&system_info.LCDcontrast, sizeof(system_info.LCDcontrast)) == false) return false;
		}

		error = system_info.CCA_Adjustment_6V*1000;
		if(error > IR_CCACOEMAX || error < IR_CCACOEMIN){
			system_info.CCA_Adjustment_6V = CCA_Coefficient_6V;
			if(save_system_info(&system_info.CCA_Adjustment_6V, sizeof(system_info.CCA_Adjustment_6V)) == false) return false;
		}

		
		error = system_info.CCA_Adjustment_12V*1000;
		if(error > IR_CCACOEMAX || error < IR_CCACOEMIN){
			system_info.CCA_Adjustment_12V = CCA_Coefficient_12V;
			if(save_system_info(&system_info.CCA_Adjustment_12V, sizeof(system_info.CCA_Adjustment_12V)) == false) return false;
		}

		
		error = system_info.IRCalCoe6V*1000;
		if(error > IR_CCACOEMAX || error < IR_CCACOEMIN){
			system_info.IRCalCoe6V = IRCalCoe;
			if(save_system_info(&system_info.IRCalCoe6V, sizeof(system_info.IRCalCoe6V)) == false) return false;
		}

		
		error = system_info.IRCalCoe12V*1000;
		if(error > IR_CCACOEMAX || error < IR_CCACOEMIN){
			system_info.IRCalCoe12V = IRCalCoe;
			if(save_system_info(&system_info.IRCalCoe12V, sizeof(system_info.IRCalCoe12V)) == false) return false;
		}

		
		error = system_info.BodyTempCoe*1000;
		if(error > TEMPMAX_B || error < TEMPMIN_B){
			system_info.BodyTempCoe = BodyTempCoe_Default;
			if(save_system_info(&system_info.BodyTempCoe, sizeof(system_info.BodyTempCoe)) == false) return false;
		}
		
		error = system_info.ObjTempCoe*1000;
		if(error > TEMPMAX_O || error < TEMPMIN_O){
			system_info.ObjTempCoe = ObjTempCoe_Default;
			if(save_system_info(&system_info.ObjTempCoe, sizeof(system_info.ObjTempCoe)) == false) return false;
		}
		
		to_be_save = false;
		for(i = 0; i < SNC_LEN ; i++){
			if(system_info.SNC[0] == '@') break;
			if(system_info.SNC[i] == 0xff){
				to_be_save = true;
				//memset(system_info.SNC, 0, SNC_LEN); //no need to clean all the element.
				system_info.SNC[0] = '@';
				break;
			}
		}
		if(to_be_save) if(save_system_info(system_info.SNC, sizeof(system_info.SNC)) == false) return false;

		to_be_save = false;
		for(i = 0; i < SNP_LEN ; i++){	
			if(system_info.SNP[0] == '@') break;
			if(system_info.SNP[i] == 0xff){
				to_be_save = true;
				//memset(system_info.SNP, 0, SNP_LEN); //no need to clean all the element.
				system_info.SNP[0] = '@';
				break;
			}
		}
		if(to_be_save) if(save_system_info(system_info.SNP, sizeof(system_info.SNP)) == false) return false;


		to_be_save = false;
		if(system_info.TestRecordOffset >= MAX_BTSSTESTCOUNTER){
			system_info.TotalTestRecord = 0;
			system_info.TestRecordOffset = 0;
			if(save_system_info(&system_info.TotalTestRecord, sizeof(system_info.TotalTestRecord)) == false) return false;
			if(save_system_info(&system_info.TestRecordOffset, sizeof(system_info.TestRecordOffset)) == false) return false;
		}

		if(system_info.SYSrecordOffset >= MAX_SYSTESTCOUNTER){
			system_info.TotalSYSrecord = 0;
			system_info.SYSrecordOffset = 0;
			if(save_system_info(&system_info.TotalSYSrecord, sizeof(system_info.TotalSYSrecord)) == false) return false;
			if(save_system_info(&system_info.SYSrecordOffset, sizeof(system_info.SYSrecordOffset)) == false) return false;
		}

		if(system_info.main_LcdBacklightConfig > 100){
			system_info.main_LcdBacklightConfig = (system_info.LcdBacklightConfig < 10 || system_info.LcdBacklightConfig > 100)? 50:system_info.LcdBacklightConfig;
			if(save_system_info(&system_info.main_LcdBacklightConfig, sizeof(system_info.main_LcdBacklightConfig)) == false) return false;
		}

		if(system_info.LcdBacklightConfig){
			system_info.LcdBacklightConfig = 0;
			if(save_system_info(&system_info.LcdBacklightConfig, sizeof(system_info.LcdBacklightConfig)) == false) return false;
		}		

		if(system_info.device_registered > 1){
			system_info.device_registered = 0;
			if(save_system_info(&system_info.device_registered, sizeof(system_info.device_registered)) == false) return false;
		}
	}




	error = EEPROM_bytes_read_safe((uint8_t *)&system_parameter, SYSTEM_PAR_MAP_ADD, sizeof(system_parameter));
	if(error) goto ERR;
	
	if(strcmp(SYSTEM_PAR_VER, (char*)system_parameter.Version )){
		strcpy((char*)system_parameter.Version, SYSTEM_PAR_VER);

		system_parameter.BAT_test_par.BatteryType		= VRLA_GEL;
		system_parameter.BAT_test_par.SelectRating		= SAE;
		system_parameter.BAT_test_par.BTSetCapacityVal	= 500;
		system_parameter.BAT_test_par.BTJIS_RgBatSelect = 0;
		system_parameter.BAT_test_par.BTJIS_AGMBatSelect = 0;

		system_parameter.SS_test_par.SSBatteryType		= AGM_FLAT_PLATE;
		system_parameter.SS_test_par.SSSelectRating		= SAE;
		system_parameter.SS_test_par.SS_JIS_EFB_Select	= 0;
		system_parameter.SS_test_par.SS_JIS_AGM_Select	= 0;
		system_parameter.SS_test_par.SSSetCapacityVal	= 500;

		system_parameter.MainMenu = 1;

		if(save_system_parameter(&system_parameter, sizeof(system_parameter)) == false) return false;
	}
	else{
		bool to_be_save = false;
		if(system_parameter.MainMenu > 5){
			system_parameter.MainMenu = 0;
			to_be_save = true;
		}
		
		if(system_parameter.BAT_test_par.BatteryType > EFB){
			system_parameter.BAT_test_par.BatteryType = FLOODED;
			to_be_save = true;
		}
		
		if(system_parameter.BAT_test_par.SelectRating > 6){
			system_parameter.BAT_test_par.SelectRating = 0;
			to_be_save = true;
		}

		if(system_parameter.BAT_test_par.BTJIS_RgBatSelect >= NUM_JIS_BT){
			system_parameter.BAT_test_par.BTJIS_RgBatSelect = 0;
			to_be_save = true;
		}

		if(system_parameter.BAT_test_par.BTJIS_AGMBatSelect >= NUM_JIS_SS_AGM){
			system_parameter.BAT_test_par.BTJIS_AGMBatSelect = 0;
			to_be_save = true;
		}

		if(system_parameter.SS_test_par.SSBatteryType != AGM_FLAT_PLATE && system_parameter.SS_test_par.SSBatteryType != EFB){
			system_parameter.SS_test_par.SSBatteryType = AGM_FLAT_PLATE;
			to_be_save = true;
		}

		if(system_parameter.SS_test_par.SS_JIS_EFB_Select >= NUM_JIS_SS_EFB){
			system_parameter.SS_test_par.SS_JIS_EFB_Select = 0;
			to_be_save = true;
		}

		if(system_parameter.SS_test_par.SS_JIS_AGM_Select >= NUM_JIS_SS_AGM){
			system_parameter.SS_test_par.SS_JIS_AGM_Select = 0;
			to_be_save = true;
		}

	
		if(to_be_save) if(save_system_parameter(&system_parameter, sizeof(system_parameter)) == false) return false;
	}

	//load QRcode setting from EEPROM to RAM
/*
	error = EEPROM_bytes_read_safe((uint8_t *)&QRcode_data, SYSTEM_QR_MAP_ADD, sizeof(QRcode_data));
	if(error) goto ERR;
	if(QRcode_data.setting.set != 0){
		if(	(QRcode_data.setting.data_width < (QRcode_data.setting.data_height+7)/8)
			||(QRcode_data.setting.data_height >= 88 ) ){
			//QRcode setting error, reset setting.
			QRcode_data.setting.set = 0;
			QRcode_data.setting.data_width = 0;
			QRcode_data.setting.data_height = 0;
			save_QRcode(&QRcode_data.setting, NULL);
		}
	}
*/
	//  load MFC test record from EEPROM to RAM
//#if defined(PCB_CHECK_MODE)
	error = EEPROM_bytes_read_safe((uint8_t *)&MFC_TEST_DATA, SYSTEM_MFC_ADD, sizeof(MFC_TEST_DATA));
	if(error) goto ERR;
	
	if(MFC_TEST_DATA.start_up != 0 && MFC_TEST_DATA.start_up != 1 && MFC_TEST_DATA.start_up != 99){
		//initial
		
		MFC_TEST_DATA.start_up = 99;//0: fail, 1:pass, 99:no test record
		MFC_TEST_DATA.switch_left = 99;
		MFC_TEST_DATA.switch_up = 99;
		MFC_TEST_DATA.switch_right = 99;
		MFC_TEST_DATA.switch_down = 99;
		MFC_TEST_DATA.switch_enter = 99;
		MFC_TEST_DATA.backlight_check_blue = 99;
		MFC_TEST_DATA.backlight_check_green = 99;
		MFC_TEST_DATA.backlight_check_orange = 99;
		MFC_TEST_DATA.backlight_check_red = 99;
		MFC_TEST_DATA.voltage_6V = 99;
		MFC_TEST_DATA.voltage_12V = 99;
		MFC_TEST_DATA.voltage_24V = 99;
		MFC_TEST_DATA.voltage_int = 99;
		MFC_TEST_DATA.voltage_VM = 99;
		MFC_TEST_DATA.voltage_AM = 99;
		MFC_TEST_DATA.clock_IC = 99;
		//MFC_TEST_DATA.flash_SPI = 99;
		MFC_TEST_DATA.step_motor = 99;
		MFC_TEST_DATA.load_on = 99;
		
		memset(MFC_TEST_DATA.SSID, 0, SSID_LEN);
		memset(MFC_TEST_DATA.SSID_PW, 0, PW_LEN);
		
		if(save_MFC_TEST_DATA(&MFC_TEST_DATA, sizeof(MFC_TEST_DATA)) == false) return false;
	}
	else{
		bool to_be_save = false;
		for(i = 0; i < SSID_LEN ; i++){	
			if(MFC_TEST_DATA.SSID[i] == 0xff){
				to_be_save = true;
				memset(MFC_TEST_DATA.SSID, 0, SSID_LEN);
				memset(MFC_TEST_DATA.SSID_PW, 0, PW_LEN);
				break;
			}
		}
		if(to_be_save){
			if(save_MFC_TEST_DATA(MFC_TEST_DATA.SSID, sizeof(MFC_TEST_DATA.SSID)) == false) return false;
			if(save_MFC_TEST_DATA(MFC_TEST_DATA.SSID_PW, sizeof(MFC_TEST_DATA.SSID_PW)) == false) return false;
		}
		



	}



	return true;
		
ERR:
	if(error == 1) set_system_error(Voltage_low_mem);
	else set_system_error(Memory_error);

	return false;
}
		
TEST_DATA* Get_test_data(void){
	return &shared_test_data;
	}

SYS_INFO* Get_system_info(void){
	return &system_info;
}

bool save_system_info(void* par, uint32_t par_len){
	uint32_t add = (uint32_t) par;
	uint32_t base = (uint32_t) &system_info;
	uint32_t error;
	uint32_t offset;
	
	if((add < base) || ((add+par_len)>(base+sizeof(system_info)))) {
		set_system_error(Parameter_error);
		return false;
	}
	
	offset = add - base;
	error = EEPROM_bytes_write_safe((uint8_t *)par, SYSTEM_INFO_MAP_ADD+offset, par_len);

	if(error){
		if(error == 1) set_system_error(Voltage_low_mem);
		else if(error == 2) set_system_error(Memory_error);
		return false;
	}
	else return true;
}


SYS_PAR* Get_system_parameter(void){
	return &system_parameter;
}



bool save_system_parameter(void* par, uint32_t par_len){
	uint32_t add = (uint32_t) par;
	uint32_t base = (uint32_t) &system_parameter;
	uint32_t error;
	uint32_t offset;

	if((add < base) || ((add+par_len)>(base+sizeof(system_parameter)))){
		set_system_error(Parameter_error);
		return false;
	}
	
	offset = add - base;
	error =  EEPROM_bytes_write_safe((uint8_t *)par, SYSTEM_PAR_MAP_ADD+offset, par_len);
	
	if(error){
		if(error == 1) set_system_error(Voltage_low_mem);
		else if(error == 2) set_system_error(Memory_error);
		return false;
	}
	else return true;
}

void set_system_mode(System_Mode mode){
	system_mode = mode;
}

System_Mode get_system_mode(void){
	return system_mode;
}

void set_system_error(ERROR_Type err){
	system_error |= err;

	UARTprintf("set_system_error: 0x%x\n",err);
}

void clear_system_error(ERROR_Type err){
	system_error &= ~err;

	UARTprintf("clear_system_error, system_error = 0x%x\n\r",system_error);
}

ERROR_Type get_system_error(void){
	return system_error;
}

bool MFC_clear_eeprom_version(void){
	UARTprintf("MFC - clear_eeprom_version!\n\r");

	strcpy((char*)system_info.Version, "MFC_");
	if(save_system_info(system_info.Version, sizeof(system_info.Version)) == false) return false;

	strcpy((char*)system_parameter.Version, "MFC_");
	if(save_system_parameter(system_parameter.Version, sizeof(system_parameter.Version)) == false) return false;

	MFC_TEST_DATA.start_up = 99;
	MFC_TEST_DATA.switch_left = 99;
	MFC_TEST_DATA.switch_up = 99;
	MFC_TEST_DATA.switch_right = 99;
	MFC_TEST_DATA.switch_down = 99;
	MFC_TEST_DATA.switch_enter = 99;
	MFC_TEST_DATA.backlight_check_blue = 99;
	MFC_TEST_DATA.backlight_check_green = 99;
	MFC_TEST_DATA.backlight_check_orange = 99;
	MFC_TEST_DATA.backlight_check_red = 99;
	MFC_TEST_DATA.voltage_6V = 99;
	MFC_TEST_DATA.voltage_12V = 99;
	MFC_TEST_DATA.voltage_24V = 99;
	MFC_TEST_DATA.voltage_int = 99;
	MFC_TEST_DATA.voltage_VM = 99;
	MFC_TEST_DATA.voltage_AM = 99;
	MFC_TEST_DATA.clock_IC = 99;
	//MFC_TEST_DATA.flash_SPI = 99;
	MFC_TEST_DATA.step_motor = 99;
	MFC_TEST_DATA.load_on = 99;
	if(save_MFC_TEST_DATA(&MFC_TEST_DATA, sizeof(MFC_TEST_DATA)) == false) return false;

	return true;
}

MFC_TD* Get_MFC_TEST_DATA(void){
	return &MFC_TEST_DATA;
}

bool save_MFC_TEST_DATA(void* par, uint32_t par_len){
	uint32_t add = (uint32_t) par;
	uint32_t base = (uint32_t) &MFC_TEST_DATA;
	uint32_t error;
	uint32_t offset;
	
	if((add < base) || ((add+par_len)>(base+sizeof(MFC_TEST_DATA)))) {
		set_system_error(Parameter_error);
		return false;
	}
	
	offset = add - base;
	error = EEPROM_bytes_write_safe((uint8_t *)par, SYSTEM_MFC_ADD+offset, par_len);

	if(error){
		if(error == 1) set_system_error(Voltage_low_mem);
		else if(error == 2) set_system_error(Memory_error);
		return false;
	}
	else return true;
}





/* these codes are directly copied from E1 project
 * all the function calls need to modified if you want use QRcode function.

bool save_QRcode(QR_data *QR_setting, uint8_t *data){
	uint32_t error;
	uint32_t add_offset, lenth;
	if(QR_setting->set == 0){
		add_offset = offsetof(QR_data_full, setting);
		QRcode_data.setting.set = 0;
		QRcode_data.setting.data_width = 0;
		QRcode_data.setting.data_height = 0;
		error = EEPROM_bytes_write_safe((uint8_t *)&QRcode_data.setting, SYSTEM_QR_MAP_ADD + add_offset, sizeof(QRcode_data.setting));
		QRcode_data.setting.set = 0;
	}
	else{ //QR_setting->set == 1
		if(QR_setting->data_width < ((QR_setting->data_height+7) /8)){	//check format
			set_system_error(Parameter_error);
			return false;
		}

		memcpy(&QRcode_data.setting, QR_setting, sizeof(QR_data));

		error = EEPROM_bytes_write_safe((uint8_t *)QR_setting, SYSTEM_QR_MAP_ADD, sizeof(QR_data));
		if(error) goto ERR;

		lenth = QR_setting->data_width * QR_setting->data_height;
		add_offset = offsetof(QR_data_full, data);
		error = EEPROM_bytes_write_safe(data, SYSTEM_QR_MAP_ADD+add_offset, lenth);

	}

	if(error){
ERR:
		if(error == 1) set_system_error(Voltage_low_mem);
		else if(error == 2) set_system_error(Memory_error);
		return false;
	}
	else return true;
}

QR_data* get_QRcode_format(void){
	return &QRcode_data.setting;
}

bool get_QRcode_row(uint8_t row, uint8_t *buf){
	uint32_t error;
	uint32_t data_offset, add_offset;

	if(row > QRcode_data.setting.data_height){ //no more data
		set_system_error(Parameter_error);
		return false;
}

	data_offset = QRcode_data.setting.data_width * row;
	add_offset = offsetof(QR_data_full, data) + data_offset;

	error = EEPROM_bytes_read_safe(buf, SYSTEM_QR_MAP_ADD + add_offset , QRcode_data.setting.data_width);

	if(error){
		if(error == 1) set_system_error(Voltage_low_mem);
		else if(error == 2) set_system_error(Memory_error);
		return false;
	}
	else return true;
}

*/
