//*****************************************************************************
//
// Source File: flashsvc.c
// Description: Functions to Read/Write A25L032 Flash memory for LCD screen
//				/Printer field data access.
//
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 06/25/15     William.H      Created file.
// 10/05/15     William.H      Check in BT2020 project which branched from LCDFontMaker(Pre-BT2020) by modifed related GPIOs.
// 10/26/15     Vincent.T      Expand usb -> printer function.
// 10/26/15     Vincent.T      Modify GetPrtOffset in Flash.
// 11/15/16     Ian.C          1. Modify GetScrnOrSectAddrInFls(); to add new language case: pt_PT.
//                             2. Modify GetPrtFieldAddrInFls(); each language memory size is 0x26000.
// 05/18/18     Jerry.W        add flash header structure and functions. 
// 09/11/18     Henry.Y        Add set_language/get_language_setting.
// 11/20/18     Jerry          add non-blocking flash write function.
// 12/07/18     Jerry.W        modify flash layout.
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/service/flashsvc.c#5 $
// $DateTime: 2017/09/22 10:27:40 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "driverlib/rom.h"
//#include "driverlib/debug.h"
#include "../driver/uartstdio.h"
#include "flashsvc.h"
#include "..\driver/a25L032_fls.h"
#include "usbsvc.h"
#include "../driver/mem_protect.h"

typedef enum{
	fls_idle,
	fls_wait_to_read,
	fls_wait_to_erase,
	fls_programing,
}FLS_WRITE_STATE_T;

FlashHeader_t flash_header;



uint32_t fls_write_nonblocking_add;
uint32_t fls_write_nonblocking_len;
uint8_t* fls_write_buffer = NULL;
uint8_t* fls_write_buffer_temp = NULL;
void (*FLS_WRITE_DONE_CB)(void);
FLS_WRITE_STATE_T fls_write_nonblocking_state = fls_idle;
uint32_t num_LCM, num_Printer;

IMAGE_VER_t IMAGE_ver;
LAYOUT_INDEX_t LAYOUT;
//be careful ui32SectAddr+ui32BufLen don't cross flash sector
void ExtFlsWrite(uint32_t ui32SectAddr, uint8_t *ui8DatBuff, uint32_t ui32BufLen){
	uint32_t i,loop;
	uint32_t sect_add = (ui32SectAddr - ui32SectAddr%FLASH_SECTOR);
	uint32_t add_offset = (ui32SectAddr%FLASH_SECTOR);
	uint32_t write_len;

	if(add_offset+ui32BufLen > FLASH_SECTOR){
		write_len = FLASH_SECTOR-add_offset;
		ExtFlsWrite( ui32SectAddr + write_len,  ui8DatBuff + write_len,  ui32BufLen - write_len);
	}
	else{
		write_len = ui32BufLen;
	}

	uint8_t *sect_buf = malloc(FLASH_SECTOR);

	if(write_len != FLASH_SECTOR){
		if(add_offset){
			A25L032ReadData(sect_add, sect_buf, add_offset);
		}
		if(add_offset+write_len < FLASH_SECTOR){
			A25L032ReadData(sect_add+add_offset+write_len, sect_buf+(add_offset+write_len), FLASH_SECTOR - (add_offset+write_len));
		}
	}

	A25L032SectorErase(sect_add);

	memcpy(sect_buf + add_offset , ui8DatBuff, write_len);

	loop = FLASH_SECTOR/FLASH_PAGE;
	for(i = 0; i < loop; i++){
		A25L032PageProgram(sect_add + (i*256), sect_buf + (i*256), 256);
	}
	free(sect_buf);
}

void ExtFlsPoll(){
	static uint8_t *buf = NULL;
	static uint32_t sect_add;
	static uint32_t add_offset;
	static uint32_t write_len;
	static uint32_t writ_idx;
	if(fls_write_nonblocking_state != fls_idle){
		if(!A25L032BusyCheck()){	//if flash is not busy
			switch(fls_write_nonblocking_state){
			case fls_wait_to_read:
			{
				buf = malloc(FLASH_SECTOR);
				if(buf == NULL){	//malloc fail
					return;
				}
				add_offset = fls_write_nonblocking_add%FLASH_SECTOR;
				sect_add = (fls_write_nonblocking_add - add_offset);
				write_len = (add_offset+fls_write_nonblocking_len > FLASH_SECTOR)?
						(FLASH_SECTOR-add_offset) : fls_write_nonblocking_len;
				if(write_len != FLASH_SECTOR){
					if(add_offset){
						A25L032ReadData(sect_add, buf, add_offset);
					}
					if(add_offset+write_len < FLASH_SECTOR){
						A25L032ReadData(sect_add+add_offset+write_len, buf+(add_offset+write_len), FLASH_SECTOR - (add_offset+write_len));
					}
				}
				fls_write_nonblocking_state = fls_wait_to_erase;
			}
				break;
			case fls_wait_to_erase:
				A25L032SectorErase(sect_add);
				fls_write_nonblocking_state = fls_programing;
				memcpy(buf+add_offset ,fls_write_buffer_temp, write_len);
				writ_idx = 0;
				break;
			case fls_programing:
				A25L032PageProgram(sect_add + (writ_idx*FLASH_PAGE), buf + (writ_idx*FLASH_PAGE), FLASH_PAGE);
				writ_idx++;
				if(writ_idx == FLASH_SECTOR/FLASH_PAGE){
					fls_write_nonblocking_len -= write_len;
					free(buf);
					buf = NULL;
					if(fls_write_nonblocking_len != 0){
						fls_write_buffer_temp += write_len;
						fls_write_nonblocking_add += write_len;
						fls_write_nonblocking_state = fls_wait_to_read;
					}
					else{
						fls_write_nonblocking_state = fls_idle;
						free(fls_write_buffer);
						fls_write_buffer = NULL;
						if(FLS_WRITE_DONE_CB) FLS_WRITE_DONE_CB();
					}


				}
				break;
			}
		}
	}
}

bool ExtFlsWrite_nonblocking(uint32_t ui32SectAddr, uint8_t *ui8DatBuff, uint32_t ui32BufLen, void(*done_cb)(void)){
	if(fls_write_nonblocking_state != fls_idle){
		return false;
	}
	if((ui32SectAddr + ui32BufLen) > FLSAH_TOTAL_SIZE){
		return false;
	}
	fls_write_buffer = malloc(ui32BufLen);
	if(fls_write_buffer == NULL){
		return false;
	}
	fls_write_buffer_temp = fls_write_buffer;
	FLS_WRITE_DONE_CB = done_cb;
	fls_write_nonblocking_add = ui32SectAddr;
	fls_write_nonblocking_len = ui32BufLen;
	memcpy(fls_write_buffer, ui8DatBuff, ui32BufLen);
	fls_write_nonblocking_state = fls_wait_to_read;
	return true;
}


uint32_t GetScrnOffset(uint8_t ui8ScrnIdx)
{
	//
	// Check the arguments.
	//
	//ASSERT(ui8ScrnIdx <= 149);

	return(ui8ScrnIdx*0x400);
}



uint32_t GetScrnOrSectAddrInFls(   uint8_t ui8ScrnIdx)
{
	uint32_t ui32Addr;

	UARTprintf("GetScrnOrSectAddrInFls().\n");

	// Language index is <BASE ADDRESS>
	// Screen index is <OFFSET>
	// Screen address in Flash = <BASE ADDRESS> + <OFFSET>
	uint32_t start_add = (flash_header.image_mode == 0)? LAYOUT.IMAGE_add : LAYOUT.IMAGE2_add;

	ui32Addr = start_add + GetScrnOffset(ui8ScrnIdx);



	UARTprintf("Screen Address = 0x%x.\n", ui32Addr);

	return ui32Addr;
}



void ExtFlsLCDUpdData(LANGUAGE langIdx, uint8_t ui8ScrnIdx, uint8_t *lcdScrnBuff, uint32_t bufLen)
{
	uint32_t ui32SectAddr;

	if(ui8ScrnIdx >= 152 ) return;

	ui32SectAddr = GetScrnOrSectAddrInFls(  ui8ScrnIdx);

	ExtFlsWrite(ui32SectAddr, lcdScrnBuff, bufLen);
}

void ExtFlsReadLCDScrnData( uint8_t ui8ScrnIdx, uint8_t *lcdScrnBuff, uint32_t bufLen)
{
	uint32_t ui32ScrnAddr;

	ui32ScrnAddr = GetScrnOrSectAddrInFls(  ui8ScrnIdx);
	A25L032ReadData(ui32ScrnAddr, lcdScrnBuff, bufLen);	
}

uint32_t GetPrtOffset(PRT_FIELD_SEL ui8PrtFldIdx)
{
	//****************
	//blreadorwrite:
	//true:		read;
	//false:	write;
	//****************
	/****************/
	/* ~ 4096 bytes */
	/****************/
	return ui8PrtFldIdx*0x800;

}

uint32_t GetPrtFieldAddrInFls( PRT_FIELD_SEL ui8PrtFldIdx)
{
	uint32_t ui32PrtAddr;

	//UARTprintf("GetPrtZoneAddrInFls().\n");
	// Language index is <BASE ADDRESS>
	// Zone index is <OFFSET>
	// The content in zone address in Flash = <BASE ADDRESS> + <OFFSET>
	uint32_t start_add = (flash_header.image_mode == 0)? LAYOUT.IMAGE_add : LAYOUT.IMAGE2_add;
	start_add += LAYOUT.IMAGE_sector_LCM_len;

	ui32PrtAddr = start_add + GetPrtOffset(ui8PrtFldIdx);
	//UARTprintf("Printerout Address = 0x%x.\n", ui32PrtAddr);
	return ui32PrtAddr;
}

void ExtFlsWritePrtFldData(uint8_t *ui8DatBuff, uint8_t ui8PgTotal, uint32_t ui32PrtAddr)
{
	uint8_t ui8PgIdx;
	uint8_t ui8FlsPgBuff[256];
	uint16_t byteIdx;
	uint32_t ui32PgAddr;
	UARTprintf("ExtFlsWritePrtFldData()...\n");
	//Write Data
	for(ui8PgIdx = 0; ui8PgIdx < ui8PgTotal; ui8PgIdx++)
	{
		ui32PgAddr = ui32PrtAddr + ui8PgIdx*256;
		for(byteIdx = 0; byteIdx < 256; byteIdx++)
		{
			ui8FlsPgBuff[byteIdx] = ui8DatBuff[ui8PgIdx*256 + byteIdx];
		}

		A25L032PageProgram(ui32PgAddr, ui8FlsPgBuff, 256);
	}
}

void ExtFlsReadPrtFldData( PRT_FIELD_SEL ui8PrtFldIdx, uint32_t ui32offset, uint8_t *ui8Databuf, uint32_t ui32DataLen)
{
	uint32_t ui32PrtAddr = GetPrtFieldAddrInFls(  ui8PrtFldIdx);
	A25L032ReadData(ui32PrtAddr+ui32offset, ui8Databuf, ui32DataLen);
}

bool ExtFlsPrtUpdData(uint8_t *ui8DatBuff, LANGUAGE langIdx, PRT_FIELD_SEL ui8PrtFldIdx)
{
	uint32_t ui32PrtAddr;

	if(langIdx > sizeof(PRT_FIELD_SEL)) return false;

	//Get address
	ui32PrtAddr = GetPrtFieldAddrInFls( ui8PrtFldIdx);

	if(ui8PrtFldIdx == PRT_LOGO)
	{
		//One Sector: 4096 byte
		ExtFlsWrite(ui32PrtAddr, ui8DatBuff, 4096);

	}
	else
	{
		ExtFlsWrite(ui32PrtAddr, ui8DatBuff, 2048);
	}
	return true;
}


void ExtFlsWriteRecord(uint32_t Addr,uint16_t idx, uint8_t* buf, uint32_t len){
	uint32_t add = (Addr + (RECORD_UNIT_LENGTH*idx));
	ExtFlsWrite(add, buf, len);

}

//be careful ui32SectAddr+ui32BufLen don't cross flash sector
void ExtFlsReWriteRecord(uint32_t Addr,uint16_t idx, uint8_t* buf, uint32_t len){
	uint32_t add = (Addr + (RECORD_UNIT_LENGTH*idx));
	A25L032PageProgram(add, buf, len);
}

void ExtFlsReadRecord(uint32_t Addr, uint16_t idx, uint8_t* buf, uint8_t offset, uint32_t len){
	uint32_t add = (Addr + offset + (RECORD_UNIT_LENGTH*idx));

	A25L032ReadData(add, buf, len);
}

FlashHeader_t* ExtFls_get_header(void){
	static bool initaled = false;
	bool to_be_saved = false;
	if(!initaled){
		initaled = true;
		A25L032ReadData(0, (uint8_t*)&flash_header, sizeof(flash_header));

		if(flash_header.image_mode != 0 &&flash_header.image_mode != 1){
			flash_header.image_mode = 0;
			to_be_saved = true;
		}

		if(flash_header.OTA_FW_status.status == 0xFF){
			memset(&flash_header.OTA_FW_status, 0, sizeof(OTA_status_t));
			to_be_saved = true;
		}
		if(flash_header.OTA_FLS_IMAGE_status.status == 0xFF){
			memset(&flash_header.OTA_FLS_IMAGE_status, 0, sizeof(OTA_status_t));
			to_be_saved = true;
		}
		if(to_be_saved){
			ExtFls_set_header(&flash_header, sizeof(flash_header));
		}
	}

	return &flash_header;
}

void ExtFls_set_header(void* data, uint32_t length){

	if((data >= &flash_header)&&((uint8_t*)data+length <= ((uint8_t*) &flash_header) + sizeof(flash_header))){
		uint32_t add = (uint32_t)data - (uint32_t)&flash_header;
		ExtFlsWrite(add,data, length);
	}
}

IMAGE_VER_t* ExtFls_get_image_ver(void){
	return &IMAGE_ver;
}

void ExtFls_set_image_ver(){
	ExtFlsWrite(IMAGE_HEADER,IMAGE_ver.LCM, num_LCM);
	ExtFlsWrite(IMAGE_HEADER+num_LCM,IMAGE_ver.Printer, num_Printer);
}


LAYOUT_INDEX_t* ExtFls_get_layout(void){
	return &LAYOUT;
}

void ExtFls_set_layout(){
	ExtFlsWrite(LAYOUT_INDEX_HEADER,(uint8_t*)&LAYOUT, sizeof(IMAGE_VER_t));
}

void ExtFls_header_init(){
	bool to_be_saved = false;

	//header initial.
	ExtFlsRead(0, (uint8_t*)&flash_header, sizeof(flash_header));

	if(flash_header.image_mode != 0 &&flash_header.image_mode != 1){
		flash_header.image_mode = 0;
		to_be_saved = true;
	}

	if(flash_header.OTA_FW_status.status == 0xFF){
		memset(&flash_header.OTA_FW_status, 0, sizeof(OTA_status_t));
		to_be_saved = true;
	}
	if(flash_header.OTA_FLS_IMAGE_status.status == 0xFF){
		memset(&flash_header.OTA_FLS_IMAGE_status, 0, sizeof(OTA_status_t));
		to_be_saved = true;
	}
	if(to_be_saved){
		ExtFls_set_header(&flash_header, sizeof(flash_header));
	}

	//layout initial
	ExtFlsRead(LAYOUT_INDEX_HEADER, (uint8_t *)&LAYOUT, sizeof(LAYOUT));
	num_LCM = LAYOUT.IMAGE_sector_LCM_len / LAYOUT.IMAGE_LCM_len;
	num_Printer = LAYOUT.IMAGE_sector_Printer_len / LAYOUT.IMAGE_Printer_len;

	//image version initial
	IMAGE_ver.LCM = malloc(num_LCM);
	IMAGE_ver.Printer = malloc(num_Printer);
	ExtFlsRead(IMAGE_HEADER, IMAGE_ver.LCM, num_LCM);
	ExtFlsRead(IMAGE_HEADER + num_LCM, IMAGE_ver.Printer, num_Printer);

}
