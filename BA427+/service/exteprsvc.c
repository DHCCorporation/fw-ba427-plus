//*****************************************************************************
//
// Source File: exteprsvc.c
// Description: Functions to Read/Write/Chip Erase/Deep Power Down of 25LC512
//              EEPROM IC.
//
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 10/14/15     Vincent.T      Created file.
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/service/exteprsvc.c#5 $
// $DateTime: 2017/09/22 10:27:40 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************


#include "exteprsvc.h"

#include <stdbool.h>
#include <stdint.h>
#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "driverlib/rom.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/ssi.h"

void ExtEprWriteSeriesData(uint8_t *ui8DataBuff, uint16_t ui16Addr, uint8_t ui8DataLen)
{
	uint8_t ui8CmdBuf = 0;

	//WERN
	ExtEprWriteWREN();

	//Busy?
	ExtEprWaitBusy();

	// -CS Low to enable extepric
	EERPOM_IC_CS_LOW;

	//Instruction
	ui8CmdBuf = EPR_WRITE;
	EPR_25LC512ModeSel(EPR_25LC512_WRITE);
	EPR_25LC512WriteByte(&ui8CmdBuf, 1);

	//Address
	//High > Low
	ui8CmdBuf = (ui16Addr >> 8);
	EPR_25LC512WriteByte(&ui8CmdBuf, 1);
	ui8CmdBuf = (ui16Addr);
	EPR_25LC512WriteByte(&ui8CmdBuf, 1);


	//Write Data
	EPR_25LC512ModeSel(EPR_25LC512_WRITE);
	EPR_25LC512WriteByte(ui8DataBuff, ui8DataLen);

	//-CS High to disable exrepric
	EERPOM_IC_CS_HIGH;

	//WRDI
	ExtEprWriteWRDI();
}

void ExtEprReadSeriesData(uint8_t *ui8DataBuff, uint16_t ui16Addr, uint8_t ui8DataLen)
{
	uint8_t ui8CmdBuf = EPR_READ;

	//Busy?
	ExtEprWaitBusy();

	// -CS Low to enable extepric
	EERPOM_IC_CS_HIGH;
	EERPOM_IC_CS_LOW;
	//Instruction
	EPR_25LC512ModeSel(EPR_25LC512_WRITE);
	EPR_25LC512WriteByte(&ui8CmdBuf, 1);

	//Address
	//High > Low
	ui8CmdBuf = (ui16Addr >> 8);
	EPR_25LC512WriteByte(&ui8CmdBuf, 1);
	ui8CmdBuf = (ui16Addr);
	EPR_25LC512WriteByte(&ui8CmdBuf, 1);

	//Read Data
	EPR_25LC512ModeSel(EPR_25LC512_READ);
	EPR_25LC512ReadByte(ui8DataBuff, ui8DataLen);

	//-CS High to disable exrepric
	EERPOM_IC_CS_HIGH;
}

void ExtEprReadStatReg(uint8_t *ui8StatReg)
{
	uint8_t ui8CmdBuf = EPR_RDSR;

	// -CS Low to enable extepric
	EERPOM_IC_CS_HIGH;
	EERPOM_IC_CS_LOW;

	//Instruction
	EPR_25LC512ModeSel(EPR_25LC512_WRITE);
	EPR_25LC512WriteByte(&ui8CmdBuf, 1);

	//Read Data
	EPR_25LC512ModeSel(EPR_25LC512_READ);
	EPR_25LC512ReadByte(ui8StatReg, 1);

	//-CS High to disable exrepric
	EERPOM_IC_CS_HIGH;
}

extern void ExtEprWriteWREN(void)
{
	uint8_t ui8CmdBuf = EPR_WREN;

	//Busy?
	ExtEprWaitBusy();

	// -CS Low to enable extepric
	EERPOM_IC_CS_HIGH;
	EERPOM_IC_CS_LOW;

	//WREN
	EPR_25LC512ModeSel(EPR_25LC512_WRITE);
	EPR_25LC512WriteByte(&ui8CmdBuf, 1);
	EERPOM_IC_CS_HIGH;

	//Check Write-Enable-Latch
	ExtEprChkWel();
	//Busy?
	while(ROM_SSIBusy(EPR_25LC512_MODULE_BASE));
}

extern void ExtEprWriteWRDI(void)
{
	uint8_t ui8CmdBuf = EPR_WRDI;

	//Busy?
	ExtEprWaitBusy();

	// -CS Low to enable exteprics
	EERPOM_IC_CS_HIGH;
	EERPOM_IC_CS_LOW;

	//WRDI
	EPR_25LC512ModeSel(EPR_25LC512_WRITE);
	EPR_25LC512WriteByte(&ui8CmdBuf, 1);
	EERPOM_IC_CS_HIGH;
	//Busy?
	while(ROM_SSIBusy(EPR_25LC512_MODULE_BASE));
}

void ExtEprWaitBusy(void)
{
	uint8_t ui8StatReg = 0x00;

	do
	{
		//Read Status Register
		ExtEprReadStatReg(&ui8StatReg);

		//WIP?
		ui8StatReg = (ui8StatReg & 0x01);
	}while(ui8StatReg);
}

void ExtEprChkWel(void)
{
	uint8_t ui8StatReg = 0x00;

	do
	{
		//Read Status Register
		ExtEprReadStatReg(&ui8StatReg);

		//WIP?
		ui8StatReg = (ui8StatReg & 0x02);
	}while(!ui8StatReg);
}

void ExtEprChipErase(void)
{
	uint8_t ui8CmdBuf = EPR_CHIP_ERASE;

	//Busy?
	ExtEprWaitBusy();

	//WERN
	ExtEprWriteWREN();

	// -CS Low to enable extepric
	EERPOM_IC_CS_HIGH;
	EERPOM_IC_CS_LOW;

	//Instruction
	EPR_25LC512ModeSel(EPR_25LC512_WRITE);
	EPR_25LC512WriteByte(&ui8CmdBuf, 1);

	//-CS High to disable exrepric
	EERPOM_IC_CS_HIGH;

	//Busy?
	ExtEprWaitBusy();

	//WRDI
	ExtEprWriteWRDI();
	//Busy?
	while(ROM_SSIBusy(EPR_25LC512_MODULE_BASE));
}
void ExtEprSectorErase(uint16_t ui16SectorNum)
{
	uint8_t ui8CmdBuf = EPR_SEC_ERASE;

	//Busy?
	ExtEprWaitBusy();

	//WERN
	ExtEprWriteWREN();

	// -CS Low to enable extepric
	EERPOM_IC_CS_HIGH;
	EERPOM_IC_CS_LOW;

	//Instruction
	EPR_25LC512ModeSel(EPR_25LC512_WRITE);
	EPR_25LC512WriteByte(&ui8CmdBuf, 1);

	//Sector Num -> all bits -> 0xFF
	//High > Low
	ui8CmdBuf = (ui16SectorNum >> 8);
	EPR_25LC512WriteByte(&ui8CmdBuf, 1);
	ui8CmdBuf = (ui16SectorNum);
	EPR_25LC512WriteByte(&ui8CmdBuf, 1);


	//-CS High to disable exrepric
	EERPOM_IC_CS_HIGH;

	//WRDI
	ExtEprWriteWRDI();
	//Busy?
	while(ROM_SSIBusy(EPR_25LC512_MODULE_BASE));
}

void ExtEprPageErase(uint16_t ui16PageNum)
{
	uint8_t ui8CmdBuf = EPR_PAGE_ERASE;

	//Busy?
	ExtEprWaitBusy();

	//WERN
	ExtEprWriteWREN();

	// -CS Low to enable extepric
	EERPOM_IC_CS_HIGH;
	EERPOM_IC_CS_LOW;

	//Instruction
	EPR_25LC512ModeSel(EPR_25LC512_WRITE);
	EPR_25LC512WriteByte(&ui8CmdBuf, 1);

	//Page Num -> all bits -> 0xFF
	//High > Low
	ui8CmdBuf = (ui16PageNum >> 8);
	EPR_25LC512WriteByte(&ui8CmdBuf, 1);
	ui8CmdBuf = (ui16PageNum);
	EPR_25LC512WriteByte(&ui8CmdBuf, 1);

	//-CS High to disable exrepric
	EERPOM_IC_CS_HIGH;

	//WRDI
	ExtEprWriteWRDI();
	//Busy?
	while(ROM_SSIBusy(EPR_25LC512_MODULE_BASE));
}

void ExtEprDPDMode(void)
{
	uint8_t ui8CmdBuf = EPR_DPD;

	//Busy?
	ExtEprWaitBusy();

	//WERN
	ExtEprWriteWREN();

	// -CS Low to enable extepric
	EERPOM_IC_CS_HIGH;
	EERPOM_IC_CS_LOW;

	//Instruction
	EPR_25LC512ModeSel(EPR_25LC512_WRITE);
	EPR_25LC512WriteByte(&ui8CmdBuf, 1);

	//-CS High to disable exrepric
	EERPOM_IC_CS_HIGH;

	//Busy?
	while(ROM_SSIBusy(EPR_25LC512_MODULE_BASE));
}

void ExtEprRelDPDMode(void)
{
	uint8_t ui8CmdBuf = EPR_RDPD;

	// -CS Low to enable extepric
	EERPOM_IC_CS_HIGH;
	EERPOM_IC_CS_LOW;

	//Instruction
	EPR_25LC512ModeSel(EPR_25LC512_WRITE);
	EPR_25LC512WriteByte(&ui8CmdBuf, 1);

	//Sent dummy adress to get manufacturer's ID
	ui8CmdBuf = 0x00;
	EPR_25LC512WriteByte(&ui8CmdBuf, 1);
	ui8CmdBuf = 0x00;
	EPR_25LC512WriteByte(&ui8CmdBuf, 1);

	//Read Data(Manufacturer's ID)
	EPR_25LC512ModeSel(EPR_25LC512_READ);
	EPR_25LC512ReadByte(&ui8CmdBuf, 1);

	//-CS High to disable exrepric
	EERPOM_IC_CS_HIGH;
	//Busy?
	while(ROM_SSIBusy(EPR_25LC512_MODULE_BASE));
}
