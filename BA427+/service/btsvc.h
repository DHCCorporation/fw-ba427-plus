//*****************************************************************************
//
// Source File: btsvc.h
// Description: Bluetooth service functions' definition.
//				
//
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 01/13/15     William.H      Created file.
// 01/27/16     Henry.Y        Declare BTClear_Status();
// 11/15/16     Ian.C          1. Modify BTClear_Status(); to BTClear_Status(g_LangSelectIdx); for multi-language.
// 11/10/17     Jerry.W        1. Modify BT service layer for more flexibile application
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/service/btsvc.h#1 $
// $DateTime: 2017/11/27 10:38:22 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#ifndef __BTSVC_H__
#define __BTSVC_H__

#include <stdint.h>
#include "flashsvc.h"

#include <stdbool.h>
#include <string.h>
#include "driverlib/rom.h"
#include "../driver/uartstdio.h"
#include "battcoresvc.h"
#include "Lcdsvc.h"
#include "testrecordsvc.h"
#include "..\driver/btuart_iocfg.h"
#include "..\driver/st7565_lcm.h"

#define BluetoothConnected (!ROM_GPIOPinRead(BT_CONN_STATUS_BASE, BT_CONN_STATUS_PIN))
extern void BTUARTSend(const char *pui8Buffer, uint32_t ui32Count);
void Bluetooth_buffer_flush();
extern void set_BTcommand_callback(void (*com_callback)(const char*), bool (*command_match)(const char*,uint8_t));
#endif
