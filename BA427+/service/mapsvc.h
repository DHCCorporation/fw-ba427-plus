//*****************************************************************************
//
// Source File: mapsvc.h
// Description: Prototype for functions to know battery good or bad.
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 10/23/15     Vincent.T      Created file.
// 10/28/15     Vincent.T      1. Modify some tested result.
//                             2. Add 6v map.
// 11/04/15     Vincent.T      1.New input parameter of BatJudgeMap()
//                             2.New member of VOL_CH: MAP_24V
// 11/18/15     Vincent.T      Modified AGM Coe. 1.24 -> 1.25
// 08/07/20     David.Wang     Add AGM Spiral map
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/service/mapsvc.h#5 $
// $DateTime: 2017/09/22 10:27:40 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#ifndef BT2100_WW_V00_L_SERVICE_MAPSVC_H_
#define BT2100_WW_V00_L_SERVICE_MAPSVC_H_


#include <stdbool.h>
#include <stdint.h>

//*******************************************************
//Must be th same with BATTERYTYPE in Batteryperselwidget
//*******************************************************
typedef enum
{
	MAP_FLOODED = 0,		/* 0: FLOODED */
	MAP_AGM_FLAT_PLATE,		/* 1: AGM FLAT PLATE */
	MAP_AGM_SPIRAL,			/* 2: AGM SPIRAL */
	MAP_VRLA_GEL,			/* 3: VRLA / GEL */
	MAP_EFB					/* 4: EFB */
} MAP_BATTERYTYPE;

//***********
//Test result
//***********
typedef enum
{
	TR_GOODnPASS = 0,		//good & pass 0
	TR_GOODnRRCHARGE,		//good & recharge 1
	TR_BADnREPLACE,			//bad & replace 2
	TR_CAUTION,				//caution 3
	TR_RECHARGEnRETEST,		//recharge & retest 4
	TR_BAD_CELL_REPLACE,	//bad cell replace 5
	TR_ASK_CUSTOM,			//y: bad cell replace; n: recharge n retest 6
	TR_GOOD_PACK,           // 20200817 DW EDIT add pack test 7
	TR_CHECK_PACK           // 20200817 DW EDIT add pack test 8
}TEST_RESULT;

//*****************
// Volatage Channel
//*****************
typedef enum
{
	MAP_6V = 0,		//6V channel
	MAP_12V,		//12V channel
	MAP_24V			//24V channel
}VOL_CH;

typedef enum
{
	RC_GOOD = 0,
	RC_LOW,
	RC_POOR,
}RC_RESULT;

//*********************
//different cca ration
//********************
#define MAP_FLOODED_CCA 	1
#define MAP_AGMFnS_CCA 		1.25
#define MAP_VRLAnGEL_CCA	1
#define MAP_EFB_CCA			1.25 //1.12

//***************
//MAP COORDINATES
//***************
typedef struct
{
	float x;	//voltage
	float y;	//state of health
}MapCoordinates;

TEST_RESULT BatJudgeMap(MAP_BATTERYTYPE Battype, float fvol, float *pfcca, float *pfsoh, VOL_CH volch);
TEST_RESULT BatJudgeMap_FOOLDED(float fvol, float *pfsoh);
TEST_RESULT BatJudgeMap_AGMFnS(float fvol, float *pfsoh);
TEST_RESULT BatJudgeMap_VRLAnGEL(float fvol, float *pfsoh);
TEST_RESULT BatJudgeMap_EFB(float fvol, float *pfsoh);
TEST_RESULT BatJudgeMap_AGMSpiral(float fvol, float *pfsoh);                                              //20200807 DW EDIT

#endif /* BT2100_WW_V00_L_SERVICE_MAPSVC_H_ */
