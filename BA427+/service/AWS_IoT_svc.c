//*****************************************************************************
//
// Source File: AWS_IoT_svc.c
// Description: service for communication with AWS IoT.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 10/02/18     Jerry.W        Created file.
// 11/19/18     Henry.Y        Add MFC upload url. Adjust AWS_IoT_Upload_test_record() rating.
// 12/07/18     Henry.Y        Correct typo of parse_result_upload() "seqId"
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/driver/wifi_driver.h#2 $
// $DateTime: 2017/12/18 16:17:54 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#include "AWS_IoT_svc.h"
#include "flashsvc.h"
#include "extrtcsvc.h"
#include "cloud_config.h"

#if AWS_IoT_DEBUG
#define 	AWSIoT_DEBUG(bug_info)		UARTprintf("AWS IoT:%s, @%d\n", bug_info, __LINE__)
#else
#define 	AWSIoT_DEBUG(bug_info)
#endif

extern uint8_t *strFWver;

bool AWS_IoT_shadow_update();

typedef enum{
	IoT_pro_idle,
	IoT_pro_wait_request_OK,
	IoT_pro_wait_shadow,	//shadow unchanged yet, wait to resent.
}PROTOCOL_STATE;


PROTOCOL_STATE WAIT_FW_VER_sta = IoT_pro_idle;
PROTOCOL_STATE WAIT_TEST_RECORD_sta = IoT_pro_idle;
PROTOCOL_STATE WAIT_OTA_sta = IoT_pro_idle;
PROTOCOL_STATE WAIT_REGIST_sta = IoT_pro_idle;

bool WAIT_SHADOW_HTTP_response = false;

uint8_t shadow_count = 0;

Timer_t* RESEND_TIMER = NULL;

const uint8_t AWS_IoT[] = "a3qsxrkd34zubn.iot.us-east-1.amazonaws.com";
const uint8_t NORMAL_UPLOAD_URL[] = URL_UPLOAD_NORMAL;
const uint8_t MFC_UPLOAD_URL[] = "/topics/ba427plus/uploadRecordSmartlab?qos=1";

const char success_s[] = "success";

static UPLOAD_MODE_t upload_mode;
static uint8_t UPLOAD_BAT_SYS;  //1 = battery test, 2 = system test

void (*upload_test_rslt_cb)(bool , uint8_t);
void(*FW_check_rslt)(uint8_t FW_result, OTA_FW_t *FW, uint8_t FLS_result, OTA_FW_t *FLS);
void(*ota_rslt)(bool result);
void(*DVC_register_rslt)(bool result, uint8_t reason);

void RESEND_TIMER_handler(){
	//this function only raise flags.
	//do not process too many thing, cause it's ran in interrupt.

	if(WAIT_FW_VER_sta == IoT_pro_wait_shadow
			|| WAIT_TEST_RECORD_sta == IoT_pro_wait_shadow
			|| WAIT_OTA_sta == IoT_pro_wait_shadow
			|| WAIT_REGIST_sta == IoT_pro_wait_shadow){
		shadow_count++;
	}
	else{
		shadow_count = 0;
	}

}

void dump_request(){
	if(WAIT_FW_VER_sta == IoT_pro_wait_shadow){
		WAIT_FW_VER_sta = IoT_pro_idle;
		if(FW_check_rslt) FW_check_rslt(CK_OTA_demand_internet_fail, NULL, CK_OTA_demand_internet_fail, NULL);
	}
	if(WAIT_TEST_RECORD_sta == IoT_pro_wait_shadow){
		WAIT_TEST_RECORD_sta = IoT_pro_idle;
		if(upload_test_rslt_cb) upload_test_rslt_cb(false, Fail_reason_network_err);
	}
	if(WAIT_OTA_sta == IoT_pro_wait_shadow){
		WAIT_OTA_sta = IoT_pro_idle;
		if(ota_rslt) ota_rslt(false);
	}
	if(WAIT_REGIST_sta == IoT_pro_wait_shadow){
		WAIT_REGIST_sta = IoT_pro_idle;
		if(DVC_register_rslt) DVC_register_rslt(false, register_fail_network_err);
	}
}

void AWS_IoT_Upload_test_record_response_handler(uint16_t status, uint16_t len,uint8_t *data){
	if(status == 200){
		if(WAIT_TEST_RECORD_sta == IoT_pro_wait_request_OK){
			WAIT_TEST_RECORD_sta = IoT_pro_wait_shadow;
			AWSIoT_DEBUG("send upload request OK");
			if(!WAIT_SHADOW_HTTP_response)	AWS_IoT_shadow_update();
		}
		else{
			AWSIoT_DEBUG("rslt status error");
		}
	}
	else if(WAIT_TEST_RECORD_sta != IoT_pro_idle){
		WAIT_TEST_RECORD_sta = IoT_pro_idle;
		if(upload_test_rslt_cb) upload_test_rslt_cb(false, Fail_reason_response_err);
	}
}

bool AWS_IoT_Upload_test_record(TEST_RECORD_T *rec, void(*rslt)(bool , uint8_t), UPLOAD_MODE_t mode){
	SYS_PAR* par = Get_system_parameter();
	SYS_INFO* info = Get_system_info();
	json_set_alloc_funcs(prtect_malloc, protect_free);

	if(WAIT_TEST_RECORD_sta != IoT_pro_idle) return false;

	upload_mode = mode;

	if((rec->Test_Type == Start_Stop_Test)||(rec->Test_Type == Battery_Test)){
		BATTERY_SARTSTOP_RECORD_T* record = (BATTERY_SARTSTOP_RECORD_T*) rec;
		uint8_t test_type = (rec->Test_Type == Start_Stop_Test)? 2 : 1;
		uint8_t judgement = 0;
		uint8_t batt_type = 0;
		uint8_t rating = 0;
		char test_time[40];
		char SNP[SNP_LEN];
		char SNC[SNC_LEN];
		uint8_t i;

		memset(SNP, 0, SNP_LEN);
		memset(SNC, 0, SNC_LEN);

		for(i = 0 ; (info->SNC[i] != '@') && (i < SNC_LEN) ; i++){
			SNC[i] = info->SNC[i];
		}

		for(i = 0 ; (info->SNP[i] != '@') && (i < SNP_LEN) ; i++){
			SNP[i] = info->SNP[i];
		}

		switch(record->Judgement){
		case TR_GOODnPASS : 		judgement = 0; break;
		case TR_BADnREPLACE : 		judgement = 2; break;
		case TR_GOODnRRCHARGE : 	judgement = 1; break;
		case TR_RECHARGEnRETEST : 	judgement = 4; break;
		case TR_BAD_CELL_REPLACE : 	judgement = 5; break;
		case TR_CAUTION : 			judgement = 6; break;
		default: return false;
		}

		switch(record->Battery_Type){
		case FLOODED : 			batt_type = 0; break;
		case AGM_FLAT_PLATE : 	batt_type = (rec->Test_Type == Start_Stop_Test)? 4:1; break;
		case AGM_SPIRAL : 		batt_type = 2; break;
		case VRLA_GEL : 		batt_type = 3; break;
		case EFB : 				batt_type = 5; break;
		default: return false;
		}

		switch(record->Ratting){
		case JIS :      rating = 4;   break;
		case SAE : 		rating = 0; 	break;
		case CA_MCA : 	rating = 6; 	break;
		case EN2 : 		rating = 2; 	break;
		case IEC : 		rating = 3; 	break;
		case DIN : 		rating = 5; 	break;
		case EN1:       rating = 2; 	break;
        case MCA:       rating = 7; 	break;
        default: return false;
		}

		if(!TIME_TO_STRING(&record->Test_Time, test_time)) return false;
		sprintf(test_time , "%4d-%02d-%02dT%02d:%02d:%02d%+03d:%02d",
				record->Test_Time.year, record->Test_Time.month,
				record->Test_Time.day, record->Test_Time.hour,
				record->Test_Time.minute, record->Test_Time.second,
				info->Time_zone/60,
				(info->Time_zone> 0)? info->Time_zone%60 : (info->Time_zone*(-1))%60 );

		json_t* root = json_object();

		par->upload_rslt_seqID++;
		save_system_parameter(&par->upload_rslt_seqID, sizeof(par->upload_rslt_seqID));

		json_object_set_new(root,"seqId",json_integer(par->upload_rslt_seqID));
		json_object_set_new(root,"csn",json_string(SNC));
		json_object_set_new(root,"psn",json_string(SNP));

		json_object_set_new(root,"testType",json_integer(test_type));
		json_object_set_new(root,"judgement",json_integer(judgement));
		json_object_set_new(root,"batteryType",json_integer(batt_type));
		json_object_set_new(root,"voltage",json_real(record->Voltage));
		json_object_set_new(root,"rating",json_integer(rating));
		json_object_set_new(root,"capacity",json_integer(record->CCA_Capacity));
		json_object_set_new(root,"measuredCca",json_integer(record->CCA_Measured));
		json_object_set_new(root,"ir",json_real(record->IR));
		json_object_set_new(root,"temperature",json_real(record->Temperature));
		json_object_set_new(root,"soh",json_integer(record->SOH));
		json_object_set_new(root,"soc",json_integer(record->SOC));
		json_object_set_new(root,"testCode",json_string((char*)record->Test_code));
		json_object_set_new(root,"vin",json_string((char*)record->VIN));
		json_object_set_new(root,"roNum",json_string((char*)record->RO));
		json_object_set_new(root,"recipient",json_string((char*)record->Email));
		json_object_set_new(root,"testTime",json_string(test_time));

		json_object_set_new(root,"batCharged",json_integer((record->IsBatteryCharged != 0)?1:2));
		json_object_set_new(root,"batInVehicle",json_integer((record->TestInVecicle != 0)?1:2));

		json_object_set_new(root,"fwVersion",json_string((char*)strFWver));

		upload_test_rslt_cb = rslt;
		UPLOAD_BAT_SYS = 1;
		uint8_t* URL = (mode == normal_upload)? (uint8_t*)NORMAL_UPLOAD_URL : (uint8_t*)MFC_UPLOAD_URL;

		if(!HTTP_request(POST, (uint8_t*)AWS_IoT, 8443, URL, Jason_header,
						(uint8_t*)json_dumps(root, JSON_INDENT(1) | JSON_REAL_PRECISION(4)), true, true,
						AWS_IoT_Upload_test_record_response_handler)){

			if(upload_test_rslt_cb) upload_test_rslt_cb(false, Fail_reason_network_err);
			json_decref(root);
			return false;
		}
		else{
			AWSIoT_DEBUG("send upload request");
			WAIT_TEST_RECORD_sta = IoT_pro_wait_request_OK;
		}
		json_decref(root);
	}
	else if(rec->Test_Type == System_Test){
	    SYSTEM_TEST_RECORD_T* record = (SYSTEM_TEST_RECORD_T*) rec;
	    uint8_t test_type = 3;
	    uint8_t crank_result = 0;
	    uint8_t idle_result = 0;
	    uint8_t loading_result = 0;
	    uint8_t ripple_result = 0;
	    char test_time[40];
	    char SNP[SNP_LEN];
	    char SNC[SNC_LEN];
	    uint8_t i;

	    memset(SNP, 0, SNP_LEN);
	    memset(SNC, 0, SNC_LEN);

	    for(i = 0 ; (info->SNC[i] != '@') && (i < SNC_LEN) && (info->SNC[i] != '\0') ; i++){
	        SNC[i] = info->SNC[i];
	    }

	    for(i = 0 ; (info->SNP[i] != '@') && (i < SNP_LEN) && (info->SNP[i] != '\0') ; i++){
	        SNP[i] = info->SNP[i];
	    }

	    switch(record->Crank_result){
	    case NO_DETECT :            crank_result = 4; break;
	    case LOW :                  crank_result = 5; break;
	    case NORMAL :               crank_result = 6; break;
	    case HIGH :                 crank_result = 7; break;
	    default :                   return false;
	    }

	    switch(record->Idle_result){
	    case NO_DETECT :            idle_result = 4; break;
	    case LOW :                  idle_result = 5; break;
	    case NORMAL :               idle_result = 6; break;
	    case HIGH :                 idle_result = 7; break;
	    default :                   return false;
	    }

	    switch(record->Loaded_result){
	    case NO_DETECT :            loading_result = 4; break;
	    case LOW :                  loading_result = 5; break;
	    case NORMAL :               loading_result = 6; break;
	    case HIGH :                 loading_result = 7; break;
	    default :                   return false;
	    }

	    switch(record->Ripple_result){
	    case NO_DETECT :            ripple_result = 4; break;
	    case LOW :                  ripple_result = 5; break;
	    case NORMAL :               ripple_result = 6; break;
	    case HIGH :                 ripple_result = 7; break;
	    default :                   return false;
	    }


	    if(!TIME_TO_STRING(&record->Test_Time, test_time)) return false;
	    sprintf(test_time , "%4d-%02d-%02dT%02d:%02d:%02d%+03d:%02d",
	            record->Test_Time.year, record->Test_Time.month,
	            record->Test_Time.day, record->Test_Time.hour,
	            record->Test_Time.minute, record->Test_Time.second,
	            info->Time_zone/60,
	            (info->Time_zone> 0)? info->Time_zone%60 : (info->Time_zone*(-1))%60 );

	    json_t* root = json_object();

	    par->upload_rslt_seqID++;
	    save_system_parameter(&par->upload_rslt_seqID, sizeof(par->upload_rslt_seqID));

	    json_object_set_new(root,"seqId",json_integer(par->upload_rslt_seqID));
	    json_object_set_new(root,"csn",json_string(SNC));
	    json_object_set_new(root,"psn",json_string(SNP));

	    json_object_set_new(root,"testType",json_integer(test_type));
	    json_object_set_new(root,"crankingResult",json_integer(crank_result));
	    json_object_set_new(root,"crankingVoltage",json_real(record->Crank_voltage));
	    json_object_set_new(root,"altIdleResult",json_integer(idle_result));
	    json_object_set_new(root,"altIdleVoltage",json_real(record->Idle_voltage));
	    json_object_set_new(root,"rippleResult",json_integer(ripple_result));
	    json_object_set_new(root,"rippleVoltage",json_real(record->Ripple_voltage));
	    json_object_set_new(root,"altLoadResult",json_integer(loading_result));
	    json_object_set_new(root,"altLoadVoltage",json_real(record->Loaded_voltage));

	    json_object_set_new(root,"vin",json_string((char*)record->VIN));
	    json_object_set_new(root,"roNum",json_string((char*)record->RO));
	    json_object_set_new(root,"recipient",json_string((char*)record->Email));
	    json_object_set_new(root,"testTime",json_string(test_time));
	    json_object_set_new(root,"fwVersion",json_string((char*)strFWver));

	    upload_test_rslt_cb = rslt;
	    UPLOAD_BAT_SYS = 2;

	    if(!HTTP_request(POST, (uint8_t*)AWS_IoT, 8443, URL_SYSTEM_RECORD, (uint8_t*)Jason_header,
	            (uint8_t*)json_dumps(root, JSON_INDENT(1) | JSON_REAL_PRECISION(4)), true, true,
	            AWS_IoT_Upload_test_record_response_handler)){

	        if(upload_test_rslt_cb) upload_test_rslt_cb(false, Fail_reason_network_err);
	        json_decref(root);
	        return false;
	    }
	    else{
	        AWSIoT_DEBUG("send upload request");
	        WAIT_TEST_RECORD_sta = IoT_pro_wait_request_OK;
	    }
	    json_decref(root);
	}
	else{
		return false;
	}

	return true;
}


void AWS_IoT_FW_check_response_handler(uint16_t status, uint16_t len,uint8_t *data){
	if(status == 200){
		if(WAIT_FW_VER_sta == IoT_pro_wait_request_OK){
			WAIT_FW_VER_sta = IoT_pro_wait_shadow;
			AWSIoT_DEBUG("send FW CK request OK");
			AWSIoT_DEBUG(data);
			if(!WAIT_SHADOW_HTTP_response)	AWS_IoT_shadow_update();
		}
		else{
			AWSIoT_DEBUG("FW CK status error");
		}
	}
	else if(WAIT_FW_VER_sta != IoT_pro_idle){
		WAIT_FW_VER_sta = IoT_pro_idle;
		if(FW_check_rslt) FW_check_rslt(CK_OTA_demand_internet_fail, NULL, CK_OTA_demand_internet_fail, NULL);
	}
}

bool AWS_IoT_FW_check(uint8_t *version, void(*rslt)(uint8_t FW_result, OTA_FW_t *FW, uint8_t FLS_result, OTA_FW_t *FLS)){
	SYS_PAR* par = Get_system_parameter();
	SYS_INFO* info = Get_system_info();
	FlashHeader_t*FH = ExtFls_get_header();
	json_set_alloc_funcs(prtect_malloc, protect_free);

	if(WAIT_FW_VER_sta != IoT_pro_idle) return false;

	FW_check_rslt = rslt;

	char SNC[SNC_LEN];
	uint8_t i;

	memset(SNC, 0, SNC_LEN);

	for(i = 0 ; (info->SNC[i] != '@') && (i < SNC_LEN) ; i++){
		SNC[i] = info->SNC[i];
	}

	json_t* root = json_object();

	par->FW_check_seqID++;
	save_system_parameter(&par->FW_check_seqID, sizeof(par->FW_check_seqID));
	json_object_set_new(root,"seqId",json_integer(par->FW_check_seqID));
	json_object_set_new(root,"csn",json_string(SNC));
	json_object_set_new(root,"fwVersion",json_string((char*)version));
	json_object_set_new(root,"flsVersion",json_string((char*)FH->version));

	if(!HTTP_request(POST, (uint8_t*)AWS_IoT, 8443, URL_CHECK_VERSION, Jason_header,
			(uint8_t*)json_dumps(root, JSON_INDENT(1)), true, true,
			AWS_IoT_FW_check_response_handler)){
		if(FW_check_rslt) FW_check_rslt(CK_OTA_demand_internet_fail, NULL, CK_OTA_demand_internet_fail, NULL);
		json_decref(root);
		return false;
	}
	else{
		AWSIoT_DEBUG("send FW CK request");
		WAIT_FW_VER_sta = IoT_pro_wait_request_OK;
	}
	json_decref(root);
	return true;

}


void AWS_IoT_send_Upgrade_result_response_handler(uint16_t status, uint16_t len,uint8_t *data){
	if(status == 200){
		if(WAIT_OTA_sta == IoT_pro_wait_request_OK){
			WAIT_OTA_sta = IoT_pro_wait_shadow;
			AWSIoT_DEBUG("send ota rslt request OK");
			if(!WAIT_SHADOW_HTTP_response)	AWS_IoT_shadow_update();
			AWSIoT_DEBUG(data);
		}
		else{
			AWSIoT_DEBUG("ota rslt status error");
		}
	}
	else if(WAIT_OTA_sta != IoT_pro_idle){
		WAIT_OTA_sta = IoT_pro_idle;
		if(ota_rslt) ota_rslt(false);
	}
}

bool AWS_IoT_send_Upgrade_result(uint8_t *OLD_version, uint8_t *NEW_version, bool success, void(*rslt)(bool result)){
	SYS_PAR* par = Get_system_parameter();
	SYS_INFO* info = Get_system_info();
	json_set_alloc_funcs(prtect_malloc, protect_free);

	if(WAIT_OTA_sta != IoT_pro_idle) return false;

	ota_rslt = rslt;

	char SNP[SNP_LEN];
	char SNC[SNC_LEN];
	uint8_t i;
	char test_time[40];

	memset(SNP, 0, SNP_LEN);
	memset(SNC, 0, SNC_LEN);

	for(i = 0 ; (info->SNC[i] != '@') && (i < SNC_LEN) ; i++){
		SNC[i] = info->SNC[i];
	}

	for(i = 0 ; (info->SNP[i] != '@') && (i < SNP_LEN) ; i++){
		SNP[i] = info->SNP[i];
	}

	uint8_t date;
	TIME_T Test_Time;
	ExtRTCRead( &Test_Time.year, &Test_Time.month, &date, &Test_Time.day, &Test_Time.hour, &Test_Time.minute, &Test_Time.second);
	if(!TIME_TO_STRING(&Test_Time, test_time)) return false;
	sprintf(test_time , "%4d-%02d-%02dT%02d:%02d:%02d%+03d:%02d",
			Test_Time.year, Test_Time.month,
			Test_Time.day, Test_Time.hour,
			Test_Time.minute, Test_Time.second,
			info->Time_zone/60,
			(info->Time_zone> 0)? info->Time_zone%60 : (info->Time_zone*(-1))%60 );

	json_t* root = json_object();

	par->OTA_rslt_seqID++;
	save_system_parameter(&par->OTA_rslt_seqID, sizeof(par->OTA_rslt_seqID));
	json_object_set_new(root,"seqId",json_integer(par->OTA_rslt_seqID));
	json_object_set_new(root,"csn",json_string(SNC));
	json_object_set_new(root,"psn",json_string(SNP));
	json_object_set_new(root,"time",json_string(test_time));
	json_object_set_new(root,"result",json_integer((success)? 1:0 ));
	json_object_set_new(root,"type",json_integer(1));
	json_object_set_new(root,"before",json_string((char*)OLD_version));
	json_object_set_new(root,"after",json_string((char*)NEW_version));

	if(!HTTP_request(POST, (uint8_t*)AWS_IoT, 8443, URL_OTA_RESULT, Jason_header,
			(uint8_t*)json_dumps(root, JSON_INDENT(1)), true, true,
			AWS_IoT_send_Upgrade_result_response_handler)){
		if(ota_rslt) ota_rslt(false);
		json_decref(root);
		return false;
	}
	else{
		AWSIoT_DEBUG("send ota rslt request");
		WAIT_OTA_sta = IoT_pro_wait_request_OK;
	}

	json_decref(root);
	return true;
}

void parse_result_upload(json_t *uploadRecord){
	json_t *temp;
	SYS_PAR* par = Get_system_parameter();
	static uint8_t status_count = 0;

	temp = json_object_get(uploadRecord, "seqId");
	if(json_is_integer(temp)){
		int32_t seq = json_integer_value(temp);
		if(seq == par->upload_rslt_seqID){
			temp = json_object_get(uploadRecord, "status");
			if(json_is_string(temp)){
				WAIT_TEST_RECORD_sta = IoT_pro_idle;
				status_count = 0;
				if(memcmp(json_string_value(temp),success_s, 7 ) == 0){
					if(upload_test_rslt_cb) upload_test_rslt_cb(true, Fail_reason_NULL);
				}
				else{
					temp = json_object_get(uploadRecord, "failReason");
					if(upload_test_rslt_cb) upload_test_rslt_cb(false, (uint8_t)json_integer_value(temp));
				}
			}
		}
	}

	if(status_count == 5){
		WAIT_TEST_RECORD_sta = IoT_pro_idle;
		status_count = 0;
		if(upload_test_rslt_cb) upload_test_rslt_cb(false, Fail_reason_response_err);
	}

}


static const char *item[3] = {"newVersion","url","note"};
static const char *Ver_s[2] = {"fw","fls"};

void parse_FW_CK(json_t *Version){
	json_t *temp, *Ver[2];
	SYS_PAR* par = Get_system_parameter();
	uint8_t result[2], i, j, len_temp, **des_temp;
	OTA_FW_t *New_VER[2] = {NULL,NULL};
	static uint8_t status_count = 0;
	temp = json_object_get(Version, "seqId");
	if(json_is_integer(temp)){
		status_count++;
		int32_t seq = json_integer_value(temp);
		if(seq == par->FW_check_seqID){
			temp = json_object_get(Version, "status");
			if(json_is_string(temp)){
				WAIT_FW_VER_sta = IoT_pro_idle;
				status_count = 0;
				if(memcmp(json_string_value(temp),success_s, 7 ) == 0){
					for(i = 0; i < (sizeof(Ver_s)/4) ; i++){
						Ver[i] = json_object_get(Version, Ver_s[i]);
						if(json_is_object(Ver[i])){
							temp = json_object_get(Ver[i], "update");
					if(!json_is_string(temp)) return;
					if(memcmp(json_string_value(temp),"yes", 3 ) == 0){
								result[i] = CK_OTA_demand_yes;
					}
					else if(memcmp(json_string_value(temp),"no", 2 ) == 0){
								result[i] = CK_OTA_demand_no;
								continue;
					}
					else if(memcmp(json_string_value(temp),"must", 4 ) == 0){
								result[i] = CK_OTA_demand_must;
					}
					else{
								goto FAIL;
					}

							New_VER[i] = malloc(sizeof(OTA_FW_t));
							memset(New_VER[i], 0, sizeof(OTA_FW_t));

							for(j = 0; j < 3; j++){
								temp = json_object_get(Ver[i], item[j]);
						if(!json_is_string(temp)){
							goto FAIL;
						}
						len_temp = json_string_length(temp)+1;

								des_temp = (j == 0)? &New_VER[i]->version :
										(j == 1)? &New_VER[i]->URL : &New_VER[i]->Note;
						*des_temp = malloc(len_temp);
						memcpy(*des_temp, json_string_value(temp), len_temp);
					}


							temp = json_object_get(Ver[i], "crc");
					if(!json_is_integer(temp)){
						goto FAIL;
					}
							New_VER[i]->CRC = json_integer_value(temp);


							temp = json_object_get(Ver[i], "length");
					if(!json_is_integer(temp)){
						goto FAIL;
					}
							New_VER[i]->FW_len = json_integer_value(temp);
						}
					}

					if(FW_check_rslt) FW_check_rslt(result[0], New_VER[0],result[1], New_VER[1]);
				}
				else{
					if(FW_check_rslt) FW_check_rslt(CK_OTA_demand_internet_fail, NULL, CK_OTA_demand_internet_fail, NULL);
				}
			}
		}
	}

	if(status_count == 5){
		WAIT_FW_VER_sta = IoT_pro_idle;
		status_count = 0;
		if(FW_check_rslt) FW_check_rslt(CK_OTA_demand_internet_fail, NULL, CK_OTA_demand_internet_fail, NULL);
	}

	return;

FAIL:
	if(FW_check_rslt) FW_check_rslt(CK_OTA_demand_internet_fail, NULL, CK_OTA_demand_internet_fail, NULL);
	for(i = 0; i < (sizeof(Ver_s)/4) ; i++){
		if(New_VER[i]){
			if(New_VER[i]->version) free(New_VER[i]->version);
			if(New_VER[i]->URL) free(New_VER[i]->URL);
			if(New_VER[i]->Note) free(New_VER[i]->Note);
			free(New_VER[i]);
		}
	}

}

void parse_regist(json_t *Regist){
	json_t *temp,*reason;
	SYS_PAR* par = Get_system_parameter();
	static uint8_t status_count = 0;
	temp = json_object_get(Regist, "seqId");
	if(json_is_integer(temp)){
		status_count++;
		int32_t seq = json_integer_value(temp);
		if(seq == par->register_rslt_seqID){
			temp = json_object_get(Regist, "status");
			if(json_is_string(temp)){
				WAIT_REGIST_sta = IoT_pro_idle;
				status_count = 0;
				if(memcmp(json_string_value(temp),success_s, 7 ) == 0){

					if(DVC_register_rslt) DVC_register_rslt(true, register_success);
				}
				else{
					reason = json_object_get(Regist, "failReason");
					if(json_is_integer(reason)){
						if(DVC_register_rslt) DVC_register_rslt(false, (uint8_t)json_integer_value(reason));
					}
					else{
						if(DVC_register_rslt) DVC_register_rslt(false, register_fail_response_err );
					}
				}
			}
		}
	}

	if(status_count == 5){
		WAIT_REGIST_sta = IoT_pro_idle;
		status_count = 0;
		if(DVC_register_rslt) DVC_register_rslt(false, register_fail_response_err);
	}
}


void parse_ota(json_t *ota){
	json_t *temp;
	SYS_PAR* par = Get_system_parameter();
	static uint8_t status_count = 0;
	temp = json_object_get(ota, "seqId");
	if(json_is_integer(temp)){
		status_count++;
		int32_t seq = json_integer_value(temp);
		if(seq == par->OTA_rslt_seqID){
			temp = json_object_get(ota, "status");
			if(json_is_string(temp)){
				WAIT_OTA_sta = IoT_pro_idle;
				status_count = 0;
				if(memcmp(json_string_value(temp),success_s, 7 ) == 0){
					if(ota_rslt) ota_rslt(true);
				}
				else{
					if(ota_rslt) ota_rslt(false);
				}
			}
		}
	}

	if(status_count == 5){
		WAIT_OTA_sta = IoT_pro_idle;
		status_count = 0;
		if(ota_rslt) ota_rslt(false);
	}

}

void AWS_IoT_shadow_response_handler(uint16_t status, uint16_t len,uint8_t *data){
	json_t *root = NULL, *temp, *Version, *rslt, *ota, *desired, *regist;
	json_error_t error;
	static uint8_t error_count = 0;

	WAIT_SHADOW_HTTP_response = false;

	UARTprintf("%s\n",(char*)data);

	if(status == 200){
		error_count = 0;
		json_set_alloc_funcs(prtect_malloc, protect_free);
		root = json_loads((char*)data, 0, &error);
		if(!json_is_object(root)){
			goto FAIL;
		}
		else{
			temp = json_object_get(root, "state");
			if(!json_is_object(temp)){
				goto FAIL;
			}

			desired = json_object_get(temp, "desired");
			if(!json_is_object(desired)){
				goto FAIL;
			}

			if(WAIT_FW_VER_sta == IoT_pro_wait_shadow){
				Version = json_object_get(desired, "checkVersion");
				if(json_is_object(Version)){
					parse_FW_CK(Version);
				}
				else{
					AWSIoT_DEBUG(data);
				}
			}

			if(WAIT_TEST_RECORD_sta == IoT_pro_wait_shadow){

			    if(upload_mode == normal_upload){
			        if(UPLOAD_BAT_SYS == 1){
			            rslt = json_object_get(desired, "uploadRecordBT");
			        }
			        else{
			            rslt = json_object_get(desired, "uploadRecordST");
			        }
			    }
			    else{
			        rslt = json_object_get(desired, "uploadRecordSmartlab");
			    }
			    if(json_is_object(rslt)){
			        parse_result_upload(rslt);
			    }
			    else{
			        AWSIoT_DEBUG(data);
			    }
			}

			if(WAIT_OTA_sta == IoT_pro_wait_shadow){
				ota = json_object_get(desired, "otaResult");
				if(json_is_object(ota)){
					parse_ota(ota);
				}
				else{
					AWSIoT_DEBUG(data);
				}
			}

			if(WAIT_REGIST_sta == IoT_pro_wait_shadow){
				regist = json_object_get(desired, "registerDevice");
				if(json_is_object(regist)){
					parse_regist(regist);
				}
				else{
					AWSIoT_DEBUG(data);
				}
			}

		}
		if(root) json_decref(root);
		return;
	}
	else{
		error_count++;
		if(error_count > 3){
			error_count = 0;
			dump_request();
		}
	}

FAIL :
	if(root) json_decref(root);
	AWSIoT_DEBUG("get shadow fail.");
}

bool AWS_IoT_shadow_update(){
	uint8_t URL[30];
	uint8_t SNC[SNC_LEN];
	uint8_t i;
	SYS_INFO* info = Get_system_info();
	memset(SNC, 0, SNC_LEN);
	for(i = 0 ; (info->SNC[i] != '@') && (i < SNC_LEN) ; i++){
		SNC[i] = info->SNC[i];
	}
	sprintf((char*)URL, "/things/%s/shadow", SNC);
	if(HTTP_request(GET, (uint8_t*)AWS_IoT, 8443, URL, Jason_header,
					NULL, true, true, AWS_IoT_shadow_response_handler) ){
		WAIT_SHADOW_HTTP_response = true;
		AWSIoT_DEBUG("try to get shadow.");
		return true;
	}
	else{
		dump_request();
		return false;
	}
}


void AWS_IoT_poll(){
	if(RESEND_TIMER == NULL){
		RESEND_TIMER = register_timer(10, RESEND_TIMER_handler);
		start_timer(RESEND_TIMER);
	}

	if(shadow_count > SHADOW_RESEND_PERIOD){
		if(!WAIT_SHADOW_HTTP_response){
			AWS_IoT_shadow_update();
			shadow_count = 0;
		}
	}

}

void AWS_IoT_DVC_register_response_handler(uint16_t status, uint16_t len,uint8_t *data){
	if(status == 200){
		if(WAIT_REGIST_sta == IoT_pro_wait_request_OK){
			WAIT_REGIST_sta = IoT_pro_wait_shadow;
			AWSIoT_DEBUG("send register request OK");
			if(!WAIT_SHADOW_HTTP_response)	AWS_IoT_shadow_update();
		}
		else{
			AWSIoT_DEBUG("register status error");
		}
	}
	else if(WAIT_REGIST_sta != IoT_pro_idle){
		WAIT_REGIST_sta = IoT_pro_idle;
		if(DVC_register_rslt) DVC_register_rslt(false, register_fail_network_err);
	}
}


bool AWS_IoT_device_register(uint8_t *data, void(*rslt)(bool result, uint8_t reason)){
	SYS_INFO* info = Get_system_info();
	SYS_PAR* par = Get_system_parameter();
	if(WAIT_REGIST_sta != IoT_pro_idle) return false;
	DVC_register_rslt = rslt;
	json_set_alloc_funcs(prtect_malloc, protect_free);
	json_t* root = json_object();

	char SNP[SNP_LEN];
	char SNC[SNC_LEN];
	uint8_t i;

	memset(SNP, 0, SNP_LEN);
	memset(SNC, 0, SNC_LEN);

	for(i = 0 ; (info->SNC[i] != '@') && (i < SNC_LEN) ; i++){
		SNC[i] = info->SNC[i];
	}

	for(i = 0 ; (info->SNP[i] != '@') && (i < SNP_LEN) ; i++){
		SNP[i] = info->SNP[i];
	}


	par->register_rslt_seqID++;
	save_system_parameter(&par->register_rslt_seqID, sizeof(par->register_rslt_seqID));
	json_object_set_new(root,"seqId",json_integer(par->register_rslt_seqID));
	json_object_set_new(root,"csn",json_string(SNC));
	json_object_set_new(root,"psn",json_string(SNP));
	json_object_set_new(root,"data",json_string((char*)data));

	if(!HTTP_request(POST, (uint8_t*)AWS_IoT, 8443, URL_DEVICE_REG, Jason_header,
			(uint8_t*)json_dumps(root, JSON_INDENT(1)), true, true,
			AWS_IoT_DVC_register_response_handler)){

		if(DVC_register_rslt) DVC_register_rslt(false, register_fail_network_err);
		json_decref(root);
		return false;
	}
	else{
		AWSIoT_DEBUG("send register request");
		WAIT_REGIST_sta = IoT_pro_wait_request_OK;
	}
	json_decref(root);
	return true;
}
