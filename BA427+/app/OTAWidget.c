//*****************************************************************************
//
// Source File: OTAWidget.c
// Description: OTA upgrade functions.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 05/28/18     Jerry.W        Created file.
// 10/23/18     Henry.Y        Jump_to_booloader_OTA() setup bootloader backlight before firmware upgrading.
// 10/25/18     Henry.Y        Add warning message before OTA downloading.
// 11/19/18     Henry.Y        #if AWS_IOT_SERVICE: add oler version information.
// 12/07/18     Henry.Y        New display api.
// 12/17/18     Jerry.W        Modify for OTA service.
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/app/OTAwidget.c#7 $
// $DateTime: 2017/09/22 11:15:59 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#include "Widgetmgr.h"
#include "../service/OTA_svc.h"

void OTA_event_cb(OTA_EVENT_T event);
uint8_t OTA_count_percent();

void draw_download_box(uint8_t box);

void OTA_display();
void OTA_SELECT();
void OTA_ROUTINE();
void OTA_LEFT();
void OTA_UP_DOWN();
void OTA_INIT();


typedef enum{
	OTA_idle,	//not in this widget
	OTA_selecting,
	OTA_warning,
	OTA_check_version,
	OTA_downloading,
	OTA_downloading_cancel_asking,
}OTA_WIDGET_Status;

OTA_WIDGET_Status OTA_Widget_s = OTA_idle;

uint32_t download_idx = 0;
uint32_t rcv_idx = 0;
uint8_t cancel_counter;
Timer_t* Cancel_timer = NULL;
static bool display_refresh = false; 		//in case status changed while LCD is displaying "message"

widget* OTA_widget_sub_W[] = {
};

widget OTA_widget = {
		.Key_UP = OTA_UP_DOWN,
		.Key_DOWN = OTA_UP_DOWN,
		.Key_RIGHT = 0,
		.Key_LEFT = OTA_LEFT,
		.Key_SELECT = OTA_SELECT,
		.routine = OTA_ROUTINE,
		.service = 0,
		.initial = OTA_INIT,
		.display = OTA_display,
		.subwidget = OTA_widget_sub_W,
		.number_of_widget = sizeof(OTA_widget_sub_W)/4,
		.index = 0,
		.name = "OTA",
//index for cancel selection
//parameter for upgrade selection
};




void OTA_display(){
	tRectangle sRect;
	uint8_t i;
	switch(OTA_Widget_s){
	case OTA_selecting:
		LCD_display(OTA_UPGRADE, OTA_widget.parameter^0x01);
		break;
	case OTA_warning:		
		LCD_display(WARNING_CLAMPS_ON,0);
		break;
	case OTA_check_version:
		LCD_display(OTA_CHECK_VERSION,0);
		break;
	case OTA_downloading:{
		uint8_t percentage = OTA_count_percent();
		LCD_display(OTA_DOWNLOADING,0);
		sRect.i16XMin = 23;
		sRect.i16YMin = 29;
		sRect.i16XMax = 104;
		sRect.i16YMax = 42;
		GrContextForegroundSet(&g_sContext, 0xFFFF);
		GrRectDraw(&g_sContext, &sRect);
		for(i = 0; i < (percentage/10) ; i++){
			draw_download_box(i);
		}


		/*
		// draw percentage
		if(download_box < 100){ 			
			char buf[5] = {0};
			snprintf(buf, sizeof(buf), "%d%%",download_box);
			ST7565DrawStringx16(107, 33, (uint8_t*)buf, Font_System5x8);
		}
		 */
		break;
	}
	case OTA_downloading_cancel_asking:
		LCD_display(OTA_UPGRADE_CANCEL,OTA_widget.index^0x01);
		break;
	}
}

void OTA_SELECT(){
	OTA_Progress_t* OTA_P = OTA_service_get_progress();
	switch(OTA_Widget_s){
	case OTA_selecting:
		if(OTA_widget.parameter){
			OTA_Widget_s = OTA_warning;
			OTA_display();
		}
		else{
			Return_pre_widget();
		}
		break;
	case OTA_warning:{
		if(OTA_P->status == OTA_status_ready){
			OTA_service_upgrade_start();
		}
		else{
			OTA_Widget_s = OTA_downloading;
			OTA_display();
		}
	}
		break;
	case OTA_downloading_cancel_asking:
		if(OTA_widget.index){
			OTA_Widget_s = OTA_idle;
			Return_pre_widget();
		}
		else{
			if(OTA_P->status == OTA_status_ready){
				OTA_service_upgrade_start();
			}
			else{
				OTA_Widget_s = OTA_downloading;
				OTA_display();
			}
		}
		break;
	}
}

void OTA_ROUTINE(){
	static uint8_t percentage = 0;
	uint8_t temp;
	if(OTA_Widget_s == OTA_downloading){
		temp = OTA_count_percent();
		if((temp/10) != (percentage/10)){
			draw_download_box(percentage/10);
		}
		percentage = temp;
	}
	if(display_refresh){
		display_refresh = false;
		OTA_display();
	}
}

void OTA_LEFT(){
	switch(OTA_Widget_s){
	case OTA_selecting:
		OTA_Widget_s = OTA_idle;
		Return_pre_widget();
		break;
	case OTA_downloading:
		OTA_Widget_s = OTA_downloading_cancel_asking;
		OTA_display();
		break;
	case OTA_downloading_cancel_asking:
		OTA_Widget_s = OTA_downloading;
		OTA_display();
		break;
	}
}

void OTA_UP_DOWN(){
	switch(OTA_Widget_s){
	case OTA_selecting:
		OTA_widget.parameter ^= 1;
		break;
	case OTA_downloading:
		break;
	case OTA_downloading_cancel_asking:
		OTA_widget.index ^= 1;
		break;
	default:
		return;
	}
	OTA_display();
}

void OTA_INIT(){
	if(wifi_get_status() == WIFI_off){
		Message_by_language(Turn_on_WIFI,0,en_US);
		Return_pre_widget();
		return;
	}
	else if(wifi_get_status() == WIFI_disconnect){
		Message_by_language(Natwork_unstable,0,en_US);
		Return_pre_widget();
		return;
	}
	else{
		OTA_Progress_t* OTA_P = OTA_service_get_progress();

		switch(OTA_P->status){
		case OTA_status_ready:
		case OTA_status_preparing:
			OTA_Widget_s = OTA_selecting;
			break;

		case OTA_status_unchecked:
			OTA_Widget_s = OTA_check_version;
			OTA_service_init(OTA_event_cb);
			break;

		case OTA_status_error:
		case OTA_status_check_fail:
		case OTA_status_lastest:
			OTA_Widget_s = OTA_check_version;
			OTA_service_refresh();
			break;
		case OTA_status_checking:
			OTA_Widget_s = OTA_check_version;
			break;
		}
	}
}

void send_update_ID_rslt_handle(bool result){
	if(result){
		UARTprintf("send ID success!!\n");
		FlashHeader_t* FH = ExtFls_get_header();
		FH->update_ID_send_flag = 0;
		ExtFls_set_header(&FH->update_ID_send_flag, sizeof(FH->update_ID_send_flag));
	}
	else{
		UARTprintf("send ID fail..\n");
	}
}

void draw_download_box(uint8_t box){
	if(box >= 10) return;
	tRectangle sRect;
	sRect.i16XMin = 25 + box*8;
	sRect.i16YMin = 31;
	sRect.i16XMax = 30 + box*8;
	sRect.i16YMax = 40;
	GrContextForegroundSet(&g_sContext, 0xFFFF);
	GrRectFill(&g_sContext, &sRect);
}

void cancel_count_down(){
	unregister_timer(Cancel_timer);
	Cancel_timer = NULL;

	if(OTA_Widget_s == OTA_downloading_cancel_asking){
		OTA_service_upgrade_start();
	}
}


void OTA_event_cb(OTA_EVENT_T event){
	switch(event){
	case OTA_event_new_FW_available:
		if(OTA_Widget_s == OTA_check_version){
			OTA_Widget_s = OTA_selecting;
			display_refresh = true;
		}
		else{
			Message_by_language(Software_available, 0,en_US);
		}
		break;
	case OTA_event_ready_to_upgrade:
		if(OTA_Widget_s == OTA_downloading){
			OTA_service_upgrade_start();
		}
		else if(OTA_Widget_s == OTA_downloading_cancel_asking){
			Message_by_language(Download_complete, 3,en_US);
			//start counter
			if(Cancel_timer != NULL) unregister_timer(Cancel_timer);
			Cancel_timer = register_timer(30, cancel_count_down);
			start_timer(Cancel_timer);
		}
		break;
	case OTA_event_internet_unstable:
		if(OTA_Widget_s == OTA_check_version){
			OTA_Widget_s = OTA_idle;
			Message_by_language(Fail_to_cloud, 0,(LANGUAGE)0);
			Return_pre_widget();
		}
		else if(OTA_Widget_s != OTA_idle){
			OTA_Widget_s = OTA_idle;
			Message_by_language(Natwork_unstable, 0,(LANGUAGE)0);
			Return_pre_widget();
		}
		break;
	case OTA_event_FW_up_to_date:
		if(OTA_Widget_s == OTA_check_version){
			OTA_Widget_s = OTA_idle;
			Message_by_language(Software_up_to_date, 0,(LANGUAGE)0);
			Return_pre_widget();
		}
		break;
	}
}

uint8_t OTA_count_percent(){
	OTA_Progress_t* OTA_P = OTA_service_get_progress();
	uint32_t total, progress;
	if(OTA_P->FW_to_download != OTA_P->FW_downloaded){
		return ((OTA_P->FW_downloaded*80)/OTA_P->FW_to_download);
	}
	else{
		total = (OTA_P->LCM_upload_total*1024) + (OTA_P->Printer_upload_total*2048);
		progress = ((OTA_P->LCM_upload_total - OTA_P->LCM_to_update)*1024) + ((OTA_P->Printer_upload_total - OTA_P->Printer_to_update)*2048);
	}
	return (progress*20/total)+80;
}
