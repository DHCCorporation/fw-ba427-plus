//*****************************************************************************
//
// Source File: Scannerwidget.c
// Description: Barcode scanner widget including barcode module and user 
//              scanner operation.
//
//
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 12/27/17     Henry.Y        Create file.
// 02/14/18     Henry.Y        flow modified
// 03/22/18     William.H      Modify ChkSurfaceCharge_sub_W[] to enter to CountDown15S_widget directly if its Test-In-Vehcile.
// 03/22/18     Henry.Y        modify operation flow, hide the flow after code scanned.
// 03/23/18     Henry.Y        add re-scan mechanism if invalid code scanned, three times.
// 10/17/18     Henry.Y        Add identifier:__BO__ and cancel 6V battery test.
// 11/19/18     Henry.Y        Add USER_SCANNER_CB().
// 12/07/18     Henry.Y        New display api.
// 12/18/18     Henry.Y        Remove reading mode check.
// 12/19/18     Henry.Y        Fix bug: scanning and press enter will make MCU stuck.
// 02/24/20     Henry.Y        Scanner control modification.
// 08/06/20     David.wang     Mask scanner function
// 09/06/20                    Remask 6v bat test for BT2000_HD_BO
// =============================================================================
//
//
// $Header$
// $DateTime$
// $Author$
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

//*******************NOTE************************
//*******************NOTE************************
//*******************NOTE************************
//
// SEARCH #if: FOR ACTION TEST
// SEARCH TODO: PENDING TO DO
//
//*******************NOTE************************
//*******************NOTE************************
//*******************NOTE************************



#include "Widgetmgr.h"

//static uint8_t user_SCANNER_RECEIVED[SCANNER_RX_BUFFER_SIZE];

//static scanner_set stcSC_state = idle;    // 2020807 DW MASK NO USED
//static bool scing_poll = false;           // 2020807 DW MASK NO USED

uint8_t code_recv_len = 0;

uint8_t user_SC_alloc_Len = 0;

//static bool user_bSCANNERecompl = false;  // 2020807 DW MASK NO USED



void Scanner_initail();
void Scanner_DISPLAY();
void Scanner_UP_DOWN();
void Scanner_SELECT();
void Scanner_Routine();
void Scanner_service();
void Scanner_LEFT();
void Manual_test();

/*
void USER_SCANNER_CB(bool flush, uint8_t data){
	 uint8_t ui8buf = 0;
	 static uint8_t RX_index = 0;
	 static bool bCR = false, bNL = false;
	 static uint8_t UART0_RX_buffer[SCANNER_RX_BUFFER_SIZE] = {0};
	 
	if(flush){
		RX_index = 0;
		bCR = false;
		bNL = false;
	}
	else{
		if(RX_index != user_SC_alloc_Len){			
			ui8buf = data;//(unsigned char)ROM_UARTCharGetNonBlocking(SCANNER_UART_BASE);
			UART0_RX_buffer[RX_index] = ui8buf;
			RX_index++;
			
			if(ui8buf == 0x0D) bCR = true;
			if(ui8buf == 0x0A) bNL = true;
			if(bCR && bNL){
			
				memcpy(user_SCANNER_RECEIVED , UART0_RX_buffer , SCANNER_RX_BUFFER_SIZE);
				user_SCANNER_RECEIVED[RX_index - 2] = '\0';
				memset(UART0_RX_buffer, 0x20, SCANNER_RX_BUFFER_SIZE);
				user_bSCANNERecompl = true;
				RX_index = 0;
				bCR = false;
				bNL = false;
			}
			else if(RX_index == user_SC_alloc_Len){
				memcpy(user_SCANNER_RECEIVED , UART0_RX_buffer , SCANNER_RX_BUFFER_SIZE);
				memset(UART0_RX_buffer, 0x20, SCANNER_RX_BUFFER_SIZE);
				user_bSCANNERecompl = true;
				RX_index = 0;
				bCR = false;
				bNL = false;
			}
		}
	}
 }
*/

widget* Scanner_sub_W[] = { //Confirm: voltage high/6V battery situation
		&IsIta6VBattery_widget,    //0
		&HdFunctionAsk_widget,     //1&BatterySel_widget,
		&CheckRipple_widget,       //2
		&CheckClamps_widget,       //3
		&VOLTAGEHIGH_widget,       //4
};

widget Scanner_widget = {
		.Key_UP = Scanner_UP_DOWN,
		.Key_DOWN = Scanner_UP_DOWN,
		.Key_RIGHT = 0,
		.Key_LEFT = Scanner_LEFT,
		.Key_SELECT = Scanner_SELECT,
		.routine = Scanner_Routine,//SCANNING
		.service = Scanner_service,
		.initial = Scanner_initail,
		.display = Scanner_DISPLAY,
		.subwidget = Scanner_sub_W,
		.number_of_widget = sizeof(Scanner_sub_W)/4,
		.index = 1,
		.no_messgae = 0,
		.name = "Scanner",
};


void Scanner_initail()
{
    Manual_test();
    /*
    SYS_PAR* par = Get_system_parameter();
	set_scanner_cb(USER_SCANNER_CB);

	memset(user_SCANNER_RECEIVED, 0x20, SCANNER_RX_BUFFER_SIZE);

	if(!Scanner_widget.parameter){
		memset(par->VIN_str, 0x20, VININ_LEN1); // VIN SET IF BAR CODE SCAN VALID
	}
	else{
		if(Scanner_widget.parameter > 2){
			Scanner_widget.parameter = 0;
			memset(par->VIN_str, 0x20, VININ_LEN1); // VIN SET IF BAR CODE SCAN VALID
		}
		else{
		
			code_recv_len = strlen((const char*)par->VIN_str);// LENGTH INCLUDE END CHARACTER \n\r
			if(code_recv_len > 17) code_recv_len = 17;
		}
	}
	
	stcSC_state = idle;
	
#if defined(__BO__)
	if(par->WHICH_TEST == Start_Stop_Test || par->WHICH_TEST == Battery_Test) Message_by_language(For_accuracy, 0,en_US);
#endif
*/
}



// Scanner_widget.parameter:
// 0: bacode scan?					yes-0/no-setting
// 1: scanning...					done-2/99
// 2: valid code scanned			enter-3(wifi on && connected)/setting(else)/left-0
// 99: invalid code					left-0
// 200: illegal barcode scan mode


//cancel operation flow
// 3: upload to server to analyze?	enter-4/98/left-2
// 4: communicating...				done-5/96/97
// 5: data exchange done			enter-testing/left-2
// 96: communication issue			enter-setting/left-2
// 97: no match vin code			enter-setting/left-2
// 98: wifi on and connection fail	enter-setting/left-2
void Scanner_DISPLAY()
{	

    /*
    uint8_t l;
	uint8_t d_b[SCANNER_RX_BUFFER_SIZE];
	SYS_PAR* par = Get_system_parameter();

	switch(Scanner_widget.parameter){
	case 0:
		if(stcSC_state == idle)	LCD_display(SCANNER,Scanner_widget.index^0x01);
		break;
	case 1:
		LCD_display(SCANNER_SCANNING,scing_poll);		
		break;
	case 2:
		memset(d_b, 0x20, SCANNER_RX_BUFFER_SIZE);
		memcpy(d_b , par->VIN_str , sizeof(par->VIN_str));
		LCD_display(SCANNER_RESULT,0);
		for(l = 0; l < SCANNER_RX_BUFFER_SIZE; l++)
		{
			ST7565Draw6x8Char(5 + l*6, 32, d_b[l], Font_System5x8);
		}
		break;
	case 99:
		LCD_display(SCANNER_INVALID,0);
		Scanner_service();		
		break;
	case 200:
		LCD_display(SCANNER_INVALID_SCANNER_MODE,0);
	default:
		while(1);
	}
	*/
}

void Scanner_UP_DOWN()
{
    /*
	if(Scanner_widget.parameter == 0){
		Scanner_widget.index ^= 0x01;
		Scanner_DISPLAY();
	}
	*/
}


void Scanner_SELECT()
{
	/*
    TEST_DATA* TD = Get_test_data();
	
	switch(Scanner_widget.parameter){
	case 0:
		if(!Scanner_widget.index){// 0: bacode scan? NO		
			TD->Code_test = 0;
			Manual_test();
		}
		else{// 1: scan
			Scanner_widget.parameter = 1;
			stcSC_state = trigger;
			Scanner_service();
		}			
		break;
	case 2: // 2: valid code scanned
		Manual_test();
	case 1: // 1: scanning...
	case 99: // 99: invalid code
	default: // error

		break;
	}
	*/
}

void Scanner_LEFT()
{
	/*
    SYS_INFO* info = Get_system_info();
	TEST_DATA* TD = Get_test_data();
	
	switch(Scanner_widget.parameter){
	case 0: // 0: bacode scan?
		Return_pre_widget();
		break;
	case 2: // 2: valid code scanned
		TD->Code_test = 0;
		if(info->pre_test_state && stcSC_state == idle){
			Widget_init(manual_test);
			return;
		}
	case 99: // 99: invalid code
		SCAN_OFF;
		Scanner_widget.parameter = 0;
		stcSC_state = idle;
		Scanner_DISPLAY();		
		break;
	case 1: // 1: scanning...
	default:
		//while(1);
		break;
	}
	*/
}



void Scanner_service()
{
	/*20220807
    start_test_flg = false;//just in case
	SYS_INFO* info = Get_system_info();
	SYS_PAR* par = Get_system_parameter();
	TEST_DATA* TD = Get_test_data();
	
	switch(Scanner_widget.parameter){
	case 1:
		// 1: scanning...
		if(stcSC_state == trigger){
			SCAN_ON;
			stcSC_state = get_result;
			user_SC_alloc_Len = SCANNER_RX_BUFFER_SIZE;
			Scanner_DISPLAY();
		}
		*/
		/*
		else if(stcSC_state == get_rd_mode){
			stcSC_state = get_result;
			user_SC_alloc_Len = SCANNER_RX_BUFFER_SIZE;
		}
		*/
		/*20200807
		else{
			UARTprintf("invalid stcSC_state: %d\n",stcSC_state);
		}
		break;
	case 2:
		// 2: valid code scanned
		if(stcSC_state == get_result){
			TD->Code_test = 1;

			// Store VIN code
			memcpy(par->VIN_str,user_SCANNER_RECEIVED,code_recv_len);
			
			save_system_parameter(&par->VIN_str, sizeof(par->VIN_str));
			//
			// set test state
			//
			info->pre_test_state = 1;
			save_system_info(&info->pre_test_state, sizeof(info->pre_test_state));
		}

		
		break;
	case 0: // 0: bacode scan?
	case 99: // 99: invalid code
	case 200: // 200: SCANNER_INVALID_SCANNER_MODE
	default: //while(1);
		break;
	}
	*/
}



void Scanner_Routine()
{
	/*
    static uint16_t poll_count = 0;
	static uint8_t retry = 0;

	
	if(Scanner_widget.parameter == 1 && stcSC_state == get_result){
		poll_count++;
		if(poll_count > 3000){
			poll_count = 0;			
			SCAN_OFF;
			Scanner_widget.parameter = 99;
			Scanner_DISPLAY();
			retry = 0;
			return;
		}
		if(poll_count % 600 == 0){
			scing_poll^= 0x01;
			Scanner_DISPLAY();
		}
	}
	
	if(user_bSCANNERecompl){
		user_bSCANNERecompl = false;

		if(stcSC_state == get_result){
				if(Scanner_widget.parameter != 1) return;
				
				SCANNER_BUZZ;
			SCAN_OFF;
				code_recv_len = strlen((const char*)user_SCANNER_RECEIVED);// LENGTH INCLUDE END CHARACTER \n\r
				Scanner_widget.parameter = 2;
				if(code_recv_len == VININ_LEN1){
					if(!check_vin(user_SCANNER_RECEIVED,VININ_LEN1)) Scanner_widget.parameter = 1;
				}
				else{
					Scanner_widget.parameter = 1;
				}
								
				poll_count = 0;
				
				if(Scanner_widget.parameter == 2){
					retry = 0;			
					Scanner_service();
					Scanner_DISPLAY();
				}
				else{				
					retry++;
					UARTprintf("invalid length code scanned %d times\n",retry);
					if(retry < 3){
						stcSC_state = trigger;
						Scanner_service();
					}
					else{
						Scanner_widget.parameter = 99;
					//SCAN_OFF;
						Scanner_DISPLAY();
						retry = 0;
					}
				}
		}
	}
	*/
}

void Manual_test(){
	float f_v;
	TEST_DATA* TD = Get_test_data();
	SYS_PAR* par = Get_system_parameter();
	BattCoreScanVoltage(&f_v);

	if(f_v < check_clamp_v){
		if((Clamp_err & get_system_error()) == 0) set_system_error(Clamp_err);
		Move_widget(Scanner_sub_W[3], 0);
		return;
	}
	
	
	if(par->WHICH_TEST == Battery_Test || par->WHICH_TEST == Start_Stop_Test){	
		if(TD->SYSTEM_CHANNEL == MAP_24V){
			Move_widget(Scanner_sub_W[4],0);
		}
		else{		
//#if defined(__BO__)                                 // 20200906 dw remask 6v bat test for BT2000_HD_BO
//			TD->SYSTEM_CHANNEL = MAP_12V;
//			Move_widget(Scanner_sub_W[1],0);
//#else
			if(par->WHICH_TEST == Battery_Test && TD->SYSTEM_CHANNEL == MAP_6V){
				Move_widget(Scanner_sub_W[0],0);
			}
			else{
				TD->SYSTEM_CHANNEL = MAP_12V;
				Move_widget(Scanner_sub_W[1],0);
			}
//#endif
		}
	}
	else{//system test	
		float fBodyTemp;
		uint32_t ui32buf;		
		BattCoreGetVoltage(&ui32buf,&f_v);

		if(TD->SYSTEM_CHANNEL == MAP_24V && (ui32buf >= 4095 || f_v >= 32.00)){
			Move_widget(Scanner_sub_W[4],0);
		}
		else{
			GetTemp( &fBodyTemp, &TD->SysObjTemp);
			UARTprintf("System test(); go to check ripple\n");
			Move_widget(Scanner_sub_W[2],0);
		}
	}
}


