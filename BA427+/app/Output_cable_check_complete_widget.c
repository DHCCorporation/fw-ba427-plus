/*
 * Output_cable_check_complete_widget.c
 *
 *  Created on: 2020年12月23日
 *      Author: ray.yuan
 */

#include "Widgetmgr.h"

void OutputCableChkComplete_DISPLAY();
void OutputCableChkComplete_Service();

widget* OutputCableChkComplete_sub_W[] = {

};

widget OutputCableChkComplete_widget = {
        .Key_UP = 0,
        .Key_DOWN = 0,
        .Key_RIGHT = 0,
        .Key_LEFT = 0,
        .Key_SELECT = 0,
        .routine = 0,
        .service = OutputCableChkComplete_Service,
        .initial = 0,
        .display = OutputCableChkComplete_DISPLAY,
        .subwidget = OutputCableChkComplete_sub_W,
        .number_of_widget = sizeof(OutputCableChkComplete_sub_W)/4,
        .index = 0,
        .no_messgae = 1,
        .name = "OutputCableChkComplete",
};

void OutputCableChkComplete_DISPLAY(){
    // Clear screen
    ST7565ClearScreen(BUFFER1);
    ST7565Refresh(BUFFER1);
    // Write SCREEN
    LCD_String_ext(16,get_Language_String(S_CABLE_CHECK_COMPLETE_1) , Align_Central);
    LCD_String_ext(32,get_Language_String(S_CABLE_CHECK_COMPLETE_2) , Align_Central);

    delay_ms(2000);
}

void OutputCableChkComplete_Service(){
    Widget_init(manual_test);
}


