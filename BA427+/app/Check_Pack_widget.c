//*****************************************************************************
//
// Source File: Check_Pack_widget.c
// Description: show one by one pack battery test txt
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 8/17/20      David.Wang       Created file.
//
//
// =============================================================================
//
//
// $Header:   $
// $DateTime: $
// $Author:   $
//
//
// Copyright (c) 2020 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#include "Widgetmgr.h"

void Check_Pack_DISPLAY();
void Check_Pack_LEFT();

widget* Check_Pack_sub_W[] = {
        &AbortTest_widget,
};

widget Check_Pack_widget = {
        .Key_UP = 0,
        .Key_DOWN = 0,
        .Key_RIGHT = 0,
        .Key_LEFT = Check_Pack_LEFT,
        .Key_SELECT = 0,
        .routine = 0,
        .service = 0,
        .initial = 0,
        .display = Check_Pack_DISPLAY,
        .subwidget = Check_Pack_sub_W,
        .number_of_widget = sizeof(Check_Pack_sub_W)/4,
        .index = 0,
        .no_messgae = 1,
        .name = "Check_Pack",
};

void Check_Pack_LEFT(){
    Move_widget(Check_Pack_sub_W[0],0);
}


void Check_Pack_DISPLAY(){
    TEST_DATA* TD = Get_test_data();
    uint8_t Bat_Num;

    Bat_Num = TD->PackTestCount;

    switch(Bat_Num){
    case 1:
        LCD_display(SEP_PACK_CONNECT_BAT1, 0);
        break;
    case 2:
        LCD_display(SEP_PACK_CONNECT_BAT2, 0);
        break;
    case 3:
        LCD_display(SEP_PACK_CONNECT_BAT3, 0);
        break;
    case 4:
        LCD_display(SEP_PACK_CONNECT_BAT4, 0);
        break;
    case 5:
        LCD_display(SEP_PACK_CONNECT_BAT5, 0);
        break;
    case 6:
        LCD_display(SEP_PACK_CONNECT_BAT6, 0);
        break;
    default:
        // NORMAL
        break;
    }
}








