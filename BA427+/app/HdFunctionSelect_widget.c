//*****************************************************************************
//
// Source File: HdFunctionSelect_widget.c
// Description:
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 08/13/20     David.Wang      Created file.
//
//
//
// =============================================================================
//
//
// $Header:   $
// $DateTime: $
// $Author:   $
//
//
// Copyright (c) 2020 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#include "Widgetmgr.h"

void HdFunctionSelect_UP();
void HdFunctionSelect_DOWN();
void HdFunctionSelect_LEFT();
void HdFunctionSelect_SELECT();
void HdFunctionSelect_display();
void HdFunctionSelect_initial();

widget* HdFunctionSelect_sub_W[] = {
     &BatterySel_widget,
};

widget HdFunctionSelect_widget = {
        .Key_UP = HdFunctionSelect_UP,
        .Key_DOWN = HdFunctionSelect_DOWN,
        .Key_RIGHT = 0,
        .Key_LEFT = HdFunctionSelect_LEFT,
        .Key_SELECT = HdFunctionSelect_SELECT,
        .initial = HdFunctionSelect_initial,
        .routine = 0,
        .service = 0,
        .display = HdFunctionSelect_display,
        .subwidget = HdFunctionSelect_sub_W,
        .number_of_widget = 4,
        .index = 0,
        .name = "HdFunctionSelect",
};
void HdFunctionSelect_initial(){
    if(HdFunctionSelect_widget.parameter == 2){
        HdFunctionSelect_widget.parameter = 0;
        HdFunctionSelect_widget.index = HdFunctionSelect_widget.number_of_widget-1;
    }
}

void HdFunctionSelect_UP(){
    if(HdFunctionSelect_widget.parameter == 0){
        HdFunctionSelect_widget.index = (HdFunctionSelect_widget.index == 0)? HdFunctionSelect_widget.number_of_widget:HdFunctionSelect_widget.index-1;
        HdFunctionSelect_display();
    }
}

void HdFunctionSelect_DOWN(){
    if(HdFunctionSelect_widget.parameter == 0){
        HdFunctionSelect_widget.index = (HdFunctionSelect_widget.index + 1) % (HdFunctionSelect_widget.number_of_widget + 1);
        HdFunctionSelect_display();
    }
}

void HdFunctionSelect_LEFT(){
    if(HdFunctionSelect_widget.parameter == 0){
        Return_pre_widget();
    }
    else{
        HdFunctionSelect_widget.parameter = 0;
        HdFunctionSelect_display();
    }
}

void HdFunctionSelect_SELECT(){

    TEST_DATA* TD = Get_test_data();
    SYS_PAR* par = Get_system_parameter();                 // 20200819 DW EDIT

    TD->PackTestNum = HdFunctionSelect_widget.index + 2 ;  // pack bat num check
    TD->PackTestCount = 0;                                 // reset count

    par->BAT_test_par.PackTestNum = TD->PackTestNum;                      // save to eeprom
    par->BAT_test_par.PackTestCount = TD->PackTestCount;                  // save to eeprom
    save_system_parameter(&par->BAT_test_par, sizeof(par->BAT_test_par)); // save to eeprom

    Move_widget(HdFunctionSelect_sub_W[0],0);              // return to BAT TEST
}

void HdFunctionSelect_display(){

        LCD_display(Of_Bat_In_Par,HdFunctionSelect_widget.index);
        DrawPageInfo(HdFunctionSelect_widget.index,5,4);
}


