//*****************************************************************************
//
// Source File: Diesel40Sec_widget.c
// Description: Confirm whether it is a diesel engine.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 08/06/20     David.Wang      Created file.
// 09/08/20     David.Wang      Add void Diesel40Sec_initial(); for clear register
// 10/20/20     David Wang      Diesel engine flow add new screen
//
// ============================================================================
//
//
// $Header:   $
// $DateTime: $
// $Author:   $
//
//
// Copyright (c) 2020 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#include "Widgetmgr.h"


void Diesel40Sec_DISPLAY();
void Diesel40Sec_Select();
void Diesel40Sec_Timercallback();
void Diesel40Sec_Service();
void Diesel40Sec_Routine();
void Diesel40Sec_LEFT();
void Diesel40Sec_UP_DOWN();
void Diesel40Sec_initial();

static uint8_t stc_SwScreen = 0;
uint16_t Display_Refresh_40SEC = 0;

widget* Diesel40Sec_sub_W[] = {
        &AltIdleVolt_widget,
        &AbortTest_widget,

};

widget Diesel40Sec_widget = {
        .Key_UP = Diesel40Sec_UP_DOWN,
        .Key_DOWN = Diesel40Sec_UP_DOWN,
        .Key_RIGHT = 0,
        .Key_LEFT = Diesel40Sec_LEFT,
        .Key_SELECT = Diesel40Sec_Select,
        .routine = Diesel40Sec_Routine,
        .service = Diesel40Sec_Service,
        .initial = Diesel40Sec_initial,
        .display = Diesel40Sec_DISPLAY,
        .subwidget = Diesel40Sec_sub_W,
        .number_of_widget = sizeof(Diesel40Sec_sub_W)/4,
        .index = 0,
        .no_messgae = 1,
        .name = "Diesel40Sec",
};

void Diesel40Sec_initial(){
    stc_SwScreen = 0;                     // clear register for new sys test can into
}


void Diesel40Sec_UP_DOWN(){
    if( stc_SwScreen == 0 ){
        Diesel40Sec_widget.parameter ^= 0x01;
        Diesel40Sec_DISPLAY();
    }
}

void Diesel40Sec_LEFT(){
    Main_routine_switch(1);
    Move_widget(Diesel40Sec_sub_W[1], 0);
}


void Diesel40Sec_Select(){
    if( stc_SwScreen == 0){
        if( Diesel40Sec_widget.parameter ){
            stc_SwScreen = 1;                     // 20200807 DW EDIT YES/NO >> COUNT DOWN 40SEC
            Diesel40Sec_DISPLAY();
        }
        else{

            Move_widget(Diesel40Sec_sub_W[0],0);  // ASK LEAVE THIS FUNCTION BACK TO MAINMENU
        }
    }
    else {//if ( stc_SwScreen == 1){
        stc_SwScreen = 2;                         // 20201020 DW EDIT add check is it ok press enter then to countdown
        Diesel40Sec_Service();                    // COUNTDOWN START
    }


}

void Diesel40Sec_Service(){
    if( stc_SwScreen == 2){// COUNTDOWN START
        //
        // Countdonw 40 sec
        //
        set_countdown_callback(Diesel40Sec_Timercallback, 40);
        Diesel40Sec_widget.parameter = 1;
        if(get_routine_switch()){
            Main_routine_switch(0);
        }
    }
    else{
        //normal
    }
}

void Diesel40Sec_DISPLAY(){

    if(stc_SwScreen == 0){
        LCD_display(IS_DIESEL_ENGINE, Diesel40Sec_widget.parameter^0x01);
    }
    else if(stc_SwScreen == 1){
        LCD_display(Please_Enter_Start, 0);
    }
    else{//if(stc_SwScreen == 2){
        LCD_display(RIPPLE_TEST, (40 - g_15SecCDCounter) % 3);
        LCD_draw_string(REV_ENGINE_FOR, 0, 15);                            // 20200803 DW EDIT
        LCD_draw_string(SEC_SET, g_15SecCDCounter, 33);                    //sTemp.i16YMax+2); 20200803 DW EDIT
    }

}
void Diesel40Sec_Routine(){
    if(stc_SwScreen == 0 || stc_SwScreen == 1 ){
          //NORMAL
    }
    else if (stc_SwScreen == 2){
        if(!g_15SecCDCounter){
            stc_SwScreen = 3;
            Main_routine_switch(1);
        }

        if(Display_Refresh_40SEC == g_15SecCDCounter){
            //no reflash
        }
        else{
            Display_Refresh_40SEC = g_15SecCDCounter;
            Diesel40Sec_DISPLAY();
        }

    }
    else{
        Move_widget(Diesel40Sec_sub_W[0],0);  // over 40sec count down , go to AltIdleVolt_widget
        delay_ms(33.3);                       // ROM_SysCtlDelay(get_sys_clk()/3/30);
    }

}

void Diesel40Sec_Timercallback(){
    /*if(Diesel40Sec_widget.parameter != 2){
        Diesel40Sec_widget.parameter = 1;
    }*/
}


