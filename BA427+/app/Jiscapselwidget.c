//*****************************************************************************
//
// Source File: Jiscapselwidget.c
// Description: JIS Capacity Select widget including normal select via Up/Down key,
//				and press Enter key to move in virtual keyboard to search the specific
//				Battery Type directly. Press Left key to pop up the stack and
//				press Right key to start to "Battery Test" or "Start Stop Test".
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 09/17/15     William.H      Created file.
// 10/01/15     William.H      Re-arrange/remove Global varaible's usage due to we've used stack to behave the manu level selection.
//                             Respective global varaible by the respective widget file to be maintained by their own.
// 10/05/15     William.H      Check in BT2020 project which branched from LCDFontMaker(Pre-BT2020) by modifed related GPIOs.
// 10/30/15     William.H      This to fix the Internal Battery Low suddenly been presented scenario, then clear menu stack and back to Main Menu immediately will cause state currupt issue.
// 01/19/16     Vincent.T      Modify JISCapSelRight();
// 01/28/16     Vincent.T      1. New g_JISSSBatType(JIS SS battery)
//                             2. New pointer pg_JIS
//                             3. global variable g_SSBatteryType
//                             4. JIS global variable: pg_JIS, g_JISSSBatType
//                             5. Modify NUM_JIS_BATTERY_TYPE(for JIS)
// 01/29/16     Vincent.T      1. global variables: g_SSSelectRating, WHICH_TEST
//                             2. Modify JISCapSelLeft(); for start-stop test
//                             3. #include "../service\testrecordsvc.h"
// 01/30/16     Vincent.T      1. Modify JISCapSelRight(); to seperate SS/BT JIS battery
// 02/15/16     Vincent.T      1. Release memory in JISCapSelRight();
// 03/28/16     Vincent.T      1. Modify JISCapSelLeft(); to handle multination language case.
//                             2. Modify JISCapSelClick(); to handle multination language case.
//                             3. Modify JISCapSelDrawBATList(); to handle multination language case.
//                             4. Modify JISCapSelSearchByVKybd(); to handle multination language case.
//                             5. Modify JISCapSelVKybd123Click(); to handdle multination language case.
//                             6. Modify JISCapSelVKybdABCClick(); to handle multination language case.
// 03/29/16     Vincent.T      1. Modify JISCapSelVKybd123Up(); to handle multination language case.
//                             2. Modify JISCapSelVKybd123Down(); to handle multinaiton language case.
//                             3. Modify JISCapSelVKybdABCDown(); to handle multination language case.
//                             4. Modify JISCapSelVKybd123Right(); to handle multination language case.
//                             5. Modify JISCapSelVKybdABCRight(); to handle multination language case.
// 08/25/16     Vincent.T      1. global variable g_SetCapacityVal, g_BTSetCapacityVal and g_SSSetCapacityVal;
//                             2. Modify JISCapSelRight(); to access final JIS CCA value.
// 01/19/17     Vincent.T      1. Modify JISCapSelVKybdABCDown(); to anti-white correct item.
// 09/21/17 	Jerry.W		   Rewrite Widget for new software architecture
// 09/30/17 	Jerry.W		   1. add const in tRectangle and tJISBatteryType array for fewer memory usage
// 12/11/17     Jerry.W        1. Add g_jisDLList check to prevetn usb plug-in issue
// 02/14/18     Henry.Y	       modified JIS list.   
// 03/22/18     William.H      Modify ChkSurfaceCharge_sub_W[] to enter to CountDown15S_widget directly if its Test-In-Vehcile. 
// 09/11/18     Henry.Y        Applied new language hierarchical architecture.
// 10/17/18     Henry.Y        To seperate regular JIS battery and AGM JIS battery.
// 12/07/18     Henry.Y        New display api.
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/app/Jiscapselwidget.c#2 $
// $DateTime: 2017/12/18 16:47:21 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#include "Widgetmgr.h"
#define JIS_DIN_AFTER 60

typedef enum {
	INSERT = 0,
	REMOVE
} JISSEARCHTYPE;

void JISCapSelAntiWhiteItem(  int16_t itemNum);
void JISCapSelVKAntiWhiteItem( int16_t itemNum);
void JISCapSelVKMove(int16_t itemNum);
void JISCapSelDrawBATList(  uint8_t itemIdx, char* text);
void JISCapSelDrawOptions(uint8_t sel);
void JISCapSelSearchByVKybd(  uint8_t chac, JISSEARCHTYPE type);
void JISCapSelVKydbDrawChar(  char *textEditStr);


void JISCapSel_SELECT();
void JISCapSel_UP();
void JISCapSel_LEFT();
void JISCapSel_RIGHT();
void JISCapSel_display();
void JISCapSel_Initial();

char g_JISTextEditStr[9];
int8_t g_JISTxtEdtStrLen;
uint8_t g_JISVKybdLegacyChar;
JISBATDLList g_jisDLList;
const tJISBatteryType *pg_JIS;
const tJISBatteryType g_JISRgBatType[] =
{	
	{"26A17",	0, 225},	{"26A19",	1, 201},	{"28A19",	2, 248},	{"30A19",	3, 264},	{"32A19",	4, 294},    {"26B17",   5, 225},
	{"28B17",   6, 246},    {"34B17",	7, 279},	{"28B19",	8, 247},	{"34B19",	9, 272},    {"38B19",	10, 300},	{"40B19",	11, 305},
	{"36B20",	12, 274},	{"38B20",	13, 300},	{"40B20",	14, 360},	{"42B20",	15, 371},   {"32B24",   16, 238},   {"46B24",	17, 325},
	{"50B24",	18, 390},	{"55B24",	19, 433},	{"60B24",	20, 467},   {"32C24",	21, 238},   {"50D20",	22, 306},	{"55D23",	23, 356},
	{"65D23",	24, 420},	{"70D23",	25, 490},   {"75D23",	26, 520},	{"48D26",	27, 278},	{"55D26",	28, 348},   {"65D26",	29, 413},
	{"75D26",	30, 490},	{"80D26",	31, 582},	{"90D26",	32, 624},	{"65D31",	33, 389},	{"75D31",	34, 447},	{"95D31",	35, 622},
	{"105D31",	36, 664},	{"115D31",	37, 782},	{"95E41",	38, 512},   {"105E41",	39, 577},	{"115E41",	40, 651},	{"120E41",	41, 686},
	{"130E41",	42, 799},   {"115F51",	43, 638},	{"130F51",	44, 705},	{"145F51",	45, 780},	{"150F51",	46, 916},	{"160F51",	74, 1005},
	{"170F51",	48, 1045},	{"145G51",	49, 754},	{"160G51",	50, 950},	{"165G51",	51, 933},	{"180G51",	52, 1090},  {"195G51",	53, 1146},
	{"190H52",	54, 924},	{"225H52",	55, 1313},	{"245H52",	56, 1532},  {"58",  57, 540},       {"65",  58, 875},       {"30HR-740", 59, 740},
	{"4DLT",  60, 840},     {"54459",  61, 210},    {"54519",  62, 220},    {"55559",  63, 255},    {"56219",  64, 280},    {"56318", 65, 300},
	{"56638", 66, 300},     {"58827",  67, 395},    {"59219",  68, 450}
};


//
// Start-stop test AGM
//
const tJISBatteryType g_JIS_SS_AGM[] =
{
	{"S34B20",	0,	320},
	{"S46B24",	1,	360},
	{"S55D23",	2,	390},
	{"S65D26",	3,	450},
	{"LN1",	4,	560},
	{"LN2",	5,	680},
	{"LN3",	6,	760},
	{"LN4",	7,	800},
	{"LN5",	8,	850},
};

//
// Start-stop test EFB
//
//const tJISBatteryType g_JISSSBatType[] =
const tJISBatteryType g_JIS_SS_EFB[] =
{	
	{"M-42",	0,	340},
	{"N-55",	1,	460},
	{"Q-85",	2,	550},
	{"S-85",	3,	600},
	{"S-95",	4,	680},
	{"T-110",	5,	780},
	{"LN2", 6,	560},
	{"LN3", 7,	680},
	{"LN4", 8,	730},
	{"LN5", 9,	850},
};

uint16_t NUM_JIS_BATTERY_TYPE;
const uint16_t NUM_JIS_BT = (sizeof(g_JISRgBatType)/ sizeof(tJISBatteryType));
const uint16_t NUM_JIS_SS_EFB = (sizeof(g_JIS_SS_EFB)/ sizeof(tJISBatteryType));
const uint16_t NUM_JIS_SS_AGM = (sizeof(g_JIS_SS_AGM)/ sizeof(tJISBatteryType));

widget* JISCapSel_sub_W[]={
        &BatTemp_widget, //PointToBAT_widget
};

widget JISCapSel_widget = {
		.Key_UP = JISCapSel_UP,
		.Key_DOWN = 0,
		.Key_RIGHT = JISCapSel_RIGHT,
		.Key_LEFT = JISCapSel_LEFT,
		.Key_SELECT = JISCapSel_SELECT,
		.routine = 0,
		.service = 0,
		.initial = JISCapSel_Initial,
		.display = JISCapSel_display,
		.subwidget = JISCapSel_sub_W,
		.number_of_widget = sizeof(JISCapSel_sub_W)/4,
		.index = 0,
		.name = "JISCapSel",
		.Combo = Combo_right | Combo_left,
};
        
uint8_t get_JIS_model_rating(uint16_t idx)
{
    return (idx > JIS_DIN_AFTER && idx < NUM_JIS_BATTERY_TYPE)? DIN:SAE;
}

void JISCapSel_Initial(){
	TEST_DATA* TD = Get_test_data();
	SYS_PAR* par = Get_system_parameter();
	pg_JIS = g_JISRgBatType;
	NUM_JIS_BATTERY_TYPE = NUM_JIS_BT;
	TD->JISCapSelect = (par->BAT_test_par.BTJIS_RgBatSelect >= NUM_JIS_BATTERY_TYPE)? \
                        0:par->BAT_test_par.BTJIS_RgBatSelect;
}

void JISCapSel_SELECT(){
	LANGUAGE LangIdx = get_language_setting();
	TEST_DATA* TD = Get_test_data();
	SYS_PAR* par = Get_system_parameter();

	TD->SetCapacityVal = pg_JIS[TD->JISCapSelect].capVal;
	par->BAT_test_par.BTJIS_RgBatSelect = TD->JISCapSelect;
	par->BAT_test_par.BTSetCapacityVal = TD->SetCapacityVal;
	save_system_parameter(&par->BAT_test_par, sizeof(par->BAT_test_par));
	Move_widget(JISCapSel_sub_W[0],0);
}



void JISCapSel_RIGHT(){
	TEST_DATA* TD = Get_test_data();

	if(TD->JISCapSelect){
	    TD->JISCapSelect--;
	}
	else{
	    TD->JISCapSelect = NUM_JIS_BATTERY_TYPE - 1;
	}
	JISCapSel_display();
	delay_ms(200);

}


void JISCapSel_LEFT(){
	TEST_DATA* TD = Get_test_data();

	TD->JISCapSelect = (TD->JISCapSelect+1) % NUM_JIS_BATTERY_TYPE;
	JISCapSel_display();
	delay_ms(200);

}

void JISCapSel_UP(){
    TEST_DATA* TD = Get_test_data();
    SYS_PAR* par = Get_system_parameter();
    par->BAT_test_par.BTJIS_RgBatSelect = TD->JISCapSelect;
    //JISDLlist_Del(&g_jisDLList);
	Return_pre_widget();
}

void JISCapSel_display(){
	LANGUAGE LangIdx = get_language_setting();
	TEST_DATA* TD = Get_test_data();
	char buf[20];
    // Clear screen
    ST7565ClearScreen(BUFFER1);
    ST7565Refresh(BUFFER1);
    // Write SCREEN
    LCD_String_ext(16,get_Language_String(S_SET_CAPACITY) , Align_Left);
    // cap num + cca
	//JISCapSelDrawOptions(TD->JISCapSelect );
    sprintf( buf, "%s", pg_JIS[TD->JISCapSelect].batString );
    if(pg_JIS[TD->JISCapSelect].itemNum>JIS_DIN_AFTER){
        sprintf( buf+strlen(buf), "%*d%s", 15-2-strlen(buf) , pg_JIS[TD->JISCapSelect].capVal,get_Language_String(S_DIN));
    }
    else{
        sprintf( buf+strlen(buf), "%*d%s", 15-2-strlen(buf) , pg_JIS[TD->JISCapSelect].capVal,get_Language_String(S_CCA));
    }

    LCD_String_ext(32, buf, Align_Left);
	LCD_Arrow(Arrow_enter | Arrow_RL);// | Arrow_up
}




#if 0
void JISCapSel_Initial(){
	TEST_DATA* TD = Get_test_data();
	SYS_PAR* par = Get_system_parameter();

	//Battery Test
	//pg_JIS = (par->BAT_test_par.BatteryType == FLOODED || par->BAT_test_par.BatteryType == VRLA_GEL)? g_JISRgBatType:g_JIS_SS_AGM;
	//NUM_JIS_BATTERY_TYPE = (par->BAT_test_par.BatteryType == FLOODED || par->BAT_test_par.BatteryType == VRLA_GEL)? NUM_JIS_BT:NUM_JIS_SS_AGM;
	//TD->JISCapSelect = (par->BAT_test_par.BatteryType == FLOODED || par->BAT_test_par.BatteryType == VRLA_GEL)? par->BAT_test_par.BTJIS_RgBatSelect:par->BAT_test_par.BTJIS_AGMBatSelect;
	// Only ONE JIS TABLE
	pg_JIS = g_JISRgBatType;
	NUM_JIS_BATTERY_TYPE = NUM_JIS_BT;
	TD->JISCapSelect = par->BAT_test_par.BTJIS_RgBatSelect;

	TD->JISCapSelect = (TD->JISCapSelect >= NUM_JIS_BATTERY_TYPE)? 0:TD->JISCapSelect;

	if(g_jisDLList.head != NULL ){
		JISDLlist_Del(&g_jisDLList);
	}

	JISDLlist_Init_All(&g_jisDLList, pg_JIS, NUM_JIS_BATTERY_TYPE);
	JISDLlist_Search_by_ItemNum(&g_jisDLList, pg_JIS[TD->JISCapSelect].itemNum );
}
void JISCapSelDrawBATList(  uint8_t itemIdx, char* text)
{
	uint8_t l, sLen;
	LANGUAGE LangIdx = get_language_setting();
	UARTprintf("JISCapSelDrawBATList(). itemIdx = %d.\n", itemIdx);

	sLen = strlen(text);

	for(l = 0; l < (8-sLen); l++)
	{
		ST7565Draw6x8Char(l*6, 15 + (itemIdx * 10), 0x20, Font_System5x8);
	}

	for(l = 0; l < sLen; l++)
	{
		//
		//	(8-sLen)*6 ==> The position of starting character. Right truing.
		//
		ST7565Draw6x8Char(((8-sLen)*6 + (l * 6)), 15 + (itemIdx * 10), text[l], Font_System5x8);
	}
}


void JISCapSelAntiWhiteItem(  int16_t itemNum)
{
	tRectangle sTemp;
	LANGUAGE LangIdx = get_language_setting();
	UARTprintf("JISCapSelAntiWhiteItem(). itemNum = %d.\n", itemNum);

	sTemp.i16XMin = 0;
	sTemp.i16XMax = 48;

	sTemp.i16YMin = 14 + (10* itemNum);
	sTemp.i16YMax = 23 + (10* itemNum);

	ST7565RectReverse(&sTemp, VERTICAL);

}


void JISCapSelDrawOptions(uint8_t sel){
	TEST_DATA* TD = Get_test_data();
	uint8_t i,offset = TD->JISCapSelect % 4;

	for(i = 0; i<4;i++){
		if((sel - offset + i) >= NUM_JIS_BATTERY_TYPE){
			JISCapSelDrawBATList( i, "        ");
		}
		else{
			JISCapSelDrawBATList( i, pg_JIS[sel - offset + i].batString);
		}
	}
	JISCapSelAntiWhiteItem( sel%4);
}

const tRectangle VK_recs[34] = {
		// 0              1              2              3              4              5              6              7              8              9
		{65,24,72,31},{78,24,85,31},{90,24,97,31},{103,24,110,31},{115,24,123,31},{65,32,72,39},{78,32,85,39},{90,32,97,39},{103,32,110,39},{116,32,123,39},
		{64,40,80,46},{84,40,90,48},{94,40,106,48},{112,40,123,46},{62,24,68,31},{70,24,76,31},{79,24,85,31},{87,24,93,31},{96,24,101,31},{104,24,109,31},
		{111,24,117,31},{119,24,125,31},{62,32,68,39},{70,32,76,39},{79,32,85,39},{87,32,93,39},{95,32,101,39},{104,32,110,39},{112,32,117,39},{119,32,125,39},
		{64,40,77,46},{84,40,90,48},{94,40,106,48},{112,40,123,46}
};

void JISCapSelVKAntiWhiteItem(  int16_t itemNum){
	tRectangle sTemp = VK_recs[itemNum];
	LANGUAGE LangIdx = get_language_setting();

	ST7565RectReverse(&sTemp, VERTICAL);

}

void JISCapSelVKMove(int16_t itemNum){
	JISCapSelVKAntiWhiteItem(JISCapSel_widget.index);
	JISCapSel_widget.index = itemNum;
	JISCapSelVKAntiWhiteItem(JISCapSel_widget.index);
}


void JISCapSelSearchByVKybd( uint8_t chac, JISSEARCHTYPE type)
{
	JISNode *currNode;
	TEST_DATA* TD = Get_test_data();

	if(type == INSERT)
	{
		g_JISTextEditStr[g_JISTxtEdtStrLen++] = chac;
		g_JISTextEditStr[g_JISTxtEdtStrLen] = '\0';
	}
	else if(type == REMOVE)
	{
		if(g_JISTxtEdtStrLen > 0)
		{
			g_JISTextEditStr[g_JISTxtEdtStrLen] = '\0';
			g_JISTxtEdtStrLen--;
			JISCapSelVKydbDrawChar( g_JISTextEditStr);
		}
		else{
			return;
		}
	}

	UARTprintf("JISCapSelSearchByVKybd(). g_JISTextEditStr = %s. \n", g_JISTextEditStr);

	currNode = JISDLlist_Search_by_BATTypeString(&g_jisDLList, g_JISTextEditStr);

	if(type == INSERT)
	{
		if(currNode == NULL)
		{
			UARTprintf("INSERT: No JIS Battery Type match!\n");
			g_JISTextEditStr[g_JISTxtEdtStrLen] = '\0';
			g_JISTxtEdtStrLen--;
		}
		else
		{
			UARTprintf("INSERT: Find the Node! : batString = %s, itemNum = %d, capVal = %d.\n", currNode->psJISBatType.batString, currNode->psJISBatType.itemNum, currNode->psJISBatType.capVal);
			JISCapSelVKydbDrawChar( g_JISTextEditStr);

			// {
			JISCapSelAntiWhiteItem( TD->JISCapSelect%4);


			TD->JISCapSelect = currNode->psJISBatType.itemNum;

			JISCapSelDrawOptions(TD->JISCapSelect);
			// }
		}
	}
	else if(type == REMOVE)
	{
		if(currNode == NULL)
		{
			UARTprintf("REMOVE: No JIS Battery Type match!\n");
		}
		else if(currNode != NULL && g_JISTxtEdtStrLen != 0)
		{
			UARTprintf("REMOVE: Find the Node! : batString = %s, itemNum = %d, capVal = %d.\n", currNode->psJISBatType.batString, currNode->psJISBatType.itemNum, currNode->psJISBatType.capVal);

			// {
			JISCapSelAntiWhiteItem( TD->JISCapSelect%4);

			TD->JISCapSelect = currNode->psJISBatType.itemNum;

			JISCapSelDrawOptions(TD->JISCapSelect);
			// }
		}
		else if(g_JISTxtEdtStrLen == 0)
		{
			UARTprintf("REMOVE: g_JISTxtEdtStrLen = -1! No JIS Battery Type match!\n");
		}
	}
}


void JISCapSelVKydbDrawChar( char *textEditStr)
{
	uint8_t l, sLen;
	LANGUAGE LangIdx = get_language_setting();
	UARTprintf("JISCapSelVKydbDrawChar(). textEditStr = %s.\n", textEditStr);

	sLen = g_JISTxtEdtStrLen;

	//
	//	Clear the Non-character area
	//
	for(l = 0; l < (8-sLen); l++)
	{
		ST7565Draw6x8Char((70 + l*6), 15, 0x20, Font_System5x8);
	}

	//
	//	70 + (8-sLen)*6 ==> The position of starting character. Right truing.
	//
	for(l = 0; l < sLen; l++)
	{
		ST7565Draw6x8Char((70+(8-sLen)*6 + (l*6)), 15, textEditStr[l], Font_System5x8);
	}
}
#endif

