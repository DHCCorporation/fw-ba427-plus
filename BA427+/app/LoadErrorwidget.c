//*****************************************************************************
//
// Source File: LoadErrorwidget.c
// Description: Declaraction load error functions of btns event.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 04/14/16     Vincent.T	   Created file.
// 09/21/17 	Jerry.W		   Rewrite Widget for new software architecture
// 12/07/18     Henry.Y        New display api.
// 20201205     David.W        Modify screen to same as BT2010
//
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/app/LoadErrorwidget.c#6 $
// $DateTime: 2017/09/22 11:20:01 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#include "Widgetmgr.h"

void LoadError_display();

widget* LoadError_sub_W[] = {
};

widget LoadError_widget = {
		.Key_UP = 0,
		.Key_DOWN = 0,
		.Key_RIGHT = 0,
		.Key_LEFT = 0,
		.Key_SELECT = 0,
		.routine = 0,
		.service = 0,
		.display = LoadError_display,
		.subwidget = LoadError_sub_W,
		.number_of_widget = sizeof(LoadError_sub_W)/4,
		.index = 0,
		.no_messgae = 1,
		.name = "LoadError",
};

void LoadError_display(){
     // Clear screen
     ST7565ClearScreen(BUFFER1);
     ST7565Refresh(BUFFER1);
     // Write SCREEN
     LCD_String_ext(16,get_Language_String(S_LOAD_ERROR) , Align_Central);
}
