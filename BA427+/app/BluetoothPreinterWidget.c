//*****************************************************************************
//
// Source File: BluetoothPreinterWidget.c
// Description: Bluetooth printer mode widget.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 11/06/17 	Jerry.W	       Created file.
// 12/25/17     Jerry.W        1. add indication protocol for condition of NoPaper, PowerLow and Printing.
//                             2. move handshake operation into command callback function to be responsive while printing.
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/app/BluetoothPreinterWidget.c#7 $
// $DateTime: 2017/12/27 11:16:43 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#include "Widgetmgr.h"


void BluetoothPreinter_UP_DOWN();
void BluetoothPreinter_DISPLAY();
void BluetoothPreinter_Select();
void BluetoothPreinter_Service();
void BluetoothPreinter_Routine();
void BluetoothPreinter_Initial();

void BluetoothPreinter_command_callback(const char *command);
bool BluetoothPreinter_command_match(const char *command,uint8_t lenth);
void BT_printer_process();
void BTPrinterAntiWhile(LANGUAGE LangIdx, uint8_t YesNo);
void EncodedToSting(uint8_t *des, uint8_t *encode);
void star_printer();
void SwitchToPrinterMode(uint32_t flag);
void BT_Printer_Timer_callback();

widget* BluetoothPreinter_sub_W[] = {
		&NoPaper_widget,
		&PowerLow_widget,
		&BluetoothPreinter_widget
};

widget BluetoothPreinter_widget = {
		.Key_UP = BluetoothPreinter_UP_DOWN,
		.Key_DOWN = BluetoothPreinter_UP_DOWN,
		.Key_RIGHT = 0,
		.Key_LEFT = 0,
		.Key_SELECT = BluetoothPreinter_Select,
		.routine = BluetoothPreinter_Routine,
		.initial = BluetoothPreinter_Initial,
		.service = BluetoothPreinter_Service,
		.display = BluetoothPreinter_DISPLAY,
		.subwidget = BluetoothPreinter_sub_W,
		.number_of_widget = sizeof(BluetoothPreinter_sub_W)/4,
		.index = 0,
		.no_messgae = 1,
		.name = "BluetoothPreinter",
};

/********************************************
 * state :			unsynced    ->     synced 		->	 	printing 		-> 		synced    		->     unsynced
 * event :				   	handshake	|	 printer start		|	 print complete	   |   bluetooth disconnected
*********************************************/
typedef enum {
	unsynced,
	synced,
	waiting_printer_parameter,
	ready_to_print,
	s_printing,
	leaving,
}Bluetooth_state;


typedef enum {
	normal = 0xFF,
	NoPaper = 1,
	c_printing = 2,
	PowerLow = 3,
}Bluetooth_condition;

#define PRINTER_PARA_SHIFTER_BT 0 //for type of battery test result
#define PRINTER_PARA_SHIFTER_ST 8 //for type of system test result
#define PRINTER_PARA_SHIFTER_INF 16 //for type of test date/time
#define PRINTER_PARA_SHIFTER_SN 24 //for type of sequence number

typedef enum{
	f_BT_voltage =	(0x00000001 << PRINTER_PARA_SHIFTER_BT),
	f_BT_CCA = 		(0x00000001 << (PRINTER_PARA_SHIFTER_BT+1)),
	f_BT_SOC = 		(0x00000001 << (PRINTER_PARA_SHIFTER_BT+2)),
	f_BT_SOH = 		(0x00000001 << (PRINTER_PARA_SHIFTER_BT+3)),
	f_BT_type = 	(0x00000001 << (PRINTER_PARA_SHIFTER_BT+4)),
	f_BT_judge = 	(0x00000001 << (PRINTER_PARA_SHIFTER_BT+5)),
	f_BT_RC = 		(0x00000001 << (PRINTER_PARA_SHIFTER_BT+6)),

	f_Sys_idle = 	(0x00000001 << PRINTER_PARA_SHIFTER_ST),
	f_Sys_loaded = 	(0x00000001 << (PRINTER_PARA_SHIFTER_ST+1)),
	f_Sys_crank = 	(0x00000001 << (PRINTER_PARA_SHIFTER_ST+2)),
	f_Sys_ripple = 	(0x00000001 << (PRINTER_PARA_SHIFTER_ST+3)),

	f_INF_temp = 	(0x00000001 << PRINTER_PARA_SHIFTER_INF),
	f_INF_date = 	(0x00000001 << (PRINTER_PARA_SHIFTER_INF+1)),
	f_INF_Tcode = 	(0x00000001 << (PRINTER_PARA_SHIFTER_INF+2)),
	f_INF_Bcode = 	(0x00000001 << (PRINTER_PARA_SHIFTER_INF+3)),

	f_SN_RO	= 		(0x00000001 << PRINTER_PARA_SHIFTER_SN),
	f_SN_VIN	= 	(0x00000001 << (PRINTER_PARA_SHIFTER_SN+1)),
	f_SN_Licn	= 	(0x00000001 << (PRINTER_PARA_SHIFTER_SN+2)),


}Printer_parameter;

#define PRINTER_ERR_HANDLE if(PrintErrorHanlder()){	return; }

//								   0	  1	  	  2	      3		  4	 	  5	  	  6	   	  7
const char c_handshake[7] = 	{0x04,	0x04,	0xFF,	0xFF,	0xFF,	0xFF,	0xFF};
char e_SH_clamped[8] = 	{0x04,	0x04,	0x01,	0x22,	0xFF,	0xFF,	0xFF,	0xFF};
char e_SH_no_clamped[8] = {0x04,	0x04,	0x02,	0x22,	0xFF,	0xFF,	0xFF,	0xFF};
const char c_print_para[7] = 	{0x07,	0x01,	0xFF,	0xFF,	0xFF,	0xFF,	0xFF};
const char e_ready_print[8] =  	{0x07,	0x0F,	0xFF,	0xFF,	0x01,	0xFF,	0xFF,	0xFF};
const char c_print_start[7] = 	{0x07,	0x11,	0xFF,	0xFF,	0xFF,	0xFF,	0xFF};
const char e_print_finish[8] =  {0x07,	0x12,	0xFF,	0x22,	0x02,	0xFF,	0xFF,	0xFF};
char e_BTP_TO[8] =  		{0x04,	0x08,	0x01,	0x01,	0xFF,	0xFF,	0xFF,	0xFF};



static Bluetooth_state BT_state = unsynced;
static Bluetooth_condition  BT_condition = normal;
bool BT_connection = false;
uint8_t BT_printer_command[20];
float IntBat_voltage,external_voltage;
uint32_t printer_parameter_flag, printer_parameter_received;
bool Test_Mode_switch = false;
uint16_t idle_counter = 0;

//Battery test printing parameter
static uint8_t valtage_P[4], Measured_CCA_P[4], Rated_CCA_P[4], RC_setting_P[4], RC_result_P[4];
static uint8_t BT_SS_P, SOC_P, SOH_P, BAT_Type_P, Ratting_P, Judge_P, RC_judge_P;

//Infos
static uint8_t Date_P[14], Test_temperature_C[3] , Test_temperature_F[3];
static uint8_t RO_len_P, VIN_len_P, License_len_P, TestCode_len_P;
static uint8_t RO_P[17], VIN_P[17], License_P[17], TestCode_P[17];

//system test parameter
static uint8_t Crank_Vol_P[4], Idle_Vol_P[4], Loaded_Vol_P[4], Ripple_Vol_P[4];
static uint8_t Crank_HL, Idle_HL, Loaded_HL, Ripple_HL;




void BluetoothPreinter_UP_DOWN(){
	if(BT_state == leaving){
		BluetoothPreinter_widget.index ^= 0x01;
		BluetoothPreinter_DISPLAY();
	}
}





void BluetoothPreinter_DISPLAY(){

	ST7565ClearScreen(BUFFER1);
	LANGUAGE LangIdx = get_language_setting();
	set_language(en_US);
	if(BT_state == leaving){
		LcdDrawScreen( 55, BUFFER1);
		BTPrinterAntiWhile(en_US,BluetoothPreinter_widget.index);
	}
	else if(BluetoothConnected){
		LcdDrawScreen( 33, BUFFER1);
	}
	else{
		LcdDrawScreen( 32, BUFFER1);
	}
	set_language(LangIdx);
}

void BluetoothPreinter_Select(){
	if(BT_state != leaving){
		if(!BluetoothConnected){
			BT_state = leaving;
			BluetoothPreinter_DISPLAY();
		}
	}
	else{
		if(BluetoothPreinter_widget.index){
			//turn off BT module
			BT_POWER_OFF;
			BT_RESET_LOW;
			//turn off internal battery
			IntBat_SW_OFF;
			//wait for 0.2 second after enter key is released, if device didn't shutdown transfer to manual test mode
			SelectBtn(0);
			g_BluetoothEnabled = false;
			ROM_SysCtlDelay(get_sys_clk()/3/5);
			PWMEnable();
			Widget_init(manual_test);
		}
		else{
			BT_state = (BluetoothConnected)? synced : unsynced;
			BluetoothPreinter_DISPLAY();
		}
	}
}

void BluetoothPreinter_Service(){
	idle_counter = 0;
	BT_condition = normal;
	set_countdown_callback(BT_Printer_Timer_callback, -1);

	if(BT_state == s_printing){ //if power low occur during printing, continue to print when return from power low
		BT_condition = c_printing;
		star_printer();
		BT_condition = normal;
		if(printer_error & get_system_error())
		{
			clear_system_error(printer_error);
			if(No_paper & get_system_error())
			{
				//
				//  Reset Variables
				//
				clear_system_error(No_paper);
				BT_condition = NoPaper;
				ROM_TimerDisable(TIMER0_BASE, TIMER_A);
				Move_widget(BluetoothPreinter_sub_W[0],1);

			}
			else if(Power_low & get_system_error())
			{
				//
				//  Reset Variables
				//
				clear_system_error(No_paper);
				ROM_TimerDisable(TIMER0_BASE, TIMER_A);
				Move_widget(BluetoothPreinter_sub_W[1],0);
			}
		}
		else{
			BT_state = synced;
			BTUARTSend(e_print_finish,sizeof(e_print_finish));
			if(Test_Mode_switch){
				Test_Mode_switch = false;
				ROM_TimerDisable(TIMER0_BASE, TIMER_A);
				Return_pre_widget();
			}
			else{
				BluetoothPreinter_DISPLAY();
			}
		}
	}
}

void BluetoothPreinter_Routine(){
	uint32_t IntBat_scale;


	BattCoreScanVoltage(&external_voltage);
	if((external_voltage < 7.5)&&(BattCoreChkIntBatt(&IntBat_scale, &IntBat_voltage))){
		BT_condition = PowerLow;
		ROM_TimerDisable(TIMER0_BASE, TIMER_A);
		Move_widget(BluetoothPreinter_sub_W[1], 0);
		return;
	}


	if(BT_connection != BluetoothConnected){
		BT_connection = BluetoothConnected;
		if(!BT_connection){
			BT_state = unsynced;
		}
		else{
			idle_counter = 0;
		}
		BluetoothPreinter_DISPLAY();
	}

	if(BT_connection){
		if(BluetoothPreinter_widget.parameter){
			BT_printer_process();
			BluetoothPreinter_widget.parameter = 0;
			idle_counter = 0;
		}
	}



}

void BluetoothPreinter_Initial(){
	if(!g_BluetoothEnabled){
		IntBat_SW_ON;
		// Turn On Bluetooth
		g_BluetoothEnabled = true;
		BT_POWER_ON;
		BT_RESET_HIGH;
		ROM_SysCtlDelay(get_sys_clk()/3/2);
		BT_state = unsynced;
	}
	Bluetooth_buffer_flush();
	set_BTcommand_callback( BluetoothPreinter_command_callback, BluetoothPreinter_command_match );
	BT_connection = BluetoothConnected;
}


void BluetoothPreinter_command_callback(const char *command){
	uint8_t i;
	char *temp;
	uint16_t ui16DatBuf[6];
	uint32_t time_temp, time_temp_host;


	BTUARTSend((const char*)command,(BT_state != waiting_printer_parameter)? 7 : 20);
	ROM_SysCtlDelay(get_sys_clk()/3/5);

	if(!BluetoothPreinter_widget.parameter){
		BluetoothPreinter_widget.parameter = 1;
		memcpy((char *)BT_printer_command, command , (BT_state != waiting_printer_parameter)? 7 : 20);

		BT_DEBUG("Command : ");
		for(i = 0;i < ((BT_state != waiting_printer_parameter)? 7 : 20);i++){
			BT_DEBUG(" %02x",BT_printer_command[i]);
		}
		BT_DEBUG("\n");
	}


	if((memcmp(command, c_handshake, 2) == 0) && (command[6] == 0xFF)){
		temp = (external_voltage > 7.5)? e_SH_clamped : e_SH_no_clamped;
		temp[4] = BT_condition;
		BTUARTSend(temp, sizeof(e_SH_no_clamped));

		if(memcmp(command +2, c_handshake +2, 4) != 0){
			memcpy(&time_temp,command +2,4);
			time_temp_host = (time_temp<<24) | ((time_temp&0x0000FF00)<<8) | ((time_temp&0x00FF0000)>>8) | (time_temp >> 24);
			ui16DatBuf[0] = (time_temp_host >> 26) + 2000;
			ui16DatBuf[1] = ((time_temp_host >> 22)&0x0F);
			ui16DatBuf[2] = ((time_temp_host >> 17)&0x1F);
			ui16DatBuf[3] = ((time_temp_host >> 12)&0x1F);
			ui16DatBuf[4] = ((time_temp_host >> 6)&0x3F);
			ui16DatBuf[5] = (time_temp_host&0x3F);
			ExtRTCWrite(ui16DatBuf[0], ui16DatBuf[1], ui16DatBuf[2], 0, ui16DatBuf[3], ui16DatBuf[4], ui16DatBuf[5]);
		}
	}

}

bool BluetoothPreinter_command_match(const char *command,uint8_t lenth){
	if(BT_state != waiting_printer_parameter){
		return (lenth == 7);
	}
	else{
		return (lenth == 20);
	}
}




void BT_printer_process(){
	uint8_t *SN;
	uint8_t *SN_len;
	uint8_t i,temp;
	//uint16_t ui16DatBuf[6];
	//uint32_t time_temp, time_temp_host;

	if(BT_state != waiting_printer_parameter){
		/*
		if((memcmp(BT_printer_command, c_handshake, 2) == 0) && (BT_printer_command[6] == 0xFF)){
			BTUARTSend((external_voltage > 7.5)? e_SH_clamped : e_SH_no_clamped, sizeof(e_SH_no_clamped));
			BT_state = synced;
			if(memcmp(BT_printer_command +2, c_handshake +2, 4) != 0){
				memcpy(&time_temp,BT_printer_command +2,4);
				time_temp_host = (time_temp<<24) | ((time_temp&0x0000FF00)<<8) | ((time_temp&0x00FF0000)>>8) | (time_temp >> 24);
				ui16DatBuf[0] = (time_temp_host >> 26) + 2000;
				ui16DatBuf[1] = ((time_temp_host >> 22)&0x0F);
				ui16DatBuf[2] = ((time_temp_host >> 17)&0x1F);
				ui16DatBuf[3] = ((time_temp_host >> 12)&0x1F);
				ui16DatBuf[4] = ((time_temp_host >> 6)&0x3F);
				ui16DatBuf[5] = (time_temp_host&0x3F);
				ExtRTCWrite(ui16DatBuf[0], ui16DatBuf[1], ui16DatBuf[2], 0, ui16DatBuf[3], ui16DatBuf[4], ui16DatBuf[5]);
			}
		}
		else */if((memcmp(BT_printer_command, c_print_para, 2) == 0) && (BT_printer_command[6] == 0xFF)){
			printer_parameter_flag = *(uint32_t*)(BT_printer_command+2);
			printer_parameter_received = 0;
			BT_state = waiting_printer_parameter;
		}
		else if(memcmp(c_print_start,BT_printer_command,sizeof(c_print_start)) == 0){
			BT_state = s_printing;
			BluetoothPreinter_Service();
		}

	}else{
		if(BT_printer_command[0] == 0x22){
			//Battery test parameter
			if((BT_printer_command[1] == 0x01) || (BT_printer_command[1] == 0x02)){
				printer_parameter_received |= (f_BT_voltage|f_BT_CCA|f_BT_SOC|f_BT_SOH|f_BT_judge|f_BT_type|f_INF_date|f_INF_temp);

				BT_SS_P = (BT_printer_command[1] == 0x01)? Battery_Test : Start_Stop_Test;

				valtage_P[0] = (BT_printer_command[2] /10)? (BT_printer_command[2]/10) + 0x30 : 0x20;
				valtage_P[1] = (BT_printer_command[2]%10) + 0x30;
				valtage_P[2] = (BT_printer_command[3]/10) + 0x30;
				valtage_P[3] = (BT_printer_command[3]%10) + 0x30;

				EncodedToSting(Measured_CCA_P, &BT_printer_command[5]);

				EncodedToSting(Rated_CCA_P, &BT_printer_command[10]);

				SOC_P = BT_printer_command[4];
				SOH_P = BT_printer_command[7];
				Judge_P = BT_printer_command[8];

				switch(BT_printer_command[9]/5){
				case 0 :
					BAT_Type_P = FLOODED;
					break;
				case 1 :
					BAT_Type_P = EFB;
					break;
				case 2 :
					BAT_Type_P = VRLA_GEL;
					break;
				case 3 :
					BAT_Type_P = (BT_SS_P == Battery_Test)? AGM_FLAT_PLATE : AGM_FLAT_PLATE;
					break;
				case 4 :
					BAT_Type_P = AGM_SPIRAL;
					break;
				default:
					BAT_Type_P = FLOODED;
				}

				switch(BT_printer_command[9]%5){
				case 0 :
					Ratting_P = CA_MCA;
					break;
				case 1 :
					Ratting_P = SAE;
					break;
				case 2 :
					Ratting_P = EN2;
					break;
				case 3 :
					Ratting_P = DIN;
					break;
				case 4 :
					Ratting_P = IEC;
					break;
				}

				//year
				Date_P[0] = '2';
				Date_P[1] = '0';
				Date_P[2] = BT_printer_command[12]/10 + 0x30;
				Date_P[3] = BT_printer_command[12]%10 + 0x30;
				//month
				Date_P[4] = BT_printer_command[13]/10 + 0x30;
				Date_P[5] = BT_printer_command[13]%10 + 0x30;
				//day
				Date_P[6] = BT_printer_command[14]/10 + 0x30;
				Date_P[7] = BT_printer_command[14]%10 + 0x30;
				//hour
				Date_P[8] = BT_printer_command[15]/10 + 0x30;
				Date_P[9] = BT_printer_command[15]%10 + 0x30;
				//minute
				Date_P[10] = BT_printer_command[16]/10 + 0x30;
				Date_P[11] = BT_printer_command[16]%10 + 0x30;
				//second
				Date_P[12] = BT_printer_command[17]/10 + 0x30;
				Date_P[13] = BT_printer_command[17]%10 + 0x30;

				temp = (BT_printer_command[18] < 20)? (20-BT_printer_command[18]) : (BT_printer_command[18] - 20);
				Test_temperature_C[0] = (BT_printer_command[18] < 20)? '-':
										(temp/100)?  temp/100 + 0x30 : ' ';
				Test_temperature_C[1] = (temp/10)? (temp/10)%10 +0x30 : ' ';
				Test_temperature_C[2] = (temp%10) + 0x30;


				temp = (BT_printer_command[18]*9)/5;

				if(temp < 4){
					Test_temperature_F[0] = ' ';
					Test_temperature_F[1] = '-';
					Test_temperature_F[2] = (4-temp) + 0x30;
				}
				else{
					temp -= 4;
					Test_temperature_F[0] = (temp/100)? temp/100 + 0x30 : ' ';
					Test_temperature_F[1] = (temp/10)? (temp/10)%10 + 0x30:' ';
					Test_temperature_F[2] = (temp%10) + 0x30;
				}



			}
			//system test parameter
			else if(BT_printer_command[1] == 0x03){
				printer_parameter_received |= (f_Sys_idle|f_Sys_loaded|f_Sys_crank|f_Sys_ripple|f_BT_RC);

				EncodedToSting(Crank_Vol_P, &BT_printer_command[2]);
				Crank_HL = BT_printer_command[4];

				EncodedToSting(Idle_Vol_P, &BT_printer_command[5]);
				Idle_HL = BT_printer_command[7];

				EncodedToSting(Loaded_Vol_P, &BT_printer_command[8]);
				Loaded_HL = BT_printer_command[10];

				EncodedToSting(Ripple_Vol_P, &BT_printer_command[11]);
				Ripple_HL = BT_printer_command[13];
				if((Ripple_HL == 3)&&(BT_printer_command[11] == 0x00)&&(BT_printer_command[12] == 0x00)){
					Ripple_HL = 1;
				}

				EncodedToSting(RC_setting_P, &BT_printer_command[14]);

				EncodedToSting(RC_result_P, &BT_printer_command[16]);

				RC_judge_P = BT_printer_command[18];

			}
			//SN numbers
			else{
				switch(BT_printer_command[1]){
				case 0x0E :
					SN = RO_P;
					SN_len = &RO_len_P;
					printer_parameter_received |= f_SN_RO;
					break;
				case 0x0F :
					SN = VIN_P;
					SN_len = &VIN_len_P;
					printer_parameter_received |= f_SN_VIN;
					break;
				case 0x0D :
					SN = License_P;
					SN_len = &License_len_P;
					printer_parameter_received |= f_SN_Licn;
					break;
				case 0x0C :
					SN = TestCode_P;
					SN_len = &TestCode_len_P;
					printer_parameter_received |= (f_INF_Tcode | f_INF_Bcode);
					break;
				default :
					return;
				}
				memset(SN, 0xFF, 17);
				*SN_len = 0;
				for(i=0 ; i<17 ; i++){
					SN[i] = BT_printer_command[2+i];
					if(SN[i] != 0xFF){
						(*SN_len)++;
					}
					else{
						break;
					}
				}

			}

			if((printer_parameter_received & printer_parameter_flag) == printer_parameter_flag){
				//ready to print
				BTUARTSend(e_ready_print, sizeof(e_ready_print));
				BT_state = ready_to_print;
			}

		}
	}


}



void BTPrinterAntiWhile(LANGUAGE LangIdx, uint8_t YesNo)
{
	tRectangle sTemp;

	UARTprintf("MemEraseAntiWhile()\n");

	if(YesNo)
	{

		sTemp.i16YMin = 27;
		sTemp.i16YMax = 35;

 		{
			sTemp.i16XMin = 83;
			sTemp.i16XMax = 103;
		}
	}
	else{

		sTemp.i16YMin = 41;
		sTemp.i16YMax = 49;

 		{
			sTemp.i16XMin = 87;
			sTemp.i16XMax = 103;
		}

	}

	ST7565RectReverse(&sTemp, VERTICAL);
}

void EncodedToSting(uint8_t *des, uint8_t *encode){
	des[0] = (encode[0] /10)? (encode[0]/10) + 0x30 : 0x20;
	des[1] = (encode[0])? (encode[0]%10) + 0x30 : 0x20;
	des[2] = ((encode[0])||(encode[1]/10))? (encode[1]/10) + 0x30 : 0x20;
	des[3] = (encode[1]%10) + 0x30;
}

void star_printer(){
	uint8_t temp, index, PrintAline[24];
	PRINTER_ERR_HANDLE

	ST7565ClearScreen(BUFFER0);
	ST7565Refresh(BUFFER0);
	// Print Animation P1
	LcdDrawScreen( 37, BUFFER0);



	PRINTER_ERR_HANDLE

	if((printer_parameter_flag >> PRINTER_PARA_SHIFTER_BT)&0xFF) {

		PrinterPrintSpace(200);

		LANGUAGE LangIdx = get_language_setting();
		set_language(en_US);
		//print LOGO
		PrinterStart( PRT_LOGO, STB_1_2_3_4_5_6, 1.8, 1, 3);
		//print "Powered by DHC"
		PrinterStart( PRT_PBDHC, STB_ALL, 1.5, 1, 3);
		set_language(LangIdx);

		PrinterPrintSpace(20);

		PRINTER_ERR_HANDLE

		PrinterStart( PRT_TESTREPORT, STB_123_456, 1.5, 1, 5);

		PRINTER_ERR_HANDLE


		//***************************************
		// ==BATTERY TEST== / ==START-STOP TEST==
		//***************************************
		PrinterStart( ( BT_SS_P == Battery_Test )? PRT_BATTERYTEST : PRT_STARTSTOP, STB_ALL, 1.5, 1, 5);

		if(printer_parameter_flag & f_BT_judge){
			//****************
			// Tested result
			//****************
			switch(Judge_P)
			{
			case 0x01:
				temp = PRT_GnP;
				break;
			case 0x02:
				temp = PRT_GnR;
				break;
			case 0x04:
				temp = PRT_BnR;
				break;
			case 0x03:
				temp = PRT_RnR;
				break;
			case 0x05:
				temp = PRT_BCR;
				break;
			}
			PrinterStart( temp, STB_123_456, 2, 1, 7);

		}

		// Print Animation P2
		LcdDrawScreen( 38, BUFFER0);

		PRINTER_ERR_HANDLE

		if(printer_parameter_flag & f_BT_type){
			//****************
			// Battery type
			//****************
			switch(BAT_Type_P){
			case FLOODED :
				temp = PRT_FLOODED;
				break;
			case EFB :
				temp = PRT_EFB;
				break;
			case VRLA_GEL :
				temp = PRT_VRLAGEL;
				break;
			case AGM_FLAT_PLATE :
				temp = ( BT_SS_P == Battery_Test )?  PRT_AGMF:PRT_SSAGMF;
				break;
			case AGM_SPIRAL :
				temp = PRT_AGMS;
				break;
			}
			PrinterStart( temp, STB_123_456, 1.5, 1, 5);
			PRINTER_ERR_HANDLE
		}

		if(printer_parameter_flag & f_BT_voltage){
			memset(PrintAline, ' ', 24);
			PrintAline[16] = valtage_P[0];
			PrintAline[17] = valtage_P[1];
			PrintAline[18] = '.';
			PrintAline[19] = valtage_P[2];
			PrintAline[20] = valtage_P[3];
			PrintAline[22] = 'V';
			PrinterStart( PRT_VOLTAGE, STB_ALL, 1.5, 1, 3);
			PrinterStartWB(PrintAline, STB_ALL, 1.5, 1, 3);
			PrinterPrintSeperate(2);
		}

		if(printer_parameter_flag & f_BT_CCA){
			memset(PrintAline, ' ', 24);
			PrinterStart( PRT_RATED, STB_ALL, 1.5, 1, 3);

			PrintAline[15] = Rated_CCA_P[0];
			PrintAline[16] = Rated_CCA_P[1];
			PrintAline[17] = Rated_CCA_P[2];
			PrintAline[18] = Rated_CCA_P[3];

			switch(Ratting_P){
			case SAE :
			case CA_MCA :
            case MCA:
				PrintAline[20] = 'S';
				PrintAline[21] = 'A';
				PrintAline[22] = 'E';
				break;
			case EN1 :		
				PrintAline[20] = 'E';
				PrintAline[21] = 'N';
				break;
			case EN2 :				
				PrintAline[20] = 'E';
				PrintAline[21] = 'N';
				PrintAline[22] = '2';
				break;
			case DIN :
				PrintAline[20] = 'D';
				PrintAline[21] = 'I';
				PrintAline[22] = 'N';
				break;
			case IEC :
				PrintAline[20] = 'I';
				PrintAline[21] = 'E';
				PrintAline[22] = 'C';
				break;

			}

			PrinterStartWB(PrintAline, STB_ALL, 1.5, 1, 3);
			PrinterPrintSeperate(2);

			//CCA Measured
			PrinterStart( PRT_MEASURED, STB_ALL, 1.5, 1, 3);

			memset(PrintAline, ' ', 24);
			PrintAline[15] = Measured_CCA_P[0];
			PrintAline[16] = Measured_CCA_P[1];
			PrintAline[17] = Measured_CCA_P[2];
			PrintAline[18] = Measured_CCA_P[3];

			PrintAline[20] = 'C';
			PrintAline[21] = 'C';
			PrintAline[22] = 'A';
			PrinterStartWB(PrintAline, STB_ALL, 1.5, 1, 3);
			PrinterPrintSeperate(2);
			PRINTER_ERR_HANDLE
		}

		if(printer_parameter_flag & f_BT_RC){
			memset(PrintAline, ' ', 24);
			PrinterStart( PRT_DIN, STB_ALL, 1.5, 1, 3);
			PrintAline[15] = RC_setting_P[1];
			PrintAline[16] = RC_setting_P[2];
			PrintAline[17] = RC_setting_P[3];

			PrintAline[19] = 'M';
			PrintAline[20] = 'I';
			PrintAline[21] = 'N';
			PrintAline[22] = '.';
			PrinterStartWB(PrintAline, STB_ALL, 1.5, 1, 3);
			PrinterPrintSeperate(2);

			memset(PrintAline, ' ', 24);
			PrinterStart( PRT_EN, STB_ALL, 1.5, 1, 3);

			temp = RC_judge_P; //eliminate RC_judge_P never used
/*
			PrintAline[10] = RC_result_P[1];
			PrintAline[11] = RC_result_P[2];
			PrintAline[12] = RC_result_P[3];

			PrintAline[14] = 'M';
			PrintAline[15] = 'I';
			PrintAline[16] = 'N';
			PrintAline[17] = '.';

			switch(RC_judge_P){
			case 0:
				PrintAline[19] = 'G';
				PrintAline[20] = 'O';
				PrintAline[21] = 'O';
				PrintAline[22] = 'D';
				break;
			case 1:
				PrintAline[20] = 'L';
				PrintAline[21] = 'O';
				PrintAline[22] = 'W';
				break;
			case 2:
				PrintAline[19] = 'P';
				PrintAline[20] = 'O';
				PrintAline[21] = 'O';
				PrintAline[22] = 'R';
				break;
			}
 */
			switch(Judge_P)
			{
			case 0x01:
			case 0x02:
				PrintAline[19] = 'G';
				PrintAline[20] = 'O';
				PrintAline[21] = 'O';
				PrintAline[22] = 'D';
				break;
			case 0x04:
			case 0x05:
				PrintAline[19] = 'P';
				PrintAline[20] = 'O';
				PrintAline[21] = 'O';
				PrintAline[22] = 'R';
				break;
			case 0x07:
			case 0x03:
				PrintAline[20] = 'L';
				PrintAline[21] = 'O';
				PrintAline[22] = 'W';
				break;
			}


			PrinterStartWB(PrintAline, STB_ALL, 1.5, 1, 3);
			PrinterPrintSeperate(2);
		}

		if(printer_parameter_flag & f_INF_temp){
			memset(PrintAline, ' ', 24);
			PrinterStart( PRT_TEMP, STB_ALL, 1.5, 1, 3);

			PrintAline[13] = Test_temperature_F[0];
			PrintAline[14] = Test_temperature_F[1];
			PrintAline[15] = Test_temperature_F[2];
			PrintAline[16] = wb_doF;
			PrintAline[17] = '/';
			PrintAline[18] = ' ';
			PrintAline[19] = Test_temperature_C[0];
			PrintAline[20] = Test_temperature_C[1];
			PrintAline[21] = Test_temperature_C[2];
			PrintAline[22] = wb_doC;
			PrinterPrintSpace(10);
			PrinterStartWB(PrintAline, STB_ALL, 1.5, 1, 3);
			PrinterPrintSeperate(2);
			PrinterPrintSpace(10);
			PRINTER_ERR_HANDLE
		}

		// Print Animation P3
		LcdDrawScreen( 39, BUFFER0);

		if(printer_parameter_flag & f_BT_SOC){
			memset(PrintAline, ' ', 24);
			PrinterStart( PRT_SOC, STB_123_456, 1.5, 1, 3);
			PrinterPrintSpace(10);
			PrintAline[10] = (SOC_P/100)? 0x31 : 0x20;
			PrintAline[11] = (SOC_P/10)? (SOC_P/10)%10 +0x30 : 0x20;
			PrintAline[12] = SOC_P%10 + 0x30;
			PrintAline[13] = '%';
			PrinterStartWB(PrintAline, STB_ALL, 1.5, 1, 3);
			PrinterPrintSpace(10);

			PrinterStart( (PRT_PERCENT_0 + (SOC_P/10)), STB_1_2_3_4_5_6, 2.2, 1, 2);
			PrinterStart( PRT_0PA_100PA, STB_1_2_3_4_5_6, 1.8, 1, 2);
			PRINTER_ERR_HANDLE
		}

		if(printer_parameter_flag & f_BT_SOH){
			memset(PrintAline, ' ', 24);
			PrinterStart( PRT_SOH, STB_123_456, 1.5, 1, 3);
			PrinterPrintSpace(10);
			PrintAline[10] = (SOH_P/100)? 0x31 : 0x20;
			PrintAline[11] = (SOH_P/10)? (SOH_P/10)%10 +0x30 : 0x20;
			PrintAline[12] = SOH_P%10 + 0x30;
			PrintAline[13] = '%';
			PrinterStartWB(PrintAline, STB_ALL, 1.5, 1, 3);
			PrinterPrintSpace(10);

			PrinterStart( (PRT_PERCENT_0 + (SOH_P/10)), STB_1_2_3_4_5_6, 2.2, 1, 2);
			PrinterStart( PRT_0PA_100PA, STB_1_2_3_4_5_6, 1.8, 1, 2);
			PRINTER_ERR_HANDLE
		}

		if(printer_parameter_flag & f_INF_Tcode){
			memset(PrintAline, ' ', 24);
			temp = (24 - TestCode_len_P) / 2;
			for(index = 0 ; index < TestCode_len_P ; index++){
				PrintAline[temp + index] = TestCode_P[index];
			}
			PrinterStartWB(PrintAline, STB_ALL, 1.5, 1, 3);
			PrinterPrintSpace(10);
			if(printer_parameter_flag & f_INF_Bcode){
				PrintBarCode(TestCode_P, STB_1_2_3_4_5_6, 2, 1, 2);
				PrinterPrintSpace(10);
			}
			PRINTER_ERR_HANDLE
		}

		if(printer_parameter_flag & f_SN_RO){
			memset(PrintAline, ' ', 24);
			PrinterStart( PRT_RO, STB_ALL, 1.5, 1, 3);
			for(index = 0 ; index < RO_len_P ; index++){
				PrintAline[22 - index] = RO_P[RO_len_P - index -1];
			}
			PrinterStartWB(PrintAline, STB_ALL, 1.5, 1, 3);
			PrinterPrintSeperate(2);
			PRINTER_ERR_HANDLE
		}

		if(printer_parameter_flag & f_SN_VIN){
			memset(PrintAline, ' ', 24);
			PrinterStart( PRT_VIN, STB_ALL, 1.5, 1, 3);
			for(index = 0 ; index < VIN_len_P ; index++){
				PrintAline[22 - index] = VIN_P[VIN_len_P - index -1];
			}
			PrinterStartWB(PrintAline, STB_ALL, 1.5, 1, 3);
			PrinterPrintSeperate(2);
			PRINTER_ERR_HANDLE
		}



		// Print Animation P4
		LcdDrawScreen( 40, BUFFER0);

		if(printer_parameter_flag & f_INF_date){
			memset(PrintAline, ' ', 24);
			PrinterStart( PRT_TESTDATE, STB_ALL, 1.5, 1, 3);
			PrinterPrintSpace(10);
			PrintAline[4]  =  Date_P[0];
			PrintAline[5]  =  Date_P[1];
			PrintAline[6]  =  Date_P[2];
			PrintAline[7]  =  Date_P[3];
			PrintAline[8]  = '/';
			PrintAline[9]  = Date_P[4];
			PrintAline[10] = Date_P[5];
			PrintAline[11] = '/';
			PrintAline[12] = Date_P[6];
			PrintAline[13] = Date_P[7];
			PrintAline[14] = ' ';
			PrintAline[15] = Date_P[8];
			PrintAline[16] = Date_P[9];
			PrintAline[17] = ':';
			PrintAline[18] = Date_P[10];
			PrintAline[19] = Date_P[11];
			PrintAline[20] = ':';
			PrintAline[21] = Date_P[12];
			PrintAline[22] = Date_P[13];

			PrinterStartWB(PrintAline, STB_ALL, 1.5, 1, 3);
			PrinterPrintSpace(10);

			PRINTER_ERR_HANDLE

		}
		PrinterPrintSpace(200);

	}


	if((printer_parameter_flag >> PRINTER_PARA_SHIFTER_ST)&0xFF) {
		PrinterPrintSpace(200);
		// Print Animation P1
		LcdDrawScreen( 37, BUFFER0);

		LANGUAGE LangIdx = get_language_setting();
		set_language(en_US);
		//print LOGO
		PrinterStart( PRT_LOGO, STB_1_2_3_4_5_6, 1.8, 1, 3);
		//print "Powered by DHC"
		PrinterStart( PRT_PBDHC, STB_ALL, 1.5, 1, 3);
		set_language(LangIdx);

		PrinterPrintSpace(20);

		PRINTER_ERR_HANDLE

		PrinterStart( PRT_TESTREPORT, STB_123_456, 1.5, 1, 5);

		PRINTER_ERR_HANDLE




		if(printer_parameter_flag & f_Sys_crank){
			memset(PrintAline, ' ', 24);

			//print STARTER TEST
			PrinterStart( PRT_STARTERTEST, STB_123_456, 1.5, 1, 3);

			if(Crank_HL == 3){ //no detected
				PrintAline[8] = '-';
				PrintAline[9] = '-';
				PrintAline[10] = '.';
				PrintAline[11] = '-';
				PrintAline[12] = '-';
				PrintAline[13] = 'V';
				temp = PRT_NODETECT;
			}
			else{
				PrintAline[8] = Crank_Vol_P[0];
				PrintAline[9] = Crank_Vol_P[1];
				PrintAline[10] = '.';
				PrintAline[11] = Crank_Vol_P[2];
				PrintAline[12] = Crank_Vol_P[3];
				PrintAline[13] = 'V';
				temp = (Crank_HL == 2)? PRT_NORMAL:PRT_LOW;
			}

			PrinterStartWB(PrintAline, STB_ALL, 1.5, 1, 3);
			PrinterStart( temp, STB_ALL, 1.5, 1, 3);
			PrinterPrintSeperate(2);

			PRINTER_ERR_HANDLE
		}

		// Print Animation P2
		LcdDrawScreen( 38, BUFFER0);

		if(printer_parameter_flag & f_Sys_idle){
			memset(PrintAline, ' ', 24);
			//print  CHARGING TEST
			PrinterStart( PRT_CHARGINGTEST, STB_123_456, 1.5, 1, 3);
			//print  LOAD OFF
			PrinterStart( PRT_LOADOFF, STB_ALL, 1.5, 1, 3);

			PrintAline[8] = Idle_Vol_P[0];
			PrintAline[9] = Idle_Vol_P[1];
			PrintAline[10] = '.';
			PrintAline[11] = Idle_Vol_P[2];
			PrintAline[12] = Idle_Vol_P[3];
			PrintAline[13] = 'V';
			PrinterStartWB(PrintAline, STB_ALL, 1.5, 1, 3);

			switch(Idle_HL){
			case 1:
				temp = PRT_LOW;
				break;
			case 2:
				temp = PRT_NORMAL;
				break;
			case 3:
				temp = PRT_HIGH;
				break;
			}
			PrinterStart( temp, STB_ALL, 1.5, 1, 3);
			PrinterPrintSeperate(2);

			PRINTER_ERR_HANDLE
		}

		// Print Animation P3
		LcdDrawScreen( 39, BUFFER0);

		if(printer_parameter_flag & f_Sys_loaded){
			memset(PrintAline, ' ', 24);
			//print LOAD ON
			PrinterStart( PRT_LOADON, STB_ALL, 1.5, 1, 3);

			PrintAline[8] = Loaded_Vol_P[0];
			PrintAline[9] = Loaded_Vol_P[1];
			PrintAline[10] = '.';
			PrintAline[11] = Loaded_Vol_P[2];
			PrintAline[12] = Loaded_Vol_P[3];
			PrintAline[13] = 'V';
			PrinterStartWB(PrintAline, STB_ALL, 1.5, 1, 3);

			switch(Loaded_HL){
			case 1:
				temp = PRT_LOW;
				break;
			case 2:
				temp = PRT_NORMAL;
				break;
			case 3:
				temp = PRT_HIGH;
				break;
			}
			PrinterStart( temp, STB_ALL, 1.5, 1, 3);
			PrinterPrintSeperate(2);


			PRINTER_ERR_HANDLE
		}

		// Print Animation P4
		LcdDrawScreen( 40, BUFFER0);

		if(printer_parameter_flag & f_Sys_ripple){
			memset(PrintAline, ' ', 24);
			PrinterStart( PRT_DIODERIPPLE, STB_123_456, 1.5, 1, 3);
			if(Ripple_HL == 1){//no detected
				PrintAline[8] = '-';
				PrintAline[9] = '-';
				PrintAline[10] = '.';
				PrintAline[11] = '-';
				PrintAline[12] = '-';
				PrintAline[13] = 'V';
				temp = PRT_NODETECT;
			}
			else{
				PrintAline[8] = Ripple_Vol_P[0];
				PrintAline[9] = Ripple_Vol_P[1];
				PrintAline[10] = '.';
				PrintAline[11] = Ripple_Vol_P[2];
				PrintAline[12] = Ripple_Vol_P[3];
				PrintAline[13] = 'V';
				temp = (Ripple_HL == 2)? PRT_NORMAL:PRT_HIGH;
			}
			PrinterStartWB(PrintAline, STB_ALL, 1.5, 1, 3);
			PrinterStart( temp, STB_ALL, 1.5, 1, 3);
			PrinterPrintSeperate(2);

			PRINTER_ERR_HANDLE
		}

		//for combination test to print out RO/VIN
		if((printer_parameter_flag >> PRINTER_PARA_SHIFTER_BT)&0xFF){
			if(printer_parameter_flag & f_SN_RO){
				memset(PrintAline, ' ', 24);
				PrinterStart( PRT_RO, STB_ALL, 1.5, 1, 3);
				for(index = 0 ; index < RO_len_P ; index++){
					PrintAline[22 - index] = RO_P[RO_len_P - index -1];
				}
				PrinterStartWB(PrintAline, STB_ALL, 1.5, 1, 3);
				PrinterPrintSeperate(2);
				PRINTER_ERR_HANDLE
			}

			if(printer_parameter_flag & f_SN_VIN){
				memset(PrintAline, ' ', 24);
				PrinterStart( PRT_VIN, STB_ALL, 1.5, 1, 3);
				for(index = 0 ; index < VIN_len_P ; index++){
					PrintAline[22 - index] = VIN_P[VIN_len_P - index -1];
				}
				PrinterStartWB(PrintAline, STB_ALL, 1.5, 1, 3);
				PrinterPrintSeperate(2);
				PRINTER_ERR_HANDLE
			}
		}

		if(printer_parameter_flag & f_INF_date){
			memset(PrintAline, ' ', 24);
			PrinterStart( PRT_TESTDATE, STB_ALL, 1.5, 1, 3);
			PrinterPrintSpace(10);
			PrintAline[4]  =  Date_P[0];
			PrintAline[5]  =  Date_P[1];
			PrintAline[6]  =  Date_P[2];
			PrintAline[7]  =  Date_P[3];
			PrintAline[8]  = '/';
			PrintAline[9]  = Date_P[4];
			PrintAline[10] = Date_P[5];
			PrintAline[11] = '/';
			PrintAline[12] = Date_P[6];
			PrintAline[13] = Date_P[7];
			PrintAline[14] = ' ';
			PrintAline[15] = Date_P[8];
			PrintAline[16] = Date_P[9];
			PrintAline[17] = ':';
			PrintAline[18] = Date_P[10];
			PrintAline[19] = Date_P[11];
			PrintAline[20] = ':';
			PrintAline[21] = Date_P[12];
			PrintAline[22] = Date_P[13];

			PrinterStartWB(PrintAline, STB_ALL, 1.5, 1, 3);
			PrinterPrintSpace(10);

			PRINTER_ERR_HANDLE
		}

		PrinterPrintSpace(200);

	}
}

void BT_Printer_Timer_callback(){
	static uint8_t blink_toggle = true;

	if(idle_counter < 1000)idle_counter++;

	if((external_voltage < 7.5 && idle_counter > 120)||(idle_counter > 600)){
		if(BluetoothConnected){
			e_BTP_TO[2] = (external_voltage < 7.5)? 0x02 : 0x01;
			BTUARTSend(e_BTP_TO, sizeof(e_BTP_TO));
			ROM_SysCtlDelay(get_sys_clk()/3/1000);
			while(BluetoothConnected);
		}
		//ROM_TimerDisable(TIMER0_BASE, TIMER_A);
		IntBat_SW_OFF;
		PWMDisable();
	}
	else{
		if(!BluetoothConnected && BT_state != s_printing){
		if(blink_toggle) PWMDisable();
		else 	   PWMEnable();
		blink_toggle = !blink_toggle;
	}
	else{
		PWMEnable();
	}
}
}

void SwitchToPrinterMode(uint32_t flag){
	printer_parameter_flag = flag;
	printer_parameter_received = 0;
	BT_state = waiting_printer_parameter;
	Test_Mode_switch = true;
	Move_widget(BluetoothPreinter_sub_W[2],0);
}

