//*****************************************************************************
//
// Source File: TestCounterConfirmWidget.c
// Description: Information's Virtual Keyboard widget including Uppercase/Lowercase
//				letters , Numeric and Symbol keyboards.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 09/26/17 	Jerry.W      	Created file.
// 11/06/17     Henry.Y        1. Add defined symbol "NEUTRAL" in TestCounterPrinting() for no logo version.
// 09/11/18     Henry.Y        Use system error flag to replace the global variables.
// 11/19/18     Henry.Y        Add warning page before printing. Add low temperature(<5C) heater setup.
// 12/07/18     Henry.Y        New display and printing api.
// 09/06/20     David.Wang     SYS TEST & IR TEST NO USED TO MASK
// 09/10/20     David.Wang     Add clear ask yes/no register
// 10/20/20     David Wang     Enlarged print fonts
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/app/TestCounterConfirmWidget.c#2 $
// $DateTime: 2017/09/26 17:06:17 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#include "Widgetmgr.h"

void TestCounterPrinting();
void TestCounterConfirm_LEFT();
void TestCounterConfirm_RIGHT();
void TestCounterConfirm_SELECT();
void TestCounterConfirm_display();
void TestCounterConfirm_initial();

enum{
    Option_Print,
    Option_Reset,
    Option_back,

    Option_number,
};


widget* TestCounterConfirm_sub_W[] = {
		&NoPaper_widget,
		&ChkIntBat_widget,
		&CheckClamps_widget,
		&InsertPaper_widget,
		&ResetCounter_widget
};

widget TestCounterConfirm_widget = {
		.Key_UP = 0,
		.Key_DOWN = 0,
		.Key_RIGHT = TestCounterConfirm_RIGHT,
		.Key_LEFT = TestCounterConfirm_LEFT,
		.Key_SELECT = TestCounterConfirm_SELECT,
		.routine = 0,
		.service = 0,
		.initial = TestCounterConfirm_initial,
		.display = TestCounterConfirm_display,
		.subwidget = TestCounterConfirm_sub_W,
		.number_of_widget = sizeof(TestCounterConfirm_sub_W)/4,
		.index = 0,
		.name = "TestCounterConfirm",
};

static bool wait_insert_paper;

void TestCounterConfirm_initial(){
    TEST_DATA* TD = Get_test_data();

	TestCounterConfirm_widget.index = 0;

    /*
    //BA427+ is ok to print in 24V
	if(TD->SYSTEM_CHANNEL == MAP_24V){
	    Move_widget_untracked(TestCounterConfirm_sub_W[4],0);
	}
	*/
	wait_insert_paper = false;
}


void TestCounterConfirm_LEFT(){
    if(TestCounterConfirm_widget.parameter != 0) return;

    if(TestCounterConfirm_widget.index != 0){
        TestCounterConfirm_widget.index--;
    }
    else{
        TestCounterConfirm_widget.index = Option_number-1;
    }
    TestCounterConfirm_display();
}

void TestCounterConfirm_RIGHT(){
    if(TestCounterConfirm_widget.parameter != 0) return;

    TestCounterConfirm_widget.index++;
    TestCounterConfirm_widget.index %= Option_number;
    TestCounterConfirm_display();
}

void TestCounterConfirm_SELECT(){
    SYS_INFO* info = Get_system_info();

    if(TestCounterConfirm_widget.parameter != 0){
        Return_pre_widget();
        return;
    }

    switch(TestCounterConfirm_widget.index){
    case Option_Print:

        if(!MP205CoSensorDetect()){ //no paper
            Move_widget(TestCounterConfirm_sub_W[3],0);
            wait_insert_paper = true;
            return;
        }

        TestCounterPrinting();


        if(printer_error & get_system_error()){
            clear_system_error(printer_error);
            if(No_paper & get_system_error())
            {
                clear_system_error(No_paper);
                Move_widget_untracked(TestCounterConfirm_sub_W[0],0);
                return;
            }
            else
            {
                Widget_init(manual_test);
            }
        }
        else{
            TestCounterConfirm_widget.parameter = 1;
            TestCounterConfirm_display();
        }
        break;

    case Option_Reset:
        Move_widget_untracked(TestCounterConfirm_sub_W[4],0);
        break;

	case Option_back:
	    Return_pre_widget();
	    break;

	}
}

void TestCounterConfirm_display(){
    ST7565ClearScreen(BUFFER1);
    ST7565Refresh(BUFFER1);

    if(wait_insert_paper){
        wait_insert_paper = false;
        if(MP205CoSensorDetect()){
            TestCounterConfirm_SELECT();
        }
        else{
            Return_pre_widget();
        }
        return;
    }

    if(TestCounterConfirm_widget.parameter != 0){
        LCD_String_ext(16,get_Language_String(S_FINISHED) , Align_Central);
        LCD_String_ext(32,get_Language_String(S_PRESS_ENTER) , Align_Central);
        LCD_Arrow(Arrow_enter);
        return;
    }

    LCD_Arrow(Arrow_enter | Arrow_RL);

    LCD_String_ext(16,get_Language_String(S_PRINT_OR_RESET) , Align_Central);
	switch(TestCounterConfirm_widget.index){
	case Option_Print:
	    LCD_String_ext(32,get_Language_String(S_PRINT) , Align_Central);
	    break;
	case Option_Reset:
	    LCD_String_ext(32,get_Language_String(S_RESET_CLEAR) , Align_Central);
        break;
	case Option_back:
	    LCD_String_ext(32,get_Language_String(S_BACK_TO_MANUAL) , Align_Central);
        break;
	}
}


void TestCounterPrinting()
{

	//****************************
	// Printout Test Time Handler
	//****************************
	uint16_t year;
	uint8_t  month;
	uint8_t  date;
	uint8_t  day;
	uint8_t  hour;
	uint8_t  min;
	uint8_t  sec;

	char buf[25];

	LCD_display(PRINTING, 0);


	SYS_INFO* info = Get_system_info();

	ExtRTCRead( &year, &month, &date, &day, &hour, &min, &sec);


	if(PrintErrorHanlder())
	{
	    return;
	}

	PrinterPrintSpace(20);

	Printer_string(get_Language_String(S_COUNTER), 4, Printe_align_central);

	PrinterPrintSpace(10);

	Printer_Dashed_line(64, 384-64);

	PrinterPrintSpace(50);

	/*  no SS test.
	memset(buf, ' ', sizeof(buf));

	sprintf(buf+4, "%s", get_Language_String(S_SS_TEST));
	sprintf(buf+strlen(buf), "%*d", 19-strlen(buf) , info->SSTestCounter);
	Printer_string(buf, 3, Printe_align_left);
	 */
	LCD_display(PRINTING, 1);

	PrinterPrintSpace(30);

	if(PrintErrorHanlder())
	{
	    return;
	}

	memset(buf, ' ', sizeof(buf));

	sprintf(buf+4, "%s", get_Language_String(S_BAT_TEST));
	sprintf(buf+strlen(buf), "%*d", 19-strlen(buf) , info->BatTestCounter);
	Printer_string(buf, 3, Printe_align_left);

	PrinterPrintSpace(30);

	if(PrintErrorHanlder())
	{
	    return;
	}

	memset(buf, ' ', sizeof(buf));

	sprintf(buf+4, "%s", get_Language_String(S_SYS_TEST));
	sprintf(buf+strlen(buf), "%*d", 19-strlen(buf) , info->SysTestCounter);
	Printer_string(buf, 3, Printe_align_left);

	PrinterPrintSpace(20);

	LCD_display(PRINTING, 2);

	if(PrintErrorHanlder())
	{
	    return;
	}

	memset(buf, ' ', sizeof(buf));

	sprintf(buf+4, "%s", get_Language_String(S_TEST_DATE));
	Printer_string(buf, 3, Printe_align_left);

	PrinterPrintSpace(20);

	if(PrintErrorHanlder())
	{
	    return;
	}

	memset(buf, ' ', sizeof(buf));

	sprintf(buf+6, "%04d/%02d/%02d", year, month, date);
	Printer_string(buf, 3, Printe_align_left);

	PrinterPrintSpace(20);

	if(PrintErrorHanlder())
	{
	    return;
	}

	memset(buf, ' ', sizeof(buf));

	sprintf(buf+8, "%02d:%02d:%02d", hour, min, sec);
	Printer_string(buf, 3, Printe_align_left);

	LCD_display(PRINTING, 3);

	PrinterPrintSpace(250);
}

