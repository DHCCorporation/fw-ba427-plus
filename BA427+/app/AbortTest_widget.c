//*****************************************************************************
//
// Source File: AbortTest_widget.c
// Description: Abort test?
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 09/05/18     Henry.Y        Created file.
// 12/07/18     Henry.Y        New display api.
// =============================================================================
//
//
// $Header$
// $DateTime$
// $Author$
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#include "Widgetmgr.h"


void AbortTest_SELECT();
void AbortTest_UP_DOWN();
void AbortTest_display();



widget* AbortTest_sub_W[] = {
};

widget AbortTest_widget = {
		.Key_UP = AbortTest_UP_DOWN,
		.Key_DOWN = AbortTest_UP_DOWN,
		.Key_RIGHT = 0,
		.Key_LEFT = 0,
		.Key_SELECT = AbortTest_SELECT,
		.routine = 0,
		.service = 0,
		.display = AbortTest_display,
		.initial = 0,
		.subwidget = AbortTest_sub_W,
		.number_of_widget = sizeof(AbortTest_sub_W)/4,
		.index = 0,
		.name = "AbortTest_widget",
};

void AbortTest_SELECT(){

    TEST_DATA* TD = Get_test_data();                       // 20200819 DW EDIT
    SYS_PAR* par = Get_system_parameter();                 // 20200819 DW EDIT

	if(AbortTest_widget.parameter){

	    if( TD->PackTestNum > 1 ){

	        TD->PackTestNum = 1;                                                  // clear
	        TD->PackTestCount = 0;                                                // clear
	        par->BAT_test_par.PackTestNum = TD->PackTestNum;                      // save to eeprom
	        par->BAT_test_par.PackTestCount = TD->PackTestCount;                  // save to eeprom
	        save_system_parameter(&par->BAT_test_par, sizeof(par->BAT_test_par)); // save to eeprom
	    }

		Widget_init(manual_test);

	}
	else{
		Return_pre_widget();
	}
}


void AbortTest_UP_DOWN(){
	AbortTest_widget.parameter^=0x01;
	AbortTest_display();

}



void AbortTest_display(){
	LCD_display(ABORT, AbortTest_widget.parameter^0x01);
}







