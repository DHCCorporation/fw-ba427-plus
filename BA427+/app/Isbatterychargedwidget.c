//*****************************************************************************
//
// Source File: IsBatteryChargedWidget.c
// Description: Asking battery condition for judgement of Battery and start-stop test.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 09/21/17 	Jerry.W	       Created file.
// 09/21/17 	Jerry.W		   Rewrite Widget for new software architecture
// 09/11/18     Henry.Y        Add IsBatteryCharged_LEFT.
// 12/07/18     Henry.Y        New display api.
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/app/Isbatterychargedwidget.c#6 $
// $DateTime: 2017/09/22 11:18:49 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#include "Widgetmgr.h"


void IsBatteryCharged_DISPLAY();
void IsBatteryCharged_LEFT_RIGHT();
void IsBatteryCharged_SELECT();


widget* IsBatteryCharged_sub_W[] = {
		&DisplayResult_widget
};

widget IsBatteryCharged_widget = {
		.Key_UP = 0,
		.Key_DOWN = 0,
		.Key_RIGHT =IsBatteryCharged_LEFT_RIGHT,
		.Key_LEFT = IsBatteryCharged_LEFT_RIGHT,
		.Key_SELECT = IsBatteryCharged_SELECT,
		.routine = 0,
		.service = 0,
		.display = IsBatteryCharged_DISPLAY,
		.subwidget = IsBatteryCharged_sub_W,
		.number_of_widget = sizeof(IsBatteryCharged_sub_W)/4,
		.index = 0,
		.name = "IsBatteryCharged",
};


void IsBatteryCharged_DISPLAY(){

    char* YN;
    // Clear screen
    ST7565ClearScreen(BUFFER1);
    ST7565Refresh(BUFFER1);
    // Write SCREEN
    LCD_String_ext(16,get_Language_String(S_BATTERY_CHARGED) , Align_Left);
    // yes/no
    if(IsBatteryCharged_widget.parameter == 1){
        YN = get_Language_String(S_YES);
    }
    else{
        YN = get_Language_String(S_NO);
    }
    LCD_String_ext2(32, YN, Align_Right, 2);
    LCD_Arrow(Arrow_enter | Arrow_RL);
}

void IsBatteryCharged_LEFT_RIGHT(){
	IsBatteryCharged_widget.parameter ^= 0x01;
	IsBatteryCharged_DISPLAY();
}



void IsBatteryCharged_SELECT(){
	TEST_DATA* TD = Get_test_data();
	TD->Judge_Result = (IsBatteryCharged_widget.parameter)? TR_BADnREPLACE:TR_RECHARGEnRETEST;
	TD->IsBatteryCharged = IsBatteryCharged_widget.parameter;

	Move_widget(IsBatteryCharged_sub_W[0],0);
}


