/*
 * Check_load_error_widget.c
 *
 *  Created on: 2020年12月23日
 *      Author: ray.yuan
 */

#include "Widgetmgr.h"

void ChkLoadErr_DISPLAY();
void ChkLoadErr_Service();

widget* ChkLoadErr_sub_W[] = {

};

widget ChkLoadErr_widget = {
        .Key_UP = 0,
        .Key_DOWN = 0,
        .Key_RIGHT = 0,
        .Key_LEFT = 0,
        .Key_SELECT = 0,
        .routine = 0,
        .service = ChkLoadErr_Service,
        .initial = 0,
        .display = ChkLoadErr_DISPLAY,
        .subwidget = ChkLoadErr_sub_W,
        .number_of_widget = sizeof(ChkLoadErr_sub_W)/4,
        .index = 0,
        .no_messgae = 1,
        .name = "ChkLoadErr",
};

void ChkLoadErr_DISPLAY(){
    // Clear screen
    ST7565ClearScreen(BUFFER1);
    ST7565Refresh(BUFFER1);
    // Write SCREEN
    LCD_String_ext(16,get_Language_String(S_CABLE_ERROR) , Align_Central);
    LCD_String_ext(32,get_Language_String(S_REPLACE_CABLE) , Align_Central);
}

void ChkLoadErr_Service(){
    uint8_t block = 1;

       wifi_config(false, NULL);

       while(block){}
}


