//*****************************************************************************
//
// Source File: Display_PackTestAllResult_widget.c
// Description: show all pack bat test result
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 8/19/20      David.Wang     Created file.
//
//
// =============================================================================
//
//
// $Header:   $
// $DateTime: $
// $Author:   $
//
//
// Copyright (c) 2020 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#include "Widgetmgr.h"

void Display_PackTestAllResult_DISPLAY();
void Display_PackTestAllResult_UP();
void Display_PackTestAllResult_DOWN();
void Display_PackTestAllResult_SELECT();
void Display_PackTestAllResult_LEFT();
void Display_PackTestAllResult_Init();

widget* Display_PackTestAllResult_sub_W[] = {
       &Pack_Test_Advised_widget,   //0               // 20200826 dw add advised action widget
       &AbortTest_widget,           //1               // GIVE UP HD PACK TEST FLOW
};

widget Display_PackTestAllResult_widget = {
        .Key_UP = Display_PackTestAllResult_UP,
        .Key_DOWN = Display_PackTestAllResult_DOWN,
        .Key_RIGHT = 0,
        .Key_LEFT = Display_PackTestAllResult_LEFT,
        .Key_SELECT = Display_PackTestAllResult_SELECT,
        .routine = 0,
        .service = 0,
        .initial = Display_PackTestAllResult_Init,
        .display = Display_PackTestAllResult_DISPLAY,
        .subwidget = Display_PackTestAllResult_sub_W,
        .number_of_widget = sizeof(Display_PackTestAllResult_sub_W)/4,
        .index = 0,
        .no_messgae = 1,
        .name = "Display_PackTestAllResult",
};

void Display_PackTestAllResult_Init(){
    uint8_t ui8RecordBuf[128] = {0x00};
    uint16_t ui16TestRecordIdx = 0;
    uint32_t ui32TestRecord = 0;
    uint32_t PackTestRecord = 0;
    SYS_INFO* info = Get_system_info();            // read TotalTestRecord
    TEST_DATA* TD = Get_test_data();

    Display_PackTestAllResult_widget.index = 1;    // init show bat num register

    //***** Read the first battery data *****//
    //Read test record counter
    ui32TestRecord = info->TotalTestRecord;
    ui16TestRecordIdx = info->TestRecordOffset;
    //Read the desired battery test data
    PackTestRecord = ((ui32TestRecord + ui16TestRecordIdx) % MAX_BTSSTESTCOUNTER) - ( TD->PackTestNum - Display_PackTestAllResult_widget.index + 1 );
    TestRecordRead(PackTestRecord, ui8RecordBuf);
    //Put the test result TO display
    BATTERY_SARTSTOP_RECORD_T* BT_rec = (BATTERY_SARTSTOP_RECORD_T*)ui8RecordBuf;
    TD->Judge_Result = BT_rec->Judgement;
}

void Display_PackTestAllResult_LEFT(){
    Move_widget(Display_PackTestAllResult_sub_W[1],0); // RETURN TO MainMenu
}


void Display_PackTestAllResult_DISPLAY(){

   LCD_display(Pack_Report, 0);                                          // Title
   LCD_draw_string(BAT_NUM, Display_PackTestAllResult_widget.index, 15); // bat num
   LCD_display(BT_PACK_TEST_RESULT,0);                                     // this bat map result
}

void Display_PackTestAllResult_UP(){
    uint8_t ui8RecordBuf[128] = {0x00};
    uint16_t ui16TestRecordIdx = 0;
    uint32_t ui32TestRecord = 0;
    uint32_t PackTestRecord = 0;
    SYS_INFO* info = Get_system_info();            // read TotalTestReco
    TEST_DATA* TD = Get_test_data();

    if ( Display_PackTestAllResult_widget.index >= TD->PackTestNum ){
        Display_PackTestAllResult_widget.index = 1;                  // num >�@1
    }
    else{
        Display_PackTestAllResult_widget.index++;
    }

    //***** Read the first battery data *****//
    //Read test record counter
    ui32TestRecord = info->TotalTestRecord;
    ui16TestRecordIdx = info->TestRecordOffset;

    //Read the desired battery test data
    PackTestRecord = ((ui32TestRecord + ui16TestRecordIdx) % MAX_BTSSTESTCOUNTER) - ( TD->PackTestNum - Display_PackTestAllResult_widget.index + 1 );
    TestRecordRead(PackTestRecord, ui8RecordBuf);

    //Put the test result TO display
    BATTERY_SARTSTOP_RECORD_T* BT_rec = (BATTERY_SARTSTOP_RECORD_T*)ui8RecordBuf;
    TD->Judge_Result = BT_rec->Judgement;

    Display_PackTestAllResult_DISPLAY();
}

void Display_PackTestAllResult_DOWN(){
    uint8_t ui8RecordBuf[128] = {0x00};
    uint16_t ui16TestRecordIdx = 0;
    uint32_t ui32TestRecord = 0;
    uint32_t PackTestRecord = 0;
    SYS_INFO* info = Get_system_info();            // read TotalTestReco
    TEST_DATA* TD = Get_test_data();

    if ( Display_PackTestAllResult_widget.index <= 1 ){
        Display_PackTestAllResult_widget.index = TD->PackTestNum;                  // 1 > num
    }
    else{
        Display_PackTestAllResult_widget.index--;
    }

    //***** Read the first battery data *****//
    //Read test record counter
    ui32TestRecord = info->TotalTestRecord;
    ui16TestRecordIdx = info->TestRecordOffset;

    //Read the desired battery test data
    PackTestRecord = ((ui32TestRecord + ui16TestRecordIdx) % MAX_BTSSTESTCOUNTER) - ( TD->PackTestNum - Display_PackTestAllResult_widget.index + 1 );
    TestRecordRead(PackTestRecord, ui8RecordBuf);

    //Put the test result TO display
    BATTERY_SARTSTOP_RECORD_T* BT_rec = (BATTERY_SARTSTOP_RECORD_T*)ui8RecordBuf;
    TD->Judge_Result = BT_rec->Judgement;

    Display_PackTestAllResult_DISPLAY();
}

void Display_PackTestAllResult_SELECT(){

    Move_widget(Display_PackTestAllResult_sub_W[0],0);

}







