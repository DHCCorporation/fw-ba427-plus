//*****************************************************************************
//
// Source File: TestCode_widget.c
// Description: show test code
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 20201203     David Wang     Created file.
//
//
// =============================================================================
//
//
// $Header:   $
// $DateTime: $
// $Author:   $
//
//
// Copyright (c) 2020 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#include "Widgetmgr.h"

void Test_Code_DISPLAY();
void Test_Code_SELECT();

widget* Test_Code_sub_W[] = {
        &PrintResult_widget, //RO_widget,
};

widget Test_Code_widget = {
        .Key_UP = 0,
        .Key_DOWN = 0,
        .Key_RIGHT = 0,
        .Key_LEFT = 0,
        .Key_SELECT = Test_Code_SELECT,
        .routine = 0,
        .service = 0,
        .initial = 0,
        .display = Test_Code_DISPLAY,
        .subwidget = Test_Code_sub_W,
        .number_of_widget = sizeof(Test_Code_sub_W)/4,
        .index = 0,
        .name = "TestCode",
};


void Test_Code_DISPLAY(){
    TEST_DATA* TD = Get_test_data();

    ST7565ClearScreen(BUFFER1);
    ST7565Refresh(BUFFER1);

    LCD_String_ext(16, get_Language_String(S_CODE), Align_Central);
    char buf[sizeof(TD->TestCode)+1] = {0};
    memcpy(buf, TD->TestCode, sizeof(TD->TestCode));
    LCD_String_ext(32, buf/*TD->TestCode*/, Align_Central);
    LCD_Arrow(Arrow_enter);

}

void Test_Code_SELECT(){

     Move_widget(Test_Code_sub_W[0],0);

}

