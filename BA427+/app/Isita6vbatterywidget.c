//*****************************************************************************
//
// Source File: Isita6vbatterywidget.c
// Description: Is a 6V battery widget to question Yes/No condition. 
//				If no, system will assign 12V channel and go.
//				If yes, system will assign 6V channel and go.
//
//
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 10/22/15     Henry.Y	      Created file.
// 11/04/15     Vincent.T     1.#include "mapsvc.h"
//                            2.SYSTEM_CHANNEL refer to VOL_CH
// 03/25/16     Vincent.T     1. Modify IsIta6VBatteryClick(); to handle multination language case.
//                            2. New global variable g_LangSelectIdx to get language index.
//                            3. Modify IsIta6VBatteryAntiWhiteItem(); to handle multination language case.
// 04/11/16     Vincent.T     1. global variable WHICH_TEST to decide Start-Stop test or Internal Resistance Test
//                            2. #include "..\service/testrecordsvc.h"
// 11/15/16     Ian.C         1. Modify IsIta6VBatteryAntiWhiteItem(); for multi-language.
// 01/16/17     Vincnet.T     1. Modify IsIta6VBatteryAntiWhiteItem(); for 16x16 characters.
// 09/21/17 	Jerry.W		  Rewrite Widget for new software architecture
// 02/14/18     Henry.Y	      modified 6V battery judge flow.   
// 09/11/18     Henry.Y       Add IsIta6VBattery_Key_LEFT.
// 12/07/18     Henry.Y       New display api.
// 09/10/20     David.Wang    Left back to widget bug modify
// 09/17/20     David.Wang    Restore the selected yes/no state of the last test
// 20201203     D.W.          SCREEN MODIFY FOR BT2010_BT2200
//
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/app/Isita6vbatterywidget.c#6 $
// $DateTime: 2017/09/22 11:19:05 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#include "Widgetmgr.h"

void IsIta6VBattery_SELECT();
void IsIta6VBattery_LEFT_RIGHT();
void IsIta6VBattery_display();
void IsIta6VBattery_initial();
void IsIta6VBattery_service();

widget* IsIta6VBattery_sub_W[] = {
		&BatterySel_widget,
		&CheckRipple_widget,
};

widget IsIta6VBattery_widget = {
		.Key_UP = 0,
		.Key_DOWN = 0,
		.Key_RIGHT = IsIta6VBattery_LEFT_RIGHT,
		.Key_LEFT = IsIta6VBattery_LEFT_RIGHT,
		.Key_SELECT = IsIta6VBattery_SELECT,
		.routine = 0,
		.service = IsIta6VBattery_service,
		.display = IsIta6VBattery_display,
		.initial = IsIta6VBattery_initial,
		.subwidget = IsIta6VBattery_sub_W,
		.number_of_widget = sizeof(IsIta6VBattery_sub_W)/4,
		.index = 0,
		.name = "IsIta6VBattery",
};

void IsIta6VBattery_initial(){
    if(IsIta6VBattery_widget.index) return;
	SYS_PAR* par = Get_system_parameter();
    TEST_DATA* TD = Get_test_data();
    if(TD->SYSTEM_CHANNEL != MAP_6V){
        TD->Isa6VBattery = 0;
        IsIta6VBattery_widget.index = 1;
    }
    IsIta6VBattery_widget.parameter = 1;
}

void IsIta6VBattery_display(){
    if(IsIta6VBattery_widget.index) return;
    char* YN;

    ST7565ClearScreen(BUFFER1);
    ST7565Refresh(BUFFER1);

    LCD_String_ext(16,get_Language_String(S_IS_IT_A_6V_BATT_1) , Align_Left);
    LCD_String_ext(32,get_Language_String(S_IS_IT_A_6V_BATT_2) , Align_Left);
    if(IsIta6VBattery_widget.parameter == 1){
        YN = get_Language_String(S_YES);
    }
    else{
        YN = get_Language_String(S_NO);
    }
    LCD_String_ext2(48,YN , Align_Right, 2);
    LCD_Arrow(Arrow_enter | Arrow_RL );
}

void IsIta6VBattery_service()
{
    if(!IsIta6VBattery_widget.index) return;
    TEST_DATA* TD = Get_test_data();
	SYS_PAR* par = Get_system_parameter();
    if(par->WHICH_TEST == System_Test){
        if(TD->Isa6VBattery){
            Return_pre_widget();
        }
        else{
            if(TD->SYSTEM_CHANNEL == MAP_6V) TD->SYSTEM_CHANNEL = MAP_12V;
            Move_widget(IsIta6VBattery_sub_W[1], 0);
        }
    }
    else{
        TD->SYSTEM_CHANNEL = (TD->Isa6VBattery)? MAP_6V:MAP_12V;
        Move_widget(IsIta6VBattery_sub_W[0], 0);
    }

}

void IsIta6VBattery_LEFT_RIGHT(){
	IsIta6VBattery_widget.parameter ^= 0x01;
	IsIta6VBattery_display();
}

void IsIta6VBattery_SELECT(){
	TEST_DATA* TD = Get_test_data();
	SYS_PAR* par = Get_system_parameter();
	TD->Isa6VBattery = IsIta6VBattery_widget.parameter;

    IsIta6VBattery_widget.index = true;
    IsIta6VBattery_service();
}

