//*****************************************************************************
//
// Source File: TestCounterwidget.c
// Description: Test Counter widget including Click(Push to new menu entry
//				to stack)/Left(Pop out stack)/Up/Down function body.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 01/04/16     Vincent.T      Created file.
// 01/05/16     Vincent.T      1. Add Test counter: SS./Bat./Sys.
// 02/19/16     Vincent.T      1. Add animation.
// 03/07/16     Vincent.T      1. Add PRINT item in TESTCOUNTERTYPE.
//                             2. #include "..\service/printersvc.h"
// 03/08/16     Vincent.T      1. Add func. to detect any error event when printing.
// 03/16/16     Vincent.T     1. Modify some function input paremeters. (see printersvc.c/.h)
// 03/25/16     Vincent.T      1. Modify TestCounterShow(); to handle multination language case.
//                             2. Modify TestCounterAntiWhiteItem(); to handle multination language case.
//                             3. Modify TestCounterLeft(); to handle multination language case.
//                             4. Modify TestCounterAntiWhiteItem(); to handle multination language case.
//                             5. Modify TestCounterClick(); to handle multination language case.
//                             6. Modify TestCounterConfirmLeft(); to handle multination language case.
// 04/06/16     Vincent.T      1. Modify TestCounterConfirmClick(); to handle multination language case.
// 04/11/16     Vincent.T      1. Modify TestCounterLeft(); due to move "Test Counter" in Tools.
//                             2. Global variable g_SettingMenuItem due to move "Test Counter" in Tools.
//                             3. #include "Settingmenuwidget.h"
// 06/14/16     Vincent.T      1. Modify all the heating time and step motor time.(for low voltage drive)
// 06/21/16     Vincent.T      1. Modify all printing setting.(same as BT. result, Sys. result, SS. result and IR result)
// 06/24/16     Vincent.T      1. Modify printing setting.(STB interval, logo height)
// 07/04/16     Vincent.T      1. Modify all heating time 2ms -> 1.8ms
// 08/10/16     Vincent.T      1. modify all printer setting, ex: heating time, STB group, step-motor control and printout length.
// 09/06/16     Vincent.T      1. Add printout content of "==TEST COUNTER"
//                             2. More space following "By"
// 11/15/16     Ian.C          1. Modify TestCounterConfirmAntiWhite(); for multi-language.
// 01/13/17     Vincent.T      1. global variable gui16IRTestCounter;
//                             2. Modify TestCounterAntiWhiteItem();, TestCounterShow() and TestCounterConfirmClick(); duw to add IR Test Counter
// 01/19/17     Vincent.T      1. global variable g_InfVkybdTextEditStr;
//                             2. Modify TestCounterAntiWhiteItem(); for 16x16 characters
//                             3. Modify TestCounterShow(); due to save IR Test Records.
// 02/10/17     Vincent.T      1. Modify TestCounterAntiWhiteItem(); to AntiWhite correct region.
// 09/26/17 	Jerry.W		   Rewrite Widget for new software architecture
// 09/11/18     Henry.Y        Use systeminfo data struct to replace the global variables.
// 11/19/18     Henry.Y        Move enum{...}TESTCOUNTERTYPE to testrecordsvc.h.
// 12/07/18     Henry.Y        New display api.
// 09/06/20     David.Wang     big id 16652/16655/16656 fix
//
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/app/TestCounterwidget.c#6 $
// $DateTime: 2017/09/26 17:09:51 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#include "Widgetmgr.h"


void TestCounterShow( uint8_t numb);
void TestCounterAntiWhiteItem(  TESTCOUNTERTYPE type);


void TestCounter_SELECT();
void TestCounter_display();


widget* TestCounter_sub_W[] = {
		&TestCounterConfirm_widget
};

widget TestCounter_widget = {
		.Key_UP = 0,
		.Key_DOWN = 0,
		.Key_RIGHT = 0,
		.Key_LEFT = 0,
		.Key_SELECT = TestCounter_SELECT,
		.routine = 0,
		.service = 0,
		.initial = 0,
		.display = TestCounter_display,
		.subwidget = TestCounter_sub_W,
		.number_of_widget = sizeof(TestCounter_sub_W)/4,
		.index = 0,
		.name = "TestCounter",
};


void TestCounter_SELECT(){
    Move_widget_untracked(TestCounter_sub_W[0],TestCounter_widget.parameter);
}

void TestCounter_display(){
    SYS_INFO* inf = Get_system_info();
    char buf[30];

    ST7565ClearScreen(BUFFER1);
    ST7565Refresh(BUFFER1);

    sprintf(buf, "%s", get_Language_String(S_BAT_TEST));
    sprintf(buf+strlen(buf), "%*d", 15-strlen(buf) , inf->BatTestCounter);
    LCD_String_ext(16,buf , Align_Left);
    sprintf(buf, "%s", get_Language_String(S_SYS_TEST));
    sprintf(buf+strlen(buf), "%*d", 15-strlen(buf) , inf->SysTestCounter);
    LCD_String_ext(32,buf , Align_Left);

    LCD_Arrow(Arrow_enter);
}



