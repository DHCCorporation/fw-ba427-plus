/*
 * EmailVKwidget.c
 *
 *  Created on: 2020�~12��23��
 *      Author: henry.yang
 */
#include "Widgetmgr.h"
#define SEL_DEFAULT 1
#define EMAIL_LEN 32

void Email_DISPLAY();
void Email_UP();
void Email_DOWN();
void Email_LEFT();
void Email_RIGHT();
void Email_SELECT();
void Email_Routine();
void Email_initail();

widget* Email_sub_W[] = {
        &PrintResult_widget,
};

char email_text[EMAIL_LEN+1];
static uint8_t str_update = false;

widget Email_widget = {
		.Key_UP = Email_UP,
		.Key_DOWN = Email_DOWN,
		.Key_RIGHT = Email_RIGHT,
		.Key_LEFT = Email_LEFT,
		.Key_SELECT = Email_SELECT,
		.routine = Email_Routine,
		.service = 0,
		.initial = Email_initail,
		.display = Email_DISPLAY,
		.subwidget = Email_sub_W,
		.number_of_widget = sizeof(Email_sub_W)/4,
		.index = 0,
		.name = "EMAIL_VK",
};
char* get_input_email_str(void)
{
    return email_text;
}

void Email_cb(char* data,uint8_t len)
{
	SYS_PAR* par = Get_system_parameter();
    str_update = true;
    memset(email_text, 0, sizeof(email_text));
    memcpy(email_text,data,len);
    //UARTprintf("Email_cb: %s\n", data);
}

void Email_initail(void)
{
    str_update = false;
    Email_widget.index = SEL_DEFAULT;
}

void Email_DISPLAY(void)
{
    if(!Email_widget.parameter){
        ST7565ClearScreen(BUFFER1);
        ST7565Refresh(BUFFER1);
        //LCD_String_ext(16, get_Language_String(S_SEND_RESULT), Align_Left);
        LCD_String_ext2(32, get_Language_String((Email_widget.index)? S_YES:S_NO), Align_Right, 2);
        LCD_Arrow(Arrow_enter | Arrow_RL);    
    }
    else{
        VK_char_num_sym_display();
    }
}

void Email_Routine(void)
{
    static uint16_t delay = 0;
    if(Email_widget.parameter){
        if(str_update){            
            Move_widget(Email_sub_W[0],0);
            return;
        }
        delay = (delay+1)%CURSOR_BLINK_T;
        if(!delay){
            VK_CNS_Cursor_blink_V();
        }
    }
}

void Email_UP(void)
{
    if(Email_widget.parameter){
        VK_char_num_sym_up();
    }
}

void Email_DOWN(void)
{
    if(Email_widget.parameter){
        VK_char_num_sym_down();
    }
}

void Email_LEFT(void)
{
    if(!Email_widget.parameter){        
        Email_widget.index^=1;
        Email_DISPLAY();
    }
    else{
        VK_char_num_sym_left();
    }
}

void Email_RIGHT(void)
{
    if(!Email_widget.parameter){
        Email_widget.index^=1;
        Email_DISPLAY();
    }
    else{
        VK_char_num_sym_right();
    }
    
}

void Email_SELECT(void)
{
    SYS_PAR* par = Get_system_parameter();
    if(!Email_widget.parameter){
        if(Email_widget.index){
            Email_widget.parameter = 1;
            VK_INFO_T info = {//.title_str = get_Language_String(S_ENTER_EMAIL),
                              .underline = false,
                              .start_x = 5,
                              .start_y = 15,
                              .char_width = 6,
                              .len = sizeof(email_text)-1,
                              .arr_flag = (Arrow_enter | Arrow_RL),// | ARROW_UP_DOWN
                              .cb = Email_cb,};
            VK_char_num_sym_init(&info);
            Email_DISPLAY();
        }
        else{
            memset(email_text, 0, sizeof(email_text));
            Move_widget(Email_sub_W[0],0);
        }
    }
    else{
        VK_char_num_sym_sel();
    }
}



