/*
 * bootupChk_widget.c
 *
 *  Created on: 2020�~12��29��
 *      Author: henry.yang
 */
#include "Widgetmgr.h"

enum{
    boootup_check_temp = 0,
    boootup_check_internal,
    boootup_check_MEMIC,
    boootup_check_clamp,
    boootup_check_cable,
};
static uint8_t bootup_check_state = boootup_check_temp;

void bootup_service();
void bootup_initial();

widget* bootup_sub_W[] = {
    &ChkBatTemp_widget,//block
    &ChkIntBat_widget,//block
    0,//&ChkMemICFull_widget,//recoverable
    &CheckClamps_widget,//recoverable
    &ChkLoadErr_widget,//block
    &OutputCableChkComplete_widget,
};

widget bootup_widget = {
		.Key_UP = 0,
		.Key_DOWN = 0,
		.Key_RIGHT = 0,
		.Key_LEFT = 0,
		.Key_SELECT = 0,
		.routine = 0,
		.service = bootup_service,
		.initial = bootup_initial,
		.display = 0,
		.subwidget = bootup_sub_W,
		.number_of_widget = sizeof(bootup_sub_W)/4,
		.index = 0,
		.name = "bootup",
};

void bootup_initial()
{
    bootup_check_state = boootup_check_temp;
}

void bootup_service(void)
{
    float Temp;
    uint32_t ui32scale;
    float fvoltage;
    SYS_INFO* info = Get_system_info();
    TEST_DATA* TD = Get_test_data();
    uint16_t update_count;
    float external_voltage = 0.0;
    
    switch(bootup_check_state){
    case boootup_check_temp:
        GetBodyTemp(&Temp);
        if(Temp < TEMP_LOW || Temp > TEMP_HIGH){
            Move_widget(bootup_sub_W[0], 0);
            break;
        }
    case boootup_check_internal:
        if(BattCoreChkIntBatt(&ui32scale, &fvoltage) == 1){
            ROM_TimerDisable(TIMER0_BASE, TIMER_A); // prevent form callback, if internal Battery low event occure
            Move_widget(bootup_sub_W[1], 0);
            break;
        }
    case boootup_check_MEMIC:
        bootup_check_state = boootup_check_clamp;
        update_count = get_saved_record_count();
        if(update_count >= MAX_BTSSTESTCOUNTER){
            //Move_widget(bootup_sub_W[2], 0);
            info->TotalTestRecord = 0;
            info->TestRecordOffset = 0;
            save_system_info(&info->TestRecordOffset, sizeof(info->TestRecordOffset));
            save_system_info(&info->TotalTestRecord, sizeof(info->TotalTestRecord));
            break;
        }
    case boootup_check_clamp:
        bootup_check_state = boootup_check_cable;
        BattCoreScanVoltage(&fvoltage);
        if(fvoltage < check_clamp_v){
            set_system_error(Clamp_err);
            ROM_TimerDisable(TIMER0_BASE, TIMER_A); // prevent form callback, if clamp error event occure
            Move_widget(bootup_sub_W[3], 0);
            break;
        }
    case boootup_check_cable:
        BattCoreScanVoltage(&external_voltage);

        if(TD->SYSTEM_CHANNEL != MAP_24V){
            if(!BattCoreCChkCable(1)){
                Move_widget(bootup_sub_W[4], 0);
                break;
            }
        }
        else{
            Widget_init(manual_test);
            break;
        }

    default:
        Move_widget(bootup_sub_W[5], 0);
        break;
    }    
}


