//*****************************************************************************
//
// Source File: message_set_app.h
// Description: to manage the message set for each languages.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 11/30/18     Jerry.W        Created file.
// 12/18/18     Henry.Y        Add event: for testing accuracy.
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/driver/wifi_driver.h#2 $
// $DateTime: 2017/12/18 16:17:54 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#include "Widgetmgr.h"
#include "message_set_app.h"

#if ENGLISH_en
const char en_US_Software_available[] = "Software upgrade available, go to setting to upgrade.";
const char en_US_Insert_VIN[] = "Please insert complete VIN code.";
const char en_US_Turn_on_WIFI[] = "Please turn on WIFI.";
const char en_US_Download_complete[] = "Download Completed, Initiate Upgrade in";
const char en_US_upgrade_available[] = "Upgrade Available After Reboot.";
const char en_US_Natwork_unstable[] = "Network Unstable, Fail to download.";
const char en_US_Software_up_to_date[] = "Software is Up To Date.";
const char en_US_Fail_to_cloud[] = "Fail to Connect Cloud.";
const char en_US_Check_connection[] = "Please check your WIFI Connection.";
const char en_US_Store_result[] = "No WIFI Connected, Result Stored Locally. Space:";
const char en_US_Uploading_result[] = "Uploading...,";
const char en_US_Result_uploaded[] = "Successfully Uploaded,";
const char en_US_Upload_failed[] = "Fail to Upload Result.";
const char en_US_WIFI_connected[] = "WIFI connected";
const char en_US_Memory_full[] = "INTERNAL MEMORY FULL, please connect to a WIFI network and upload.";
const char en_US_Data_uploaded[] = "Data Upload in Cloud / Data Saved in Device Memory, Space:";
const char en_US_Regarge_time[] = "Estimated Recharging Time:";
      
const char en_US_NSec[] = "Sec.";
const char en_US_NHour[] = "Hour";
const char en_US_NHours[] = "Hours";
const char en_US_NResult_pending[] = " Result Pending.";
const char en_US_NResult_to_upload[] = " Result Found Need to Upload.";
const char en_US_For_accuracy[] = "For accuracy, battery should not charged/ discharged in past 4 hours.";
const char en_US_unregistered_device_plz_register[] = "Unregistered device,\nPlease register";
const char en_US_invalid_code[] = "INVALID CODE.";
const char en_US_network_unstable_fail_to_register[] = "Network unstable, fail to register.";
const char en_US_register_fail_code[] = "Registration failed.\nError:";
const char en_US_register_successfully[] = "Registration success.";
const char en_US_fail_to_upload_unregister[] = "Fail to Upload Result, Unregistered device.";
const char en_US_unregister_result_stored[] = "Unregistered device, Result Stored Locally. Space:";

#endif

#if DEUTSCH_en
const char de_DE_Software_available[] = "Software-Aktualisierung verfügbar. Zum Aktualisieren zu Einstellung gehen.";
const char de_DE_Insert_VIN[] = "Den vollständigen VIN-Code eingeben.";
const char de_DE_Turn_on_WIFI[] = "Die WIFI einschalten.";
const char de_DE_Download_complete[] = "Herunterladen abgeschlossen. Hinaufladen wird in";
const char de_DE_upgrade_available[] = "Hinaufladen nach Neustart verfügbar.";
const char de_DE_Natwork_unstable[] = "Unstabiles Netzwerk. Fehler beim Herunterladen.";
const char de_DE_Software_up_to_date[] = "Software wurde aktualisiert";
const char de_DE_Fail_to_cloud[] = "Fehler beim Verbinden mit dem Cloud.";
const char de_DE_Check_connection[] = "Prüfen Sie Ihre WIFI-Verbindung.";
const char de_DE_Store_result[] = "Keine WIFI angeschlossen. Das Resultat wird lokal gespeichert. Speicherplatz:";
const char de_DE_Uploading_result[] = "Wird hinaufgeladen...,";
const char de_DE_Result_uploaded[] = "Hinaufladen erfolgreich. Hinaufladen des gefundenen";
const char de_DE_Upload_failed[] = "Fehler beim Hinaufladen des Resultats";
const char de_DE_WIFI_connected[] = "WIFI angeschlossen. Hinaufladen des gefundenen";
const char de_DE_Memory_full[] = "INTERNER SPEICHER VOLL. An ein WIFI-Netz anschließen und hinaufladen.";
const char de_DE_Data_uploaded[] = "Daten in Cloud hinaufladen / Daten im Gerätespeicher gespeichert, Speicherplatz: ";
const char de_DE_Regarge_time[] = "Geschätzte Ladezeit:";
      
const char de_DE_NSec[] = "Sek. initialisiert.";
const char de_DE_NHour[] = "Stunde";
const char de_DE_NHours[] = "Stunden";
const char de_DE_NResult_pending[] = "-Resultat ausstehend";
const char de_DE_NResult_to_upload[] = "-Resultats notwendig.";
const char de_DE_For_accuracy[] = "Für die Genauigkeit darf die Batterie in den letzten 4 Stunden nicht geladen/entladen werden.";
const char de_DE_unregistered_device_plz_register[] = "Nicht registriertes Gerät,\nBitte registrieren";
const char de_DE_invalid_code[] = "UNGÜLTIGER CODE";
const char de_DE_network_unstable_fail_to_register[] = "Unstabiles Netz, Fehler beim Registrieren";
const char de_DE_register_fail_code[] = "Registrierung fehlgeschlagen\nError:";
const char de_DE_register_successfully[] = "Registrierung erfolgreich.";
const char de_DE_fail_to_upload_unregister[] = "Fehler beim Hinaufladen des Resultats,\nNicht registriertes Gerät.";
const char de_DE_unregister_result_stored[] = "Nicht registriertes Gerät. Das Resultat wird lokal gespeichert. Platz:";

#endif

#if ESPANOL_en
const char es_ES_Software_available[] = "La actualización del software está disponible, valla a configuración para la actualización.";
const char es_ES_Insert_VIN[] = "Por favor introduzca el código VIN completo.";
const char es_ES_Turn_on_WIFI[] = "Por favor encienda el WIFI.";
const char es_ES_Download_complete[] = "Descarga Completa, Iniciar la Actualización en";
const char es_ES_upgrade_available[] = "La Actualización Está Disponible Después de Reiniciar.";
const char es_ES_Natwork_unstable[] = "Red Inestable, Descarga fallida.";
const char es_ES_Software_up_to_date[] = "El Software Esta Al Día.";
const char es_ES_Fail_to_cloud[] = "Conexión con la Nube Fallida.";
const char es_ES_Check_connection[] = "Por favor verifique su conexión WIFI.";
const char es_ES_Store_result[] = "No está conectado a WIFI, Los Resultados Están Guardados Localmente. Espacio:";
const char es_ES_Uploading_result[] = "Cargando...,";
const char es_ES_Result_uploaded[] = "Carga Satisfactoria,";
const char es_ES_Upload_failed[] = "Falla en la Carga del Resultado.";
const char es_ES_WIFI_connected[] = "El WIFI está conectado,";
const char es_ES_Memory_full[] = "MEMORIA INTERNA LLENA, por favor conéctese a WIFI para cargar.";
const char es_ES_Data_uploaded[] = "Datos Cargados en la Nube / Datos Guardados en el Dispositivo, Espacio:";
const char es_ES_Regarge_time[] = "Tiempo de Recarga Estimado:";
      
const char es_ES_NSec[] = "Segundos.";
const char es_ES_NHour[] = "Hora";
const char es_ES_NHours[] = "Horas";
const char es_ES_NResult_pending[] = " Resultados Pendiantes.";
const char es_ES_NResult_to_upload[] = " Resultados Encontrados Necesitan Ser Cargados.";
const char es_ES_For_accuracy[] = "para mayor precisión, la batería no debe ser cargada/descargada en las 4 horas anteriores.";
const char es_ES_unregistered_device_plz_register[] = "Dispositivo no registrado,\nPor favor registrar";
const char es_ES_invalid_code[] = "CÓDIGO INVÁLIDO.";
const char es_ES_network_unstable_fail_to_register[] = "red inestable, falló en el registro.";
const char es_ES_register_fail_code[] = "Registro fallido.\nError:";
const char es_ES_register_successfully[] = "Registro satisfactorio.";
const char es_ES_fail_to_upload_unregister[] = "Fallo en la Actualización del Resultado,\nDispositivo no registrado.";
const char es_ES_unregister_result_stored[] = "Dispositivo no registrado, El Resultado Se Almacena Localmente. Espacio:";
#endif

#if FRANCAIS_en
const char fr_FR_Software_available[] = "Mise à jour du logiciel disponible, allez au réglage pour mettre à jour.";
const char fr_FR_Insert_VIN[] = "Veuillez insérer le code NIV complet.";
const char fr_FR_Turn_on_WIFI[] = "Veuillez activer le WIFI.";
const char fr_FR_Download_complete[] = "Téléchargement terminé, lancement de la mise à jour dans";
const char fr_FR_upgrade_available[] = "Mise à jour disponible après redémarrage.";
const char fr_FR_Natwork_unstable[] = "Réseau instable, échec du téléchargement.";
const char fr_FR_Software_up_to_date[] = "Le logiciel est à jour.";
const char fr_FR_Fail_to_cloud[] = "Échec de la connexion au cloud.";
const char fr_FR_Check_connection[] = "Veuillez vérifier votre connexion WIFI.";
const char fr_FR_Store_result[] = "Pas de WIFI connecté, résultat enregistré localement. Espace:";
const char fr_FR_Uploading_result[] = "Téléchargement ...,";
const char fr_FR_Result_uploaded[] = "Téléchargé avec succès,";
const char fr_FR_Upload_failed[] = "Échec du téléchargement du résultat.";
const char fr_FR_WIFI_connected[] = "WIFI connecté";
const char fr_FR_Memory_full[] = "MÉMOIRE INTERNE PLEINE, veuillez vous connecter à un réseau WIFI et télécharger.";
const char fr_FR_Data_uploaded[] = "Chargement de données dans le cloud / Données enregistrées dans la mémoire du périphérique, Espace:";
const char fr_FR_Regarge_time[] = "Estimation du temps de recharge:";
     
const char fr_FR_NSec[] = "Sec.";
const char fr_FR_NHour[] = "heure";
const char fr_FR_NHours[] = "heures";
const char fr_FR_NResult_pending[] = " Résultat en attente.";
const char fr_FR_NResult_to_upload[] = " Résultat trouvé qui ont besoin d'être téléchargé.";

const char fr_FR_For_accuracy[] = "Pour des raisons de précision, la batterie ne doit pas être chargée/déchargée au cours des 4 dernières heures.";
const char fr_FR_unregistered_device_plz_register[] = "Appareil non enregistré,\nVeuillez vous enregistrez";
const char fr_FR_invalid_code[] = "CODE INVALIDE.";
const char fr_FR_network_unstable_fail_to_register[] = "Réseau instable, échec de l'enregistrement.";
const char fr_FR_register_fail_code[] = "L'enregistrement a échoué\nErreur:";
const char fr_FR_register_successfully[] = "Enregistrement réussit.";
const char fr_FR_fail_to_upload_unregister[] = "Échec du téléchargement du résultat,\nAppareil non enregistré.";
const char fr_FR_unregister_result_stored[] = "Périphérique non enregistré, résultat stocké localement. Espace:";

#endif

#if ITALIAN_en
const char it_IT_Software_available[] = "Aggiornamento software disponibile, accedere alle impostazioni per eseguire l'aggiornamento.";
const char it_IT_Insert_VIN[] = "Immettere un codice VIN completo.";
const char it_IT_Turn_on_WIFI[] = "Accendere il WIFI.";
const char it_IT_Download_complete[] = "Download completato, Aggiornamento avviato in";
const char it_IT_upgrade_available[] = "Aggiornamento disponibile dopo il riavvio del dispositivo.";
const char it_IT_Natwork_unstable[] = "Connessione instabile, impossibile completare il download.";
const char it_IT_Software_up_to_date[] = "Il Software è aggiornato.";
const char it_IT_Fail_to_cloud[] = "Impossibile connettersi al Cloud.";
const char it_IT_Check_connection[] = "Controllare la connessione WIFI.";
const char it_IT_Store_result[] = "Connessione WIFI non disponibile, i risultati sono archiviati localmente. Spazio:";
const char it_IT_Uploading_result[] = "Upload in corso...,";
const char it_IT_Result_uploaded[] = "Upload effettuato con successo,";
const char it_IT_Upload_failed[] = "Impossibile completare l'upload dei risultati.";
const char it_IT_WIFI_connected[] = "WIFI connesso,";
const char it_IT_Memory_full[] = "MEMORIA INTERNA ESAURITA, connettersi ad una rete WIFI ed eseguire l'upload.";
const char it_IT_Data_uploaded[] = "Dati su Cloud / Dati salvati sulla memoria del dispositivo, Spazio:";
const char it_IT_Regarge_time[] = "Tempo di ricarica previsto:";
      
const char it_IT_NSec[] = "secondi.";
const char it_IT_NHour[] = "Ora";
const char it_IT_NHours[] = "Ore";
const char it_IT_NResult_pending[] = " risultati in attesa.";
const char it_IT_NResult_to_upload[] = " risultati trovati per l'upload.";
const char it_IT_For_accuracy[] = "Per garantire l'accuratezza dell'operazione, la batteria non deve essere stata caricata / scarica nelle ultime 4 ore.";
const char it_IT_unregistered_device_plz_register[] = "Dispositivo non registrato\nSi prega di effettuare la registrazione";
const char it_IT_invalid_code[] = "CODICE NON VALIDO";
const char it_IT_network_unstable_fail_to_register[] = "Connessione instabile, impossibile completare la registrazione.";
const char it_IT_register_fail_code[] = "Registrazione non riuscita.\nErrore:";
const char it_IT_register_successfully[] = "Registrazione effettuata con successo.";
const char it_IT_fail_to_upload_unregister[] = "Impossibile Caricare il Risultato,\nDispositivo non registrato.";
const char it_IT_unregister_result_stored[] = "Dispositivo non registrato, Risultato Archiviato Localmente. Spazio:";

#endif

#if PORTUGUES_en
const char pt_PT_Software_available[] = "Upgrade de software disponível, dirija-se a configurações para fazer upgrade.";
const char pt_PT_Insert_VIN[] = "Por favor insira o código completo NIV.";
const char pt_PT_Turn_on_WIFI[] = "Por favor ligue a WIFI.";
const char pt_PT_Download_complete[] = "Descarregamento finalizado, Iniciar Upgrade em";
const char pt_PT_upgrade_available[] = "Upgrade Disponível Após Reiniciar.";
const char pt_PT_Natwork_unstable[] = "Ligação Instável, Falha no descarregamento.";
const char pt_PT_Software_up_to_date[] = "O Software está Atualizado.";
const char pt_PT_Fail_to_cloud[] = "Falha ao Conetar à Nuvem.";
const char pt_PT_Check_connection[] = "Por favor verifique a sua Conexão WIFI.";
const char pt_PT_Store_result[] = "Não foi Conetado à WIFI Resultado Armazenado Localmente. Espaço:";
const char pt_PT_Uploading_result[] = "A Carregar..., Resultado";
const char pt_PT_Result_uploaded[] = "Carregamento com êxito, Resultado";
const char pt_PT_Upload_failed[] = "Falha ao Carregar o Resultado.";
const char pt_PT_WIFI_connected[] = "Conetado à WIFI Resultado";
const char pt_PT_Memory_full[] = "MEMÓRIA INTERNA CHEIA, por favor conete a uma rede WIFI e faça o carregamento.";
const char pt_PT_Data_uploaded[] = "Dados Carregados na Nuvem / Dados Guardados na Memória do Dispositivo, Espaço:";
const char pt_PT_Regarge_time[] = "Tempo Estimado de Recarga:";
      
const char pt_PT_NSec[] = "Seg.";
const char pt_PT_NHour[] = "Hora";
const char pt_PT_NHours[] = "Horas";
const char pt_PT_NResult_pending[] = " em Espera.";
const char pt_PT_NResult_to_upload[] = " Encontrado Necessário Fazer o Carregamento.";
const char pt_PT_For_accuracy[] = "Para mais precisão, a bateria não deve ser carregada/descarregada mais de 4 horas.";
const char pt_PT_unregistered_device_plz_register[] = "Dispositivo não registado,\nPor favor registe";
const char pt_PT_invalid_code[] = "CÓDIGO INVÁLIDO.";
const char pt_PT_network_unstable_fail_to_register[] = "Rede instável, falha ao registar.";
const char pt_PT_register_fail_code[] = "Registo falhado.\nErro:";
const char pt_PT_register_successfully[] = "Registo com êxito.";
const char pt_PT_fail_to_upload_unregister[] = "Carregamento do resultado falhado,\nDispositivo não registado.";
const char pt_PT_unregister_result_stored[] = "Dispositivo não registado, Resultado Guardado Localmente. Espaço:";
#endif

#if NEDERLANDS_en
const char nd_ND_Software_available[] = "Software-upgrade beschikbaar, ga naar instellingen om upgrade uit te voeren.";
const char nd_ND_Insert_VIN[] = "Voer volledige VIN-code in.";
const char nd_ND_Turn_on_WIFI[] = "Schakel WIFI in.";
const char nd_ND_Download_complete[] = "Download voltooid, upgrade wordt over";
const char nd_ND_upgrade_available[] = "Upgrade beschikbaar na herstart.";
const char nd_ND_Natwork_unstable[] = "Netwerk instabiel, downloaden mislukt.";
const char nd_ND_Software_up_to_date[] = "Software is actueel.";
const char nd_ND_Fail_to_cloud[] = "Verbinding met cloud mislukt.";
const char nd_ND_Check_connection[] = "Controleer de WIFI-verbinding.";
const char nd_ND_Store_result[] = "Geen WIF verbonden, resultaat lokaal opgeslagen. Afstand:";
const char nd_ND_Uploading_result[] = "Uploaden... Nog";
const char nd_ND_Result_uploaded[] = "Upload geslaagd,";
const char nd_ND_Upload_failed[] = "Uploaden resultaten mislukt.";
const char nd_ND_WIFI_connected[] = "WIFI verbonden";
const char nd_ND_Memory_full[] = "INTERN GEHEUGEN VOL, maak verbinding met een WIFI-netwerk en upload.";
const char nd_ND_Data_uploaded[] = "Data upload in cloud / Data opgeslagen in apparaatgeheugen, ruimte:";
const char nd_ND_Regarge_time[] = "Geschatte oplaadtijd:";
      
const char nd_ND_NSec[] = "sec. uitgevoerd.";
const char nd_ND_NHour[] = "uur";
const char nd_ND_NHours[] = "uur";
const char nd_ND_NResult_pending[] = " resultaten.";
const char nd_ND_NResult_to_upload[] = " resultaten voor uploaden gevonden.";
const char nd_ND_For_accuracy[] = "Voor de nauwkeurigheid mag de accu de komende vier uur niet worden opgeladen/ontladen.";
const char nd_ND_unregistered_device_plz_register[] = "Niet-geregistreerd apparaat,\nS.v.p. registreren";
const char nd_ND_invalid_code[] = "ONGELDIGE CODE.";
const char nd_ND_network_unstable_fail_to_register[] = "Netwerk instabiel, registreren mislukt.";
const char nd_ND_register_fail_code[] = "Registratie mislukt.\nFout:";
const char nd_ND_register_successfully[] = "Registratie geslaagd.";
const char nd_ND_fail_to_upload_unregister[] = "Uploaden resultaat mislukt,\nNiet-geregistreerd apparaat.";
const char nd_ND_unregister_result_stored[] = "Niet-geregistreerd apparaat, resultaat lokaal opgeslagen. Afstand:";
#endif

#if DANSK_en
const char dk_DK_Software_available[] = "Softwareopgradering er tilgængelig, gå til indstilling for at opgradere.";
const char dk_DK_Insert_VIN[] = "Indsæt den fulde stelnummerkode.";
const char dk_DK_Turn_on_WIFI[] = "Slå WI-FI til.";
const char dk_DK_Download_complete[] = "Download fuldført, start opgradering om";
const char dk_DK_upgrade_available[] = "Opgradering tilgængelig efter genstart.";
const char dk_DK_Natwork_unstable[] = "Netværk er ustabilt, kunne ikke downloade.";
const char dk_DK_Software_up_to_date[] = "Software er opdateret.";
const char dk_DK_Fail_to_cloud[] = "Kunne ikke oprette forbindelse til cloud.";
const char dk_DK_Check_connection[] = "Kontrollér din WI-FI-forbindelse.";
const char dk_DK_Store_result[] = "WI-FI ikke tilsluttet, resultat er lagret lokalt. Plads:";
const char dk_DK_Uploading_result[] = "Uploader...,";
const char dk_DK_Result_uploaded[] = "Uploadet,";
const char dk_DK_Upload_failed[] = "Kunne ikke uploade resultat.";
const char dk_DK_WIFI_connected[] = "WI-FI forbundet";
const char dk_DK_Memory_full[] = "INTERN HUKOMMELSE ER FULD, opret forbindelse til et WI-FI-netværk, og upload.";
const char dk_DK_Data_uploaded[] = "Dataupload i cloud / data er gemt i enhedshukommelse, plads:";
const char dk_DK_Regarge_time[] = "Estimeret genopladningstid:";
 
const char dk_DK_NSec[] = "sek.";
const char dk_DK_NHour[] = "time";
const char dk_DK_NHours[] = "timer";
const char dk_DK_NResult_pending[] = " resultat venter.";
const char dk_DK_NResult_to_upload[] = " resultat fundet, upload kræves.";
const char dk_DK_For_accuracy[] = "For at sikre nøjagtighed skal batteriet ikke oplades/aflades de sidste 4 timer.";
const char dk_DK_unregistered_device_plz_register[] = "Uregistreret enhed.\nRegistrer";
const char dk_DK_invalid_code[] = "UGYLDIG KODE.";
const char dk_DK_network_unstable_fail_to_register[] = "Ustabilt netværk. Registrering mislykkedes.";
const char dk_DK_register_fail_code[] = "Registreringsfejl.\nFejl:";
const char dk_DK_register_successfully[] = "Registrering fuldført.";
const char dk_DK_fail_to_upload_unregister[] = "Kunne ikke uploade resultat.\nUregistreret enhed.";
const char dk_DK_unregister_result_stored[] = "Uregistreret enhed. Resultat gemt lokalt. Plads:";
#endif

#if NORSK_en
const char nl_NW_Software_available[] = "Programvare-\noppgradering er tilgjengelig. Gå til innstillinger for å oppgradere.";
const char nl_NW_Insert_VIN[] = "Skriv inn fullstendig VIN-kode.";
const char nl_NW_Turn_on_WIFI[] = "Slå på Wi-Fi.";
const char nl_NW_Download_complete[] = "Nedlasting er fullført. Starter oppgradering om";
const char nl_NW_upgrade_available[] = "Oppgradering er tilgjengelig etter omstart.";
const char nl_NW_Natwork_unstable[] = "Nettverket er ustabilt. Nedlasting mislyktes.";
const char nl_NW_Software_up_to_date[] = "Programvaren er oppdatert.";
const char nl_NW_Fail_to_cloud[] = "Kunne ikke koble til skyen.";
const char nl_NW_Check_connection[] = "Kontroller Wi-Fi-tilkoblingen din.";
const char nl_NW_Store_result[] = "Ingen Wi-Fi tilkoblet. Resultat er lagret lokalt. Område:";
const char nl_NW_Uploading_result[] = "Laster opp ....";
const char nl_NW_Result_uploaded[] = "Er lastet opp.";
const char nl_NW_Upload_failed[] = "Kunne ikke laste opp resultat.";
const char nl_NW_WIFI_connected[] = "Wi-Fi er tilkoblet";
const char nl_NW_Memory_full[] = "INTERNT MINNE ER FULLT. Koble til et Wi-Fi-nettverk og last opp.";
const char nl_NW_Data_uploaded[] = "Dataopplasting i skyen / Data lagret i enhetens minne. Plass:";
const char nl_NW_Regarge_time[] = "Beregnet ladetid:";
      
const char nl_NW_NSec[] = "sek.";
const char nl_NW_NHour[] = "time";
const char nl_NW_NHours[] = "timer";
const char nl_NW_NResult_pending[] = " resultat på vent.";
const char nl_NW_NResult_to_upload[] = " resultater funnet som må lastes opp.";
const char nl_NW_For_accuracy[] = "For nøyaktighet bør batteriet ikke lades / utlades i løpet av 4 timer.";
const char nl_NW_unregistered_device_plz_register[] = "Uregistrert enhet,\nVennligst registrer";
const char nl_NW_invalid_code[] = "UGYLDIG KODE.";
const char nl_NW_network_unstable_fail_to_register[] = "Nettverket er ustabilt, kunne ikke registrere.";
const char nl_NW_register_fail_code[] = "Registrering feilet.\nFeil:";
const char nl_NW_register_successfully[] = "Registrering fullført.";
const char nl_NW_fail_to_upload_unregister[] = "Kunne ikke laste opp resultat,\nUregistrert enhet.";
const char nl_NW_unregister_result_stored[] = "Uregistrert enhet, resultatet er lagret lokalt. Område:";
#endif

#if SOOMI_en
const char sm_FN_Software_available[] = "Ohjelmistopäivitys saatavilla, siirry asetuksiin päivitystä varten.";
const char sm_FN_Insert_VIN[] = "Anna täydellinen VIN-koodi.";
const char sm_FN_Turn_on_WIFI[] = "Ota WiFi-yhteys käyttöön.";
const char sm_FN_Download_complete[] = "Lataus on valmis, aloita päivitys";
const char sm_FN_upgrade_available[] = "Päivitys käytettävissä uudelleenkäynnistyksen jälkeen.";
const char sm_FN_Natwork_unstable[] = "Verkko epävakaa, lataus ei onnistunut.";
const char sm_FN_Software_up_to_date[] = "Ohjelmisto on ajan tasalla.";
const char sm_FN_Fail_to_cloud[] = "Yhteyttä pilvipalvelimeen ei voitu muodostaa.";
const char sm_FN_Check_connection[] = "Tarkista WiFi-yhteys.";
const char sm_FN_Store_result[] = "Ei WiFi-yhteyttä, tulos tallennetaan paikallisesti. Tila:";
const char sm_FN_Uploading_result[] = "Ladataan...,";
const char sm_FN_Result_uploaded[] = "Lataus onnistui,";
const char sm_FN_Upload_failed[] = "Tuloksen lataaminen epäonnistui.";
const char sm_FN_WIFI_connected[] = "WiFi yhdistetty";
const char sm_FN_Memory_full[] = "SISÄINEN MUISTI TÄYNNÄ, muodosta yhteys WiFi-verkkoon ja lataa.";
const char sm_FN_Data_uploaded[] = "Tiedot ladattu pilveen / Tiedot tallennettu laitteen muistiin, tila:";
const char sm_FN_Regarge_time[] = "Arvioitu latausaika:";

const char sm_FN_NSec[] = "sekunnissa.";
const char sm_FN_NHour[] = "tunti";
const char sm_FN_NHours[] = "tuntia";
const char sm_FN_NResult_pending[] = " tulosta odottaa.";
const char sm_FN_NResult_to_upload[] = " ladattavaa tulosta löytyi.";
const char sm_FN_For_accuracy[] = "Akkua ei tule ladata/purkaa 4 tunnin ajan tarkkuuden varmistamiseksi.";
const char sm_FN_unregistered_device_plz_register[] = "Laitetta ei ole rekisteröity.\nRekisteröi";
const char sm_FN_invalid_code[] = "VIRHEELLINEN KOODI";
const char sm_FN_network_unstable_fail_to_register[] = "Verkko epävakaa, rekisteröinti ei onnistunut.";
const char sm_FN_register_fail_code[] = "Rekisteröinti epäonnistui.\nVirhe ";
const char sm_FN_register_successfully[] = "Rekisteröinti onnistui.";
const char sm_FN_fail_to_upload_unregister[] = "Tuloksen lataaminen epäonnistui.\nLaitetta ei ole rekisteröity.";
const char sm_FN_unregister_result_stored[] = "Laitetta ei ole rekisteröity, tulos tallennetaan paikallisesti. Tila:";
#endif

#if SVENSKA_en
const char sv_SW_Software_available[] = "Programvaru-\nuppdatering tillgänglig, gå till inställningarför att uppgradera.";
const char sv_SW_Insert_VIN[] = "Infoga den fullständiga VIN-koden.";
const char sv_SW_Turn_on_WIFI[] = "Slå på WIFI.";
const char sv_SW_Download_complete[] = "Hämtning klar, initiera uppgradering om";
const char sv_SW_upgrade_available[] = "Uppgradering tillgänglig efter omstart.";
const char sv_SW_Natwork_unstable[] = "Nätverket är instabilt, hämtningen kunde inte genomföras.";
const char sv_SW_Software_up_to_date[] = "Programvaran är uppdaterad.";
const char sv_SW_Fail_to_cloud[] = "Det gick inte att ansluta till molnet.";
const char sv_SW_Check_connection[] = "Kontrollera din WIFI-anslutning.";
const char sv_SW_Store_result[] = "Ingen WIFI-anslutning, resultatet lagras lokalt. Utrymme:";
const char sv_SW_Uploading_result[] = "Laddar upp...,";
const char sv_SW_Result_uploaded[] = "Uppladdning klar,";
const char sv_SW_Upload_failed[] = "Det gick inte att ladda upp resultat.";
const char sv_SW_WIFI_connected[] = "WIFI-anslutning";
const char sv_SW_Memory_full[] = "DET INTERNA MINNET ÄR FULLT. Anslut till ett WIFI-nätverk och ladda upp.";
const char sv_SW_Data_uploaded[] = "Datauppladdning i molnet / Data sparade i enhetsminne, Utrymme:";
const char sv_SW_Regarge_time[] = "Uppskattad laddningstid:";
      
const char sv_SW_NSec[] = "sek.";
const char sv_SW_NHour[] = "timme";
const char sv_SW_NHours[] = "timmar";
const char sv_SW_NResult_pending[] = " resultat väntande.";
const char sv_SW_NResult_to_upload[] = " resultat hittades som behöver laddas upp.";
const char sv_SW_For_accuracy[] = "För noggrannhet så ska inte batteriet ha laddats/urladdats de senaste 4 timmarna.";
const char sv_SW_unregistered_device_plz_register[] = "Oregistrerad enhet,\nRegistrera";
const char sv_SW_invalid_code[] = "OGILTIG KOD.";
const char sv_SW_network_unstable_fail_to_register[] = "Nätverket är instabilt, det gick inte att registrera.";
const char sv_SW_register_fail_code[] = "Registreringen misslyckades.\nFel:";
const char sv_SW_register_successfully[] = "Registreringen lyckades.";
const char sv_SW_fail_to_upload_unregister[] = "Det gick inte att överföra resultatet,\nOregistrerad enhet.";
const char sv_SW_unregister_result_stored[] = "Oregistrerad enhet, resultatet lagras lokalt. Utrymme:";
#endif

#if CESTINA_en
const char cs_CZ_Software_available[] = "Upgrade softwaru je k dispozici, přejděte na nastavení pro upgrade.";
const char cs_CZ_Insert_VIN[] = "Vložte prosím kompletní VIN kód.";
const char cs_CZ_Turn_on_WIFI[] = "Zapněte prosím Wi-Fi.";
const char cs_CZ_Download_complete[] = "Stažení dokončeno, iniciovat upgrade za";
const char cs_CZ_upgrade_available[] = "Upgrade je k dispozici po restartování.";
const char cs_CZ_Natwork_unstable[] = "Síť nestabilní, stahování selhalo.";
const char cs_CZ_Software_up_to_date[] = "Software je aktuální.";
const char cs_CZ_Fail_to_cloud[] = "Nepodařilo se připojit ke cloudu.";
const char cs_CZ_Check_connection[] = "Zkontrolujte prosím připojení Wi-Fi.";
const char cs_CZ_Store_result[] = "Žádné Wi-Fi připojení, výsledek uložen místně. Prostor:";
const char cs_CZ_Uploading_result[] = "Nahrávání...,";
const char cs_CZ_Result_uploaded[] = "Úspěšně nahráno,";
const char cs_CZ_Upload_failed[] = "Nepodařilo se nahrát výsledek.";
const char cs_CZ_WIFI_connected[] = "Wi-Fi připojena";
const char cs_CZ_Memory_full[] = "VNITŘNÍ PAMĚŤ PLNÁ, připojte se prosím k síti Wi-Fi a nahrajte.";
const char cs_CZ_Data_uploaded[] = "Nahrání dat do Cloudu / Data uložená v paměti zařízení, Prostor:";
const char cs_CZ_Regarge_time[] = "Odhadovaný čas dobíjení:";
      
const char cs_CZ_NSec[] = "sekund.";
const char cs_CZ_NHour[] = "hodin";
const char cs_CZ_NHours[] = "hodin";
const char cs_CZ_NResult_pending[] = " Výsledek čeká.";
const char cs_CZ_NResult_to_upload[] = " výsledek nalezen Je třeba jej nahrát.";
const char cs_CZ_For_accuracy[] = "Pro přesnost by baterie neměla být nabíjena/vybíjena během posledních 4 hodin.";
const char cs_CZ_unregistered_device_plz_register[] = "Neregistrované zařízení,\nProveďte prosím registraci";
const char cs_CZ_invalid_code[] = "NEPLATNÝ KÓD.";
const char cs_CZ_network_unstable_fail_to_register[] = "Síť je nestabilní, registrace se nezdařila.";
const char cs_CZ_register_fail_code[] = "Registrace se nezdařila.\nChyba:";
const char cs_CZ_register_successfully[] = "Registrace byla úspěšná.";
const char cs_CZ_fail_to_upload_unregister[] = "Nepodařilo se nahrát výsledek,\nNeregistrované zařízení.";
const char cs_CZ_unregister_result_stored[] = "Neregistrované zařízení, výsledek uložen místně. Prostor:";
#endif

#if LIETUVIU_K_en
const char lt_LT_Software_available[] = "Yra programinės įrangos atnaujinimas, norėdami atnaujinti eikite į nustatymą.";
const char lt_LT_Insert_VIN[] = "Įveskite visą VIN kodą.";
const char lt_LT_Turn_on_WIFI[] = "Įjunkite WI-FI.";
const char lt_LT_Download_complete[] = "Atsisiuntimas baigtas, inicijuokite naujinimą per";
const char lt_LT_upgrade_available[] = "Perkrovus galima atnaujinti.";
const char lt_LT_Natwork_unstable[] = "Tinklas nestabilus, nepavyko atsisiųsti.";
const char lt_LT_Software_up_to_date[] = "Programinė įranga yra naujausia.";
const char lt_LT_Fail_to_cloud[] = "Nepavyko prisijungti prie debesies.";
const char lt_LT_Check_connection[] = "Patikrinkite WI-FI ryšį.";
const char lt_LT_Store_result[] = "WI-FI neprijungtas, rezultatas išsaugotas vietoje. Vieta:";
const char lt_LT_Uploading_result[] = "Įkeliama..., laukia";
const char lt_LT_Result_uploaded[] = "Sėkmingai įkelta, rastas";
const char lt_LT_Upload_failed[] = "Nepavyko įkelti rezultato.";
const char lt_LT_WIFI_connected[] = "Prijungtas WI-FI Rastas";
const char lt_LT_Memory_full[] = "VIDINĖ ATMINTIS PILNA, prisijunkite prie WI-FI tinklo ir įkelkite.";
const char lt_LT_Data_uploaded[] = "Duomenys įkelti į debesį / duomenys išsaugoti įrenginio atmintyje, vieta:";
const char lt_LT_Regarge_time[] = "Apskaičiuotas įkrovimo laikas:";
      
const char lt_LT_NSec[] = "sek.";
const char lt_LT_NHour[] = "val.";
const char lt_LT_NHours[] = "val.";
const char lt_LT_NResult_pending[] = " rezultatų.";
const char lt_LT_NResult_to_upload[] = " rezultatas, reikia įkelti.";
const char lt_LT_For_accuracy[] = "Siekiant tikslumo, akumuliatorius negali būti įkrautas / iškrautas per pastarąsias 4 valandas.";
const char lt_LT_unregistered_device_plz_register[] = "Neužregistruotas įrenginys,\nUžregistruokite";
const char lt_LT_invalid_code[] = "NETINKAMAS KODAS.";
const char lt_LT_network_unstable_fail_to_register[] = "Tinklas nestabilus, nepavyko UŽREGISTRUOTI.";
const char lt_LT_register_fail_code[] = "Nepavyko užregistruoti.\nKlaida:";
const char lt_LT_register_successfully[] = "Sėkmingai užregistruota.";
const char lt_LT_fail_to_upload_unregister[] = "Nepavyko įkelti rezultato,\nNeužregistruotas įrenginys.";
const char lt_LT_unregister_result_stored[] = "Neužregistruotas įrenginys, rezultatas išsaugotas vietoje. Vieta:";
#endif

#if POLSKI_en
const char pl_PL_Software_available[] = "Dostępne uaktualnienie oprogramowania, przejdź do ustawień w celu uaktualnienia.";
const char pl_PL_Insert_VIN[] = "Wprowadź cały numer VIN.";
const char pl_PL_Turn_on_WIFI[] = "Włącz łączność WIFI.";
const char pl_PL_Download_complete[] = "Pobieranie zakończone, inicjacja aktualizacji za";
const char pl_PL_upgrade_available[] = "Uaktualnienie będzie dostępne po ponownym uruchomieniu.";
const char pl_PL_Natwork_unstable[] = "Sieć niestabilna, pobieranie nie powiodło się.";
const char pl_PL_Software_up_to_date[] = "Oprogramowanie jest aktualne.";
const char pl_PL_Fail_to_cloud[] = "Połączenie z chmurą nie powiodło się.";
const char pl_PL_Check_connection[] = "Sprawdź łączność WIFI.";
const char pl_PL_Store_result[] = "Brak podłączonej sieci WIFI, wynik zapisany lokalnie. Miejsce:";
const char pl_PL_Uploading_result[] = "Wysyłanie...,";
const char pl_PL_Result_uploaded[] = "Wysyłanie powiodło się, znaleziono";
const char pl_PL_Upload_failed[] = "Wysyłanie wyniku nie powiodło się.";
const char pl_PL_WIFI_connected[] = "Podłączona sieć WIFI Znaleziono";
const char pl_PL_Memory_full[] = "PAMIĘĆ WEWNĘTRZNA ZAPEŁNIONA, podłącz sieć WIFI i wyślij wyniki.";
const char pl_PL_Data_uploaded[] = "Dane wysłane do chmury / Dane zapisane w pamięci urządzenia, Miejsce";
const char pl_PL_Regarge_time[] = "Szacowany czas ładowania:";
      
const char pl_PL_NSec[] = "s.";
const char pl_PL_NHour[] = "h";
const char pl_PL_NHours[] = "h";
const char pl_PL_NResult_pending[] = " wyników w kolejce.";
const char pl_PL_NResult_to_upload[] = " wyników wymagających wysłania.";
const char pl_PL_For_accuracy[] = "Aby zapewnić precyzję wskazań, nie można wcześniej ładować/rozładowywać akumulatora przez 4 godziny.";
const char pl_PL_unregistered_device_plz_register[] = "Niezarejestrowane urządzenie,\nzarejestruj";
const char pl_PL_invalid_code[] = "NIEPRAWIDŁOWY KOD.";
const char pl_PL_network_unstable_fail_to_register[] = "Sieć niestabilna, rejestracja nie powiodła się.";
const char pl_PL_register_fail_code[] = "Niepowodzenie rejestracji.\nBłąd:";
const char pl_PL_register_successfully[] = "Zarejestrowano pomyślnie.";
const char pl_PL_fail_to_upload_unregister[] = "Wysyłanie wyniku nie powiodło się,\nniezarejestrowane urządzenie.";
const char pl_PL_unregister_result_stored[] = "Niezarejestrowane urządzenie, wynik zapisany lokalnie. Miejsce:";
#endif

#if SLOVENCINA_en
const char sl_SL_Software_available[] = "K dispozícii je aktualizácia softvéru. Prejdite na nastavenie a aktualizujte.";
const char sl_SL_Insert_VIN[] = "Zadajte úplný kód VIN.";
const char sl_SL_Turn_on_WIFI[] = "Zapnite sieť WI-FI.";
const char sl_SL_Download_complete[] = "Preberanie dokončené. Aktualizácia sa spustí o";
const char sl_SL_upgrade_available[] = "Aktualizácia bude k dispozícii po reštartovaní.";
const char sl_SL_Natwork_unstable[] = "Sieť je nestabilná, preberanie zlyhalo.";
const char sl_SL_Software_up_to_date[] = "Softvér je aktuálny.";
const char sl_SL_Fail_to_cloud[] = "Zlyhalo pripojenie ku cloudu.";
const char sl_SL_Check_connection[] = "Skontrolujte pripojenie k sieti WI-FI.";
const char sl_SL_Store_result[] = "Žiadne pripojenie k sieti WI-FI, výsledok sa ukladá lokálne. Priestor:";
const char sl_SL_Uploading_result[] = "Odosiela sa... spracúva sa výsledok ";
const char sl_SL_Result_uploaded[] = "Odovzdávanie úspešné. Zistený výsledok";
const char sl_SL_Upload_failed[] = "Odosielanie výsledku zlyhalo.";
const char sl_SL_WIFI_connected[] = "Pripojené k sieti WI-FI Zistený výsledok";
const char sl_SL_Memory_full[] = "PLNÁ VNÚTORNÁ PAMÄŤ. Pripojte sa k sieti WI-FI a odošlite údaje.";
const char sl_SL_Data_uploaded[] = "Údaje odoslané do cloudu / údaje uložené v pamäti zariadenia, priestor:";
const char sl_SL_Regarge_time[] = "Odhadovaný čas nabíjania:";
      
const char sl_SL_NSec[] = "s.";
const char sl_SL_NHour[] = "hod.";
const char sl_SL_NHours[] = "hod.";
const char sl_SL_NResult_pending[] = ".";
const char sl_SL_NResult_to_upload[] = " sa musí odoslať.";
const char sl_SL_For_accuracy[] = "Pre presnosť sa akumulátor nesmie 4 hodiny vopred nabíjať/vybíjať.";
const char sl_SL_unregistered_device_plz_register[] = "Neregistrované zariadenie.\nZaregistrujte ho.";
const char sl_SL_invalid_code[] = "NEPLATNÝ KÓD.";
const char sl_SL_network_unstable_fail_to_register[] = "Sieť je nestabilná, registrácia zlyhala.";
const char sl_SL_register_fail_code[] = "Registrácia zlyhala.\nChyba:";
const char sl_SL_register_successfully[] = "Registrácia bola úspešná.";
const char sl_SL_fail_to_upload_unregister[] = "Odoslanie výsledku zlyhalo.\nNeregistrované zariadenie.";
const char sl_SL_unregister_result_stored[] = "Neregistrované zariadenie. Výsledok sa uložil lokálne. Priestor:";
#endif

#if SLOVENSCINA_en
const char sls_SLJ_Software_available[] = "Na voljo je nadgradnja programske opreme. Odprite nastavitve, da jo nadgradite.";
const char sls_SLJ_Insert_VIN[] = "Vnesite celotno kodo VIN.";
const char sls_SLJ_Turn_on_WIFI[] = "Vklopite WI-FI.";
const char sls_SLJ_Download_complete[] = "Prenos je končan. Nadgradnja se bo začela čez";
const char sls_SLJ_upgrade_available[] = "Nadgradnja bo na voljo po ponovnem zagonu.";
const char sls_SLJ_Natwork_unstable[] = "Omrežje ni stabilno, zato prenos ni uspel.";
const char sls_SLJ_Software_up_to_date[] = "Programska oprema je posodobljena.";
const char sls_SLJ_Fail_to_cloud[] = "Povezave z oblakom ni bilo mogoče vzpostaviti.";
const char sls_SLJ_Check_connection[] = "Preverite povezavo z omrežjem WI-FI.";
const char sls_SLJ_Store_result[] = "Ni povezave z omrežjem WI-FI. Rezultat je shranjen lokalno. Prostor:";
const char sls_SLJ_Uploading_result[] = "Nalaganje ...";
const char sls_SLJ_Result_uploaded[] = "Uspešno naloženo. Najden je bil";
const char sls_SLJ_Upload_failed[] = "Rezultata ni bilo mogoče naložiti.";
const char sls_SLJ_WIFI_connected[] = "Povezava z omrežjem WI-FI je vzpostavljena Najden je bil";
const char sls_SLJ_Memory_full[] = "NOTRANJI POMNILNIK JE POLN. Vzpostavite povezavo z omrežjem WI-FI in naložite.";
const char sls_SLJ_Data_uploaded[] = "Podatki, naloženi v oblak / shranjeni v pomnilnik naprave; prostor:";
const char sls_SLJ_Regarge_time[] = "Predviden čas polnjenja:";
      
const char sls_SLJ_NSec[] = "s.";
const char sls_SLJ_NHour[] = "h";
const char sls_SLJ_NHours[] = "h";
const char sls_SLJ_NResult_pending[] = " rezultat je v čakanju.";
const char sls_SLJ_NResult_to_upload[] = " rezultat, ki ga je treba naložiti.";
const char sls_SLJ_For_accuracy[] = "Baterije zaradi natančnosti ne smete napolniti/izprazniti v zadnjih 4 urah.";
const char sls_SLJ_unregistered_device_plz_register[] = "Neregistrirana naprava.\nRegistrirajte jo.";
const char sls_SLJ_invalid_code[] = "NEVELJAVNA KODA.";
const char sls_SLJ_network_unstable_fail_to_register[] = "Omrežje ni stabilno, zato registracija ni uspela.";
const char sls_SLJ_register_fail_code[] = "Registracija ni uspela.\nNapaka:";
const char sls_SLJ_register_successfully[] = "Registracija je uspela.";
const char sls_SLJ_fail_to_upload_unregister[] = "Rezultata ni bilo mogoče naložiti.\nNeregistrirana naprava.";
const char sls_SLJ_unregister_result_stored[] = "Neregistrirana naprava. Rezultat je shranjen lokalno. Prostor:";
#endif

#if TURKU_en
const char tk_TR_Software_available[] = "Yazılım yükseltmesi mevcut, yükseltmek için ayara gidin.";
const char tk_TR_Insert_VIN[] = "Lütfen VIN kodunu eksiksiz girin.";
const char tk_TR_Turn_on_WIFI[] = "Lütfen WIFI seçeneğini açın.";
const char tk_TR_Download_complete[] = "Karşıdan Yükleme Tamamlandı, Yükseltme İşlemi";
const char tk_TR_upgrade_available[] = "Yeniden Başlattıktan Sonra Yükseltme Yapılabilir.";
const char tk_TR_Natwork_unstable[] = "Dengesiz Ağ, Karşıdan yükleme başarısız.";
const char tk_TR_Software_up_to_date[] = "Yazılım Güncel.";
const char tk_TR_Fail_to_cloud[] = "Buluta Bağlanılamadı.";
const char tk_TR_Check_connection[] = "Lütfen WIFI Bağlantınızı kontrol edin.";
const char tk_TR_Store_result[] = "WIFI Bağlantısı Yok, Sonuç Yerel Ortama Kaydedildi. Alan:";
const char tk_TR_Uploading_result[] = "Karşıya Yükleniyor...,";
const char tk_TR_Result_uploaded[] = "Karşıya Yükleme Başarılı, Bulunan";
const char tk_TR_Upload_failed[] = "Sonuç Karşıya Yüklenemedi.";
const char tk_TR_WIFI_connected[] = "WIFI bağlantısı kuruldu Bulunan";
const char tk_TR_Memory_full[] = "DAHİLİ BELLEK DOLU, lütfen bir WIFI ağına bağlanın ve karşıya yükleyin.";
const char tk_TR_Data_uploaded[] = "Buluta Veri Yüklemesi / Cihaz Belleğine Veri Kaydı, Alan:";
const char tk_TR_Regarge_time[] = "Tahmini Şarj Süresi:";
      
const char tk_TR_NSec[] = "Saniye Sonra Başlatılacak.";
const char tk_TR_NHour[] = "Saat";
const char tk_TR_NHours[] = "Saat";
const char tk_TR_NResult_pending[] = " Sonuç Bekleniyor.";
const char tk_TR_NResult_to_upload[] = " Sonucun Karşıya Yüklenmesi Gerekiyor.";
const char tk_TR_For_accuracy[] = "Doğruluk için akü 4 saatin üzerinde şarj/deşarj edilmemelidir.";
const char tk_TR_unregistered_device_plz_register[] = "Kayıtlı olmayan cihaz,\nLütfen kaydedin";
const char tk_TR_invalid_code[] = "GEÇERSİZ KOD.";
const char tk_TR_network_unstable_fail_to_register[] = "Ağ kararsız, kaydedilemedi.";
const char tk_TR_register_fail_code[] = "Kaydedilemedi.\nHata:";
const char tk_TR_register_successfully[] = "Kaydedildi.";
const char tk_TR_fail_to_upload_unregister[] = "Sonuç Karşıya Yüklenemedi,\nKayıtlı olmayan cihaz.";
const char tk_TR_unregister_result_stored[] = "Kayıtlı olmayan cihaz, Sonuç Yerel Ortama Kaydedildi. Alan:";
#endif

#if EESTI_en
const char ee_EE_Software_available[] = "Saadaval on tarkvarauuendus, uuendamiseks ava seadistus.";
const char ee_EE_Insert_VIN[] = "Sisesta täielik VIN-kood.";
const char ee_EE_Turn_on_WIFI[] = "Lülita sisse WIFI.";
const char ee_EE_Download_complete[] = "Allalaadimine on valmis, uuendust alustatakse";
const char ee_EE_upgrade_available[] = "Uuendus on saadaval pärast taaskäivitust.";
const char ee_EE_Natwork_unstable[] = "Ebastabiilne võrk, allalaadimine nurjus.";
const char ee_EE_Software_up_to_date[] = "Tarkvara on ajakohane.";
const char ee_EE_Fail_to_cloud[] = "Pilvega ei saanud ühendust.";
const char ee_EE_Check_connection[] = "Kontrolli WIFI ühendust.";
const char ee_EE_Store_result[] = "WIFI ühendust pole, tulemus salvestati seadmesse. Ruum:";
const char ee_EE_Uploading_result[] = "Üleslaadimine...,";
const char ee_EE_Result_uploaded[] = "Edukalt üles laaditud,";
const char ee_EE_Upload_failed[] = "Tulemuse üleslaadimine nurjus.";
const char ee_EE_WIFI_connected[] = "WIFI ühendatud";
const char ee_EE_Memory_full[] = "SISEMÄLU TÄIS, ühenda WIFI võrguga ja laadi üles.";
const char ee_EE_Data_uploaded[] = "Andmete üleslaadimine pilve / Seadme mällu salvestatud andmed, ruum:";
const char ee_EE_Regarge_time[] = "Eeldatav laadimisaeg:";
      
const char ee_EE_NSec[] = "sekundi pärast.";
const char ee_EE_NHour[] = "tund";
const char ee_EE_NHours[] = "tundi";
const char ee_EE_NResult_pending[] = " tulemust ootel.";
const char ee_EE_NResult_to_upload[] = " tulemust leiti üleslaadimiseks.";
const char ee_EE_For_accuracy[] = "Täpsuse tagamise huvides ei tohiks viimase nelja tunni jooksul olla toimunud aku laadimist või täielikku tühjenemist.";
const char ee_EE_unregistered_device_plz_register[] = "Registreerimata seade,\nRegistreerige";
const char ee_EE_invalid_code[] = "VALE KOOD.";
const char ee_EE_network_unstable_fail_to_register[] = "Ebastabiilne võrk, registreerimine nurjus.";
const char ee_EE_register_fail_code[] = "Registreerimine nurjus.\nViga:";
const char ee_EE_register_successfully[] = "Registreeritud.";
const char ee_EE_fail_to_upload_unregister[] = "Tulemuse üleslaadimine nurjus,\nRegistreerimata seade.";
const char ee_EE_unregister_result_stored[] = "Registreerimata seade, tulemus salvestati seadmesse. Ruum:";
#endif

#if LATVIESU_en
const char la_LA_Software_available[] = "Pieejama programmatūras jaunināšana, dodieties uz iestatījumu, lai jauninātu.";
const char la_LA_Insert_VIN[] = "Lūdzu, ievietojiet pilnu VIN kodu.";
const char la_LA_Turn_on_WIFI[] = "Lūdzu, ieslēdziet WIFI.";
const char la_LA_Download_complete[] = "Lejupielāde pabeigta, sāk jaunināšanu pēc";
const char la_LA_upgrade_available[] = "Jaunināšana pieejama pēc restartēšanas.";
const char la_LA_Natwork_unstable[] = "Tīkls nav stabils, neizdevās lejupielādēt.";
const char la_LA_Software_up_to_date[] = "Programmatūra ir atjaunināta.";
const char la_LA_Fail_to_cloud[] = "Neizdevās savienojums ar mākoni.";
const char la_LA_Check_connection[] = "Lūdzu, pārbaudiet WIFI savienojumu.";
const char la_LA_Store_result[] = "Nav WIFI savienojuma, rezultāts saglabāts lokāli. Vieta:";
const char la_LA_Uploading_result[] = "Augšupielāde...,";
const char la_LA_Result_uploaded[] = "Augšupielāde sekmīga, atrasti";
const char la_LA_Upload_failed[] = "Neizdevās augšupielādēt rezultātus.";
const char la_LA_WIFI_connected[] = "WIFI savienots Atrasti";
const char la_LA_Memory_full[] = "IEKŠĒJĀ ATMIŅA PILNA, lūdzu, savienojiet ar WIFI tīklu un augšupielādējiet.";
const char la_LA_Data_uploaded[] = "Datu augšupielāde mākonī / dati saglabāti ierīces atmiņā, vieta:";
const char la_LA_Regarge_time[] = "Aptuvenais uzlādes laiks:";
      
const char la_LA_NSec[] = "sek.";
const char la_LA_NHour[] = "stunda";
const char la_LA_NHours[] = "stundas";
const char la_LA_NResult_pending[] = " rezultāti gaida.";
const char la_LA_NResult_to_upload[] = " rezultāti, kas jāaugšupielādē.";
const char la_LA_For_accuracy[] = "Lai iegūtu lielāku precizitāti, akumulatoru nedrīkst uzlādēt/izlādēt pēdējās 4 stundas.";
const char la_LA_unregistered_device_plz_register[] = "Nereģistrēta ierīce,\nlūdzu, reģistrējiet";
const char la_LA_invalid_code[] = "NEDERĪGS KODS.";
const char la_LA_network_unstable_fail_to_register[] = "Tīkls nav stabils, neizdevās reģistrēt.";
const char la_LA_register_fail_code[] = "Reģistrācijas neizdevās.\nKļūda:";
const char la_LA_register_successfully[] = "Reģistrācija izdevās.";
const char la_LA_fail_to_upload_unregister[] = "Neizdevās augšupielādēt rezultātus,\nnereģistrēta ierīce.";
const char la_LA_unregister_result_stored[] = "Nereģistrēta ierīce, rezultāts saglabāts lokāli. Vieta:";
#endif

#if LIMBA_ROMANA_en
const char lm_ML_Software_available[] = "Este disponibil un upgrade de software, accesați setările pentru a face upgrade.";
const char lm_ML_Insert_VIN[] = "Introduceți codul VIN complet.";
const char lm_ML_Turn_on_WIFI[] = "Activați WIFI.";
const char lm_ML_Download_complete[] = "Descărcarea s-a terminat, inițiați upgrade în";
const char lm_ML_upgrade_available[] = "Upgrade-ul va fi disponibil după repornire.";
const char lm_ML_Natwork_unstable[] = "Rețeaua este instabilă, descărcarea nu a reușit.";
const char lm_ML_Software_up_to_date[] = "Software-ul este la zi.";
const char lm_ML_Fail_to_cloud[] = "Conectarea la cloud nu a reușit.";
const char lm_ML_Check_connection[] = "Verificați conexiunea WIFI.";
const char lm_ML_Store_result[] = "Nu există conexiune WIFI, rezultatul se stochează local. Spațiu:";
const char lm_ML_Uploading_result[] = "Se încarcă...,";
const char lm_ML_Result_uploaded[] = "Încărcat cu succes, s-au găsit";
const char lm_ML_Upload_failed[] = "Încărcarea rezultatului nu a reușit.";
const char lm_ML_WIFI_connected[] = "WIFI conectat S-au găsit";
const char lm_ML_Memory_full[] = "MEMORIE INTERNĂ PLINĂ, conectați-vă la WIFI și încărcați.";
const char lm_ML_Data_uploaded[] = "Date încărcate în cloud / date salvate în memorie, spațiu:";
const char lm_ML_Regarge_time[] = "Timp estimat de reîncărcare:";
      
const char lm_ML_NSec[] = "sec.";
const char lm_ML_NHour[] = "ore";
const char lm_ML_NHours[] = "ore";
const char lm_ML_NResult_pending[] = " rezultate în așteptare.";
const char lm_ML_NResult_to_upload[] = " rezultate de încărcat.";
const char lm_ML_For_accuracy[] = "Pentru precizie, bateria nu trebuie să fi fost încărcată/descărcată în ultimele 4 ore.";
const char lm_ML_unregistered_device_plz_register[] = "Dispozitiv neînregistrat,\nvă rugăm să-l înregistrați.";
const char lm_ML_invalid_code[] = "COD INCORECT.";
const char lm_ML_network_unstable_fail_to_register[] = "Rețeaua este instabilă, înregistrarea nu a reușit.";
const char lm_ML_register_fail_code[] = "Înregistrarea nu a reușit.\nEroare:";
const char lm_ML_register_successfully[] = "Înregistrarea a reușit.";
const char lm_ML_fail_to_upload_unregister[] = "Încărcarea rezultatului nu a reușit,\nDispozitiv neînregistrat.";
const char lm_ML_unregister_result_stored[] = "Dispozitiv neînregistrat, rezultatul a fost stocat local. Spațiu:";
#endif

#if PUSSIJI_en
const char pu_RS_Software_available[] = "Доступно обновление программного обеспечения, перейдите в настройки, чтобы выполнить обновление.";
const char pu_RS_Insert_VIN[] = "Вставьте полный VIN-код.";
const char pu_RS_Turn_on_WIFI[] = "Включите WIFI.";
const char pu_RS_Download_complete[] = "Загрузка завершена, обновление начнется через";
const char pu_RS_upgrade_available[] = "Обновление доступно после перезагрузки.";
const char pu_RS_Natwork_unstable[] = "Сеть нестабильна. Не удалось загрузить.";
const char pu_RS_Software_up_to_date[] = "Программное обеспечение в актуальном состоянии.";
const char pu_RS_Fail_to_cloud[] = "Не удалось подключиться к облаку.";
const char pu_RS_Check_connection[] = "Проверьте подключение к WIFI.";
const char pu_RS_Store_result[] = "Нет подключения по WIFI, результаты сохранены локально. Свободное пространство:";
const char pu_RS_Uploading_result[] = "Выгрузка..., осталось";
const char pu_RS_Result_uploaded[] = "Успешно выгружено, необходимо выгрузить";
const char pu_RS_Upload_failed[] = "Не удалось выгрузить результаты.";
const char pu_RS_WIFI_connected[] = "Сеть WIFI подключена Необходимо выгрузить";
const char pu_RS_Memory_full[] = "ВНУТРЕННЯЯ ПАМЯТЬ ЗАПОЛНЕНА, подключитесь к сети WIFI и выгрузите.";
const char pu_RS_Data_uploaded[] = "Выгрузка данных в облако / данные сохранены в памяти устройства, свободное пространство:";
const char pu_RS_Regarge_time[] = "Ожидаемое время подзарядки:";
      
const char pu_RS_NSec[] = "сек.";
const char pu_RS_NHour[] = "ч";
const char pu_RS_NHours[] = "ч";
const char pu_RS_NResult_pending[] = " результатов.";
const char pu_RS_NResult_to_upload[] = " результатов.";
const char pu_RS_For_accuracy[] = "Для обеспечения точности не следует заряжать или разряжать аккумулятор в течение последних 4 часов.";
const char pu_RS_unregistered_device_plz_register[] = "Незарегистрированное устройство.\nВыполните регистрацию";
const char pu_RS_invalid_code[] = "НЕДОПУСТИМЫЙ КОД.";
const char pu_RS_network_unstable_fail_to_register[] = "Сеть нестабильна. Не удалось выполнить регистрацию.";
const char pu_RS_register_fail_code[] = "Регистрация не выполнена.\nОшибка:";
const char pu_RS_register_successfully[] = "Регистрация выполнена успешно.";
const char pu_RS_fail_to_upload_unregister[] = "Не удалось выгрузить результат.\nНезарегистрированное устройство.";
const char pu_RS_unregister_result_stored[] = "Незарегистрированное устройство, результат сохранен локально. Свободное пространство:";
#endif

#if EAAHNIKA_en
const char ea_GK_Software_available[] = "Υπάρχει διαθέσιμη αναβάθμιση λογισμικού. Μεταβείτε στις ρυθμίσεις για αναβάθμιση.";
const char ea_GK_Insert_VIN[] = "Εισαγάγετε τον πλήρη κωδικό VIN.";
const char ea_GK_Turn_on_WIFI[] = "Ενεργοποιήστε το WIFI.";
const char ea_GK_Download_complete[] = "Η λήψη ολοκληρώθηκε. Η αναβάθμιση θα ξεκινήσει σε";
const char ea_GK_upgrade_available[] = "Η αναβάθμιση θα είναι διαθέσιμη μετά την επανεκκίνηση.";
const char ea_GK_Natwork_unstable[] = "Το δίκτυο δεν είναι σταθερό. Η λήψη απέτυχε.";
const char ea_GK_Software_up_to_date[] = "Το λογισμικό είναι ενημερωμένο.";
const char ea_GK_Fail_to_cloud[] = "Η σύνδεση στο Cloud απέτυχε.";
const char ea_GK_Check_connection[] = "Ελέγξτε τη σύνδεση WIFI.";
const char ea_GK_Store_result[] = "Δεν πραγματοποιήθηκε σύνδεση WIFI. Το αποτέλεσμα αποθηκεύτηκε τοπικά. Χώρος:";
const char ea_GK_Uploading_result[] = "Αποστολή..., Το αποτέλεσμα";
const char ea_GK_Result_uploaded[] = "Η αποστολή ολοκληρώθηκε με επιτυχία. Απαιτείται αποστολή για το αποτέλεσμα";
const char ea_GK_Upload_failed[] = "Η αποστολή του αποτελέσματος απέτυχε.";
const char ea_GK_WIFI_connected[] = "Το WIFI συνδέθηκε Απαιτείται αποστολή για το αποτέλεσμα";
const char ea_GK_Memory_full[] = "Η ΕΣΩΤΕΡΙΚΗ ΜΝΗΜΗ ΕΙΝΑΙ ΠΛΗΡΗΣ. Συνδεθείτε σε δίκτυο WIFI και πραγματοποιήστε αποστολή.";
const char ea_GK_Data_uploaded[] = "Αποστολή δεδομένων στο Cloud / Τα δεδομένα αποθηκεύτηκαν στη μνήμη της συσκευής, Χώρος:";
const char ea_GK_Regarge_time[] = "Εκτιμώμενος χρόνος επαναφόρτισης:";
      
const char ea_GK_NSec[] = "δευτ.";
const char ea_GK_NHour[] = "ώρα";
const char ea_GK_NHours[] = "ώραες";
const char ea_GK_NResult_pending[] = " εκκρεμεί.";
const char ea_GK_NResult_to_upload[] = ".";
const char ea_GK_For_accuracy[] = "Για λόγους ακριβείας, η μπαταρία δεν πρέπει να έχει φορτιστεί/αποφορτιστεί κατά τις προηγούμενες 4 ώρες. ";
const char ea_GK_unregistered_device_plz_register[] = "Μη εγγεγραμμένη συσκευή.\nΠραγματοποιήστε εγγραφή";
const char ea_GK_invalid_code[] = "Ο ΚΩΔΙΚΟΣ ΔΕΝ ΕΙΝΑΙ ΕΓΚΥΡΟΣ.";
const char ea_GK_network_unstable_fail_to_register[] = "Το δίκτυο δεν είναι σταθερό. Η εγγραφή απέτυχε.";
const char ea_GK_register_fail_code[] = "Η εγγραφή απέτυχε.\nErr:";
const char ea_GK_register_successfully[] = "Επιτυχία εγγραφής.";
const char ea_GK_fail_to_upload_unregister[] = "Η αποστολή του αποτελέσματος απέτυχε.\nΜη εγγεγραμμένη συσκευή.";
const char ea_GK_unregister_result_stored[] = "Μη εγγεγραμμένη συσκευή. Το αποτέλεσμα αποθηκεύτηκε τοπικά. Χώρος:";
#endif

const char *message_list_Software_available[] = {
#if ENGLISH_en
	en_US_Software_available,
#endif
#if FRANCAIS_en
	fr_FR_Software_available,
#endif
#if DEUTSCH_en
	de_DE_Software_available,
#endif
#if ESPANOL_en
	es_ES_Software_available,
#endif
#if ITALIAN_en
	it_IT_Software_available,
#endif
#if PORTUGUES_en
	pt_PT_Software_available,
#endif
#if NEDERLANDS_en
	nd_ND_Software_available,
#endif
#if DANSK_en
	dk_DK_Software_available,
#endif
#if NORSK_en
	nl_NW_Software_available,
#endif
#if SOOMI_en
	sm_FN_Software_available,
#endif
#if SVENSKA_en
	sv_SW_Software_available,
#endif
#if CESTINA_en
	cs_CZ_Software_available,
#endif
#if LIETUVIU_K_en
	lt_LT_Software_available,
#endif
#if POLSKI_en
	pl_PL_Software_available,
#endif
#if SLOVENCINA_en
	sl_SL_Software_available,
#endif
#if SLOVENSCINA_en
	sls_SLJ_Software_available,
#endif
#if TURKU_en
	tk_TR_Software_available,
#endif
#if EESTI_en
	ee_EE_Software_available,
#endif
#if LATVIESU_en
	la_LA_Software_available,
#endif
#if LIMBA_ROMANA_en
	lm_ML_Software_available,
#endif
#if PUSSIJI_en
	pu_RS_Software_available,
#endif
#if EAAHNIKA_en
	ea_GK_Software_available,
#endif
};

const char *message_list_Insert_VIN[] = {
#if ENGLISH_en
	en_US_Insert_VIN,
#endif
#if FRANCAIS_en
	fr_FR_Insert_VIN,
#endif
#if DEUTSCH_en
	de_DE_Insert_VIN,
#endif
#if ESPANOL_en
	es_ES_Insert_VIN,
#endif
#if ITALIAN_en
	it_IT_Insert_VIN,
#endif
#if PORTUGUES_en
	pt_PT_Insert_VIN,
#endif
#if NEDERLANDS_en
	nd_ND_Insert_VIN,
#endif
#if DANSK_en
	dk_DK_Insert_VIN,
#endif
#if NORSK_en
	nl_NW_Insert_VIN,
#endif
#if SOOMI_en
	sm_FN_Insert_VIN,
#endif
#if SVENSKA_en
	sv_SW_Insert_VIN,
#endif
#if CESTINA_en
	cs_CZ_Insert_VIN,
#endif
#if LIETUVIU_K_en
	lt_LT_Insert_VIN,
#endif
#if POLSKI_en
	pl_PL_Insert_VIN,
#endif
#if SLOVENCINA_en
	sl_SL_Insert_VIN,
#endif
#if SLOVENSCINA_en
	sls_SLJ_Insert_VIN,
#endif
#if TURKU_en
	tk_TR_Insert_VIN,
#endif
#if EESTI_en
	ee_EE_Insert_VIN,
#endif
#if LATVIESU_en
	la_LA_Insert_VIN,
#endif
#if LIMBA_ROMANA_en
	lm_ML_Insert_VIN,
#endif
#if PUSSIJI_en
	pu_RS_Insert_VIN,
#endif
#if EAAHNIKA_en
	ea_GK_Insert_VIN,
#endif
};

const char *message_list_Turn_on_WIFI[] = {
#if ENGLISH_en
	en_US_Turn_on_WIFI,
#endif
#if FRANCAIS_en
	fr_FR_Turn_on_WIFI,
#endif
#if DEUTSCH_en
	de_DE_Turn_on_WIFI,
#endif
#if ESPANOL_en
	es_ES_Turn_on_WIFI,
#endif
#if ITALIAN_en
	it_IT_Turn_on_WIFI,
#endif
#if PORTUGUES_en
	pt_PT_Turn_on_WIFI,
#endif
#if NEDERLANDS_en
	nd_ND_Turn_on_WIFI,
#endif
#if DANSK_en
	dk_DK_Turn_on_WIFI,
#endif
#if NORSK_en
	nl_NW_Turn_on_WIFI,
#endif
#if SOOMI_en
	sm_FN_Turn_on_WIFI,
#endif
#if SVENSKA_en
	sv_SW_Turn_on_WIFI,
#endif
#if CESTINA_en
	cs_CZ_Turn_on_WIFI,
#endif
#if LIETUVIU_K_en
	lt_LT_Turn_on_WIFI,
#endif
#if POLSKI_en
	pl_PL_Turn_on_WIFI,
#endif
#if SLOVENCINA_en
	sl_SL_Turn_on_WIFI,
#endif
#if SLOVENSCINA_en
	sls_SLJ_Turn_on_WIFI,
#endif
#if TURKU_en
	tk_TR_Turn_on_WIFI,
#endif
#if EESTI_en
	ee_EE_Turn_on_WIFI,
#endif
#if LATVIESU_en
	la_LA_Turn_on_WIFI,
#endif
#if LIMBA_ROMANA_en
	lm_ML_Turn_on_WIFI,
#endif
#if PUSSIJI_en
	pu_RS_Turn_on_WIFI,
#endif
#if EAAHNIKA_en
	ea_GK_Turn_on_WIFI,
#endif
};

const char *message_list_Download_complete[] = {
#if ENGLISH_en
	en_US_Download_complete,
#endif
#if FRANCAIS_en
	fr_FR_Download_complete,
#endif
#if DEUTSCH_en
	de_DE_Download_complete,
#endif
#if ESPANOL_en
	es_ES_Download_complete,
#endif
#if ITALIAN_en
	it_IT_Download_complete,
#endif
#if PORTUGUES_en
	pt_PT_Download_complete,
#endif
#if NEDERLANDS_en
	nd_ND_Download_complete,
#endif
#if DANSK_en
	dk_DK_Download_complete,
#endif
#if NORSK_en
	nl_NW_Download_complete,
#endif
#if SOOMI_en
	sm_FN_Download_complete,
#endif
#if SVENSKA_en
	sv_SW_Download_complete,
#endif
#if CESTINA_en
	cs_CZ_Download_complete,
#endif
#if LIETUVIU_K_en
	lt_LT_Download_complete,
#endif
#if POLSKI_en
	pl_PL_Download_complete,
#endif
#if SLOVENCINA_en
	sl_SL_Download_complete,
#endif
#if SLOVENSCINA_en
	sls_SLJ_Download_complete,
#endif
#if TURKU_en
	tk_TR_Download_complete,
#endif
#if EESTI_en
	ee_EE_Download_complete,
#endif
#if LATVIESU_en
	la_LA_Download_complete,
#endif
#if LIMBA_ROMANA_en
	lm_ML_Download_complete,
#endif
#if PUSSIJI_en
	pu_RS_Download_complete,
#endif
#if EAAHNIKA_en
	ea_GK_Download_complete,
#endif
};

const char *message_list_upgrade_available[] = {
#if ENGLISH_en
	en_US_upgrade_available,
#endif
#if FRANCAIS_en
	fr_FR_upgrade_available,
#endif
#if DEUTSCH_en
	de_DE_upgrade_available,
#endif
#if ESPANOL_en
	es_ES_upgrade_available,
#endif
#if ITALIAN_en
	it_IT_upgrade_available,
#endif
#if PORTUGUES_en
	pt_PT_upgrade_available,
#endif
#if NEDERLANDS_en
	nd_ND_upgrade_available,
#endif
#if DANSK_en
	dk_DK_upgrade_available,
#endif
#if NORSK_en
	nl_NW_upgrade_available,
#endif
#if SOOMI_en
	sm_FN_upgrade_available,
#endif
#if SVENSKA_en
	sv_SW_upgrade_available,
#endif
#if CESTINA_en
	cs_CZ_upgrade_available,
#endif
#if LIETUVIU_K_en
	lt_LT_upgrade_available,
#endif
#if POLSKI_en
	pl_PL_upgrade_available,
#endif
#if SLOVENCINA_en
	sl_SL_upgrade_available,
#endif
#if SLOVENSCINA_en
	sls_SLJ_upgrade_available,
#endif
#if TURKU_en
	tk_TR_upgrade_available,
#endif
#if EESTI_en
	ee_EE_upgrade_available,
#endif
#if LATVIESU_en
	la_LA_upgrade_available,
#endif
#if LIMBA_ROMANA_en
	lm_ML_upgrade_available,
#endif
#if PUSSIJI_en
	pu_RS_upgrade_available,
#endif
#if EAAHNIKA_en
	ea_GK_upgrade_available,
#endif
};

const char *message_list_Natwork_unstable[] = {
#if ENGLISH_en
	en_US_Natwork_unstable,
#endif
#if FRANCAIS_en
	fr_FR_Natwork_unstable,
#endif
#if DEUTSCH_en
	de_DE_Natwork_unstable,
#endif
#if ESPANOL_en
	es_ES_Natwork_unstable,
#endif
#if ITALIAN_en
	it_IT_Natwork_unstable,
#endif
#if PORTUGUES_en
	pt_PT_Natwork_unstable,
#endif
#if NEDERLANDS_en
	nd_ND_Natwork_unstable,
#endif
#if DANSK_en
	dk_DK_Natwork_unstable,
#endif
#if NORSK_en
	nl_NW_Natwork_unstable,
#endif
#if SOOMI_en
	sm_FN_Natwork_unstable,
#endif
#if SVENSKA_en
	sv_SW_Natwork_unstable,
#endif
#if CESTINA_en
	cs_CZ_Natwork_unstable,
#endif
#if LIETUVIU_K_en
	lt_LT_Natwork_unstable,
#endif
#if POLSKI_en
	pl_PL_Natwork_unstable,
#endif
#if SLOVENCINA_en
	sl_SL_Natwork_unstable,
#endif
#if SLOVENSCINA_en
	sls_SLJ_Natwork_unstable,
#endif
#if TURKU_en
	tk_TR_Natwork_unstable,
#endif
#if EESTI_en
	ee_EE_Natwork_unstable,
#endif
#if LATVIESU_en
	la_LA_Natwork_unstable,
#endif
#if LIMBA_ROMANA_en
	lm_ML_Natwork_unstable,
#endif
#if PUSSIJI_en
	pu_RS_Natwork_unstable,
#endif
#if EAAHNIKA_en
	ea_GK_Natwork_unstable,
#endif
};

const char *message_list_Software_up_to_date[] = {
#if ENGLISH_en
	en_US_Software_up_to_date,
#endif
#if FRANCAIS_en
	fr_FR_Software_up_to_date,
#endif
#if DEUTSCH_en
	de_DE_Software_up_to_date,
#endif
#if ESPANOL_en
	es_ES_Software_up_to_date,
#endif
#if ITALIAN_en
	it_IT_Software_up_to_date,
#endif
#if PORTUGUES_en
	pt_PT_Software_up_to_date,
#endif
#if NEDERLANDS_en
	nd_ND_Software_up_to_date,
#endif
#if DANSK_en
	dk_DK_Software_up_to_date,
#endif
#if NORSK_en
	nl_NW_Software_up_to_date,
#endif
#if SOOMI_en
	sm_FN_Software_up_to_date,
#endif
#if SVENSKA_en
	sv_SW_Software_up_to_date,
#endif
#if CESTINA_en
	cs_CZ_Software_up_to_date,
#endif
#if LIETUVIU_K_en
	lt_LT_Software_up_to_date,
#endif
#if POLSKI_en
	pl_PL_Software_up_to_date,
#endif
#if SLOVENCINA_en
	sl_SL_Software_up_to_date,
#endif
#if SLOVENSCINA_en
	sls_SLJ_Software_up_to_date,
#endif
#if TURKU_en
	tk_TR_Software_up_to_date,
#endif
#if EESTI_en
	ee_EE_Software_up_to_date,
#endif
#if LATVIESU_en
	la_LA_Software_up_to_date,
#endif
#if LIMBA_ROMANA_en
	lm_ML_Software_up_to_date,
#endif
#if PUSSIJI_en
	pu_RS_Software_up_to_date,
#endif
#if EAAHNIKA_en
	ea_GK_Software_up_to_date,
#endif
};

const char *message_list_Fail_to_cloud[] = {
#if ENGLISH_en
	en_US_Fail_to_cloud,
#endif
#if FRANCAIS_en
	fr_FR_Fail_to_cloud,
#endif
#if DEUTSCH_en
	de_DE_Fail_to_cloud,
#endif
#if ESPANOL_en
	es_ES_Fail_to_cloud,
#endif
#if ITALIAN_en
	it_IT_Fail_to_cloud,
#endif
#if PORTUGUES_en
	pt_PT_Fail_to_cloud,
#endif
#if NEDERLANDS_en
	nd_ND_Fail_to_cloud,
#endif
#if DANSK_en
	dk_DK_Fail_to_cloud,
#endif
#if NORSK_en
	nl_NW_Fail_to_cloud,
#endif
#if SOOMI_en
	sm_FN_Fail_to_cloud,
#endif
#if SVENSKA_en
	sv_SW_Fail_to_cloud,
#endif
#if CESTINA_en
	cs_CZ_Fail_to_cloud,
#endif
#if LIETUVIU_K_en
	lt_LT_Fail_to_cloud,
#endif
#if POLSKI_en
	pl_PL_Fail_to_cloud,
#endif
#if SLOVENCINA_en
	sl_SL_Fail_to_cloud,
#endif
#if SLOVENSCINA_en
	sls_SLJ_Fail_to_cloud,
#endif
#if TURKU_en
	tk_TR_Fail_to_cloud,
#endif
#if EESTI_en
	ee_EE_Fail_to_cloud,
#endif
#if LATVIESU_en
	la_LA_Fail_to_cloud,
#endif
#if LIMBA_ROMANA_en
	lm_ML_Fail_to_cloud,
#endif
#if PUSSIJI_en
	pu_RS_Fail_to_cloud,
#endif
#if EAAHNIKA_en
	ea_GK_Fail_to_cloud,
#endif
};

const char *message_list_Check_connection[] = {
#if ENGLISH_en
	en_US_Check_connection,
#endif
#if FRANCAIS_en
	fr_FR_Check_connection,
#endif
#if DEUTSCH_en
	de_DE_Check_connection,
#endif
#if ESPANOL_en
	es_ES_Check_connection,
#endif
#if ITALIAN_en
	it_IT_Check_connection,
#endif
#if PORTUGUES_en
	pt_PT_Check_connection,
#endif
#if NEDERLANDS_en
	nd_ND_Check_connection,
#endif
#if DANSK_en
	dk_DK_Check_connection,
#endif
#if NORSK_en
	nl_NW_Check_connection,
#endif
#if SOOMI_en
	sm_FN_Check_connection,
#endif
#if SVENSKA_en
	sv_SW_Check_connection,
#endif
#if CESTINA_en
	cs_CZ_Check_connection,
#endif
#if LIETUVIU_K_en
	lt_LT_Check_connection,
#endif
#if POLSKI_en
	pl_PL_Check_connection,
#endif
#if SLOVENCINA_en
	sl_SL_Check_connection,
#endif
#if SLOVENSCINA_en
	sls_SLJ_Check_connection,
#endif
#if TURKU_en
	tk_TR_Check_connection,
#endif
#if EESTI_en
	ee_EE_Check_connection,
#endif
#if LATVIESU_en
	la_LA_Check_connection,
#endif
#if LIMBA_ROMANA_en
	lm_ML_Check_connection,
#endif
#if PUSSIJI_en
	pu_RS_Check_connection,
#endif
#if EAAHNIKA_en
	ea_GK_Check_connection,
#endif
};

const char *message_list_Store_result[] = {
#if ENGLISH_en
	en_US_Store_result,
#endif
#if FRANCAIS_en
	fr_FR_Store_result,
#endif
#if DEUTSCH_en
	de_DE_Store_result,
#endif
#if ESPANOL_en
	es_ES_Store_result,
#endif
#if ITALIAN_en
	it_IT_Store_result,
#endif
#if PORTUGUES_en
	pt_PT_Store_result,
#endif
#if NEDERLANDS_en
	nd_ND_Store_result,
#endif
#if DANSK_en
	dk_DK_Store_result,
#endif
#if NORSK_en
	nl_NW_Store_result,
#endif
#if SOOMI_en
	sm_FN_Store_result,
#endif
#if SVENSKA_en
	sv_SW_Store_result,
#endif
#if CESTINA_en
	cs_CZ_Store_result,
#endif
#if LIETUVIU_K_en
	lt_LT_Store_result,
#endif
#if POLSKI_en
	pl_PL_Store_result,
#endif
#if SLOVENCINA_en
	sl_SL_Store_result,
#endif
#if SLOVENSCINA_en
	sls_SLJ_Store_result,
#endif
#if TURKU_en
	tk_TR_Store_result,
#endif
#if EESTI_en
	ee_EE_Store_result,
#endif
#if LATVIESU_en
	la_LA_Store_result,
#endif
#if LIMBA_ROMANA_en
	lm_ML_Store_result,
#endif
#if PUSSIJI_en
	pu_RS_Store_result,
#endif
#if EAAHNIKA_en
	ea_GK_Store_result,
#endif
};

const char *message_list_Uploading_result[] = {
#if ENGLISH_en
	en_US_Uploading_result,
#endif
#if FRANCAIS_en
	fr_FR_Uploading_result,
#endif
#if DEUTSCH_en
	de_DE_Uploading_result,
#endif
#if ESPANOL_en
	es_ES_Uploading_result,
#endif
#if ITALIAN_en
	it_IT_Uploading_result,
#endif
#if PORTUGUES_en
	pt_PT_Uploading_result,
#endif
#if NEDERLANDS_en
	nd_ND_Uploading_result,
#endif
#if DANSK_en
	dk_DK_Uploading_result,
#endif
#if NORSK_en
	nl_NW_Uploading_result,
#endif
#if SOOMI_en
	sm_FN_Uploading_result,
#endif
#if SVENSKA_en
	sv_SW_Uploading_result,
#endif
#if CESTINA_en
	cs_CZ_Uploading_result,
#endif
#if LIETUVIU_K_en
	lt_LT_Uploading_result,
#endif
#if POLSKI_en
	pl_PL_Uploading_result,
#endif
#if SLOVENCINA_en
	sl_SL_Uploading_result,
#endif
#if SLOVENSCINA_en
	sls_SLJ_Uploading_result,
#endif
#if TURKU_en
	tk_TR_Uploading_result,
#endif
#if EESTI_en
	ee_EE_Uploading_result,
#endif
#if LATVIESU_en
	la_LA_Uploading_result,
#endif
#if LIMBA_ROMANA_en
	lm_ML_Uploading_result,
#endif
#if PUSSIJI_en
	pu_RS_Uploading_result,
#endif
#if EAAHNIKA_en
	ea_GK_Uploading_result,
#endif
};

const char *message_list_Result_uploaded[] = {
#if ENGLISH_en
	en_US_Result_uploaded,
#endif
#if FRANCAIS_en
	fr_FR_Result_uploaded,
#endif
#if DEUTSCH_en
	de_DE_Result_uploaded,
#endif
#if ESPANOL_en
	es_ES_Result_uploaded,
#endif
#if ITALIAN_en
	it_IT_Result_uploaded,
#endif
#if PORTUGUES_en
	pt_PT_Result_uploaded,
#endif
#if NEDERLANDS_en
	nd_ND_Result_uploaded,
#endif
#if DANSK_en
	dk_DK_Result_uploaded,
#endif
#if NORSK_en
	nl_NW_Result_uploaded,
#endif
#if SOOMI_en
	sm_FN_Result_uploaded,
#endif
#if SVENSKA_en
	sv_SW_Result_uploaded,
#endif
#if CESTINA_en
	cs_CZ_Result_uploaded,
#endif
#if LIETUVIU_K_en
	lt_LT_Result_uploaded,
#endif
#if POLSKI_en
	pl_PL_Result_uploaded,
#endif
#if SLOVENCINA_en
	sl_SL_Result_uploaded,
#endif
#if SLOVENSCINA_en
	sls_SLJ_Result_uploaded,
#endif
#if TURKU_en
	tk_TR_Result_uploaded,
#endif
#if EESTI_en
	ee_EE_Result_uploaded,
#endif
#if LATVIESU_en
	la_LA_Result_uploaded,
#endif
#if LIMBA_ROMANA_en
	lm_ML_Result_uploaded,
#endif
#if PUSSIJI_en
	pu_RS_Result_uploaded,
#endif
#if EAAHNIKA_en
	ea_GK_Result_uploaded,
#endif
};

const char *message_list_Upload_failed[] = {
#if ENGLISH_en
	en_US_Upload_failed,
#endif
#if FRANCAIS_en
	fr_FR_Upload_failed,
#endif
#if DEUTSCH_en
	de_DE_Upload_failed,
#endif
#if ESPANOL_en
	es_ES_Upload_failed,
#endif
#if ITALIAN_en
	it_IT_Upload_failed,
#endif
#if PORTUGUES_en
	pt_PT_Upload_failed,
#endif
#if NEDERLANDS_en
	nd_ND_Upload_failed,
#endif
#if DANSK_en
	dk_DK_Upload_failed,
#endif
#if NORSK_en
	nl_NW_Upload_failed,
#endif
#if SOOMI_en
	sm_FN_Upload_failed,
#endif
#if SVENSKA_en
	sv_SW_Upload_failed,
#endif
#if CESTINA_en
	cs_CZ_Upload_failed,
#endif
#if LIETUVIU_K_en
	lt_LT_Upload_failed,
#endif
#if POLSKI_en
	pl_PL_Upload_failed,
#endif
#if SLOVENCINA_en
	sl_SL_Upload_failed,
#endif
#if SLOVENSCINA_en
	sls_SLJ_Upload_failed,
#endif
#if TURKU_en
	tk_TR_Upload_failed,
#endif
#if EESTI_en
	ee_EE_Upload_failed,
#endif
#if LATVIESU_en
	la_LA_Upload_failed,
#endif
#if LIMBA_ROMANA_en
	lm_ML_Upload_failed,
#endif
#if PUSSIJI_en
	pu_RS_Upload_failed,
#endif
#if EAAHNIKA_en
	ea_GK_Upload_failed,
#endif
};

const char *message_list_WIFI_connected[] = {
#if ENGLISH_en
	en_US_WIFI_connected,
#endif
#if FRANCAIS_en
	fr_FR_WIFI_connected,
#endif
#if DEUTSCH_en
	de_DE_WIFI_connected,
#endif
#if ESPANOL_en
	es_ES_WIFI_connected,
#endif
#if ITALIAN_en
	it_IT_WIFI_connected,
#endif
#if PORTUGUES_en
	pt_PT_WIFI_connected,
#endif
#if NEDERLANDS_en
	nd_ND_WIFI_connected,
#endif
#if DANSK_en
	dk_DK_WIFI_connected,
#endif
#if NORSK_en
	nl_NW_WIFI_connected,
#endif
#if SOOMI_en
	sm_FN_WIFI_connected,
#endif
#if SVENSKA_en
	sv_SW_WIFI_connected,
#endif
#if CESTINA_en
	cs_CZ_WIFI_connected,
#endif
#if LIETUVIU_K_en
	lt_LT_WIFI_connected,
#endif
#if POLSKI_en
	pl_PL_WIFI_connected,
#endif
#if SLOVENCINA_en
	sl_SL_WIFI_connected,
#endif
#if SLOVENSCINA_en
	sls_SLJ_WIFI_connected,
#endif
#if TURKU_en
	tk_TR_WIFI_connected,
#endif
#if EESTI_en
	ee_EE_WIFI_connected,
#endif
#if LATVIESU_en
	la_LA_WIFI_connected,
#endif
#if LIMBA_ROMANA_en
	lm_ML_WIFI_connected,
#endif
#if PUSSIJI_en
	pu_RS_WIFI_connected,
#endif
#if EAAHNIKA_en
	ea_GK_WIFI_connected,
#endif
};

const char *message_list_Memory_full[] = {
#if ENGLISH_en
	en_US_Memory_full,
#endif
#if FRANCAIS_en
	fr_FR_Memory_full,
#endif
#if DEUTSCH_en
	de_DE_Memory_full,
#endif
#if ESPANOL_en
	es_ES_Memory_full,
#endif
#if ITALIAN_en
	it_IT_Memory_full,
#endif
#if PORTUGUES_en
	pt_PT_Memory_full,
#endif
#if NEDERLANDS_en
	nd_ND_Memory_full,
#endif
#if DANSK_en
	dk_DK_Memory_full,
#endif
#if NORSK_en
	nl_NW_Memory_full,
#endif
#if SOOMI_en
	sm_FN_Memory_full,
#endif
#if SVENSKA_en
	sv_SW_Memory_full,
#endif
#if CESTINA_en
	cs_CZ_Memory_full,
#endif
#if LIETUVIU_K_en
	lt_LT_Memory_full,
#endif
#if POLSKI_en
	pl_PL_Memory_full,
#endif
#if SLOVENCINA_en
	sl_SL_Memory_full,
#endif
#if SLOVENSCINA_en
	sls_SLJ_Memory_full,
#endif
#if TURKU_en
	tk_TR_Memory_full,
#endif
#if EESTI_en
	ee_EE_Memory_full,
#endif
#if LATVIESU_en
	la_LA_Memory_full,
#endif
#if LIMBA_ROMANA_en
	lm_ML_Memory_full,
#endif
#if PUSSIJI_en
	pu_RS_Memory_full,
#endif
#if EAAHNIKA_en
	ea_GK_Memory_full,
#endif
};

const char *message_list_Data_uploaded[] = {
#if ENGLISH_en
	en_US_Data_uploaded,
#endif
#if FRANCAIS_en
	fr_FR_Data_uploaded,
#endif
#if DEUTSCH_en
	de_DE_Data_uploaded,
#endif
#if ESPANOL_en
	es_ES_Data_uploaded,
#endif
#if ITALIAN_en
	it_IT_Data_uploaded,
#endif
#if PORTUGUES_en
	pt_PT_Data_uploaded,
#endif
#if NEDERLANDS_en
	nd_ND_Data_uploaded,
#endif
#if DANSK_en
	dk_DK_Data_uploaded,
#endif
#if NORSK_en
	nl_NW_Data_uploaded,
#endif
#if SOOMI_en
	sm_FN_Data_uploaded,
#endif
#if SVENSKA_en
	sv_SW_Data_uploaded,
#endif
#if CESTINA_en
	cs_CZ_Data_uploaded,
#endif
#if LIETUVIU_K_en
	lt_LT_Data_uploaded,
#endif
#if POLSKI_en
	pl_PL_Data_uploaded,
#endif
#if SLOVENCINA_en
	sl_SL_Data_uploaded,
#endif
#if SLOVENSCINA_en
	sls_SLJ_Data_uploaded,
#endif
#if TURKU_en
	tk_TR_Data_uploaded,
#endif
#if EESTI_en
	ee_EE_Data_uploaded,
#endif
#if LATVIESU_en
	la_LA_Data_uploaded,
#endif
#if LIMBA_ROMANA_en
	lm_ML_Data_uploaded,
#endif
#if PUSSIJI_en
	pu_RS_Data_uploaded,
#endif
#if EAAHNIKA_en
	ea_GK_Data_uploaded,
#endif
};

const char *message_list_Regarge_time[] = {
#if ENGLISH_en
	en_US_Regarge_time,
#endif
#if FRANCAIS_en
	fr_FR_Regarge_time,
#endif
#if DEUTSCH_en
	de_DE_Regarge_time,
#endif
#if ESPANOL_en
	es_ES_Regarge_time,
#endif
#if ITALIAN_en
	it_IT_Regarge_time,
#endif
#if PORTUGUES_en
	pt_PT_Regarge_time,
#endif
#if NEDERLANDS_en
	nd_ND_Regarge_time,
#endif
#if DANSK_en
	dk_DK_Regarge_time,
#endif
#if NORSK_en
	nl_NW_Regarge_time,
#endif
#if SOOMI_en
	sm_FN_Regarge_time,
#endif
#if SVENSKA_en
	sv_SW_Regarge_time,
#endif
#if CESTINA_en
	cs_CZ_Regarge_time,
#endif
#if LIETUVIU_K_en
	lt_LT_Regarge_time,
#endif
#if POLSKI_en
	pl_PL_Regarge_time,
#endif
#if SLOVENCINA_en
	sl_SL_Regarge_time,
#endif
#if SLOVENSCINA_en
	sls_SLJ_Regarge_time,
#endif
#if TURKU_en
	tk_TR_Regarge_time,
#endif
#if EESTI_en
	ee_EE_Regarge_time,
#endif
#if LATVIESU_en
	la_LA_Regarge_time,
#endif
#if LIMBA_ROMANA_en
	lm_ML_Regarge_time,
#endif
#if PUSSIJI_en
	pu_RS_Regarge_time,
#endif
#if EAAHNIKA_en
	ea_GK_Regarge_time,
#endif
};

const char *message_list_NSec[] = {
#if ENGLISH_en
	en_US_NSec,
#endif
#if FRANCAIS_en
	fr_FR_NSec,
#endif
#if DEUTSCH_en
	de_DE_NSec,
#endif
#if ESPANOL_en
	es_ES_NSec,
#endif
#if ITALIAN_en
	it_IT_NSec,
#endif
#if PORTUGUES_en
	pt_PT_NSec,
#endif
#if NEDERLANDS_en
	nd_ND_NSec,
#endif
#if DANSK_en
	dk_DK_NSec,
#endif
#if NORSK_en
	nl_NW_NSec,
#endif
#if SOOMI_en
	sm_FN_NSec,
#endif
#if SVENSKA_en
	sv_SW_NSec,
#endif
#if CESTINA_en
	cs_CZ_NSec,
#endif
#if LIETUVIU_K_en
	lt_LT_NSec,
#endif
#if POLSKI_en
	pl_PL_NSec,
#endif
#if SLOVENCINA_en
	sl_SL_NSec,
#endif
#if SLOVENSCINA_en
	sls_SLJ_NSec,
#endif
#if TURKU_en
	tk_TR_NSec,
#endif
#if EESTI_en
	ee_EE_NSec,
#endif
#if LATVIESU_en
	la_LA_NSec,
#endif
#if LIMBA_ROMANA_en
	lm_ML_NSec,
#endif
#if PUSSIJI_en
	pu_RS_NSec,
#endif
#if EAAHNIKA_en
	ea_GK_NSec,
#endif
};

const char *message_list_NHour[] = {
#if ENGLISH_en
	en_US_NHour,
#endif
#if FRANCAIS_en
	fr_FR_NHour,
#endif
#if DEUTSCH_en
	de_DE_NHour,
#endif
#if ESPANOL_en
	es_ES_NHour,
#endif
#if ITALIAN_en
	it_IT_NHour,
#endif
#if PORTUGUES_en
	pt_PT_NHour,
#endif
#if NEDERLANDS_en
	nd_ND_NHour,
#endif
#if DANSK_en
	dk_DK_NHour,
#endif
#if NORSK_en
	nl_NW_NHour,
#endif
#if SOOMI_en
	sm_FN_NHour,
#endif
#if SVENSKA_en
	sv_SW_NHour,
#endif
#if CESTINA_en
	cs_CZ_NHour,
#endif
#if LIETUVIU_K_en
	lt_LT_NHour,
#endif
#if POLSKI_en
	pl_PL_NHour,
#endif
#if SLOVENCINA_en
	sl_SL_NHour,
#endif
#if SLOVENSCINA_en
	sls_SLJ_NHour,
#endif
#if TURKU_en
	tk_TR_NHour,
#endif
#if EESTI_en
	ee_EE_NHour,
#endif
#if LATVIESU_en
	la_LA_NHour,
#endif
#if LIMBA_ROMANA_en
	lm_ML_NHour,
#endif
#if PUSSIJI_en
	pu_RS_NHour,
#endif
#if EAAHNIKA_en
	ea_GK_NHour,
#endif
};

const char *message_list_NHours[] = {
#if ENGLISH_en
	en_US_NHours,
#endif
#if FRANCAIS_en
	fr_FR_NHours,
#endif
#if DEUTSCH_en
	de_DE_NHours,
#endif
#if ESPANOL_en
	es_ES_NHours,
#endif
#if ITALIAN_en
	it_IT_NHours,
#endif
#if PORTUGUES_en
	pt_PT_NHours,
#endif
#if NEDERLANDS_en
	nd_ND_NHours,
#endif
#if DANSK_en
	dk_DK_NHours,
#endif
#if NORSK_en
	nl_NW_NHours,
#endif
#if SOOMI_en
	sm_FN_NHours,
#endif
#if SVENSKA_en
	sv_SW_NHours,
#endif
#if CESTINA_en
	cs_CZ_NHours,
#endif
#if LIETUVIU_K_en
	lt_LT_NHours,
#endif
#if POLSKI_en
	pl_PL_NHours,
#endif
#if SLOVENCINA_en
	sl_SL_NHours,
#endif
#if SLOVENSCINA_en
	sls_SLJ_NHours,
#endif
#if TURKU_en
	tk_TR_NHours,
#endif
#if EESTI_en
	ee_EE_NHours,
#endif
#if LATVIESU_en
	la_LA_NHours,
#endif
#if LIMBA_ROMANA_en
	lm_ML_NHours,
#endif
#if PUSSIJI_en
	pu_RS_NHours,
#endif
#if EAAHNIKA_en
	ea_GK_NHours,
#endif
};

const char *message_list_NResult_pending[] = {
#if ENGLISH_en
	en_US_NResult_pending,
#endif
#if FRANCAIS_en
	fr_FR_NResult_pending,
#endif
#if DEUTSCH_en
	de_DE_NResult_pending,
#endif
#if ESPANOL_en
	es_ES_NResult_pending,
#endif
#if ITALIAN_en
	it_IT_NResult_pending,
#endif
#if PORTUGUES_en
	pt_PT_NResult_pending,
#endif
#if NEDERLANDS_en
	nd_ND_NResult_pending,
#endif
#if DANSK_en
	dk_DK_NResult_pending,
#endif
#if NORSK_en
	nl_NW_NResult_pending,
#endif
#if SOOMI_en
	sm_FN_NResult_pending,
#endif
#if SVENSKA_en
	sv_SW_NResult_pending,
#endif
#if CESTINA_en
	cs_CZ_NResult_pending,
#endif
#if LIETUVIU_K_en
	lt_LT_NResult_pending,
#endif
#if POLSKI_en
	pl_PL_NResult_pending,
#endif
#if SLOVENCINA_en
	sl_SL_NResult_pending,
#endif
#if SLOVENSCINA_en
	sls_SLJ_NResult_pending,
#endif
#if TURKU_en
	tk_TR_NResult_pending,
#endif
#if EESTI_en
	ee_EE_NResult_pending,
#endif
#if LATVIESU_en
	la_LA_NResult_pending,
#endif
#if LIMBA_ROMANA_en
	lm_ML_NResult_pending,
#endif
#if PUSSIJI_en
	pu_RS_NResult_pending,
#endif
#if EAAHNIKA_en
	ea_GK_NResult_pending,
#endif
};

const char *message_list_NResult_to_upload[] = {
#if ENGLISH_en
	en_US_NResult_to_upload,
#endif
#if FRANCAIS_en
	fr_FR_NResult_to_upload,
#endif
#if DEUTSCH_en
	de_DE_NResult_to_upload,
#endif
#if ESPANOL_en
	es_ES_NResult_to_upload,
#endif
#if ITALIAN_en
	it_IT_NResult_to_upload,
#endif
#if PORTUGUES_en
	pt_PT_NResult_to_upload,
#endif
#if NEDERLANDS_en
	nd_ND_NResult_to_upload,
#endif
#if DANSK_en
	dk_DK_NResult_to_upload,
#endif
#if NORSK_en
	nl_NW_NResult_to_upload,
#endif
#if SOOMI_en
	sm_FN_NResult_to_upload,
#endif
#if SVENSKA_en
	sv_SW_NResult_to_upload,
#endif
#if CESTINA_en
	cs_CZ_NResult_to_upload,
#endif
#if LIETUVIU_K_en
	lt_LT_NResult_to_upload,
#endif
#if POLSKI_en
	pl_PL_NResult_to_upload,
#endif
#if SLOVENCINA_en
	sl_SL_NResult_to_upload,
#endif
#if SLOVENSCINA_en
	sls_SLJ_NResult_to_upload,
#endif
#if TURKU_en
	tk_TR_NResult_to_upload,
#endif
#if EESTI_en
	ee_EE_NResult_to_upload,
#endif
#if LATVIESU_en
	la_LA_NResult_to_upload,
#endif
#if LIMBA_ROMANA_en
	lm_ML_NResult_to_upload,
#endif
#if PUSSIJI_en
	pu_RS_NResult_to_upload,
#endif
#if EAAHNIKA_en
	ea_GK_NResult_to_upload,
#endif
};

const char *message_list_For_accuracy[] = {
#if ENGLISH_en
	en_US_For_accuracy,
#endif
#if FRANCAIS_en
	fr_FR_For_accuracy,
#endif
#if DEUTSCH_en
	de_DE_For_accuracy,
#endif
#if ESPANOL_en
	es_ES_For_accuracy,
	
#endif
#if ITALIAN_en
	it_IT_For_accuracy,
	
#endif
#if PORTUGUES_en
	pt_PT_For_accuracy,
	
#endif
#if NEDERLANDS_en
	nd_ND_For_accuracy,
	
#endif
#if DANSK_en
	dk_DK_For_accuracy,
	
#endif
#if NORSK_en
	nl_NW_For_accuracy,
	
#endif
#if SOOMI_en
	sm_FN_For_accuracy,
	
#endif
#if SVENSKA_en
	sv_SW_For_accuracy,
	
#endif
#if CESTINA_en
	cs_CZ_For_accuracy,
	
#endif
#if LIETUVIU_K_en
	lt_LT_For_accuracy,
	
#endif
#if POLSKI_en
	pl_PL_For_accuracy,
	
#endif
#if SLOVENCINA_en
	sl_SL_For_accuracy,
	
#endif
#if SLOVENSCINA_en
	sls_SLJ_For_accuracy,
	
#endif
#if TURKU_en
	tk_TR_For_accuracy,
	
#endif
#if EESTI_en
	ee_EE_For_accuracy,
	
#endif
#if LATVIESU_en
	la_LA_For_accuracy,
	
#endif
#if LIMBA_ROMANA_en
	lm_ML_For_accuracy,
	
#endif
#if PUSSIJI_en
	pu_RS_For_accuracy,
	
#endif
#if EAAHNIKA_en
	ea_GK_For_accuracy,
	
#endif
};

const char *message_list_unregistered_device_plz_register[] = {
#if ENGLISH_en
	en_US_unregistered_device_plz_register,
#endif
#if FRANCAIS_en
	fr_FR_unregistered_device_plz_register,
	
#endif
#if DEUTSCH_en
	de_DE_unregistered_device_plz_register,
	
#endif
#if ESPANOL_en
	es_ES_unregistered_device_plz_register,
	
#endif
#if ITALIAN_en
	it_IT_unregistered_device_plz_register,
	
#endif
#if PORTUGUES_en
	pt_PT_unregistered_device_plz_register,
	
#endif
#if NEDERLANDS_en
	nd_ND_unregistered_device_plz_register,
	
#endif
#if DANSK_en
	dk_DK_unregistered_device_plz_register,
	
#endif
#if NORSK_en
	nl_NW_unregistered_device_plz_register,
	
#endif
#if SOOMI_en
	sm_FN_unregistered_device_plz_register,
	
#endif
#if SVENSKA_en
	sv_SW_unregistered_device_plz_register,
	
#endif
#if CESTINA_en
	cs_CZ_unregistered_device_plz_register,
	
#endif
#if LIETUVIU_K_en
	lt_LT_unregistered_device_plz_register,
	
#endif
#if POLSKI_en
	pl_PL_unregistered_device_plz_register,
	
#endif
#if SLOVENCINA_en
	sl_SL_unregistered_device_plz_register,
	
#endif
#if SLOVENSCINA_en
	sls_SLJ_unregistered_device_plz_register,
	
#endif
#if TURKU_en
	tk_TR_unregistered_device_plz_register,
	
#endif
#if EESTI_en
	ee_EE_unregistered_device_plz_register,
	
#endif
#if LATVIESU_en
	la_LA_unregistered_device_plz_register,
	
#endif
#if LIMBA_ROMANA_en
	lm_ML_unregistered_device_plz_register,
	
#endif
#if PUSSIJI_en
	pu_RS_unregistered_device_plz_register,
	
#endif
#if EAAHNIKA_en
	ea_GK_unregistered_device_plz_register,
	
#endif
};
const char *message_list_invalid_code[] = {
#if ENGLISH_en
	en_US_invalid_code,
#endif
#if FRANCAIS_en
	fr_FR_invalid_code,
	
#endif
#if DEUTSCH_en
	de_DE_invalid_code,

#endif
#if ESPANOL_en
	es_ES_invalid_code,

#endif
#if ITALIAN_en
	it_IT_invalid_code,

#endif
#if PORTUGUES_en
	pt_PT_invalid_code,

#endif
#if NEDERLANDS_en
	nd_ND_invalid_code,

#endif
#if DANSK_en
	dk_DK_invalid_code,

#endif
#if NORSK_en
	nl_NW_invalid_code,

#endif
#if SOOMI_en
	sm_FN_invalid_code,

#endif
#if SVENSKA_en
	sv_SW_invalid_code,

#endif
#if CESTINA_en
	cs_CZ_invalid_code,

#endif
#if LIETUVIU_K_en
	lt_LT_invalid_code,

#endif
#if POLSKI_en
	pl_PL_invalid_code,

#endif
#if SLOVENCINA_en
	sl_SL_invalid_code,

#endif
#if SLOVENSCINA_en
	sls_SLJ_invalid_code,

#endif
#if TURKU_en
	tk_TR_invalid_code,

#endif
#if EESTI_en
	ee_EE_invalid_code,

#endif
#if LATVIESU_en
	la_LA_invalid_code,

#endif
#if LIMBA_ROMANA_en
	lm_ML_invalid_code,

#endif
#if PUSSIJI_en
	pu_RS_invalid_code,

#endif
#if EAAHNIKA_en
	ea_GK_invalid_code,

#endif
};
const char *message_list_network_unstable_fail_to_register[] = {
#if ENGLISH_en
	en_US_network_unstable_fail_to_register,
#endif
#if FRANCAIS_en
	fr_FR_network_unstable_fail_to_register,

#endif
#if DEUTSCH_en
	de_DE_network_unstable_fail_to_register,

#endif
#if ESPANOL_en
	es_ES_network_unstable_fail_to_register,

#endif
#if ITALIAN_en
	it_IT_network_unstable_fail_to_register,

#endif
#if PORTUGUES_en
	pt_PT_network_unstable_fail_to_register,

#endif
#if NEDERLANDS_en
	nd_ND_network_unstable_fail_to_register,

#endif
#if DANSK_en
	dk_DK_network_unstable_fail_to_register,

#endif
#if NORSK_en
	nl_NW_network_unstable_fail_to_register,

#endif
#if SOOMI_en
	sm_FN_network_unstable_fail_to_register,

#endif
#if SVENSKA_en
	sv_SW_network_unstable_fail_to_register,

#endif
#if CESTINA_en
	cs_CZ_network_unstable_fail_to_register,

#endif
#if LIETUVIU_K_en
	lt_LT_network_unstable_fail_to_register,

#endif
#if POLSKI_en
	pl_PL_network_unstable_fail_to_register,

#endif
#if SLOVENCINA_en
	sl_SL_network_unstable_fail_to_register,

#endif
#if SLOVENSCINA_en
	sls_SLJ_network_unstable_fail_to_register,

#endif
#if TURKU_en
	tk_TR_network_unstable_fail_to_register,

#endif
#if EESTI_en
	ee_EE_network_unstable_fail_to_register,

#endif
#if LATVIESU_en
	la_LA_network_unstable_fail_to_register,

#endif
#if LIMBA_ROMANA_en
	lm_ML_network_unstable_fail_to_register,

#endif
#if PUSSIJI_en
	pu_RS_network_unstable_fail_to_register,

#endif
#if EAAHNIKA_en
	ea_GK_network_unstable_fail_to_register,

#endif
};
const char *message_list_register_fail_code[] = {
#if ENGLISH_en
	en_US_register_fail_code,
#endif
#if FRANCAIS_en
	fr_FR_register_fail_code,

#endif
#if DEUTSCH_en
	de_DE_register_fail_code,

#endif
#if ESPANOL_en
	es_ES_register_fail_code,

#endif
#if ITALIAN_en
	it_IT_register_fail_code,

#endif
#if PORTUGUES_en
	pt_PT_register_fail_code,

#endif
#if NEDERLANDS_en
	nd_ND_register_fail_code,

#endif
#if DANSK_en
	dk_DK_register_fail_code,

#endif
#if NORSK_en
	nl_NW_register_fail_code,

#endif
#if SOOMI_en
	sm_FN_register_fail_code,

#endif
#if SVENSKA_en
	sv_SW_register_fail_code,

#endif
#if CESTINA_en
	cs_CZ_register_fail_code,

#endif
#if LIETUVIU_K_en
	lt_LT_register_fail_code,

#endif
#if POLSKI_en
	pl_PL_register_fail_code,

#endif
#if SLOVENCINA_en
	sl_SL_register_fail_code,

#endif
#if SLOVENSCINA_en
	sls_SLJ_register_fail_code,

#endif
#if TURKU_en
	tk_TR_register_fail_code,

#endif
#if EESTI_en
	ee_EE_register_fail_code,

#endif
#if LATVIESU_en
	la_LA_register_fail_code,

#endif
#if LIMBA_ROMANA_en
	lm_ML_register_fail_code,

#endif
#if PUSSIJI_en
	pu_RS_register_fail_code,

#endif
#if EAAHNIKA_en
	ea_GK_register_fail_code,

#endif
};
const char *message_list_register_successfully[] = {
#if ENGLISH_en
	en_US_register_successfully,
#endif
#if FRANCAIS_en
	fr_FR_register_successfully,

#endif
#if DEUTSCH_en
	de_DE_register_successfully,

#endif
#if ESPANOL_en
	es_ES_register_successfully,

#endif
#if ITALIAN_en
	it_IT_register_successfully,

#endif
#if PORTUGUES_en
	pt_PT_register_successfully,

#endif
#if NEDERLANDS_en
	nd_ND_register_successfully,

#endif
#if DANSK_en
	dk_DK_register_successfully,

#endif
#if NORSK_en
	nl_NW_register_successfully,

#endif
#if SOOMI_en
	sm_FN_register_successfully,

#endif
#if SVENSKA_en
	sv_SW_register_successfully,

#endif
#if CESTINA_en
	cs_CZ_register_successfully,

#endif
#if LIETUVIU_K_en
	lt_LT_register_successfully,

#endif
#if POLSKI_en
	pl_PL_register_successfully,

#endif
#if SLOVENCINA_en
	sl_SL_register_successfully,

#endif
#if SLOVENSCINA_en
	sls_SLJ_register_successfully,

#endif
#if TURKU_en
	tk_TR_register_successfully,

#endif
#if EESTI_en
	ee_EE_register_successfully,

#endif
#if LATVIESU_en
	la_LA_register_successfully,

#endif
#if LIMBA_ROMANA_en
	lm_ML_register_successfully,

#endif
#if PUSSIJI_en
	pu_RS_register_successfully,

#endif
#if EAAHNIKA_en
	ea_GK_register_successfully,

#endif
};

const char *message_list_fail_to_upload_unregister[] = {
#if ENGLISH_en
	en_US_fail_to_upload_unregister,
#endif
#if FRANCAIS_en
	fr_FR_fail_to_upload_unregister,

#endif
#if DEUTSCH_en
	de_DE_fail_to_upload_unregister,

#endif
#if ESPANOL_en
	es_ES_fail_to_upload_unregister,

#endif
#if ITALIAN_en
	it_IT_fail_to_upload_unregister,

#endif
#if PORTUGUES_en
	pt_PT_fail_to_upload_unregister,

#endif
#if NEDERLANDS_en
	nd_ND_fail_to_upload_unregister,

#endif
#if DANSK_en
	dk_DK_fail_to_upload_unregister,

#endif
#if NORSK_en
	nl_NW_fail_to_upload_unregister,

#endif
#if SOOMI_en
	sm_FN_fail_to_upload_unregister,

#endif
#if SVENSKA_en
	sv_SW_fail_to_upload_unregister,

#endif
#if CESTINA_en
	cs_CZ_fail_to_upload_unregister,

#endif
#if LIETUVIU_K_en
	lt_LT_fail_to_upload_unregister,

#endif
#if POLSKI_en
	pl_PL_fail_to_upload_unregister,

#endif
#if SLOVENCINA_en
	sl_SL_fail_to_upload_unregister,

#endif
#if SLOVENSCINA_en
	sls_SLJ_fail_to_upload_unregister,

#endif
#if TURKU_en
	tk_TR_fail_to_upload_unregister,

#endif
#if EESTI_en
	ee_EE_fail_to_upload_unregister,

#endif
#if LATVIESU_en
	la_LA_fail_to_upload_unregister,

#endif
#if LIMBA_ROMANA_en
	lm_ML_fail_to_upload_unregister,

#endif
#if PUSSIJI_en
	pu_RS_fail_to_upload_unregister,

#endif
#if EAAHNIKA_en
	ea_GK_fail_to_upload_unregister,

#endif
};
const char *message_list_unregister_result_stored[] = {
#if ENGLISH_en
	en_US_unregister_result_stored,
#endif
#if FRANCAIS_en
	fr_FR_unregister_result_stored,

#endif
#if DEUTSCH_en
	de_DE_unregister_result_stored,

#endif
#if ESPANOL_en
	es_ES_unregister_result_stored,

#endif
#if ITALIAN_en
	it_IT_unregister_result_stored,

#endif
#if PORTUGUES_en
	pt_PT_unregister_result_stored,

#endif
#if NEDERLANDS_en
	nd_ND_unregister_result_stored,

#endif
#if DANSK_en
	dk_DK_unregister_result_stored,

#endif
#if NORSK_en
	nl_NW_unregister_result_stored,

#endif
#if SOOMI_en
	sm_FN_unregister_result_stored,

#endif
#if SVENSKA_en
	sv_SW_unregister_result_stored,

#endif
#if CESTINA_en
	cs_CZ_unregister_result_stored,

#endif
#if LIETUVIU_K_en
	lt_LT_unregister_result_stored,

#endif
#if POLSKI_en
	pl_PL_unregister_result_stored,

#endif
#if SLOVENCINA_en
	sl_SL_unregister_result_stored,

#endif
#if SLOVENSCINA_en
	sls_SLJ_unregister_result_stored,

#endif
#if TURKU_en
	tk_TR_unregister_result_stored,

#endif
#if EESTI_en
	ee_EE_unregister_result_stored,

#endif
#if LATVIESU_en
	la_LA_unregister_result_stored,

#endif
#if LIMBA_ROMANA_en
	lm_ML_unregister_result_stored,

#endif
#if PUSSIJI_en
	pu_RS_unregister_result_stored,

#endif
#if EAAHNIKA_en
	ea_GK_unregister_result_stored,

#endif
};

void Message_by_language(Message_list_t item, uint32_t parameter, LANGUAGE lang){
	char buf[300];
	memset(buf,0,300);
	uint32_t temp;
	lang = get_language_setting();
	
	switch(item){
	case Software_available      :
		message(message_list_Software_available[lang]);
	break;
	case Insert_VIN              :
		message(message_list_Insert_VIN[lang]);
	break;
	case Turn_on_WIFI            :
		message(message_list_Turn_on_WIFI[lang]);
	break;
	case Download_complete       :
		temp = UTF8_str_size(message_list_Download_complete[lang]);
		memcpy(buf, message_list_Download_complete[lang], temp);
		sprintf(buf + temp, " %d ", parameter);
		temp += strlen(buf + temp);
		memcpy(buf+temp, message_list_NSec[lang], UTF8_str_size(message_list_NSec[lang]));
		message(buf);
	break;
	case upgrade_available       :
		message(message_list_upgrade_available[lang]);
	break;
	case Natwork_unstable        :
		message(message_list_Natwork_unstable[lang]);
	break;
	case Software_up_to_date     :
		message(message_list_Software_up_to_date[lang]);
	break;
	case Fail_to_cloud           :
		message(message_list_Fail_to_cloud[lang]);
	break;
	case Check_connection        :
		message(message_list_Check_connection[lang]);
	break;
	case Store_result            :
		temp = UTF8_str_size(message_list_Store_result[lang]);
		memcpy(buf, message_list_Store_result[lang], temp);
		sprintf(buf + temp, " %d/500", parameter);
		message(buf);
	break;
	case Uploading_result        :
		temp = UTF8_str_size(message_list_Uploading_result[lang]);
		memcpy(buf, message_list_Uploading_result[lang], temp);
		sprintf(buf + temp, " %d", parameter);
		temp += strlen(buf + temp);
		memcpy(buf+temp, message_list_NResult_pending[lang], UTF8_str_size(message_list_NResult_pending[lang]));
		message(buf);
	break;
	case Result_uploaded         :
		temp = UTF8_str_size(message_list_Result_uploaded[lang]);
		memcpy(buf, message_list_Result_uploaded[lang], temp);
		sprintf(buf + temp, " %d", parameter);
		temp += strlen(buf + temp);
		memcpy(buf+temp, message_list_NResult_to_upload[lang], UTF8_str_size(message_list_NResult_to_upload[lang]));
		message(buf);
	break;
	case Upload_failed           :
		message(message_list_Upload_failed[lang]);
	break;
	case WIFI_connected_find_result   	:
		temp = UTF8_str_size(message_list_WIFI_connected[lang]);
		memcpy(buf, message_list_WIFI_connected[lang], temp);
		sprintf(buf + temp, " %d", parameter);
		temp += strlen(buf + temp);
		memcpy(buf+temp, message_list_NResult_to_upload[lang], UTF8_str_size(message_list_NResult_to_upload[lang]));
		message(buf);
	break;
	case Memory_full             :
		message(message_list_Memory_full[lang]);
	break;
	case Data_uploaded           :
		temp = UTF8_str_size(message_list_Data_uploaded[lang]);
		memcpy(buf, message_list_Data_uploaded[lang], temp);
		sprintf(buf + temp, " %d/500", parameter);
		message(buf);
	break;
	case Regarge_time            :
		temp = UTF8_str_size(message_list_Regarge_time[lang]);
		memcpy(buf, message_list_Regarge_time[lang], temp);
		sprintf(buf + temp, " %d ", parameter);
		temp += strlen(buf + temp);
		if(parameter > 1){
			memcpy(buf+temp, message_list_NHours[lang], UTF8_str_size(message_list_NHours[lang]));
		}
		else{
			memcpy(buf+temp, message_list_NHour[lang], UTF8_str_size(message_list_NHour[lang]));
		}
		message(buf);
	break;
	case For_accuracy			:
		message(message_list_For_accuracy[lang]);
	break;
	case unregistered_device_plz_register	:
		message(message_list_unregistered_device_plz_register[lang]);
	break;
	case invalid_code			:
		message(message_list_invalid_code[lang]);
	break;
	case network_unstable_fail_to_register		:
		message(message_list_network_unstable_fail_to_register[lang]);
	break;
	case register_fail_code		:
		temp = UTF8_str_size(message_list_register_fail_code[lang]);
		memcpy(buf,message_list_register_fail_code[lang],temp);
		sprintf(buf+temp," %02d",parameter);
		message(buf);
	break;
	case register_successfully		:
		message(message_list_register_successfully[lang]);
	break;
	
	case fail_to_upload_unregister:
		message(message_list_fail_to_upload_unregister[lang]);
	break;
	case unregister_result_stored:
		temp = UTF8_str_size(message_list_unregister_result_stored[lang]);
		memcpy(buf, message_list_unregister_result_stored[lang], temp);
		sprintf(buf + temp, " %d/500", parameter);
		message(buf);
	break;
	}
}
