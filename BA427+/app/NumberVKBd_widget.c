//*****************************************************************************
//
// Source File: NumberVKBd_widget.c
// Description: Repair Order(RO) number and VIN number's Virtual Keyboard widget including Uppercase
//				letters and Numeric keyboards input method.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 01/29/16     William.H      Created file.
// 02/16/16     Vincent.T      1. Remove reset par->RO_str[]/ par->VIN_str[] procedure.
// 03/31/16     Vincent.T      1. Modify RoVinVKybd123Click(); to handle multination language case.
//                             2. Modify RoVinVKybdABCClick(); to handle multination language case.
// 01/13/17     Vincent.T      1. Data type changed: par->RO_str and par->VIN_str
//                             2. Modify VINVKydbUpdateTextEditChar();, RoVinVKybdABCClick); and RoVinVKybd123Click;(default: '0' -> 'OK'; Exapnd Lincense# form 8 -> 12, aka vin# here.)
// 01/19/17     Vincent.T      1. Modify RoVinVKybdABCClick(); default:'0' -> 'OK'
// 03/08/17     Vincent.T      1. Expand par->RO_str[] 4 -> 8
//                             2. Modify RoVKydbUpdateTextEditChar(); due to more characters are added
//                             3. Modify RoVinVKybd123Click(), RoVinVKybd123Up(), RoVinVKybd123Left(), RoVinVKybd123Right(), RoVinVKybd123Down() and RoVinVKybdABCClick(); due to more characters are added.
// 03/27/17     Vincent.T      1. Modify RoVKydbUpdateTextEditChar(); to correct par->RO_str[] index right.
//                             2. Modify VINVKydbUpdateTextEditChar(); to correct par->VIN_str[] index right.
// 09/21/17 	Jerry.W		   Rewrite Widget for new software architecture
// 09/30/17 	Jerry.W		   1. add const in tRectangle and char array for fewer memory usage
// 02/14/18     Henry.Y        add debug message
// 03/22/18     Henry.Y        modify flow, it will go to CheckVIN_widget after RO/VIM input.
// 03/23/18 	Jerry.W		   	add service to move to next widget while VIN is scaned by barcode scaner.
//								initial index before move to next widget.
//								don't display character while VIN is full filled.
// 12/07/18     Henry.Y        New display api.
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/app/RO_VIN_NumberVKBdWidget.c#6 $
// $DateTime: 2017/09/30 16:48:40 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************


#include "Widgetmgr.h"

/* Character Update Mode */

uint8_t g_RoVKybdCharPos = 0;

const char text[] = {
		//0  1   2   3   4   5   6   7   8   9
		'0','1','2','3','4','5','6','7','8','9',
	   //10  11 12  13  14  15  16  17  18  19  20  21  22
		' ',' ',' ','Q','W','E','R','T','Y','U','I','O','P',
	   //23  24  25  26  27  28  29  30  31  32  33
		'A','S','D','F','G','H','J','K','L',' ',' ',
	   //34  35  36  37  38  39  40  41  42  43
		'Z','X','C','V','B','N','M',' ',' ','-'
};

void Ro_VIN_VKydbUpdateTextEditChar(uint8_t position,   bool appearStat);

void NumberVKBdAntiWhiteItem(uint8_t selRslt);

void NumberVKBd_DISPLAY();
void NumberVKBd_UP();
void NumberVKBd_DOWN();
void NumberVKBd_LEFT();
void NumberVKBd_RIGHT();
void NumberVKBd_SELECT();
void NumberVKBd_Service();
void NumberVKBd_Routine();
void NumberVKBd_initail();

static uint8_t insert_vin_manually = 1;//0: no, 1: yes
static uint8_t abort_vin_input = 0;//0:no, 1:yes

widget* NumberVKBd_sub_W[] = {
		&CheckVIN_widget,
		&PrintResult_widget,
		&AbortTest_widget
};

widget NumberVKBd_widget = {
		.Key_UP = NumberVKBd_UP,
		.Key_DOWN = NumberVKBd_DOWN,
		.Key_RIGHT = NumberVKBd_RIGHT,
		.Key_LEFT = NumberVKBd_LEFT,
		.Key_SELECT = NumberVKBd_SELECT,
		.routine = NumberVKBd_Routine,
		.service = NumberVKBd_Service,
		.initial = NumberVKBd_initail,
		.display = NumberVKBd_DISPLAY,
		.subwidget = NumberVKBd_sub_W,
		.number_of_widget = sizeof(NumberVKBd_sub_W)/4,
		.index = 12, //12=OK
		.name = "Ro_VIN_VK",
};

void NumberVKBd_initail(){
	SYS_PAR* par = Get_system_parameter();
	TEST_DATA* TD = Get_test_data();
	memset(&par->RO_str, 0x20, ROIN_LEN);
	g_RoVKybdCharPos = 0;
	NumberVKBd_widget.index = 12;//initial location at 'OK'
	insert_vin_manually = 1;
	abort_vin_input = 0;
}

void NumberVKBd_DISPLAY(){
	uint8_t i;
	TEST_DATA* TD = Get_test_data();
	
	if(TD->Code_test) return;
	
	ST7565ClearScreen(BUFFER0);
	LANGUAGE LangIdx = get_language_setting();

	if(NumberVKBd_widget.parameter == 0){
		LCD_display(INSERT_VIN_MANUALLY, insert_vin_manually^0x01);
	}
	else if(NumberVKBd_widget.parameter == 1){
		LCD_display(ABORT_VIN_INPUT, abort_vin_input^0x01);
	}
	else{
		LCD_display(VIN_INPUT_VIRTUAL_KEYBOARD,0);
	
		if(NumberVKBd_widget.index>12 && NumberVKBd_widget.index < 42){			
			LCD_display(VIN_INPUT_VIRTUAL_KEYBOARD,1);
		}
		
		NumberVKBdAntiWhiteItem(NumberVKBd_widget.index);
		
		for(i = 0; i != g_RoVKybdCharPos ; i++){
			Ro_VIN_VKydbUpdateTextEditChar(i, true);
		}
	}

	
	set_language(LangIdx);
	
	
}


void NumberVKBd_Service(){
	TEST_DATA* TD = Get_test_data();

	if(TD->Code_test){
		Move_widget(NumberVKBd_sub_W[0],0);
	}
	else{
		
		if(NumberVKBd_widget.parameter == 2){
			Message_by_language(Insert_VIN, 0,en_US);
		}
	}
}



void Move_NUMBER_index(uint8_t new_index){
	NumberVKBdAntiWhiteItem(NumberVKBd_widget.index);
	NumberVKBd_widget.index = new_index;
	NumberVKBdAntiWhiteItem(NumberVKBd_widget.index);
}

void NumberVKBd_UP(){
	if(NumberVKBd_widget.parameter == 0){
		insert_vin_manually ^= 0x01;
		NumberVKBd_DISPLAY();		
	}
	else if(NumberVKBd_widget.parameter == 1){
		abort_vin_input ^= 0x01;
		NumberVKBd_DISPLAY();		
	}
	else{		
		UARTprintf("g_RoVKybdCharPos = %d", g_RoVKybdCharPos);
		
		if((NumberVKBd_widget.index >= 5) && (NumberVKBd_widget.index <= 12))
		{
			if(NumberVKBd_widget.index == 10)
			{
				Move_NUMBER_index(4 );
			}
			else if(NumberVKBd_widget.index == 11)
			{
				Move_NUMBER_index( 5);
			}
			else if(NumberVKBd_widget.index == 12)
			{
				Move_NUMBER_index( 10);
			}
			else if((NumberVKBd_widget.index >= 5) && (NumberVKBd_widget.index <= 9))
			{
				Move_NUMBER_index( NumberVKBd_widget.index - 5);
			}
		
		}
		else if((NumberVKBd_widget.index >= 42) && (NumberVKBd_widget.index <= 43))
		{
			if(NumberVKBd_widget.index == 42)
			{
				Move_NUMBER_index( 8);
			}
			else if(NumberVKBd_widget.index == 43)
			{
				Move_NUMBER_index(6 );
			}
		
		}
		else if((NumberVKBd_widget.index >= 23) && (NumberVKBd_widget.index <= 41) )
		{
		
			if((NumberVKBd_widget.index >= 23) && (NumberVKBd_widget.index <= 40))
			{
				Move_NUMBER_index( NumberVKBd_widget.index - 10);
		
			}
			else if(NumberVKBd_widget.index == 41)
			{
				Move_NUMBER_index( 32);
			}
		
		}
		
		
		Ro_VIN_VKydbUpdateTextEditChar(g_RoVKybdCharPos, true);
	}
}

void NumberVKBd_DOWN(){
	if(NumberVKBd_widget.parameter == 0){
		insert_vin_manually ^= 0x01;
		NumberVKBd_DISPLAY();		
	}
	else if(NumberVKBd_widget.parameter == 1){
		abort_vin_input ^= 0x01;
		NumberVKBd_DISPLAY();		
	}
	else{
		UARTprintf("g_RoVKybdCharPos = %d", g_RoVKybdCharPos);
		
		if(NumberVKBd_widget.index <= 10)
		{
			if(NumberVKBd_widget.index == 4)
			{
				Move_NUMBER_index( 9);
			}
			else if(NumberVKBd_widget.index == 5)
			{
				Move_NUMBER_index( 11);
			}
			else if((NumberVKBd_widget.index >= 6) && (NumberVKBd_widget.index <= 7))
			{
				Move_NUMBER_index( 43);
			}
			else if((NumberVKBd_widget.index >= 8) && (NumberVKBd_widget.index <= 8))
			{
				Move_NUMBER_index( 42);
			}
			else if((NumberVKBd_widget.index == 9)||(NumberVKBd_widget.index == 10))
			{
				Move_NUMBER_index( 12);
			}
			else if(NumberVKBd_widget.index <= 3)
			{
				Move_NUMBER_index( NumberVKBd_widget.index + 5);
			}
		}
		else if((NumberVKBd_widget.index >= 13) && (NumberVKBd_widget.index <= 32) )
		{
			if((NumberVKBd_widget.index >= 13) && (NumberVKBd_widget.index <= 30))
			{
		
				Move_NUMBER_index( NumberVKBd_widget.index + 10);
			}
			else if(NumberVKBd_widget.index == 31)
			{
				Move_NUMBER_index( 41);
		
			}
			else if(NumberVKBd_widget.index == 32)
			{
				Move_NUMBER_index(41);
			}
		
		}
		
		Ro_VIN_VKydbUpdateTextEditChar(g_RoVKybdCharPos, true);
	}

}

void NumberVKBd_LEFT(){
	if(NumberVKBd_widget.parameter != 2){
		Move_widget(NumberVKBd_sub_W[2],0);
	}
	else{
		UARTprintf("g_RoVKybdCharPos = %d", g_RoVKybdCharPos);
		
		if(NumberVKBd_widget.index <= 12)
		{
			if(NumberVKBd_widget.index == 0)
			{
				Move_NUMBER_index(12);
			}
			else if(NumberVKBd_widget.index == 12)
			{
				Move_NUMBER_index(42);
			}
			else
			{
				Move_NUMBER_index(NumberVKBd_widget.index - 1);
			}
		
		}
		else if(NumberVKBd_widget.index >= 13 && NumberVKBd_widget.index <= 41)
		{
			if(NumberVKBd_widget.index == 13)
			{
				Move_NUMBER_index(41);
			}
			else
			{
				Move_NUMBER_index(NumberVKBd_widget.index - 1);
			}
		
		}
		else if((NumberVKBd_widget.index >= 42) && (NumberVKBd_widget.index <= 43))
		{
			if(NumberVKBd_widget.index == 42)
			{
				Move_NUMBER_index(43);
			}
			else if(NumberVKBd_widget.index == 43)
			{
				Move_NUMBER_index(11);
			}
		}
		Ro_VIN_VKydbUpdateTextEditChar(g_RoVKybdCharPos, true);
	}
}




void NumberVKBd_RIGHT(){
	if(NumberVKBd_widget.parameter == 2){
		UARTprintf("g_RoVKybdCharPos = %d", g_RoVKybdCharPos);
		if(NumberVKBd_widget.index <= 12)
		{
			if (NumberVKBd_widget.index == 12)
			{
				Move_NUMBER_index(11);
			}
			else if(NumberVKBd_widget.index == 11)
			{
				Move_NUMBER_index(43);
			}
			else
			{
				Move_NUMBER_index(NumberVKBd_widget.index + 1 );
			}
		}
		else if(NumberVKBd_widget.index >= 13 && NumberVKBd_widget.index <= 41)
		{
			if (NumberVKBd_widget.index == 41)
			{
				Move_NUMBER_index(13);
			}
			else
			{
				Move_NUMBER_index(NumberVKBd_widget.index +1);
			}
		}
		else if((NumberVKBd_widget.index >= 42) && (NumberVKBd_widget.index <= 43))
		{
			if(NumberVKBd_widget.index == 42)
			{
				Move_NUMBER_index(12);
			}
			else if(NumberVKBd_widget.index == 43)
			{
				Move_NUMBER_index(42);
			}
		}
		
		Ro_VIN_VKydbUpdateTextEditChar(g_RoVKybdCharPos, true);
	}
}

void NumberVKBd_SELECT(){
	SYS_PAR* par = Get_system_parameter();

	if(NumberVKBd_widget.parameter == 0){
		if(insert_vin_manually){
			NumberVKBd_widget.parameter = 2;
			NumberVKBd_DISPLAY();
			NumberVKBd_Service();
		}
		else{
			NumberVKBd_widget.parameter = 1;
			NumberVKBd_DISPLAY();			
		}
	}
	else if(NumberVKBd_widget.parameter == 1){
		if(abort_vin_input){
			Move_widget(NumberVKBd_sub_W[1], 0);			
		}
		else{
			NumberVKBd_widget.parameter = 0;
			NumberVKBd_DISPLAY();			
		}
	}
	else{
		Ro_VIN_VKydbUpdateTextEditChar(g_RoVKybdCharPos, true);
		
		if((NumberVKBd_widget.index == 10)||(NumberVKBd_widget.index == 32)){
			par->VIN_str[g_RoVKybdCharPos] = text[NumberVKBd_widget.index];
			Ro_VIN_VKydbUpdateTextEditChar(g_RoVKybdCharPos, false);
			if(g_RoVKybdCharPos > 0){
				g_RoVKybdCharPos--;
			}
		}
		else if(NumberVKBd_widget.index == 11){ //ABC
			Move_NUMBER_index(33);
			LCD_display(VIN_INPUT_VIRTUAL_KEYBOARD,1);

			NumberVKBdAntiWhiteItem(NumberVKBd_widget.index);
		
		}
		else if(NumberVKBd_widget.index == 33){ //123
			Move_NUMBER_index(11);
			LCD_display(VIN_INPUT_VIRTUAL_KEYBOARD,2);
			NumberVKBdAntiWhiteItem(NumberVKBd_widget.index);
		}
		else if((NumberVKBd_widget.index == 12)||(NumberVKBd_widget.index == 41)){ //OK
			NumberVKBd_widget.index = 12;
			Move_widget(NumberVKBd_sub_W[0],0);
		}
		else{
			if(g_RoVKybdCharPos < VININ_LEN1){
				if(text[NumberVKBd_widget.index] == 'i' || text[NumberVKBd_widget.index] == 'o' || text[NumberVKBd_widget.index] == 'q' ||
					text[NumberVKBd_widget.index] == 'I' || text[NumberVKBd_widget.index] == 'O' || text[NumberVKBd_widget.index] == 'Q' ||
					text[NumberVKBd_widget.index] == ' ' || text[NumberVKBd_widget.index] == '-') return;
				
				par->VIN_str[g_RoVKybdCharPos] = text[NumberVKBd_widget.index];
				g_RoVKybdCharPos++;
			}
			else{
			
				if((NumberVKBd_widget.index < 10)||(NumberVKBd_widget.index > 41)){
					Move_NUMBER_index(12);
				}
				else{
					Move_NUMBER_index(41);
				}
			}
		}
	}	
}

void NumberVKBd_Routine(){
	static uint16_t N_counter = 0;

	if(NumberVKBd_widget.parameter == 2){
		if(N_counter == 600){
			N_counter = 0;
			Ro_VIN_VKydbUpdateTextEditChar(g_RoVKybdCharPos, false);
		}
		else if(N_counter == 300){
			N_counter++;
			Ro_VIN_VKydbUpdateTextEditChar(g_RoVKybdCharPos, true);
		}
		else{
			N_counter++;
		}
	}
}


void Ro_VIN_VKydbUpdateTextEditChar(uint8_t position,   bool appearStat)
{
	SYS_PAR* par = Get_system_parameter();
	int x, y;
	if(position == VININ_LEN1) return;
	UARTprintf("RoVKydbUpdateTextEditChar(). g_RoVKybdCharPos = %d, appearStat = %d.\n", position, appearStat);
	x = 5 + (position)*7;
	y = 20;

	if(appearStat){
		if((g_RoVKybdCharPos == position)&&((NumberVKBd_widget.index <= 9)||((NumberVKBd_widget.index >= 13)&&(NumberVKBd_widget.index <=40))||(NumberVKBd_widget.index == 42)||(NumberVKBd_widget.index == 43)||(NumberVKBd_widget.index == 32)||(NumberVKBd_widget.index == 33))){
			ST7565Draw6x8Char(x, y,text[NumberVKBd_widget.index], Font_System5x8);	// '0' ~ '9' 'A'~'Z'
		}
		else{
			ST7565Draw6x8Char(x, y,par->VIN_str[position], Font_System5x8);
		}
	}
	else{
		ST7565Draw6x8Char(x, y, 0x20, Font_System5x8);	// 'Space'
	}
}




const tRectangle recs[44] = {
		{23,40,31,48},{42,40,48,48},{61,40,69,48},{81,40,89,48},{101,40,109,48}, //0~4
		{23,49,31,57},{42,49,50,57},{61,49,69,57},{81,49,89,57},{101,49,109,57},//5~9
		{115,47,127,55},{2,54,19,61},{112,55,125,61},{10,40,18,47},{21,40,32,47},//10~14
		{35,40,42,47},{46,40,54,47},{57,40,65,47},{68,40,76,47},{79,40,87,47},//15~19
		{91,40,95,47},{100,40,108,47},{113,40,120,47},{18,48,26,55},{30,48,37,55},//20~24
		{41,48,49,55},{53,48,60,55},{63,48,71,55},{74,48,82,55},{85,48,91,55},//25~29
		{95,48,103,55},{106,48,112,55},{115,47,127,55},{2,55,16,61},{30,56,37,63},//30~34
		{41,56,49,63},{52,56,60,63},{63,56,71,63},{75,56,82,63},{85,56,93,63},//35~39
		{95,56,103,63},{112,55,125,61},{71,58,79,63},{52,58,60,63}		  //40~43

};

void NumberVKBdAntiWhiteItem(uint8_t selRslt){
	ST7565RectReverse(&recs[NumberVKBd_widget.index], VERTICAL);
}







