//*****************************************************************************
//
// Source File: Infvkybdwidget.c
// Description: Information's Virtual Keyboard widget including Uppercase/Lowercase
//				letters , Numeric and Symbol keyboards.
//				
//
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/app/Infvkybdwidget.c#1 $
// $DateTime: 2017/11/27 10:38:22 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#include "Widgetmgr.h"



void InfVKybdUpdateTextEditChar();
void InfVKybdAntiWhiteItem(uint8_t index);
void InVK_save();

void InVKbd_confirm_LEFT();
void InVKbd_confirm_RIGHT();
void InVKbd_confirm_SELECT();
void InVKbd_confirm_INIT();
void InVKbd_confirm_display();

widget* InVKbd_confirm_sub_W[] = {
};

widget InVKbd_confirm_widget = {
		.Key_UP = 0,
		.Key_DOWN = 0,
		.Key_RIGHT = InVKbd_confirm_RIGHT,
		.Key_LEFT = InVKbd_confirm_LEFT,
		.Key_SELECT = InVKbd_confirm_SELECT,
		.routine = 0,
		.service = 0,
		.initial = InVKbd_confirm_INIT,
		.display = InVKbd_confirm_display,
		.subwidget = InVKbd_confirm_sub_W,
		.number_of_widget = sizeof(InVKbd_confirm_sub_W)/4,
		.index = 0,
		.name = "InVKbd_confirm",
};

enum{
    Confirm_Save_yes,
    Confirm_Save_no,
    Confirm_Clear_yes,
    Confirm_Clear_sure_no,
    Confirm_Clear_sure_yes
};


void InVKbd_confirm_display(){
	SYS_INFO* info = Get_system_info();

	ST7565ClearScreen(BUFFER1);
	ST7565Refresh(BUFFER1);

	switch(InVKbd_confirm_widget.index){
	case Confirm_Save_no:
	    LCD_String_ext(16,get_Language_String(S_SAVE_CHANGES) , Align_Central);
	    LCD_String_ext2(32,get_Language_String(S_NO) , Align_Right, 1);
	    break;
	case Confirm_Save_yes:
	    LCD_String_ext(16,get_Language_String(S_SAVE_CHANGES) , Align_Central);
	    LCD_String_ext2(32,get_Language_String(S_YES) , Align_Right, 1);
	    break;
	case Confirm_Clear_yes:
	    LCD_String_ext(16,get_Language_String(S_CLEAR_MEMORY) , Align_Central);
	    LCD_String_ext2(32,get_Language_String(S_YES) , Align_Right, 1);
	        break;
	case Confirm_Clear_sure_no:
	    LCD_String_ext(16,get_Language_String(S_ARE_YOU_SURE) , Align_Left);
	    LCD_String_ext2(32,get_Language_String(S_NO) , Align_Right, 1);
	        break;
	case Confirm_Clear_sure_yes:
	    LCD_String_ext(16,get_Language_String(S_ARE_YOU_SURE) , Align_Left);
	    LCD_String_ext2(32,get_Language_String(S_YES) , Align_Right, 1);
	        break;
	default:
	    while(1){}
	}
	LCD_Arrow(Arrow_enter | Arrow_RL);
}


void InVKbd_confirm_INIT(){
    InVKbd_confirm_widget.index = 0;
}

void InVKbd_confirm_LEFT(){
    switch(InVKbd_confirm_widget.index){
    case Confirm_Save_no:
        InVKbd_confirm_widget.index = Confirm_Save_yes;
        break;
    case Confirm_Save_yes:
        InVKbd_confirm_widget.index = Confirm_Clear_yes;
        break;
    case Confirm_Clear_yes:
        InVKbd_confirm_widget.index = Confirm_Save_no;
        break;
    case Confirm_Clear_sure_no:
        InVKbd_confirm_widget.index = Confirm_Clear_sure_yes;
        break;
    case Confirm_Clear_sure_yes:
        InVKbd_confirm_widget.index = Confirm_Clear_sure_no;
        break;
    default:
        while(1){}
    }

    InVKbd_confirm_display();
}

void InVKbd_confirm_RIGHT(){
    switch(InVKbd_confirm_widget.index){
    case Confirm_Save_no:
        InVKbd_confirm_widget.index = Confirm_Clear_yes;
        break;
    case Confirm_Save_yes:
        InVKbd_confirm_widget.index = Confirm_Save_no;
        break;
    case Confirm_Clear_yes:
        InVKbd_confirm_widget.index = Confirm_Save_yes;
        break;
    case Confirm_Clear_sure_no:
        InVKbd_confirm_widget.index = Confirm_Clear_sure_yes;
        break;
    case Confirm_Clear_sure_yes:
        InVKbd_confirm_widget.index = Confirm_Clear_sure_no;
        break;
    default:
        while(1){}
    }
    InVKbd_confirm_display();
}

void InVKbd_confirm_SELECT(){
    switch(InVKbd_confirm_widget.index){
    case Confirm_Save_no:
        Return_pre_widget();
        return;
    case Confirm_Save_yes:
    {
        InVK_save();
    }
        Return_pre_widget();
        return;
    case Confirm_Clear_yes:
        InVKbd_confirm_widget.index = Confirm_Clear_sure_no;
        InVKbd_confirm_display();
        break;
    case Confirm_Clear_sure_no:
        Return_pre_widget();
        return;
    case Confirm_Clear_sure_yes:
    {
        SYS_INFO* info = Get_system_info();
        memset(info->VkybdTextEditStr, ' ', sizeof(info->VkybdTextEditStr));
        info->VkybdTextEditStr[0][16] = '\0';
        info->VkybdTextEditStr[1][16] = '\0';
        info->VkybdTextEditStr[2][16] = '\0';
        info->VkybdTextEditStr[3][16] = '\0';
        info->VkybdTextEditStr[4][16] = '\0';
        info->Has_DIY = 0;
        save_system_info(&info->VkybdTextEditStr, sizeof(info->VkybdTextEditStr));
        save_system_info(&info->Has_DIY, sizeof(info->Has_DIY));
    }
        Return_pre_widget();
        return;
    default:
        while(1){}
    }

}












