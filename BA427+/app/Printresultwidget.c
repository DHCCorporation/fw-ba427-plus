//*****************************************************************************
//
// Source File: Printresultwidget.c
// Description: 'Print result?' page.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 10/26/15     Henry.Y	      Created file.
// 10/30/15     Vincent.T     1. Add extern global variables: voltage, cca, rating
//                            2. Add extern global variables: g_BatteryType, judge_result
//                            3. Built more content in one printout
//                            4. Add #include Selratingwidget.h
// 11/02/15     Vincent.T     1. Change name of some global variables
//                            2. Modified some accumulation error
// 11/03/15     Vicent.T      1. Add global to access cca setting value
//                            2. Add more printout content
// 11/18/15     Vincent.T     1. Printout CCA -> SAE
// 11/23/15     Vincent.T     1. Printout add "Time"
// 01/19/16     Vincent.T     1. Add JIS Type Printout Field
//                            2. extern global variable g_JISRgBatType
//                            3. extern global variable g_JISCapSelect
//                            4. #include "Jiscapselwidget.h"
// 01/22/16     Vincent.T     1. global variable info->VkybdTextEditStr
//                            2. Add print Info. field
// 01/25/16     Vincent.T     1. Re-arrange info. printout format(to align the format on display )
// 01/30/16     Vincent.T     1. global variables: WHICH_TES, g_SSBatteryType, g_SSSelectRating, pg_JIS
//                            2. #include "../service\testrecordsvc.h"
// 02/05/16     Vincent.T     1. Add system test printout routine
// 02/16/16     Vincent.T     1. Add RO/VIN printout routine
//                            2. global variables g_RoVkybdTextEditStr, par->VIN_str
// 02/19/16     Vincent.T     1. Re-arrange printing animation.
// 03/08/16     Vincent.T     1. Add func. to detect any error event when printing.
//                            2. global variables blPrintError, blNoPaper
// 03/11/13     Vincent.T     1. global variable gfObjTestingTemp
//                            2. Add temperature printout info.
//                            3. Adjust PRT_0PA_100PA heeating time.
// 03/15/16     Vincent.T     1. Add printout(Battery test/Start-stop test) test code field.
// 03/16/16     Vincent.T     1. Modify some function input paremeters. (see printersvc.c/.h)
// 03/31/16     Vincent.T     1. Modify PrintResultAntiWhiteItem(); to handle multination language case.
// 04/14/16     Vincent.T     1. Add global variables IRvoltage, fIR for IR printout routine.
//                            2. Modify PrintResultClick(); due to IR test.
// 04/27/16     Vincent.T       1. Modify printout character(temperature).
// 05/20/16     Vincent.T     1. Modify temperature conversion between C and F.
// 06/14/16     Vincent.T     1. #include "..\driver/battcore_iocfg.h"
//                            2. Modify all the heating time and step motor time.(for low voltage drive)
// 06/21/16     Vincent.T     1. Modify IR value when over 999.99 or under 0.01 mini Ohm.(---.--)
//                            2. Re-Adjust all printing setting.(STB interval, heating time and STB setting)
// 06/24/16     Vincent.T     1. All measured CCA rating changed to CCA.
//                            2. Re-Adjust all printing setting(STB interval, some fields height)
// 06/28/16     Vincent.T     1. Mode Date time handler to main.c(PrintResultClick)
//                            2. global variables year, month, date, day, hour min and sec
// 07/04/16     Vincent.T     1. All heating time 1.3ms -> 1ms
// 08/10/16     Vincent.T     1. global variable extern blInitBatReplaceEvent to determine "REPLACE INTERNAL BATTERY" or not.
//                            2. make all time&date variables as global variables.
//                            3. modify IR printout formation. ex: 02.34m -> 2.34m
//                            4. add temperature handler when doing IR test
//                            5. modify all printer setting, ex: heating time, STB group, step-motor control and printout length.
// 08/24/16     Vincent.T     1. Modiy "Test Report", "Test Result" and "Battery Type" printout lenght.
// 09/06/16     Vincent.T     1. Modify most printing setting in PrintResultClick();
//                            2. Add test item printout(==BAT./SS/.SYS/IR TEST==).
//                            3. Printout "SAE" -> "CCA"
// 10/13/16     Ian.C         1. Add SOH & SOC figure.
//                            2. Modify tested result to make it bigger.
// 11/15/16     Ian.C         1. Modify PrintResultAntiWhiteItem(); for multi-language.
// 01/13/17     Vincent.T     1. global variables: g_RoVkybdTextEditStr, par->VIN_str and g_ui8TestCode;
//                            2. Modify PrintResultClick();(fixed logo langiage idx; add powered by DHC; removed testcode handler)
// 01/19/17     Vincent.T     1. Modify PrintResultAntiWhiteItem(); for 16x16 characters
//                            2. Modify PrintResultClick(); -> Remove TestCode-related variables; Add slinding effect.
// 02/17/17     Vincent.T     1. Modify PrintResultClick(); -> Crank. V is "No Detected", the content on priout "xx.xxv" -> "--.--v"0
// 03/08/17     Vincent.T     1. Modify g_RoVkybdTextEditStr[] size 4 -> 8
//                            2. Modify PrintResultClick(); due to RO/VIN are expanded.
// 03/22/17     Vincent.T     1. Modify PrintResultAntiWhiteItem(); to make sure print"--.--" when test items were detected as "NO DETECTED"
// 03/30/17     Vincent.T     1. Modify PrintResultClick(); to make sure temperature of printout same as PC.
// 06/13/17     Henry         FW version: BT2100_WW_V01.b
//                            1. Modify PrintResultClick() battery test and start stop test setcapacity rating printout:SAE -> CCA/SAE, JIS SAE -> CCA/SAE.
// 09/21/17 	Jerry.W		   Rewrite Widget for new software architecture
// 11/06/17     Henry.Y       1. Modify IR test result lower bound in PrintResultClick().
//                            2. Add defined symbol "NEUTRAL" for no logo version.
// 11/14/17		Jerry.W		  1. Add RC printing
//							  2. Modify RO VIN length
// 11/22/17     Henry.Y       1. Move TestCodeGenerater()/TestCounterInc()/TestRecord() to Displayresultwidget.c, remove PrintResult_Service().
//                            2. Modify PrintResultClick(): RC set and measured printout.
// 03/22/18     Henry.Y       System test add VIN and RO printout.
// 03/23/18     Jerry.W       Remove RO printout.
// 09/11/18     Henry.Y       1. Use systeminfo data struct to replace the global variables.
//                            2. Applied language hierarchical architecture of each screen display and print function.
//                            3. Use system error flag to replace the global variables.
// 10/22/18     Henry.Y       Add warning information before printing.
// 11/19/18     Henry.Y       Add low temperature(<5C) heater setup.
// 12/07/18     Henry.Y       New display and printing api.
// 03/20/19     Henry.Y       The test value trans to printout string same as the display way.
//                            Add new customized printing flow: print result flow and printing data covered after reset.
// 03/29/19     Henry.Y       Show the clamps voltage if the internal or external power not support printing.
// 08/21/20     David.Wang    Add check pack bat result , Print_CheckPack_ResultClick()
// 09/06/20                   SYS TEST RESULT vin code no used to mask from BT2200_HD_BO_V00.B
// 09/11/20                   ENTER BUTTON trigger time is too long correction
// 20201016     David Wang    Rating add CA/MCA
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/app/Printresultwidget.c#4 $
// $DateTime: 2017/12/19 10:59:01 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#include "Widgetmgr.h"
#define USER_PRINT_EXP_ENHANCEMENT 0

#if USER_PRINT_EXP_ENHANCEMENT
//static bool power_support_printing_flag = true;
//static float g_fval;
//static bool warning_on_off = false;
//static uint8_t loop_count = 0;
#endif

void PrintResultClick(void);
void Print_CheckPack_ResultClick(void);

void PrintResult_DISPLAY();
void PrintResult_LEFT_RIGHT();
void PrintResult_SELECT();
void PrintResult_initial();

widget* PrintResult_sub_W[] = {
		&NoPaper_widget,
		&ChkIntBat_widget,
		&CheckClamps_widget,
		&InsertPaper_widget
};

widget PrintResult_widget = {
		.Key_UP = 0,
		.Key_DOWN = 0,
		.Key_RIGHT = PrintResult_LEFT_RIGHT,
		.Key_LEFT = PrintResult_LEFT_RIGHT,
		.Key_SELECT = PrintResult_SELECT,
		.routine = 0,
		.service = 0,
		.initial = PrintResult_initial,
		.display = PrintResult_DISPLAY,
		.subwidget = PrintResult_sub_W,
		.number_of_widget = sizeof(PrintResult_sub_W)/4,
		.index = 1,
		.name = "PrintResult",
		//.parameter 0:first print 1:print again 2:warning
};

typedef enum{
	Data_save,
	Data_get
}PRINTING_DATA;
	
#if USER_PRINT_EXP_ENHANCEMENT
static bool PRINTING_DATA_PROCESS(PRINTING_DATA do_what){
	SYS_PAR* par = Get_system_parameter();
	TEST_DATA* TD = Get_test_data();

	if(do_what == Data_save){
		switch(par->WHICH_TEST){			
		case Start_Stop_Test:
		case Battery_Test:
		par->record.Bat_SS_test.Test_Type = (TEST_TYPE)par->WHICH_TEST;
		par->record.Bat_SS_test.Test_Time.year = TD->year;
		par->record.Bat_SS_test.Test_Time.month = TD->month;
		par->record.Bat_SS_test.Test_Time.day = TD->date;
		par->record.Bat_SS_test.Test_Time.hour = TD->hour;
		par->record.Bat_SS_test.Test_Time.minute = TD->min;
		par->record.Bat_SS_test.Test_Time.second = TD->sec;
		memcpy(par->record.Bat_SS_test.VIN, par->VIN_str, VININ_LEN1);	
		par->record.Bat_SS_test.Battery_Type = (par->WHICH_TEST == Start_Stop_Test)? (BATTERYTYPE)par->SS_test_par.SSBatteryType:(BATTERYTYPE)par->BAT_test_par.BatteryType;
		par->record.Bat_SS_test.Voltage = TD->battery_test_voltage;
		par->record.Bat_SS_test.Ratting = (par->WHICH_TEST == Start_Stop_Test)? (SELRATING)par->SS_test_par.SSSelectRating:(SELRATING)par->BAT_test_par.SelectRating;
		par->record.Bat_SS_test.CCA_Capacity = TD->SetCapacityVal; //(par->WHICH_TEST == Start_Stop_Test)? par->SS_test_par.SSSetCapacityVal:par->BAT_test_par.BTSetCapacityVal;
		par->record.Bat_SS_test.CCA_Measured = TD->Measured_CCA;
		par->record.Bat_SS_test.Judgement = (TEST_RESULT)TD->Judge_Result;
		par->record.Bat_SS_test.SOH = (uint8_t)TD->SOH;
		par->record.Bat_SS_test.SOC = (uint8_t)TD->SOC;
		memcpy(par->record.Bat_SS_test.Test_code, TD->TestCode, TESTCODE_LEN);	
		par->record.Bat_SS_test.Temperature = TD->ObjTestingTemp;
		//if(!save_system_parameter(&par->record.Bat_SS_test, sizeof(par->record.Bat_SS_test))) return 0;
		break;
		case System_Test:
		par->record.System_test.Test_Type = (TEST_TYPE)par->WHICH_TEST;
		par->record.System_test.Test_Time.year = TD->year;
		par->record.System_test.Test_Time.month = TD->month;
		par->record.System_test.Test_Time.day = TD->date;
		par->record.System_test.Test_Time.hour = TD->hour;
		par->record.System_test.Test_Time.minute = TD->min;
		par->record.System_test.Test_Time.second = TD->sec;			
		memcpy(par->record.System_test.VIN, par->VIN_str, VININ_LEN1);	
		par->record.System_test.Crank_result = (SYSRANK)TD->CrankVResult;
		par->record.System_test.Crank_voltage = TD->CrankVol;
		par->record.System_test.Idle_result = (SYSRANK)TD->AltIdleVResult;
		par->record.System_test.Idle_voltage = TD->IdleVol;
		par->record.System_test.Ripple_result = (SYSRANK)TD->RipVResult;
		par->record.System_test.Ripple_voltage = TD->ripple;
		par->record.System_test.Loaded_result = (SYSRANK)TD->AltLoadVResult;
		par->record.System_test.Loaded_voltage = TD->LoadVol;
		par->record.System_test.Temperature = TD->SysObjTemp;
		//if(!save_system_parameter(&par->record.System_test, sizeof(par->record.System_test))) return 0;
		break;
		case IR_Test:
		par->record.IR_test.Test_Type = (TEST_TYPE)par->WHICH_TEST;
		par->record.IR_test.Test_Time.year = TD->year;
		par->record.IR_test.Test_Time.month = TD->month;
		par->record.IR_test.Test_Time.day = TD->date;
		par->record.IR_test.Test_Time.hour = TD->hour;
		par->record.IR_test.Test_Time.minute = TD->min;
		par->record.IR_test.Test_Time.second = TD->sec;			
		par->record.IR_test.IR_Vol = TD->IRvoltage;
		par->record.IR_test.Measured_IR = TD->IR;
		par->record.IR_test.Temperature = TD->ObjTestingTemp;
		//if(!save_system_parameter(&par->record.IR_test, sizeof(par->record.IR_test))) return 0;
		break;
		}
		return (save_system_parameter(&par->record, sizeof(par->record)));
	}
	else{		
		switch(par->WHICH_TEST){			
		case Start_Stop_Test:
		case Battery_Test:
			memcpy(par->VIN_str, par->record.Bat_SS_test.VIN,VININ_LEN1);	
			TD->year = par->record.Bat_SS_test.Test_Time.year;
			TD->month = par->record.Bat_SS_test.Test_Time.month; 
			TD->date = par->record.Bat_SS_test.Test_Time.day;
			TD->hour = par->record.Bat_SS_test.Test_Time.hour;
			TD->min = par->record.Bat_SS_test.Test_Time.minute;
			TD->sec = par->record.Bat_SS_test.Test_Time.second;
			if(par->WHICH_TEST == Start_Stop_Test) par->SS_test_par.SSBatteryType = par->record.Bat_SS_test.Battery_Type;
			else par->BAT_test_par.BatteryType = par->record.Bat_SS_test.Battery_Type;

			TD->battery_test_voltage = par->record.Bat_SS_test.Voltage;

			if(par->WHICH_TEST == Start_Stop_Test) par->SS_test_par.SSSelectRating = par->record.Bat_SS_test.Ratting;
			else par->BAT_test_par.SelectRating = par->record.Bat_SS_test.Ratting;
			
			TD->SetCapacityVal = par->record.Bat_SS_test.CCA_Capacity;

			TD->Measured_CCA = par->record.Bat_SS_test.CCA_Measured;
			TD->Judge_Result = par->record.Bat_SS_test.Judgement;
			TD->SOH = par->record.Bat_SS_test.SOH;
			TD->SOC = par->record.Bat_SS_test.SOC;

			memcpy(TD->TestCode, par->record.Bat_SS_test.Test_code, TESTCODE_LEN);
			TD->ObjTestingTemp = par->record.Bat_SS_test.Temperature;
			if(check_vin(par->VIN_str,VININ_LEN1)) TD->Code_test = 1;
			break;
		case System_Test:
			TD->year = par->record.System_test.Test_Time.year;
			TD->month = par->record.System_test.Test_Time.month; 
			TD->date = par->record.System_test.Test_Time.day;
			TD->hour = par->record.System_test.Test_Time.hour;
			TD->min = par->record.System_test.Test_Time.minute;
			TD->sec = par->record.System_test.Test_Time.second;
			memcpy(par->VIN_str, par->record.System_test.VIN, VININ_LEN1);	
			
			TD->CrankVResult = par->record.System_test.Crank_result;
			TD->CrankVol = par->record.System_test.Crank_voltage;
			TD->AltIdleVResult = par->record.System_test.Idle_result;
			TD->IdleVol = par->record.System_test.Idle_voltage;
			TD->RipVResult = par->record.System_test.Ripple_result;
			TD->ripple = par->record.System_test.Ripple_voltage;
			TD->AltLoadVResult = par->record.System_test.Loaded_result;
			TD->LoadVol = par->record.System_test.Loaded_voltage;
			TD->SysObjTemp = par->record.System_test.Temperature;
			break;
		case IR_Test:
			TD->year = par->record.System_test.Test_Time.year;
			TD->month = par->record.System_test.Test_Time.month; 
			TD->date = par->record.System_test.Test_Time.day;
			TD->hour = par->record.System_test.Test_Time.hour;
			TD->min = par->record.System_test.Test_Time.minute;
			TD->sec = par->record.System_test.Test_Time.second;
			TD->IRvoltage = par->record.IR_test.IR_Vol;
			TD->IR = par->record.IR_test.Measured_IR;
			TD->ObjTestingTemp = par->record.IR_test.Temperature;
			break;
		}
	}
	return 1;
}
#endif

static bool wait_insert_paper;

void PrintResult_initial(){
	SYS_INFO* info = Get_system_info();
	SYS_PAR* par = Get_system_parameter();
	TEST_DATA* TD = Get_test_data();
//	if(info->pre_test_state != 2){
	    if(par->WHICH_TEST == System_Test){
	        uint8_t idle_test_result = 0;
	        uint8_t load_test_result = 0;
	        uint8_t crank_test_result = 0;
	        uint8_t ripple_test_result = 0;

	        // for bt2200 change to old ba427
	        // IDLE & LOAD
	        if( (SYSRANK)TD->AltIdleVResult == HIGH ){
	            idle_test_result = 3;
	        }
	        else if( (SYSRANK)TD->AltIdleVResult == LOW ){
	            idle_test_result = 2;
	        }
	        else {
	            idle_test_result = 1;
	        }

            // IDLE & LOAD
            if( (SYSRANK)TD->AltLoadVResult == HIGH ) {
                load_test_result = 3;
            }
            else if( (SYSRANK)TD->AltLoadVResult == LOW ){
                load_test_result = 2;
            }
            else {
                load_test_result = 1;
            }

            // CRANK
            if( (SYSRANK)TD->CrankVResult == NO_DETECT ){
                crank_test_result = 1;
            }
            else if( (SYSRANK)TD->CrankVResult == NORMAL){
                crank_test_result = 2;
            }
            else{
                crank_test_result = 3;
            }

            // ripple
            if( (SYSRANK)TD->RipVResult == NO_DETECT ){
                ripple_test_result = 1;
            }
            else if( (SYSRANK)TD->RipVResult == NORMAL){
                ripple_test_result = 2;
            }
            else{
                ripple_test_result = 3;
            }



	        SYSTEM_TEST_RECORD_T record = { .Test_Type = System_Test,
	                                        .Test_Time = {TD->year, TD->month, TD->date, TD->hour, TD->min, TD->sec,},
	                                        .Crank_result = (SYSRANK)crank_test_result,
	                                        .Crank_voltage = TD->CrankVol,
	                                        .Idle_result = (SYSRANK)idle_test_result,
	                                        .Idle_voltage = TD->IdleVol,
	                                        .Ripple_result = (SYSRANK)ripple_test_result,
	                                        .Ripple_voltage = TD->ripple,
	                                        .Loaded_result = (SYSRANK)load_test_result,
	                                        .Loaded_voltage = TD->LoadVol,
	                                        .Temperature = {'\0'},};

	        memcpy(record.VIN, par->VIN_str, VININ_LEN1);
	        record.VIN[VININ_LEN1] = '\0';
	        memcpy(record.RO, par->RO_str, ROIN_LEN);
	        record.RO[ROIN_LEN] = '\0';
	        sprintf(record.Email, get_input_email_str());
	        SaveTestRecords(System_Test,&record, sizeof(record), Wait_uplaod);

	        //BA427+: capability to do 24V printing
//	        if(TD->SYSTEM_CHANNEL == MAP_24V){
//	            PRINTING_DATA_PROCESS(Data_save);
//	            info->pre_test_state = 2;
//	            save_system_info(&info->pre_test_state, sizeof(info->pre_test_state));
//	            Widget_init(manual_test);
//	            return;
//	        }
	    }
	    else{   //Battery Test
	            uint8_t batype = 0;
	            uint8_t ssbatype = 0;
	            uint8_t BTtest_result = 0;

	            //bat type
	            if (par->WHICH_TEST == Battery_Test){

                    switch(par->BAT_test_par.BatteryType){
                    case AGM_FLAT_PLATE:
                        batype = 2;
                        break;
                    case AGM_SPIRAL:
                        batype = 1;
                        break;
                    case VRLA_GEL:
                        batype = 0;
                        break;
                    case FLOODED:
                    default:
                        batype = 3;
                        break;
                    }
                    //batype = par->BAT_test_par.BatteryType;
                    ssbatype = 2;
                }
                else{
                    batype = 4;
                    ssbatype = (par->SS_test_par.SSBatteryType == EFB)? 0:1;
                }

	        //bat rating
	        uint8_t rating = par->BAT_test_par.SelectRating;

            switch(rating){
            case JIS:
                rating = get_JIS_model_rating(par->BAT_test_par.BTJIS_RgBatSelect);
                break;
            case DIN:
                rating = 5;
                break;
            case IEC:
                rating = 4;
                break;
            case SAE:
                rating = 3;
                break;
            case MCA:
                rating = 2;
                break;
            case CA_MCA:
                rating = 1;
                break;
            case EN2:
            default:
                rating = 0;
                break;
            }
	        //if(rating == JIS){
            //    rating = get_JIS_model_rating(par->BAT_test_par.BTJIS_RgBatSelect);
            //}

            // test result Conversion
            switch(TD->Judge_Result){
                    case TR_GOODnPASS:
                        BTtest_result = 0;
                        break;
                    case TR_GOODnRRCHARGE:
                        BTtest_result = 1;
                        break;
                    case TR_RECHARGEnRETEST:
                        BTtest_result = 2;
                        break;
                    case TR_BADnREPLACE:
                        BTtest_result = 3;
                        break;
                    case TR_BAD_CELL_REPLACE:
                        BTtest_result = 4;
                        break;
                    case TR_CAUTION:
                        BTtest_result = 5;
                        break;
                    default :
                        TD->Judge_Result = TR_RECHARGEnRETEST;
                        BTtest_result = 2;
                        break;
                    }

            //save in record
	        BATTERY_SARTSTOP_RECORD_T record = {.Test_Type = Battery_Test,
	                                            .Battery_Type = (BATTERYTYPE)batype,
	                                            .SSBattery_Type = (BATTERYTYPE)ssbatype,
	                                            .Ratting = (SELRATING)rating,
	                                            .Voltage = TD->battery_test_voltage,
	                                            .CCA_Capacity = par->BAT_test_par.BTSetCapacityVal,
	                                            .CCA_Measured = TD->Measured_CCA,
	                                            .Judgement = (TEST_RESULT)BTtest_result, //TD->Judge_Result
	                                            .SOH = (uint8_t)TD->SOH,
	                                            .SOC = (uint8_t)TD->SOC,
	                                            .Temperature = TD->ObjTestingTemp,
	                                            .Test_code = {'\0'},
	                                            .JIS = {'\0'},
	                                            .IR = TD->IR,
	                                            .Test_Time = {TD->year, TD->month, TD->date, TD->hour, TD->min, TD->sec,},
	                                            .RC_Set = TD->SetRCVal,
	                                            .RC_Measured = TD->Measured_RC,
	                                            .RC_judegment = (RC_RESULT)TD->RCJudge_Result,
	                                            .IsBatteryCharged = TD->IsBatteryCharged,
	                                            .TestInVecicle = TD->TestInVehicle,
	                                            .PackTestNum = TD->PackTestNum,
	                                            .PackTestCount = TD->PackTestCount,};


	        snprintf((char*)record.JIS, sizeof(record.Test_code), (char*)TD->TestCode);

	        memcpy(record.VIN, par->VIN_str, VININ_LEN1);

	        snprintf((char*)record.Test_code, sizeof(record.Test_code), (char*)TD->TestCode);

	        memcpy(record.RO, par->RO_str, ROIN_LEN);
	        record.RO[ROIN_LEN] = '\0';

	        sprintf(record.Email, get_input_email_str());

	        SaveTestRecords(Battery_Test,&record, sizeof(record), Wait_uplaod);
	    }
        
        uint16_t update_count = get_saved_record_count();

        /*
        if(info->device_registered == 0){
            Message_by_language(unregister_result_stored, update_count,en_US);
        }
        else{
            if(wifi_get_status() == WIFI_off){
                Message_by_language(Store_result, update_count,en_US);
            }
            else{
                Message_by_language(Data_uploaded, update_count,en_US);
                upload_saved_recod();
            }
        }
        */
//	}
//	else{
//		// recover test record to TEST_DATA
//		PRINTING_DATA_PROCESS(Data_get);
//	}
	wait_insert_paper = false;
}


void PrintResult_DISPLAY(){
    char* YN;
    SYS_INFO* info = Get_system_info();
    ST7565ClearScreen(BUFFER1);
    ST7565Refresh(BUFFER1);
    if(wait_insert_paper){
        wait_insert_paper = false;
        if(MP205CoSensorDetect()){
            PrintResult_SELECT();
            return;
        }
    }
    if(PrintResult_widget.parameter == 0){
        if(info->pre_test_state != 2){
            LCD_String_ext(16,get_Language_String(S_PRINT_RESULT) , Align_Left);
            if(PrintResult_widget.index == 1){
                YN = get_Language_String(S_YES);
            }
            else{
                YN = get_Language_String(S_NO);
            }
            LCD_String_ext2(32,YN , Align_Right, 2);
        }
        else{
            LCD_String_ext(16,get_Language_String(S_PRINT_24V_SYSTEM) , Align_Left);
            if(PrintResult_widget.index == 1){
                YN = get_Language_String(S_RESULT_YES);
            }
            else{
                YN = get_Language_String(S_RESULT_NO);
            }
            LCD_String_ext(32,YN , Align_Left);
        }
        LCD_Arrow(Arrow_enter | Arrow_RL);
    }
    else{
        LCD_String_ext(16,get_Language_String(S_FINISHED) , Align_Central);
        LCD_String_ext(32,get_Language_String(S_PRESS_ENTER) , Align_Central);
        LCD_Arrow(Arrow_enter);
    }
}

void PrintResult_LEFT_RIGHT(){
    if(PrintResult_widget.parameter == 0){
        PrintResult_widget.index ^= 1;
        PrintResult_DISPLAY();
    }
}


void PrintResult_SELECT(){
    SYS_INFO* info = Get_system_info();
    TEST_DATA* TD = Get_test_data();

    if(PrintResult_widget.parameter == 1){
        PrintResult_widget.parameter = 0;
        PrintResult_DISPLAY();
        return;
    }

    if(!PrintResult_widget.index){
        info->pre_test_state = 0;
        save_system_info(&info->pre_test_state, sizeof(info->pre_test_state));
        Widget_init(manual_test);
    }
    else{

        if(!MP205CoSensorDetect()){ //no paper
            Move_widget(PrintResult_sub_W[3],0);
            wait_insert_paper = true;
            return;
        }

        PrintResultClick();

        if(printer_error & get_system_error()){
            clear_system_error(printer_error);
            if(No_paper & get_system_error()){
                clear_system_error(No_paper);
                Move_widget(PrintResult_sub_W[0],0);
            }
            else{
                Widget_init(manual_test);
            }
        }
        else{
            PrintResult_widget.parameter = 1;
            PrintResult_DISPLAY();
        }
    }
}




void PrintResultClick(void)
{
	//*************************************
	//Local variables for cca & v printout
	//*************************************
	SYS_PAR* par = Get_system_parameter();
	TEST_DATA* TD = Get_test_data();
	SYS_INFO* info = Get_system_info();

	//*************************
	//Printer a line bits(byte)
	//Initialize first
	//*************************
	uint8_t ui8PrinterALine[24] = {0};
    char data_str[16];
    uint8_t space_num;
	memset(ui8PrinterALine, ' ', 24);

	float BodyTestingTemp;
	float ObjTestingTemp;
	GetTemp(&BodyTestingTemp, &ObjTestingTemp);
	uint8_t low_temp_heating_flg = (BodyTestingTemp < 5)? 1:0;

	if(PrintErrorHanlder())
	{
		return;
	}
	//******************************************************************
	// -LOGO
	// -Due to open this func. to customer, all language -> one language
	//******************************************************************
	LANGUAGE LangIdx = get_language_setting();

	//************************************
	// Display Animation P1
	//************************************
	// Clears the screen image buffer
	ST7565ClearScreen(BUFFER0);

	// Refresh the clean screen buffer to see the results
	ST7565Refresh(BUFFER0);

	//
	// Print Animation P1
	//
	LCD_display(PRINTING, 0);
	//***********************************
	//***********************************

//#if defined(NEUTRAL)
//#else
	//
	// Go to print result.
	//
	PrinterPrintSpace(20);

	if(PrintErrorHanlder())
	{
		return;
	}
	set_language(en_US);
	PrinterStart( PRT_LOGO, (low_temp_heating_flg)? STB_123_456:STB_123_456, 1.8, 1, 2);
	set_language(LangIdx);
	//
	// - Powered by DHC
	// - Due to open this func. to customer, all language -> one language
	//
	/* NO PRINT WORDS
	PrinterStart( PRT_PBDHC, (low_temp_heating_flg)? STB_1_2_3_4_5_6:STB_1_2_3_4_5_6, 1.8, 1, 3);
	set_language(LangIdx);
	PrinterPrintSpace(20);

	if(PrintErrorHanlder())
	{
		return;
	}
	*/
//#endif
	//***************
	// Test Report
	//***************

	if(PrintErrorHanlder())
	{
		return;
	}
	PrinterPrintSpace(20);

	switch(par->WHICH_TEST)
	{
	//
	//	Start of case SS&BT
	//
	case Start_Stop_Test:
	case Battery_Test:
	{
		//***************************************
		// Test report
		//***************************************
	    Printer_string(get_Language_String(S_TEST_REPORT), 4, Printe_align_central);
	    PrinterPrintSpace(20);
	    //****************
	    //TEST FUNCTION
	    //****************	    
        Printer_string(get_Language_String(S_P_BATTERY_TEST), 4, Printe_align_central);
        PrinterPrintSpace(20);
		//************************************
		// Display Animation P2
		//************************************
		//
		// Print Animation P2
		//
		LCD_display(PRINTING, 1);
		//***********************************
		// Error  hanlder check
		//***********************************
		if(PrintErrorHanlder())
		{
			return;
		}
		//************************************
		// BATTERY TYPE
		//************************************
	    if(par->WHICH_TEST == Start_Stop_Test){
	        Printer_string(get_Language_String(S_START_STOP), 3, Printe_align_central);
            PrinterPrintSpace(20);
	    }
        uint8_t batype = (par->WHICH_TEST == Battery_Test )? par->BAT_test_par.BatteryType:par->SS_test_par.SSBatteryType;
        Printer_string(get_Language_String(get_batype_str_idx(par->WHICH_TEST, batype)), 3, Printe_align_central);
		PrinterPrintSpace(20);
		//***********
		// VOLTAGE:
		//***********
        sprintf((char*)ui8PrinterALine, get_Language_String(S_VOLTAGE));
        snprintf(data_str, sizeof(data_str),"%.2fV", TD->battery_test_voltage);
        space_num = PRINT_OPTION_SPACE - strlen(data_str);
        while(strlen((char*)ui8PrinterALine) < space_num+1){
            strcat((char*)ui8PrinterALine, " ");
        }
        strcat((char*)ui8PrinterALine, data_str);
        Printer_string((char*)ui8PrinterALine, 3, Printe_align_central);
        PrinterPrintSpace(10);
		//************
		// Rating
		//************
        uint8_t rating = par->BAT_test_par.SelectRating;
        if(rating == JIS){
            rating = get_JIS_model_rating(par->BAT_test_par.BTJIS_RgBatSelect);
        }
        sprintf((char*)ui8PrinterALine, get_Language_String(S_RATED));
        snprintf(data_str, sizeof(data_str),"%4d%s", TD->SetCapacityVal, get_Language_String(get_rating_str_idx(rating)));
        space_num = PRINT_OPTION_SPACE - strlen(data_str);
        while(strlen((char*)ui8PrinterALine) < space_num+1){
            strcat((char*)ui8PrinterALine, " ");
        }
        strcat((char*)ui8PrinterALine, data_str);
        Printer_string((char*)ui8PrinterALine, 3, Printe_align_central);
        PrinterPrintSpace(10);
		//****************
		// CCA(MEASURED):
		//****************
        sprintf((char*)ui8PrinterALine, get_Language_String(S_MEASURED));
        snprintf(data_str, sizeof(data_str),"%4d%s",(uint16_t)TD->Measured_CCA,get_Language_String(get_rating_str_idx(rating)));
        space_num = PRINT_OPTION_SPACE - strlen(data_str);
        while(strlen((char*)ui8PrinterALine) < space_num+1){
            strcat((char*)ui8PrinterALine, " ");
        }
        strcat((char*)ui8PrinterALine, data_str);
        Printer_string((char*)ui8PrinterALine, 3, Printe_align_central);
        PrinterPrintSpace(10);
        
		//***********************************
		//
		//    TEMPERATURE: XX C/ XX F
		//
		//***********************************
		#if 1

        //BA427+ VERSION
        uint8_t ctemp = TD->ObjTestingTemp;

        sprintf((char*)ui8PrinterALine, get_Language_String(S_P_TEMP));

        switch(ctemp){
         case 11:
             snprintf(data_str, sizeof(data_str), get_Language_String(S_OV37_F_C));
             break;
         case 10:
             snprintf(data_str, sizeof(data_str), get_Language_String(S_OV32_F_C));
             break;
         case 9:
             snprintf(data_str, sizeof(data_str), get_Language_String(S_OV26_F_C));
             break;
         case 8:
             snprintf(data_str, sizeof(data_str), get_Language_String(S_OV21_F_C));
             break;
         case 7:
             snprintf(data_str, sizeof(data_str), get_Language_String(S_OV15_F_C));
             break;
         case 6:
             snprintf(data_str, sizeof(data_str), get_Language_String(S_OV10_F_C));
             break;
         case 5:
             snprintf(data_str, sizeof(data_str), get_Language_String(S_OV4_F_C));
             break;
         case 4:
             snprintf(data_str, sizeof(data_str), get_Language_String(S_ON1_F_C));
             break;
         case 3:
             snprintf(data_str, sizeof(data_str), get_Language_String(S_ON6_F_C));
             break;
         case 2:
             snprintf(data_str, sizeof(data_str), get_Language_String(S_ON12_F_C));
             break;
         case 1:
             snprintf(data_str, sizeof(data_str), get_Language_String(S_ON17_F_C));
             break;
         case 0:
         default:
             BatTemp_widget.index = 0;
             snprintf(data_str, sizeof(data_str), get_Language_String(S_ON23_F_C));
             break;
         }

        space_num = PRINT_OPTION_SPACE - strlen(data_str);
        while(strlen((char*)ui8PrinterALine) < space_num+1+2){//° = C2 B0
            strcat((char*)ui8PrinterALine, " ");
        }
        strcat((char*)ui8PrinterALine, data_str);
        char *degree = "°";
        char *temp;
        temp = strstr((char*)ui8PrinterALine, degree);
        sprintf(temp, "~%s", temp+strlen(degree));
        temp = strstr(temp+1, degree);
        sprintf(temp, "~%s", temp+strlen(degree));
        Printer_string((char*)ui8PrinterALine, 3, Printe_align_central);
        PrinterPrintSpace(40);
        #else
        //BT2010_2200 VERSION
        char buf[25];
        char *degree = "°";
        char *temp;
        sprintf(buf, "%s", get_Language_String((TD->ObjTestingTemp)? S_P_BELOW_32F_0C : S_P_ABOVE_32F_0C));
        temp = strstr(buf, degree);
        sprintf(temp, "~%s", temp+strlen(degree));
        temp = strstr(temp+1, degree);
        sprintf(temp, "~%s", temp+strlen(degree));

        sprintf((char*)ui8PrinterALine, buf);
        while(strlen((char*)ui8PrinterALine) < 17){
            strcat((char*)ui8PrinterALine, " ");
        }
        Printer_string((char*)ui8PrinterALine, 3, Printe_align_central);
		PrinterPrintSpace(40);
        #endif
        //****************
        // Tested result
        //****************
        Printer_string(get_Language_String(get_result_str_idx(TD->Judge_Result)),4,Printe_align_central);
        PrinterPrintSpace(40);
		//************************************
		// Display Animation P3
		//************************************
		//
		// Print Animation P3
		//
		LCD_display(PRINTING, 2);
		//***********************************
		//***********************************

		if(PrintErrorHanlder())
		{
			return;
		}
		//*****************
		// STATE OF HEALTH
		//*****************
		sprintf((char*)ui8PrinterALine, get_Language_String(S_STATE_OF_HEALTH));
		Printer_string((char*)ui8PrinterALine, 3, Printe_align_central);
		PrinterPrintSpace(10);
		Printer_BT_Percent((uint8_t)TD->SOH);
		PrinterPrintSpace(20);
		//*****************
		// STATE OF CHARGE
		//*****************
		sprintf((char*)ui8PrinterALine, get_Language_String(S_STATE_OF_CHARGE));
		Printer_string((char*)ui8PrinterALine, 3, Printe_align_central);
		PrinterPrintSpace(10);
		Printer_BT_Percent((uint8_t)TD->SOC);
		PrinterPrintSpace(20);

		//*****************
		//    TEST CODE
		//*****************		
		Printer_string(get_Language_String(S_CODE), 3, Printe_align_central);
		PrinterPrintSpace(10);
        memset(ui8PrinterALine, 0, sizeof(ui8PrinterALine));
        memcpy(ui8PrinterALine, TD->TestCode, sizeof(TD->TestCode));
		Printer_string((char*)ui8PrinterALine, 3, Printe_align_central);
	    PrinterPrintSpace(20);

		break;
	}
	case System_Test:
	{
	    Printer_string(get_Language_String(S_TEST_REPORT), 4, Printe_align_central);
	    PrinterPrintSpace(20);
	    Printer_string(get_Language_String(S_STARTER_TEST), 4, Printe_align_central);
	    PrinterPrintSpace(10);

	    LCD_display(PRINTING, 1);

	    if(TD->CrankVResult == NO_DETECT){
	        Printer_string(get_Language_String(S_NO_DETECTED), 3, Printe_align_central);
	    }
	    else{
	        sprintf((char*)ui8PrinterALine, "%.2fV %s", TD->CrankVol, get_Language_String((TD->CrankVResult == NORMAL)? S_NORMAL:S_LOW));
	        Printer_string((char*)ui8PrinterALine, 3, Printe_align_central);
	    }
	    PrinterPrintSpace(30);

	    Printer_string(get_Language_String(S_CHARGING_TEST), 4, Printe_align_central);
	    PrinterPrintSpace(20);
	    Printer_string(get_Language_String(S_NO_LOAD), 3, Printe_align_central);

	    float temp = TD->IdleVol;
	    if(TD->SYSTEM_CHANNEL == MAP_24V || info->pre_test_state == 2) temp /= 2;
	    temp = (temp - ALT_IDLE_12V_VOL_LOW_ABOVE_0)*100/(ALT_IDLE_12V_VOL_NORMAL_ABOVE_0 - ALT_IDLE_12V_VOL_LOW_ABOVE_0);
	    Printer_SYS_Percent(temp, (TD->AltIdleVResult == LOW)? arrow_left : (TD->AltIdleVResult == HIGH)? arrow_right : arrow_middle);

	    PrinterPrintSpace(40);
	    Printer_string(get_Language_String(S_LOAD), 3, Printe_align_central);

	    temp = TD->LoadVol;
	    if(TD->SYSTEM_CHANNEL == MAP_24V || info->pre_test_state == 2) temp /= 2;
	    temp = (temp - ALT_LOAD_12V_VOL_LOW_ABOVE_0)*100/(ALT_LOAD_12V_VOL_NORMAL_ABOVE_0 - ALT_LOAD_12V_VOL_LOW_ABOVE_0);
	    Printer_SYS_Percent(temp, (TD->AltLoadVResult == LOW)? arrow_left : (TD->AltLoadVResult == HIGH)? arrow_right : arrow_middle);
	    PrinterPrintSpace(40);

        sprintf((char*)ui8PrinterALine, get_Language_String(S_LOAD_OFF));
        snprintf(data_str, sizeof(data_str),"%.2fV", TD->IdleVol);
        space_num = PRINT_OPTION_SPACE - strlen(data_str);
        while(strlen((char*)ui8PrinterALine) < space_num+1){
            strcat((char*)ui8PrinterALine, " ");
        }
        strcat((char*)ui8PrinterALine, data_str);
	    Printer_string((char*)ui8PrinterALine, 3, Printe_align_central);
	    PrinterPrintSpace(10);

	    LCD_display(PRINTING, 2);

        sprintf((char*)ui8PrinterALine, get_Language_String(S_LOAD_ON));
        snprintf(data_str, sizeof(data_str),"%.2fV", TD->LoadVol);
        space_num = PRINT_OPTION_SPACE - strlen(data_str);
        while(strlen((char*)ui8PrinterALine) < space_num+1){
            strcat((char*)ui8PrinterALine, " ");
        }
        strcat((char*)ui8PrinterALine, data_str);
	    Printer_string((char*)ui8PrinterALine, 3, Printe_align_central);
	    PrinterPrintSpace(30);

	    Printer_string(get_Language_String(S_DIODE_RIPPLE), 3, Printe_align_central);
	    PrinterPrintSpace(10);

	    if(TD->RipVResult == NO_DETECT){
	        Printer_string(get_Language_String(S_NO_RIPPLE_DETECT), 3, Printe_align_central);
	    }
	    else{
	        uint32_t s_list[] = {S_NO_DETECTED, S_LOW, S_NORMAL, S_HIGH};
	        uint32_t len = strlen(get_Language_String(s_list[TD->RipVResult]));
	        len++;
	        if(len < 7) len = 7;
	        sprintf((char*)ui8PrinterALine, "%.2fV%*s", TD->ripple, len, get_Language_String(s_list[TD->RipVResult]));
	        Printer_string((char*)ui8PrinterALine, 3, Printe_align_central);
	    }
	    PrinterPrintSpace(10);

	    break;
	}

	default:
		UARTprintf("printresult TEST_TYPE error\n");
		return;
	}

	//************************************
	// Display Animation P4
	//************************************
	LCD_display(PRINTING, 3);
	//***********************************
	//***********************************

	if(PrintErrorHanlder())
	{
		return;
	}

    /*
	#if 1//middle side
    // RO
    Printer_string(get_Language_String(S_P_RO_NUMBER), 3, Printe_align_central);
    PrinterPrintSpace(10);
    memset(ui8PrinterALine, 0, sizeof(ui8PrinterALine));
    memcpy(ui8PrinterALine, par->RO_str, sizeof(par->RO_str));
    Printer_string((char*)ui8PrinterALine, 3, Printe_align_central);
    PrinterPrintSpace(20);
    // VIN
    Printer_string(get_Language_String(S_P_VIN_NUMBER), 3, Printe_align_central);
    PrinterPrintSpace(10);
    memset(ui8PrinterALine, 0, sizeof(ui8PrinterALine));
    memcpy(ui8PrinterALine, par->VIN_str, sizeof(par->VIN_str));
    Printer_string((char*)ui8PrinterALine, 3, Printe_align_central);
    PrinterPrintSpace(20);
    #else//left/right side
    // RO
	Printer_string2(get_Language_String(S_ENTER_RO), 3, Printe_align_left, 4);
    memcpy(ui8PrinterALine, par->RO_str, sizeof(par->RO_str));
	Printer_string2(ui8PrinterALine, 3, Printe_align_right, 2);
    PrinterPrintSeperate(2);
    // VIN
	Printer_string2(get_Language_String(S_ENTER_VIN), 3, Printe_align_left, 4);
    memcpy(ui8PrinterALine, par->VIN_str, sizeof(par->VIN_str));
	Printer_string2(ui8PrinterALine, 3, Printe_align_right, 2);
    PrinterPrintSeperate(2);
    #endif
    */
    //***********
	// CLIENT:
	//***********
	PrinterPrintSpace(20); //20201020 DW Enlarged print fonts need more space 10=>20
	Printer_string2(get_Language_String(S_CLIENT), 3, Printe_align_left, 4);
	PrinterPrintSpace(40);

	//************
	// TEST DATE:
	//************
	Printer_string2(get_Language_String(S_TEST_DATE), 3, Printe_align_left, 4);

	PrinterPrintSpace(10);

	if(PrintErrorHanlder())
	{
	    return;
	}

	memset(ui8PrinterALine, ' ', sizeof(ui8PrinterALine));

	sprintf((char*)ui8PrinterALine+6, "%04d/%02d/%02d", TD->year, TD->month, TD->date);
	Printer_string((char*)ui8PrinterALine, 3, Printe_align_left);

	PrinterPrintSpace(10);

	if(PrintErrorHanlder())
	{
	    return;
	}

	memset(ui8PrinterALine, ' ', sizeof(ui8PrinterALine));

	sprintf((char*)ui8PrinterALine+8, "%02d:%02d:%02d", TD->hour, TD->min, TD->sec);
	Printer_string((char*)ui8PrinterALine, 3, Printe_align_left);

	if(PrintErrorHanlder())
	{
		return;
	}

	PrinterPrintSpace(20);

	//********
	// By:
	//********	
	Printer_string2(get_Language_String(S_BY), 3, Printe_align_left, 4);
	PrinterPrintSpace(40);

	if(info->Has_DIY){
	    uint32_t i;
	    for(i = 0 ; i < 5 ; i++){
	        Printer_string2(info->VkybdTextEditStr[i], 3, Printe_align_left, 4);
	        PrinterPrintSpace(10);
	    }

	    PrinterPrintSpace(250);
	    return;
	}

	if(PrintErrorHanlder())
	{
	    return;
	}

	PrinterPrintSpace(200);

}

#if 0
void Print_CheckPack_ResultClick(void)
{
    //*************************************
    // CALL RECORED DATA REDISTER
    //*************************************
    uint8_t ui8RecordBuf[128] = {0x00};
    uint16_t ui16TestRecordIdx = 0;
    uint32_t ui32TestRecord = 0;
    uint32_t PackTestRecord = 0;
    int i;
    //*************************************
    //Local variables for cca & v printout
    //*************************************
    SYS_PAR* par = Get_system_parameter();
    TEST_DATA* TD = Get_test_data();
    SYS_INFO* info = Get_system_info();

    //*************************
    //Printer a line bits(byte)
    //Initialize first
    //*************************
    uint8_t ui8PrinterALine[24] = {0};
    memset(ui8PrinterALine, ' ', 24);

    //*************
    // JIS Handler
    //*************
    uint8_t sLen = 0;
    uint8_t l = 0;

    //***************************
    // Info. Handler
    //***************************
    uint8_t ui8LopIdx = 0;
    uint8_t ui8Dat = 0;
    uint8_t ui8last = 0;
    uint8_t ui8TotalIdx = 0;

    //******************************************************
    //   Local variables for testing temperature printout
    //******************************************************
    float TempC = 0.0;
    float TempF = 0.0;
    uint16_t ui16PrtTestedTempCBuf[5] = {0};
    uint16_t ui16PrtTestedTempFBuf[5] = {0};

    //******************************************************
    // RO/VIN/LICENSE NUMBER
    //******************************************************
    uint8_t ui8NumLopIdx = 0;

    float BodyTestingTemp;
    float ObjTestingTemp;
    GetTemp(&BodyTestingTemp, &ObjTestingTemp);
    uint8_t low_temp_heating_flg = (BodyTestingTemp < 5)? 1:0;


    //******************************************************************
    // -LOGO
    // -Due to open this func. to customer, all language -> one language
    //******************************************************************
    LANGUAGE LangIdx = get_language_setting();

    //************************************
    // Display Animation P1
    //************************************
    // Clears the screen image buffer
    ST7565ClearScreen(BUFFER0);

    // Refresh the clean screen buffer to see the results
    ST7565Refresh(BUFFER0);

    //
    // Print Animation P1
    //
    LCD_display(PRINTING, 0);
    //***********************************
    //***********************************

    //#if defined(NEUTRAL)
    //#else
    //
    // Go to print result.
    //
    PrinterPrintSpace(20);

    if(PrintErrorHanlder())
    {
        return;
    }
    set_language(en_US);
    PrinterStart( PRT_LOGO, (low_temp_heating_flg)? STB_1_2_3_4_5_6:STB_1_2_3_4_5_6, 1.8, 1, 3);
    set_language(LangIdx);

    //***************
    // Test Report
    //***************
    Print_String_by_language(P_TEST_REPORT,0);
    PrinterPrintSpace(20); //20201020 DW Enlarged print fonts need more space 10=>20

    if(PrintErrorHanlder())
    {
        return;
    }

    Print_String_by_language(P_TEST_TYPE,0);
    PrinterPrintSpace(20); //20201020 DW Enlarged print fonts need more space 10=>20

    //*****************************************
    // CALL FASH DATA FOR ALL PACK TEST RESULT
    //*****************************************
    for( i = (TD->PackTestNum+1); i>0; i-- ){
        //***** Read the first battery data *****//
        //Read test record counter
        ui32TestRecord = info->TotalTestRecord;
        ui16TestRecordIdx = info->TestRecordOffset;
        //Read the desired battery test data
        PackTestRecord = ((ui32TestRecord + ui16TestRecordIdx) % MAX_BTSSTESTCOUNTER) - i ;
        TestRecordRead(PackTestRecord, ui8RecordBuf);
        //Get the test result TO print
        BATTERY_SARTSTOP_RECORD_T* BT_rec = (BATTERY_SARTSTOP_RECORD_T*)ui8RecordBuf;
        TD->ObjTestingTemp = BT_rec->Temperature;
        TD->Judge_Result = BT_rec->Judgement;
        TD->battery_test_voltage = BT_rec->Voltage;
        par->BAT_test_par.SelectRating = BT_rec->Ratting;
        TD->SetCapacityVal = BT_rec->CCA_Capacity;
        TD->Measured_CCA = BT_rec->CCA_Measured;
        par->WHICH_TEST = Battery_Test;

        if(ui8NumLopIdx==0){
            TD->SetCapacityVal = TD->SetCapacityVal * TD->PackTestNum;
        }

    //*************************
    //  Temperature Handler
    //*************************
    //*****************************
    //   Avoid to print 0000
    //*****************************
    TempF = ((TD->ObjTestingTemp * 9) / 5) + 32;

    if(TempF < 0)
    {
        TempF = TempF * (-1);
        ui16PrtTestedTempFBuf[0] = '-';
    }
    else
    {
        //TempF = TempF;
        ui16PrtTestedTempFBuf[0] = ' ';
    }

    ui16PrtTestedTempFBuf[1] = (((uint16_t)(TempF *10)) / 1000) + 0x30;
    ui16PrtTestedTempFBuf[2] = (((uint16_t)(TempF *10)) % 1000) / 100 + 0x30;
    ui16PrtTestedTempFBuf[3] = (((uint16_t)(TempF *10)) % 100) / 10 + 0x30;
    ui16PrtTestedTempFBuf[4] = (((uint16_t)(TempF *10)) % 10) + 0x30;
    if(ui16PrtTestedTempFBuf[1] == '0')
    {
        ui16PrtTestedTempFBuf[1] = ' ';
        if(ui16PrtTestedTempFBuf[2] == '0')
        {
            ui16PrtTestedTempFBuf[2] = ' ';
        }
    }

    if(TD->ObjTestingTemp < 0)
    {
        TempC = TD->ObjTestingTemp * (-1);
        ui16PrtTestedTempCBuf[0] = '-';
    }
    else
    {
        TempC = TD->ObjTestingTemp;
        ui16PrtTestedTempCBuf[0] = ' ';
    }
    ui16PrtTestedTempCBuf[1] = (((uint16_t)(TempC *10)) / 1000) + 0x30;
    ui16PrtTestedTempCBuf[2] = (((uint16_t)(TempC *10)) % 1000) / 100 + 0x30;
    ui16PrtTestedTempCBuf[3] = (((uint16_t)(TempC *10)) % 100) / 10 + 0x30;
    ui16PrtTestedTempCBuf[4] = (((uint16_t)(TempC *10)) % 10) + 0x30;
    if(ui16PrtTestedTempCBuf[1] == '0')
    {
        ui16PrtTestedTempCBuf[1] = ' ';
        if(ui16PrtTestedTempCBuf[2] == '0')
        {
            ui16PrtTestedTempCBuf[2] = ' ';
        }
    }

    if(PrintErrorHanlder()){
        return;
    }

        //***************************************
        // PACK TEST PRINTER Start
        //***************************************
        //*****************
        // BATTERY #Number
        //*****************
        if( ui8NumLopIdx >0 ){
            Print_String_by_language(P_BATTERY_NUM,ui8NumLopIdx);
            PrinterPrintSpace(20); //20201020 DW Enlarged print fonts need more space 10=>20
        }
        ui8NumLopIdx ++;

        //****************
        // Tested result
        //****************
        Print_String_by_language(P_TEST_RESULT,0);
        PrinterPrintSpace(20); //20201020 DW Enlarged print fonts need more space 10=>20

        //************************************
        // Display Animation P2
        //************************************
        //
        // Print Animation P2
        //
        LCD_display(PRINTING, 1);
        //***********************************
        //***********************************

        if(PrintErrorHanlder())
        {
            return;
        }

        if( ui8NumLopIdx >0 ){
            Print_String_by_language(P_BATTERY_TYPE,0);
            PrinterPrintSpace(20); //20201020 DW Enlarged print fonts need more space 10=>20
        }

        //***********
        // VOLTAGE:
        //***********
        Print_String_by_language(P_VOLTAGE,0);
        snprintf((char*)ui8PrinterALine+16,sizeof(ui8PrinterALine),"%5.2f ",TD->battery_test_voltage);
        ui8PrinterALine[strlen((char*)ui8PrinterALine)] = 'V';
        PrinterStartWB(ui8PrinterALine, (low_temp_heating_flg)? STB_1_2_3_4_5_6:STB_ALL, 1.5, 1, 3);
        memset(ui8PrinterALine, ' ', 24);
        PrinterPrintSeperate(2);

        //**********
        // Rating
        //**********
        Print_String_by_language(P_RATED,0);
        if(((par->BAT_test_par.SelectRating == JIS)&&(par->WHICH_TEST == Battery_Test)) || ((par->WHICH_TEST == Start_Stop_Test)&&(par->SS_test_par.SSSelectRating == JIS))){
            sLen = strlen(/*pg_JIS[TD->JISCapSelect].batString*/(char*)par->JIS_BAT_STRING);
            for(l = 0; l < sLen; l++)
            {
                ui8PrinterALine[22 - l] =  /*pg_JIS[TD->JISCapSelect].batString*/par->JIS_BAT_STRING[sLen - l - 1];
            }
            PrinterStartWB(ui8PrinterALine, (low_temp_heating_flg)? STB_1_2_3_4_5_6:STB_ALL, 1.5, 1, 3);
            memset(ui8PrinterALine, ' ', 24);
        }
        else{
            switch(( par->WHICH_TEST == Battery_Test )? par->BAT_test_par.SelectRating:par->SS_test_par.SSSelectRating){
            case EN1:
                snprintf((char*)ui8PrinterALine+16,sizeof(ui8PrinterALine),"%4d EN",TD->SetCapacityVal);
                break;
            case EN2:
                snprintf((char*)ui8PrinterALine+15,sizeof(ui8PrinterALine),"%4d EN",TD->SetCapacityVal);
                break;
            case IEC:
                snprintf((char*)ui8PrinterALine+15,sizeof(ui8PrinterALine),"%4d IEC",TD->SetCapacityVal);
                break;
            case DIN:
                snprintf((char*)ui8PrinterALine+15,sizeof(ui8PrinterALine),"%4d DIN",TD->SetCapacityVal);
                break;
            case CA_MCA:
                snprintf((char*)ui8PrinterALine+15,sizeof(ui8PrinterALine),"%4d CA",TD->SetCapacityVal);
                break;
            case MCA://TODO
                snprintf((char*)ui8PrinterALine+15,sizeof(ui8PrinterALine),"%4d MCA",TD->SetCapacityVal);
                break;
            default:
                snprintf((char*)ui8PrinterALine+15,sizeof(ui8PrinterALine),"%4d CCA",TD->SetCapacityVal);
                break;
            }
            ui8PrinterALine[strlen((char*)ui8PrinterALine)] = ' ';

            PrinterStartWB(ui8PrinterALine, (low_temp_heating_flg)? STB_1_2_3_4_5_6:STB_ALL, 1.5, 1, 3);
            memset(ui8PrinterALine, ' ', 24);
        }
        PrinterPrintSeperate(2);

        //****************
        // CCA(MEASURED):
        //****************
        Print_String_by_language(P_MEASURED_CCA,0);
        switch(( par->WHICH_TEST == Battery_Test )? par->BAT_test_par.SelectRating:par->SS_test_par.SSSelectRating){
            case EN1:
                snprintf((char*)ui8PrinterALine+15,sizeof(ui8PrinterALine),"%4d EN",(uint16_t)TD->Measured_CCA);
                break;
            case EN2:
                snprintf((char*)ui8PrinterALine+15,sizeof(ui8PrinterALine),"%4d EN",(uint16_t)TD->Measured_CCA);
                break;
            case IEC:
                snprintf((char*)ui8PrinterALine+15,sizeof(ui8PrinterALine),"%4d IEC",(uint16_t)TD->Measured_CCA);
                break;
            case DIN:
                snprintf((char*)ui8PrinterALine+15,sizeof(ui8PrinterALine),"%4d DIN",(uint16_t)TD->Measured_CCA);
                break;
            case CA_MCA:
                snprintf((char*)ui8PrinterALine+15,sizeof(ui8PrinterALine),"%4d CA",(uint16_t)TD->Measured_CCA);
                break;
            case MCA://TODO
                snprintf((char*)ui8PrinterALine+15,sizeof(ui8PrinterALine),"%4d MCA",(uint16_t)TD->Measured_CCA);
                break;
            default:
                snprintf((char*)ui8PrinterALine+15,sizeof(ui8PrinterALine),"%4d CCA",(uint16_t)TD->Measured_CCA);
                break;
        }
        ui8PrinterALine[strlen((char*)ui8PrinterALine)] = ' ';
        PrinterStartWB(ui8PrinterALine, (low_temp_heating_flg)? STB_1_2_3_4_5_6:STB_ALL, 1.5, 1, 3);
        memset(ui8PrinterALine, ' ', 24);
        PrinterPrintSeperate(2);

        //***********************************
        //
        //    TEMPERATURE: XX C/ XX F
        //
        //***********************************
        Print_String_by_language(P_TEMPERATURE,0);
        ui8PrinterALine[8] = ui16PrtTestedTempFBuf[0];
        ui8PrinterALine[9] = ui16PrtTestedTempFBuf[1];
        ui8PrinterALine[10] = ui16PrtTestedTempFBuf[2];
        ui8PrinterALine[11] = ui16PrtTestedTempFBuf[3];
        ui8PrinterALine[12] = '.';
        ui8PrinterALine[13] = ui16PrtTestedTempFBuf[4];
        ui8PrinterALine[14] = wb_doF;
        ui8PrinterALine[15] = '/';
        ui8PrinterALine[16] = ui16PrtTestedTempCBuf[0];
        ui8PrinterALine[17] = ui16PrtTestedTempCBuf[1];
        ui8PrinterALine[18] = ui16PrtTestedTempCBuf[2];
        ui8PrinterALine[19] = ui16PrtTestedTempCBuf[3];
        ui8PrinterALine[20] = '.';
        ui8PrinterALine[21] = ui16PrtTestedTempCBuf[4];
        ui8PrinterALine[22] = wb_doC;
        PrinterPrintSpace(10);
        PrinterStartWB(ui8PrinterALine, (low_temp_heating_flg)? STB_1_2_3_4_5_6:STB_ALL, 1.5, 1, 3);
        memset(ui8PrinterALine, ' ', 24);
        PrinterPrintSeperate(2);
        PrinterPrintSpace(10);

        PrinterPrintSpace(20);                 // 20200824 DW EDIT

    //***********************************
    // Display Animation P4
    //***********************************
    LCD_display(PRINTING, 3);

    //***********************************
    //***********************************
    if(PrintErrorHanlder()){
        return;
    }
  }// for loop end 20200821 dw edit

    //***********************************
    // PACK TEST print end warning 20200821 DW EDIT
    //***********************************
    Print_String_by_language(P_Warning_word,0);
    PrinterPrintSpace(10);
    Print_String_by_language(P_Warning_word1,0);
    PrinterPrintSpace(10);
    Print_String_by_language(P_Warning_word2,0);
    PrinterPrintSpace(10);
    Print_String_by_language(P_Warning_word3,0);
    PrinterPrintSpace(80);

    //***********
    // CLIENT:
    //***********
    Print_String_by_language(P_NOTE,0);
    PrinterPrintSpace(80);

    //************
    // TEST DATE:
    //************
    Print_String_by_language(P_TEST_DATE,0);
    PrinterPrintSpace(10);
    snprintf((char*)ui8PrinterALine+4,sizeof(ui8PrinterALine),"%04d/%02d/%02d %02d:%02d:%02d",TD->year,TD->month,TD->date,TD->hour,TD->min,TD->sec);
    ui8PrinterALine[strlen((char*)ui8PrinterALine)] = ' ';
    PrinterStartWB(ui8PrinterALine, (low_temp_heating_flg)? STB_1_2_3_4_5_6:STB_ALL, 1.5, 1, 3);
    memset(ui8PrinterALine, ' ', 24);
    PrinterPrintSpace(20); //20201020 DW Enlarged print fonts need more space 10=>20

    if(PrintErrorHanlder())
    {
        return;
    }

    //********
    // By:
    //********
    Print_String_by_language(P_INFORMATION,0);
    //*********************************
    //
    // Read info->VkybdTextEditStr[128]
    //
    //*********************************
    ui8last = 128;

    while(ui8last && (info->VkybdTextEditStr[ui8last -1] == 0x00 || info->VkybdTextEditStr[ui8last -1] == 0x20)) ui8last--;


    for(ui8TotalIdx = 0; ui8TotalIdx < 8; ui8TotalIdx++)
    {
        if(ui8Dat >= ui8last) break;

        for(ui8LopIdx = 4; ui8LopIdx < 20; ui8LopIdx++)
        {
            if(ui8Dat < 128)
            {
                ui8PrinterALine[ui8LopIdx] = info->VkybdTextEditStr[ui8Dat];
                if(ui8PrinterALine[ui8LopIdx]  == 0)
                {
                    ui8PrinterALine[ui8LopIdx]  = ' ';
                }
                ui8Dat++;
            }
            else
            {
                ui8PrinterALine[ui8LopIdx] = ' ';
            }
        }
        PrinterStartWB(ui8PrinterALine, (low_temp_heating_flg)? STB_1_2_3_4_5_6:STB_123_456, 1.5, 1, 3);
        memset(ui8PrinterALine, ' ', 24);
    }

    if(PrintErrorHanlder())
    {
        return;
    }

    if(ui8last == 0){
        PrinterPrintSpace(100);
    PrinterPrintSpace(200);
    }
    else{
        PrinterPrintSpace(200);
    }


}
#endif

