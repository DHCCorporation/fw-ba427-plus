/*
 * ClearMemory_widget.c
 *
 *  Created on: 2020��蕭12嚙踝蕭嚙�24嚙踐��
 *      Author: ray.yuan
 */

#include "Widgetmgr.h"


void ClearMemory_LEFT_RIGHT();
void ClearMemory_display();
void ClearMemory_SELECT();
void ClearMemory_Init();

widget* ClearMemory_sub_W[] = {
};

uint8_t Mem_fin_index = 0;

widget ClearMemory_widget = {
        .Key_UP = 0,
        .Key_DOWN = 0,
        .Key_RIGHT = ClearMemory_LEFT_RIGHT,
        .Key_LEFT = ClearMemory_LEFT_RIGHT,
        .Key_SELECT = ClearMemory_SELECT,
        .routine = 0,
        .service = 0,
        .initial = ClearMemory_Init,
        .display = ClearMemory_display,
        .subwidget = ClearMemory_sub_W,
        .number_of_widget = sizeof(ClearMemory_sub_W)/4,
        .index = 0,
        .name = "ClearMemory",
};

void ClearMemory_Init(){
    Configration_widget.index = 0;
}

void ClearMemory_LEFT_RIGHT(){
    Configration_widget.index ^= 1;
    ClearMemory_display();
}


void ClearMemory_display(){
    SYS_INFO* inf = Get_system_info();
    ST7565ClearScreen(BUFFER1);
    ST7565Refresh(BUFFER1);

    if ( Mem_fin_index == 0 ){
        LCD_String_ext(16,get_Language_String(S_ARE_YOU_SURE) , Align_Left);

        if(Configration_widget.index == 1){
            LCD_String_ext2(32,get_Language_String(S_YES) , Align_Right, 1);
        }
        else{
            LCD_String_ext2(32,get_Language_String(S_NO) , Align_Right, 1);
        }
    }
    else{
        LCD_String_ext(16,get_Language_String(S_CLEARING_MEMORY) , Align_Central);
        LCD_String_ext(32,get_Language_String(S_FINISHED) , Align_Central);
        LCD_String_ext(48,get_Language_String(S_PRESS_ENTER) , Align_Central);
    }
	LCD_Arrow(Arrow_enter | Arrow_RL);
}

void ClearMemory_SELECT(){
    SYS_INFO* info = Get_system_info();

    if(Mem_fin_index == 0 && Configration_widget.index == 1){
        Mem_fin_index = 1;
        info->TotalTestRecord = 0;
        info->TestRecordOffset = 0;
        save_system_info(&info->TestRecordOffset, sizeof(info->TestRecordOffset));
        save_system_info(&info->TotalTestRecord, sizeof(info->TotalTestRecord));
        ClearMemory_display();
    }
    else{
        Return_pre_widget();
    }
}



