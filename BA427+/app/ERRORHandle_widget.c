//*****************************************************************************
//
// Source File: ERRORHandle_widget.c
// Description: System error handler.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 09/04/18     Henry.Y        Create file.
// =============================================================================
//
//
// $Header$
// $DateTime$
// $Author$
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#include "Widgetmgr.h"


void ERRORHandle_routine();
void ERRORHandle_display();
void ERRORHandle_SELECT();


 widget* ERRORHandle_sub_W[] = {

 };

 widget ERRORHandle_widget = {
		 .Key_UP = 0,
		 .Key_DOWN = 0,
		 .Key_RIGHT = 0,
		 .Key_LEFT = 0,
		 .Key_SELECT = ERRORHandle_SELECT,
		 .routine = ERRORHandle_routine,
		 .service = 0,
		 .initial = 0,
		 .display = ERRORHandle_display,
		 .subwidget = ERRORHandle_sub_W,
		 .number_of_widget = sizeof(ERRORHandle_sub_W)/4,
		 .index = 0,
		 .no_messgae = 1,
		 .name = "ERRORHandle",
 };




 void ERRORHandle_routine(){

 }


 void ERRORHandle_display(){


 }


 void ERRORHandle_SELECT(){

 }



