//*****************************************************************************
//
// Source File: Setcapwidget.c
// Description: Set Capacity widget including normal setting via Up/Down key, 
//				and press Enter key to pop up numeric keyboard to edit the specific
//				capacity number directly. Press Left key to pop up the stack and 
//				press Right key to start to "Battery Test" or "Start Stop Test".
//
//
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 09/17/15     William.H      Created file.
// 10/01/15     William.H      Re-arrange/remove Global varaible's usage due to we've used stack to behave the manu level selection.
//                             Respective global varaible by the respective widget file to be maintained by their own.
// 10/05/15     William.H      Check in BT2020 project which branched from LCDFontMaker(Pre-BT2020) by modifed related GPIOs.
// 10/19/15     William.H      To add "CHKSURFACECHARGE" entry, enter into "CHKSURFACECHARGE" entry when press right key at "Set Capacity" menu.
// 10/22/15     William.H      To modify the art knitting and circular in the edge frame of edit box, the up/down arrow's anti-white region need to be modified accordingly.
// 10/29/15     Vincent.T      Add HY's part: SetCapRight() -> MenuLvlStack_Push(&g_MenuLvlStack, CHECKRIPPLE);
// 11/04/15     Vincent.T      Modified max cca setting val(capVal) 1685 -> g_MaxCCASet
// 11/18/15     Vincent.T      1. Let CCA Setting Val eql 0, if virtual keyborad event is on in SetCCA Page.
//                             2. Hide navigation icon due to QA's suggestion
// 01/29/16     Vincent.T      1. global variables: g_SSSelectRating, WHICH_TEST
//                             2. Modify SetCapLeft(); for start-stop test
//                             3. #include "../service\testrecordsvc.h"
//                             4. global variables: g_BTSetCapacityVal, g_SSSetCapacityVal; To seperate SS and BT.
// 03/28/16     Vincent.T      1. Modify SelRatingAntiWhiteItem(); to handle multination language case.
// 03/29/16     Vincent.T      1. Modify SetCapClick(); to handle multination language case.
// 03/30/16     Vincent.T      1. Modify SetCapRight(); to handle multination language case.
// 09/21/17 	Jerry.W		   Rewrite Widget for new software architecture
// 11/14/17		Jerry.W		   1. Modify diraction of next widget for Hyundai
// 11/22/17     Henry.Y        1. Set #define of CCA min. value.
//                             2. Fix wrong act of press down key.
// 12/01/17     Jerry.W        1. add SetCapNormal_INIT function
//                             2. add temp_g_SetCapacityVal for TD->SetCapacityVal conversion
// 03/22/18     William.H      Modify ChkSurfaceCharge_sub_W[] to enter to CountDown15S_widget directly if its Test-In-Vehcile. 
// 05/03/18     Henry.Y        add EN1 and EN2.
// 09/11/18     Henry.Y        Use systeminfo data struct to replace the global variables.
// 12/07/18     Henry.Y        New display api.
// 16/10/20     David Wang     Mask CCAValConvert to BT2200_HD_BO
// 20201204     D.W.           Modify screen to same BT2010
//
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/app/Setcapwidget.c#2 $
// $DateTime: 2017/12/18 16:27:58 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#include "Widgetmgr.h"

void SetCapNormal_SELECT();
void SetCapNormal_UP();
void SetCapNormal_LEFT();
void SetCapNormal_RIGHT();
void SetCapNormal_display();
void SetCapNormal_INIT();
void SetCapNormal_LEFT_SUPER();
void SetCapNormal_RIGHT_SUPER();

widget* SetCapNormal_sub_W[] = {
       &BatTemp_widget, //PointToBAT_widget
};

widget SetCapNormal_widget = {
		.Key_UP = SetCapNormal_UP,
		.Key_DOWN = 0,
		.Key_RIGHT = SetCapNormal_RIGHT,
		.Key_LEFT = SetCapNormal_LEFT,
		.Key_SELECT = SetCapNormal_SELECT,
		.Key_RIGHT_SUPER = SetCapNormal_RIGHT_SUPER,
		.Key_LEFT_SUPER = SetCapNormal_LEFT_SUPER,
		.initial = SetCapNormal_INIT,
		.routine = 0,
		.service = 0,
		.display = SetCapNormal_display,
		.subwidget = SetCapNormal_sub_W,
		.number_of_widget = sizeof(SetCapNormal_sub_W)/4,
		.index = 0,
		.name = "SetCapNormal",
		.no_messgae = 0,
		.Combo = Combo_right | Combo_left,
};

//*****************************************************************************
//
// Declare Set Capacity Max. Value.
//
//*****************************************************************************
static uint16_t g_MaxCCASet = 0;
static uint16_t g_MinCCASet = 0;

uint16_t temp_g_SetCapacityVal;

void SetCapNormal_display(){
    SYS_PAR* par = Get_system_parameter();
    char buf[20];
    uint8_t temp = (par->WHICH_TEST == Start_Stop_Test)? par->SS_test_par.SSSelectRating:par->BAT_test_par.SelectRating;
    ST7565ClearScreen(BUFFER1);
    ST7565Refresh(BUFFER1);
    LCD_String_ext(16,get_Language_String(S_SET_CAPACITY) , Align_Left);

    char rating_display[4+1] = {0};
    uint16_t target_size = strlen(get_Language_String(get_rating_str_idx(temp)));
    if(target_size > sizeof(rating_display)-1){// just in case
        while(1);
    }
    memset(rating_display, ' ', sizeof(rating_display)-1);
    memcpy(rating_display, get_Language_String(get_rating_str_idx(temp)), target_size);

    sprintf( buf, "%d%s", temp_g_SetCapacityVal,rating_display);
    LCD_String_ext(32, buf, Align_Right);
    LCD_Arrow(Arrow_enter | Arrow_RL);// | Arrow_up
}

void SetCapNormal_INIT(){
	TEST_DATA* TD = Get_test_data();
	SYS_PAR* par = Get_system_parameter();
	uint8_t temp = (par->WHICH_TEST == Start_Stop_Test)? par->SS_test_par.SSSelectRating:par->BAT_test_par.SelectRating;
    TD->SetCapacityVal = par->BAT_test_par.BTSetCapacityVal;

	switch(temp){
	case SAE:{
		g_MaxCCASet = MAX_SETCCA_VAL_SAE;
		g_MinCCASet = MIN_SETCCA_VAL_SAE;
		break;
	}
	case EN1:{
		g_MaxCCASet = MAX_SETCCA_VAL_EN;
		g_MinCCASet = MIN_SETCCA_VAL_EN;
		break;
	}
	case EN2:{
		g_MaxCCASet = MAX_SETCCA_VAL_EN2;
		g_MinCCASet = MIN_SETCCA_VAL_EN2;
		break;
	}
	case IEC:{
		g_MaxCCASet = MAX_SETCCA_VAL_IEC;
		g_MinCCASet = MIN_SETCCA_VAL_IEC;
		break;
	}
	case DIN:{
		g_MaxCCASet = MAX_SETCCA_VAL_DIN;
		g_MinCCASet = MIN_SETCCA_VAL_DIN;
		break;
	}
    case MCA:
    case CA_MCA:{
        g_MaxCCASet = MAX_SETCCA_VAL_CA;
        g_MinCCASet = MIN_SETCCA_VAL_CA_MCA;
        break;
    }
	}

	temp_g_SetCapacityVal = TD->SetCapacityVal;
	if(temp_g_SetCapacityVal > g_MaxCCASet){
		temp_g_SetCapacityVal = g_MaxCCASet;
	}
	else if(temp_g_SetCapacityVal < g_MinCCASet){
		temp_g_SetCapacityVal = g_MinCCASet;
	}
	else
	{
		if((temp_g_SetCapacityVal % 5) > 3)
		{
			temp_g_SetCapacityVal = temp_g_SetCapacityVal + (5 - (temp_g_SetCapacityVal % 5));
		}
		else
		{
			temp_g_SetCapacityVal = temp_g_SetCapacityVal - (temp_g_SetCapacityVal % 5);
		}
	}
}


void SetCapNormal_SELECT(){
	SYS_PAR* par = Get_system_parameter();
	TEST_DATA* TD = Get_test_data();

    TD->SetCapacityVal = temp_g_SetCapacityVal;
    par->BAT_test_par.BTSetCapacityVal = TD->SetCapacityVal;
    save_system_parameter(&par->BAT_test_par, sizeof(par->BAT_test_par));
    if(par->WHICH_TEST == Start_Stop_Test){            
        par->SS_test_par.SSSetCapacityVal = TD->SetCapacityVal;
        save_system_parameter(&par->SS_test_par, sizeof(par->SS_test_par));
    }
    
    Move_widget(SetCapNormal_sub_W[0],0);
}

void SetCapNormal_RIGHT(){
    if(temp_g_SetCapacityVal >= g_MaxCCASet)
    {
        temp_g_SetCapacityVal = g_MinCCASet;
    }
    else
    {
        temp_g_SetCapacityVal = temp_g_SetCapacityVal + 5;
        if(temp_g_SetCapacityVal > g_MaxCCASet)
        {
            temp_g_SetCapacityVal = g_MaxCCASet;
        }
    }
    SetCapNormal_display();
}

void SetCapNormal_LEFT(){
    if(temp_g_SetCapacityVal <= g_MinCCASet)
    {
        temp_g_SetCapacityVal = g_MaxCCASet;
    }
    else
    {
        temp_g_SetCapacityVal = temp_g_SetCapacityVal - 5;
        if(temp_g_SetCapacityVal < 25)
        {
            temp_g_SetCapacityVal = 25;
        }
    }
    SetCapNormal_display();
}

void SetCapNormal_LEFT_SUPER(){
    if(temp_g_SetCapacityVal <= g_MinCCASet)
    {
        temp_g_SetCapacityVal = g_MaxCCASet;
    }
    else
    {
        temp_g_SetCapacityVal = temp_g_SetCapacityVal - 10;
        if(temp_g_SetCapacityVal < 25)
        {
            temp_g_SetCapacityVal = 25;
        }
    }
    SetCapNormal_display();
}

void SetCapNormal_RIGHT_SUPER(){
    if(temp_g_SetCapacityVal >= g_MaxCCASet)
    {
        temp_g_SetCapacityVal = g_MinCCASet;
    }
    else
    {
        temp_g_SetCapacityVal = temp_g_SetCapacityVal + 10;
        if(temp_g_SetCapacityVal > g_MaxCCASet)
        {
            temp_g_SetCapacityVal = g_MaxCCASet;
        }
    }
    SetCapNormal_display();
}


void SetCapNormal_UP(){
    SYS_PAR* par = Get_system_parameter();
    TEST_DATA* TD = Get_test_data();

    TD->SetCapacityVal = temp_g_SetCapacityVal;
    par->BAT_test_par.BTSetCapacityVal = TD->SetCapacityVal;
    par->SS_test_par.SSSetCapacityVal = TD->SetCapacityVal;

    Return_pre_widget();
}



