/*
 * Check_battery_temp_widget.c
 *
 *  Created on: 2020年12月23日
 *      Author: ray.yuan
 */

#include "Widgetmgr.h"

void ChkBatTemp_DISPLAY();
void ChkBatTemp_Service();

widget* ChkBatTemp_sub_W[] = {

};

widget ChkBatTemp_widget = {
        .Key_UP = 0,
        .Key_DOWN = 0,
        .Key_RIGHT = 0,
        .Key_LEFT = 0,
        .Key_SELECT = 0,
        .routine = 0,
        .service = ChkBatTemp_Service,
        .initial = 0,
        .display = ChkBatTemp_DISPLAY,
        .subwidget = ChkBatTemp_sub_W,
        .number_of_widget = sizeof(ChkBatTemp_sub_W)/4,
        .index = 0,
        .no_messgae = 1,
        .name = "ChkBatTemp",
};

void ChkBatTemp_DISPLAY(){
    // Clear screen
    ST7565ClearScreen(BUFFER1);
    ST7565Refresh(BUFFER1);
    // Write SCREEN
    LCD_String_ext(16,get_Language_String(S_OVER_RANGE) , Align_Central);
}


void ChkBatTemp_Service(){
    uint8_t block = 1;

       wifi_config(false, NULL);

       while(block){}
}
