//*****************************************************************************
//
// Source File:Restart_To_do_HDFunction_widget.c
// Description: Confirm whether to continue HD FUNCTION after restart
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 8/06/20      David.Wang       Created file.
//
//
// =============================================================================
//
//
// $Header:   $
// $DateTime: $
// $Author:   $
//
//
// Copyright (c) 2020 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#include "Widgetmgr.h"

void Restart_To_do_HDFunction_DISPLAY();
void Restart_To_do_HDFunction_SELECT();
void Restart_To_do_HDFunction_LEFT();
//void Restart_To_do_HDFunction_Init();

widget* Restart_To_do_HDFunction_sub_W[] = {
        &PointToBAT_widget,  //0               // To do hd function battery test directly one by one // 20200824 Measuring_widget > PointToBAT_widget
        &AbortTest_widget,   //1               // GIVE UP HD PACK TEST FLOW
        &CheckClamps_widget, //2               // CHECK CLAMP
        &VOLTAGEHIGH_widget, //3               // VOL HIGH
};

widget Restart_To_do_HDFunction_widget = {
        .Key_UP = 0,
        .Key_DOWN = 0,
        .Key_RIGHT = 0,
        .Key_LEFT = Restart_To_do_HDFunction_LEFT,
        .Key_SELECT = Restart_To_do_HDFunction_SELECT,
        .routine = 0,
        .service = 0,
        .initial = 0,
        .display = Restart_To_do_HDFunction_DISPLAY,
        .subwidget = Restart_To_do_HDFunction_sub_W,
        .number_of_widget = sizeof(Restart_To_do_HDFunction_sub_W)/4,
        .index = 0,
        .no_messgae = 1,
        .name = "Restart_To_do_HDFunction",
};

void Restart_To_do_HDFunction_LEFT(){
    Move_widget(Restart_To_do_HDFunction_sub_W[1],0);     // RETURN TO MAINMENU 20200819 DW EDIT
}


void Restart_To_do_HDFunction_DISPLAY(){

    TEST_DATA* TD = Get_test_data();
    uint8_t Screen_Bat_NUM ;


    Screen_Bat_NUM = TD->PackTestCount % 10 ;

    switch(Screen_Bat_NUM){
    case 1 :
        LCD_display(GR_TEST_BAT1_START, 0);
        break;
    case 2 :
        LCD_display(GR_TEST_BAT2_START, 0);
        break;
    case 3 :
        LCD_display(GR_TEST_BAT3_START, 0);
        break;
    case 4 :
        LCD_display(GR_TEST_BAT4_START, 0);
        break;
    case 5 :
        LCD_display(GR_TEST_BAT5_START, 0);
        break;
    case 6 :
        LCD_display(GR_TEST_BAT6_START, 0);
        break;
    }


}


void Restart_To_do_HDFunction_SELECT(){
    SYS_PAR* par = Get_system_parameter();   // get data init
    TEST_DATA* TD = Get_test_data();         // get data init
    float f_v;
    float external_voltage = 0.0;

    //*************************************
    // CALL RECORED DATA REDISTER
    //*************************************
    uint8_t ui8RecordBuf[128] = {0x00};
    uint16_t ui16TestRecordIdx = 0;
    uint32_t ui32TestRecord = 0;
    uint32_t PackTestRecord = 0;
    SYS_INFO* info = Get_system_info();

    BattCoreScanVoltage(&f_v);               // check vol
    BattCoreScanVoltage(&external_voltage);  // check bat vol

    TD->SetCapacityVal = par->BAT_test_par.BTSetCapacityVal;
    par->WHICH_TEST = Battery_Test;

    if(f_v < check_clamp_v){
        if((Clamp_err & get_system_error()) == 0) set_system_error(Clamp_err);
        Move_widget(Restart_To_do_HDFunction_sub_W[2],0);                      // CHECK CLAMP
        return;
    }

    if(TD->SYSTEM_CHANNEL == MAP_24V){
        Move_widget(Restart_To_do_HDFunction_sub_W[3],0);                      //VOL HIGH
    }
    else{
        //***** Read the last battery data *****//
        //Read test record counter
        ui32TestRecord = info->TotalTestRecord;
        ui16TestRecordIdx = info->TestRecordOffset;
        //Read the desired battery test data
        PackTestRecord = ((ui32TestRecord + ui16TestRecordIdx) % MAX_BTSSTESTCOUNTER) - 1 ;
        TestRecordRead(PackTestRecord, ui8RecordBuf);
        //Get the test result TO print
        BATTERY_SARTSTOP_RECORD_T* BT_rec = (BATTERY_SARTSTOP_RECORD_T*)ui8RecordBuf;
        par->BAT_test_par.SelectRating = BT_rec->Ratting;
        TD->SetCapacityVal = BT_rec->CCA_Capacity;
        //******* END *******//

        Move_widget(Restart_To_do_HDFunction_sub_W[0],0);     // go to test
    }

}
