/*
 * DeviceRegisterManualVKwidget.c
 *
 *  Created on: 2020�~12��23��
 *      Author: henry.yang
 */
#include "Widgetmgr.h"
#define SEL_DEFAULT 1
#define MANUAL_LEN 6

enum{
    Register_ask,
    Register_insert,
    Register_registering,
};


void DeviceRegisterManual_DISPLAY();
void DeviceRegisterManual_UP();
void DeviceRegisterManual_DOWN();
void DeviceRegisterManual_LEFT();
void DeviceRegisterManual_RIGHT();
void DeviceRegisterManual_SELECT();
void DeviceRegisterManual_Routine();
void DeviceRegisterManual_initail();

widget* DeviceRegisterManual_sub_W[] = {
        &PrintResult_widget,
};

char Register_text[MANUAL_LEN+1];

widget DeviceRegisterManual_widget = {
		.Key_UP = DeviceRegisterManual_UP,
		.Key_DOWN = DeviceRegisterManual_DOWN,
		.Key_RIGHT = DeviceRegisterManual_RIGHT,
		.Key_LEFT = DeviceRegisterManual_LEFT,
		.Key_SELECT = DeviceRegisterManual_SELECT,
		.routine = DeviceRegisterManual_Routine,
		.service = 0,
		.initial = DeviceRegisterManual_initail,
		.display = DeviceRegisterManual_DISPLAY,
		.subwidget = DeviceRegisterManual_sub_W,
		.number_of_widget = sizeof(DeviceRegisterManual_sub_W)/4,
		.index = 0,
		.name = "DeviceRegisterManual",
};


static void regist_manual_result(bool result, uint8_t reason){
    SYS_INFO* info = Get_system_info();

    Return_pre_widget();

    if(result){
        UARTprintf("device register success!\n");
        Message_by_language(register_successfully,0,en_US);
        info->device_registered = 1;
        save_system_info(&info->device_registered, sizeof(info->device_registered));
    }
    else{
        UARTprintf("device register fail:%d\n", reason);
        Message_by_language((reason == register_fail_network_err)? network_unstable_fail_to_register:register_fail_code, reason,en_US);
    }

}


void DeviceRegisterManual_cb(char* data,uint8_t len)
{
	SYS_PAR* par = Get_system_parameter();
	if(len!=6){
	    Message_by_language(invalid_code, 0, en_US);
	    Return_pre_widget();
	}
	else{
	    memset(Register_text, 0, sizeof(Register_text));
	    memcpy(Register_text,data,len);
	    AWS_IoT_device_register((uint8_t*)Register_text,regist_manual_result);
	    DeviceRegisterManual_widget.parameter = Register_registering;
	    DeviceRegisterManual_DISPLAY();
	}


}

void DeviceRegisterManual_initail(void)
{
	if(wifi_get_status() == WIFI_off){
		Message_by_language(Turn_on_WIFI,0,en_US);
		Return_pre_widget();
		return;
	}
	else if(wifi_get_status() == WIFI_disconnect){
		Message_by_language(Check_connection,0,en_US);
		Return_pre_widget();
		return;
	}
    else{
        DeviceRegisterManual_widget.parameter = Register_ask;
        DeviceRegisterManual_widget.index = SEL_DEFAULT;
    }
}

void DeviceRegisterManual_DISPLAY(void)
{
    if(DeviceRegisterManual_widget.parameter == Register_ask){
        ST7565ClearScreen(BUFFER1);
        ST7565Refresh(BUFFER1);
        //LCD_String_ext(16, get_Language_String(S_REGISTER_DEVICE), Align_Left);
        LCD_String_ext2(32, get_Language_String((DeviceRegisterManual_widget.index)? S_YES:S_NO), Align_Right, 2);
        LCD_Arrow(Arrow_enter | Arrow_RL);    
    }
    else if(DeviceRegisterManual_widget.parameter == Register_insert){
        VK_char_num_display();
    }
    else{
        ST7565ClearScreen(BUFFER1);
        ST7565Refresh(BUFFER1);
        //LCD_String_ext(16, get_Language_String(S_REGISTERING), Align_Central);
    }
}

void DeviceRegisterManual_Routine(void)
{
    static uint16_t delay = 0;
    if(DeviceRegisterManual_widget.parameter == Register_insert){
        delay = (delay+1)%CURSOR_BLINK_T;
        if(!delay){
            VK_CN_Cursor_blink();
        }
    }
}

void DeviceRegisterManual_UP(void)
{
    if(DeviceRegisterManual_widget.parameter == Register_insert){
        VK_char_num_up();
    }
}

void DeviceRegisterManual_DOWN(void)
{
    if(DeviceRegisterManual_widget.parameter == Register_insert){
        VK_char_num_down();
    }
}

void DeviceRegisterManual_LEFT(void)
{
    if(DeviceRegisterManual_widget.parameter == Register_ask){
        DeviceRegisterManual_widget.index^=1;
        DeviceRegisterManual_DISPLAY();
    }
    else if(DeviceRegisterManual_widget.parameter == Register_insert){
        VK_char_num_left();
    }
}

void DeviceRegisterManual_RIGHT(void)
{
    if(DeviceRegisterManual_widget.parameter == Register_ask){
        DeviceRegisterManual_widget.index^=1;
        DeviceRegisterManual_DISPLAY();
    }
    else if(DeviceRegisterManual_widget.parameter == Register_insert){
        VK_char_num_right();
    }
    
}

void DeviceRegisterManual_SELECT(void)
{
    SYS_PAR* par = Get_system_parameter();
    if(DeviceRegisterManual_widget.parameter == Register_ask){
        if(DeviceRegisterManual_widget.index){
            DeviceRegisterManual_widget.parameter = 1;

            VK_INFO_T info = {//.title_str = get_Language_String(S_DV_REG_MANUAL),
                              .underline = true,
                              .start_x = 0,
                              .start_y = 20,
                              .char_width = 7,
                              .len = sizeof(Register_text)-1,
                              .arr_flag = (Arrow_enter | Arrow_RL), // | ARROW_UP_DOWN
                              .cb = DeviceRegisterManual_cb,};
            VK_char_num_init(&info);
            DeviceRegisterManual_DISPLAY();
        }
        else{
            Return_pre_widget();
        }
    }
    else if(DeviceRegisterManual_widget.parameter == Register_insert){
        VK_char_num_sel();
    }
}



