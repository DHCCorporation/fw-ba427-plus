//*****************************************************************************
//
// Source File: WiFiSetwidget.c
// Description: WiFi scan SSID and connect operation widget.
//
//
// Edit History:
//
// when 		who 		   what, where, why  
// -------- 	----------	   ------------------------------------------------
// 01/10/18 	Henry.Y 	   Create file.
// 02/14/18 	Henry.Y 	   stay for a while when wifi service status change.
// 05/18/18     Jerry.W            add message.
// 09/11/18     Henry.Y        Add FW_check_rslt_handle/send_update_ID_rslt_handle in wifi_service_cb().
//                             Modify display string.
// 10/17/18     Henry.Y        Add TCP connect to check connection status if WIFI poer on and connected.
// 11/19/18     Henry.Y        Enhance wifi connection check and connecting user experience.
// 12/07/18     Henry.Y        New display api.
// =============================================================================
//
//
// $Header$
// $DateTime$
// $Author$
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#include "Widgetmgr.h"
#include "../service/OTA_svc.h"

void OTA_event_cb(OTA_EVENT_T event);
void send_update_ID_rslt_handle(bool result);


typedef enum{
	CHK_STATUS = 0,
	SET_MENU
}WiFi_mode;


WIFI_CONN_T wifi_svc_s;
SOC_STATE_t tcp_svc_s;


uint8_t event_trig = 0;
uint8_t CNT_SSID[SSID_LEN] = {0x00};



void WiFiSet_UP_DOWN();
void WiFiSet_SELECT();
void WiFiSet_Routine();
void WiFiSet_initail();
void WiFiSet_DISPLAY();

void WiFiSet_LEFT();

widget* WiFiSet_sub_W[] = {
		&WiFiScanSSID_widget
};

widget WiFiSet_widget = {
		.Key_UP = WiFiSet_UP_DOWN,
		.Key_DOWN = WiFiSet_UP_DOWN,
		.Key_RIGHT = 0,
		.Key_LEFT = WiFiSet_LEFT,
		.Key_SELECT = WiFiSet_SELECT,
		.routine = WiFiSet_Routine,
		.service = 0,
		.initial = WiFiSet_initail,
		.display = WiFiSet_DISPLAY,
		.subwidget = WiFiSet_sub_W,
		.number_of_widget = sizeof(WiFiSet_sub_W)/4,
		.index = 0,
		.name = "WiFiSet",
};
void try_ev_cb(void* reserve, uint8_t status);

Socket_t try_soc = {
		.port = 80,
		.pair = "www.google.com",
		.evn_cb = try_ev_cb,
		.rcv_cb = 0,
};

void try_ev_cb(void* reserve, uint8_t status){
	SYS_INFO* info = Get_system_info();
	UARTprintf("ev_cb : %d\n", status);
	event_trig = 1;


	if(status == Soc_TCP_connected){
		tcp_svc_s = Soc_TCP_connected;
		TCP_disconnect(&try_soc);

		uint16_t update_count = get_saved_record_count();
#if AWS_IOT_SERVICE
		if(info->device_registered == 0){
			//Message_by_language(unregistered_device_plz_register, 0, en_US);
		}
		else
#endif
			if(update_count){
			Message_by_language(WIFI_connected_find_result, update_count,en_US);
			upload_saved_recod();
		}


		FlashHeader_t* FH = ExtFls_get_header();
		if(FH->update_ID_send_flag == 1){
#if AWS_IOT_SERVICE
			if(memcmp(FH->OTA_FW_status.old_ver, "BT2200", 6) == 0){
				AWS_IoT_send_Upgrade_result(FH->OTA_FW_status.old_ver, strFWver, true, send_update_ID_rslt_handle);
			}
			else{
				//for tha case of EC2 -> IoT
#if defined(__BO__)
				uint8_t* old_version = "BT2200_BO_V00.N";
#else
				uint8_t* old_version = "BT2200_WW_V00.A";
#endif				
				AWS_IoT_send_Upgrade_result(old_version, strFWver, true, send_update_ID_rslt_handle);
			}
#else
			send_Upgrade_ID(&FH->update_ID, info->Time_zone, send_update_ID_rslt_handle);
#endif
		}
		else{
			OTA_Progress_t* OTA_s = OTA_service_get_progress();
			if(OTA_s->status == OTA_status_unchecked){
			OTA_service_init(OTA_event_cb);
		}
			else{
				OTA_service_refresh();
			}
		}

		Time_adjust(info->Time_zone);
	}
	else{
		tcp_svc_s = Soc_TCP_disconnected;
	}
}



void wifi_service_cb(uint32_t type){
	UARTprintf("\n\n######################\n");
	event_trig = 1;
	SYS_INFO* info = Get_system_info();

	switch(type){
	case WIFI_connected                    :
		UARTprintf("WIFI_connected\n");
		// GET AP
		if(!wifi_get_connected_AP(CNT_SSID, SSID_LEN)){
			if(CNT_SSID!=NULL){
				memset(CNT_SSID, 0x00, SSID_LEN);
			}
		}


		if(!TCP_connect(&try_soc,  false )){
			wifi_svc_s = WIFI_disconnect;
			tcp_svc_s = Soc_TCP_disconnected;
		}
		else{
			wifi_svc_s = WIFI_connected;
			tcp_svc_s = Soc_TCP_connecting;
			event_trig = 0;
		}

		break;
	case WIFI_disconnect                   :
		UARTprintf("WIFI_disconnect\n");
		if(CNT_SSID!=NULL){
			memset(CNT_SSID, 0x00, SSID_LEN);
		}
		wifi_svc_s = WIFI_disconnect;
		tcp_svc_s = Soc_TCP_disconnected;
		break;

	case WIFI_off                          :
		UARTprintf("WIFI_off\n");
		break;
	case WIFI_connect_fail                 :
		UARTprintf("WIFI_connect_fail\n");
		if(CNT_SSID!=NULL){
			memset(CNT_SSID, 0x00, SSID_LEN);
		}
		wifi_svc_s = WIFI_disconnect;
		tcp_svc_s = Soc_TCP_disconnected;
		break;
	case WIFI_getting_IP:
		UARTprintf("WIFI_getting_IP\n");
		wifi_svc_s = WIFI_getting_IP;
		tcp_svc_s = Soc_TCP_disconnected;
		break;
	case WIFI_module_fail                  :
		UARTprintf("WIFI_module_fail\n");
		wifi_svc_s = WIFI_disconnect;
		tcp_svc_s = Soc_TCP_disconnected;
		break;
	case WIFI_TX_buffer_over_flow          :
		UARTprintf("WIFI_TX_buffer_over_flow\n");
		wifi_svc_s = WIFI_disconnect;
		tcp_svc_s = Soc_TCP_disconnected;
		break;
	case WIFI_receive_command_error_format :
		UARTprintf("WIFI_receive_command_error_format\n");
		wifi_svc_s = WIFI_disconnect;
		tcp_svc_s = Soc_TCP_disconnected;
		break;
	case WIFI_receive_command_error_type   :
		UARTprintf("WIFI_receive_command_error_type\n");
		wifi_svc_s = WIFI_disconnect;
		tcp_svc_s = Soc_TCP_disconnected;
		break;
	}
	UARTprintf("######################\n\n");
}


void WiFiSet_initail(){
	SYS_INFO* info = Get_system_info();
	WiFiSet_widget.index = 0;
	wifi_svc_s = wifi_get_status();
	WiFiSet_widget.parameter = (info->WIFIPR == 1)? CHK_STATUS:SET_MENU;

	if(wifi_svc_s == WIFI_connected){//try TCP/IP
		if(!TCP_connect(&try_soc,  false )){
			wifi_svc_s = WIFI_disconnect;
			tcp_svc_s = Soc_TCP_disconnected;
		}
		else{
			tcp_svc_s = Soc_TCP_connecting;
			event_trig = 0;
		}
	}
}



void WiFiSet_DISPLAY(){
	tRectangle sTemp;
	uint8_t l,sLen;
	LANGUAGE lang = get_language_setting();
	SYS_INFO* info = Get_system_info();	

	switch(WiFiSet_widget.parameter){
	case CHK_STATUS:

		if(wifi_svc_s == WIFI_connected){
			if(tcp_svc_s == Soc_TCP_connecting){
				LCD_display(WIFI_CHECK_CONNECTION, 0);
			}
			else{
				sTemp = LCD_display((tcp_svc_s == Soc_TCP_connected)? WIFI_CONNECTION_CONNECT:WIFI_CONNECTION_LIMIT, 0);
				sLen = strlen((const char*)CNT_SSID);
				uint8_t START_LOC = (sLen > 20)? 0:60-(sLen*3);
				for(l = 0; l < sLen; l++){
					ST7565Draw6x8Char(START_LOC + l*6, sTemp.i16YMax+3, (l>18 && (sLen > 20))? '.':CNT_SSID[l], Font_System5x8);
				}
			}
		}
		else if(wifi_svc_s == WIFI_getting_IP){
			LCD_display(WIFI_CHECK_CONNECTION, 0);
		}
		else{// if(wifi_svc_s == WIFI_disconnect)
			LCD_display(WIFI_CONNECTION_DISCONNECT, 0);
		}
		break;
	case SET_MENU:

		//*********************
		//
		//	WIFI SET LIST
		//
		//*********************
		sTemp = LCD_display(WIFI_SETTING, WiFiSet_widget.index);
		if(info->WIFIPR){
			if(wifi_svc_s == WIFI_connected){
				if(tcp_svc_s == Soc_TCP_connecting){
					LCD_draw_string(WIFI_CONNECTING, 0, sTemp.i16YMax+4);
				}
				else{
					sTemp = LCD_draw_string((tcp_svc_s == Soc_TCP_connected)? WIFI_CONNECTION_CONNECT:WIFI_CONNECTION_LIMIT, 0, sTemp.i16YMax+3);
					//
					// SHOW CONNECTED SSID
					//
					SHOW_profile_t profile = {side_left, 2, false};
					char buf[20] = {0};
					snprintf(buf,sizeof(buf),"WIFI: %s",(const char*)CNT_SSID);

					UTF8_LCM_show(sTemp.i16XMin,sTemp.i16YMax+2, buf, (127-sTemp.i16XMin), &profile);
					/*
					UTF8_LCM_show(sTemp.i16XMin,sTemp.i16YMax+4, "WIFI:", (127-sTemp.i16XMin), &profile);
					sLen = strlen((const char*)CNT_SSID);
					for(l = 0; l < sLen; l++){
						if(l == 15) break;
						ST7565Draw6x8Char((profile.region.x2+2)+l*6, profile.region.y1, (l>12 && sLen > 15)? '.':CNT_SSID[l], Font_System5x8);
					}*/
				}
			}
			else if(wifi_svc_s == WIFI_getting_IP){
				LCD_draw_string(WIFI_CONNECTING, 0, sTemp.i16YMax+4);
			}
			else{
				LCD_draw_string(WIFI_CONNECTION_DISCONNECT, 0, sTemp.i16YMax+4);
			}

		}
		break;
	default:
		break;
	}
}



void WiFiSet_SELECT(){
	SYS_INFO* info = Get_system_info();

	switch(WiFiSet_widget.parameter){
	case CHK_STATUS:
		if(wifi_svc_s != WIFI_getting_IP){
			WiFiSet_widget.parameter = SET_MENU;
		}
		else return;
		break;
	case SET_MENU:
		if(WiFiSet_widget.index == 0){ //WIFI POWER
			info->WIFIPR ^= 0x01;
			wifi_config(info->WIFIPR, wifi_service_cb);
			wifi_svc_s = WIFI_getting_IP;
			tcp_svc_s = Soc_TCP_disconnected;
		}
		else{//WIFI LIST
			Move_widget(WiFiSet_sub_W[0],0);
			return;
		}
		break;
	default:	return;
	}
	WiFiSet_DISPLAY();
}

void WiFiSet_LEFT(){
	SYS_INFO* info = Get_system_info();

	switch(WiFiSet_widget.parameter){
	case CHK_STATUS:
		if(wifi_svc_s == WIFI_connected && tcp_svc_s == Soc_TCP_connected){
			save_system_info(&info->WIFIPR, sizeof(info->WIFIPR));
			Return_pre_widget();
		}
		else if(wifi_svc_s == WIFI_getting_IP){
			WiFiSet_widget.parameter = SET_MENU;
			WiFiSet_DISPLAY();		
		}
		break;
	case SET_MENU:
		save_system_info(&info->WIFIPR, sizeof(info->WIFIPR));
		Return_pre_widget();
		break;
	default:
		break;
	}
}





void WiFiSet_UP_DOWN(){
	SYS_INFO* info = Get_system_info();
	switch(WiFiSet_widget.parameter){
	case SET_MENU:	
		if(info->WIFIPR){
			WiFiSet_widget.index ^= 1;
			WiFiSet_DISPLAY();
		}
		break;
	default:
		break;
	}
}



void WiFiSet_Routine(){
	SYS_INFO* info = Get_system_info();

	switch(WiFiSet_widget.parameter){
	case CHK_STATUS:
		if(event_trig){
			wifi_svc_s = wifi_get_status();
			event_trig = 0;
			WiFiSet_DISPLAY();
		}


		break;
	case SET_MENU:
		if(event_trig){
			event_trig = 0;		

			WiFiSet_DISPLAY();		
		}
		break;
	default:
		break;
	}

}




