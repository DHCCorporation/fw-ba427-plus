//*****************************************************************************
//
// Source File: VMAMwidget.c
// Description: Prototype for functions of Memory Erase Widget. Including Click
//				(Push to new menu entry to stack)/Right/Left(Pop out stack)/Up/
//				Down function body.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 01/11/16     Vincent.T      Created file.
// 01/12/16     Vincent.T      Add AM function
// 03/10/16     Vincent.T      1. Slow donw the freq. to refesh display.
// 03/23/16     Vincent.T      1. Modify some AM vol to current conversion.
// 03/25/16     Vincent.T      1. Modify VMAMLeft(); to handle multination language case.
// 06/14/16     Vincent.T      1. #include "driverlib/rom.h"
//                             2. #include "../driver\buttons.h"
//                             3. New global variable VMAMScrnFreshCounter(for fresh VMAM screen)
//                             4. Modify VMAMLeft();(when left btn event occurs, reset VMAMScrnFreshCounter)
//                             5. Modify VMAMshow();(to collect more samples to present precise resut)
// 08/10/16     Vincent.T      1. When VM or AM over range, display --.--
// 09/21/17 	Jerry.W		   Rewrite Widget for new software architecture
// 10/23/18     Henry.Y        Decrease AM measuring times.
// 12/07/18     Henry.Y        New display api.
// 03/29/19     Henry.Y        Fix the bug: -100.00A and trans to 100.00A will cause an extra 'A' on the screen.
// 08/03/20     David.Wang     VM/AM meter add reminder on/off screen
//
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/app/VMAMwidget.c#6 $
// $DateTime: 2017/09/22 11:22:59 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#include "Widgetmgr.h"





void VMAM_LEFT();
void VMAM_Routine();
void VMAM_display();
void VMAM_show();
void VMAM_SELECT();           //20200803 DW EDIT

widget* VMAM_sub_W[] = {
};

widget VMAM_widget = {
		.Key_UP = 0,
		.Key_DOWN = 0,
		.Key_RIGHT = 0,
		.Key_LEFT = VMAM_LEFT,
		.Key_SELECT = VMAM_SELECT,                  //20200803 DW EDIT
		.routine = VMAM_Routine,
		.service = 0,
		.display = VMAM_display,
		.subwidget = VMAM_sub_W,
		.number_of_widget = sizeof(VMAM_sub_W)/4,
		.index = 0,
		.no_messgae = 1,
		.name = "VMAM",
};

void VMAM_display(){
	
    if(VMAM_widget.parameter == 0){
        LCD_display(AMVM_TURN_ON,0);                 // 20200803 DW EDIT
        LCD_draw_string(AMVM_TURN_ON_AMP, 0, 15);    // 20200803 DW EDIT
        LCD_draw_string(AMVM_AFTER_ATT, 0, 25);      // 20200803 DW EDIT
        LCD_draw_string(AMVM_ENTER_PROCEED, 0, 35);  // 20200803 DW EDIT
    }
    else if(VMAM_widget.parameter == 1){
        LCD_display(VM_AM,0);
	    VMAM_show();
    }
    else{ //(VMAM_widget.parameter == 2)
        LCD_display(AMVM_TURN_ON,0);                 // 20200803 DW EDIT
        LCD_draw_string(AMVM_TURN_OFF_AMP, 0, 15);    // 20200803 DW EDIT
        LCD_draw_string(AMVM_AMP_METER, 0, 25);      // 20200803 DW EDIT
        LCD_draw_string(AMVM_ENTER_PROCEED, 0, 35);  // 20200803 DW EDIT
    }

}

void VMAM_SELECT()     // 20200803 DW ADD
{
    if(VMAM_widget.parameter == 0){
        VMAM_widget.parameter = 1;
        VMAM_display();
    }
    else if(VMAM_widget.parameter == 1){
        VMAM_widget.parameter = 2;
        VMAM_display();
    }
    else {//(VMAM_widget.parameter == 2){
        VMAM_widget.parameter = 0;
        VMAM_widget.index = 0;
        Return_pre_widget();
    }
}

void VMAM_LEFT()
{
	VMAM_widget.index = 0;

	Return_pre_widget();

}

void VMAM_show(){
	//Handle voltage meter value
	//Handle amper meter value
	float fVMAM = 0,fVMAMacc = 0;
	char ui8DatBuf[10];
	uint8_t ui8LopIdx = 0;
	uint32_t ADC;
	snprintf(ui8DatBuf, sizeof(ui8DatBuf), "         ");//9 space+end char
	ST7565DrawStringx16(48, 16, (uint8_t*)ui8DatBuf, Font_System5x8);
	
	//voltage meter	
	GetVM(&fVMAM,&ADC);
	
	//
	// Reset
	//
	if(fVMAM < 0.50) fVMAM = 0;
	
	//
	//VM > 40V?
	//
	if(fVMAM > 40.00){
		snprintf(ui8DatBuf, sizeof(ui8DatBuf), "--.--V");
	}
	else{			
		snprintf(ui8DatBuf, sizeof(ui8DatBuf), "%5.2fV",fVMAM);
	}
	ST7565DrawStringx16(48, 16, (uint8_t*)ui8DatBuf, Font_System5x8);
	
	
	//amper meter
	//
	// Sampling 100 times for stablizing AM sensor
	//
	for(ui8LopIdx = 0; ui8LopIdx < 100; ui8LopIdx++){		
		GetAM(&fVMAM,&ADC);
	
		fVMAMacc = fVMAMacc + fVMAM;
		if(LeftBtn(3))	return;
	}
	
	fVMAM = fVMAMacc/100;
	
	//
	// Reset
	//
	snprintf(ui8DatBuf, sizeof(ui8DatBuf), "         ");//9 space+end char
	ST7565DrawStringx16(42, 44, (uint8_t*)ui8DatBuf, Font_System5x8);
	if(fabs(fVMAM) < 0.9) fVMAM = 0;
	
	if(fabs(fVMAM) > 600.00){
		snprintf(ui8DatBuf, sizeof(ui8DatBuf), "---.--A");
	}
	else{			
		snprintf(ui8DatBuf, sizeof(ui8DatBuf), "%6.2fA",fVMAM);
	}
	ST7565DrawStringx16(42, 44, (uint8_t*)ui8DatBuf, Font_System5x8);
}

void VMAM_Routine(void)
{
	VMAM_widget.index = (VMAM_widget.index+1) %200;

	if(VMAM_widget.parameter == 1){
	    if(VMAM_widget.index == 0){
	        VMAM_show();
	    }
	}

}



