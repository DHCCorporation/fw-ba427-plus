//*****************************************************************************
//
// Source File: AltIdleVoltWidget.c
// Description: Alt. Idle Votlage test widget including Click(Push to new menu entry to stack)/
//				Right/Left(Pop out stack)/Up/Down function body.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 02/02/16     Vincent.T      Created file.
// 09/21/17 	Jerry.W		   Rewrite Widget for new software architecture
// 05/18/18     Jerry.W        Add message in widget structure.
// 09/11/18     Henry.Y        Add AltIdleVolt_service/AltIdleVolt_LEFT.
// 12/07/18     Henry.Y        New display api.
// 03/20/19     Henry.Y        Rounddown test voltage to the second decimal place.
// 08/03/20     David.Wang     Some pictures of sys test changed to text description
//
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/app/AltIdleVoltwidget.c#7 $
// $DateTime: 2017/09/22 11:15:59 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#include "Widgetmgr.h"


widget* AltIdleVolt_sub_W[] = {
		&TurnOnLoads_widget,
		&AbortTest_widget
};

void AltIdleVolt_display();
void AltIdleVolt_service();
void AltIdleVolt_SELECT();

widget AltIdleVolt_widget = {
		.Key_UP = 0,
		.Key_DOWN = 0,
		.Key_RIGHT = 0,
		.Key_LEFT = 0,
		.Key_SELECT = AltIdleVolt_SELECT,
		.routine = 0,
		.service = AltIdleVolt_service,
		.display = AltIdleVolt_display,
		.subwidget = AltIdleVolt_sub_W,
		.number_of_widget = sizeof(AltIdleVolt_sub_W)/4,
		.index = 0,
		.no_messgae = 1,
		.name = "AltIdleVolt",
};


void AltIdleVolt_SELECT(){
	if(AltIdleVolt_widget.parameter == 0){
	    AltIdleVolt_widget.parameter = 1;
	    AltIdleVolt_display();
	}
	else if(AltIdleVolt_widget.parameter == 1){
	    AltIdleVolt_widget.parameter = 2;
	    AltIdleVolt_service();
	}
	else if(AltIdleVolt_widget.parameter == 2){
	    Move_widget(AltIdleVolt_sub_W[0], 0);
	}
}


void AltIdleVolt_display(){	
    TEST_DATA* TD = Get_test_data();
    ST7565ClearScreen(BUFFER1);
    ST7565Refresh(BUFFER1);
	if(AltIdleVolt_widget.parameter == 0){
	    LCD_String_ext(16,get_Language_String(S_PRESS_ENTER_FOR) , Align_Central);
	    LCD_String_ext(32,get_Language_String(S_CHARGING_TEST) , Align_Central);
	}
	else if(AltIdleVolt_widget.parameter == 1){
	    LCD_String_ext(16,get_Language_String(S_MAKE_SURE_ALL) , Align_Central);
	    LCD_String_ext(32,get_Language_String(S_LOADS_ARE_OFF) , Align_Central);
	}
	else if(AltIdleVolt_widget.parameter == 2){
	    LCD_String_ext(16,get_Language_String(S_ALT_IDLE_VOLTS) , Align_Left);
	    char buf[10];
	    uint16_t result[] = {S_LOW, S_NORMAL, S_HIGH};
	    sprintf(buf, "%.2fV", TD->IdleVol );
	    LCD_String_ext2(32,buf , Align_Left, 2);
	    LCD_String_ext2(48,get_Language_String(result[TD->AltIdleVResult-LOW]) , Align_Right, 2);
	}
	LCD_Arrow(Arrow_enter);
}


void AltIdleVolt_service(){	
	if(AltIdleVolt_widget.parameter == 2){
		float fval;
		TEST_DATA* TD = Get_test_data();
		TD->AltIdleVResult = AltIdleVoltTest(&fval);
		TD->IdleVol = floating_point_rounding(fval, 2, ffloor);
		AltIdleVolt_widget.parameter = 2;
		AltIdleVolt_display();
	}
}



