//*****************************************************************************
//
// Source File: Is24vSystemCheck_widget.c
// Description: Confirm whether it is a 24v system before battery test
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 8/06/20      David.Wang     Created file.
// 09/06/20                    BUG ID 16666 In show " TEST EACH BATTERY SEPARATELY" screen can not to do enter select
//
// =============================================================================
//
//
// $Header:   $
// $DateTime: $
// $Author:   $
//
//
// Copyright (c) 2020 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#include "Widgetmgr.h"

void Is24vSystemCheck_DISPLAY();
void Is24vSystemCheck_UP_DOWN();
void Is24vSystemCheck_SELECT();
void Is24vSystemCheck_LEFT();
void Is24vSystemCheck_Init();

widget* Is24vSystemCheck_sub_W[] = {
        &VOLTAGEHIGH_widget,//0
};

widget Is24vSystemCheck_widget = {
        .Key_UP = Is24vSystemCheck_UP_DOWN,
        .Key_DOWN = Is24vSystemCheck_UP_DOWN,
        .Key_RIGHT = 0,
        .Key_LEFT = Is24vSystemCheck_LEFT,
        .Key_SELECT = Is24vSystemCheck_SELECT,
        .routine = 0,
        .service = 0,
        .initial = Is24vSystemCheck_Init,
        .display = Is24vSystemCheck_DISPLAY,
        .subwidget = Is24vSystemCheck_sub_W,
        .number_of_widget = sizeof(Is24vSystemCheck_sub_W)/4,
        .index = 0,
        .no_messgae = 1,
        .name = "Is24vSystemCheck",
};

void Is24vSystemCheck_Init(){
    Is24vSystemCheck_widget.index = 0;
    Is24vSystemCheck_widget.number_of_widget = 0;
}

void Is24vSystemCheck_LEFT(){
    Widget_init(manual_test); // RETURN TO MainMenu
}


void Is24vSystemCheck_DISPLAY(){
    if( !Is24vSystemCheck_widget.index ){
        LCD_display(IS_24V_SYSTEM, Is24vSystemCheck_widget.parameter^0x01);
    }
    else{
        LCD_display(TEST_EACH_BAT_SEP, 0);
    }
}

void Is24vSystemCheck_UP_DOWN(){
    Is24vSystemCheck_widget.parameter ^= 0x01;
    Is24vSystemCheck_DISPLAY();
}


void Is24vSystemCheck_SELECT(){

    if(Is24vSystemCheck_widget.number_of_widget < 1 ){

        if( Is24vSystemCheck_widget.parameter ){
            Is24vSystemCheck_widget.index = 1;
            Is24vSystemCheck_widget.number_of_widget = 1;
            Is24vSystemCheck_DISPLAY();
        }
        else{
            Move_widget(Is24vSystemCheck_sub_W[0],0);
        }
    }
}







