//*****************************************************************************
//
// Source File: Widgetmgr.h
// Description: Prototype for functions to construct Widget Manager to build 
//				a complex user interface that includes individual controls such
//				as buttons, sliders...etc. This layer ties the graphical display
//				to the input system. It manages the input and updates the displayed
//				widgets according to presses made by end-user.
//
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 09/21/15     William.H      Created file.
// 10/01/15     William.H      To integrate all the BUTTON_PRESSED event: SELECT_BUTTON/UP_BUTTON/DOWN_BUTTON/LEFT_BUTTON/RIGHT_BUTTON, 
//                             and respective control function into Widget Manager.c file.
// 10/05/15     William.H      Check in BT2020 project which branched from LCDFontMaker(Pre-BT2020) by modifed related GPIOs.
// 10/08/15     William.H      Implement LCD Backlight Setting Menu and add the menu entry into Setting Menu widget.
// 10/19/15     William.H      Add new entry of "CHKSURFACECHARGE" / "COUNTDOWN15SEC" / "TESTINVEHICLE" of Battery Test item.
// 10/21/15     Henry.Y        Add new entry of "VOLTAGEHIGH" of Battery Test item.
// 10/22/15     Henry.Y        Add new entry of "ISA6VBATTERY" of Battery Test item.
// 10/30/15     William.H      To fix the Internal Battery Low suddenly been presented scenario, then clear menu stack and back to Main Menu immediately will cause state currupt issue. Add SystemErrHandler() function to handle/fix this issue.
// 08/12/15     Vincent.T      Add new entry of "DATETIMESET" of Battery Test item.
// 01/04/16     Vincent.T      Add new entry of "TESTCOUNTER"
// 01/06/16     Vincent.T      Add new entry of "MEMORYERASE" of setting item
// 01/11/16     Vincent.T      Add new entry of "VMAM"
// 01/19/16     William.H      Add BluetoothSetWidgetMsgProc() func which will be called when 'BLUETOOTHSET' menu entry exist in stack.
// 02/15/16     William.H      1. Add RONUMBERVKYBD / VINNUMBERVKYBD enum to process RO and VIN characters input.
//                             2. Add RoVinVkybdWidgetMsgProc() to deal with ket events.
// 02/02/16     Vincent.T      1. Add new entry of "CRANKVDETEST" of system test item.
//                             2. Add CrankingVoltWidgetMsgProc() func to handle button event when testing cranking voltage.
//                             3. Add new entry of "ALTIDLEVOL" of system test item.
// 02/03/16     Vincent.T      1. Add AltIdleVoltWidgetMsgProc fun to handle button event when testing idle voltage.
//                             2. Add new entry of "ALTIDLEVOLRESULT" of system test item.
//                             3. Add AltIdleVoltResultWidgetMsgProc() func. to handle button event when testing idle voltage.
// 02/04/16     Vincent.T      1. Add RipVoltResultWidgetMsgProc() func. to handle button event when testing ripple voltage.
//                             2. Add TurnOnLoadsWidgetMsgProc() func. to handle button event when showing "TURN ON LOADS".
//                             3. Add new entry of "CHECKRIPPLE15SEC" of system test item.
//                             4. Add new entry of "RIPPLE15SECRESULT" of system test item.
//                             5. Add new entry of "TURNONLOADS" of system test item.
//                             6. Add new entry of "ALTLOADVTEST" of system test item.
//                             7. Add new entry of "ALTLOADVTESTRESULT" of system test item.
//                             8. Add AltLoadVoltWidgetMsgProc() func. to to handle button event when testing load voltage.
// 02/05/16    Vincent.T       1. Add new entry of "SYSRESULT" of system test item
//                             2. Add AltLoadVoltResultWidgetMsgProc() func. to handle button event when showing Alt. Load test result.
//                             3. Add SysResultWidgetMsgProc() func. to handel button when showing System result.
// 03/09/16    Vincent.T       1. Add new entry of "NOPAPER" of error item.
//                             2. Add NoPaperWidgetMsgProc()  func. to handle button event when printing.
// 03/30/16    Vincent.T       1. Delete TRANSTODISPLAY move handling process to DisplayResultWidgetMsgProc();
//                             2. extern SelRatingStrOnDisplay(); to draw cca rating on LCD.
// 04/11/16    Vincent.T       1. New IRTESTCHECK, IRTEST for IR test routine.
// 04/12/16    Vincent.T       1. New IRTESTRESULT of IR test routine.
//                             2. Add IRTestResultWidgetMsgProc(); to handle IR test result.
// 04/14/16    Vincent.T       1. Add IRRechargeRetestWidgetMsgProc(); to handle IR Recharge & Retest case.
//                             2. Add new entry "IRRECHARGERETEST".
//                             3. Add LoadErrorWidgetMsgProc(); to handle load error case.
//                             4. Add new entry "LOADERROR".
// 04/25/16    Vincent.T       1. extern void JudgetResltStrOnDisplay(uint8_t ui8JudgeResult);
// 06/21/16    Vincent.T       1. Add new entry "POINTTOBAT".
// 01/13/17    Vincent.T       1. Modify some func. declaraction.
// 09/21/17 	Jerry.W		   Rewrite Widget for new software architecture
// 09/26/17		Jerry.W		   1. remove unused variable
//							   2. add Widget_routine declaration
// 11/06/17     Henry.Y	       1. Add execption handling function LoadErrorHandler()
// 11/14/17     Jerry.W		   1. Add new Widget declaration
// 11/22/17     Henry.Y        1. Modify max value setting of each rating
//                             2. Change declaration type of Measured_RC.
//                             3. Add global variable: RCJudge_Result, g_TestInVehicle, g_ui8Inventory.
// 02/14/18     Henry.Y	       JIS related variables
// 03/22/18     Henry.Y	       add extern widget CheckVIN_widget;
// 03/22/18     William.H      Add g_SelClampConnMeth & g_SelCarModel global varaibles, SelClampConnMeth_widget & SelCarModel_widget widgets.
// 05/18/18     Jerry.W        add message functionality.
// 09/03/18     Henry.Y        global variable g_SelClampConnMeth merged with TEST_DATA*.
// 09/11/18     Henry.Y        Add Main_routine_switch/get_routine_switch. Remove useless global variables.
// 11/19/18     Henry.Y        include header: UTF8_WBsvc. Change tcp_svc_s type. Remove some scanner global variables.
// 08/06/20     David.Wang     Add start ask voltage high check screen
// 08/26/20     David.Wang     Add advised action widget
//
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/app/Widgetmgr.h#1 $
// $DateTime: 2017/11/27 10:38:22 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#ifndef __WIDGETMGR_H__
#define __WIDGETMGR_H__



#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include "Message_set_app.h"

#include "../service/btsvc.h"
#include "../service/flashsvc.h"
#include "../service/lcdsvc.h"
#include "../service/usbsvc.h"
#include "../service/battcoresvc.h"
#include "../service/exteprsvc.h"
#include "../service/mapsvc.h"
#include "../service/printersvc.h"
#include "../service/wordbanksvc.h"
#include "../service/testrecordsvc.h"
#include "../service/extrtcsvc.h"
#include "../service/tempsvc.h"
#include "../service/vmsvc.h"
#include "../service/engmodesvc.h"
#include "../service/amsvc.h"
#include "../service/dlinklistsvc.h"
#include "../service/scanner_svc.h"
#include "../service/WIFI_service.h"
#include "../service/cloud.h"
#include "../service/SystemInfosvs.h"
#include "../service/Socket_svc.h"
#include "../service/HTTP_service.h"
#include "../service/AWS_IoT_svc.h"
#include "../service/UTF8_WBsvc.h"
#include "../service/langaugesvc.h"
#include "../service/OTA_svc.h"
#include "../service/WordString.h" //20200812 dw dit

#include "../driver/led_iocfg.h"
#include "../driver/battcore_iocfg.h"
#include "../driver/ssi_master.h"
#include "../driver/st7565_lcm.h"
#include "../driver/a25L032_fls.h"
#include "../driver/usb_serial_structs.h"
#include "../driver/uart_iocfg.h"
#include "../driver/buttons.h"
#include "../driver/pwm.h"
#include "../driver/25lc512_epr.h"
#include "../driver/mp205_prt.h"
#include "../driver/s35190a_clk.h"
#include "../driver/vm.h"
#include "../driver/am.h"
#include "../driver/btuart_iocfg.h"
#include "../driver/scanner_driver.h"
#include "../driver/smallfonts.h"
#include "../driver/wifi_driver.h"
#include "../driver/mem_protect.h"
#include "../driver/soft_timer.h"
#include "../driver/delaydrv.h"

#include "../driver/uartstdio.h"
#include "driverlib/timer.h"

#include "BT2200_DISPLAY_SERVICE.h"
#include "VKmgr.h"
#define IR_USER_MODE 99.99
#define IR_EE_MODE 999.99

#define TEMP_LOW -20
#define TEMP_HIGH 60
#define PRINT_OPTION_SPACE 16


#define WIDGET_MSG_KEY_UP		0x0001
#define WIDGET_MSG_KEY_DOWN		0x0002
#define WIDGET_MSG_KEY_LEFT		0x0003
#define WIDGET_MSG_KEY_RIGHT	0x0004
#define WIDGET_MSG_KEY_SELECT	0x0005


#define MESSAGE_LENGTH 100
#define MESSAGE_TOTAL_LENGTH 300

//void SystemErrHandler(void);

//*****************************************************************************
//
//  Load on circuit protection function.
//  Focus on the out-of-control error of testing
//
//*****************************************************************************
void LoadErrorHandler(void);

// Test code

#if 1
// BT2010_2200 version
// BA427+ version
// SHEET INFORMATION:
// PW FOR FRAME PROTECTION: 111
// PW FOR SHEET PROTECTION: 222
//#define TESTCODE_LEN 16
typedef struct{
    float fvol;
    uint8_t ui8TestResult;
    uint8_t ui8CCARating;
    uint16_t ui16SetCCA;
    uint16_t ui16TestCCA;
    float fTemp;
    uint16_t ui16Year;
    uint8_t ui8Month;
    uint8_t ui8Day;
    uint8_t hour;
    uint8_t minute;
}TESTCODE_T;
#else
// BT2200 version
// SHEET INFORMATION:
// PW FOR FRAME PROTECTION: 2222
// PW FOR SHEET PROTECTION: 1111
//#define TESTCODE_LEN 12
typedef struct{
    float fvol;
    uint8_t ui8TestResult;
    uint8_t ui8CCARating;
    uint16_t ui16SetCCA;
    uint16_t ui16TestCCA;
    float fTemp;
    uint16_t ui16Year;
    uint8_t ui8Month;
    uint8_t ui8Day;
}TESTCODE_T;
#endif

void TestCodeGenerater(uint8_t *pui8TestCode, uint8_t len, TESTCODE_T* data);
uint8_t ASCII2TestCode(uint8_t ui8Num);


// FOR defined enum and string mapping
typedef struct{
    uint32_t type;
    uint32_t text_index;    
}TYPE_TEXT_T;


/**** variable defined in main.c *****/
extern int16_t g_15SecCDCounter;
extern bool g_bENTERON;


//extern uint8_t *strFWver;

extern Timer_t* loc_timer;
extern Timer_t* button_timer;
extern volatile uint8_t counter;
extern volatile uint8_t button_counter;
extern volatile bool dummy_system_off;


/**** variable defined in app. layer ****/

extern const tJISBatteryType *pg_JIS;
extern const tJISBatteryType g_JISRgBatType[];	//Battery Test
extern const tJISBatteryType g_JIS_SS_AGM[];
extern const tJISBatteryType g_JIS_SS_EFB[];
extern uint16_t NUM_JIS_BATTERY_TYPE;
extern const uint16_t NUM_JIS_BT;
extern const uint16_t NUM_JIS_SS_EFB;
extern const uint16_t NUM_JIS_SS_AGM;
extern bool blClrCCAVal;
extern uint8_t g_CurtSeltdLang;
extern bool g_BluetoothEnabled;
extern uint8_t g_RoVKybdCharPos;
extern uint8_t CNT_SSID[SSID_LEN];
extern WIFI_CONN_T wifi_svc_s;
extern SOC_STATE_t tcp_svc_s;
//extern uint8_t SC_alloc_Len;
extern uint8_t code_recv_len;
//extern uint8_t SCANNER_RECEIVED[SCANNER_RX_BUFFER_SIZE];
extern bool up;
extern bool down;
extern bool right;
extern bool left;
extern bool enter;
extern bool test[];
extern uint8_t event_trig;
extern bool bSCANNERecompl;
extern tContext g_sContext;
extern bool g_bCranking_timeout;

//extern const tBrand brand_list[];



/**** variable defined in service layer *****/



//======================================================================================================//
//											Widget Data structure										//
//======================================================================================================//

typedef struct widget_t{
	void (*Key_UP)(void);
	void (*Key_DOWN)(void);
	void (*Key_RIGHT)(void);
	void (*Key_LEFT)(void);
    void (*Key_RIGHT_SUPER)(void);
    void (*Key_LEFT_SUPER)(void);
	void (*Key_SELECT)(void);
	void (*Key_SELECT_SUPER)(void);
	void (*routine)(void);
	void (*service)(void);
	void (*display)(void);
	void (*initial)(void);

	struct widget_t** subwidget;
	uint8_t *name;
	uint8_t number_of_widget;
	uint8_t index;
	uint8_t parameter;
	uint8_t no_messgae;
	uint8_t tra_index;
	uint8_t Combo;
}widget;

typedef enum{
    Combo_right = 0x01,
    Combo_left = 0x02,
    Combo_Up = 0x04,
    Combo_down = 0x08,
    Combo_enter = 0x10,
}Button_Combo;

typedef enum{
	widget_normal,
	widget_message_wait,
	widget_message_show,
}WIDGET_STATE;

typedef enum{
	message_up = 1,
	message_down = 2,
}MESSAGE_ROLL;

//======================================================================================================//
//											Widget management API										//
//======================================================================================================//
void Move_widget(widget* new_w,uint8_t parameter);
void Move_widget_untracked(widget* new_w,uint8_t parameter);
void Return_pre_widget();
void Widget_init(uint8_t mode);
void Widget_routine();
void Main_routine_switch(uint8_t routine_switch);
uint8_t get_routine_switch();
void Widget_scan();


void set_countdown_callback(void (*sec_callback)(void),int16_t sec);

void keybaord(void);

WIDGET_STATE get_widget_mode();
void set_widget_mode(WIDGET_STATE state);
void message(const char *mesag);
void message_display(uint8_t start_line);
void message_roll(MESSAGE_ROLL up_down);	//MESSAGE_ROLL
void message_routine();

void upload_saved_recod();
// Defined in WiFiSet_widget.c
void wifi_service_cb(uint32_t type);
uint8_t check_vin(uint8_t* buf,uint8_t length);//0: fault, 1: ok

//======================================================================================================//
//											Widget declarations											//
//======================================================================================================//
extern widget MAINMENU_widget;
extern widget VOLTAGEHIGH_widget;
extern widget BatterySel_widget;
extern widget IsIta6VBattery_widget;
extern widget Selrating_widget;
extern widget SetCapNormal_widget;
extern widget CheckRipple_widget;
extern widget TestInVehicle_widget;
extern widget PointToBAT_widget;
extern widget Measuring_widget;
extern widget DisplayResult_widget;
extern widget IsBatteryCharged_widget;
extern widget NumberVKBd_widget;
extern widget PrintResult_widget;
extern widget NoPaper_widget;
extern widget ChkIntBat_widget;
extern widget JISCapSel_widget;
extern widget CrankingVolt_widget;
extern widget AltIdleVoltResult_widget;
extern widget AltIdleVolt_widget;
extern widget TurnOnLoads_widget;
extern widget Ripple15Sec_widget;
extern widget ALTLoadVol_widget;
extern widget SysResult_widget;
extern widget LoadError_widget;
extern widget VMAM_widget;
extern widget SettingMenu_widget;
extern widget BackLightSet_widget;
extern widget LangSel_widget;
extern widget DateTimeSet_widget;
extern widget MemErase_widget;
extern widget InVKbd_widget;
extern widget TestCounter_widget;
extern widget TestCounterConfirm_widget;
extern widget CheckClamps_widget;
extern widget Bluetooth_widget;
extern widget SetRCNormal_widget;
extern widget TestCode_widget;
extern widget PowerLow_widget;
extern widget BluetoothPreinter_widget;
extern widget Scanner_widget;
extern widget WiFiSet_widget;
extern widget WiFiScanSSID_widget;
extern widget engmode_widget;
extern widget CheckVIN_widget;
extern widget TimeZone_widget;

extern widget SelClampConnMeth_widget;
extern widget SelCarModel_widget;
extern widget OTA_widget;

extern widget AbortTest_widget;
extern widget ERRORHandle_widget;

extern widget USB_widget;
extern widget MFC_widget;

extern widget devicereg_widget;

extern widget Is24vSystemCheck_widget;          // 20200806 DW EDIT
extern widget Diesel40Sec_widget;               // 20200806 DW EDIT
extern widget HdFunctionAsk_widget;             // 20200812 DW EDIT
extern widget HdFunctionSelect_widget;          // 20200813 DW EDIT
extern widget Check_Pack_widget;                // 20200817 DW EDIT
extern widget Restart_To_do_HDFunction_widget;  // 20200818 DW EDIT
extern widget Display_PackTestAllResult_widget; // 20200819 DW EDIT
extern widget Pack_Test_Advised_widget;         // 20200826 DW EDIT  Add advised action widget
extern widget Test_Code_widget;                  // 20201203 DW EDIT  for BT2010_BT2200
extern widget InsertPaper_widget;
extern widget ResetCounter_widget;
extern widget BatTemp_widget;
extern widget InVKbd_confirm_widget;
extern widget RO_widget;
extern widget VIN_widget;
extern widget Email_widget;
char* get_input_email_str(void);
uint32_t get_batype_str_idx(uint8_t testype, uint8_t batype);
uint32_t get_rating_str_idx(uint8_t rating);
uint32_t get_result_str_idx(uint8_t result);
uint8_t get_JIS_model_rating(uint16_t idx);


extern widget ChkBatTemp_widget;
extern widget ChkMemICFull_widget;
extern widget ChkLoadErr_widget;
extern widget OutputCableChkComplete_widget;
extern widget Configration_widget;
extern widget DeviceRegisterManual_widget;
extern widget bootup_widget;
extern widget ClearMemory_widget;
extern widget Voltmeter_widget;
extern widget Ammeter_widget;

#endif

