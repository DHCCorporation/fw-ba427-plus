/*
 * VKmgr.h
 *
 *  Created on: 2020�~12��22��
 *      Author: henry.yang
 */

#ifndef APP_VKMGR_H_
#define APP_VKMGR_H_
#include "../service/lcdsvc.h"
#include "../service/wordbanksvc.h"
#include "../service/SystemInfosvs.h"
#include "../service/UTF8_WBsvc.h"
#include "../service/langaugesvc.h"
#include "../service/WordString.h" //20200812 dw dit

#include "../driver/st7565_lcm.h"
#include "../driver/a25L032_fls.h"
#include "../driver/smallfonts.h"
#include "../driver/mem_protect.h"
#include "../driver/soft_timer.h"
#include "../driver/delaydrv.h"

#include "../driver/uartstdio.h"

#define CURSOR_WIDTH 2
#define CURSOR_HEIGHT 7
#define CURSOR_BLINK_T 50

typedef struct {
  tRectangle recs;
  const char c;
} BUTTON_DATA;

enum{
    VK_KEY_BACKSPACE = 0x08,
    VK_KEY_LEFT      = 0x09,
    VK_KEY_UP        = 0x0a,
    VK_KEY_RIGHT     = 0x0b,
    VK_KEY_DOWN      = 0x0c,
    VK_KEY_ENTER     = 0x0d,
    VK_KEY_Fn_ABC    = 0x11,
    VK_KEY_Fn_abc    = 0x12,
    VK_KEY_Fn_123    = 0x13,
    VK_KEY_Fn_sym    = 0x14,
};

typedef struct{
    char* title_str;
    bool underline;
    uint16_t start_x;
    uint16_t start_y;
    uint16_t char_width;
    uint8_t len;
    uint8_t arr_flag;
    void (*cb)(char*,uint8_t);
}VK_INFO_T;

enum{
    RO_INPUT,
    VIN_INPUT,
    EMAIL_INPUT,
};

uint8_t VK_char_num_init(VK_INFO_T* vkinfo);
void VK_char_num_display(void);
char VK_char_num_up(void);
char VK_char_num_down(void);
char VK_char_num_left(void);
char VK_char_num_right(void);
void VK_char_num_sel(void);
void VK_CN_Cursor_blink(void);


uint8_t VK_char_num_sym_init(VK_INFO_T* vkinfo);
void VK_char_num_sym_display(void);
char VK_char_num_sym_up(void);
char VK_char_num_sym_down(void);
char VK_char_num_sym_left(void);
char VK_char_num_sym_right(void);
char VK_char_num_sym_sel(void);
void VK_CNS_Cursor_blink_V(void);

#endif /* APP_VKMGR_H_ */
