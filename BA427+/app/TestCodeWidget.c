//*****************************************************************************
//
// Source File: TestCodeWidget.c
// Description: Show "TEST CODE", generate test code please refer [DisplayResult_widget].
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 11/10/17     Jerry.W      	Created file.
// 11/16/17     Henry.Y        Move TestCodeGenerater() to DisplayResult_widget
// 02/14/18     Henry.Y        upload test record at widget initial.
// 03/22/18     Henry.Y        1. TestCode_initial() add system test record and data upload.
//                             2. add TestCode_service()
// 03/23/18     Jerry.W        Add determination of upload test record for valid/invalid VIN in TestCodeWidget widget.
// 05/21/18     Jerry.W        Add updating message.
// 09/11/18     Henry.Y        Use systeminfo data struct to replace the global variables.
// 12/07/18     Henry.Y        New display api.
// =============================================================================
//
//
// $Header$
// $DateTime$
// $Author$
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#include "Widgetmgr.h"



void TestCode_initial();

void TestCode_SELECT();
void TestCode_display();
void TestCode_service();


widget* TestCode_sub_W[] = {
		&PrintResult_widget,
};

widget TestCode_widget = {
		.Key_UP = 0,
		.Key_DOWN = 0,
		.Key_RIGHT = 0,
		.Key_LEFT = 0,
		.Key_SELECT = TestCode_SELECT,
		.routine = 0,
		.service = TestCode_service,
		.initial = TestCode_initial,
		.display = TestCode_display,
		.subwidget = TestCode_sub_W,
		.number_of_widget = sizeof(TestCode_sub_W)/4,
		.index = 0,
		.name = "TestCode",
};



void TestCode_initial(){
	//
	// Process data record and upload 
	//
	uint16_t update_count;
	TEST_DATA* TD = Get_test_data();
	SYS_PAR* par = Get_system_parameter();
	SYS_INFO* info = Get_system_info();
	RECORD_STATUS UploadOrNot = (par->WHICH_TEST == System_Test)?  No_upload:Wait_uplaod;

	if(par->WHICH_TEST == Start_Stop_Test){

		BATTERY_SARTSTOP_RECORD_T record = {.Test_Type = Start_Stop_Test,
											.Test_Time = {TD->year, TD->month, TD->date, TD->hour, TD->min, TD->sec,},
											.VIN = {'\0'},
											.RO = {'\0'},
											.Battery_Type = (BATTERYTYPE)par->SS_test_par.SSBatteryType,
											.Voltage = TD->battery_test_voltage,
											.Ratting = (SELRATING)par->SS_test_par.SSSelectRating,
											.CCA_Capacity = par->SS_test_par.SSSetCapacityVal,
											.CCA_Measured = TD->Measured_CCA,
											.Judgement = (TEST_RESULT)TD->Judge_Result,
											.SOH = (uint8_t)TD->SOH,
											.SOC = (uint8_t)TD->SOC,
											.RC_Set = TD->SetRCVal,
											.RC_Measured = TD->Measured_RC,
											.RC_judegment = (RC_RESULT)TD->RCJudge_Result,
											.Test_code = {'\0'},
											.Temperature = TD->ObjTestingTemp,
											.IsBatteryCharged = TD->IsBatteryCharged};
		memcpy(record.VIN, par->VIN_str, VININ_LEN1);
		memcpy(record.RO, par->RO_str, ROIN_LEN);
		memcpy(record.Test_code, TD->TestCode, TESTCODE_LEN);
		SaveTestRecords(Start_Stop_Test,&record, sizeof(record), UploadOrNot);

	}	
	if(par->WHICH_TEST == Battery_Test){

		BATTERY_SARTSTOP_RECORD_T record = {.Test_Type = Battery_Test,
											.Test_Time = {TD->year, TD->month, TD->date, TD->hour, TD->min, TD->sec,},
											.VIN = {'\0'},
											.RO = {'\0'},
											.Battery_Type = (BATTERYTYPE)par->BAT_test_par.BatteryType,
											.Voltage = TD->battery_test_voltage,
											.Ratting = (SELRATING)par->BAT_test_par.SelectRating,
											.CCA_Capacity = par->BAT_test_par.BTSetCapacityVal,
											.CCA_Measured = TD->Measured_CCA,
											.Judgement = (TEST_RESULT)TD->Judge_Result,
											.SOH = (uint8_t)TD->SOH,
											.SOC = (uint8_t)TD->SOC,
											.RC_Set = TD->SetRCVal,
											.RC_Measured = TD->Measured_RC,
											.RC_judegment = (RC_RESULT)TD->RCJudge_Result,
											.Test_code = {'\0'},
											.Temperature = TD->ObjTestingTemp,
											.IsBatteryCharged = TD->IsBatteryCharged};
		memcpy(record.VIN, par->VIN_str, VININ_LEN1);
		memcpy(record.RO, par->RO_str, ROIN_LEN);
		memcpy(record.Test_code, TD->TestCode, TESTCODE_LEN);
		SaveTestRecords(Battery_Test,&record, sizeof(record), UploadOrNot);
	}

	if(par->WHICH_TEST == System_Test){
		
		SYSTEM_TEST_RECORD_T record = { .Test_Type = System_Test,
										.Test_Time = {TD->year, TD->month, TD->date, TD->hour, TD->min, TD->sec,},
										.VIN = {'\0'},
										.RO = {'\0'},
										.Crank_result = (SYSRANK)TD->CrankVResult,
										.Crank_voltage = TD->CrankVol,
										.Idle_result = (SYSRANK)TD->AltIdleVResult,
										.Idle_voltage = TD->IdleVol,
										.Ripple_result = (SYSRANK)TD->RipVResult,
										.Ripple_voltage = TD->ripple,
										.Loaded_result = (SYSRANK)TD->AltLoadVResult,
										.Loaded_voltage = TD->LoadVol,
										.Temperature = TD->SysObjTemp,};
		memcpy(record.VIN, par->VIN_str, VININ_LEN1);
		memcpy(record.RO, par->RO_str, ROIN_LEN);
		SaveTestRecords(System_Test,&record, sizeof(record), UploadOrNot);
		return;
	}

	update_count = get_saved_record_count();
#if AWS_IOT_SERVICE
	if(info->device_registered == 0){
		Message_by_language(unregister_result_stored, update_count,en_US);
	}
	else
#endif
	{
		if(wifi_get_status() == WIFI_off){
			if(UploadOrNot == Wait_uplaod){

				Message_by_language(Store_result, update_count,en_US);
			}
		}
		else{
			if(UploadOrNot == Wait_uplaod){
				Message_by_language(Data_uploaded, update_count,en_US);
			}
			else{
				if(update_count){
					Message_by_language(Uploading_result, update_count,en_US);
				}
			}
			upload_saved_recod();
		}
	}

}


void TestCode_SELECT(){
	SYS_PAR* par = Get_system_parameter();
	if(par->WHICH_TEST == System_Test) return;

	Move_widget(TestCode_sub_W[0], 0);
}

void TestCode_display(){
	SYS_PAR* par = Get_system_parameter();
	if(par->WHICH_TEST == System_Test) return;

	uint8_t i;
	TEST_DATA* TD = Get_test_data();

	LCD_display(TEST_CODE,0);	

	for(i= 0;i<TESTCODE_LEN;i++){
		ST7565Draw6x8Char(25 + i*7, 25, TD->TestCode[i], Font_System5x8);
	}
}

void TestCode_service(){
	SYS_PAR* par = Get_system_parameter();
	if(par->WHICH_TEST == System_Test) Move_widget(TestCode_sub_W[0], 0);
}




