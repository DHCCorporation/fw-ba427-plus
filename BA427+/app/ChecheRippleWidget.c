//*****************************************************************************
//
// Source File: ChecheRippleWidget.c
// Description: Check ripple for Battery, start-stop test and system test.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 09/21/17 	Jerry.W	       Created file.
// 09/21/17 	Jerry.W		   Rewrite Widget for new software architecture
// 09/30/17 	Jerry.W		   1. add rouitne function, and move the function in CheckRipple_SERVICE besides test record saving operation into CheckRipple_Routine
//							   2. remove CheckRipple_SERVICE from CheckRipple_widget, the test record saving operation will be process at Printresultwidget
// 11/06/17     Henry.Y        1. reduce screen polling time of system test
// 11/10/17     Jerry.W        1. Modify the direction of next widget for Hyundai
// 11/22/17     Henry.Y	       1. Modify CheckRipple_sub_W[0]: ChkSurfaceCharge_widget
//                             2. Set g_TestInVehicle to 0 before surface charge check.
// 09/11/18     Henry.Y        Add CheckRipple_LEFT to abort the test.
//                             Adjust voltage display location.
// 10/17/18     Henry.Y        Adjust check ripple coding layout.
// 12/07/18     Henry.Y        New display api.
// 09/08/20     David.Wang     Sys test Graphic display replaced by text prompt
// 20201204     D.W.           The modification process is the same as BT2010
//
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/app/ChecheRippleWidget.c#1 $
// $DateTime: 2017/11/27 10:38:22 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#include "Widgetmgr.h"


void CheckRipple_display();
void CheckRipple_service();
void CheckRipple_Routine();


widget* CheckRipple_sub_W[] = {
		&TestInVehicle_widget,
		&CrankingVolt_widget,
};

widget CheckRipple_widget = {
		.Key_UP = 0,
		.Key_DOWN = 0,
		.Key_RIGHT = 0,
		.Key_LEFT = 0,
		.Key_SELECT = 0,
		.routine = CheckRipple_Routine,
		.initial = 0,
		.service = CheckRipple_service,
		.display = CheckRipple_display,
		.subwidget = CheckRipple_sub_W,
		.number_of_widget = sizeof(CheckRipple_sub_W)/4,
		.index = 0,
		.no_messgae = 1,
		.name = "CheckRipple",
};

void CheckRipple_display(){
    SYS_PAR* par = Get_system_parameter();

    if(!CheckRipple_widget.parameter){
        if(par->WHICH_TEST != System_Test){
            LCD_display(TESTING_PROCESSING_ARROW,0);
            delay_ms(500);
        }
    }
    else{
        ST7565ClearScreen(BUFFER1);
        ST7565Refresh(BUFFER1);

        if(par->WHICH_TEST == System_Test){
            LCD_String_ext(16,get_Language_String(S_OFF_LOADS_n_ENGINE) , Align_Central);
            LCD_String_ext(32,get_Language_String(S_AND_KEYSWITCH_ON) , Align_Central);
        }
        else{
            LCD_String_ext(16,get_Language_String(S_VOLTAGE_UNSTABLE) , Align_Central);
            LCD_String_ext(32,get_Language_String(S_WAIT_FOR_STABLE) , Align_Left);
        }
    }
}

void CheckRipple_service()
{
	SYS_PAR* par = Get_system_parameter();
    float ripple_voltage;
    if(BattCoreJudgeRipple(&ripple_voltage)){        
        if(!CheckRipple_widget.parameter){
            CheckRipple_widget.parameter = 1;
            CheckRipple_display();
        }
    }
    else{
       if((par->WHICH_TEST == Battery_Test) || (par->WHICH_TEST == Start_Stop_Test)){
           Move_widget_untracked(CheckRipple_sub_W[0],0);      // ALL tO Chk Surface Charge widget
       }
       else{
           Move_widget_untracked(CheckRipple_sub_W[1],0);
       }
    }    
}

void CheckRipple_Routine()
{
    CheckRipple_service();
}

