//*****************************************************************************
//
// Source File: Checkintbatwidget.c
// Description: Check internal battery voltage and pop up the warning message if voltage is low.
//              Press Enter to exit the popup message and turn back to Main Menu.
//
//
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 10/22/15     Henry.Y	      Created file.
// 04/06/16     Vincent.T     1. Modify CheckIntBatClick(); to handle multination language case.
// 09/21/17 	Jerry.W		   Rewrite Widget for new software architecture
// 11/19/18 	Henry.Y		   Turn off wifi.
// 12/07/18     Henry.Y        New display api.
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/app/Checkintbatwidget.c#6 $
// $DateTime: 2017/09/22 11:17:33 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#include "Widgetmgr.h"

void ChkIntBat_DISPLAY();
void ChkIntBat_SELECT();
void ChkIntBat_Service();

widget* ChkIntBat_sub_W[] = {

};

widget ChkIntBat_widget = {
		.Key_UP = 0,
		.Key_DOWN = 0,
		.Key_RIGHT = 0,
		.Key_LEFT = 0,
		.Key_SELECT = ChkIntBat_SELECT,
		.routine = 0,
		.service = ChkIntBat_Service,
		.initial = 0,
		.display = ChkIntBat_DISPLAY,
		.subwidget = ChkIntBat_sub_W,
		.number_of_widget = sizeof(ChkIntBat_sub_W)/4,
		.index = 0,
		.no_messgae = 1,
		.name = "ChkIntBat",
};


void ChkIntBat_SELECT( )
{
	UARTprintf("CheckIntBatClick()\n");

	Widget_init(manual_test);
}

void ChkIntBat_DISPLAY(){
	//LCD_display(REPLACE_INTERNAL_BATTERY,0);
    // Clear screen
    ST7565ClearScreen(BUFFER1);
    ST7565Refresh(BUFFER1);
    // Write SCREEN
    LCD_String_ext(16,get_Language_String( S_POWER_LOW ) , Align_Central);
}

void ChkIntBat_Service(){
	uint8_t block = 1;
	IntBat_SW_OFF;
	wifi_config(false, NULL);

	while(block){}
}
