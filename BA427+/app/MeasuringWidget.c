//*****************************************************************************
//
// Source File: MeasuringWidget.c
// Description: Measureing service for Battery and start-stop test.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 09/21/17 	Jerry.W	       Created file.
// 09/21/17 	Jerry.W		   Rewrite Widget for new software architecture
// 11/22/17     Henry.Y        1. Remove gfMeasuredVolt
//                             2. Move test range limitation after temperature compensation.
// 09/11/18     Henry.Y        Measuring_Service() add IR test.
// 12/07/18     Henry.Y        New display api.
// 12/18/18     Henry.Y        Add new judged condition: V < 8.0V before load on testing.
// 03/20/19     Henry.Y        Rounddown test voltage and IR to the second decimal place.
// 09/06/20     David.Wang     Remask 6v bat test for BT2000_HD_BO
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/app/MeasuringWidget.c#1 $
// $DateTime: 2017/11/27 10:38:22 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#include "Widgetmgr.h"

void Measuring_DISPLAY();
void Measuring_Service();


widget* Measuring_sub_W[] = {
		&DisplayResult_widget,
		&IsBatteryCharged_widget,
		&LoadError_widget
};

widget Measuring_widget = {
		.Key_UP = 0,
		.Key_DOWN = 0,
		.Key_RIGHT = 0,
		.Key_LEFT = 0,
		.Key_SELECT = 0,
		.routine = 0,
		.service = Measuring_Service,
		.display = 0,
		.subwidget = Measuring_sub_W,
		.number_of_widget = sizeof(Measuring_sub_W)/4,
		.index = 0,
		.no_messgae = 1,
		.name = "Measuring",
};

void Measuring_Service(){
	LCD_display(TESTING_PROCESSING_ARROW, 0);
	uint32_t ui32Voltage_scale;
	SYS_PAR* par = Get_system_parameter();
	TEST_DATA* TD = Get_test_data();
	SYS_INFO* info = Get_system_info();
	float fval;
	//
	// Load test count++
	//

/*  no SS test
	if(par->WHICH_TEST == Start_Stop_Test){
		counter_increase(&info->SSTestCounter);
		save_system_info(&info->SSTestCounter, sizeof(info->SSTestCounter));
	}
 */
	if(par->WHICH_TEST == Battery_Test || par->WHICH_TEST == Start_Stop_Test){
		counter_increase(&info->BatTestCounter);
		save_system_info(&info->BatTestCounter, sizeof(info->BatTestCounter));
	}

	//
	// set test state
	//
	if(info->pre_test_state){
		info->pre_test_state = 0;
		save_system_info(&info->pre_test_state, sizeof(info->pre_test_state));
	}
	
	TD->IsBatteryCharged = 0;	//initial this parameter

	ExtRTCRead( &TD->year, &TD->month, &TD->date, &TD->day, &TD->hour, &TD->min, &TD->sec);
	delay_ms(100);
	LCD_display(TESTING_PROCESSING_ARROW,1);

	BattCoreGetVoltage(&ui32Voltage_scale, &fval);	

	TD->battery_test_voltage = floating_point_rounding(fval, 2, ffloor);
    float IR;
	if(BattCoreGetCCA(&IR, &TD->Measured_CCA,3)){
		BattCoreGetVoltage(&ui32Voltage_scale, &fval);
		TD->battery_test_voltage = floating_point_rounding(fval, 2, ffloor);
		CCATempComp(&TD->Measured_CCA, TD->ObjTestingTemp);
	
		IRTempComp(&IR, TD->ObjTestingTemp);
        TD->IR = floating_point_rounding(IR, 2, ffloor);
        
		if(TD->Measured_CCA > 3600){
			Move_widget(Measuring_sub_W[2],0);
			return;
		}
	
        LCD_display(TESTING_PROCESSING_ARROW,0);
        
		if(par->WHICH_TEST == Start_Stop_Test){
			BattCoreCCAUnitTransform(&TD->Measured_CCA, par->SS_test_par.SSBatteryType, par->SS_test_par.SSSelectRating, 0);
			TD->SOC = BattCoreCalSoc((MAP_BATTERYTYPE)par->SS_test_par.SSBatteryType, TD->battery_test_voltage, (VOL_CH)TD->SYSTEM_CHANNEL);
			TD->Judge_Result = BatJudgeMap((MAP_BATTERYTYPE)par->SS_test_par.SSBatteryType, TD->battery_test_voltage, &TD->Measured_CCA, &TD->SOH, (VOL_CH)TD->SYSTEM_CHANNEL);
		}
		else { //Battery Test
			BattCoreCCAUnitTransform(&TD->Measured_CCA, par->BAT_test_par.BatteryType, par->BAT_test_par.SelectRating, pg_JIS[TD->JISCapSelect].itemNum);
			TD->SOC = BattCoreCalSoc((MAP_BATTERYTYPE)par->BAT_test_par.BatteryType, TD->battery_test_voltage, (VOL_CH)TD->SYSTEM_CHANNEL);
			TD->Judge_Result = BatJudgeMap((MAP_BATTERYTYPE)par->BAT_test_par.BatteryType, TD->battery_test_voltage, &TD->Measured_CCA, &TD->SOH, (VOL_CH)TD->SYSTEM_CHANNEL);
		}
	
		if(TD->Judge_Result == TR_ASK_CUSTOM){//Is battery charged?
			Move_widget(Measuring_sub_W[1],0);
		}
        else{
        	Move_widget(Measuring_sub_W[0],0);
        }        
	}
	else{
		Move_widget(Measuring_sub_W[2],0);
	}
}

#if 0
void Measuring_Service(){
	uint32_t ui32Voltage_scale;
	SYS_PAR* par = Get_system_parameter();
	TEST_DATA* TD = Get_test_data();
	SYS_INFO* info = Get_system_info();
	float fval;
	//
	// Load test count++
	//
	if(par->WHICH_TEST == Start_Stop_Test){
		counter_increase(&info->SSTestCounter);
		save_system_info(&info->SSTestCounter, sizeof(info->SSTestCounter));
	}
	if(par->WHICH_TEST == Battery_Test){
		counter_increase(&info->BatTestCounter);
		save_system_info(&info->BatTestCounter, sizeof(info->BatTestCounter));
	}
	if(par->WHICH_TEST == IR_Test){	
		counter_increase(&info->IRTestCounter);
		save_system_info(&info->IRTestCounter, sizeof(info->IRTestCounter));
	}
	
	//
	// set test state
	//
	if(info->pre_test_state && par->WHICH_TEST != IR_Test){
		info->pre_test_state = 0;
		save_system_info(&info->pre_test_state, sizeof(info->pre_test_state));
	}
	
	TD->IsBatteryCharged = 0;	//initial this parameter

	ExtRTCRead( &TD->year, &TD->month, &TD->date, &TD->day, &TD->hour, &TD->min, &TD->sec);
	delay_ms(100);
	//LCD_display(TESTING_PROCESSING_ARROW,0);

	BattCoreGetVoltage(&ui32Voltage_scale, &fval);
	
	if(par->WHICH_TEST == IR_Test){
		TD->IRvoltage = floating_point_rounding(fval, 2, ffloor);
		
		if(BattCoreGetIR(&fval, 1, 10)){		
			switch(TD->SYSTEM_CHANNEL){
				case MAP_6V:
					BattCoreGetIR(&fval, 3, 10);
					break;
				default:
					if(fval > ThreewireTh){
						BattCoreGetIR(&fval, 3, 11);
					}
					else{
						BattCoreGetIR(&fval, 3, 10);
					}
					break;
			}
		
			//
			// Temperature Compensation
			// 20180904 change to use ambient temperature to de the compensation.
			//
			IRTempComp(&fval, TD->BodyTestingTemp);
			TD->IR = floating_point_rounding(fval, 2, ffloor);
		
			if(TD->IR <= 0.8){
			
				// New load error condition
				Move_widget(Measuring_sub_W[2], 0);
				return;
			}
		}
		else{
			Move_widget(Measuring_sub_W[2], 0);
			return;
		}	
	}
	else{
			TD->battery_test_voltage = floating_point_rounding(fval, 2, ffloor);
//#if defined(__BO__)       //20200906 dw remask 6v bat test for BT2000_HD_BO
//		if(TD->battery_test_voltage < 8.00){//20181214 new condition
//			TD->Measured_CCA = 0;
//			TD->SOC = 0;
//			TD->SOH = 0;
//			TD->Judge_Result = TR_ASK_CUSTOM;
//			Move_widget(Measuring_sub_W[1],0);
//			return;
//		}
//#endif
		if(BattCoreGetCCA(&TD->Measured_CCA,3)){
			BattCoreGetVoltage(&ui32Voltage_scale, &fval);
			TD->battery_test_voltage = floating_point_rounding(fval, 2, ffloor);
			CCATempComp(&TD->Measured_CCA, TD->ObjTestingTemp);
		
			if(TD->Measured_CCA > 3600){
				// New load error condition
				Move_widget(Measuring_sub_W[2],0);
				return;
			}
		
			if(par->WHICH_TEST == Start_Stop_Test){
				BattCoreCCAUnitTransform(&TD->Measured_CCA, par->SS_test_par.SSBatteryType, par->SS_test_par.SSSelectRating, 0);
				TD->SOC = BattCoreCalSoc((MAP_BATTERYTYPE)par->SS_test_par.SSBatteryType, TD->battery_test_voltage, (VOL_CH)TD->SYSTEM_CHANNEL);
				TD->Judge_Result = BatJudgeMap((MAP_BATTERYTYPE)par->SS_test_par.SSBatteryType, TD->battery_test_voltage, &TD->Measured_CCA, &TD->SOH, (VOL_CH)TD->SYSTEM_CHANNEL);
			}
			else { //Battery Test
				BattCoreCCAUnitTransform(&TD->Measured_CCA, par->BAT_test_par.BatteryType, par->BAT_test_par.SelectRating, pg_JIS[TD->JISCapSelect].itemNum);
				TD->SOC = BattCoreCalSoc((MAP_BATTERYTYPE)par->BAT_test_par.BatteryType, TD->battery_test_voltage, (VOL_CH)TD->SYSTEM_CHANNEL);
				TD->Judge_Result = BatJudgeMap((MAP_BATTERYTYPE)par->BAT_test_par.BatteryType, TD->battery_test_voltage, &TD->Measured_CCA, &TD->SOH, (VOL_CH)TD->SYSTEM_CHANNEL);
			}
		
			if(TD->Judge_Result == TR_ASK_CUSTOM){//Is battery charged?
				Move_widget(Measuring_sub_W[1],0);
				return;	
			}
		}
		else{
			Move_widget(Measuring_sub_W[2],0);
			return;
		}
	}

	Move_widget(Measuring_sub_W[0],0);
}
#endif







