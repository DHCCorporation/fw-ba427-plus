//*****************************************************************************
//
// Source File: Bluetoothsetwidget.c
// Description: Bluetooth setting menu's widget.
//
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 01/12/16     William.H      Created file.
// 01/27/16     Henry.Y	       global variables: g_BTServiceStart, to indicate the entry 'BLUETOOTHSET' and ready to do the 
//                             associated work.								
// 02/19/16     Vincent.T      1. Enable Timer -> g_BTCntStatTimer1AEnabled = true; To avoid LCD blinking.
// 03/26/16     Vincent.T      1. Modify BluetoothSetClick(); to handle multination language case.
//                             2. Modify BluetoothSetLeft(); to handle multination language case.
// 04/06/16     Vincent.T      1. Modify BluetoothSetClick(); to make sure turn on Bluetooth(power on and reset).
// 09/28/17 	Jerry.W		   1. Rewrite Widget for new software architecture
//							   2. Move all BT command process into this file
// 09/29/17 	Jerry.W		   1. finish BT command process function BT_commd_process()
//							   2. add CHECKRIPPLE15SEC operation in routine
//							   3. all coounter_callback() function for CHECKRIPPLE15SEC countdown
//							   4. add Measure() for Battery�BStart-Stop Test
// 11/06/17     Henry.Y	       1. Modify BattCoreChkClamp()
// 11/10/17     Jerry.W	       1. add Bluetooth_command_match() for service layer callback
// 11/22/17     Henry.Y	       1. BT and SS test record add parameter (..., g_SetRCVal, Measured_RC, RCJudge_Result)
// 11/23/17     Jerry.W	       1. change BT_commd_process return type for switching to printer mode functionality
//                             2. add functionality of RO/VIN receiving and test code return
//                             3. move set_BTcommand_callback function call from init into service function for return from printer mode
//                             4. add functionality of time calibration via bluetooth
//                             5. move test records saving operation form end of measuring to returning test code
// 12/21/17     Jerry.W        1. Add sending of disconnect protocol while closing Bluetooth.
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/app/Bluetoothsetwidget.c#4 $
// $DateTime: 2017/12/26 10:46:37 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#include "Widgetmgr.h"




void Bluetooth_DISPLAY();
void Bluetooth_Select();
void Bluetooth_Initial();
void Bluetooth_LEFT();
void Bluetooth_Service();
void Bluetooth_Routine();

widget* Bluetooth_sub_W[] = {
	&ChkIntBat_widget,
	&CheckClamps_widget
};

widget Bluetooth_widget = {
		.Key_UP = 0,
		.Key_DOWN = 0,
		.Key_RIGHT = 0,
		.Key_LEFT = Bluetooth_LEFT,
		.Key_SELECT = Bluetooth_Select,
		.routine = Bluetooth_Routine,
		.service = Bluetooth_Service,
		.display = Bluetooth_DISPLAY,
		.initial = Bluetooth_Initial,
		.subwidget = Bluetooth_sub_W,
		.number_of_widget = sizeof(Bluetooth_sub_W)/4,
		.index = 0,
		.no_messgae = 1,
		.name = "Bluetooth",
};


//								   0	  1	  	  2	      3		  4	 	  5	  	  6	   	  7
const char Voltage_high[8] = 	{0x04,	0x01,	0x03,	0xFF,	0xFF,	0xFF,	0xFF,	0xFF};
const char IntBat_low[8] = 		{0x04,	0x05,	0xFF,	0xFF,	0xFF,	0xFF,	0xFF,	0xFF};
const char clamp_coarse[8] =	{0x04,	0x04,	0x02,	0x21,	0xFF,	0xFF,	0xFF,	0xFF};
const char clamp_fine[8] =		{0x04,	0x04,	0x01,	0x21,	0xFF,	0xFF,	0xFF,	0xFF};
const char isit6VBAT[8] =		{0x04,	0x01,	0x01,	0xFF,	0xFF,	0xFF,	0xFF,	0xFF};
const char Ripple_high[8] =		{0x04,	0x02,	0x01,	0xFF,	0xFF,	0xFF,	0xFF,	0xFF};
const char IsItTestInVehi[8] =	{0x04,	0x01,	0x04,	0xFF,	0xFF,	0xFF,	0xFF,	0xFF};
const char LoadError[8] =		{0x04,	0x03,	0xFF,	0xFF,	0xFF,	0xFF,	0xFF,	0xFF};
const char IsBatCharged[8] =	{0x04,	0x07,	0xFF,	0xFF,	0xFF,	0xFF,	0xFF,	0xFF};
const char BatVolFalls[8] =		{0x04,	0x01,	0x04,	0x03,	0xFF,	0xFF,	0xFF,	0xFF};
const char StartCrank[8] =		{0x04,	0x02,	0x02,	0xFF,	0xFF,	0xFF,	0xFF,	0xFF};
const char disconnect[8] =		{0x04,	0x02,	0x02,	0x02,	0xFF,	0xFF,	0xFF,	0xFF};
extern const char c_handshake[7];

void Bluetooth_backblink_callback();
void coounter_callback();
void Bluetooth_command_callback(const char *command);
bool Bluetooth_command_match(const char *command,uint8_t lenth);
uint8_t BT_commd_process();
bool test_prepare();
bool Measure();

void SwitchToPrinterMode(uint32_t flag);		//defined by BluetoothPreinterWidget


bool timer_edabled = false;
uint8_t BT_command[20];
bool blink_toggle = true;
uint8_t timeout_counter;

static uint8_t  TestCodeFlag;
enum{
	RO_number = 1,
	VIN_number = 2
}_TestCodeFlag;

void Bluetooth_Initial(){

}


void Bluetooth_DISPLAY(){
	ST7565ClearScreen(BUFFER0);
	if(!g_BluetoothEnabled){
		LcdDrawScreen( 68, BUFFER0);
	}
	else if(BluetoothConnected){
		LcdDrawScreen( 70, BUFFER0);
	}
	else{
		LcdDrawScreen( 69, BUFFER0);
	}
}

void Bluetooth_Select(){
	if(BluetoothConnected){
		if(Bluetooth_widget.index || Bluetooth_widget.parameter){
			return;
		}
		else{
		BTUARTSend((const char*)disconnect,sizeof(disconnect));
			timeout_counter = 2;
			set_countdown_callback(coounter_callback, 2);
			while(BluetoothConnected && timeout_counter);
		}
	}
	g_BluetoothEnabled = (!g_BluetoothEnabled);

	Bluetooth_Service();
	Bluetooth_DISPLAY();
}

void Bluetooth_LEFT(){
	if(!g_BluetoothEnabled){
		Return_pre_widget();
	}
}

void Bluetooth_backblink_callback(){

	if(blink_toggle) PWMDisable();
	else 	   PWMEnable();
	blink_toggle = !blink_toggle;

}

void Bluetooth_command_callback(const char *command){
	TEST_DATA* TD = Get_test_data();

	if(!Bluetooth_widget.parameter){
		Bluetooth_widget.parameter = 1;
		if(*command == 0x22){
			memcpy((char *)BT_command, command , 20);
			BTUARTSend((const char*)BT_command,20);
		}
		else{
			memcpy((char *)BT_command, command , 7);
			BTUARTSend((const char*)BT_command,7);
		}
		ROM_SysCtlDelay(get_sys_clk()/3/10);
	}
	else if(command[0] == 0x55 && command[1] == 0x56 && command[2] == 0xFF && command[3] == 0xFF && command[4] == 0xFF && command[5] == 0xFF){
		TD->System_stop = true;
		BTUARTSend(command,7);
		//ROM_SysCtlDelay(get_sys_clk()/3/5);
	}
}

bool Bluetooth_command_match(const char *command,uint8_t lenth){
	if(*command == 0x22){
		return (lenth == 20);
	}
	else{
		return (lenth == 7);
	}
}


void Bluetooth_Service(){
	Bluetooth_widget.index = 0;
	Bluetooth_widget.parameter = 0;

	if(g_BluetoothEnabled){

		// Turn On Bluetooth
		BT_POWER_ON;
		BT_RESET_HIGH;

		// Per Henry.Y mention, bluetooth on/off processing time need 1ms(to fine tune to minimize the delay time).
		SysCtlDelay((get_sys_clk()/3)/1000);	// Wait 1mS

		Bluetooth_buffer_flush();
		set_BTcommand_callback( Bluetooth_command_callback, Bluetooth_command_match ); // in case of return from printer mode
	}
	else{
		BT_RESET_LOW;
		BT_POWER_OFF;

		ROM_TimerDisable(TIMER0_BASE, TIMER_A);
		PWMEnable();
		timer_edabled = false;
	}
}

void Bluetooth_Routine(){
	char System_test_result[8];
	uint32_t Vol;
	uint32_t IntBat_scale;
	float IntBat_voltage;
	TEST_DATA* TD = Get_test_data();

	if(g_BluetoothEnabled){
		if(BluetoothConnected){

			if(timer_edabled){
				ROM_TimerDisable(TIMER0_BASE, TIMER_A);
				PWMEnable();
				timer_edabled = false;
				Bluetooth_DISPLAY();
			}



			if(Bluetooth_widget.parameter){ //if has command to process

				if(BT_commd_process()) return;
				Bluetooth_DISPLAY();
				Bluetooth_widget.parameter = Bluetooth_widget.index; //lock command if CHECKRIPPLE15SEC didn't finished
			}
			if(Bluetooth_widget.index == 1){ //for CHECKRIPPLE15SEC
				if((TD->RipVResult = SysRipTest(&TD->ripple)) == NO_DETECT && (timeout_counter != 0)){
					//if no ripple detected and have not timeout, do nothing
					return;
				}

				System_test_result[0] = System_test_result[1] = 0x03;
				System_test_result[7] = 0xFF;

				Vol = TD->ripple*100;

				if(TD->RipVResult == NO_DETECT){
					System_test_result[2] = System_test_result[3] = System_test_result[4] = System_test_result[5] = 0x30;
					System_test_result[6] = 0x03;
				}
				else{
					System_test_result[2] = (Vol/1000)%10 + 0x30;
					System_test_result[3] = (Vol/100)%10 + 0x30;
					System_test_result[4] = (Vol/10)%10 + 0x30;
					System_test_result[5] = (Vol)%10 + 0x30;
					System_test_result[6] = (TD->RipVResult == HIGH)? 0x03 : 0x02;
				}
				BTUARTSend(System_test_result, sizeof(System_test_result));
				ROM_TimerDisable(TIMER0_BASE, TIMER_A);
				Bluetooth_widget.parameter = 0;
				Bluetooth_widget.index = 0;
			}
		}
		else{
			if(!timer_edabled){
				blink_toggle = true;
				set_countdown_callback(Bluetooth_backblink_callback, -1);
				timer_edabled = true;
				Bluetooth_widget.parameter = 0;
				Bluetooth_DISPLAY();
			}



			if(BattCoreChkIntBatt(&IntBat_scale, &IntBat_voltage)){
				ROM_TimerDisable(TIMER0_BASE, TIMER_A);
				g_BluetoothEnabled = false;
				Bluetooth_widget.service();
				Move_widget(Bluetooth_sub_W[0], 0);
			}

			if(BattCoreChkClamp()){
				ROM_TimerDisable(TIMER0_BASE, TIMER_A);
				g_BluetoothEnabled = false;
				Bluetooth_widget.service();
				Move_widget(Bluetooth_sub_W[1], 0);
			}

		}
	}
}



uint8_t BT_commd_process(){
	uint8_t i;
	uint32_t IntBat_scale, ui32Voltage_scale, Vol, CCA_show, flag;
	char BT_result[20] = {0x01,0x31,0x32,0x33,0x33,0x64,0x30,0x30,0x30,0x30,0x00,0x04,0xFF};
	char System_test_result[8];
	char TestCode_result[20];
	float IntBat_voltage, ripple_voltage, external_voltage, test_voltage, BT_SurfaceCharge_Voltage_V1, BT_SurfaceCharge_Voltage_V2, SurfaceCharge_Th;
	uint16_t ui16DatBuf[6];
	uint32_t time_temp, time_temp_host;
	SYS_PAR* par = Get_system_parameter();
	TEST_DATA* TD = Get_test_data();
	SYS_INFO* info = Get_system_info();
	if(BT_command[0] == 0x09){
		BTUARTSend((const char*)strFWver, strlen((const char*)strFWver));
	}
	else{
		//==================================//
		//		Check clamp command			//
		//==================================//
		if(BT_command[0] == 0x04 && BT_command[1] == 0x04 && BT_command[6] == 0xFF){
			if(BattCoreChkClamp()){
				BTUARTSend(clamp_coarse, sizeof(clamp_coarse));
			}
			else{
				BTUARTSend(clamp_fine, sizeof(clamp_fine));
			}
			if(memcmp(BT_command +2, c_handshake +2, 4) != 0){
				memcpy(&time_temp,BT_command +2,4);
				time_temp_host = (time_temp<<24) | ((time_temp&0x0000FF00)<<8) | ((time_temp&0x00FF0000)>>8) | (time_temp >> 24);
				ui16DatBuf[0] = (time_temp_host >> 26) + 2000;
				ui16DatBuf[1] = ((time_temp_host >> 22)&0x0F);
				ui16DatBuf[2] = ((time_temp_host >> 17)&0x1F);
				ui16DatBuf[3] = ((time_temp_host >> 12)&0x1F);
				ui16DatBuf[4] = ((time_temp_host >> 6)&0x3F);
				ui16DatBuf[5] = (time_temp_host&0x3F);
				ExtRTCWrite(ui16DatBuf[0], ui16DatBuf[1], ui16DatBuf[2], 0, ui16DatBuf[3], ui16DatBuf[4], ui16DatBuf[5]);
			}
			return 0;
		}

		//check battery voltage & internal battery & clamp when command arrives
		BattCoreScanVoltage(&external_voltage);
		if(external_voltage >= 31.99){
			BTUARTSend(Voltage_high, sizeof(Voltage_high));
			return 0;
		}
		if(BattCoreChkIntBatt(&IntBat_scale, &IntBat_voltage)){
			BTUARTSend(IntBat_low, sizeof(IntBat_low));
			return 0;
		}
		if(BattCoreChkClamp()){
			BTUARTSend(clamp_coarse, sizeof(clamp_coarse));
			return 0;
		}


		//==================================//
		//	Battery test & start-stop test	//
		//==================================//
		if(BT_command[0] == 0x01 || BT_command[0] == 0x02){
			if(test_prepare()) return 0;

			if(TD->SYSTEM_CHANNEL == MAP_24V) {
				BTUARTSend(Voltage_high, sizeof(Voltage_high));
				return 0;
			}

			if(par->WHICH_TEST == Start_Stop_Test) TD->SYSTEM_CHANNEL = MAP_12V;

			if(TD->SYSTEM_CHANNEL == MAP_6V) {
				BTUARTSend(isit6VBAT, sizeof(isit6VBAT)); // ask is is 6V?
				return 0;
			}

Check_Ripple :
			//check ripple
			if(BattCoreJudgeRipple(&ripple_voltage)){
				BTUARTSend(Ripple_high, sizeof(Ripple_high));
				return 0;
			}

			if(par->WHICH_TEST == Battery_Test){
				save_system_parameter(&par->BAT_test_par, sizeof(par->BAT_test_par));
			}
			else{
				save_system_parameter(&par->SS_test_par, sizeof(par->SS_test_par));
			}

			ROM_SysCtlDelay(get_sys_clk()/3);

			//check surface charge
			BattCoreGetVoltage(&ui32Voltage_scale, &test_voltage);
			if(TD->Surface_charge){
				BTUARTSend(IsItTestInVehi, sizeof(IsItTestInVehi));
				return 0;
			}

Measuring :
			if(!Measure()){
				BTUARTSend(LoadError, sizeof(LoadError));
				return 0;
			}

			if(TD->Judge_Result == 6){
				BTUARTSend(IsBatCharged, sizeof(IsBatCharged));
				return 0;
			}

Send_result :
			//preparing
			Vol = TD->battery_test_voltage*100;
			BT_result[0] = (par->WHICH_TEST == Battery_Test)? 0x01 : 0x02;
			BT_result[1] = (Vol/1000)%10  + 0x30;
			BT_result[2] = (Vol/100)%10 + 0x30;
			BT_result[3] = (Vol/10)%10 + 0x30;
			BT_result[4] = (Vol)%10  + 0x30;

			BT_result[5] = TD->SOC;

			CCA_show = TD->Measured_CCA;
			BT_result[6] = (TD->Judge_Result == 5)? 0x30 : (CCA_show/1000)%10  + 0x30;
			BT_result[7] = (TD->Judge_Result == 5)? 0x30 : (CCA_show/100)%10  + 0x30;
			BT_result[8] = (TD->Judge_Result == 5)? 0x30 : (CCA_show/10)%10  + 0x30;
			BT_result[9] = (TD->Judge_Result == 5)? 0x30 : (CCA_show)%10  + 0x30;

			BT_result[10] = (TD->Judge_Result == 5)? 0x30 : TD->SOH;

			BT_result[11] = (TD->Judge_Result == 0)? 1:
							(TD->Judge_Result == 1)? 2:
							(TD->Judge_Result == 2)? 4:
							(TD->Judge_Result == 3)? 7:
							(TD->Judge_Result == 4)? 3:
							(TD->Judge_Result == 5)? 5: 6;

			BT_result[12] = TD->year%100;
			BT_result[13] = TD->month;
			BT_result[14] = TD->date;
			BT_result[15] = TD->hour;
			BT_result[16] = TD->min;
			BT_result[17] = TD->sec;

			BT_result[18] = ((char)TD->ObjTestingTemp) + 20;

			BT_result[19] = 0xFF;


			TestCodeFlag = 0;

			//****************************
			// send result to app
			//****************************
			for(i = 0 ; i < 3 ; i++){
				PWMDisable();
				ROM_SysCtlDelay(get_sys_clk()/3/10);
				PWMEnable();
				ROM_SysCtlDelay(get_sys_clk()/3/10);
			}

			BTUARTSend(BT_result, sizeof(BT_result));


			return 0;
		}


		//==================================//
		//		APP response message		//
		//==================================//
		if(BT_command[0] == 0x04){
			if(BT_command[1] == 0x01){
				if(BT_command[2] <= 2){ // app answer that is it 6V battery?

					TD->SYSTEM_CHANNEL = (BT_command[2] == 0x01)? MAP_6V : MAP_12V;
					goto Check_Ripple;

				}
				else if(BT_command[2] == 0x04){ //app answer that is it test in vehicle?

					if(BT_command[3] == 0x02){
						goto Measuring;
					}
					else{
						TD->TestInVehicle = 1;

						SurfaceCharge_Th = (TD->SYSTEM_CHANNEL == MAP_12V)? 0.1 : 0.05;

						BattCoreGetVoltage(&ui32Voltage_scale, &BT_SurfaceCharge_Voltage_V1);
						ROM_SysCtlDelay(get_sys_clk()/3/500);

						timeout_counter = 25;
						set_countdown_callback(coounter_callback, timeout_counter);

						while(timeout_counter){
							BattCoreGetVoltage(&ui32Voltage_scale, &BT_SurfaceCharge_Voltage_V2);

							if((BT_SurfaceCharge_Voltage_V1 - BT_SurfaceCharge_Voltage_V2) > SurfaceCharge_Th){
								BTUARTSend(BatVolFalls, sizeof(BatVolFalls));
								break;
							}

						}
						ROM_TimerDisable(TIMER0_BASE, TIMER_A);
					}
				}
			}
			else if(BT_command[1] == 0x07){// app answer that is battery charged?

				if(BT_command[2] == 0x01){
					TD->Judge_Result = 2;
				}
				else{
					TD->Judge_Result = 4;
				}
				goto Send_result;

			}
			return 0;
		}

		//==================================//
		//			System test				//
		//==================================//
		if(BT_command[0] == 0x03){
			if(BT_command[1] == 0x01){
				par->WHICH_TEST = System_Test;
				//check ripple
				if(BattCoreJudgeRipple(&ripple_voltage)){
					BTUARTSend(Ripple_high, sizeof(Ripple_high));
					return 0;
				}

				BTUARTSend(StartCrank, sizeof(StartCrank));

				TD->CrankVResult = BattCoreCrankingVolt(&TD->CrankVol, &TD->Crank_vmin);

				Vol = TD->CrankVol*100;

				System_test_result[0] = 0x03;
				System_test_result[1] = 0x01;
				System_test_result[7] = 0xFF;

				switch(TD->CrankVResult){
				case NO_DETECT :
					System_test_result[2] = System_test_result[3] = System_test_result[4] = System_test_result[5] = 0x30;
					System_test_result[6] = 0x03;
					break;
				case LOW :
					System_test_result[2] = (Vol/1000)%10 + 0x30;
					System_test_result[3] = (Vol/100)%10 + 0x30;
					System_test_result[4] = (Vol/10)%10 + 0x30;
					System_test_result[5] = (Vol)%10 + 0x30;
					System_test_result[6] = 0x01;
					break;
				case NORMAL :
					System_test_result[2] = (Vol/1000)%10 + 0x30;
					System_test_result[3] = (Vol/100)%10 + 0x30;
					System_test_result[4] = (Vol/10)%10 + 0x30;
					System_test_result[5] = (Vol)%10 + 0x30;
					System_test_result[6] = 0x02;
					break;
				default :
					return 0;
				}
				BTUARTSend(System_test_result, sizeof(System_test_result));

			}
			else if(BT_command[1] == 0x02){ 				//ALTIDLEVOLRESULT

				TD->AltIdleVResult = AltIdleVoltTest(&TD->IdleVol);

				Vol = TD->IdleVol*100;

				System_test_result[0] = 0x03;
				System_test_result[1] = 0x02;
				System_test_result[7] = 0xFF;

				System_test_result[2] = (Vol/1000)%10 + 0x30;
				System_test_result[3] = (Vol/100)%10 + 0x30;
				System_test_result[4] = (Vol/10)%10 + 0x30;
				System_test_result[5] = (Vol)%10 + 0x30;

				switch(TD->AltIdleVResult){
				case LOW:
					System_test_result[6] = 0x01;
					break;
				case NORMAL:
					System_test_result[6] = 0x02;
					break;
				case HIGH:
					System_test_result[6] = 0x03;
					break;
				default:
					return 0;
				}
				BTUARTSend(System_test_result, sizeof(System_test_result));

			}
			else if(BT_command[1] == 0x03){ 				//CHECKRIPPLE15SEC
				if(Bluetooth_widget.index != 1){
					Bluetooth_widget.index = 1;
					//it will make Routine function to check ripple for 15 Sec
					timeout_counter = 14;
					set_countdown_callback(coounter_callback, timeout_counter);
				}
			}
			else if(BT_command[1] == 0x04){					//ALTLOADVTESTRESULT

				TD->AltLoadVResult = AltLoadVoltTest(&TD->LoadVol);

				Vol = TD->LoadVol*100;

				System_test_result[0] = 0x03;
				System_test_result[1] = 0x04;
				System_test_result[2] = (Vol/1000)%10 + 0x30;
				System_test_result[3] = (Vol/100)%10 + 0x30;
				System_test_result[4] = (Vol/10)%10 + 0x30;
				System_test_result[5] = (Vol)%10 + 0x30;

				System_test_result[6] = (TD->AltLoadVResult == LOW)? 	0x01 :
										(TD->AltLoadVResult == NORMAL)? 0x02 : 0x03;

				System_test_result[7] = 0xFF;

				BTUARTSend(System_test_result, sizeof(System_test_result));

				//save test record
				ExtRTCRead( &TD->year, &TD->month, &TD->date, &TD->day, &TD->hour, &TD->min, &TD->sec);
				//SysTestRecord(gui16TotalTestRecord, gui8CrankVResult, gfCrankVol, gui8AltIdleVResult, gfIdleVol, gui8RipVResult, gfripple, gui8AltLoadVResult, gfLoadVol, fSysObjTemp, year, month, date, day, hour, min, sec);
				SYSTEM_TEST_RECORD_T record = {	.Test_Type = System_Test,
												.Test_Time = {TD->year, TD->month, TD->date, TD->hour, TD->min, TD->sec,},
												.VIN = {'\0'},
												.RO = {'\0'},
												.Crank_result = (SYSRANK)TD->CrankVResult,
												.Crank_voltage = TD->CrankVol,
												.Idle_result = (SYSRANK)TD->AltIdleVResult,
												.Idle_voltage = TD->IdleVol,
												.Ripple_result = (SYSRANK)TD->RipVResult,
												.Ripple_voltage = TD->ripple,
												.Loaded_result = (SYSRANK)TD->AltLoadVResult,
												.Loaded_voltage = TD->LoadVol,
												.Temperature = TD->ObjTestingTemp,};
				SaveTestRecords(System_Test,&record, sizeof(record), No_upload);
				//increase test counter
				counter_increase(&info->SysTestCounter);
				save_system_info(&info->SysTestCounter, sizeof(info->SysTestCounter));
			}
		}

		if((BT_command[0] == 0x07)&&(BT_command[1] == 0x01)){
			flag = *(uint32_t*)(BT_command+2);
			SwitchToPrinterMode(flag);
			return 1;
		}

		if(BT_command[0] == 0x22){
			if(BT_command[1] == 0x0E){ //RO
				TestCodeFlag |= RO_number;
				memcpy(par->RO_str, BT_command + 2 , 6);
			}
			else if(BT_command[1] == 0x0F){ //VIN
				TestCodeFlag |= VIN_number;
				memcpy(par->VIN_str, BT_command + 2 , 6);
			}

			if(TestCodeFlag == (VIN_number|RO_number) ){
				//send test code back
				uint8_t Rating = (par->WHICH_TEST == Start_Stop_Test)? par->SS_test_par.SSSelectRating : par->BAT_test_par.SelectRating;
                if(Rating == JIS){
                    Rating = get_JIS_model_rating(par->BAT_test_par.BTJIS_RgBatSelect);
                }
                TESTCODE_T data = { .fvol = TD->battery_test_voltage,
                                    .ui8TestResult = TD->Judge_Result, 
                                    .ui8CCARating = Rating,
                                    .ui16SetCCA = TD->SetCapacityVal,
                                    .ui16TestCCA = (uint16_t)TD->Measured_CCA,
                                    .fTemp = TD->ObjTestingTemp,
                                    .ui16Year = TD->year,
                                    .ui8Month =TD->month, 
                                    .ui8Day = TD->date,
                                    .hour = TD->hour,
                                    .minute = TD->min};
                TestCodeGenerater(TD->TestCode, sizeof(TD->TestCode), &data);
				//TestCodeGenerater(TD->TestCode, TD->battery_test_voltage, TD->Judge_Result, Rating, TD->SetCapacityVal, (uint16_t)TD->Measured_CCA, TD->ObjTestingTemp, TD->year, TD->month, TD->date, TD->hour, TD->min);
				

				//save test parameters
				if(par->WHICH_TEST == Battery_Test){
					// Save test parameters
					save_system_parameter(&par->BAT_test_par, sizeof(par->BAT_test_par));

					// Increase Test Counter
					counter_increase(&info->BatTestCounter);
					save_system_info(&info->BatTestCounter, sizeof(info->BatTestCounter));

					//  Save Battery Test Record
					BATTERY_SARTSTOP_RECORD_T record = {.Test_Type = Battery_Test,
														.Test_Time = {TD->year, TD->month, TD->date, TD->hour, TD->min, TD->sec,},
														.VIN = {'\0'},
														.RO = {'\0'},
														.Battery_Type = (BATTERYTYPE)par->BAT_test_par.BatteryType,
														.Voltage = TD->battery_test_voltage,
														.Ratting = (SELRATING)par->BAT_test_par.SelectRating,
														.CCA_Capacity = par->BAT_test_par.BTSetCapacityVal,
														.CCA_Measured = TD->Measured_CCA,
														.Judgement = (TEST_RESULT)TD->Judge_Result,
														.SOH = (uint8_t)TD->SOH,
														.SOC = (uint8_t)TD->SOC,
														.RC_Set = TD->SetRCVal,
														.RC_Measured = TD->Measured_RC,
														.RC_judegment = (RC_RESULT)TD->RCJudge_Result,
														.Test_code = {'\0'},
														.Temperature = TD->ObjTestingTemp};
					memcpy(record.VIN, par->VIN_str, 6);
					memcpy(record.RO, par->RO_str, 6);
					memcpy(record.Test_code, TD->TestCode, 11);
					SaveTestRecords(Battery_Test,&record, sizeof(record), Wait_uplaod);

				}
				else{ //start-stop test
					// Save test parameters
					save_system_parameter(&par->SS_test_par, sizeof(par->SS_test_par));

					// Increase Test Counter
					counter_increase(&info->SSTestCounter);
					save_system_info(&info->SSTestCounter, sizeof(info->SSTestCounter));

					//  Save Battery Test Record
					BATTERY_SARTSTOP_RECORD_T record = {.Test_Type = Start_Stop_Test,
														.Test_Time = {TD->year, TD->month, TD->date, TD->hour, TD->min, TD->sec,},
														.VIN = {'\0'},
														.RO = {'\0'},
														.Battery_Type = (BATTERYTYPE)par->SS_test_par.SSBatteryType,
														.Voltage = TD->battery_test_voltage,
														.Ratting = (SELRATING)par->SS_test_par.SSSelectRating,
														.CCA_Capacity = par->SS_test_par.SSSetCapacityVal,
														.CCA_Measured = TD->Measured_CCA,
														.Judgement = (TEST_RESULT)TD->Judge_Result,
														.SOH = (uint8_t)TD->SOH,
														.SOC = (uint8_t)TD->SOC,
														.RC_Set = TD->SetRCVal,
														.RC_Measured = TD->Measured_RC,
														.RC_judegment = (RC_RESULT)TD->RCJudge_Result,
														.Test_code = {'\0'},
														.Temperature = TD->ObjTestingTemp};
					memcpy(record.VIN, par->VIN_str, 6);
					memcpy(record.RO, par->RO_str, 6);
					memcpy(record.Test_code, TD->TestCode, 11);
					SaveTestRecords(Start_Stop_Test,&record, sizeof(record) , Wait_uplaod);
				}

				memset(TestCode_result, 0xFF, 20);
				TestCode_result[0] = 0x22;
				TestCode_result[1] = 0x0C;
				memcpy(TestCode_result+2,  TD->TestCode, TESTCODE_LEN);

				BTUARTSend(TestCode_result, sizeof(TestCode_result));
			}
		}
	}
	return 0;
}

bool Measure(){
	uint32_t ui32Voltage_scale;
	SYS_PAR* par = Get_system_parameter();
	TEST_DATA* TD = Get_test_data();
	//start Measuring
	ST7565ClearScreen(BUFFER1);
	LcdDrawScreen( 35, BUFFER1); // "Testing" screen
	ROM_SysCtlDelay(get_sys_clk()/3);
	ST7565ClearScreen(BUFFER1);
	LcdDrawScreen( 34, BUFFER1);

	if(BattCoreGetCCA(NULL, &TD->Measured_CCA,3)){
		//get test time
		ExtRTCRead( &TD->year, &TD->month, &TD->date, &TD->day, &TD->hour, &TD->min, &TD->sec);

		//get temperature for test recording only, no temperature compensation
		GetTemp(&TD->BodyTestingTemp, &TD->ObjTestingTemp);
		if(TD->ObjTestingTemp <= -20)
		{
			TD->ObjTestingTemp = -20;
		}
		else if(TD->ObjTestingTemp >= 60)
		{
			TD->ObjTestingTemp = 60;
		}

		BattCoreGetVoltage(&ui32Voltage_scale, &TD->battery_test_voltage);

		if(par->WHICH_TEST == Battery_Test){
			BattCoreCCAUnitTransform(&TD->Measured_CCA, par->BAT_test_par.BatteryType, par->BAT_test_par.SelectRating, 0);
			TD->SOC = BattCoreCalSoc((MAP_BATTERYTYPE)par->BAT_test_par.BatteryType, TD->battery_test_voltage, (VOL_CH)TD->SYSTEM_CHANNEL);
			TD->Judge_Result = BatJudgeMap((MAP_BATTERYTYPE)par->BAT_test_par.BatteryType, TD->battery_test_voltage, &TD->Measured_CCA, &TD->SOH, (VOL_CH)TD->SYSTEM_CHANNEL);
		}
		else{ //start-stop
			BattCoreCCAUnitTransform(&TD->Measured_CCA, par->SS_test_par.SSBatteryType, par->SS_test_par.SSSelectRating, pg_JIS[TD->JISCapSelect].itemNum);
			TD->SOC = BattCoreCalSoc((MAP_BATTERYTYPE)par->SS_test_par.SSBatteryType, TD->battery_test_voltage, (VOL_CH)TD->SYSTEM_CHANNEL);
			TD->Judge_Result = BatJudgeMap((MAP_BATTERYTYPE)par->SS_test_par.SSBatteryType, TD->battery_test_voltage, &TD->Measured_CCA, &TD->SOH, (VOL_CH)TD->SYSTEM_CHANNEL);
		}
		return true;
	}
	else{
		return false;
	}
}

bool test_prepare(){
	uint8_t temp;
	SYS_PAR* par = Get_system_parameter();
	TEST_DATA* TD = Get_test_data();
	if(BT_command[0] == 0x01){ //Battery_Test
		par->WHICH_TEST = Battery_Test;
		TD->SetCapacityVal = par->BAT_test_par.BTSetCapacityVal = ((BT_command[2]-0x30) * 1000) + ((BT_command[3]-0x30) * 100) + ((BT_command[4]-0x30) * 10) + ((BT_command[5]-0x30));
	}
	else{
		par->WHICH_TEST = Start_Stop_Test;
		TD->SetCapacityVal = par->SS_test_par.SSSetCapacityVal = ((BT_command[2]-0x30) * 1000) + ((BT_command[3]-0x30) * 100) + ((BT_command[4]-0x30) * 10) + ((BT_command[5]-0x30));
	}


	temp = BT_command[1] / 5; //battery type

	if(temp == 0){
		par->BAT_test_par.BatteryType = FLOODED;
	}
	else if(temp == 1){
		par->SS_test_par.SSBatteryType = EFB;
	}
	else if(temp == 2){
		par->BAT_test_par.BatteryType = VRLA_GEL;
	}
	else if(temp == 3){
		if(par->WHICH_TEST == Battery_Test){
			par->BAT_test_par.BatteryType = AGM_FLAT_PLATE;
		}
		else{
			par->SS_test_par.SSBatteryType = AGM_FLAT_PLATE;
		}
	}
	else if(temp == 4){
		par->BAT_test_par.BatteryType = AGM_SPIRAL;
	}
	else{
		return 1;
	}



	temp = BT_command[1] % 5; // CCA rating

	if(temp == 0){
		return 1;
	}
	else if(temp == 1){
		if(par->WHICH_TEST == Battery_Test){
			par->BAT_test_par.SelectRating = SAE;
		}
		else{
			par->SS_test_par.SSSelectRating = SAE;
		}
	}
	else if(temp == 2){
		if(par->WHICH_TEST == Battery_Test){
			par->BAT_test_par.SelectRating = EN2;
		}
		else{
			par->SS_test_par.SSSelectRating = EN2;
		}
	}
	else if(temp == 3){
		if(par->WHICH_TEST == Battery_Test){
			par->BAT_test_par.SelectRating = DIN;
		}
		else{
			par->SS_test_par.SSSelectRating = DIN;
		}
	}
	else if(temp == 4){
		if(par->WHICH_TEST == Battery_Test){
			par->BAT_test_par.SelectRating = IEC;
		}
		else{
			par->SS_test_par.SSSelectRating = IEC;
		}
	}
	else{
		return 1;
	}
	return 0;
}

void coounter_callback(){
	timeout_counter--;
}
