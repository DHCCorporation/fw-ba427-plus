//*****************************************************************************
//
// Source File: Widgetmgr.c
// Description: Functions definition to construct Widget Manager to build 
//				a complex user interface that includes individual controls such
//				as buttons, sliders...etc. This layer ties the graphical display
//				to the input system. It manages the input and updates the displayed
//				widgets according to presses made by end-user.
//
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 09/21/15     William.H      Created file.
// 10/01/15     William.H      To integrate all the BUTTON_PRESSED event: SELECT_BUTTON/UP_BUTTON/DOWN_BUTTON/LEFT_BUTTON/RIGHT_BUTTON, 
//                             and respective control function into Widget Manager.c file.
// 10/05/15     William.H      Check in BT2020 project which branched from LCDFontMaker(Pre-BT2020) by modifed related GPIOs.
// 10/08/15     William.H      Implement LCD Backlight Setting Menu and add the menu entry into Setting Menu widget.
// 10/19/15     William.H      1. Add TestInVehicleWidgetMsgProc() to handle Up/Down/Enter key event.
//                             2. Enter into "CHKSURFACECHARGE" entry when press right key at "Set Capacity" menu - SetCapRight().
// 10/21/15     Henry.Y        Add VoltageHighWidgetMsgProc() to handle Enter key event.
// 10/22/15     Henry.Y        Add Isita6VBatteryWidgetMsgProc() to handle Enter key event.
// 10/22/15     William.H      Add right key event handle process be identical to click event process in TestInVehicleWidgetMsgProc().
// 10/27/15     William.H      Add BackLightAnimation() funtion which has 10 levels of backlight setting. Each level has its own screen index.
// 10/30/15     William.H      To fix the Internal Battery Low suddenly been presented scenario, then clear menu stack and back to Main Menu immediately will cause state currupt issue. Add SystemErrHandler() function to handle/fix this issue.
// 11/04/15     Vincent.T      Modified max cca setting val(capVal) -> g_MaxCCASet(extern variable)
// 11/18/15     Vincent.T      Let CCA Setting Val eql 0, if virtual keyborad event is on in SetCCA Page.
// 12/07/15     Vincent.T      1. New void DateTimeSetWidgetMsgProc(uint16_t ui16Msg) body;
//                             2. Include extrtcsvc.h
//                             3. Extern global variable blDateTimeSetMode
// 01/06/16     William.H      1. "g_LangSelectHandle" is just the menu up/down selection index, not the extact the final selected language code.
//                             2. "g_CurtSeltdLang" is the current selected language index. 
//                             3. The final language code is " g_LangSet[g_CurtSeltdLang].lanSel" decided by WIDGET_MSG_KEY_SELECT @ LangSelMenuWidgetMsgProc() function.
//                             4. Modified the re-draw menu list sequence upon WIDGET_MSG_KEY_UP / WIDGET_MSG_KEY_DOWN events, to meet the dynamic language set requested by customer.
//                             5. @ WIDGET_MSG_KEY_LEFT event, "g_LangSelectHandle = g_CurtSeltdLang;", this is in order to re-store the selected language handle with anti-white cursor when back to "Language Select" menu.
//                             6. @ WIDGET_MSG_KEY_SELECT event, to deal with radio button status update, and decide the final language code by "UARTprintf("g_LangSet[g_CurtSeltdLang].lanSel = %d\n", g_LangSet[g_CurtSeltdLang].lanSel);".
// 01/04/16     Vincent.T      1. Add TestCounterSetWidgetMsgProc() func. to access test counter
//                             2. #include "TestCounterwidget.h"
// 01/06/16     Vincent.T      1. Add MemoryEraseWidgetMsgProc() func. to Clear Memory
// 01/11/16     Vincent.T      1. #include "VMAMwidget.h"
// 01/19/16     William.H      Add BluetoothSetWidgetMsgProc() func which will be called when 'BLUETOOTHSET' menu entry exist in stack.
// 01/19/16     Vincent.T      1. Modify PrintResultWidgetMsgProc(); up, down selection
// 01/25/16     Vincent.T      1. Only Battery type == Flooded can chose JIS item.
//                             2. Modify code make left event in SetCapNormalWidgetMsgProc(); revocer origin set cca val when click event aren't trigged.
//                             3. modify g_PrintResult = 1.(Default print result: Yes)
//                             4. Save LCD backlight setting
// 01/25/16     Vincent.T      1. New global variable to get battery type of start-stop test
//                             2. New global variable to get CCA rating of start-stop
//                             3. global variable WHICH_TEST
// 01/29/16     Vincent.T      1. Modifiy NUM_JIS_BATTERY_TYPE for Start-stop
//                             2. Modify SelRatingWidgetMsgProc() for Start-stop
//                             3. global variables for JIS: NUM_JIS_BT, NUM_JIS_SS
//                             4. New global variables g_BTJISCapSelect, g_SSJISCapSelect
//                             5. global variables: g_BTSetCapacityVal, g_SSSetCapacityVal
// 02/02/16     Vincent.T      1. Add CrankingVoltWidgetMsgProc() body;
//                             2. #include "CrankingVoltwidget.h"
// 02/03/16     Vincent.T      1. #include "AltIdleVoltWidget.h"
//                             2. #include "AltIdleVoltResultWidget.h"
//                             3. Add AltIdleVoltWidgetMsgProc(), AltIdleVoltResultWidgetMsgProc() body.
// 02/04/16     Vincent.T      1. Add RipVoltResultWidgetMsgProc() body.
//                             2. #include "RipVoltResultwidget.h";
//                             3. #include "TurnOnLoadswidget.h"
//                             4. Add TurnOnLoadsWidgetMsgProc() body.
//                             5. #include "AltLoadVoltwidget.h"
// 02/05/16    Vincent.T       1. Add AltLoadVoltResultWidgetMsgProc() body.
//                             2. Add SysResultWidgetMsgProc() body.
//                             3. #include "SysResultwidget.h"
// 02/15/16     William.H      1. Add RONUMBERVKYBD / VINNUMBERVKYBD enum to process RO and VIN characters input.
//                             2. Add RoVinVkybdWidgetMsgProc() to deal with ket events.
// 03/07/16     Vincent.T      Add PRINT func. in TESTCOUNTERTYPE.
// 03/09/16     Vincent.T      1. global variables blPrintError, blNoPaper
//                             2. Add NoPaperWidgetMsgProc() body.
//                             3. #include "NoPaperwidget.h"
// 03/24/16     Vincent.T      1. New global variable to get language index.
//                             2. #include "..\service/flashsvc.h"
//                             3. Modify SettingMenuWidgetMsgProc(); to handle multination language case.
// 03/25/16     Vincent.T      1. Modify LcdBackLightSetWidgetMsgProc(); to handle multination languate case.
//                             2. Modify DateTimeSetWidgetMsgProc(); to handle multination language case.
//                             3. Modify MemoryEraseWidgetMsgProc(); to handle multination language case.
//                             4. Modify TestCounterSetWidgetMsgProc(); to handle multination language case.
//                             5. Modify TestCounterConfirmSetWidgetMsgProc(); to handle multination language case.
//                             6. Modify BatTypeSelWidgetMsgProc(); to handle multination language case.
// 03/28/16     Vincent.T      1. Modify SetCapNormalWidgetMsgProc(); to handle multination language case.
//                             2. Modify JISCapSelNormalWidgetMsgProc(); to handle multination language case.
// 03/28/16     Vincent.T      1. Modify SelRatingWidgetMsgProc(); to handle multination language case.
//                             2. Modify JISCapSelNormalWidgetMsgProc(); to handle multination language case.
//                             3. Modify JISCapSelVkybdWidgetMsgProc(); to handle multination language case.
// 03/29/16     Vincent.T      1. Modify JISCapSelVKybd123Left(); to handle multination language case.
//                             2. Modify SetCapVKybdWidgetMsgProc(); to handle multination language case.
// 03/30/16     Vincent.T      1. Modify TestInVehicleWidgetMsgProc(); to handel multination language case.
//                             2. Modify VoltageHighWidgetMsgProc(); to handle multination language case.
//                             3. Modify IsIta6VBatteryWidgetMsgProc(); to handle multination language case.
//                             4. Modify IsBatteryChargedWidgetMsgProc(); to handle multination language case.
//                             5. Modify DisplayResultWidgetMsgProc(); to handle multination language case.
//                             6. global variables:Measured_CCA, SOH, SOC, Judge_Result&gfMeasuredVolt.
//                             7. #include "..\driver/smallfonts.h"
// 03/31/16     Vincent.T      1. Modify RoVinVkybdWidgetMsgProc(); to handle multination language case.
//                             2. Modify PrintResultWidgetMsgProc(); to handle multination language case.
//                             3. Modify NoPaperWidgetMsgProc(); to handle multination language case.
//                             4. Modify SysResultWidgetMsgProc(); to handle multination language case.
//                             5. global variables gfCrankVol, gfIdleVol, gfripple and gfLoadVol.
//                             6. global variables gui8CrankVResult, gui8AltIdleVResult, gui8RipVResult and gui8AltLoadVResult.
//                             7. global variable SYSTEM_CHANNEL
// 04/06/16     Vincent.T      1. Modify TestCounterConfirmClick(); to handle multination language case.
//                             2. Modify MainMenuWidgetMsgProc(); to handle multination language case.
//                             3. Modify LangSelMenuWidgetMsgProc(); to handle multination language case.
//                             4. Modify InfoVKybdWidgetMsgProc(); to handle multination language case.
//                             5. Modify BluetoothSetWidgetMsgProc(); to handle multination language case.
//                             6. Modify CheckClampsWidgetMsgProc(); to handle multination language case.
//                             7. Modify CheckIntBatWidgetMsgProc(); to handle multination language case.
//                             8. Modify VMAMWidgetMsgProc(); to handle multination language case.
// 04/07/16      Vincent.T     1. Save Language Setting.
//                             2. Modify SettingMenuLeft(); to handle multination language case.
// 04/08/16      Vincent.T     1. Move Test Counter into Tools menu(Modify SettingMenuWidgetMsgProc();)
// 04/12/16      Vincent.T     1. Add IRTestResultWidgetMsgProc(); to handle IR test result.
// 04/13/16      Vincent.T     1. Global variable IRvoltage, fIR.
// 04/14/16      Vincent.T     1. #include "IRTestResultwidget.h"
//                             2. #include "IRRechargeRetestwiget.h"
//                             3. #include "LoadErrorwidget.h"
// 04/15/16      Vincent.T     1. Add IRRechargeRetestWidgetMsgProc(); to handle "Recharge & Retest" event.
//                             2. Add LoadErrorWidgetMsgProc; to handle "Load Error" event.
//                             3. Re-arrange all select & right btn event.
// 04/27/16      Vincent.T     1. global variable battery_test_voltage
//                             2. global variables: gui16SSTestCounter, gui16BatTestCounter, gui16SysTestCounter and gui16TotalTestRecord
//                             3. Modify DisplayResultWidgetMsgProc(); to save test parameters.
// 05/20/16      Vincent.T     1. Remove testcounter function in DisplayResultWidgetMsgProc();
//                             2. Remove global variables declaraction:
//                                - extern uint16_t gui16SSTestCounter;
//                                - extern uint16_t gui16BatTestCounter;
//                                - extern uint16_t gui16SysTestCounter;
//                                - extern uint16_t gui16TotalTestRecord;
// 05/24/16      Vincent.T     1. Modify SysResultWidgetMsgProc(); due to boundaries of  ALT. IDLE VOLT. and ALT. LOADS VOLT. are changed.
//                             2. global variable gbSysTemp
// 06/14/16      Vincent.T     1. Detect internal battery voltage when printing. If internal less than 8v, lock the tester.
// 06/21/16      Vincent.T     1. #include "PointToBatwidget.h"
//                             2. Add PointToBatWidgetMsgProc(); to handle "Point to battery" event.
//                             3. reset blFT variable when errors occurs.
//                             4. Modify JISCapSelNormalWidgetMsgProc(); to access correct JIS battery capacity.(Start-Stop Test)
//                             5. Modify DisplayResultWidgetMsgProc();, SysResult and IRresult funcs to avoid btn event causes unexpect screen content.(related blFT variable)
// 06/24/16      Vincent.T     1. All measured CCA rating changed to CCA.
//                             2. All are ratings allowed to access JIS batterys.
// 08/10/16      Vincent.T     1. Make test result information display on same page.(only Halfwidth Froms)
// 09/06/16      Vincent.T     1. Modify TestInVehicleWidgetMsgProc(); up/down can be cycled.
//                             2. Modify SysResultWidgetMsgProc(); to show system test result(not bar graph)
// 10/13/16      Ian.C         1. Modify LangSelMenuWidgetMsgProc(). Add LangSelMenuDrawPageInfo(); to draw page information at bottom right.
// 11/15/16      Ian.C         1. Modify DisplayResultWidgetMsgProc(); Change the position of SOH, SOC, CCA and Voltage.
// 01/13/17      Vincent.T     1. Modify g_LcdBacklightConfig default value 50 -> 80
//                             2. Modify g_SetCapacityVal, g_BTSetCapacityVal  and g_SSSetCapacityVal default value 25 -> 500
//                             3. Modify g_RoVinVKybdChar default value 0 -> 12
//                             4. Modify SetCapNormalWidgetMsgProc(); due to "set cca capacity every step from 1 to 5"
//                             5. Modify TestCounterSetWidgetMsgProc(); due to add IR Test Counter
//                             6. Modify SysResultWidgetMsgProc(); Make test report "good looking".
// 01/19/17     Vincent.T      1. Modify TestCounterSetWidgetMsgProc(); due to save IR Test Record
//                             2. Modify SysResultWidgetMsgProc(); remove High/Low/... due to multi-language issue.
// 03/08/17     Vincent.T      1. Modify RoVinVkybdWidgetMsgProc(); due to more characters are added.
// 03/27/17     Vincent.T      1. Modify RoVinVkybdWidgetMsgProc():(g_RoVinVKybdChar === 23) -> (g_RoVinVKybdChar >= 23)
// 06/01/17     Henry          FW version: BT2100_WW_V01.a
//                             1. Modify global variables initial value: g_SetCapacityVal/g_BTSetCapacityVal/g_SSSetCapacityVal.
// 09/26/17		Jerry.W		   1. remove unused variable
//							   2. add Widget_routine function
// 09/30/17		Jerry.W		   1. Modify Widget_routine(), disable timer, if internal battery low or clamp error event occure
// 11/06/17     Henry.Y	       1. Modify BattCoreChkClamp()
//                             2. Add execption handling function LoadErrorHandler()
// 11/14/17     Jerry.W        1. Add variables(g_SetRCVal,g_SSSetRCVal,g_BTSetRCVal) for RC setting and measuring
// 11/22/17     Henry.Y        Modify g_ui8TestCode[] length
// 11/23/17     Jerry.W        1. modify Widget_init for mode choosing
// 02/14/18     Henry.Y        global variables:g_SS_JIS_EFB_Select,g_SS_JIS_AGM_Select; change 6V selection initial value to 'YES';
// 03/22/18     William.H      Add g_SelClampConnMeth & g_SelCarModel global varaibles.
// 03/23/18     Henry.Y        virtual key operation enabled by #define VIRTUAL_KEY
// 05/18/18     Jerry.W        add message functionality.
// 09/03/18     Henry.Y        global variable g_SelClampConnMeth merged with TEST_DATA*.
// 09/11/18     Henry.Y        Add Main_routine_switch/get_routine_switch. Increase widget stack size to 20.
//                             Use system error flag to replace the global variables.
//                             Use systeminfo data struct to replace the global variables.
// 11/19/18     Henry.Y        (Jerry) Message add UTF8 type. records_upload_cb() add MFC_upload backlight setup.
// 11/27/18     Jerry.W        modify for the change of UTF8 service.
// 03/20/19     Henry.Y        Add new customized printing flow: print result flow and printing data covered after reset.
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/app/Widgetmgr.c#1 $
// $DateTime: 2017/11/27 10:38:22 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************


#include "Widgetmgr.h"


#define WIDGET_STACK_SIZE	20



//*****************************************************************************
//
// Declare Bluetooth enabled/disabled condition.
//
//*****************************************************************************
bool g_BluetoothEnabled = false;






//*****************************************************************************
//
// Declare Set Capacity Menu's Virtual Keyboard digit select result.
//
//*****************************************************************************
int8_t g_SetCapVKybdDigit = 0;




//*****************************************************************************
//
// Declare "Print result?" yes/no selection result.
//
//*****************************************************************************
uint8_t g_PrintResult = 1;

//*****************************************************************************
//
// Declare "Is battery charged?" yes/no selection result.
//
//*****************************************************************************
uint8_t g_IsBatCharged = 0;



//*****************************************************************************
//
// Declare 'Repair Order & VIN' Virtual Keyboard Character selection result.
//
//*****************************************************************************
uint8_t g_RoVinVKybdChar = 12;

//*****************************************************************************
//
// Declare 'OK' key erased status.
//
//*****************************************************************************
bool g_OkKeyErased = false;

//*******************************************************
//
//To know the CCA Setting Value need to clear or not
//
//*******************************************************
bool blClrCCAVal = true;

//*****************************************************************************
//
// Declare software key control.
//
//*****************************************************************************
bool up = false;
bool down = false;
bool right = false;
bool left = false;
bool enter = false;
bool test[4] = {false};



//*****************************************************************************
//
// Declare software timer callback.
//
//*****************************************************************************
void Soft_timer_cb(void);

//*****************************************************************************
//
// Declare routine on/off flag: disable[0]/enable[1]
//
//*****************************************************************************
static uint8_t M_routine_switch = 1;


extern widget* current_W;
WIDGET_STATE Widget_s = widget_normal;

char message_buf[MESSAGE_TOTAL_LENGTH];

tContext g_sContext;
extern tDisplay g_sST7565V128x64x1;


widget* init_Widget[] ={
        &MAINMENU_widget,
        &WiFiSet_widget,
        &engmode_widget,
        &Scanner_widget,
};


widget* ERROR_Widget[] ={
        &ChkIntBat_widget,
        &CheckClamps_widget,
};



widget* pri_W[WIDGET_STACK_SIZE] = {0};
uint8_t pri_W_index = 0;

void Move_widget(widget* new_w,uint8_t parameter){

    if(pri_W_index == WIDGET_STACK_SIZE){
        UARTprintf("WIDGET OVERFLOW : %d\n",pri_W_index);
        return;
    }

    if(current_W != NULL){
        UARTprintf("[W_move] from %s to %s\n", current_W->name, new_w->name);

        UARTprintf("pri_W_index = %d",pri_W_index);
        pri_W[pri_W_index] = current_W;
        pri_W_index++;
        UARTprintf(" pri_W_index++ = %d",pri_W_index);
    }

    current_W = new_w;
    new_w->parameter = parameter;

    if(Widget_s == widget_message_show) Widget_s = widget_normal;

    if(new_w->initial){
        new_w->initial();
    }

    if(new_w != current_W) return;	//in case move widget in initial()

    if(new_w->display){
        new_w->display();
    }
    if(new_w->service){
        new_w->service();
    }
}

void Move_widget_untracked(widget* new_w,uint8_t parameter){
    UARTprintf("pri_W_index = %d",pri_W_index);

    UARTprintf("[W_move] from %s to %s\n", current_W->name, new_w->name);

    current_W = new_w;
    new_w->parameter = parameter;

    if(Widget_s == widget_message_show) Widget_s = widget_normal;

    if(new_w->initial){
        new_w->initial();
    }

    if(new_w != current_W) return;	//in case move widget in initial()

    if(new_w->display ){
        new_w->display();
    }
    if(new_w->service){
        new_w->service();
    }
}

void Return_pre_widget(){
    UARTprintf("pri_W_index = %d",pri_W_index);

    UARTprintf("[W_return] from %s to %s\n", current_W->name, pri_W[pri_W_index-1]->name);

    if(pri_W_index){
        current_W = pri_W[pri_W_index-1];
        pri_W_index--;
        DEBUG_PRT("pri_W_index-- = %d",pri_W_index);
        if(current_W->display){
            current_W->display();
        }
        if(current_W->service){
            current_W->service();
        }
    }
}


void Widget_init(uint8_t mode){
    UARTprintf("mode = %d\n",mode);
    Widget_s = widget_normal;
    SYS_INFO* info = Get_system_info();
    TEST_DATA* TD = Get_test_data();

    pri_W_index = 0;

    switch(mode){
    case WiFiSet:
        Move_widget_untracked(&WiFiSet_widget,0);
        break;
    case engineer_mode:
        Move_widget_untracked(&engmode_widget,0);
        return;
    case pre_tested:
        set_system_mode(manual_test);
        Move_widget_untracked(&Scanner_widget, 2);
        break;
    case USB_mode:
        Move_widget_untracked(&USB_widget, 0);
        return;
    case MFC:
        Move_widget_untracked(&MFC_widget, 0);
        return;
    case pre_printed:
        set_system_mode(manual_test);
        if(TD->SYSTEM_CHANNEL == MAP_24V){
            Widget_init(manual_test);
            return;
        }
        Move_widget_untracked(&PrintResult_widget, 0);
        break;
    case bootup_check:
        Move_widget_untracked(&bootup_widget, 0);
        break;
    case manual_test:
    default :
        //
        // clear test state
        //
        if(info->pre_test_state == 1){
            info->pre_test_state = 0;
            save_system_info(&info->pre_test_state, sizeof(info->pre_test_state));
        }
        Move_widget_untracked(&MAINMENU_widget,0);
        break;
    }

    UARTprintf("pri_W_index reset\n");
}

void Main_routine_switch(uint8_t routine_switch){
    UARTprintf("\n\nM_routine_switch = %d\n\n", routine_switch);
    M_routine_switch = routine_switch;
}

uint8_t get_routine_switch(){
    return M_routine_switch;
}


void Widget_routine(){
    uint32_t ui32scale;
    float fvoltage;
//    static uint8_t btn_ct = 0;

    if(M_routine_switch){
        if(BattCoreChkIntBatt(&ui32scale, &fvoltage) == 1){
            ROM_TimerDisable(TIMER0_BASE, TIMER_A); // prevent form callback, if internal Battery low event occure
            Move_widget(&ChkIntBat_widget, 0);
            return;
        }

        BattCoreGetVoltage(&ui32scale, &fvoltage);
        
        if(fvoltage < check_clamp_v){
            if((Clamp_err & get_system_error()) == 0) set_system_error(Clamp_err);

//            if(g_bENTERON && !loc_timer){
//                counter = SOFT_T_IDLE_COUNTER;
//                loc_timer = register_timer(10,Soft_timer_cb);
//                //UARTprintf("Voltage low Soft timer register, initial timer counter = %d", counter);
//                start_timer(loc_timer);
//            }

            if((current_W != &CheckClamps_widget) && !g_bENTERON && !(enter|up|down|left|right)){
                ROM_TimerDisable(TIMER0_BASE, TIMER_A); // prevent form callback, if clamp error event occure
                Move_widget(&CheckClamps_widget, 0);
                return;
            }
        }
        else{
            if(Clamp_err & get_system_error()) clear_system_error(Clamp_err);
//            if(loc_timer){
//                //UARTprintf("External voltage exist, Soft timer unregister");
//
//                unregister_timer(loc_timer);
//                loc_timer = NULL;
//            }
        }

//        if(g_bENTERON && !MAP_GPIOPinRead(BUTTONS_GPIO_BASE, SELECT_BUTTON)){
//            //
//            // LONG PRESS DETECT
//            //
//            if(!button_timer){
//                btn_ct++;
//                if(btn_ct > 10){
//                    btn_ct = 0;
//
//                    button_counter = SOFT_T_BUTTON_COUNTER;
//                    button_timer = register_timer(10,Soft_timer_cb);
//                    //UARTprintf("Button Soft timer register, initial timer counter = %d", counter);
//                    start_timer(button_timer);
//                }
//            }
//        }
//        else{
//            btn_ct = 0;
//            if(button_timer){
//                unregister_timer(button_timer);
//                button_timer = NULL;
//            }
//        }

//        if(dummy_system_off){
//            if(loc_timer){
//                unregister_timer(loc_timer);
//                loc_timer = NULL;
//            }
//            if(button_timer){
//                unregister_timer(button_timer);
//                button_timer = NULL;
//            }
//            ST7565ClearScreen(BUFFER0);
//            ST7565Refresh(BUFFER0);
//            PWMDutySet(0);
//            IntBat_SW_OFF;
//            ROM_IntMasterDisable();
//            while(1);//just in case
//        }
        delay_ms(10);
    }
//    else{
//        btn_ct = 0;
//        if(loc_timer){
//            unregister_timer(loc_timer);
//            loc_timer = NULL;
//        }
//        if(button_timer){
//            unregister_timer(button_timer);
//            button_timer = NULL;
//        }
//
//    }



    if(get_widget_mode() != widget_message_show){
        if(current_W->routine){
            current_W->routine();
        }
    }

}

void Widget_traversal(widget * Wid, uint8_t n, uint8_t layer){
    uint8_t i,j;

    if(Wid->tra_index == n) return;

    Wid->tra_index = n;

    for(i = 0 ; i < layer*2; i++){
        UARTprintf(" ");
    }

    UARTprintf("Layer%d, Widget:%s{\n", layer, Wid->name);

    for(i = 0; i < Wid->number_of_widget ; i++){
        for(j = 0 ; j < layer*2; j++){
            UARTprintf(" ");
        }
        UARTprintf("     %s,\n", Wid->subwidget[i]->name);
    }

    for(i = 0 ; i < layer*2; i++){
        UARTprintf(" ");
    }
    UARTprintf("}\n\n");

    for(i = 0; i < Wid->number_of_widget ; i++){
        Widget_traversal(Wid->subwidget[i],  n, layer+1);
    }
    delay_ms(1);
}

void Widget_scan(){
    static uint8_t scan_time = 1;
    uint8_t i;

    UARTprintf("\n\n################\n");
    UARTprintf("#Widget scanner#\n");
    UARTprintf("################\n\n");

    for(i = 0; i < sizeof(init_Widget)/4 ; i++){
        Widget_traversal(init_Widget[i], scan_time, 0);
    }

    for(i = 0; i < sizeof(ERROR_Widget)/4 ; i++){
        Widget_traversal(ERROR_Widget[i], scan_time, 0);
    }

    scan_time++;


}


WIDGET_STATE get_widget_mode(){
    return Widget_s;
}

void set_widget_mode(WIDGET_STATE state){
    Widget_s = state;
}

uint8_t message_current_line = 0;
bool message_has_next_line = false;


void message(const char *mesag){
    uint32_t len;
    len = UTF8_str_size(mesag);
    if(len >= MESSAGE_TOTAL_LENGTH){
        len = MESSAGE_TOTAL_LENGTH-1;
    }
    memcpy(message_buf,mesag , len);
    message_buf[len] = '\0';
    Widget_s = widget_message_wait;
    message_current_line = 0;
}


uint8_t get_line(char *buf, char *string, uint8_t length){
    char* start_line = string, *end_line, *last_end_line;

    while(*start_line == ' ') start_line++;
    if(*start_line == '\n'){
        *buf = '\0';
        return start_line-string+1;
    }
    if(*start_line == '\0'){
        *buf = '\0';
        return start_line-string;
    }

    end_line = start_line;
    do{
        last_end_line = end_line;

        if(*end_line == '\0' || *end_line == '\n'){
            break;
        }
        else if(*end_line == ' '){
            end_line++;
        }
        while(*end_line != ' ' && *end_line != '\n' && *end_line != '\0') end_line++;
    }while((end_line - start_line) <= length);

    memcpy(buf, start_line, last_end_line - start_line);
    buf[last_end_line - start_line] = '\0';
    return last_end_line - string+1;
}




void message_display(uint8_t start_line){
    tRectangle sRect;
    uint32_t hight;
    //uint32_t width;
    uint8_t i;
    uint32_t len, idx = 0;
    char buf[MESSAGE_LENGTH+1];
    uint8_t screen_width;

    //width = GrContextDpyWidthGet(&g_sContext);
    hight = GrContextDpyHeightGet(&g_sContext);

    sRect.i16XMin = 6;
    sRect.i16YMin = 5;
    sRect.i16XMax = 121;
    sRect.i16YMax = hight - 5;
    screen_width = sRect.i16XMax - sRect.i16XMin - 7;

    GrContextForegroundSet(&g_sContext, 0);
    GrRectFill(&g_sContext, &sRect);
    GrContextForegroundSet(&g_sContext, 0xFFFF);
    GrRectDraw(&g_sContext, &sRect);
    sRect.i16XMin += 2;
    sRect.i16YMin += 2;
    sRect.i16XMax -= 2;
    sRect.i16YMax -= 2;
    GrRectDraw(&g_sContext, &sRect);

    len = UTF8_str_size(message_buf);

    for(i = 0; i < start_line && idx < len ; i++){
        idx += get_line_UTF8(buf, message_buf+idx, screen_width,0,0,0, font_12 ,1);
    }

    for(i = 0; i < 4 && idx < len ; i++){
        uint16_t new_idx = idx;

        if(i == 3){
            if(start_line != 0){
                new_idx += get_line_UTF8(buf, message_buf+new_idx, screen_width -12,0,0,0, font_12,1);	//for the space of up arrow
            }
            else{
                new_idx += get_line_UTF8(buf, message_buf+new_idx, screen_width,0,0,0, font_12,1);
            }

            if(new_idx < len){
                message_has_next_line = true;
                new_idx = idx;
                new_idx += get_line_UTF8(buf, message_buf+new_idx, screen_width - 12,0,0,0, font_12,1);
            }
        }
        else{
            new_idx += get_line_UTF8(buf, message_buf+new_idx, screen_width,0,0,0, font_12,1);
            message_has_next_line = false;
        }

        idx = new_idx;


        UTF8_printf(10, 8 + i*12, buf);
    }

}


void message_roll(MESSAGE_ROLL up_down){
    if(up_down == message_up){
        if(message_current_line != 0){
            message_current_line--;
            message_display(message_current_line);
        }
    }
    else if(up_down == message_down){
        if(message_has_next_line){
            message_current_line++;
            message_display(message_current_line);
        }
    }
}

void message_routine(){
    static uint16_t count = 0;
    XY_DISPLAY display;
    if(count == 600){
        count++;
        if(message_current_line != 0 && message_has_next_line){
            char buf[3] = {127, 128, 0};
            display.string = buf;
            display.x = 106;
            display.y = 49;
        }
        else if(message_has_next_line){
            char buf[2] = { 127, 0};
            display.string = buf;
            display.x = 112;
            display.y = 49;
        }
        else if(message_current_line != 0){
            char buf[2] = { 128, 0};
            display.string = buf;
            display.x = 112;
            display.y = 49;
        }
        else{
            return;
        }
        ST7565printf5x8(&display);
    }
    else if(count == 1200){
        count = 0;
        if(message_current_line != 0 || message_has_next_line){
            char buf[3] = {' ', ' ', 0};
            display.string = buf;
            display.x = 106;
            display.y = 49;
            ST7565printf5x8(&display);
        }
    }
    else{
        count++;
    }
}

#define VIRTUAL_KEY

void keybaord(){

#ifdef VIRTUAL_KEY
    //up = left = down = right = enter = false;
    static uint8_t command = false;
    static char command_buf[200];
    static uint8_t command_index;
    uint8_t temp;

    if(UARTRxBytesAvail()){
        temp = UARTgetc();
        if(command){
            command_buf[command_index] = temp;
            if(command_buf[command_index] == 0x0d || command_index == 199){
                command_buf[command_index] = '\0';
                if(command == '+'){
                    uint32_t lan = get_language_setting();
                    lan++;
                    lan %= number_total_language;
                    set_language((LANGUAGE)lan);
                    if(current_W->display){
                        current_W->display();
                    }
                }

                if(command == '-'){
                    message(command_buf);
                    uint32_t lan = get_language_setting();
                    if(lan != 0){
                        lan--;
                    }
                    else{
                        lan = (number_total_language-1);
                    }
                    set_language((LANGUAGE)lan);
                    if(current_W->display){
                        current_W->display();
                    }
                }

                command = 0;
            }
            command_index++;
        }
        else{
            switch(temp){
            case 'w' :
                up = true;
                break;

            case 'a' :
                left = true;
                break;

            case 'x' :
                down = true;
                break;

            case 'd' :
                right = true;
                break;

            case 's' :
                enter = true;
                break;
            case '!' :
                test[0] = true;
                break;
            case '@' :
                test[1] = true;
                break;
            case '#' :
                test[2] = true;
                break;
            case '$' :
                test[3] = true;
                break;
            case 'r':
                ROM_SysCtlReset();
                while(1);
            case '+' :
            case '-' :
                command = temp;
                command_index = 0;
                memset(command_buf, 0, 40);
                break;
            }
        }
    }
#endif
}





void (*TIMER0A_sec_callback)(void) = 0;

void Timer0AIntHandler(void)
{

    // Clear the timer interrupt
    ROM_TimerIntClear(TIMER0_BASE, TIMER_TIMA_TIMEOUT);

    if( g_15SecCDCounter!= -1) g_15SecCDCounter--;





    if(TIMER0A_sec_callback){
        TIMER0A_sec_callback();
    }


    if(g_15SecCDCounter == 0)
    {
        //
        //	Disable Timer0A
        //
        ROM_TimerDisable(TIMER0_BASE, TIMER_A);
        TIMER0A_sec_callback = 0;
        //g_15SecCDCounter = 15; // This line will trigger "ROM_TimerEnable(TIMER0_BASE, TIMER_A);" again @ if(menuLvl == COUNTDOWN15SEC).
    }
}



void set_countdown_callback(void (*sec_callback)(void), int16_t sec){
    g_15SecCDCounter = sec;
    TIMER0A_sec_callback = sec_callback;
    ROM_TimerEnable(TIMER0_BASE, TIMER_A);
}

void LoadErrorHandler(void)
{
    CCA_load_1_OFF;
    CCA_load_2_OFF;
    CCA_load_1_protect_ON;
    CCA_load_2_protect_ON;
}

#if 0
void
TestCodeGenerater(uint8_t *pui8TestCode, float fvol, uint16_t ui16TestCCA, uint16_t ui16SetCCA, uint8_t ui8TestResult, float fTemp, uint8_t ui8IOVehicle, uint8_t ui8Inventory, uint8_t ui8TestType, uint8_t ui8Day, uint8_t ui8Month, uint8_t *strRO, uint8_t *strVin, uint8_t ui8FWver)
{
    float fBuf;
    uint8_t ui8Dat_pre_buf;
    uint8_t ui8Datbuf;
    uint8_t l;

    //
    // digit 1: fvol
    //
    ui8Datbuf = (fvol < 5.00)? 0:(fvol*10 - 50);
    pui8TestCode[0] = (ui8Datbuf>>2)&(0x1F);
    //
    // digit 2: fvol + ui16TestCCA
    //
    ui8Dat_pre_buf = ui8Datbuf;
    ui8Datbuf = (ui16TestCCA > 2550)? 255:(ui16TestCCA/10);
    pui8TestCode[1] = ((ui8Dat_pre_buf<<3)&(0x18))|((ui8Datbuf>>5)&(0x07));
    //
    // digit 3: ui16TestCCA
    //
    pui8TestCode[2] = (ui8Datbuf) & (0x1F);
    //
    // digit 4: ui16SetCCA
    //
    ui8Datbuf = (ui16SetCCA > 2550)? 255:(ui16SetCCA/10);
    pui8TestCode[3] = (ui8Datbuf>>3) & (0x1F);
    //
    // digit 5: ui16SetCCA + ui8TestResult
    //
    ui8Dat_pre_buf = ui8Datbuf;
    ui8Datbuf = (ui8TestResult == TR_BADnREPLACE)? 3:
            (ui8TestResult == TR_RECHARGEnRETEST)? 2:
                    (ui8TestResult == TR_BAD_CELL_REPLACE)? 4:ui8TestResult;

    pui8TestCode[4] = ((ui8Dat_pre_buf<<2)&(0x1C))|((ui8Datbuf>>1)&(0x03));
    //
    // digit 6: ui8TestResult + fTemp
    //
    ui8Dat_pre_buf = ui8Datbuf;
    fBuf = (fTemp*9)/5 + 32;
    ui8Datbuf = (fBuf > 0)? ((fBuf+20)/5):0;
    pui8TestCode[5] = ((ui8Dat_pre_buf<<4)&(0x10))|((ui8Datbuf>>1)&(0x0F));
    //
    // digit 7: fTemp + ui8IOVehicle + ui8Inventory + ui8TestType
    //
    ui8Dat_pre_buf = ui8Datbuf;
    ui8Datbuf = (ui8TestType<1)? ui8TestType:0;
    pui8TestCode[6] = ((ui8Dat_pre_buf<<4)&(0x10))|((!ui8IOVehicle<<3)&(0x08))|((ui8Inventory<<1)&(0x06))|((ui8Datbuf>>1)&(0x01));
    //
    // digit 8: ui8TestType + ui8Day
    //
    ui8Dat_pre_buf = ui8Datbuf;
    ui8Datbuf = ui8Day;
    pui8TestCode[7] = ((ui8Dat_pre_buf<<4)&(0x10))|((ui8Datbuf>>1)&(0x0F));
    //
    // digit 9: ui8Day + ui8Month
    //
    ui8Dat_pre_buf = ui8Datbuf;
    ui8Datbuf = ui8Month;
    pui8TestCode[8] = ((ui8Dat_pre_buf<<4)&(0x10))|((ui8Datbuf)&(0x0F));
    //
    // digit 10: *strRO + *strVin
    //
    ui8Datbuf = 0;

    for(l=0;l<6;l++){
        ui8Datbuf += ((strRO[l] + strVin[l] - 0x60));
    }

    pui8TestCode[9] = (ui8Datbuf%32)&(0x1F);
    //
    // digit 11: strFWver + unused(0)
    //
    ui8Datbuf = (ui8FWver>0x5A)? (ui8FWver-0x21)%8:(ui8FWver-1)%8;
    pui8TestCode[10] = (ui8Datbuf<<2)&(0x1C);

    //7-DIGIT ENCODE
    for(ui8Datbuf = 0; ui8Datbuf < 11; ui8Datbuf++)
    {
        pui8TestCode[ui8Datbuf] = (pui8TestCode[ui8Datbuf]>9)? pui8TestCode[ui8Datbuf] + 0x37 : pui8TestCode[ui8Datbuf] + 0x30;
        if(pui8TestCode[ui8Datbuf] >= 'I'){
            pui8TestCode[ui8Datbuf] += 1;
            if(pui8TestCode[ui8Datbuf] >= 'O')
                pui8TestCode[ui8Datbuf] += 1;
        }
    }

}
#endif

void TestCodeGenerater(uint8_t *pui8TestCode, uint8_t len, TESTCODE_T* data)
{
    uint32_t temp;
    uint32_t date;
    uint32_t i;
    if(len > TESTCODE_LEN){
        while(1);
    }
    
    // Voltage limitation
    if(data->fvol >= 20.47) data->fvol = 20.47;
    // CCA limitation
    if(data->ui16SetCCA >= 3600) data->ui16SetCCA = 3600;
    if(data->ui16TestCCA >= 3600) data->ui16TestCCA = 3600;
    // Rating limitation
    if(data->ui8CCARating >= SELRATING_num || data->ui8CCARating == JIS){
        while(1);
    }
    // Temperature limitation
    /*
    if(data->fTemp >= 60)
    {
        data->fTemp = 60;
    }
    else if(data->fTemp <= -20)
    {
        data->fTemp = -20;
    }
    */
    // YYYY/MM/DD limitation
    if(data->ui8Day >= 31) data->ui8Day = 31;
    if(data->ui8Month >= 12) data->ui8Month = 12;
    if(data->ui16Year >= 2099) data->ui16Year = 2099;    

    data->ui16SetCCA = data->ui16SetCCA / 5;
    data->ui16TestCCA = data->ui16TestCCA;
    //data->fTemp = data->fTemp + 20;
    data->ui16Year = data->ui16Year - 2020;

    uint8_t pre_code[8] = {0};
    uint64_t pre_code36;

    uint16_t vol = 0;
    uint16_t setcca = 0;
    uint16_t testcca = 0;
    uint8_t result = 0;
    uint8_t value = 0;

    uint8_t l = 0;
    uint8_t shift_num = 0;

    srand(time(NULL));

    // tested CCA
    testcca = data->ui16TestCCA;
    pre_code[0] = testcca & 0xff;
    pre_code[1] = testcca >> 8;

    // set CCA
    setcca = data->ui16SetCCA;

    pre_code[1] = ((setcca & 0x000f) << 4) + pre_code[1];
    pre_code[2] = (setcca >> 4) & 0x00ff;

    //*************************
    // Voltage
    //*************************
    vol = data->fvol * 100;
    pre_code[3] = vol & 0x00ff;
    pre_code[4] = (vol >> 8) & 0x00ff;

    //****************************
    // result
    //****************************
    result = data->ui8TestResult;
    pre_code[4] = (result << 4) + pre_code[4];
    //
    // #1st random num
    //
    //  srand(gfMeasuredVolt);
    shift_num = rand()%36;
    //
    //#1st randon num to shift left to precode[]
    //
    value=0;

    for(l=0; l<shift_num; l++)
    {
        vol = 0;
        vol = pre_code[0];
        vol = vol << 1;
        pre_code[0] = vol & 0x00ff;
        value = (vol >> 8) & 0x000f;
        //------------------------//
        vol = 0;
        vol = pre_code[1];
        vol = vol << 1;
        pre_code[1] = (vol & 0x00ff) | value;
        value = (vol >> 8) & 0x000f;
        //------------------------//
        vol = 0;
        vol = pre_code[2];
        vol = vol << 1;
        pre_code[2] = (vol & 0x00ff) | value;
        value = (vol >> 8) & 0x000f;
        //------------------------//
        vol = 0;
        vol = pre_code[3];
        vol = vol << 1;
        pre_code[3] = (vol & 0x00ff) | value;
        value = (vol >> 8) & 0x000f;
        //------------------------//
        vol = 0;
        vol = pre_code[4];
        vol = vol << 1;
        pre_code[4] = (vol & 0x00ff) | value;
        value = (vol >> 8) & 0x000f;
        pre_code[0] = pre_code[0] | value;
    }
    //
    //pre_code[] % 36 => Test_Code
    //
    //=====20200221 DW EDIT ========//
    if(data->fTemp > 0)
    {
        temp = 1;
    }
    else
    {
        temp = 2;
    }

    temp <<= 24;

    date = data->ui16Year;
    date *= 12;
    date += data->ui8Month-1;
    date *= 31;
    date += data->ui8Day-1;
    date *= 24;
    date += data->hour;
    date *= 60;
    date += data->minute;

    date += temp;

    for(i = 0 ; i < 5 ; i++){
        pui8TestCode[15-i] = ASCII2TestCode(date % 36);
        date /= 36;
    }

    memcpy(&pre_code36, pre_code, sizeof(pre_code));

    pui8TestCode[10] = ASCII2TestCode(pre_code36 % 36);
    pre_code36 /= 36;
    pui8TestCode[9] = ASCII2TestCode(pre_code36 % 36);
    pre_code36 /= 36;
    pui8TestCode[8] = ASCII2TestCode(pre_code36 % 36);
    pre_code36 /= 36;
    pui8TestCode[7] = ASCII2TestCode(pre_code36 % 36);
    pre_code36 /= 36;
    pui8TestCode[5] = ASCII2TestCode(pre_code36 % 36);
    pre_code36 /= 36;
    pui8TestCode[4] = ASCII2TestCode(pre_code36 % 36);
    pre_code36 /= 36;
    pui8TestCode[3] = ASCII2TestCode(pre_code36 % 36);
    pre_code36 /= 36;
    pui8TestCode[1] = ASCII2TestCode(pre_code36 % 36);
    //------------------------//
    pui8TestCode[0] = ASCII2TestCode(shift_num);
    //
    // #2ed random num
    //
    //  srand(Measured_CCA);
    pui8TestCode[2] = ASCII2TestCode(data->ui8CCARating);
    //
    // #3ed random num
    //
    //  srand(Measured_CCA);
    pui8TestCode[6] = ASCII2TestCode(rand()%36);
}

uint8_t ASCII2TestCode(uint8_t ui8Num)
{
    if(ui8Num < 10)
    {
        ui8Num = ui8Num + '0';
    }
    else
    {
        ui8Num = ui8Num - 10;
        ui8Num = ui8Num + 'A';
    }
    return ui8Num;
}

static int8_t upload_retry = -1;
static Timer_t* upload_retry_timer = NULL;

void records_upload_cb(bool succeed, uint8_t reason);

void upload_retry_cb(){
    UARTprintf("_______________________________________upload retry\n");
    unregister_timer(upload_retry_timer);
    upload_retry_timer = NULL;
    upload_recod(records_upload_cb);
}


void records_upload_cb(bool succeed, uint8_t reason){
    if(succeed){
        if(get_upload_recod_mode() == MFC_upload) ST7650Green();
        Message_by_language(Result_uploaded, get_saved_record_count(),en_US);
    }
    else{
        if(upload_retry){
            if(get_upload_recod_mode() == MFC_upload) ST7650Yellow();
            if(upload_retry_timer) unregister_timer(upload_retry_timer);
            upload_retry_timer = register_timer(20, upload_retry_cb);
            start_timer(upload_retry_timer);
        }
        else{
            if(get_upload_recod_mode() == MFC_upload) ST7650Red();
            Message_by_language(Upload_failed, 0,en_US);
        }
        upload_retry--;

#if AWS_IOT_SERVICE
        if(reason == Fail_reason_not_register){
            SYS_INFO* info = Get_system_info();
            info->device_registered = 0;
            save_system_info(&info->device_registered, sizeof(info->device_registered));
            Message_by_language(fail_to_upload_unregister, 0, en_US);
        }
#endif
    }
}
void upload_saved_recod(){
    if(upload_retry < 0) upload_retry = 0;

    upload_recod(records_upload_cb);
}

uint8_t check_vin(uint8_t* buf,uint8_t length){
    uint8_t i;
    for(i = 0; i < length; i++){
        // lower case to upper case
        buf[i] = (buf[i] >= 'a' && buf[i] <= 'z')? buf[i]-' ':buf[i];

        if((buf[i]>='0' && buf[i]<='9') || (buf[i]>='A'&&buf[i]<='Z' && buf[i]!= 'I' && buf[i]!='O' && buf[i]!='Q')){
        }
        else{
            UARTprintf("check_vin(), VIN[%d] = %c is invalid.\n", i, buf);
            return 0;
        }
    }
    return 1;
}

