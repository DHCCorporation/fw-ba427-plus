//*****************************************************************************
//
// Source File: SelCarModel.c
// Description: Select Car Model widget. 
//				
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 03/22/18     William.H      Created file.
// 03/23/18     Henry.Y        SelCarModel_DISPLAY() force trans SelCarModel_widget.index when calling SelCarModelAntiWhiteItem().
// 09/11/18     Henry.Y        Add car brand/model/version listing and dummy IR compensation.
// 12/07/18     Henry.Y        New display api.
// =============================================================================
//
//
// $Header: $
// $DateTime: $
// $Author: $
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#include "Widgetmgr.h"

typedef enum{
	BRAND_SEL = 0,
	MODEL_SEL,	
	VERSION_SEL,
}OPTION_SEL;


const tBrand car_model[] = {
	{"Fiat",1,
		{
			{"Freemont",2,
				{
					{"2.0 Diesel",0},	
					{"3.6 Petrol",0},
				}
			},
		}
	},

	{"Dodge",1,
		{
			{"Journey",2,
				{
					{"2.0 Diesel",0}, 
					{"3.6 Petrol",0},
				}
			},
		}
	},

	{"Lancia",1,
		{			
			{"Flavia",1,
				{
					{"2.4 Petrol",0},					
				}
			},
		}
	},
	
	{"Chrysler",1,
		{			
			{"200C",1,
				{
					{"2.4 Petrol",0},					
				}
			},
		}
	},



};

static const uint8_t BRAND_NUM = (sizeof(car_model)/ sizeof(tBrand));


void SelCarModel_DISPLAY();
void SelCarModel_UP();
void SelCarModel_DOWN();
void SelCarModel_LEFT();
void SelCarModel_SELECT();
/*void SelCarModel_initial();*/

static void DrawOptions(OPTION_SEL option_sel, uint8_t draw_start_y);

widget* SelCarModel_sub_W[] = {		
		&PointToBAT_widget
};

widget SelCarModel_widget = {
		.Key_UP = SelCarModel_UP,
		.Key_DOWN = SelCarModel_DOWN,
		.Key_RIGHT = 0,
		.Key_LEFT = SelCarModel_LEFT,
		.Key_SELECT = SelCarModel_SELECT,
		.routine = 0,
		.service = 0,
		.initial = /*SelCarModel_initial*/0,
		.display = SelCarModel_DISPLAY,
		.subwidget = SelCarModel_sub_W,
		.number_of_widget = sizeof(SelCarModel_sub_W)/4,
		.index = 0,
		.name = "SelCarModel",
};
/*
void SelCarModel_initial(){
	uint8_t i,j,k,md,ve;

	UARTprintf("brand#:%04d\r\n",BRAND_NUM);
	for(i=0;i<BRAND_NUM;i++){
		md = car_model[i].model_num;
		UARTprintf("brand%04d, name:%s, model#:%04d\r\n",i,car_model[i].brand_name,md);
		for(j=0;j<md;j++){
			ve = car_model[i].model_list[j].version_num;
			UARTprintf("  model%04d, name:%s, model#:%04d\r\n",j,car_model[i].model_list[j].model_name,ve);
			for(k=0;k<ve;k++){
				UARTprintf("    version%04d, name:%s\r\n",k,car_model[i].model_list[j].version_list[k].version);
			}
		}
	}
}
*/



void SelCarModel_DISPLAY(){	
	TEST_DATA* TD = Get_test_data();
	SHOW_profile_t profile = {side_middle, 2, false};
	tRectangle sTemp;
	char buf[20] = {0};

	sTemp = LCD_display(JUMP_START_POST, SelCarModel_widget.parameter);



	if(SelCarModel_widget.parameter == BRAND_SEL){
		sTemp = LCD_draw_string(CAR_BRAND,0, sTemp.i16YMax+2);		
	}
	else if(SelCarModel_widget.parameter == MODEL_SEL){
		snprintf(buf,sizeof(buf),"%s/",car_model[TD->Brand_sel].brand_name);
		UTF8_LCM_show(2,sTemp.i16YMax+2, buf, 125, &profile);
		sTemp = LCD_draw_string(CAR_MODEL,0, profile.region.y2+2);

	}
	else{/*VERSION_SEL*/ 
		snprintf(buf,sizeof(buf),"%s/%s/",car_model[TD->Brand_sel].brand_name,car_model[TD->Brand_sel].model_list[TD->Model_sel].model_name);
		UTF8_LCM_show(2,sTemp.i16YMax+2, buf, 125, &profile);
		sTemp = LCD_draw_string(CAR_VERSION,0, profile.region.y2+2);
	}
	
	DrawOptions((OPTION_SEL)SelCarModel_widget.parameter,sTemp.i16YMax+2);
	
}


void SelCarModel_UP(){
	TEST_DATA* TD = Get_test_data();
	
	if(SelCarModel_widget.parameter == BRAND_SEL){
		if(BRAND_NUM == 1) return;
		TD->Brand_sel = (!TD->Brand_sel)? BRAND_NUM-1:TD->Brand_sel-1;
	}
	else if(SelCarModel_widget.parameter == MODEL_SEL){
		if(car_model[TD->Brand_sel].model_num == 1) return;
		TD->Model_sel = (!TD->Model_sel)? (car_model[TD->Brand_sel].model_num)-1:TD->Model_sel-1;
	}
	else/* if(SelCarModel_widget.parameter == VERSION_SEL)*/{
		if(car_model[TD->Brand_sel].model_list[TD->Model_sel].version_num == 1) return;
		TD->Version_sel = (!TD->Version_sel)? (car_model[TD->Brand_sel].model_list[TD->Model_sel].version_num)-1:TD->Version_sel-1;
	}

	SelCarModel_DISPLAY();

}

void SelCarModel_DOWN(){
	TEST_DATA* TD = Get_test_data();

	if(SelCarModel_widget.parameter == BRAND_SEL){
		if(BRAND_NUM == 1) return;
		TD->Brand_sel = (TD->Brand_sel + 1)%BRAND_NUM;
	}
	else if(SelCarModel_widget.parameter == MODEL_SEL){
		if(car_model[TD->Brand_sel].model_num == 1) return;
		TD->Model_sel = (TD->Model_sel + 1)%(car_model[TD->Brand_sel].model_num);
	}
	else/* if(SelCarModel_widget.parameter == VERSION_SEL)*/{
		if(car_model[TD->Brand_sel].model_list[TD->Model_sel].version_num == 1) return;
		TD->Version_sel = (TD->Version_sel + 1)%(car_model[TD->Brand_sel].model_list[TD->Model_sel].version_num);
	}
	
	SelCarModel_DISPLAY();
}

void SelCarModel_LEFT(){
	if(SelCarModel_widget.parameter == BRAND_SEL){
		Return_pre_widget();		
	}
	else{
		SelCarModel_widget.parameter--;
		SelCarModel_DISPLAY();		
	}
}

void SelCarModel_SELECT(){
	TEST_DATA* TD = Get_test_data();

	if(SelCarModel_widget.parameter == BRAND_SEL){
		SelCarModel_widget.parameter = MODEL_SEL;
		TD->Model_sel = (TD->Model_sel > (car_model[TD->Brand_sel].model_num)-1)? 0:TD->Model_sel;

		SelCarModel_DISPLAY();
	}
	else if(SelCarModel_widget.parameter == MODEL_SEL){
		SelCarModel_widget.parameter = VERSION_SEL;
		TD->Version_sel = (TD->Version_sel > (car_model[TD->Brand_sel].model_list[TD->Model_sel].version_num)-1)? 0:TD->Version_sel;
		SelCarModel_DISPLAY();
	}
	else/* if(SelCarModel_widget.parameter == VERSION_SEL)*/{
		resistance_compensation(car_model[TD->Brand_sel].model_list[TD->Model_sel].version_list[TD->Version_sel].cable_resistance);

		Move_widget(SelCarModel_sub_W[0],0);
	}
}

typedef struct{
	uint8_t brand_num_page;
	uint8_t model_num_page;
	uint8_t version_num_page;
}
page_num_t;

const page_num_t num_of_page[] = {
#if ENGLISH_en
	{4,3,3},
#endif
#if FRANCAIS_en
	{2,1,1},
#endif
#if DEUTSCH_en
	{4,3,3},
#endif
#if ESPANOL_en
	{4,3,2},
#endif
#if ITALIAN_en
	{3,2,2},
#endif
#if PORTUGUES_en
	{4,3,2},
#endif
#if NEDERLANDS_en
	{4,3,3},
#endif
#if DANSK_en
	{4,3,3},
#endif
#if NORSK_en
	{4,3,3},
#endif
#if SOOMI_en
	{4,3,3},
#endif
#if SVENSKA_en
	{4,3,3},
#endif
#if CESTINA_en
	{2,2,2},
#endif
#if LIETUVIU_K_en
	{3,2,2},
#endif
#if POLSKI_en
	{4,3,3},
#endif
#if SLOVENCINA_en
	{3,3,3},
#endif
#if SLOVENSCINA_en
	{3,2,2},
#endif
#if TURKU_en
	{2,1,1},
#endif
#if EESTI_en
	{4,3,3},
#endif
#if LATVIESU_en
	{2,1,1},
#endif
#if LIMBA_ROMANA_en
	{3,3,3},
#endif
#if PUSSIJI_en
	{3,2,2},
#endif
#if EAAHNIKA_en
	{3,2,2},
#endif

};


static void DrawOptions(OPTION_SEL option_sel, uint8_t draw_start_y){
	uint8_t i, num_per_page, sel, total_item_num,offset,height = 0;
	TEST_DATA* TD = Get_test_data();
	SHOW_profile_t profile = {side_left, 2, false};
	LANGUAGE lang = get_language_setting();
	char text[20];
	tRectangle sTemp;

	num_per_page = (option_sel == BRAND_SEL)? (num_of_page[lang].brand_num_page):\
					(option_sel == MODEL_SEL)? num_of_page[lang].model_num_page:num_of_page[lang].version_num_page;


	sel = (option_sel == BRAND_SEL)? TD->Brand_sel:(option_sel == MODEL_SEL)? TD->Model_sel:TD->Version_sel;
	total_item_num = (option_sel == BRAND_SEL)? BRAND_NUM:(option_sel == MODEL_SEL)? car_model[TD->Brand_sel].model_num:car_model[TD->Brand_sel].model_list[TD->Model_sel].version_num;

	DrawPageInfo(sel,total_item_num,num_per_page);
	offset = sel%num_per_page;

	
	for(i = 0; i< num_per_page;i++){
		if((sel - offset + i) < total_item_num){
			memset(text,0,sizeof(text));
			if(option_sel == BRAND_SEL){
				strcpy(text, car_model[sel - offset + i].brand_name);
			}
			else if(option_sel == MODEL_SEL){
				strcpy(text, car_model[TD->Brand_sel].model_list[sel - offset + i].model_name);
			}
			else/* if(option_sel == VERSION_SEL)*/{
				strcpy(text, car_model[TD->Brand_sel].model_list[TD->Model_sel].version_list[sel - offset + i].version);
			}			

			draw_start_y+=(height+1);
			height = UTF8_LCM_show(10,draw_start_y, text, 117, &profile);
			
			if(i == (sel%num_per_page)){
				sTemp.i16XMin = profile.region.x1 - 1;
				sTemp.i16XMax = profile.region.x2 + 1;
				sTemp.i16YMin = profile.region.y1 - 1;
				sTemp.i16YMax = profile.region.y2; //+ 1;
				Lcd_selection_antiwhite(sTemp);
			}
		}
		else{
			// empty
		}
	}
}




