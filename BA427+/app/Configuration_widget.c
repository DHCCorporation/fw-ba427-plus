/*
 * Configuration_widget.c
 *
 *  Created on: 2020撟�12���24�
 *      Author: ray.yuan
 */

#include "Widgetmgr.h"

typedef enum{
    Configuration_language,
    Configuration_set_date,
    Configuration_brightness,
    Configuration_counter,
    Configuration_customize,
    Configuration_ITEM_NUM,
    Configuration_voitmeter,
    Configuration_ammeter,
    Configuration_clear_memory,
    Configuration_WIFI,
    Configuration_OTA,
    Configuration_Device_Reg,
}CONFIGURATION;

void Configuration_LEFT();
void Configuration_RIGHT();
void Configuration_display();
void Configuration_SELECT();
void Configuration_Init();

uint8_t arrow_num = 0 ;

widget* Configuration_sub_W[] = {
        &LangSel_widget,
        &DateTimeSet_widget, //&TimeZone_widget,
        &BackLightSet_widget,
        &TestCounter_widget,
        &InVKbd_widget,
        &ClearMemory_widget,
        &WiFiSet_widget,
        &DeviceRegisterManual_widget,
        &Voltmeter_widget,
        &Ammeter_widget,
        &OTA_widget,
};



widget Configration_widget = {
        .Key_UP = 0,
        .Key_DOWN = 0,
        .Key_RIGHT = Configuration_RIGHT,
        .Key_LEFT = Configuration_LEFT,
        .Key_SELECT = Configuration_SELECT,
        .routine = 0,
        .service = 0,
        .initial = Configuration_Init,
        .display = Configuration_display,
        .subwidget = Configuration_sub_W,
        .number_of_widget = sizeof(Configuration_sub_W)/4,
        .index = 0,
        .name = "Configuration",
};

void Configuration_Init(){
    Configration_widget.index = 0;
    arrow_num = 0;
}

void Configuration_LEFT(){
    Configration_widget.index = (Configration_widget.index == 0)? Configuration_ITEM_NUM-1:Configration_widget.index-1;

    if(Configration_widget.index < 1 && arrow_num == 1 ) arrow_num = 0;

    if(Configration_widget.index >= 4) arrow_num = 1;

    Configuration_display();
}

void Configuration_RIGHT(){
    Configration_widget.index = (Configration_widget.index+1)%Configuration_ITEM_NUM;

    if(Configration_widget.index >= 4) arrow_num = 1;

    if(Configration_widget.index == 0) arrow_num = 0;

    Configuration_display();
}

void Configuration_display(){
    SYS_INFO* inf = Get_system_info();
    ST7565ClearScreen(BUFFER1);
    ST7565Refresh(BUFFER1);


    if(Configration_widget.index < 4 && arrow_num == 0 ){
        LCD_String_ext(0 ,get_Language_String(S_LANGUAGE) , Align_Central);
        LCD_String_ext(16,get_Language_String(S_SET_DATE) , Align_Central);
        LCD_String_ext(32,get_Language_String(S_BRIGHTNESS), Align_Central);
        LCD_String_ext(48,get_Language_String(S_COUNTER) , Align_Central);
        LCD_Select_Arrow(16*(Configration_widget.index%4));
    }
    else{
        LCD_String_ext(0,get_Language_String(S_SET_DATE) , Align_Central);
        LCD_String_ext(16,get_Language_String(S_BRIGHTNESS), Align_Central);
        LCD_String_ext(32,get_Language_String(S_COUNTER) , Align_Central);
        LCD_String_ext(48,get_Language_String(S_CUSTOMIZE), Align_Central);
        LCD_Select_Arrow(16*((Configration_widget.index-1)%4));
    }

}

void Configuration_SELECT(){

    switch(Configration_widget.index){
    case Configuration_language:
        Move_widget_untracked(Configuration_sub_W[0],0);
        break;
    case Configuration_set_date:
        Move_widget_untracked(Configuration_sub_W[1],0);
        break;
    case Configuration_brightness:
        Move_widget_untracked(Configuration_sub_W[2],0);
        break;
    case Configuration_counter:
        Move_widget_untracked(Configuration_sub_W[3],0);
        break;
    case Configuration_voitmeter:
        Move_widget_untracked(Configuration_sub_W[8],0);
        break;
    case Configuration_ammeter:
        Move_widget_untracked(Configuration_sub_W[9],0);
        break;
    case Configuration_customize:
        Move_widget_untracked(Configuration_sub_W[4],0);
        break;
    case Configuration_clear_memory:
        Move_widget_untracked(Configuration_sub_W[5],0);
        break;
    case Configuration_WIFI:
        //Move_widget_untracked(Configuration_sub_W[6],0);
        //break;
    case Configuration_OTA:
        //Move_widget_untracked(Configuration_sub_W[10],0);
        //break;
    case Configuration_Device_Reg:
        //Move_widget_untracked(Configuration_sub_W[7],0);
    default:
        Configration_widget.index = Configuration_language;
        Move_widget_untracked(Configuration_sub_W[0],0);
        break;
    }
}



