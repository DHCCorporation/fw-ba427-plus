/*
 * Ammeter_widget.c
 *
 *  Created on: 2020撟�12���29�
 *      Author: ray.yuan
 */

#include "Widgetmgr.h"
#define OVER_RANGE_AMPER 600//A

#define UPDATE_SHOW_A_PERIOD 10//ms

static uint32_t DelayCounter = 0;

void Ammeter_DISPLAY();
void Ammeter_SELECT();
void Ammeter_ROUTE();
void Ammeter_Init();

widget* Ammeter_sub_W[] = {

};

widget Ammeter_widget = {
        .Key_UP = 0,
        .Key_DOWN = 0,
        .Key_RIGHT = 0,
        .Key_LEFT = 0,
        .Key_SELECT = Ammeter_SELECT,
        .routine = Ammeter_ROUTE,
        .service = 0,
        .initial = Ammeter_Init,
        .display = Ammeter_DISPLAY,
        .subwidget = Ammeter_sub_W,
        .number_of_widget = sizeof(Ammeter_sub_W)/4,
        .index = 0,
        .no_messgae = 1,
        .name = "Ammeter",
};

void Ammeter_show_Cur(){
    uint8_t ui8LopIdx = 0;
    float fVMAM = 0,fVMAMacc = 0;
    char buf[20];
    uint32_t ADC;

    //amper meter
    //
    // Sampling 100 times for stablizing AM sensor
    //
    for(ui8LopIdx = 0; ui8LopIdx < 100; ui8LopIdx++){
        GetAM(&fVMAM,&ADC);

        fVMAMacc = fVMAMacc + fVMAM;
    }

    fVMAM = fVMAMacc/100;
    if(fabs(fVMAM) > OVER_RANGE_AMPER){
        if(!Ammeter_widget.index){
            Ammeter_widget.index = 1;
            Ammeter_DISPLAY();
        }
    }
    else{
        if(Ammeter_widget.index){
            Ammeter_widget.index = 0;
            Ammeter_DISPLAY();
        }

        if(fVMAM>-0.5 && fVMAM<0){
            fVMAM = 0;
        }

        sprintf(buf, "  %4.0fA  ", fVMAM);
        LCD_String_ext(32,buf , Align_Central);
    }
}

void Ammeter_Init(){
    Ammeter_widget.index = 0;

}

void Ammeter_DISPLAY(){
    // Clear screen
    ST7565ClearScreen(BUFFER1);
    ST7565Refresh(BUFFER1);
    // Write SCREEN
    LCD_String_ext(16,get_Language_String(S_AMMETER) , Align_Central);
    if(Ammeter_widget.index){
        LCD_String_ext(32,get_Language_String(S_OVER_RANGE) , Align_Central);
    }
    LCD_Arrow(Arrow_enter);
}

void Ammeter_SELECT(){
    Return_pre_widget();
}

void Ammeter_ROUTE(){
    if(DelayCounter == 0){
        DelayCounter = UPDATE_SHOW_A_PERIOD;
        Ammeter_show_Cur();
    }
    else{
        DelayCounter--;
    }
}
