//*****************************************************************************
//
// Source File: deviceregwidget.c
// Description: Device registration widget.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 01/02/19     Henry.Y        Created file.
// 02/24/20     Henry.Y        Scanner control modification.
// =============================================================================
//
//
// $Header$
// $DateTime$
// $Author$
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#include "Widgetmgr.h"

static char registration_code[150] = {0};
static bool scing_poll = false;
static bool bldata_get = false;
static bool blregist_result = false;


void devicereg_UP_DOWN();
void devicereg_LEFT();
void devicereg_SELECT();
void devicereg_display();
void devicereg_service();
void devicereg_routine();
void devicereg_initial();

void REGISTRATION_SCANNER_CB(bool flush, uint8_t data){
	 uint8_t ui8buf = 0;
	 static uint8_t RX_index = 0;
	 static bool bCR = false, bNL = false;
	 static char UART0_RX_buffer[150] = {0};
	 
	if(flush){
		RX_index = 0;
		bCR = false;
		bNL = false;
	}
	else{
		ui8buf = data;
		UART0_RX_buffer[RX_index] = ui8buf;
		RX_index++;
		
		if(ui8buf == 0x0D) bCR = true;
		if(ui8buf == 0x0A) bNL = true;
		if(bCR && bNL){
			UART0_RX_buffer[RX_index - 2] = '\0';
			if(!memcmp("{\"data\":\"",UART0_RX_buffer,9)){//{"data":"a25...:..."}
				memcpy(registration_code, UART0_RX_buffer+9 , strlen(UART0_RX_buffer)-2-9);
			}
			else registration_code[0] = 0;
		
			memset(UART0_RX_buffer, 0, 150);
			bldata_get = true;
			RX_index = 0;
			bCR = false;
			bNL = false;
		}
	}
 }

void regist_result(bool result, uint8_t reason){
	SYS_INFO* info = Get_system_info();
	if(result){
		UARTprintf("device register success!\n");
		Message_by_language(register_successfully,0,en_US);
		info->device_registered = 1;
		save_system_info(&info->device_registered, sizeof(info->device_registered));
	}
	else{
		UARTprintf("device register fail:%d\n", reason);
		Message_by_language((reason == register_fail_network_err)? network_unstable_fail_to_register:register_fail_code, reason,en_US);
	}
	blregist_result = true;
}

widget devicereg_widget = {
		.Key_UP = devicereg_UP_DOWN,
		.Key_DOWN = devicereg_UP_DOWN,
		.Key_RIGHT = 0,
		.Key_LEFT = devicereg_LEFT,
		.Key_SELECT = devicereg_SELECT,
		.initial = devicereg_initial,
		.routine = devicereg_routine,
		.service = devicereg_service,
		.display = devicereg_display,
		.subwidget = 0,
		.number_of_widget = 0,
		.index = 0,
		.name = "device_registration",
};
		
void devicereg_initial(){
	if(wifi_get_status() == WIFI_off){
		Message_by_language(Turn_on_WIFI,0,en_US);
		Return_pre_widget();
		return;
	}
	else if(wifi_get_status() == WIFI_disconnect){
		Message_by_language(Check_connection,0,en_US);
		Return_pre_widget();
		return;
	}

	set_scanner_cb(REGISTRATION_SCANNER_CB);

}


void devicereg_display(){
	switch(devicereg_widget.parameter){
	case 0:
		LCD_display(SCAN_QR_CODE,devicereg_widget.index^0x01);
	break;
	case 1:
		LCD_display(SCANNER_SCANNING,scing_poll);
	break;
	case 2:
		LCD_display(REGISTERING,0);
	break;
	}	
}

void devicereg_UP_DOWN(){
	if(devicereg_widget.parameter == 0){
		devicereg_widget.index^=0x01;
		devicereg_display();
	}
}


void devicereg_LEFT(){
	if(devicereg_widget.parameter == 0){
		Return_pre_widget();
	}
}

void devicereg_SELECT(){
	if(devicereg_widget.parameter == 0){
		if(devicereg_widget.index){
			devicereg_widget.parameter = 1;	
			devicereg_service();
		}
		else{
			Return_pre_widget();
		}
	}
}

void devicereg_service(){
	if(devicereg_widget.parameter == 1){
		memset(registration_code,0,150);
		bldata_get = false;
		SCAN_ON;
		devicereg_display();
	}
	else if(devicereg_widget.parameter == 2){
		blregist_result = false;
		AWS_IoT_device_register((uint8_t*)registration_code,regist_result);
		SYS_PAR* par = Get_system_parameter();
		UARTprintf("Registration SeqID = %d\n",par->register_rslt_seqID);

	}
}

void devicereg_routine(){
	static uint16_t poll_count = 0;

	if(devicereg_widget.parameter == 1){
		if(bldata_get){
			poll_count = 0;
			SCANNER_BUZZ;
			SCAN_OFF;
			if(registration_code[0]){
				devicereg_widget.parameter = 2;
				UARTprintf("data scanned: %s\n",registration_code);
				devicereg_service();
			}
			else{
				devicereg_widget.parameter = 0;
				Message_by_language(invalid_code, 0, en_US);
			}
		}
		else{
			poll_count = (poll_count+1)%3000;
			if(!poll_count){
				SCAN_OFF;
				Message_by_language(invalid_code, 0, en_US);
				devicereg_widget.parameter = 0;
			}
			else if(poll_count%600 == 0){
				scing_poll^=0x01;
			}
			else return;
		}		
		devicereg_display();
	}
	else if(devicereg_widget.parameter == 2){
		if(blregist_result)	Return_pre_widget();
	}
	else return;
}

