//*****************************************************************************
//
// Source File: TurnOnLoadswidget.c
// Description: Ripple Votlage test result widget including Click(Push to new menu entry to stack)/
//				Right/Left(Pop out stack)/Up/Down function body.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 02/04/16     Vincent.T      Created file.
// 03/31/16     Vincent.T      1. Modify TurnOnLoadsClick(); to handle multination language case.
// 09/21/17 	Jerry.W		   Rewrite Widget for new software architecture
// 09/11/18     Henry.Y        Add TurnOnLoads_LEFT.
// 12/07/18     Henry.Y        New display api.
// 08/03/20     David.Wang     Some pictures of sys test changed to text description
//
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/app/TurnOnLoadswidget.c#6 $
// $DateTime: 2017/09/22 11:22:39 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************


#include "Widgetmgr.h"


void TurnOnLoads_SELECT();
void TurnOnLoads_display();

widget* TurnOnLoads_sub_W[] = {
		&Ripple15Sec_widget,
		&AbortTest_widget
};



widget TurnOnLoads_widget = {
		.Key_UP = 0,
		.Key_DOWN = 0,
		.Key_RIGHT = 0,
		.Key_LEFT = 0,
		.Key_SELECT = TurnOnLoads_SELECT,
		.routine = 0,
		.service = 0,
		.display = TurnOnLoads_display,
		.subwidget = TurnOnLoads_sub_W,
		.number_of_widget = sizeof(TurnOnLoads_sub_W)/4,
		.index = 0,
		.no_messgae = 1,
		.name = "TurnOnLoads",
};



void TurnOnLoads_display(){
    ST7565ClearScreen(BUFFER1);
    ST7565Refresh(BUFFER1);
    LCD_String_ext(16,get_Language_String(S_TURN_ON_LOADS) , Align_Central);
    LCD_String_ext(32,get_Language_String(S_AND_PRESS_ENTER) , Align_Central);
    LCD_Arrow(Arrow_enter);
}


void TurnOnLoads_SELECT()
{
	Move_widget(TurnOnLoads_sub_W[0],0);

}



