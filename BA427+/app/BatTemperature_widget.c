//*****************************************************************************
//
// Source File: BatTemperature_widget.c
// Description: Bat is it >0c YES/NO ?
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 20201205     David.Wang     Created file.
//
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/app/Testinvehiclewidget.c#1 $
// $DateTime: 2017/11/27 10:38:22 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#include "Widgetmgr.h"


void BatTemp_DISPLAY();
void BatTemp_SELECT();
void BatTemp_RIGHT();
void BatTemp_LEFT();
void BatTemp_initial();

widget* BatTemp_sub_W[] = {
    &CheckRipple_widget,
};

widget BatTemp_widget = {
        .Key_UP = 0,
        .Key_DOWN = 0,
        .Key_RIGHT = BatTemp_RIGHT,
        .Key_LEFT = BatTemp_LEFT,
        .Key_SELECT = BatTemp_SELECT,
        .routine = 0,
        .service = 0,
        .display = BatTemp_DISPLAY,
        .initial = BatTemp_initial,
        .subwidget = BatTemp_sub_W,
        .number_of_widget = sizeof(BatTemp_sub_W)/4,
        .index = 0,
        .no_messgae = 1,
        .name = "BatTemp",
};


void BatTemp_initial(){
    SYS_PAR* par = Get_system_parameter();

    BatTemp_widget.index = par->BAT_test_par.Temp_Range;
}


void BatTemp_DISPLAY(){
    SYS_PAR* par = Get_system_parameter();
    //char* YN;
    // Clear screen
    ST7565ClearScreen(BUFFER1);
    ST7565Refresh(BUFFER1);
    // Write SCREEN
    LCD_String_ext(16,get_Language_String(S_BAT_TEMPERATURE) , Align_Left);

    char buf[25];
    char *degree = "°";
    char *temp;

    switch(BatTemp_widget.index){
    case 11:
        sprintf(buf, "%s", get_Language_String(S_OV37_F_C));
        break;
    case 10:
        sprintf(buf, "%s", get_Language_String(S_OV32_F_C));
        break;
    case 9:
        sprintf(buf, "%s", get_Language_String(S_OV26_F_C));
        break;
    case 8:
        sprintf(buf, "%s", get_Language_String(S_OV21_F_C));
        break;
    case 7:
        sprintf(buf, "%s", get_Language_String(S_OV15_F_C));
        break;
    case 6:
        sprintf(buf, "%s", get_Language_String(S_OV10_F_C));
        break;
    case 5:
        sprintf(buf, "%s", get_Language_String(S_OV4_F_C));
        break;
    case 4:
        sprintf(buf, "%s", get_Language_String(S_ON1_F_C));
        break;
    case 3:
        sprintf(buf, "%s", get_Language_String(S_ON6_F_C));
        break;
    case 2:
        sprintf(buf, "%s", get_Language_String(S_ON12_F_C));
        break;
    case 1:
        sprintf(buf, "%s", get_Language_String(S_ON17_F_C));
        break;
    case 0:
    default:
        BatTemp_widget.index = 0;
        sprintf(buf, "%s", get_Language_String(S_ON23_F_C));
        break;
    }

    temp = strstr(buf, degree);
    sprintf(temp, "~%s", temp+strlen(degree));
    temp = strstr(temp+1, degree);
    sprintf(temp, "~%s", temp+strlen(degree));
    LCD_String_ext(32,buf , Align_Central);

    LCD_Arrow(Arrow_enter | Arrow_RL);

}


void BatTemp_LEFT(){

    if(BatTemp_widget.index <= 0) BatTemp_widget.index = 11;
    else BatTemp_widget.index --;

    BatTemp_DISPLAY();
}

void BatTemp_RIGHT(){

    if(BatTemp_widget.index >= 11) BatTemp_widget.index = 0;
    else BatTemp_widget.index ++;

    BatTemp_DISPLAY();
}

void BatTemp_SELECT(){
    TEST_DATA* TD = Get_test_data();
    SYS_PAR* par = Get_system_parameter();

    TD->ObjTestingTemp = BatTemp_widget.index;
    TD->BodyTestingTemp = BatTemp_widget.index;
    par->BAT_test_par.Temp_Range = BatTemp_widget.index;

    save_system_parameter(&par->BAT_test_par, sizeof(par->BAT_test_par));

    Move_widget(BatTemp_sub_W[0],0);

}




