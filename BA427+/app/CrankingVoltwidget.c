//*****************************************************************************
//
// Source File: CrankingVoltWidget.c
// Description: Cranking Voltage test widget including Click(Push to new menu entry to stack)/
//				Right/Left(Pop out stack)/Up/Down function body.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 02/02/16     Vincent.T      Created file.
// 09/21/17 	Jerry.W		   Rewrite Widget for new software architecture
// 03/05/18     Henry.Y        add measurement on lowest cranking voltage.
// 09/11/18     Henry.Y        Adjust detection flow. Add CrankingVolt_LEFT.
// 12/07/18     Henry.Y        New display api.
// 03/20/19     Henry.Y        Rounddown test voltage to the second decimal place.
// 08/03/20     David.Wang     Some pictures of sys test changed to text description
// 08/07/20     David.Wang     Add start ask voltage high check screen
// 10/16/20     David.Wang     Add SYSTEM_CHANNEL = MAP_24V && engine_voltage < 26.8v into diesel ask
// 10/20/20     David Wang     Fix caught cranking vol miss bug to mask V1-V2 > 0.1V rule
// 20201127     David Wang     Add countdown timer reset
//
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/app/CrankingVoltwidget.c#6 $
// $DateTime: 2017/09/22 11:17:55 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#include "Widgetmgr.h"

void CrankingVolt_SERVICE();
void CrankingVolt_SELECT();
void CrankingVolt_display();
void CrankingVolt_Routine();


widget* CrankingVolt_sub_W[] = {
		&AltIdleVolt_widget,
		&AbortTest_widget,
};

widget CrankingVolt_widget = {
		.Key_UP = 0,
		.Key_DOWN = 0,
		.Key_RIGHT = 0,
		.Key_LEFT = 0,
		.Key_SELECT = CrankingVolt_SELECT,
		.routine = CrankingVolt_Routine,
		.service = CrankingVolt_SERVICE,
		.display = CrankingVolt_display,
		.subwidget = CrankingVolt_sub_W,
		.number_of_widget = sizeof(CrankingVolt_sub_W)/4,
		.index = 0,
		.no_messgae = 1,
		.name = "CrankingVolt",
};


void CrankingVolt_SERVICE(){
	TEST_DATA* TD = Get_test_data();
	if(CrankingVolt_widget.parameter == 0){
		CrankingVolt_widget.parameter = 1;
		if(get_routine_switch()){
			Main_routine_switch(0);
		}
		ROM_TimerDisable(TIMER2_BASE, TIMER_A);
		g_bCranking_timeout = 0;
		
		TD->CrankVResult = NO_DETECT;
		TD->CrankVol = 0;

		TD->Crank_vmin = (TD->SYSTEM_CHANNEL == MAP_24V)? 32:16;

		ROM_TimerLoadSet(TIMER2_BASE, TIMER_A, get_sys_clk()*30 - 1);   // Timer interrupt in 30 sec
		ROM_TimerEnable(TIMER2_BASE, TIMER_A);
	}
}



void CrankingVolt_SELECT(){
    TEST_DATA* TD = Get_test_data();

	if(CrankingVolt_widget.parameter >= 2){
	    float engine_voltage;
	    uint32_t ui32EngineVol_scale = 0;

	    BattCoreGetVoltage(&ui32EngineVol_scale, &engine_voltage);

	    Main_routine_switch(1);
	    Move_widget(CrankingVolt_sub_W[0], 0);
	    CrankingVolt_widget.index = 0;
	}
}

void CrankingVolt_display(){
	TEST_DATA* TD = Get_test_data();
	LANGUAGE lang = get_language_setting();

	ST7565ClearScreen(BUFFER1);
	ST7565Refresh(BUFFER1);

	if(CrankingVolt_widget.parameter == 0 || CrankingVolt_widget.parameter == 1){
	    LCD_String_ext(16,get_Language_String(S_TURN_OFF_LOADS) , Align_Central);
	    LCD_String_ext(32,get_Language_String(S_START_ENGINE) , Align_Central);
	}
	else{

		if(TD->CrankVResult == NO_DETECT){
		    LCD_String_ext(16,get_Language_String(S_CRANKING_VOLTS) , Align_Central);
		    LCD_String_ext(32,get_Language_String(S_NO_DETECTED) , Align_Central);
		}
		else{
		    LCD_String_ext(16,get_Language_String(S_CRANKING_VOLTS) , Align_Central);
		    char buf[10];
		    uint16_t result[] = {S_LOW, S_NORMAL, S_HIGH};
		    sprintf(buf, "%.2fV", TD->CrankVol );
		    LCD_String_ext2(32,buf , Align_Left, 2);
		    LCD_String_ext2(48,get_Language_String(result[TD->CrankVResult-LOW]) , Align_Right, 2);
		}
		LCD_Arrow(Arrow_enter);
	}
}


void CrankingVolt_Routine(){
	TEST_DATA* TD = Get_test_data();

	if(CrankingVolt_widget.parameter == 1){
		if(get_routine_switch()){
			Main_routine_switch(0);
		}

		if(g_bCranking_timeout){
			TD->Crank_vmin = floating_point_rounding(TD->Crank_vmin, 2, ffloor);
			TD->CrankVol = floating_point_rounding(TD->CrankVol, 2, ffloor);
			Main_routine_switch(1);
			CrankingVolt_widget.parameter = 2;
			CrankingVolt_display();
			return;
		}		
		// TD->CrankVResult = BattCoreCrankingVolt(&TD->CrankVol, &TD->Crank_vmin);
		// int BattCoreCrankingVolt(float *cranking_voltage, float *vmin)
		float voltage_max = 0, cranking_volt_buffer = 0;
		float voltage1, voltage2, voltage0, voltage_min, voltage_temp, voltage_V50, delta_voltage2, delta_voltage3;

		uint8_t cranking_loop = 0;
		uint32_t ui32Voltage_scale = 0;
		TEST_DATA* TD = Get_test_data();
		
		// voltage threshold
		//const float delta_V1 = (TD->SYSTEM_CHANNEL == MAP_24V)? 0.2:0.1;
		const float delta_V2 = (TD->SYSTEM_CHANNEL == MAP_24V)? 3.0:1.5;
		const float delta_V4 = (TD->SYSTEM_CHANNEL == MAP_24V)? 0.3:0.15;

		voltage_min = (TD->SYSTEM_CHANNEL == MAP_24V)? 32:16;


		BattCoreGetVoltage(&ui32Voltage_scale, &voltage1);	//* Get V1  
		delay_ms(2);
		BattCoreGetVoltage(&ui32Voltage_scale, &voltage2);	//* Get V2  
		if(voltage2 < TD->Crank_vmin) TD->Crank_vmin = voltage2;

		//delta_voltage1 = (voltage1 > voltage2)? (voltage1 - voltage2):0;
		//if(delta_voltage1 <= delta_V1) return; 	// condition1: delta_voltage>0.1V?
		
		voltage0 = voltage1;	// record V1

		for(cranking_loop = 0; cranking_loop < 200; cranking_loop++){	// 0.25ms*200 = 50ms, get Vmin
			BattCoreGetVoltage(&ui32Voltage_scale, &voltage_temp);
			if(voltage_temp < voltage_min)	voltage_min = voltage_temp;
			delay_ms(0.25);
		}
		
		if(voltage_min < TD->Crank_vmin) TD->Crank_vmin = voltage_min;
		delta_voltage2 = (voltage0 > voltage_min)? (voltage0 - voltage_min):0;
		if(delta_voltage2 <= delta_V2) return; //condition2: V0-Vmin>1.5?
		
		BattCoreGetVoltage(&ui32Voltage_scale, &voltage_V50);
		if(voltage_V50 > voltage_min){	// condition: rising?
			delta_voltage3 = (voltage_V50 - voltage_min);
		}				
		else{
			if(voltage_V50 < TD->Crank_vmin) TD->Crank_vmin = voltage_V50;
			return;
		}
		
		if(delta_voltage2 <= delta_voltage3) return;// condition3: V1-Vmin>V50-Vmin
		delay_ms(150);

		voltage_min = (TD->SYSTEM_CHANNEL == MAP_24V)? 32:16;//reset Vmin

		for(cranking_loop = 0; cranking_loop < 200; cranking_loop++){// 200*1ms = 200ms get MAX & MIN
			BattCoreGetVoltage(&ui32Voltage_scale, &voltage_temp);
			
			if(voltage_temp > voltage_max) voltage_max = voltage_temp;
			if(voltage_temp < voltage_min) voltage_min = voltage_temp;
			cranking_volt_buffer += voltage_temp;			
			delay_ms(1);
		}
		if(voltage_min < TD->Crank_vmin) TD->Crank_vmin = voltage_min;

		if((voltage_max > voltage_min) && ((voltage_max - voltage_min) > delta_V4)){	// condition4: ripple>0.15V
			TD->CrankVol = (cranking_volt_buffer/200);
			ROM_TimerDisable(TIMER2_BASE, TIMER_A);
			g_bCranking_timeout = false;

			if(TD->SYSTEM_CHANNEL == MAP_24V){
				TD->CrankVResult = (TD->CrankVol >= CRANKVOLT_24V)? NORMAL:LOW;
			}
			else{
				TD->CrankVResult = (TD->CrankVol >= CRANKVOLT_12V)? NORMAL:LOW;
			}
			
			TD->Crank_vmin = floating_point_rounding(TD->Crank_vmin, 2, ffloor);
			TD->CrankVol = floating_point_rounding(TD->CrankVol, 2, ffloor);
			Main_routine_switch(1);
			CrankingVolt_widget.parameter = 2;
			CrankingVolt_display();
		}
	}
	else{		
		if(get_routine_switch() == 0){
			Main_routine_switch(1);
		}
	}	
}



