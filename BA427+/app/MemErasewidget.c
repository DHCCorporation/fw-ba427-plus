//*****************************************************************************
//
// Source File: MemErasewidget.c
// Description: Prototype for functions of Memory Erase Widget. Including Click
//				(Push to new menu entry to stack)/Right/Left(Pop out stack)/Up/
//				Down function body.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 01/06/16     Vincent.T      Created file.
// 01/25/16     Vincent.T      1. Add func. to. Erase Info.
//                             2. global variable g_InfVkybdTextEditStr
//                             3. Add Erase items: Battery test parameters
//                             4. global variables g_BatteryType, g_SelectRating, g_PrintResult
//                             5. global variables g_SetCapacityVal, g_JISCapSelect
// 01/28/16     Vincent.T      1. global variable g_SSBatteryType, g_SSSelectRating
// 01/29/16     Vincent.T      1. gloabal variables: g_BTSetCapacityVal, g_SSSetCapacityVal
//                             2. #include "battypeselwidget.h"
// 02/19/16     Vincent.T      1. Will not erase information when "Memeory Erase: Yes" btn is pressed.
//                             2. Add animation.
// 03/25/16     Vincent.T      1. Modify MemEraseAntiWhile(); to handle multination language case.
//                             2. Modify MemEraseLeft(); to handle multination language case.
// 06/14/16     Vincent.T      1. global variables gui16SSTestCounter, gui16BatTestCounter, gui16SysTestCounter and gui16TotalTestRecord
//                             2. Mpdify Memory Erase Procedure(test records only) 
// 11/15/16     Ian.C          1. Modify MemEraseAntiWhile(); for multi-language.
// 01/13/17     Vincent.T      1. global variable gui16IRTestCounter to count times of IR testing
// 01/19/17     Vincent.T      1. Modify MemEraseAntiWhile(); for 16x16 characters.
// 09/22/17 	Jerry.W		   Rewrite Widget for new software architecture
// 12/07/18     Henry.Y        New display api.
// 03/29/19     Henry.Y        memory erase add test record offset.
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/app/MemErasewidget.c#6 $
// $DateTime: 2017/09/26 17:09:29 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#include "Widgetmgr.h"


void MemErase_UP_DOWN();
void MemErase_LEFT();
void MemErase_SELECT();
void MemErase_display();

widget* MemErase_sub_W[] = {
};

widget MemErase_widget = {
		.Key_UP = MemErase_UP_DOWN,
		.Key_DOWN = MemErase_UP_DOWN,
		.Key_RIGHT = 0,
		.Key_LEFT = MemErase_LEFT,
		.Key_SELECT = MemErase_SELECT,
		.routine = 0,
		.service = 0,
		.display = MemErase_display,
		.subwidget = MemErase_sub_W,
		.number_of_widget = sizeof(MemErase_sub_W)/4,
		.index = 0,
		.name = "MemErase",
};


void MemErase_UP_DOWN(){
	MemErase_widget.parameter ^= 0x01;
	MemErase_display();
}

void MemErase_LEFT(){
	Return_pre_widget();
}

void MemErase_SELECT(){
	if(MemErase_widget.parameter){
		SYS_INFO* info = Get_system_info();
		
		//
		//Let's erase!
		//
		LCD_display(TESTING_PROCESSING_ARROW,0);
		delay_ms(500);

		info->TotalTestRecord = 0;
		info->TestRecordOffset = 0;
		
		info->TotalSYSrecord = 0;
		info->SYSrecordOffset = 0;
		
		save_system_info(&info->TotalTestRecord, sizeof(info->TotalTestRecord));
		save_system_info(&info->TestRecordOffset, sizeof(info->TestRecordOffset));
		save_system_info(&info->TotalSYSrecord, sizeof(info->TotalSYSrecord));
		save_system_info(&info->SYSrecordOffset, sizeof(info->SYSrecordOffset));

		
		//
		// Infomation
		//
		//for(ui32buf = 0 + 19; ui32buf < 51; ui32buf++)
		//{
		//	ui8buf = 0;
		//	IntEepromSvcSaveOneByte(&ui8buf, ui8buf, 1);
		//	IntEepromSvcSaveOneByte(&ui8buf, ui8buf, 2);
		//	IntEepromSvcSaveOneByte(&ui8buf, ui8buf, 3);
		//	IntEepromSvcSaveOneByte(&ui8buf, ui8buf, 4);
		//}
		//
		// g_InfVkybdTextEditStr
		//
		//for(ui32buf = 0; ui32buf < 128; ui32buf++)
		//{
		//	g_InfVkybdTextEditStr[ui32buf] = 0;
		//}

		LCD_display(TESTING_PROCESSING_ARROW,1);
		delay_ms(500);

	}	
	Return_pre_widget();
}

void MemErase_display(){
	LCD_display(MEMORY_ERASE, MemErase_widget.parameter^0x01);

}



