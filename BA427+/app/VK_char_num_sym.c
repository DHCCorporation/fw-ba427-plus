/*
 * VK_char_num_sym.c
 *
 *  Created on: 2020�~12��23��
 *      Author: henry.yang
 */
#include "VKmgr.h"

#define DATA_LEN 64
#define DEFAULT_CHAR VK_KEY_ENTER
#define VK_LCD_INDEX 12


static const BUTTON_DATA _aButtonData[] = {
		{{10,34,18,41},       'Q' },
		{{21,34,32,41},       'W' },
		{{35,34,42,41},       'E' },
		{{46,34,54,41},       'R' },
		{{57,34,65,41},       'T' },
		{{68,34,76,41},       'Y' },
		{{79,34,87,41},       'U' },
		{{90,34,95,41},       'I' },
		{{100,34,108,41},     'O' },
		{{113,34,120,41},	  'P' },//0~9
		{{18,42,26,49},       'A' },
		{{30,42,37,49},       'S' },
		{{41,42,49,49},       'D' },
		{{53,42,60,49},       'F' },
		{{63,42,71,49},		  'G' },
		{{74,42,82,49},       'H' },
		{{85,42,93,49},       'J' },
		{{95,42,103,49},      'K' },
		{{108,42,114,49},     'L' },
		{{2,46,15,53},		  VK_KEY_Fn_abc },//10~19
		{{30,50,37,57},       'Z' },
		{{41,50,49,57},       'X' },
		{{52,50,60,57},       'C' },
		{{63,50,71,57},       'V' },
		{{75,50,82,57},       'B' },
		{{85,50,93,57},       'N' },
		{{95,50,103,57},      'M' },
		{{115,46,127,54},     VK_KEY_BACKSPACE },
		{{2,55,16,61},        VK_KEY_Fn_123 },
		{{20,57,24,63},		  VK_KEY_LEFT },//20~29
		{{25,57,29,63},       VK_KEY_RIGHT },
		{{30,58,36,62},       VK_KEY_DOWN },
		{{37,58,43,62},       VK_KEY_UP },
		{{46,58,107,63},      ' ' },
		{{112,55,61,125},	  VK_KEY_ENTER },
		{{11,35,18,41},       'q' },
		{{23,35,31,40},       'w' },
		{{35,35,41,40},       'e' },
		{{48,35,54,40},       'r' },
		{{58,34,65,40},       't' },//30~39
		{{69,34,77,40},       'y' },
		{{80,35,87,40},       'u' },
		{{91,33,96,40},       'i' },
		{{101,35,108,40},     'o' },
		{{113,35,120,41},     'p' },
		{{18,43,25,48},       'a' },
		{{29,43,35,48},       's' },
		{{40,41,47,48},       'd' },
		{{52,41,59,48},       'f' },
		{{63,43,70,49},       'g' },//40~49
		{{74,41,81,48},       'h' },
		{{84,41,90,49},       'j' },
		{{95,41,102,48},      'k' },
		{{106,41,111,48},     'l' },
		{{2,46,15,53},        VK_KEY_Fn_ABC },
		{{29,52,35,57},       'z' },
		{{40,52,47,57},       'x' },
		{{51,52,57,57},       'c' },
		{{62,52,69,57},       'v' },
		{{73,50,80,57},       'b' },//50~59
		{{84,52,91,57},       'n' },
		{{94,52,102,57},      'm' },
		{{115,46,127,54},     VK_KEY_BACKSPACE },
		{{2,55,16,61},        VK_KEY_Fn_123 },
		{{20,57,24,63},       VK_KEY_LEFT },
		{{25,57,29,63},       VK_KEY_RIGHT },
		{{30,58,36,62},       VK_KEY_DOWN },
		{{37,58,43,62},       VK_KEY_UP },
		{{46,58,107,63},      ' ' },
		{{112,55,125,61},     VK_KEY_ENTER },//60~69
		{{9,34,14,40},        '1' },
		{{21,34,27,40},       '2' },
		{{33,34,39,40},       '3' },
		{{45,34,52,40},       '4' },
		{{56,34,62,40},       '5' },
		{{66,34,72,40},       '6' },
		{{77,34,84,40},       '7' },
		{{88,34,94,40},       '8' },
		{{98,34,105,40},      '9' },
		{{110,34,117,40},     '0' }, //70~79
		{{10,44,15,46},       '-' },
		{{21,41,27,49},       '/' },
		{{34,44,38,48},       ':' },
		{{45,44,50,49},       ';' },
		{{56,41,61,49},       '(' },
		{{67,41,72,49},       ')' },
		{{78,41,84,49},       '$' },
		{{87,42,95,48},	      '&' },
		{{97,41,108,50},      '@' },
		{{111,42,117,45},     '"' }, //80~89
		{{2,47,16,53},	      VK_KEY_Fn_sym },
		{{40,55,44,57},	      '.' },
		{{53,54,57,57},	      ',' },
		{{62,50,68,57},	      '?' },
		{{74,50,78,57}, 	  '!' },
		{{85,50,90,53},	      '\''},
		{{115,46,127,54},     VK_KEY_BACKSPACE },
		{{2,55,16,61},	      VK_KEY_Fn_ABC },
		{{20,57,24,63},	      VK_KEY_LEFT },
		{{25,57,29,63},	      VK_KEY_RIGHT },//90~99
		{{30,58,36,62},	      VK_KEY_DOWN },
		{{37,58,43,62},	      VK_KEY_UP },
		{{46,58,107,63},	  ' ' },
		{{112,55,125,61},     VK_KEY_ENTER },
		{{10,34,15,42},	      '[' },
		{{21,34,26,42},	      ']' },
		{{32,34,38,42},	      '{' },
		{{46,34,52,42},	      '}' },
		{{55,35,63,41},	      '#' },
		{{66,34,74,41},	      '%' },//100~109
		{{77,34,85,40},	      '^' },
		{{88,34,94,39},	      '*' },
		{{99,35,105,39},	  '+' },
		{{110,35,117,39},     '=' },
		{{38,48,46,50},		  '_' },
		{{51,43,58,51},	      '\\'},
		{{61,45,69,48},	      '~' },
		{{72,43,79,49},	      '<' },
		{{84,43,91,49},	      '>' },
		{{2,47,16,53},	      VK_KEY_Fn_123 },//110~119
		{{40,55,44,57},	      '.' },
		{{53,54,57,57},	      ',' },
		{{62,50,68,57},	      '?' },
		{{74,50,78,57},	      '!' },
		{{85,50,90,53},	      '\''},
		{{115,46,127,54},	  VK_KEY_BACKSPACE },
		{{2,55,16,61},	      VK_KEY_Fn_ABC },
		{{20,57,24,63},	      VK_KEY_LEFT },
		{{25,57,29,63},		  VK_KEY_RIGHT },
		{{30,58,36,62},       VK_KEY_DOWN },//120~129
		{{37,58,43,62},	      VK_KEY_UP },
		{{46,58,107,63},	  ' ' },
		{{112,55,125,61},     VK_KEY_ENTER }//130~132
};
static uint16_t ButtonData_size = sizeof(_aButtonData)/sizeof(BUTTON_DATA);
static uint8_t kb_index;
static char vk_buf[DATA_LEN+1];
static VK_INFO_T VK_info;
static uint8_t keyboard_page;

static void VKInputUpdate(void)
{
    uint8_t i, len = strlen(vk_buf);
    uint8_t start_x;
    uint8_t off_idx = 0;
    for(i = 0; i < VK_info.len; i++){
        if(VK_info.start_x + (i%off_idx)*VK_info.char_width > (127-VK_info.char_width)){
            off_idx = i;
        }
        start_x = VK_info.start_x + (i%off_idx)*VK_info.char_width;
        
        ST7565Draw6x8Char(start_x, VK_info.start_y + (i/off_idx)*(8+1), (i < len)? vk_buf[i]:' ', Font_System5x8);
    }    
}

uint8_t VK_char_num_sym_init(VK_INFO_T* vkinfo)
{
    memcpy(&VK_info, vkinfo, sizeof(VK_INFO_T));
    if(VK_info.len > (sizeof(vk_buf)-1)) return 0;
    memset(vk_buf, 0, sizeof(vk_buf));
    keyboard_page = 0;
    uint16_t i;
    for(i = 0; i < ButtonData_size; i++){
        if(_aButtonData[i].c == DEFAULT_CHAR){
            kb_index = i;
            break;
        }
    }
    return 1;
}

void VK_char_num_sym_display(void)
{
	SHOW_profile_t profile = {side_middle, 2, false};
    
    ST7565ClearScreen(BUFFER1);
    LcdDrawScreen(VK_LCD_INDEX+keyboard_page, BUFFER1);
    
    if(VK_info.title_str) UTF8_LCM_show(0, 4, VK_info.title_str, 128, &profile);

    //Draw underline
    if(VK_info.underline){
        uint8_t i, start_x;
        for(i = 0; i < VK_info.len; i++){
            start_x = VK_info.start_x + (i)*VK_info.char_width;
            ST7565V128x64x1LineDrawH(0, start_x, start_x+4, VK_info.start_y+8, 1);
        }
    }
    VKInputUpdate();
    ST7565RectReverse(&_aButtonData[kb_index].recs, VERTICAL);
        
    //LCD_Arrow(VK_info.arr_flag);
}


char VK_char_num_sym_up(void)
{
	ST7565RectReverse(&_aButtonData[kb_index].recs, VERTICAL);

	//	'ABC' Virtual Keyboard
    if(kb_index == 18) kb_index = 9;
    else if(kb_index >= 29 && kb_index <= 31) kb_index = 20;
    else if(kb_index == 32) kb_index = 21;
    else if(kb_index == 33) kb_index = 24;
    else if(kb_index == 34) kb_index = 27;
    else if((kb_index >= 10) && (kb_index <= 17)) kb_index -= 10;
    else if((kb_index >= 19) && (kb_index <= 28)) kb_index -= 9;

	//	'abc' Virtual Keyboard
    else if(kb_index == 53)    kb_index = 44;
    else if(kb_index == 64 || kb_index == 65)    kb_index = 54;
    else if(kb_index == 66 || kb_index == 67)    kb_index -= 11;
    else if(kb_index == 68)    kb_index = 59;
    else if(kb_index == 69)    kb_index = 62;
    else if(kb_index >= 45 && kb_index <= 52)    kb_index -= 10;
    else if(kb_index >= 54 && kb_index <= 63)    kb_index -= 9;

	//	'123' Virtual Keyboard
    else if(kb_index == 92)    kb_index = 83;
    else if(kb_index == 93)    kb_index = 84;
    else if(kb_index == 94)    kb_index = 86;
    else if(kb_index == 95)    kb_index = 88;
    else if(kb_index == 96)    kb_index = 89;
    else if(kb_index >= 97 && kb_index <= 99)    kb_index = 90;
    else if(kb_index == 100 || kb_index == 101)    kb_index = 91;
    else if(kb_index == 102)    kb_index = 93;
    else if(kb_index == 103)    kb_index = 96;
    else if(kb_index >= 80 && kb_index <= 91)    kb_index -= 10;

	// '#+=' Virtual Keyboard
    else if(kb_index == 114)    kb_index = 106;
    else if(kb_index == 119)    kb_index = 104;
    else if(kb_index == 125)    kb_index = 113;
    else if(kb_index >= 126 && kb_index <= 128)    kb_index = 119;
    else if(kb_index == 129 || kb_index == 130)    kb_index = 120;
    else if(kb_index == 131)    kb_index = 122;
    else if(kb_index == 132)    kb_index = 125;
    else if(kb_index >= 115 && kb_index <= 118)    kb_index -= 7;
    else if(kb_index >= 120 && kb_index <= 124)    kb_index -= 6;

    ST7565RectReverse(&_aButtonData[kb_index].recs, VERTICAL);
    return _aButtonData[kb_index].c;
}



char VK_char_num_sym_down(void)
{
	ST7565RectReverse(&_aButtonData[kb_index].recs, VERTICAL);

    if(kb_index == 9)    kb_index = 18;
    else if(kb_index == 20)    kb_index = 31;
    else if(kb_index == 21)    kb_index = 32;
    else if((kb_index >= 22) && (kb_index <= 26))    kb_index = 33;
    else if(kb_index == 27)    kb_index = 34;
    else if(kb_index <= 8)    kb_index += 10;
    else if((kb_index >= 10) && (kb_index <= 19))    kb_index += 9;

	//	'abc' Virtual Keyboard
    else if(kb_index == 44)    kb_index = 53;
    else if(kb_index == 55)    kb_index = 66;
    else if(kb_index == 56)    kb_index = 67;
    else if((kb_index >= 57) && (kb_index <= 61))    kb_index = 68;
    else if(kb_index == 62)    kb_index = 69;
    else if((kb_index >= 35) && (kb_index <= 43))    kb_index += 10;
    else if((kb_index >= 45) && (kb_index <= 54))    kb_index += 9;

	//	'123' Virtual Keyboard
    else if(kb_index == 82)    kb_index = 91;
    else if(kb_index == 83)    kb_index = 92;
    else if(kb_index == 84 || kb_index == 85)    kb_index = 93;
    else if(kb_index == 86)    kb_index = 94;
    else if(kb_index == 87 || kb_index == 88)    kb_index = 95;
    else if(kb_index == 89)    kb_index = 96;
    else if(kb_index == 90)    kb_index = 97;
    else if(kb_index == 91)    kb_index = 101;
    else if(kb_index >= 92 && kb_index <= 95)    kb_index = 102;
    else if(kb_index == 96)    kb_index = 103;
    else if(kb_index >= 70 && kb_index <= 81)    kb_index += 10;

	//	'#+=' Virtual Keyboard
    else if(kb_index == 104 || kb_index == 105)    kb_index = 119;
    else if(kb_index == 106 || kb_index == 107)    kb_index = 114;
    else if(kb_index == 112 || kb_index == 113)    kb_index = 125;
    else if(kb_index == 119)    kb_index = 126;
    else if(kb_index == 120)    kb_index = 130;
    else if(kb_index >= 121 && kb_index <= 124)    kb_index = 131;
    else if(kb_index == 125)    kb_index = 132;
    else if(kb_index >= 108 && kb_index <= 111)    kb_index += 7;
    else if(kb_index >= 114 && kb_index <= 118)    kb_index += 6;

	ST7565RectReverse(&_aButtonData[kb_index].recs, VERTICAL);
    return _aButtonData[kb_index].c;
}

char VK_char_num_sym_left(void)
{
	ST7565RectReverse(&_aButtonData[kb_index].recs, VERTICAL);

	if(kb_index == 0)    kb_index = 34;
	else if(kb_index == 35)    kb_index = 69;
	else if(kb_index == 70)    kb_index = 103;
	else if(kb_index == 104)    kb_index = 132;
	else kb_index--;

	ST7565RectReverse(&_aButtonData[kb_index].recs, VERTICAL);
    return _aButtonData[kb_index].c;
}

char VK_char_num_sym_right(void)
{
	ST7565RectReverse(&_aButtonData[kb_index].recs, VERTICAL);

	if(kb_index == 34)    kb_index = 0;
	else if(kb_index == 69)    kb_index = 35;
	else if(kb_index == 103)    kb_index = 70;
	else if(kb_index == 132)    kb_index = 104;
	else    kb_index++;

	ST7565RectReverse(&_aButtonData[kb_index].recs, VERTICAL);
    return _aButtonData[kb_index].c;
}

char VK_char_num_sym_sel(void)
{
    uint16_t i;
    
    switch(_aButtonData[kb_index].c){
    case VK_KEY_LEFT:
    case VK_KEY_UP:
    case VK_KEY_RIGHT:
    case VK_KEY_DOWN:
        return _aButtonData[kb_index].c;
    case VK_KEY_Fn_abc:
        keyboard_page = 1;
        kb_index = 35;
        break;
    case VK_KEY_Fn_sym:
        keyboard_page = 3;
        kb_index = 104;
        break;
    case VK_KEY_Fn_ABC:
        keyboard_page = 0;
        kb_index = 0;
        break;
    case VK_KEY_Fn_123:
        keyboard_page = 2;
        kb_index = 70;
        break;
    case VK_KEY_BACKSPACE:
        i = strlen(vk_buf);
        if(i){
            vk_buf[i-1] = '\0';
        }
        VKInputUpdate();
        return 0;
    case VK_KEY_ENTER:
        if(VK_info.cb) VK_info.cb(vk_buf, strlen(vk_buf));
        return 0;
    default:
        i = strlen(vk_buf);
        if(i < VK_info.len){
            vk_buf[i] = _aButtonData[kb_index].c;
            VKInputUpdate();
            return 0;
        }
        break;
    }

    VK_char_num_sym_display();
    return 0;
}


void VK_CNS_Cursor_blink_V(void)
{    
    if(strlen(vk_buf) >= VK_info.len) return;
    tRectangle rec;
    uint8_t len = strlen(vk_buf);
	rec.i16XMin = VK_info.start_x+len*VK_info.char_width;
	rec.i16XMax = rec.i16XMin+CURSOR_WIDTH-1;
	rec.i16YMin = VK_info.start_y;
	rec.i16YMax = rec.i16YMin+CURSOR_HEIGHT-1;
    ST7565RectReverse(&rec, VERTICAL);
}


