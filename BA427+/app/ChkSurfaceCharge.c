//*****************************************************************************
//
// Source File: ChkSurfaceCharge.c
// Description: Check surface charge for Battery and start-stop test.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 09/21/17 	Jerry.W	       Created file.
// 09/21/17 	Jerry.W		   Rewrite Widget for new software architecture
// 11/06/17     Henry.Y        1. Set the surface charge criteria of different test and battery type.
// 03/22/18     William.H      Modify ChkSurfaceCharge_sub_W[] to enter to CountDown15S_widget directly if its Test-In-Vehcile. 
// 09/05/18     Henry.Y        Merge CountDown15S widget. 
//                             Modify flow to avoid widget stack overflow.
// 10/17/18     Henry.Y        Add __BO__ identifier for no judge surface charge.
// 12/07/18     Henry.Y        New display api.
// 10/19/20     David Wang     Turn headlights countdown 15sec Picture to text
// 20201204     D.W.           The modification process is the same as BT2010
//
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/app/ChkSurfaceCharge.c#5 $
// $DateTime: 2017/09/22 10:33:48 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#if 0
#include "Widgetmgr.h"


void ChkSurfaceCharge_SERVICE();
void ChkSurfaceCharge_DISPLAY();
void ChkSurfaceCharge_routine();
void CountDown15S_Timer_callback();
void ChkSurfaceCharge_SELECT();

static float SurfaceCharge_V_start;
uint32_t ui32Voltage_scale = 0;
int16_t Old_counter = 0;

widget* ChkSurfaceCharge_sub_W[] = {
		//&Measuring_widget,     //0
        //&PointToBAT_widget,    //0
        &BatTemp_widget,         //0
        &AbortTest_widget,       //1
		&TestInVehicle_widget    //2
};

widget ChkSurfaceCharge_widget = {
		.Key_UP = 0,
		.Key_DOWN = 0,
		.Key_RIGHT = 0,
		.Key_LEFT = 0,
		.Key_SELECT = ChkSurfaceCharge_SELECT,
		.routine = ChkSurfaceCharge_routine,
		.service = ChkSurfaceCharge_SERVICE,
		.display = ChkSurfaceCharge_DISPLAY,
		.subwidget = ChkSurfaceCharge_sub_W,
		.number_of_widget = sizeof(ChkSurfaceCharge_sub_W)/4,
		.index = 0,
		.no_messgae = 1,
		.name = "ChkSurfaceCharge",
};


void ChkSurfaceCharge_SELECT(){
    if(ChkSurfaceCharge_widget.parameter == 2 && g_15SecCDCounter == 0){
        ChkSurfaceCharge_widget.parameter = 0;
        g_15SecCDCounter = 15;
        ChkSurfaceCharge_SERVICE();
    }
}


void ChkSurfaceCharge_DISPLAY(){
    char buf[20];

    if(ChkSurfaceCharge_widget.parameter==0) return;
	// Clear screen
	ST7565ClearScreen(BUFFER1);
	ST7565Refresh(BUFFER1);
	if(g_15SecCDCounter > 0){
	    // Write SCREEN
	    LCD_String_ext(16,get_Language_String(S_TURN_HEADLIGHTS), Align_Left);
	    // COUNT DOWN 15SEC
        sprintf( buf, "%s", get_Language_String(S_ON_FOR_15SEC));
        char* sec = strstr(buf, "1");
        sec[0] = ('0' + (g_15SecCDCounter/10));
        sec[1] = ((g_15SecCDCounter%10) + '0');
	    LCD_String_ext(32, buf, Align_Central);}
	else{
	    LCD_String_ext(16,get_Language_String(S_TURN_HEADLIGHTS_1), Align_Left);
	    LCD_String_ext(32,get_Language_String(S_OFF_1), Align_Central);
	    LCD_String_ext(48,get_Language_String(S_PRESS_ENTER), Align_Central);
	}
}


void ChkSurfaceCharge_SERVICE(){

	if(ChkSurfaceCharge_widget.parameter == 0){
		float test_voltage = 0;
		uint32_t ui32Voltage_scale = 0;
		uint16_t ui16SCcriteria = 0;
		SYS_PAR* par = Get_system_parameter();
		TEST_DATA* TD = Get_test_data();
		ROM_SysCtlDelay(get_sys_clk()/3);
		
		BattCoreGetVoltage(&ui32Voltage_scale, &test_voltage);
		
		if(par->WHICH_TEST == Start_Stop_Test){
			if(par->SS_test_par.SSBatteryType == 4) ui16SCcriteria = 1290;//12.9
			else ui16SCcriteria = 1300; 	
		}
		else if(par->WHICH_TEST == Battery_Test){			
			if(par->BAT_test_par.BatteryType == 0) ui16SCcriteria = 1290;
			else if(par->BAT_test_par.BatteryType == 1 || par->BAT_test_par.BatteryType == 3){
				ui16SCcriteria = 1300;
			}
			else ui16SCcriteria = 1310; 			
		}
		//CHECK BAT VOLTAGE
		if(TD->SYSTEM_CHANNEL == MAP_6V){
			TD->Surface_charge = ((test_voltage*100) >= (ui16SCcriteria/2))? 1:0;
		}
		else{
			TD->Surface_charge = ((test_voltage*100) >= (ui16SCcriteria))? 1:0;
		}

		if(TD->Surface_charge == 0){
			Move_widget(ChkSurfaceCharge_sub_W[0],0);
		}
		else{
		    Move_widget(ChkSurfaceCharge_sub_W[2],0);  // go to TestInVehicle_widget
		}

	}
	else if(ChkSurfaceCharge_widget.parameter == 1){
		// detect voltage drop
	    BattCoreGetVoltage(&ui32Voltage_scale, &SurfaceCharge_V_start);
	}
	else{
		   // count down 15 sec
		   //if(g_15SecCDCounter == 0){
           //ChkSurfaceCharge_widget.parameter = 0;
           //g_15SecCDCounter = 15;
           //ChkSurfaceCharge_SERVICE();
		   //}
	}
}


void ChkSurfaceCharge_routine(){

	float SurfaceCharge_V_test;
	float delta_SurfaceCharge_V;
	float SurfaceCharge_V_thresold;	
	
	TEST_DATA* TD = Get_test_data();

	if(ChkSurfaceCharge_widget.parameter == 0){
		g_15SecCDCounter = 15;
		ChkSurfaceCharge_widget.parameter = 1;
		ChkSurfaceCharge_DISPLAY();	
		BattCoreGetVoltage(&ui32Voltage_scale, &SurfaceCharge_V_start);
	}
	else if(ChkSurfaceCharge_widget.parameter == 1){
		SurfaceCharge_V_thresold = (TD->SYSTEM_CHANNEL == MAP_6V)? 0.05:0.1;
		
		BattCoreGetVoltage(&ui32Voltage_scale, &SurfaceCharge_V_test);
		delta_SurfaceCharge_V = SurfaceCharge_V_start - SurfaceCharge_V_test;
		if(delta_SurfaceCharge_V >= SurfaceCharge_V_thresold){
			ChkSurfaceCharge_widget.parameter = 2;
			set_countdown_callback(CountDown15S_Timer_callback, 15);
		}
	}
	else{

	    if(Old_counter != g_15SecCDCounter ){
	        Old_counter = g_15SecCDCounter;
	        ChkSurfaceCharge_DISPLAY();
	    }

	        //if(g_15SecCDCounter == 0){
	        //ChkSurfaceCharge_widget.parameter = 0;
	        //g_15SecCDCounter = 15;
	        //ChkSurfaceCharge_SERVICE();
		    //}
	}
}


void CountDown15S_Timer_callback(){

}
#endif

