//*****************************************************************************
//
// Source File: MFC_widget.c
// Description: The widget about factory test.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 10/19/18     Henry.Y        Create file.
// 11/19/18     Henry.Y        MFC LCD/button/voltage/peripheral function check.
// ============================================================================
//
//
// $Header$
// $DateTime$
// $Author$
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#include "Widgetmgr.h"

//////////////////////////Y=0
//const XY_DISPLAY MFC_backlight = 				{"LCD TESTING",0,0};
//const XY_DISPLAY MFC_SWITCH = 					{"SWITCH TESTING",0,0};
//const XY_DISPLAY MFC_VOLTAGE = 					{"VOLT. TEST",0,0};
//const XY_DISPLAY MFC_CLOCK_IC = 				{"CLOCK-IC TEST",0,0};
//const XY_DISPLAY MFC_STEP_MOTOR = 				{"STEP MOTOR TEST",0,0};
//const XY_DISPLAY MFC_LOAD_ON =	 				{"LOAD ON TEST",0,0};

//////////////////////////Y=9


//////////////////////////Y=18
//const XY_DISPLAY MFC_backlight_check_blue = 	{"COLOR BLUE?",0,18};
//const XY_DISPLAY MFC_backlight_check_green = 	{"COLOR GREEN?",0,18};
//const XY_DISPLAY MFC_backlight_check_orange = 	{"COLOR ORANGE?",0,18};
//const XY_DISPLAY MFC_backlight_check_red = 		{"COLOR RED?",0,18};
//const XY_DISPLAY MFC_SWITCH_LEFT =				{"PRESS LEFT",0,18};
//const XY_DISPLAY MFC_SWITCH_UP =				{"PRESS UP",0,18};
//const XY_DISPLAY MFC_SWITCH_RIGHT =				{"PRESS RIGHT",0,18};
//const XY_DISPLAY MFC_SWITCH_DOWN =				{"PRESS DOWN",0,18};
//const XY_DISPLAY MFC_SWITCH_ENTER =				{"PRESS ENTER",0,18};
//const XY_DISPLAY MFC_VOLTAGE_6V = 				{"SWITCH 6V(5~7V)",0,18};
//const XY_DISPLAY MFC_VOLTAGE_12V = 				{"SWITCH 12V(11~13V)",0,18};
//const XY_DISPLAY MFC_VOLTAGE_24V = 				{"SWITCH 24V(23~25V)",0,18};
//const XY_DISPLAY MFC_VOLTAGE_INT = 				{"INTERNAL VOLT. CHECK",0,18};
//const XY_DISPLAY MFC_VOLTAGE_VM = 				{"VM CHECK",0,18};
//const XY_DISPLAY MFC_VOLTAGE_AM = 				{"AM CHECK",0,18};
//const XY_DISPLAY MFC_function_check = 			{"FUNCTION CHECKING...",0,18};
//const XY_DISPLAY MFC_LOAD = 					{"SWITCH LOAD/PRINTER",0,18};
//const XY_DISPLAY MFC_MOTOR_WORKS = 				{"STEP MOTOR WORKS?",0,18};



//////////////////////////Y=27
//const XY_DISPLAY MFC_PRESS_ENT_TO = 			{"PRESS ENTER TO",0,27};
//const XY_DISPLAY MFC_BUTTON_PASS = 				{"YES: PRESS ENTER",0,27};

//////////////////////////Y=36
//const XY_DISPLAY MFC_START_TEST = 				{"START TESTING",0,36};
//const XY_DISPLAY MFC_NEXT = 					{"NEXT TEST",0,36};
//const XY_DISPLAY MFC_BUTTON_FAIL = 				{"NO: PRESS OTHERS",0,36};

//////////////////////////Y=45


//////////////////////////Y=54



/////////////////////////

typedef enum{
	start_up = 0,
	switch_left,
	switch_up,
	switch_right,
	switch_down,
	switch_enter,
	backlight_check_blue,
	backlight_check_green,
	backlight_check_orange,
	backlight_check_red,

	voltage_6V,
	voltage_12V,
	voltage_24V,
	voltage_int,
	voltage_VM,
	voltage_AM,
	clock_IC,
	//flash_SPI,
	step_motor,
	load_on,
	MFC_test_result,
	MFC_ERR
}MFC_flow;

typedef enum{
	first_show = 0,
	second_test
}STATE;




void MFC_UP();
void MFC_DOWN();
void MFC_RIGHT();
void MFC_LEFT();
void MFC_SELECT();
void MFC_service();
void MFC_initail();
void MFC_DISPLAY();
void MFC_routine();


static STATE state = first_show;
static float g_fbuf_0,g_fbuf_1;
static uint32_t g_ui32buf = 0;
static uint16_t MFC_year;			
static uint8_t MFC_month,MFC_date,MFC_day,MFC_hour,MFC_min,MFC_sec;

widget MFC_widget = {
		.Key_UP = MFC_UP,
		.Key_DOWN = MFC_DOWN,
		.Key_RIGHT = MFC_RIGHT,
		.Key_LEFT = MFC_LEFT,
		.Key_SELECT = MFC_SELECT,
		.routine = MFC_routine,
		.service = MFC_service,
		.initial = MFC_initail,
		.display = MFC_DISPLAY,
		.subwidget = 0,
		.number_of_widget = 0,
		.index = 0,
		.no_messgae = 1,
};




void MFC_initail(){
	IntBat_SW_ON;
	if(get_routine_switch()){
		Main_routine_switch(0);
	}


}



void MFC_UP(){
	MFC_TD* MT = Get_MFC_TEST_DATA();
	uint8_t *temp_ptr;
	if(MFC_widget.parameter == switch_up){
		MT->switch_up = 1;
		if(save_MFC_TEST_DATA(&MT->switch_up, sizeof(MT->switch_up)) == false) MFC_widget.parameter = MFC_ERR;
		else MFC_widget.parameter++;
	}
	else if(MFC_widget.parameter == backlight_check_blue ||
			MFC_widget.parameter == backlight_check_green ||
			MFC_widget.parameter == backlight_check_orange ||
			MFC_widget.parameter == backlight_check_red ||
			MFC_widget.parameter == switch_left || 
			MFC_widget.parameter == switch_enter || 
			MFC_widget.parameter == switch_right || 
			MFC_widget.parameter == switch_down || 
			MFC_widget.parameter == step_motor){
		temp_ptr = (&MT->start_up)+(MFC_widget.parameter)*(sizeof(uint8_t));
		(*temp_ptr) = 0;
		if(save_MFC_TEST_DATA(temp_ptr, sizeof(uint8_t)) == false) MFC_widget.parameter = MFC_ERR;
		else MFC_widget.parameter++;
	}
	else{
		return;
	}	
	MFC_service();	
}


void MFC_DOWN(){
	MFC_TD* MT = Get_MFC_TEST_DATA();
	uint8_t *temp_ptr;
	if(MFC_widget.parameter == switch_down){
		MT->switch_down = 1;		
		if(save_MFC_TEST_DATA(&MT->switch_down, sizeof(MT->switch_down)) == false) MFC_widget.parameter = MFC_ERR;
		else MFC_widget.parameter++;
	}
	else if(MFC_widget.parameter == backlight_check_blue ||
			MFC_widget.parameter == backlight_check_green ||
			MFC_widget.parameter == backlight_check_orange ||
			MFC_widget.parameter == backlight_check_red ||
			MFC_widget.parameter == switch_left ||
			MFC_widget.parameter == switch_enter || 
			MFC_widget.parameter == switch_right || 
			MFC_widget.parameter == switch_up || 
			MFC_widget.parameter == step_motor){
		temp_ptr = (&MT->start_up)+(MFC_widget.parameter)*(sizeof(uint8_t));
		(*temp_ptr) = 0;
		if(save_MFC_TEST_DATA(temp_ptr, sizeof(uint8_t)) == false) MFC_widget.parameter = MFC_ERR;
		else MFC_widget.parameter++;
	}
	else{
		return;
	}
	MFC_service();	
}


void MFC_RIGHT(){
	MFC_TD* MT = Get_MFC_TEST_DATA();
	uint8_t *temp_ptr;
	if(MFC_widget.parameter == start_up){
		MFC_widget.index = 1;
		MFC_widget.parameter = MFC_test_result;		
	}
	else if(MFC_widget.parameter == backlight_check_blue ||
			MFC_widget.parameter == backlight_check_green ||
			MFC_widget.parameter == backlight_check_orange ||
			MFC_widget.parameter == backlight_check_red ||
			MFC_widget.parameter == switch_left ||
			MFC_widget.parameter == switch_enter ||
			MFC_widget.parameter == switch_down ||
			MFC_widget.parameter == switch_up || 
			MFC_widget.parameter == step_motor){
		temp_ptr = (&MT->start_up)+(MFC_widget.parameter)*(sizeof(uint8_t));
		(*temp_ptr) = 0;
		if(save_MFC_TEST_DATA(temp_ptr, sizeof(uint8_t)) == false) MFC_widget.parameter = MFC_ERR;
		else MFC_widget.parameter++;
	}
	else if(MFC_widget.parameter == switch_right){
		MT->switch_right = 1;
		if(save_MFC_TEST_DATA(&MT->switch_right, sizeof(MT->switch_right)) == false) MFC_widget.parameter = MFC_ERR;
		else MFC_widget.parameter++;
	}
	else if(MFC_widget.parameter == MFC_test_result){
		if(MFC_widget.index){
			MFC_widget.index = 0;
			MFC_widget.parameter = start_up;
		}
	}
	else{
		return;
	}
	MFC_service();	
}


void MFC_LEFT(){
	MFC_TD* MT = Get_MFC_TEST_DATA();
	uint8_t *temp_ptr;
	if(MFC_widget.parameter == start_up){
		MFC_widget.index = 1;
		MFC_widget.parameter = MFC_test_result;		
	}
	else if(MFC_widget.parameter == backlight_check_blue ||
			MFC_widget.parameter == backlight_check_green || 
			MFC_widget.parameter == backlight_check_orange || 
			MFC_widget.parameter == backlight_check_red ||
			MFC_widget.parameter == switch_right ||
			MFC_widget.parameter == switch_enter ||
			MFC_widget.parameter == switch_down ||
			MFC_widget.parameter == switch_up || 
			MFC_widget.parameter == step_motor){

		temp_ptr = (&MT->start_up)+(MFC_widget.parameter)*(sizeof(uint8_t));
		(*temp_ptr) = 0;
		if(save_MFC_TEST_DATA(temp_ptr, sizeof(uint8_t)) == false) MFC_widget.parameter = MFC_ERR;
		else MFC_widget.parameter++;
	}
	else if(MFC_widget.parameter == switch_left){
		MT->switch_left = 1;
		if(save_MFC_TEST_DATA(&MT->switch_left, sizeof(MT->switch_left)) == false) MFC_widget.parameter = MFC_ERR;
		else MFC_widget.parameter++;
	}
	else if(MFC_widget.parameter == MFC_test_result){
		if(MFC_widget.index){
			MFC_widget.index = 0;
			MFC_widget.parameter = start_up;
		}
	}
	else{
		return;
	}
	MFC_service();	
}


void MFC_DISPLAY(){
	TEST_DATA* TD = Get_test_data();
	MFC_TD* MT = Get_MFC_TEST_DATA();

	char ui8buf[21] = {0x00};

	ST7565ClearScreen(BUFFER0);
	ST7565Refresh(BUFFER0);

	switch(MFC_widget.parameter){
	case start_up:
		if(MT->start_up == 1){
		ST7650Blue();
		}
		else if(MT->start_up == 99){
			ST7650Yellow();
		}
		else{
			ST7650Red();
		}
		ST7565DrawStringx16(0, 9, strFWver, Font_System5x8);

		snprintf(ui8buf,sizeof(ui8buf),"HW VER: V%02d", (uint8_t)get_hardware_version());
		ST7565DrawStringx16(0, 18, (uint8_t*)ui8buf, Font_System5x8);

		// TODO: flash SPI test
		ST7565DrawStringx16(0, 27, "PRESS ENTER TO", Font_System5x8);
		ST7565DrawStringx16(0, 36, "START TESTING", Font_System5x8);
		ST7565DrawStringx16(0, 45, "<PRESS L/R SEE LAST>", Font_System5x8);
		break;
	case backlight_check_blue:
		ST7650Blue();
		ST7565DrawStringx16(0,  0, "LCD TESTING", Font_System5x8);
		ST7565DrawStringx16(0, 18, "COLOR BLUE?", Font_System5x8);
		ST7565DrawStringx16(0, 27, "YES: PRESS ENTER", Font_System5x8);
		ST7565DrawStringx16(0, 36, "NO: PRESS OTHERS", Font_System5x8);
		break;
	case backlight_check_green:
		ST7650Green();
		ST7565DrawStringx16(0,  0, "LCD TESTING", Font_System5x8);
		ST7565DrawStringx16(0, 18, "COLOR GREEN?", Font_System5x8);
		ST7565DrawStringx16(0, 27, "YES: PRESS ENTER", Font_System5x8);
		ST7565DrawStringx16(0, 36, "NO: PRESS OTHERS", Font_System5x8);
		break;		
	case backlight_check_orange:
		ST7650Yellow();
		ST7565DrawStringx16(0,  0, "LCD TESTING", Font_System5x8);
		ST7565DrawStringx16(0, 18, "COLOR ORANGE?", Font_System5x8);
		ST7565DrawStringx16(0, 27, "YES: PRESS ENTER", Font_System5x8);
		ST7565DrawStringx16(0, 36, "NO: PRESS OTHERS", Font_System5x8);
		break;
	case backlight_check_red:
		ST7650Red();
		ST7565DrawStringx16(0,  0, "LCD TESTING", Font_System5x8);
		ST7565DrawStringx16(0, 18, "COLOR RED?", Font_System5x8);
		ST7565DrawStringx16(0, 27, "YES: PRESS ENTER", Font_System5x8);
		ST7565DrawStringx16(0, 36, "NO: PRESS OTHERS", Font_System5x8);
		break;

	case switch_left:
		ST7650Blue();
		ST7565DrawStringx16(0,  0, "SWITCH TESTING", Font_System5x8);
		ST7565DrawStringx16(0, 18, "PRESS LEFT", Font_System5x8);
		break;
	case switch_up:
		ST7650Blue();
		ST7565DrawStringx16(0,  0, "SWITCH TESTING", Font_System5x8);
		ST7565DrawStringx16(0, 18, "PRESS UP", Font_System5x8);
		break;
	case switch_right:
		ST7650Blue();
		ST7565DrawStringx16(0,  0, "SWITCH TESTING", Font_System5x8);
		ST7565DrawStringx16(0, 18, "PRESS RIGHT", Font_System5x8);
		break;
	case switch_down:
		ST7650Blue();
		ST7565DrawStringx16(0,  0, "SWITCH TESTING", Font_System5x8);
		ST7565DrawStringx16(0, 18, "PRESS DOWN", Font_System5x8);
		break;
	case switch_enter:
		ST7650Blue();
		ST7565DrawStringx16(0,  0, "SWITCH TESTING", Font_System5x8);
		ST7565DrawStringx16(0, 18, "PRESS ENTER", Font_System5x8);
		break;

	case voltage_6V:
		ST7565DrawStringx16(0,  0, "VOLT. TEST", Font_System5x8);
		ST7565DrawStringx16(0, 18, "SWITCH 6V(5~7V)", Font_System5x8);
		ST7565DrawStringx16(0, 27, "PRESS ENTER TO", Font_System5x8);
		if(state == first_show){
			ST7650Blue();
			ST7565DrawStringx16(0, 36, "START TESTING", Font_System5x8);
		}
		else{
			ST7565DrawStringx16(0, 36, "NEXT TEST", Font_System5x8);
		}
		break;
	case voltage_12V:
		ST7565DrawStringx16(0,  0, "VOLT. TEST", Font_System5x8);
		ST7565DrawStringx16(0, 18, "SWITCH 12V(11~13V)", Font_System5x8);
		ST7565DrawStringx16(0, 27, "PRESS ENTER TO", Font_System5x8);
		if(state == first_show){
			ST7650Blue();
			ST7565DrawStringx16(0, 36, "START TESTING", Font_System5x8);
		}
		else{
			ST7565DrawStringx16(0, 36, "NEXT TEST", Font_System5x8);
		}
		break;
	case voltage_24V:
		ST7565DrawStringx16(0,  0, "VOLT. TEST", Font_System5x8);
		ST7565DrawStringx16(0, 18, "SWITCH 24V(23~25V)", Font_System5x8);
		ST7565DrawStringx16(0, 27, "PRESS ENTER TO", Font_System5x8);
		if(state == first_show){
			ST7650Blue();
			ST7565DrawStringx16(0, 36, "START TESTING", Font_System5x8);
		}
		else{
			ST7565DrawStringx16(0, 36, "NEXT TEST", Font_System5x8);
		}
		break;
	case voltage_int:
		ST7565DrawStringx16(0,  0, "VOLT. TEST", Font_System5x8);
		ST7565DrawStringx16(0, 18, "INTERNAL VOLT. CHECK", Font_System5x8);
		ST7565DrawStringx16(0, 27, "PRESS ENTER TO", Font_System5x8);
		if(state == first_show){
			ST7650Blue();
			ST7565DrawStringx16(0, 36, "START TESTING", Font_System5x8);
		}
		else{
			ST7565DrawStringx16(0, 36, "NEXT TEST", Font_System5x8);
		}
		break;
	case voltage_VM:
		ST7565DrawStringx16(0,  0, "VOLT. TEST", Font_System5x8);
		ST7565DrawStringx16(0, 18, "VM CHECK", Font_System5x8);
		ST7565DrawStringx16(0, 27, "PRESS ENTER TO", Font_System5x8);
		if(state == first_show){
			ST7650Blue();
			ST7565DrawStringx16(0, 36, "START TESTING", Font_System5x8);
		}
		else{
			ST7565DrawStringx16(0, 36, "NEXT TEST", Font_System5x8);
		}
		break;
	case voltage_AM:
		ST7565DrawStringx16(0,  0, "VOLT. TEST", Font_System5x8);
		ST7565DrawStringx16(0, 18, "AM CHECK", Font_System5x8);
		ST7565DrawStringx16(0, 27, "PRESS ENTER TO", Font_System5x8);
		if(state == first_show){
			ST7650Blue();
			ST7565DrawStringx16(0, 36, "START TESTING", Font_System5x8);
		}
		else{
			ST7565DrawStringx16(0, 36, "NEXT TEST", Font_System5x8);
		}
		break;
	case clock_IC:
	{
		ST7565DrawStringx16(0,  0, "CLOCK-IC TEST", Font_System5x8);

		if(state == first_show){
			ST7650Blue();
			ST7565DrawStringx16(0,18, "FUNCTION CHECKING...", Font_System5x8);
			state = second_test;
		}
		else{
			ST7650Red();
			ST7565DrawStringx16(0, 27, "PRESS ENTER TO", Font_System5x8);
			ST7565DrawStringx16(0, 36, "NEXT TEST", Font_System5x8);
			snprintf(ui8buf,sizeof(ui8buf),"1:%d/%d/%d,%d:%d:%d",
					MFC_year%100,MFC_month,MFC_date,MFC_hour,MFC_min,MFC_sec);//1:2099/12/31,23:59:59
			ST7565DrawStringx16(0, 45, (uint8_t*)ui8buf, Font_System5x8);
			memset(ui8buf,0,sizeof(ui8buf));
			snprintf(ui8buf,sizeof(ui8buf),"2:%d/%d/%d,%d:%d:%d",
					TD->year%100,TD->month,TD->date,TD->hour,TD->min,TD->sec);//2:2098/11/30,22:58:58
			ST7565DrawStringx16(0, 45, (uint8_t*)ui8buf, Font_System5x8);
		}
		return;
	}

	case step_motor:
		ST7650Blue();
		ST7565DrawStringx16(0,  0, "STEP MOTOR TEST", Font_System5x8);
		if(state == first_show){
			ST7565DrawStringx16(0, 18, "SWITCH LOAD/PRINTER", Font_System5x8);
			ST7565DrawStringx16(0, 27, "PRESS ENTER TO", Font_System5x8);
			ST7565DrawStringx16(0, 36, "START TESTING", Font_System5x8);
		}
		else{
			ST7565DrawStringx16(0, 18, "STEP MOTOR WORKS?", Font_System5x8);
			ST7565DrawStringx16(0, 27, "YES: PRESS ENTER", Font_System5x8);
			ST7565DrawStringx16(0, 36, "NO: PRESS OTHERS", Font_System5x8);
		}
		return;
	case load_on:
		ST7565DrawStringx16(0,  0, "LOAD ON TEST", Font_System5x8);
		ST7565DrawStringx16(0, 18, "SWITCH LOAD/PRINTER", Font_System5x8);
		ST7565DrawStringx16(0, 27, "PRESS ENTER TO", Font_System5x8);
		if(state == first_show){
			ST7650Blue();
			ST7565DrawStringx16(0, 36, "START TESTING", Font_System5x8);
		}
		else{
			ST7650Red();
			ST7565DrawStringx16(0, 36, "NEXT TEST", Font_System5x8);
		}
		break;
	case MFC_test_result:
		if(MFC_widget.index){
			snprintf(ui8buf,sizeof(ui8buf),"LAST TEST, %s", (MT->start_up == 1)? "PASS":(MT->start_up == 99)? "UNDO":"FAIL:");
		}
		else{
			snprintf(ui8buf,sizeof(ui8buf),"TEST OVER, %s", (MT->start_up)? "PASS":"FAIL:");
		}
		ST7565DrawStringx16(0, 0, (uint8_t*)ui8buf, Font_System5x8);
		memset(ui8buf,0,sizeof(ui8buf));

		if(MT->start_up == 1){
			ST7650Green();
			return;
		}
		else if(MT->start_up == 99){
			ST7650Yellow();
		}
		else{
			ST7650Red();			
		}

		
		// TODO: test result review
		// TEST OVER: FAIL   Y=0
		// LB:X GRN:X INT:X  Y=9
		// UB:X ORG:X VM:X   Y=18
		// RB:X RED:X AM:X   Y=27
		// DB:X  6V:X CLK:X  Y=36
		// EB:X 12V:X MOTOR:X  Y=45
		// BU:X 24V:X LOAD:X Y=54
		snprintf(ui8buf,sizeof(ui8buf),"LB:%c GRN:%c INT:%c",(MT->switch_left == 1)? ' ':(!MT->switch_left)? 'X':'?',
																(MT->backlight_check_green == 1)? ' ':(!MT->backlight_check_green)? 'X':'?',
																(MT->voltage_int == 1)? ' ':(!MT->voltage_int)? 'X':'?');
		ST7565DrawStringx16(0, 9, (uint8_t*)ui8buf, Font_System5x8);
		memset(ui8buf,0,sizeof(ui8buf));

		snprintf(ui8buf,sizeof(ui8buf),"UB:%c ORG:%c VM:%c",(MT->switch_up==1)? ' ':(!MT->switch_up)? 'X':'?',
																(MT->backlight_check_orange==1)? ' ':(!MT->backlight_check_orange)? 'X':'?',
																(MT->voltage_VM==1)? ' ':(!MT->voltage_VM)? 'X':'?');
		ST7565DrawStringx16(0, 18, (uint8_t*)ui8buf, Font_System5x8);
		memset(ui8buf,0,sizeof(ui8buf));

		snprintf(ui8buf,sizeof(ui8buf),"RB:%c RED:%c AM:%c",(MT->switch_right==1)? ' ':(!MT->switch_right)? 'X':'?',
															(MT->backlight_check_red==1)? ' ':(!MT->backlight_check_red)? 'X':'?',
															(MT->voltage_AM==1)? ' ':(!MT->voltage_AM)? 'X':'?');
		ST7565DrawStringx16(0, 27, (uint8_t*)ui8buf, Font_System5x8);
		memset(ui8buf,0,sizeof(ui8buf));

		snprintf(ui8buf,sizeof(ui8buf),"DB:%c  6V:%c CLK:%c",(MT->switch_down==1)? ' ':(!MT->switch_down)? 'X':'?',
																(MT->voltage_6V==1)? ' ':(!MT->voltage_6V)? 'X':'?',
																(MT->clock_IC==1)? ' ':(!MT->clock_IC)? 'X':'?');
		ST7565DrawStringx16(0, 36, (uint8_t*)ui8buf, Font_System5x8);
		memset(ui8buf,0,sizeof(ui8buf));

		snprintf(ui8buf,sizeof(ui8buf),"EB:%c 12V:%c MOTOR:%c",(MT->switch_enter==1)? ' ':(!MT->switch_enter)? 'X':'?',
																(MT->voltage_12V==1)? ' ':(!MT->voltage_12V)? 'X':'?',
																(MT->step_motor==1)? ' ':(!MT->step_motor)? 'X':'?');
		ST7565DrawStringx16(0, 45, (uint8_t*)ui8buf, Font_System5x8);
		memset(ui8buf,0,sizeof(ui8buf));

		snprintf(ui8buf,sizeof(ui8buf),"BU:%c 24V:%c LOAD:%c",(MT->backlight_check_blue==1)? ' ':(!MT->backlight_check_blue)? 'X':'?',
																(MT->voltage_24V==1)? ' ':(!MT->voltage_24V)? 'X':'?',
																(MT->load_on==1)? ' ':(!MT->load_on)? 'X':'?');
		ST7565DrawStringx16(0, 54, (uint8_t*)ui8buf, Font_System5x8);

		return;
	case MFC_ERR:
		ST7650Red();
		ST7565DrawStringx16(0, 9, "MCU EEPROM ERROR", Font_System5x8);
		while(1)
	default:
		return;
	}

	if(state == second_test){
		ST7650Red();
		if(MFC_widget.parameter == voltage_AM){
			snprintf(ui8buf,sizeof(ui8buf),"A: %.3f,%d",g_fbuf_0,g_ui32buf);
		}
		else if(MFC_widget.parameter == load_on){
			snprintf(ui8buf,sizeof(ui8buf),"MAX:%.3f,MIN:%.3F",g_fbuf_1,g_fbuf_0);
			
			ST7565DrawStringx16(0, 45, (uint8_t*)ui8buf, Font_System5x8);
			memset(ui8buf,0,sizeof(ui8buf));
			/*snprintf(ui8buf,sizeof(ui8buf),"LMX:%.3f,LMN:%.3F",g_fbuf_L1,g_fbuf_L0);
			sLen = strlen(ui8buf);
			for(l = 0; l < sLen; l++){
				ST7565Draw6x8Char(0 + l*6, 54, ui8buf[l], Font_System5x8);
			}*/
			return;
		}
		else{
			snprintf(ui8buf,sizeof(ui8buf),"FAIL:%6.3f,%d",g_fbuf_0, g_ui32buf);
		}
		ST7565DrawStringx16(0, 54, (uint8_t*)ui8buf, Font_System5x8);
	}
}


void MFC_service(){
	uint16_t paper_length;
	TEST_DATA* TD = Get_test_data();
	MFC_TD* MT = Get_MFC_TEST_DATA();
	uint8_t *temp_ptr;

	if((MFC_widget.parameter == voltage_6V || MFC_widget.parameter == voltage_12V || MFC_widget.parameter == voltage_24V || MFC_widget.parameter == voltage_VM) && state == second_test){
		if(MFC_widget.parameter == voltage_VM){
			GetVM(&g_fbuf_0,&g_ui32buf);
		}
		else{
			BattCoreGetVoltage(&g_ui32buf,&g_fbuf_0);
		}
		temp_ptr = (&MT->start_up)+(MFC_widget.parameter)*(sizeof(uint8_t));
		
		if( (MFC_widget.parameter == voltage_6V && (g_fbuf_0 >= 5 && g_fbuf_0 <= 7)) ||
			(MFC_widget.parameter == voltage_12V && (g_fbuf_0 >= 11 && g_fbuf_0 <= 13)) ||
			(MFC_widget.parameter == voltage_24V && (g_fbuf_0 >= 23 && g_fbuf_0 <= 25)) ||
			(MFC_widget.parameter == voltage_VM && ((g_fbuf_0 >= 11 && g_fbuf_0 <= 13) || (g_fbuf_0 >= 23 && g_fbuf_0 <= 25))) ){

			(*temp_ptr) = 1;			
			state = first_show;
			if(save_MFC_TEST_DATA(temp_ptr, sizeof(uint8_t)) == false) MFC_widget.parameter = MFC_ERR;
			else MFC_widget.parameter++;
		}
		else{
			(*temp_ptr) = 0;
		}		
	}
	else if(MFC_widget.parameter == voltage_int && state == second_test){
		BattCoreChkIntBatt(&g_ui32buf, &g_fbuf_0);
		if(g_fbuf_0 >= 8 && g_fbuf_0 <= 9){
			MT->voltage_int = 1;
			state = first_show;
			if(save_MFC_TEST_DATA(&MT->voltage_int, sizeof(MT->voltage_int)) == false) MFC_widget.parameter = MFC_ERR;
			else MFC_widget.parameter++;
		}
		else{
			MT->voltage_int = 0;
		}
	}
	else if(MFC_widget.parameter== voltage_AM && state == second_test){
		BattCoreScanVoltage(&g_fbuf_0);
		if(g_fbuf_0 > 10){
			GetAM(&g_fbuf_0,&g_ui32buf);
			if(g_fbuf_0 >= 200 && g_fbuf_0 <= 400){
				MT->voltage_AM = 1;
				state = first_show;
				if(save_MFC_TEST_DATA(&MT->voltage_AM, sizeof(MT->voltage_AM)) == false) MFC_widget.parameter = MFC_ERR;
				else MFC_widget.parameter++;
			}
			else{			
				MT->voltage_AM = 0;
			}
		}
		else{
			MT->voltage_AM = 0;
			g_ui32buf = 9999;			
		}
	}
	else if(MFC_widget.parameter == clock_IC && state == second_test){		
		ExtRTCRead( &MFC_year, &MFC_month, &MFC_date, &MFC_day, &MFC_hour, &MFC_min, &MFC_sec);
		delay_ms(1000); 		
		ExtRTCRead( &TD->year, &TD->month, &TD->date, &TD->day, &TD->hour, &TD->min, &TD->sec);
		if((TD->year == MFC_year) && (TD->month == MFC_month) && (TD->date == MFC_date) && (TD->day == MFC_day) && 
				(TD->hour == MFC_hour) && (TD->min == MFC_min) && ((TD->sec - MFC_sec) == 1)){

			MT->clock_IC = 1;
			state = first_show;
			if(save_MFC_TEST_DATA(&MT->clock_IC, sizeof(MT->clock_IC)) == false) MFC_widget.parameter = MFC_ERR;
			else MFC_widget.parameter++;
		}
		else{			
			MT->clock_IC = 0;
		}
	}
	else if(MFC_widget.parameter == step_motor){
		PowerOn();
		
		uint8_t ui8DatBuf[MP205_PRINTOUT_SIZE] = {0x00};
		MP205DataTransControl(ui8DatBuf);
		
		PrinterStepMotorControl(0, 1);
		for(paper_length = 0; paper_length <100; paper_length++)
		{
			BA6845StepMotorControl((paper_length % 4) + 1, 6);
		}
		PowerOff();


	}
	else if(MFC_widget.parameter == load_on){

		CCA_load_1_protect_OFF;
		CCA_load_1_ON;
		delay_ms(1.5);		
		BattCoreGetVoltage(&g_ui32buf, &g_fbuf_0);
		//BattCoreGetVLoad(&g_fbuf_L1, &g_fbuf_L0,15);

		CCA_load_1_OFF;
		CCA_load_1_protect_ON;
		delay_ms(2);
		BattCoreGetVoltage(&g_ui32buf, &g_fbuf_1);

		if((g_fbuf_0 >= g_fbuf_1) || ((g_fbuf_1-g_fbuf_0) < delta_voltage_CCA)){
			MT->load_on = 0;
		}
		else{
			MT->load_on = 1;
			state = first_show;
			
			if(save_MFC_TEST_DATA(&MT->load_on, sizeof(MT->load_on)) == false) MFC_widget.parameter = MFC_ERR;
			else MFC_widget.parameter++;
		}
	}
	else{
		//return;
	}
	if(MFC_widget.parameter == MFC_test_result){
		if(MFC_widget.index == 0){
			IntBat_SW_OFF;
			MT->start_up = (MT->switch_left &
							MT->switch_up &
							MT->switch_right &
							MT->switch_down &
							MT->switch_enter &
							MT->backlight_check_blue &
							MT->backlight_check_green &
							MT->backlight_check_orange &
							MT->backlight_check_red &
							MT->voltage_6V &
							MT->voltage_12V &
							MT->voltage_24V &
							MT->voltage_int &
							MT->voltage_VM &
							MT->voltage_AM &
							MT->clock_IC &
							MT->step_motor &
							MT->load_on);
			if(save_MFC_TEST_DATA(&MT->start_up, sizeof(MT->start_up)) == false) MFC_widget.parameter = MFC_ERR;
		}
	}

	MFC_DISPLAY();
}

void MFC_SELECT(){
	TEST_DATA* TD = Get_test_data();
	MFC_TD* MT = Get_MFC_TEST_DATA();
	switch(MFC_widget.parameter){
	case start_up:
		MFC_clear_eeprom_version();
		MFC_widget.parameter++;
		MFC_widget.index = 0;
		break;
	case backlight_check_blue:
		MT->backlight_check_blue = 1;
		if(save_MFC_TEST_DATA(&MT->backlight_check_blue, sizeof(MT->backlight_check_blue)) == false) MFC_widget.parameter = MFC_ERR;
		else MFC_widget.parameter++;
		break;
	case backlight_check_green:
		MT->backlight_check_green = 1;
		if(save_MFC_TEST_DATA(&MT->backlight_check_green, sizeof(MT->backlight_check_green)) == false) MFC_widget.parameter = MFC_ERR;
		else MFC_widget.parameter++;
		break;
	case backlight_check_orange:
		MT->backlight_check_orange = 1;
		if(save_MFC_TEST_DATA(&MT->backlight_check_orange, sizeof(MT->backlight_check_orange)) == false) MFC_widget.parameter = MFC_ERR;
		else MFC_widget.parameter++;
		break;
	case backlight_check_red:
		MT->backlight_check_red = 1;
		if(save_MFC_TEST_DATA(&MT->backlight_check_red, sizeof(MT->backlight_check_red)) == false) MFC_widget.parameter = MFC_ERR;
		else MFC_widget.parameter++;
		break;
	case switch_enter:
		MT->switch_enter = 1;
		if(save_MFC_TEST_DATA(&MT->switch_enter, sizeof(MT->switch_enter)) == false) MFC_widget.parameter = MFC_ERR;
		else MFC_widget.parameter++;
		break;
	case switch_left:
		MT->switch_left = 0;
		if(save_MFC_TEST_DATA(&MT->switch_left, sizeof(MT->switch_left)) == false) MFC_widget.parameter = MFC_ERR;
		else MFC_widget.parameter++;
		break;
	case switch_up:
		MT->switch_up = 0;
		if(save_MFC_TEST_DATA(&MT->switch_up, sizeof(MT->switch_up)) == false) MFC_widget.parameter = MFC_ERR;
		else MFC_widget.parameter++;
		break;
	case switch_right:
		MT->switch_right = 0;
		if(save_MFC_TEST_DATA(&MT->switch_right, sizeof(MT->switch_right)) == false) MFC_widget.parameter = MFC_ERR;
		else MFC_widget.parameter++;
		break;
	case switch_down:
		MT->switch_down = 0;
		if(save_MFC_TEST_DATA(&MT->switch_down, sizeof(MT->switch_down)) == false) MFC_widget.parameter = MFC_ERR;
		else MFC_widget.parameter++;
		break;
	case voltage_6V:
		if(state == first_show){
			TD->SYSTEM_CHANNEL = MAP_6V;
			state = second_test;
		}
		else{
			state = first_show;
			if(save_MFC_TEST_DATA(&MT->voltage_6V, sizeof(MT->voltage_6V)) == false) MFC_widget.parameter = MFC_ERR;
			else MFC_widget.parameter++;
		}
		break;
	case voltage_12V:
		if(state == first_show){
			TD->SYSTEM_CHANNEL = MAP_12V;
			state = second_test;
		}
		else{
			state = first_show;
			if(save_MFC_TEST_DATA(&MT->voltage_12V, sizeof(MT->voltage_12V)) == false) MFC_widget.parameter = MFC_ERR;
			else MFC_widget.parameter++;
		}
		break;
	case voltage_24V:
		if(state == first_show){
			TD->SYSTEM_CHANNEL = MAP_24V;
			state = second_test;
		}
		else{
			state = first_show;
			if(save_MFC_TEST_DATA(&MT->voltage_24V, sizeof(MT->voltage_24V)) == false) MFC_widget.parameter = MFC_ERR;
			else MFC_widget.parameter++;
		}
		break;
	case voltage_int:
		if(state == first_show){
			state = second_test;
		}
		else{
			state = first_show;
			if(save_MFC_TEST_DATA(&MT->voltage_int, sizeof(MT->voltage_int)) == false) MFC_widget.parameter = MFC_ERR;
			else MFC_widget.parameter++;
		}
		break;
	case voltage_VM:
		if(state == first_show){
			state = second_test;
		}
		else{
			state = first_show;
			if(save_MFC_TEST_DATA(&MT->voltage_VM, sizeof(MT->voltage_VM)) == false) MFC_widget.parameter = MFC_ERR;
			else MFC_widget.parameter++;
		}
		break;
	case voltage_AM:
		if(state == first_show){
			state = second_test;
		}
		else{
			state = first_show;
			if(save_MFC_TEST_DATA(&MT->voltage_AM, sizeof(MT->voltage_AM)) == false) MFC_widget.parameter = MFC_ERR;
			else MFC_widget.parameter++;
		}
		break;
	case clock_IC:
		state = first_show;
		if(save_MFC_TEST_DATA(&MT->clock_IC, sizeof(MT->clock_IC)) == false) MFC_widget.parameter = MFC_ERR;
		else MFC_widget.parameter++;
		break;
	case step_motor:
		if(state == first_show){
			state = second_test;
		}
		else{
			MT->step_motor = 1;
			state = first_show;
			if(save_MFC_TEST_DATA(&MT->step_motor, sizeof(MT->step_motor)) == false) MFC_widget.parameter = MFC_ERR;
			else MFC_widget.parameter++;
		}
		break;
	case load_on:
		if(state == first_show){
			state = second_test;
		}
		else{
			state = first_show;
			if(save_MFC_TEST_DATA(&MT->load_on, sizeof(MT->load_on)) == false) MFC_widget.parameter = MFC_ERR;
			else MFC_widget.parameter++;
		}
		break;
	default:
		return;
	}
	MFC_service();
}



void MFC_routine(){
	MFC_TD* MT = Get_MFC_TEST_DATA();
	static uint16_t count = 0;
	
	switch(MFC_widget.parameter){
	case voltage_6V:
	case voltage_12V:
	case voltage_24V:
	case voltage_int:
	case voltage_VM:
	case voltage_AM:
	case clock_IC:
		if(state == second_test){
			count = (count+1)%500;
			if(!count) break;
		}
		return;	
	default:
		return;
	}
	MFC_service();
}

