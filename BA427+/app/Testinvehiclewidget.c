//*****************************************************************************
//
// Source File: Testinvehiclewidget.c
// Description: Test In Vehicle widget to question Yes/No condition. 
//		If no, then directly go to "CALCCCASOCSOH" stage.
//		If yes, then go to "COUNTDOWN15SEC" and pop up "Turn On Headlight!
//                      Wait for 15 sec" countdown dialog.
//
//
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 10/16/15     William.H      Created file.
// 10/22/15     William.H      The pseudo code to present when hit 'No' in 'Test in Vehicle?' widget, it will display 'Print Animation P1/P2/P3/P4',
//                             to a).clear stack, b).push 'Main Menu' back to stack, c).re-draw 'Main Menu' screen on LCD d). Anti-white item.
// 10/29/15     Vincent.T      1. Add HY's part: Test in vechile: NO -> Testing...
//                             2. add HY's part: extern global variables SYSTEM_CHANNEL
//                             3. add HY's part: TestInVehicleClick() -> testing flow
// 10/30/15     William.H      1. Modify 'Test In Vehicle' screen, to adopt 'Icon' instead of 'Yes/No' dialog.
//                             2. Add the 'Turn On Headlights' screen to replace the string display.
// 11/04/15     Vincent.T      1.#include "mapsvc.h"
//                             2.SYSTEM_CHANNEL refer to VOL_CH
// 03/24/16     Henry.Y        1.Modify BattCoreGetVoltage(): Add adc test parameter.
// 03/30/16     Vincent.T      1. Modify TestInVehicleClick(); to handle multination language case.
// 06/21/16     Vincent.T      1. MenuLvlStack_Push(&g_MenuLvlStack, MEASURING); to MenuLvlStack_Push(&g_MenuLvlStack, POINTTOBAT);
// 09/06/16     Vincent.T      1. #include "driverlib/timer.h"
//                             2. Modify TestInVehicleClick(); to accomplish 15 sec countdown.
// 09/21/17 	Jerry.W		   Rewrite Widget for new software architecture
// 11/13/17     Jerry.W        1. Modify the direction of next widget for Hyundai
// 11/22/17     Henry.Y        1. Modify TestInVehicle_sub_W[0]: CountDown15S_widget if 'yes' selected.
//                             2. Set g_TestInVehicle of test in vehicle yes or not.
// 03/22/18     William.H      Modify ChkSurfaceCharge_sub_W[] to enter to CountDown15S_widget directly if its Test-In-Vehcile. 
// 09/11/18     Henry.Y        Add TestInVehicle_LEFT. Modify operation flow to PointToBAT_widget if not in vehicle test selected.
// 10/17/18     Henry.Y        Add identifier: __BO__.
// 12/07/18     Henry.Y        New display api.
// 12/17/18     Henry.Y        BT2200_WW, remove jump start post flow.
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/app/Testinvehiclewidget.c#1 $
// $DateTime: 2017/11/27 10:38:22 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#include "Widgetmgr.h"
#define SURFACE_DELTA_V 0.1//V
#define COUNTDOWN_T 15//s

void TestInVehicleAntiWhiteItem(uint8_t selRslt);

void TestInVehicle_service();
void TestInVehicle_DISPLAY();
void TestInVehicle_SELECT();
void TestInVehicle_LEFT_RIGHT();
void TestInVehicle_routine();

static float SurfaceCharge_V_start;
static int16_t Old_counter;//for smoothly update view

widget* TestInVehicle_sub_W[] = {
        &Measuring_widget
};

widget TestInVehicle_widget = {
		.Key_UP = 0,
		.Key_DOWN = 0,
		.Key_RIGHT = TestInVehicle_LEFT_RIGHT,
		.Key_LEFT = TestInVehicle_LEFT_RIGHT,
		.Key_SELECT = TestInVehicle_SELECT,
		.routine = TestInVehicle_routine,
		.service = TestInVehicle_service,
		.display = TestInVehicle_DISPLAY,
		.initial = 0,
		.subwidget = TestInVehicle_sub_W,
		.number_of_widget = sizeof(TestInVehicle_sub_W)/4,
		.index = 1,//default yes
		.no_messgae = 1,
		.name = "TestInVehicle",
};
        
void CountDown15S_Timer_callback(void)
{
}

void TestInVehicle_DISPLAY(){
    TEST_DATA* TD = Get_test_data();
    SYS_PAR* par = Get_system_parameter();
    if(TestInVehicle_widget.parameter == 1){//test in vehicle?
        ST7565ClearScreen(BUFFER1);
        ST7565Refresh(BUFFER1);
        LCD_String_ext(16,get_Language_String(S_TEST_IN_VEHICLE) , Align_Left);
        LCD_String_ext2(32, get_Language_String((TestInVehicle_widget.index)? S_YES:S_NO), Align_Right, 2);
        LCD_Arrow(Arrow_enter | Arrow_RL );
    }
    else if(TestInVehicle_widget.parameter == 2 || TestInVehicle_widget.parameter == 3){// countdown 15s
        char buf[20];
    	ST7565ClearScreen(BUFFER1);
    	ST7565Refresh(BUFFER1);
    	if(g_15SecCDCounter > 0){
    	    LCD_String_ext(16,get_Language_String(S_TURN_HEADLIGHTS), Align_Left);
            sprintf( buf, "%s", get_Language_String(S_ON_FOR_15SEC));
            char* sec = strstr(buf, "1");
            sec[0] = ('0' + (g_15SecCDCounter/10));
            sec[1] = ((g_15SecCDCounter%10) + '0');
    	    LCD_String_ext(32, buf, Align_Central);}
    	else{
    	    LCD_String_ext(16,get_Language_String(S_TURN_HEADLIGHTS_1), Align_Left);
    	    LCD_String_ext(32,get_Language_String(S_OFF_1), Align_Central);
    	    LCD_String_ext(48,get_Language_String(S_PRESS_ENTER), Align_Central);
    	}
    }
}

// Fix bug: continued surface charge will cause widget stack overflow
// TestInVehicle_widget.parameter: State
// 0: initial> check surface charge
// 1: Test in vehicle Y/N
// 2: prepare countdown 15s> check voltage drop
// 3: countdown 15s and done> to State 0 

void TestInVehicle_service()
{
    float test_voltage = 0;
    uint32_t ui32Voltage_scale = 0;
    SYS_PAR* par = Get_system_parameter();
    TEST_DATA* TD = Get_test_data();
    /*if(par->WHICH_TEST == IR_Test){
        TD->TestInVehicle = 0;
        Move_widget(TestInVehicle_sub_W[0],0);
        return;
    }*/
    
    BattCoreGetVoltage(&ui32Voltage_scale, &test_voltage);
    
    uint8_t target_batype = (par->WHICH_TEST == Start_Stop_Test)? par->SS_test_par.SSBatteryType:par->BAT_test_par.BatteryType;
    TD->Surface_charge = Surface_charge_detect((BATTERYTYPE)target_batype, (VOL_CH)TD->SYSTEM_CHANNEL, test_voltage);
    if(TD->Surface_charge == 0){
        Move_widget(TestInVehicle_sub_W[0],0);
    }
    else{
        TestInVehicle_widget.parameter = 1;
        TestInVehicle_DISPLAY();
    }
}

void TestInVehicle_LEFT_RIGHT(){
    if(TestInVehicle_widget.parameter == 1){
        TestInVehicle_widget.index ^= 0x01;
        TestInVehicle_DISPLAY();
    }
}

void TestInVehicle_SELECT(){
	TEST_DATA* TD = Get_test_data();
	SYS_PAR* par = Get_system_parameter();
    uint32_t ui32Voltage_scale;

    if(TestInVehicle_widget.parameter == 1){
        TD->TestInVehicle = TestInVehicle_widget.index;
        if(TD->TestInVehicle){
            BattCoreGetVoltage(&ui32Voltage_scale, &SurfaceCharge_V_start);
            g_15SecCDCounter = COUNTDOWN_T;
            Old_counter = g_15SecCDCounter;
            TestInVehicle_widget.parameter = 2;
            TestInVehicle_DISPLAY();
        }
        else{
            Move_widget(TestInVehicle_sub_W[0],0);
        }
    }
    else if(TestInVehicle_widget.parameter == 3){
        if(g_15SecCDCounter == 0){
            TestInVehicle_widget.parameter = 0;
            TestInVehicle_service();
        }
    }
}

void TestInVehicle_routine()
{
    TEST_DATA* TD = Get_test_data();
    uint32_t ui32Voltage_scale;
    float SurfaceCharge_V_test;
    float delta_SurfaceCharge_V;
    float SurfaceCharge_V_thresold;
    
    if(TestInVehicle_widget.parameter == 2){
        SurfaceCharge_V_thresold = (TD->SYSTEM_CHANNEL == MAP_6V)? (SURFACE_DELTA_V/2):SURFACE_DELTA_V;
        BattCoreGetVoltage(&ui32Voltage_scale, &SurfaceCharge_V_test);
        delta_SurfaceCharge_V = SurfaceCharge_V_start - SurfaceCharge_V_test;
        if(delta_SurfaceCharge_V >= SurfaceCharge_V_thresold){
            TestInVehicle_widget.parameter = 3;
            set_countdown_callback(CountDown15S_Timer_callback, COUNTDOWN_T);
        }        
    }
    else if(TestInVehicle_widget.parameter == 3){
        if(Old_counter != g_15SecCDCounter ){
            Old_counter = g_15SecCDCounter;
            TestInVehicle_DISPLAY();
        }
    }
}


