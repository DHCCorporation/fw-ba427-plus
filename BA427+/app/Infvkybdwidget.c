//*****************************************************************************
//
// Source File: Infvkybdwidget.c
// Description: Information's Virtual Keyboard widget including Uppercase/Lowercase
//				letters , Numeric and Symbol keyboards.
//				
//
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 09/17/15     William.H      Created file.
// 10/01/15     William.H      Re-arrange/remove Global varaible's usage due to we've used stack to behave the manu level selection.
//                             Respective global varaible by the respective widget file to be maintained by their own.
// 10/05/15     William.H      Check in BT2020 project which branched from LCDFontMaker(Pre-BT2020) by modifed related GPIOs.
// 10/30/15     William.H      This to fix the Internal Battery Low suddenly been presented scenario, then clear menu stack and back to Main Menu immediately will cause state currupt issue.
// 11/12/15     William.H      1. To implement the memory storage feature of 'Information' via vitual keyboard input. info->VkybdTextEditStr[128] to store input characters; g_InfVkybdLastCharPos to record the latest position.
//                             2. g_InfVkybdLastCharPos does not reset to zero when back to previos menu.
// 01/22/16     Vincent.T      1. Press "OK" save g_Info. into internal eeprom
//                             2. #include "**\service/eepromsvc.h"
// 01/25/16     Vincent.T      1. Save g_InfVKybdCharPos to move cursor when there is any data inside info->VkybdTextEditStr
//                             2. Add new case when g_InfVKybdCharPos == 128 (InfVKybdUpCaseClick(33))
// 03/24/16     Vincent.T      1. Modify InfVKybdUpCaseClick(); to handle multination language case.
//                             2. Modify InfVKybdNumClick(); to handle multination language case.
//                             3. Modify InfVKybdLowCaseClick(); to handle multination language case.
//                             4. Modify InfVKybdSymClick(); to handle multination language case.
// 03/08/17     Vincent.T      1. Modify InfVKybdUpdateTextEditChar(); Font_System7x8 -> Font_System5x8
// 09/26/17 	Jerry.W		   Rewrite Widget for new software architecture
// 09/30/17 	Jerry.W		   1. add draw bottom line operation in every key operation for batter display result
//							   2. add const in tRectangle array for fewer memory usage
//							   3. modify blink_counter cycle for batter display result
// 11/13/17     Jerry.W        1. Fix bug for non-inialized condition
// 09/11/18     Henry.Y        Use systeminfo data struct to replace global variable.
// 10/17/18     Henry.Y        Fix bug: virtual up key invalid operation at 1st of the second row.
// 12/07/18     Henry.Y        New display api.
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/app/Infvkybdwidget.c#1 $
// $DateTime: 2017/11/27 10:38:22 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#include "Widgetmgr.h"


void InfVKybdUpdateTextEditChar();
void InfVKybdAntiWhiteItem(uint8_t index);


//void InVKbd_UP();
//void InVKbd_DOWN();
void InVKbd_LEFT();
void InVKbd_RIGHT();
void InVKbd_SELECT();
void InVKbd_display();
void InVKbd_initial();


widget* InVKbd_sub_W[] = {
        &InVKbd_confirm_widget,
};

widget InVKbd_widget = {
		.Key_UP = 0,   //InVKbd_UP,
		.Key_DOWN = 0, //InVKbd_DOWN,
		.Key_RIGHT = InVKbd_RIGHT,
		.Key_LEFT = InVKbd_LEFT,
		.Key_SELECT = InVKbd_SELECT,
		.routine = 0,
		.service = 0,
		.initial = InVKbd_initial,
		.display = InVKbd_display,
		.subwidget = InVKbd_sub_W,
		.number_of_widget = sizeof(InVKbd_sub_W)/4,
		.index = 0,
		.name = "InVKbd",
		.Combo = Combo_enter | Combo_right | Combo_left,
};

static const char CHAR_List[] = " ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.%,/=&#-+*:?'";
static char Edit_buf[5][17];

void Change_Char(char* chara, uint8_t up_down){
    uint32_t i;
    for(i = 0 ; i < strlen(CHAR_List) ; i++){
        if(CHAR_List[i] == *chara) break;
    }

    if(i == strlen(CHAR_List)){
        while(1){}
    }

    if(up_down){    //up -> ++
        i++;
        i %= strlen(CHAR_List);
    }
    else{   //down -> --
        if(i == 0) i = strlen(CHAR_List)-1;
        else i--;
    }

    *chara = CHAR_List[i];
}

void InVKbd_display(){
	SYS_INFO* info = Get_system_info();

	ST7565ClearScreen(BUFFER1);
	ST7565Refresh(BUFFER1);

	LCD_String_ext(0,get_Language_String(S_INFO) , Align_Central);

	LCD_Arrow(Arrow_enter | Arrow_RL); //| ARROW_UP_DOWN

	if(InVKbd_widget.parameter <= 2){
	    LCD_String_ext(16,Edit_buf[0] , Align_Left);
	    LCD_String_ext(32,Edit_buf[1] , Align_Left);
	    LCD_String_ext(48,Edit_buf[2] , Align_Left);
	}
	else{
	    LCD_String_ext(16,Edit_buf[3] , Align_Left);
	    LCD_String_ext(32,Edit_buf[4] , Align_Left);
	    LCD_String_ext(48,"----------------" , Align_Left);
	}

	uint8_t x = (InVKbd_widget.parameter%3) * 16 + 30;
	uint8_t y = InVKbd_widget.index*8;

	ST7565V128x64x1LineDrawH(0, y, y+7, x, 1);
	ST7565V128x64x1LineDrawH(0, y, y+7, x+1, 1);

}

void InVKbd_initial(){
    SYS_INFO* info = Get_system_info();
    InVKbd_widget.index = 0;
    InVKbd_widget.parameter = 0;

    if(info->Has_DIY == 0){
        memset(info->VkybdTextEditStr, ' ', sizeof(info->VkybdTextEditStr));
    }

    info->VkybdTextEditStr[0][16] = '\0';
    info->VkybdTextEditStr[1][16] = '\0';
    info->VkybdTextEditStr[2][16] = '\0';
    info->VkybdTextEditStr[3][16] = '\0';
    info->VkybdTextEditStr[4][16] = '\0';

    memcpy(Edit_buf, info->VkybdTextEditStr, sizeof(Edit_buf));

}


/*
void InVKbd_UP(){
    SYS_INFO* info = Get_system_info();
    Change_Char(&Edit_buf[InVKbd_widget.parameter][InVKbd_widget.index], 1);
    InVKbd_display();
}

void InVKbd_DOWN(){
    SYS_INFO* info = Get_system_info();
    Change_Char(&Edit_buf[InVKbd_widget.parameter][InVKbd_widget.index], 0);
    InVKbd_display();
}
*/

void InVKbd_LEFT(){
    /*
    if(InVKbd_widget.index == 0){
        if(InVKbd_widget.parameter > 0){
            InVKbd_widget.parameter--;
        }
        else{
            InVKbd_widget.parameter = 4;
        }
        InVKbd_widget.index = 15;
    }
    else{
        InVKbd_widget.index--;
    }
    */
    SYS_INFO* info = Get_system_info();
    Change_Char(&Edit_buf[InVKbd_widget.parameter][InVKbd_widget.index], 0);
    InVKbd_display();
}

void InVKbd_RIGHT(){
    /*
    if(InVKbd_widget.index == 15){
        if(InVKbd_widget.parameter < 4){
            InVKbd_widget.parameter++;
        }
        else{
            InVKbd_widget.parameter = 0;
        }
        InVKbd_widget.index = 0;
    }
    else{
        InVKbd_widget.index++;
    }
    */
    SYS_INFO* info = Get_system_info();
    Change_Char(&Edit_buf[InVKbd_widget.parameter][InVKbd_widget.index], 1);
    InVKbd_display();
}

void InVKbd_SELECT(){
    if(InVKbd_widget.index == 15){
        if(InVKbd_widget.parameter < 4){
            InVKbd_widget.parameter++;
            InVKbd_widget.index = 0;
            InVKbd_display();
        }
        else{
            InVKbd_widget.parameter = 0;
            InVKbd_widget.index = 0;
            Move_widget_untracked(InVKbd_sub_W[0],0);
        }
        InVKbd_widget.index = 0;
    }
    else{
        InVKbd_widget.index++;
        InVKbd_display();
    }
}



void InVK_save(){
    SYS_INFO* info = Get_system_info();
    info->Has_DIY = 1;
    memcpy(info->VkybdTextEditStr, Edit_buf, sizeof(Edit_buf));
    save_system_info(&info->VkybdTextEditStr, sizeof(info->VkybdTextEditStr));
    save_system_info(&info->Has_DIY, sizeof(info->Has_DIY));
}








