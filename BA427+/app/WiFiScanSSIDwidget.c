 //*****************************************************************************
 //
 // Source File: WiFiScanSSIDwidget.c
 // Description: WiFi scan SSID and connect operation widget.
 //
 //
 // Edit History:
 //
 // when		 who			what, where, why  
 // --------	 ---------- 	------------------------------------------------
 // 01/30/17	 Henry.Y		Create file.
 // 02/14/18	 Henry.Y		modify operation flow.
 // 10/17/18     Henry.Y        Fix bug: virtual up key invalid operation at 1st of the second row.
 // 11/19/18     Henry.Y        Can not connect to the designated AP if it was connected.
 // 12/07/18	 Henry.Y		New display api.
 // =============================================================================
 //
 //
 // $Header$
 // $DateTime$
 // $Author$
 //
 //
 // Copyright (c) 2015 DHC Specialty Corp.	All Rights Reserved.
 // DHC Proprietary.
 //
 //*****************************************************************************

#include "Widgetmgr.h"

#define pw_key_offset_x 26
#define pw_key_offset_y 11 

static const char infVK[] = {
		//0   1   2   3   4   5   6   7   8   9
		'Q','W','E','R','T','Y','U','I','O','P', //0~9

		'A','S','D','F','G','H','J','K','L',' ', //10~19

		'Z','X','C','V','B','N','M',' ',' ',' ', //20~29

		' ',' ',' ',' ',' ','q','w','e','r','t', //30~39

		'y','u','i','o','p','a','s','d','f','g', //40~49

		'h','j','k','l',' ','z','x','c','v','b', //50~59

		'n','m',' ',' ',' ',' ',' ',' ',' ',' ', //60~69

		'1','2','3','4','5','6','7','8','9','0', //70~79

		'-','/',':',';','(',')','$','&','@','"', //80~89

		' ','.',',','?','!','\'',' ',' ',' ',' ', //90~99

		' ',' ',' ',' ','[',']','{','}','#','%', //100~109

		'^','*','+','=','_','\\','~','<','>',' ', //110~119

		'.',',','?','!','\'','6','7','8','9','0', //120~129

		' ',' ',' ' 							  //130~132
};

//*****************************************************************************
//
// Language menu - Normal radio check button.
// Width: 9 / Height: 8 / radioChkCode bank
// 
//*****************************************************************************
static const uint8_t radioChkCode_nor[] = {	
		0x00,0x1C,0x22,0x5D,0x5D,0x5D,0x22,0x1C,0x00
};
static const LANGMENUFONT_DEF LangMenuFont_radioChk_nor = {9, 8, radioChkCode_nor};




static SSID_list_T* SSID_list = NULL;


static const XY_DISPLAY WIFI =	 {"WIFI:",				2,	1};
static const XY_DISPLAY KEY =	 {"KEY:",				2,	11};

static WIFI_CONN_T pre_cnt_status = WIFI_disconnect;

static uint8_t max_option_val = 0;
static uint8_t PW_in[PW_LEN] = {0x00};
static uint8_t dig_loc = 0;
static uint8_t kb_index = 0;
static uint8_t abort_input = 0;



void wifi_service_cb(uint32_t type);

tRectangle SSIDSelDrawList(uint8_t itemIdx, const char* text,uint8_t draw_start_y);
void SSIDSelDrawOptions(uint8_t sel, uint8_t max_val,uint8_t draw_start_y);
void WiFiScanSSID_UP();
void WiFiScanSSID_DOWN();
void WiFiScanSSID_RIGHT();

void WiFiScanSSID_SELECT();
void WiFiScanSSID_LEFT();
void WiFiScanSSID_Routine();
void WiFiScanSSID_initail();
void WiFiScanSSID_DISPLAY();

void PWVKybdAntiWhiteItem(uint8_t index);
void PWVKybdUpdateTextEditChar();
uint8_t PWVKbd_SELECT();
void PWVKbd_RIGHT();
void PWVKbd_LEFT();
void PWVKbd_DOWN();
void PWVKbd_UP();


widget* WiFiScanSSID_sub_W[] = {
};

widget WiFiScanSSID_widget = {
		.Key_UP = WiFiScanSSID_UP,
		.Key_DOWN = WiFiScanSSID_DOWN,
		.Key_RIGHT = WiFiScanSSID_RIGHT,
		.Key_LEFT = WiFiScanSSID_LEFT,
		.Key_SELECT = WiFiScanSSID_SELECT,
		.routine = WiFiScanSSID_Routine,
		.service = 0,
		.initial = WiFiScanSSID_initail,
		.display = WiFiScanSSID_DISPLAY,
		.subwidget = WiFiScanSSID_sub_W,
		.number_of_widget = sizeof(WiFiScanSSID_sub_W)/4,
		.index = 0,
		.no_messgae = 1,
		.name = "WiFiScanSSID",
};

void scan_cb(SSID_list_T * list){
	//uint8_t i;
	uint16_t n;
	UARTprintf("**********************\n");
	free(SSID_list);
	SSID_list = NULL;
	
	n = sizeof(list->AP_number) + list->AP_number * sizeof(AP_info_T);
	SSID_list = malloc(n);
	memcpy(SSID_list, list, n);
	
	max_option_val = SSID_list->AP_number+1;

	IntMasterDisable();
	UARTprintf("%d AP scanned:\n", SSID_list->AP_number);
	/*for(i = 0; i < SSID_list->AP_number; i++){
		UARTprintf("AP%d : %s\n", i, SSID_list->AP[i].ssid);
	}*/
	IntMasterEnable();
	UARTprintf("**********************\n\n");
}

void WiFiScanSSID_initail(){

	if(SSID_list){
		free(SSID_list);
		SSID_list = NULL;
	}

	wifi_scan_AP(scan_cb);
	WiFiScanSSID_widget.index = 0;
	pre_cnt_status = wifi_get_status();
}


void WiFiScanSSID_DISPLAY(){
	LANGUAGE LangIdx = get_language_setting();

	tRectangle sTemp;
	uint8_t l,sLen;

	if(WiFiScanSSID_widget.index < 2){
		LCD_display(TESTING_PROCESSING_ARROW, WiFiScanSSID_widget.index);
	}
	else if(WiFiScanSSID_widget.index == 2){
		sTemp = LCD_display(WIFI_LIST,0);
		SSIDSelDrawOptions(WiFiScanSSID_widget.parameter, max_option_val, sTemp.i16YMax+3);
	}
	else if(WiFiScanSSID_widget.index == 3){
		LCD_display(VIRTUAL_KB_ABC, 0);

		ST7565printf5x8(&WIFI);
		uint8_t slen_wifi = strlen(WIFI.string);		
		sLen = strlen((const char*)SSID_list->AP[WiFiScanSSID_widget.parameter].ssid);
		for(l = 0; l < sLen; l++){
			ST7565Draw6x8Char((slen_wifi + l)*6+3, 1, (l>12 && sLen > 15)? '.':SSID_list->AP[WiFiScanSSID_widget.parameter].ssid[l], Font_System5x8);
		}
		ST7565printf5x8(&KEY);
		
		PWVKybdAntiWhiteItem(kb_index);

		//
		// TODO: add cancel pw key in selection ------ pending infinitely
		//
	}
	else if(WiFiScanSSID_widget.index == 4){
		
		if(wifi_svc_s == WIFI_getting_IP){
			LCD_display(WIFI_CONNECTING,0);
		}
		else if(wifi_svc_s == WIFI_connected){
			if(tcp_svc_s == Soc_TCP_connecting){
				LCD_display(WIFI_CONNECTING,0);
			}
			else{
				sTemp = LCD_display((tcp_svc_s == Soc_TCP_connected)? WIFI_CONNECTION_CONNECT:WIFI_CONNECTION_LIMIT, 0);
				sLen = strlen((const char*)CNT_SSID);
				uint8_t START_LOC = (sLen > 20)? 0:60-(sLen*3);
				for(l = 0; l < sLen; l++){
					ST7565Draw6x8Char(START_LOC + l*6, sTemp.i16YMax+3, (l>18 && (sLen > 20))? '.':CNT_SSID[l], Font_System5x8);
				}
			}			
		}
		else{
			LCD_display(WIFI_CONNECTION_DISCONNECT, 0);
		}
	}
	else if(WiFiScanSSID_widget.index == 5){
		LCD_display(ABORT, abort_input^0x01);// abort password key in
	}
}

void WiFiScanSSID_SELECT(){
	UARTprintf(".index = %d", WiFiScanSSID_widget.index);

	if(WiFiScanSSID_widget.index == 2){
	
		if(WiFiScanSSID_widget.parameter == (max_option_val - 1)){ //Select "Refresh list"
			free(SSID_list);
			SSID_list = NULL;

			wifi_scan_AP(scan_cb);
			WiFiScanSSID_widget.index = 0;
			WiFiScanSSID_widget.parameter = 0;
			
			WiFiScanSSID_DISPLAY();
		}
		else{
			if(!strcmp((char*)CNT_SSID, (char*)SSID_list->AP[WiFiScanSSID_widget.parameter].ssid)) return;
			memset(PW_in, 0x00, PW_LEN);
			dig_loc = 0;
			kb_index = 34;
			WiFiScanSSID_widget.index = 3;
			WiFiScanSSID_DISPLAY();
		}
	}
	else if(WiFiScanSSID_widget.index == 3){
		switch(PWVKbd_SELECT()){
			case 1:
			{
				//
				// TODO: check password format valid or not
				//
				// if() ,message("invalid character")
				{
					//
					// password valid, turns to connecting
					//
					WiFiScanSSID_widget.index = 4;
					wifi_svc_s = WIFI_getting_IP;
					tcp_svc_s = Soc_TCP_disconnected;					
					wifi_connnect_policy(conn_disconnect);
					wifi_connnect_AP(SSID_list->AP[WiFiScanSSID_widget.parameter].ssid, PW_in);
				
					WiFiScanSSID_DISPLAY();
				}
				break;
			}
			
			case 99:
			{
				// abort?
				WiFiScanSSID_widget.index = 5;
				abort_input = 0;
				WiFiScanSSID_DISPLAY();
				break;
			}
			case 0:
			default:
			{
				//input or what
				break;
			}
		}		
	}
	else if(WiFiScanSSID_widget.index == 4){
		//
		// Enter to SSID list
		//
		if(wifi_svc_s != WIFI_getting_IP){
			WiFiScanSSID_widget.index = 2;
			WiFiScanSSID_DISPLAY();
		}
	}
	else if(WiFiScanSSID_widget.index == 5){
		if(abort_input){				
			WiFiScanSSID_initail();
		}
		else{
			WiFiScanSSID_widget.index = 3;
		}
		WiFiScanSSID_DISPLAY();
	}
}


void WiFiScanSSID_LEFT(){
	UARTprintf(".index = %d", WiFiScanSSID_widget.index);
	if(WiFiScanSSID_widget.index == 2){
		free(SSID_list);
		SSID_list = NULL;
	
		Return_pre_widget();
	}
	else if(WiFiScanSSID_widget.index == 3){
		PWVKbd_LEFT();
	}
	else if(WiFiScanSSID_widget.index == 4){
		if(wifi_svc_s == WIFI_connected && tcp_svc_s == Soc_TCP_connected){
			
			free(SSID_list);
			SSID_list = NULL;
			
			Return_pre_widget();
		}
	}
}


void WiFiScanSSID_RIGHT(){
	UARTprintf(".index = %d", WiFiScanSSID_widget.index);
	if(WiFiScanSSID_widget.index == 3){
		PWVKbd_RIGHT();
	}
}


void WiFiScanSSID_UP(){
	UARTprintf(".index = %d", WiFiScanSSID_widget.index);

	if(WiFiScanSSID_widget.index == 2){		
		WiFiScanSSID_widget.parameter = (!WiFiScanSSID_widget.parameter)? max_option_val-1:WiFiScanSSID_widget.parameter-1;
		WiFiScanSSID_DISPLAY(); 	
	}
	else if(WiFiScanSSID_widget.index == 3){
		PWVKbd_UP();
	}
	else if(WiFiScanSSID_widget.index == 5){
		abort_input^=0x01;
		WiFiScanSSID_DISPLAY();		
	}
}


void WiFiScanSSID_DOWN(){
	UARTprintf(".index = %d", WiFiScanSSID_widget.index);
	
	if(WiFiScanSSID_widget.index == 2){
		WiFiScanSSID_widget.parameter = (WiFiScanSSID_widget.parameter == max_option_val-1)? 0:WiFiScanSSID_widget.parameter+1;
		WiFiScanSSID_DISPLAY(); 	
	}
	else if(WiFiScanSSID_widget.index == 3){
		PWVKbd_DOWN();
	}
	else if(WiFiScanSSID_widget.index == 5){
		abort_input^=0x01;
		WiFiScanSSID_DISPLAY();		
	}
}


void SSIDSelDrawOptions(uint8_t sel, uint8_t max_val, uint8_t draw_start_y){
	uint8_t i,index,num_per_page = 4;
	uint8_t offset = sel%num_per_page;
	tRectangle sTemp,sTemp_1 = {0,0,127,0};
	
	DrawPageInfo(sel,max_val,num_per_page);

	for(i = 0; i< num_per_page;i++){
		index = (sel - offset + i);
		if(index < max_val - 1){
			//UARTprintf("AP%d : %s\n", sel - offset + i, SSID_list->AP[index].ssid);		
			sTemp = SSIDSelDrawList(i, (const char*)SSID_list->AP[index].ssid,draw_start_y);
			if(i == sel%num_per_page){
				sTemp_1.i16YMin = sTemp.i16YMin-1;
				sTemp_1.i16YMax = sTemp.i16YMax;
			}
		}
		else{
			sTemp = LCD_draw_string(WIFI_REFRESH_LIST, 0, draw_start_y+i*10);
			
			if(i == sel%num_per_page){
				sTemp_1.i16YMin = sTemp.i16YMin-1;
				sTemp_1.i16YMax = sTemp.i16YMax+1;
			}
			break;
		}
	}
	Lcd_selection_antiwhite(sTemp_1);
}






void WiFiScanSSID_Routine(){
	static uint16_t blink_counter = 0;
	static uint8_t toggle = 0;

	uint16_t n = 0;
	
	if(WiFiScanSSID_widget.index == 0 || WiFiScanSSID_widget.index == 1){
		if(SSID_list != NULL){
			blink_counter = 0;

			WiFiScanSSID_widget.index = 2;
			WiFiScanSSID_DISPLAY();
		}
		else{
			blink_counter++;
			if(blink_counter > 3000){
				blink_counter = 0;
				WiFiScanSSID_widget.index = 2;
								
				n = sizeof(uint8_t) + sizeof(AP_info_T);
				SSID_list = malloc(n);
				SSID_list->AP_number = 0;
				max_option_val = 1;
				//WiFiScanSSID_DISPLAY();
				return;
			}
			if(blink_counter % 600 == 0){
				WiFiScanSSID_widget.index ^= 0x01;
				WiFiScanSSID_DISPLAY();
			}
		}
	}
	else if(WiFiScanSSID_widget.index == 2){
		//
		// scanned SSID add icon if connection refresh
		//

		if(pre_cnt_status != wifi_get_status()){
			if(wifi_get_status() == WIFI_connected){
				if(SSID_list!=NULL){
					for(n = 0; n < SSID_list->AP_number;n++){
						if(!strcmp((const char*)CNT_SSID, (const char*)SSID_list->AP[n].ssid)){						
							pre_cnt_status = wifi_get_status();
							
							WiFiScanSSID_widget.parameter = 0;
							WiFiScanSSID_DISPLAY();
							return; 
						}			
					}
					// no match , refresh SSID
					free(SSID_list);
					SSID_list = NULL;
					
					wifi_scan_AP(scan_cb);
				}
			}
			else{
				pre_cnt_status = wifi_get_status();
				if(CNT_SSID != NULL){
					memset(CNT_SSID, 0x00, SSID_LEN);
				}
				
				WiFiScanSSID_widget.parameter = 0;
				WiFiScanSSID_DISPLAY();
			}
		}	
	}
	else if(WiFiScanSSID_widget.index == 3){
		if(blink_counter == 100){
			toggle ^= 0x01;
			ST7565V128x64x1LineDrawH(0, pw_key_offset_x + (dig_loc%16)*6, \
										pw_key_offset_x + ((dig_loc%16)*6)+5, \
										(dig_loc/16 == 0)? pw_key_offset_y + ((dig_loc/16)*8)+8:pw_key_offset_y + ((dig_loc/16)*8)+9, toggle);
		}
		
		blink_counter = (blink_counter+1)%400;
	}
	else if(WiFiScanSSID_widget.index == 4){
		if(event_trig){
			event_trig = 0;		
		
			WiFiScanSSID_DISPLAY();		
		}
	}
}



tRectangle SSIDSelDrawList(uint8_t itemIdx, const char* text, uint8_t draw_start_y)
{
	uint8_t l, sLen;
	tRectangle sTemp = {0,0,127,0};

	sLen = strlen(text);

	for(l = 0; l < sLen; l++)
	{
		ST7565Draw6x8Char(l*6, draw_start_y + (itemIdx * 10), ' ', Font_System5x8);
	}

	for(l = 0; l < sLen; l++)
	{
		ST7565Draw6x8Char(2+l*6, draw_start_y + (itemIdx * 10), (l>15 && sLen>18)? '.':text[l], Font_System5x8);
		
	}
	if(!strcmp((const char*)CNT_SSID, text)){
		//UARTprintf("^^^^^^ CONNECTED THIS ONE ^^^^^^\n");
		ST7565DrawLangSelMenu(117, draw_start_y +(itemIdx*10), (LANGUAGE)0, LangMenuFont_radioChk_nor);
	}
	sTemp.i16YMin = draw_start_y +(itemIdx*10);
	sTemp.i16YMax = sTemp.i16YMin+8;

	return sTemp;
}


void PWVKbd_UP(){
	PWVKybdAntiWhiteItem(kb_index);

	//
	//	'ABC' Virtual Keyboard
	//
	if((kb_index >= 10) && (kb_index <= 34) )
	{
		if(kb_index == 18)
		{
			kb_index = 9;
		}
		else if(kb_index == 29 || kb_index == 30 || kb_index == 31)
		{
			kb_index = 20;
		}
		else if(kb_index == 32)
		{
			kb_index = 21;
		}
		else if(kb_index == 33)
		{
			kb_index = 24;
		}
		else if(kb_index == 34)
		{
			kb_index = 27;
		}
		else if((kb_index >= 10) && (kb_index <= 17))
		{
			kb_index -= 10;
		}
		else if((kb_index >= 19) && (kb_index <= 28))
		{
			kb_index -= 9;
		}
	}
	//
	//	'abc' Virtual Keyboard
	//
	else if(kb_index >= 45 && kb_index <= 69)
	{
		if(kb_index == 53)
		{
			kb_index = 44;
		}
		else if(kb_index == 64 || kb_index == 65)
		{
			kb_index = 54;
		}
		else if(kb_index == 66 || kb_index == 67)
		{
			kb_index -= 11;
		}
		else if(kb_index == 68)
		{
			kb_index = 59;
		}
		else if(kb_index == 69)
		{
			kb_index = 62;
		}
		else if(kb_index >= 45 && kb_index <= 52)
		{
			kb_index -= 10;
		}
		else if(kb_index >= 54 && kb_index <= 63)
		{
			kb_index -= 9;
		}

	}
	//
	//	'123' Virtual Keyboard
	//
	else if(kb_index >= 80 && kb_index <= 103)
	{
		if(kb_index == 92)
		{
			kb_index = 83;
		}
		else if(kb_index == 93)
		{
			kb_index = 84;
		}
		else if(kb_index == 94)
		{
			kb_index = 86;
		}
		else if(kb_index == 95)
		{
			kb_index = 88;
		}
		else if(kb_index == 96)
		{
			kb_index = 89;
		}
		else if(kb_index == 97 || kb_index == 98 || kb_index == 99)
		{
			kb_index = 90;
		}
		else if(kb_index == 100 || kb_index == 101)
		{
			kb_index = 91;
		}
		else if(kb_index == 102)
		{
			kb_index = 93;
		}
		else if(kb_index == 103)
		{
			kb_index = 96;
		}
		else if(kb_index >= 80 && kb_index <= 91)
		{
			kb_index -= 10;
		}

	}
	//
	// '#+=' Virtual Keyboard
	//
	else if(kb_index >= 114 && kb_index <= 132)
	{
		if(kb_index == 114)
		{
			kb_index = 106;
		}
		else if(kb_index == 119)
		{
			kb_index = 104;
		}
		else if(kb_index == 125)
		{
			kb_index = 113;
		}
		else if(kb_index == 126 || kb_index == 127 || kb_index == 128)
		{
			kb_index = 119;
		}
		else if(kb_index == 129 || kb_index == 130)
		{
			kb_index = 120;
		}
		else if(kb_index == 131)
		{
			kb_index = 122;
		}
		else if(kb_index == 132)
		{
			kb_index = 125;
		}
		else if(kb_index >= 115 && kb_index <= 118)
		{
			kb_index -= 7;
		}
		else if(kb_index >= 120 && kb_index <= 124)
		{
			kb_index -= 6;
		}
	}
	PWVKybdUpdateTextEditChar();
	PWVKybdAntiWhiteItem(kb_index);
	ST7565V128x64x1LineDrawH(0, pw_key_offset_x + (dig_loc%16)*6, \
								pw_key_offset_x + ((dig_loc%16)*6)+5, \
								(dig_loc/16 == 0)? pw_key_offset_y + ((dig_loc/16)*8)+8:pw_key_offset_y + ((dig_loc/16)*8)+9, 0x01);
}



void PWVKbd_DOWN(){
	PWVKybdAntiWhiteItem(kb_index);

	if(kb_index <= 27)
	{
		if(kb_index == 9)
		{
			kb_index = 18;
		}
		else if(kb_index == 20)
		{
			kb_index = 31;
		}
		else if(kb_index == 21)
		{
			kb_index = 32;
		}
		else if((kb_index >= 22) && (kb_index <= 26))
		{
			kb_index = 33;
		}
		else if(kb_index == 27)
		{
			kb_index = 34;
		}
		else if(kb_index <= 8)
		{
			kb_index += 10;
		}
		else if((kb_index >= 10) && (kb_index <= 19))
		{
			kb_index += 9;
		}


	}
	//
	//	'abc' Virtual Keyboard
	//
	else if((kb_index >= 35) && (kb_index <= 62))
	{
		if(kb_index == 44)
		{
			kb_index = 53;
		}
		else if(kb_index == 55)
		{
			kb_index = 66;
		}
		else if(kb_index == 56)
		{
			kb_index = 67;
		}
		else if((kb_index >= 57) && (kb_index <= 61))
		{
			kb_index = 68;
		}
		else if(kb_index == 62)
		{
			kb_index = 69;
		}
		else if((kb_index >= 35) && (kb_index <= 43))
		{
			kb_index += 10;
		}
		else if((kb_index >= 45) && (kb_index <= 54))
		{
			kb_index += 9;
		}

	}
	//
	//	'123' Virtual Keyboard
	//
	else if((kb_index >= 70) && (kb_index <= 96))
	{
		if(kb_index == 82)
		{
			kb_index = 91;
		}
		else if(kb_index == 83)
		{
			kb_index = 92;
		}
		else if(kb_index == 84 || kb_index == 85)
		{
			kb_index = 93;
		}
		else if(kb_index == 86)
		{
			kb_index = 94;
		}
		else if(kb_index == 87 || kb_index == 88)
		{
			kb_index = 95;
		}
		else if(kb_index == 89)
		{
			kb_index = 96;
		}
		else if(kb_index == 90)
		{
			kb_index = 97;
		}
		else if(kb_index == 91)
		{
			kb_index = 101;
		}
		else if(kb_index == 92 || kb_index == 93 || kb_index == 94 || kb_index == 95)
		{
			kb_index = 102;
		}
		else if(kb_index == 96)
		{
			kb_index = 103;
		}
		else if(kb_index >= 70 && kb_index <= 81)
		{
			kb_index += 10;
		}
	}
	//
	//	'#+=' Virtual Keyboard
	//
	else if(kb_index >= 104 && kb_index <= 125)
	{
		if(kb_index == 104 || kb_index == 105)
		{
			kb_index = 119;
		}
		else if(kb_index == 106 || kb_index == 107)
		{
			kb_index = 114;
		}
		else if(kb_index == 112 || kb_index == 113)
		{
			kb_index = 125;
		}
		else if(kb_index == 119)
		{
			kb_index = 126;
		}
		else if(kb_index == 120)
		{
			kb_index = 130;
		}
		else if(kb_index == 121 || kb_index == 122 || kb_index == 123 || kb_index == 124)
		{
			kb_index = 131;
		}
		else if(kb_index == 125)
		{
			kb_index = 132;
		}
		else if(kb_index >= 108 && kb_index <= 111)
		{
			kb_index += 7;
		}
		else if(kb_index >= 114 && kb_index <= 118)
		{
			kb_index += 6;
		}
	}
	PWVKybdUpdateTextEditChar();
	PWVKybdAntiWhiteItem(kb_index);
	ST7565V128x64x1LineDrawH(0, pw_key_offset_x + (dig_loc%16)*6, \
								pw_key_offset_x + ((dig_loc%16)*6)+5, \
								(dig_loc/16 == 0)? pw_key_offset_y + ((dig_loc/16)*8)+8:pw_key_offset_y + ((dig_loc/16)*8)+9, 0x01);
}



void PWVKbd_LEFT(){
	PWVKybdAntiWhiteItem(kb_index);

	if(kb_index == 0)
	{
		kb_index = 34;
	}
	else if(kb_index == 35)
	{
		kb_index = 69;
	}

	else if(kb_index == 70)
	{
		kb_index = 103;
	}
	else if(kb_index == 104)
	{
		kb_index = 132;
	}
	else {
		kb_index--;
	}

	PWVKybdUpdateTextEditChar();
	PWVKybdAntiWhiteItem(kb_index);
	ST7565V128x64x1LineDrawH(0, pw_key_offset_x + (dig_loc%16)*6, \
								pw_key_offset_x + ((dig_loc%16)*6)+5, \
								(dig_loc/16 == 0)? pw_key_offset_y + ((dig_loc/16)*8)+8:pw_key_offset_y + ((dig_loc/16)*8)+9, 0x01);
}

void PWVKbd_RIGHT(){
	PWVKybdAntiWhiteItem(kb_index);

	if(kb_index == 34)
	{
		kb_index = 0;
	}
	//
	//	'abc' Virtual Keyboard
	//
	else if(kb_index == 69)
	{
		kb_index = 35;
	}
	//
	//	'123' Virtual Keyboard
	//
	else if(kb_index == 103)
	{
		kb_index = 70;
	}
	//
	//	'#+=' Virtual Keyboard
	//
	else if(kb_index == 132)
	{
		kb_index = 104;
	}
	else{
		kb_index++;
	}

	PWVKybdUpdateTextEditChar();
	PWVKybdAntiWhiteItem(kb_index);
	ST7565V128x64x1LineDrawH(0, pw_key_offset_x + (dig_loc%16)*6, \
								pw_key_offset_x + ((dig_loc%16)*6)+5, \
								(dig_loc/16 == 0)? pw_key_offset_y + ((dig_loc/16)*8)+8:pw_key_offset_y + ((dig_loc/16)*8)+9, 0x01);
}


uint8_t PWVKbd_SELECT(){
	char txtEditChar[2] = {0};

	//clear underline
	ST7565V128x64x1LineDrawH(0, pw_key_offset_x + (dig_loc%16)*6, \
								pw_key_offset_x + ((dig_loc%16)*6)+5, \
								(dig_loc/16 == 0)? pw_key_offset_y + ((dig_loc/16)*8)+8:pw_key_offset_y + ((dig_loc/16)*8)+9, 0x00);

	LANGUAGE LangIdx = get_language_setting();
	set_language(en_US);
	switch(kb_index){
	case 19 :  // Switch to 'abc'
		LCD_display(VIRTUAL_KB_123, 2);		
		kb_index = 35;
		PWVKybdAntiWhiteItem(kb_index);

		break;

	case 27 : 	case 62:	case 96:	case 125:	//backspace
		PW_in[dig_loc] = 0x20;
		// put char in sreen buffer1 for roll up/down
		ST7565Clear8x8Char( (dig_loc%16)*8, (dig_loc/16)*8, BUFFER1);
		PWVKybdUpdateTextEditChar();

		if(dig_loc != 0){
			dig_loc--;
		}
		else{
			return 99;
		}
		break;

	case 28 : 	case 63:	case 119:	// Switch to '123'
		LCD_display(VIRTUAL_KB_123, 3);

		kb_index = 70;
		PWVKybdAntiWhiteItem(kb_index);

		break;

	case 29 : 	case 64:	case 98:	case 127:	// Navi Left Key
		if(dig_loc){
			dig_loc--;
		}
		break;

	case 30: 	case 65:	case 99:	case 128:	// Navi Right Key
		if((dig_loc+1) != PW_LEN){
			dig_loc++;
		}
		break;
	case 31: 	case 66:	case 100:	case 129:	// Navi Down Key
		if((dig_loc+16) < PW_LEN){

			dig_loc += 16;
		}
		break;

	case 32: 	case 67:	case 101:	case 130:	// Navi Up Key
		if(dig_loc >= 16){
			dig_loc -= 16;
		}

		break;

	case 34: 	case 69:	case 103:	case 132:	//OK

		
		UARTprintf("VK sel OK, PW_in[%d] = %s",dig_loc,PW_in);
		if(strlen((char*)PW_in) == 0) PW_in[0] = 'x';
		set_language(LangIdx);
		return 1;

	case 54: 	case 97:	case 126:	// Switch to 'ABC'
		LCD_display(VIRTUAL_KB_123, 1);

		kb_index = 0;
		PWVKybdAntiWhiteItem(kb_index);
		break;

	case 90: // Switch to  '#+='
		LCD_display(VIRTUAL_KB_123, 4);

		kb_index = 104;
		PWVKybdAntiWhiteItem(kb_index);
		break;

	default :

		PW_in[dig_loc] = infVK[kb_index];
		UARTprintf("VK sel char, PW_in[%d] = %c",dig_loc,PW_in[dig_loc]);
		// put char in sreen buffer1 for roll up/down
		txtEditChar[0] = infVK[kb_index];
		ST7565DrawString( (dig_loc%16)*8, (dig_loc/16)*8, txtEditChar, Font_System5x8, BUFFER1);

		dig_loc ++;

		if(dig_loc == PW_LEN){
			UARTprintf("VK sel OK, PW_in[%d] = %s",dig_loc,PW_in);				
			set_language(LangIdx);
			return 1;
		}
		break;
	}
	PWVKybdUpdateTextEditChar();
	ST7565V128x64x1LineDrawH(0, pw_key_offset_x + (dig_loc%16)*6, \
								pw_key_offset_x + ((dig_loc%16)*6)+5, \
								(dig_loc/16 == 0)? pw_key_offset_y + ((dig_loc/16)*8)+8:pw_key_offset_y + ((dig_loc/16)*8)+9, 0x01);
	set_language(LangIdx);
	return 0;
								
}







void PWVKybdUpdateTextEditChar(){
	int x, y;

	x = pw_key_offset_x + (dig_loc%16)*6;

	y = (dig_loc/16 == 0)? pw_key_offset_y + (dig_loc/16)*8:pw_key_offset_y + (dig_loc/16)*8 + 1;
	switch (kb_index){
	case 90:	case 54: 	case 97:	case 126:		case 34: 	case 69:	case 103:		case 132:
	case 32: 	case 67:	case 101:	case 130:		case 31: 	case 66:	case 100:		case 129:
	case 30: 	case 65:	case 99:	case 128:		case 29 : 	case 64:	case 98:		case 127:
	case 28: 	case 63:	case 119:	case 27 : 		case 62:	case 96:	case 125:		case 19 :
		if(PW_in[dig_loc])
			ST7565Draw6x8Char(x, y, PW_in[dig_loc], Font_System5x8);
		else
			ST7565Draw6x8Char(x, y, ' ', Font_System5x8);

		break;
	default:
		ST7565Draw6x8Char(x, y, infVK[kb_index], Font_System5x8);
		break;
	}
}

static const tRectangle rec_VK[] = {
		{10,34,18,41},		{21,34,32,41},		{35,34,42,41},		{46,34,54,41},		{57,34,65,41}, 		//0~4
		{68,34,76,41},		{79,34,87,41},		{90,34,95,41},		{100,34,108,41},	{113,34,120,41},	//5~9
		{18,42,26,49},		{30,42,37,49},		{41,42,49,49},		{53,42,60,49},		{63,42,71,49},		//10~14
		{74,42,82,49},		{85,42,93,49},		{95,42,103,49},		{108,42,114,49},	{2,46,15,53},		//15~19
		{30,50,37,57},		{41,50,49,57},		{52,50,60,57},		{63,50,71,57},		{75,50,82,57},		//20~24
		{85,50,93,57},		{95,50,103,57},		{115,46,127,54},	{2,55,16,61},		{20,57,24,63},		//25~29
		{25,57,29,63},		{30,58,36,62},		{37,58,43,62},		{46,58,107,63},		{112,55,61,125},	//30~34
		{11,35,18,41},		{23,35,31,40},		{35,35,41,40},		{48,35,54,40},		{58,34,65,40},		//35~39
		{69,34,77,40},		{80,35,87,40},		{91,33,96,40},		{101,35,108,40},	{113,35,120,41},	//40~44
		{18,43,25,48},		{29,43,35,48},		{40,41,47,48},		{52,41,59,48},		{63,43,70,49}, 		//45~49
		{74,41,81,48},		{84,41,90,49},		{95,41,102,48},		{106,41,111,48},	{2,46,15,53},		//50~54
		{29,52,35,57},		{40,52,47,57},		{51,52,57,57},		{62,52,69,57},		{73,50,80,57},		//55~59
		{84,52,91,57},		{94,52,102,57},		{115,46,127,54},	{2,55,16,61},		{20,57,24,63},		//60~64
		{25,57,29,63},		{30,58,36,62},		{37,58,43,62},		{46,58,107,63},		{112,55,125,61},	//65~69
		{9,34,14,40},		{21,34,27,40},		{33,34,39,40},		{45,34,52,40},		{56,34,62,40},		//70~74
		{66,34,72,40},		{77,34,84,40},		{88,34,94,40},		{98,34,105,40},		{110,34,117,40},	//75~79
		{10,44,15,46},		{21,41,27,49},		{34,44,38,48},		{45,44,50,49},		{56,41,61,49},		//80~84
		{67,41,72,49},		{78,41,84,49},		{87,42,95,48},		{97,41,108,50},		{111,42,117,45},	//85~89
		{2,47,16,53},		{40,55,44,57},		{53,54,57,57},		{62,50,68,57},		{74,50,78,57}, 		//90~94
		{85,50,90,53},		{115,46,127,54},	{2,55,16,61},		{20,57,24,63},		{25,57,29,63},		//95~99
		{30,58,36,62},		{37,58,43,62},		{46,58,107,63},		{112,55,125,61},	{10,34,15,42},		//100~104
		{21,34,26,42},		{32,34,38,42},		{46,34,52,42},		{55,35,63,41},		{66,34,74,41},		//105~109
		{77,34,85,40},		{88,34,94,39},		{99,35,105,39},		{110,35,117,39},	{38,48,46,50},		//110~114
		{51,43,58,51},		{61,45,69,48},		{72,43,79,49},		{84,43,91,49},		{2,47,16,53},		//115~119
		{40,55,44,57},		{53,54,57,57},		{62,50,68,57},		{74,50,78,57},		{85,50,90,53},		//120~124
		{115,46,127,54},	{2,55,16,61},		{20,57,24,63},		{25,57,29,63},		{30,58,36,62},		//125~129
		{37,58,43,62},		{46,58,107,63},		{112,55,125,61}												//130~132
};


void PWVKybdAntiWhiteItem(uint8_t index){

	ST7565RectReverse(&rec_VK[index], VERTICAL);

}


