//*****************************************************************************
//
// Source File: CheckVin_widget.c
// Description: check VIN code invalid character included?
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 03/22/18     Henry.Y        Created file.
// 09/11/18     Henry.Y        Adjust flow and display.
// 12/07/18     Henry.Y        New display api.
// =============================================================================
//
//
// $Header$
// $DateTime$
// $Author$
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#include "Widgetmgr.h"


void CheckVin_SELECT();
void CheckVin_UP_DOWN();
void CheckVin_display();
void CheckVin_service();
void CheckVin_initial();


widget* CheckVIN_sub_W[] = {
		&TestCode_widget,
		&AbortTest_widget
};

widget CheckVIN_widget = {
		.Key_UP = CheckVin_UP_DOWN,
		.Key_DOWN = CheckVin_UP_DOWN,
		.Key_RIGHT = 0,
		.Key_LEFT = 0,
		.Key_SELECT = CheckVin_SELECT,
		.routine = 0,
		.service = CheckVin_service,
		.initial = CheckVin_initial,
		.display = CheckVin_display,
		.subwidget = CheckVIN_sub_W,
		.number_of_widget = sizeof(CheckVIN_sub_W)/4,
		.index = 0,
		.name = "CheckVIN",
};

void CheckVin_initial(){
	CheckVIN_widget.index = 0;
	SYS_PAR* par = Get_system_parameter();
	TEST_DATA* TD = Get_test_data();
	uint8_t length = (TD->Code_test)? code_recv_len:g_RoVKybdCharPos;
	
	if(length == VININ_LEN1 && check_vin(par->VIN_str,VININ_LEN1)){
		TD->Code_test = 1;
		return;
	}
	if(TD->Code_test) TD->Code_test = 0;
	CheckVIN_widget.parameter = 1;
}

void CheckVin_display(){
	if(CheckVIN_widget.parameter){// INVALID VIN, Result Not Suitable For Warranty
		LCD_display(INVALID_VIN, CheckVIN_widget.index);
	}
}

void CheckVin_SELECT(){

	if(CheckVIN_widget.parameter){
		g_RoVKybdCharPos = 0;
		if(CheckVIN_widget.index){
			Move_widget(CheckVIN_sub_W[1],0);
		}
		else{
			Return_pre_widget();
		}
	}
}


void CheckVin_UP_DOWN(){
	CheckVIN_widget.index ^= 0x01;
	CheckVin_display();
}




void CheckVin_service(){
	if(CheckVIN_widget.parameter == 0){
		Move_widget(CheckVIN_sub_W[0],0);
	}	
}


