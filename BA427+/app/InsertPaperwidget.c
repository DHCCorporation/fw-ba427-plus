//*****************************************************************************
//
// Source File: InsertPaperwidget.c
// Description: Handle no paper event when printing including Click(Push to new menu entry
//				to stack)/Left(Pop out stack)/Up/Down function body.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 03/09/16     Vincent.T      Created file.
// 03/31/16     Vincent.T      1. Modify InsertPaperClick(); to handle multination language case.
//                             2. Modify InsertPaperShow(); to handle multination language case.
// 06/14/16     Vincent.T      1. #include "TestCounterwidget.h"
//                             2. #include "Printresultwidget.h"
//                             3. global variables g_PrintResult and  ui8TestCounterYesNo
//                             4. Modify InsertPaperClick();(No paper event -> print Yes/NO)
// 09/21/17 	Jerry.W		   Rewrite Widget for new software architecture
// 11/23/17     Jerry.W        1. add functionality of countdown for printer mode to shut down
// 12/07/18     Henry.Y        New display api.
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/app/InsertPaperwidget.c#1 $
// $DateTime: 2017/11/27 10:38:22 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#include "Widgetmgr.h"

void InsertPaper_DISPLAY();
void InsertPaper_SELECT();
void InsertPaper_routine();

widget* InsertPaper_sub_W[] = {
};

widget InsertPaper_widget = {
		.Key_UP = 0,
		.Key_DOWN = 0,
		.Key_RIGHT = 0,
		.Key_LEFT = 0,
		.Key_SELECT = InsertPaper_SELECT,
		.routine = 0,
		.service = 0,
		.initial = 0,
		.display = InsertPaper_DISPLAY,
		.subwidget = InsertPaper_sub_W,
		.number_of_widget = sizeof(InsertPaper_sub_W)/4,
		.index = 0,
		.name = "InsertPaper",
};


void InsertPaper_SELECT( )
{
	Return_pre_widget();
}

void InsertPaper_DISPLAY()
{
    ST7565ClearScreen(BUFFER1);
    ST7565Refresh(BUFFER1);
    LCD_String_ext(16,get_Language_String(S_INSERT_PAPER) , Align_Central);
    LCD_String_ext(32,get_Language_String(S_PRESS_ENTER) , Align_Central);
    LCD_Arrow(Arrow_enter);
}




