//*****************************************************************************
//
// Source File: USB_widget.c
// Description: engineering mode
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 06/21/18     Henry.Y        Create file.
// 11/19/18     Henry.Y        Add WIFI and scanner USB command.
// 12/07/18     Henry.Y        New display api.
// 02/24/20     Henry.Y        Scanner control modification.
// =============================================================================
//
//
// $Header$
// $DateTime$
// $Author$
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#include "Widgetmgr.h"

//**************************************************
//
// Tester name: DHCBT2100,BT,@
//
//**************************************************
#ifdef PCB_CHECK_MODE
uint8_t *ui8TesterName = "DHCBT2200,MFC,@";
#else
uint8_t *ui8TesterName = "DHCBT2200,BT,@";
#endif


typedef struct{
	uint8_t ui8Lang;
	uint8_t ui8Scrn_Prt_FldIdx;	
}
screen_printout_update_info;

static screen_printout_update_info update_info = {0,0};
//*****************
// Global variables
//*****************
static USB_MODE ui8USBMode = USB_READY;
static uint8_t ui8USBModeSub = 0;
uint8_t *pui8USBUpdataBuf = NULL;
uint32_t USB_rcv_len = 0;
static uint8_t *pui8_FLS_UpdataBuf = NULL;//Pointer to indicate printer/LCD database(usb -> fls)
static uint32_t ui32USBDataMoveOffset = 0; 	//memory offset per 48byte for data storage(usb -> fls)
static uint16_t ui32CMDLen;

uint32_t USB_FLS_start_add;
uint32_t USB_FLS_total_len;
uint32_t USB_FLS_CRC;
uint32_t USB_FLS_CRC_cal;
uint32_t USB_FLS_idx;

Timer_t* USB_get_module_version_timer;
Timer_t* USB_module_test_timer;
volatile uint8_t USB_get_module_version_counter;
volatile uint8_t USB_module_test_timer_counter;

uint8_t *module_test_data = NULL;
uint16_t module_test_len = 1024*4;
static volatile uint8_t module_test_flag = 0;
static uint8_t USB_get_module_version_flg = 0;
static uint8_t WIFI_VER[40] = {0};
bool need_display = false;

void module_test_cb(uint16_t len, uint8_t *data){
	uint16_t i;
	if(len != module_test_len){
		module_test_flag = 100;
		//UARTprintf("Module test fail : length error\n");
	}

	for(i = 0 ; i < module_test_len; i++){
		if(module_test_data[i] != data[i]) break;
	}
	if(i != module_test_len){
		module_test_flag = 101;
		//UARTprintf("Module test fail : data error @ %d byte\n", i);
	}
	else{
		module_test_flag = 1;
		//UARTprintf("Module test success: %d bytes.\n", module_test_len);
	}

	free(module_test_data);
	module_test_data = NULL;
}

void wifi_module_test(){
	uint16_t i;
	if(module_test_data) free(module_test_data);
	module_test_data = malloc(module_test_len);

	for(i = 0; i < module_test_len ; i++){
		module_test_data[i] = rand() % 256;
	}
	wifi_module_loopback(module_test_len, module_test_data, module_test_cb);
}

void USB_WIFI_version_timer_cb(void){
	if(USB_get_module_version_timer){
		if(USB_get_module_version_counter) USB_get_module_version_counter--;
		else{
			if(!USB_get_module_version_flg) USB_get_module_version_flg = 105;// get WIFI module version timeout
		}
		
		UARTprintf("USB_WIFI_version_timer_cb, counter = %d\n\r", USB_get_module_version_counter);
	}
}

void USB_GET_WIFI_version_cb(uint8_t*ver){

	if(strlen((char*)ver) > sizeof(WIFI_VER)){
		USB_get_module_version_flg = 100;
	}
	else{
		USB_get_module_version_flg = 1;
		memcpy(WIFI_VER,ver,strlen((char*)ver));
	}
}

void USB_module_test_timer_cb(void){
	if(USB_module_test_timer){
		if(USB_module_test_timer_counter) USB_module_test_timer_counter--;
		else{
			if(!module_test_flag) module_test_flag = 105;
		}
		
		UARTprintf("USB_module_test_timer_cb, counter = %d\n\r", USB_module_test_timer_counter);
	}
}

static uint8_t USB_get_WIFI_ver(uint8_t sec){
	if(!Get_module_version(USB_GET_WIFI_version_cb)) return 101;
	
	if(USB_get_module_version_timer) unregister_timer(USB_get_module_version_timer);
	if(sec){
		USB_get_module_version_counter = sec;
		USB_get_module_version_timer = register_timer(10,USB_WIFI_version_timer_cb);
		start_timer(USB_get_module_version_timer);
	}
	return 0;
}

static uint8_t USB_module_test_timer_setup(uint8_t sec){	
	if(USB_module_test_timer) unregister_timer(USB_module_test_timer);
	if(sec){	
		USB_module_test_timer_counter = sec;
		USB_module_test_timer = register_timer(10,USB_module_test_timer_cb);
		start_timer(USB_module_test_timer);
	}
	return 0;
}

static uint8_t USB_SCANNER_RECEIVED[SCANNER_RX_BUFFER_SIZE];
static uint8_t USB_scanner_alloc_Len;

void USB_SCANNER_CB(bool flush, uint8_t data){
	 uint8_t ui8buf = 0;
	 static uint8_t RX_index = 0;
	 static bool bCR = false, bNL = false;
	 static uint8_t UART0_RX_buffer[SCANNER_RX_BUFFER_SIZE] = {0};
	 
	if(flush){
		RX_index = 0;
		bCR = false;
		bNL = false;
	}
	else{
		if(RX_index != USB_scanner_alloc_Len){
			ui8buf = data;//(unsigned char)ROM_UARTCharGetNonBlocking(SCANNER_UART_BASE);
			UART0_RX_buffer[RX_index] = ui8buf;
			RX_index++;
			
			if(ui8buf == 0x0D) bCR = true;
			if(ui8buf == 0x0A) bNL = true;
			if(bCR && bNL){
			
				memcpy(USB_SCANNER_RECEIVED , UART0_RX_buffer , SCANNER_RX_BUFFER_SIZE);
				USB_SCANNER_RECEIVED[RX_index - 1] = '\0';
				USB_SCANNER_RECEIVED[RX_index - 2] = '@';
				memset(UART0_RX_buffer, 0, SCANNER_RX_BUFFER_SIZE);
				module_test_flag = 1;
				RX_index = 0;
				bCR = false;
				bNL = false;
			}
			else if(RX_index == USB_scanner_alloc_Len){
				memcpy(USB_SCANNER_RECEIVED , UART0_RX_buffer , SCANNER_RX_BUFFER_SIZE);
				USB_SCANNER_RECEIVED[RX_index] = '@';
				memset(UART0_RX_buffer, 0, SCANNER_RX_BUFFER_SIZE);
				module_test_flag = 1;
				RX_index = 0;
				bCR = false;
				bNL = false;
			}
		}
	}
 }

void USB_UP();
void USB_DOWN();
void USB_RIGHT();
void USB_LEFT();
void USB_SELECT();
void USB_service();
void USB_initail();
void USB_DISPLAY();
void USB_routine();
void USB_CMD_PROCESS(void);
void USB_command_callback(uint8_t *command, uint32_t len);

widget USB_widget = {
		.Key_UP = USB_UP,
		.Key_DOWN = USB_DOWN,
		.Key_RIGHT = USB_RIGHT,
		.Key_LEFT = USB_LEFT,
		.Key_SELECT = USB_SELECT,
		.routine = USB_routine,
		.service = 0,
		.initial = USB_initail,
		.display = USB_DISPLAY,
		.subwidget = 0,
		.number_of_widget = 0,
		.index = 0,
		.no_messgae = 1,
};
void USB_initail(){
	UARTprintf("USB_initail()\n");	
	//IntBat_SW_ON;
	set_language(en_US);
	set_scanner_cb(USB_SCANNER_CB);
	if(get_routine_switch()){
		Main_routine_switch(0);
	}
	ui8USBMode = USB_READY;	

	USB_svc_set_CB(USB_command_callback);
	
	//wifi_config(1, 0);
	USB_get_module_version_flg = USB_get_WIFI_ver(5);
}

void USB_DISPLAY(){
	UARTprintf("USB_DISPLAY()\n");	
	// Clears the screen
	ST7565ClearScreen(BUFFER0); // _st7565buffer1

	switch(ui8USBMode){
	case USB_DETECT_VOLTAGE:
		ST7565Refresh(BUFFER0);
		ST7565DrawStringx16(20, 21, "USB MODE", Font_System5x8);
		ST7565DrawStringx16(10, 31, "DETECT VOLTAGE", Font_System5x8);		
		break;
	case USB_WIFI:
		ST7565Refresh(BUFFER0);
		ST7565DrawStringx16(20, 21, "USB MODE", Font_System5x8);
		ST7565DrawStringx16(10, 31, "WIFI CHECK", Font_System5x8);		
		break;
	case USB_SCANNER:
		ST7565Refresh(BUFFER0);
		ST7565DrawStringx16(20, 21, "USB MODE", Font_System5x8);
		ST7565DrawStringx16(10, 31, "SCANNER FUNCTION", Font_System5x8);		
		break;
	default:
		LCD_display(USB,0);
		break;
	}
}



void USB_UP(){
	if(ui8USBMode == USB_BUTTON){
		ui8USBMode = USB_READY;		
		USBSendByte("U@", 2);		
	}
}
void USB_DOWN(){
	if(ui8USBMode == USB_BUTTON){
		ui8USBMode = USB_READY;		
		USBSendByte("D@", 2);		
	}

}

void USB_RIGHT(){
	if(ui8USBMode == USB_BUTTON){
		ui8USBMode = USB_READY;		
		USBSendByte("R@", 2);		
	}

}
void USB_LEFT(){
	if(ui8USBMode == USB_BUTTON){
		ui8USBMode = USB_READY;		
		USBSendByte("L@", 2);		
	}

}
void USB_SELECT(){
	if(ui8USBMode == USB_BUTTON){
		ui8USBMode = USB_READY;		
		USBSendByte("E@", 2);		
	}

}

void USB_command_callback(uint8_t *command, uint32_t len){
	uint32_t i;

#if 1
	{
		UARTprintf("USB get ");
		for(i= 0; i<len; i++){
			UARTprintf("%c ",command[i]);
		}
		UARTprintf("\n");
	}
#endif

	if(!USB_widget.parameter){
		USB_widget.parameter = 1;
        USB_rcv_len = len;
		if(ui8USBMode == USB_READY && len == 1){
			memcpy((char *)pui8USBUpdataBuf, command , len);
		}
        else if(ui8USBMode == USB_FLASH){
            if(pui8USBUpdataBuf == NULL){
                UARTprintf("FxxK Error #4!!!!\n");
                USBSendByte("!@", 2);
                ui8USBMode = USB_READY;

                return;
            }

            uint32_t offset = (USB_FLS_start_add + USB_FLS_idx) % 4096;

            memcpy(pui8USBUpdataBuf+offset, command, len);

            USB_FLS_idx += len;
            if((USB_FLS_idx % (4*1024)) != 0){
                if(USB_FLS_total_len != USB_FLS_idx){
                    USB_widget.parameter = 0;
                }
            }
        }
		else if(command[len -1] == '@' && len <= ui32CMDLen){
			memcpy((char *)pui8USBUpdataBuf, command , len);
		}
		else if(len == ui32CMDLen){
			memcpy((char *)pui8USBUpdataBuf, command , len);
		}
		else{	//command error
			//USB_widget.parameter = 0;
			pui8USBUpdataBuf[0] = '!';
			ui8USBMode = USB_READY;
		}
	}
    else{
        UARTprintf("drop\n");
    }
}

void screen_printout_upload_cb(uint8_t *command, uint32_t len)
{
	static uint32_t i = 0;
	uint16_t data_len;
	
#if 1
	{
		uint16_t ii;
		UARTprintf("USB get ");
		for(ii= 0; ii<len; ii++){
			UARTprintf("%c ",command[ii]);
		}
		UARTprintf("\n");
	}
#endif
	

	if(ui8USBMode == USB_PRT_UP && ui8USBModeSub == SUSB_PRT_DATA){
		data_len = USB_PRT_UPDATE_LEN;
	}
	else if(ui8USBMode == USB_LCD_UP && ui8USBModeSub == SUSB_LCD_DATA){
		data_len = USB_LCD_UPDATE_LEN;
	}
	else{
		UARTprintf("\nscreen_printout_upload_cb unknown issue\n");
		i = 0;
		ui8USBMode = USB_READY;
		
		return;
	}
	
	while(len){
		pui8USBUpdataBuf[i] = (*command);		
		i++;
	
		switch(*command){
		case '@':
			i = 0;
			// update done
			USB_widget.parameter = 1;
			ui8USBModeSub = (ui8USBMode == USB_PRT_UP)? SUSB_PRT_END:SUSB_LCD_END;
			
			USB_svc_set_CB(USB_command_callback);
			return;
	
		default:
			if(i == data_len){
				i = 0;
				USB_widget.parameter = 1;
				return;
			}
			break;
	
		}
		len--;
		command++;
	
	}

}





void USB_CMD_PROCESS(void){
	SYS_INFO* info = Get_system_info();	
	MFC_TD* MT = Get_MFC_TEST_DATA();

	if(ui8USBMode == USB_READY){
		switch(pui8USBUpdataBuf[0]){
		case 'z':
		{
			USBSendByte(ui8TesterName, strlen((const char *)ui8TesterName));
			break;
		}		
		case 'M':
		{
			char* buf = (MT->switch_up)? "MFCpass@":"MFCfail@";
			USBSendByte((uint8_t*)buf, strlen(buf));
			break;
		}
		case 'f':
		{
			// fw + fls version
			char buf[50] = {0X00};
			char FLS_ver[21] = {0X00};
			
			
			A25L032ReadData(0, (uint8_t*)FLS_ver, sizeof(FLS_ver));
			snprintf(buf,sizeof(buf),"%s,%s@",strFWver,FLS_ver);
			
			USBSendByte((uint8_t*)buf, strlen(buf));
			break;
		}			
		case 'p':
		{
			//************************
			//USB_READY -> USB_PRT_UP
			//************************
			UARTprintf("USB Ready -> USB Printer Update...\n");
			ui8USBMode = USB_PRT_UP;
			ui8USBModeSub = SUSB_PRT_START;
			USBSendByte(pui8USBUpdataBuf, 1);
			break;
		}
		case 'o':
		{
			//*************************
			//USB_READY -> USB_PRT_READ
			//*************************
			UARTprintf("USB Ready -> USB Printer Read...\n");
			ui8USBMode = USB_PRT_READ;
			ui8USBModeSub = SUSB_PRT_START;
			USBSendByte(pui8USBUpdataBuf, 1);
			break;
		}
		case 'u':
		{
			//***********************
			//USB_READY -> USB_LCD_UP
			//***********************
			UARTprintf("USB Ready -> USB LCD Update...\n");			
			ui8USBMode = USB_LCD_UP;
			ui8USBModeSub = SUSB_LCD_STRART;
			USBSendByte(pui8USBUpdataBuf, 1);
			break;
		}
		case 'r':
		{
            UARTprintf("get [r]!\n");

            if(pui8USBUpdataBuf[1] == 'f'){
                UARTprintf("get [f]!\n");

                char *temp;
                USB_FLS_start_add = strtoul((char*)pui8USBUpdataBuf+3, &temp, 16);
                USB_FLS_total_len = strtoul(temp+1, &temp, 16);
                USB_FLS_CRC = strtoul(temp+1, NULL, 16);
                UARTprintf("USB_FLS_CRC = %x, %s\n", USB_FLS_CRC, temp+1);
                USB_FLS_idx = 0;
                USB_FLS_CRC_cal = 0;

                if(USB_FLS_start_add < 1024*1024 && USB_FLS_total_len != 0)
                {
                    free(pui8USBUpdataBuf);
                    pui8USBUpdataBuf = malloc(FLASH_SECTOR_SIZE);
                    if(pui8USBUpdataBuf == NULL)
                    {
                        UARTprintf("FxxK Error #1!!!!\n");
                        USBSendByte((uint8_t*)"!@", 2);
                        return;
                    }
                    ui8USBMode = USB_FLASH;
                    UARTprintf("USB flash write mode!\n");
                    UARTprintf("start add %d\n", USB_FLS_start_add);
                    UARTprintf("data length %d\n", USB_FLS_total_len);

                    USBSendByte("rf@", 3);
                }
                return;
            }
            else{
                UARTprintf("get [?]!\n");

			//*************************
			//USB_READY -> USB_LCD_READ
			//*************************
			UARTprintf("USB Ready -> USB LCD Read...\n");
			ui8USBMode = USB_LCD_READ;
			ui8USBModeSub = SUSB_LCD_STRART;
			USBSendByte(pui8USBUpdataBuf, 1);

            }
			break;
		}
		case 'a':
		{
			USBTestRecordUpload();
			UARTprintf("USB Test Record uploading finish!\n");
			break;
		}
		case 'v':
		{
			//************************
			//USB_READY -> USB_EXTV_CAL
			//************************
			UARTprintf("USB Ready -> USB EXTERNAL VOLTAGE CALIBRATION...\n");					
			ui8USBMode = USB_EXTV_CAL;
			USBSendByte("v@", 2);
			break;
		}
		case 'i':
		{
			//************************
			//USB_READY -> USB_INTV_CAL
			//************************
			UARTprintf("USB Ready -> USB INTERNAL VOLTAGE CALIBRATION...\n");
			ui8USBMode = USB_INTV_CAL;
			USBSendByte("i@", 2);
			break;
		}
		case 'V':
		{
			//************************
			//
			//  Voltage Meter
			//  - Calibration
			//
			//************************
			UARTprintf("USB Ready -> USB VM Kmode...\n");
			ui8USBMode = USB_KVM;
			USBSendByte("V@", 2);
			break;
		}
		case 'A':
		{
			//************************
			//
			//  Amper Meter
			//  - Calibration
			//
			//************************
			UARTprintf("USB Ready -> USB AM Kmode...\n");
			ui8USBMode = USB_KAM;
			USBSendByte("A@", 2);
			break;
		}
		case 'c':
		{
			//
			// Test Record Erase!
			//
			info->TotalTestRecord = 0;
			save_system_info(&info->TotalTestRecord, sizeof(info->TotalTestRecord));

			info->TestRecordOffset = 0;
			save_system_info(&info->TestRecordOffset, sizeof(info->TestRecordOffset));

			info->TotalSYSrecord = 0;
			save_system_info(&info->TotalSYSrecord, sizeof(info->TotalSYSrecord));
			
			info->SYSrecordOffset = 0;
			save_system_info(&info->SYSrecordOffset, sizeof(info->SYSrecordOffset));

			UARTprintf("USB Test Record delete finish!\n");
			
			USBSendByte("@", 1);
			break;
		}
		case 'T':
		{
			//
			// Temperature Sensor
			//	  Calibration
			//
			UARTprintf("USB Ready -> USB Temp Kmode...\n");
			ui8USBMode = USB_KTEMP;
			USBSendByte("T@", 2);
			break;
		}
		case 't':
		{
			UARTprintf("USB Ready -> USB Time upload...\n");
			USBSendByte(pui8USBUpdataBuf, 1);
			USBTime();
			break;
		}			
		case 'L':
		{
			//
			//  Load Resistor Voltage
			//    Calibration
			//
			UARTprintf("USB Ready -> USB Load Resistor Voltage Kmode...\n");
			ui8USBMode = USB_KVL;
			USBSendByte("L@", 2);
			break;
		}
		case 'C':
		{
			//
			// Cold Cranking Amper
			//    Calibration
			//
			UARTprintf("USB Ready -> USB CCA Kmode...\n");
			ui8USBMode = USB_KCCA;
			USBSendByte("C@", 2);
			break;
		}
		case 'R':
		{
			//
			// Internal Resistor
			//    Calibration
			//
			UARTprintf("USB Ready -> USB IR Kmode...\n");
			ui8USBMode = USB_KIR;
			USBSendByte("R@", 2);
			break;
		}
		case 'S':
		{
			//
			//Serial Number Flash -> DFU
			//
			UARTprintf("USB Ready -> USB Serial Number Flash Mode...\n");
			ui8USBMode = USB_SN;
			ui8USBModeSub = USB_SN_READY;
			USBSendByte("S@", 2);
			break;
		}
		case 'b':
		{
			UARTprintf("USB Ready -> USB BUTTON CHECK MODE...\n");
			ui8USBMode = USB_BUTTON;
			USBSendByte("b@", 2);
			break;
		}
		case '?':
		{
			UARTprintf("USB Ready -> USB AUTO SCAN VOLTAGE MODE...\n");
			ui8USBMode = USB_DETECT_VOLTAGE;
			USBSendByte("?@", 2);
			break;
		}
		case 'B':
		{
			UARTprintf("USB Ready -> USB SCANNER CHECK MODE...\n");			
			ui8USBMode = USB_SCANNER;
			ui8USBModeSub = USB_SCANNER_READY;			
			USBSendByte("B@", 2);
			break;
		}
		case 'W':
		{
			ui8USBMode = USB_WIFI;
			ui8USBModeSub = USB_WIFI_FUNCTION_CHECK_READY;
			USBSendByte("W@", 2);			
			break;
		}
		case 'd':
		{					
			//**********************
			//DFU Mode
			//Config.
			//**********************
			//Terminate the USB device and detach from the bus.
			// Delay 3s to make the disconnection between device and PC work safely.
			ROM_SysCtlDelay(get_sys_clk() / 3);
			ROM_SysCtlDelay(get_sys_clk() / 3);
			ROM_SysCtlDelay(get_sys_clk() / 3);

			USBDCDTerm(0);
			//Disable SysTick and its interrupt.
			ROM_SysTickIntDisable();
			ROM_SysTickDisable();
			// Disable all processor interrupts.  Instead of disabling them one at a
			// time, a direct write to NVIC is done to disable all peripheral
			// interrupts.
			HWREG(NVIC_DIS0) = 0xffffffff;
			HWREG(NVIC_DIS1) = 0xffffffff;
			HWREG(NVIC_DIS2) = 0xffffffff;
			HWREG(NVIC_DIS3) = 0xffffffff;
			HWREG(NVIC_DIS4) = 0xffffffff;
			// Enable and reset the USB peripheral.
			ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_USB0);
			ROM_SysCtlPeripheralReset(SYSCTL_PERIPH_USB0);
			ROM_USBClockEnable(USB0_BASE, 8, USB_CLOCK_INTERNAL);
			ROM_SysCtlDelay(get_sys_clk() / 3);
			// Re-enable interrupts at the NVIC level.
			ROM_IntMasterEnable();
			//Call the USB boot loader.
			Jump_to_booloader_DFU();
			break;
		}			
		case '!':		
		default:
			//**********************
			//@#$#@T^#$^T%$^&$@#%#%@
			//**********************
			USBSendByte("!@", 2);
			break;
		}
	}
	else if(ui8USBMode == USB_BUTTON){
		ui8USBMode = USB_READY;		
		USBSendByte("!@", 2);		
	}
	//********************
	//PRITNOUT UPDATA MODE
	//********************
	else if(ui8USBMode == USB_PRT_UP){
		//*****************************
		//Renew Printout Database Index
		//*****************************
		if(ui8USBModeSub == SUSB_PRT_START){
			if(pui8_FLS_UpdataBuf != NULL){
				free(pui8_FLS_UpdataBuf);
				pui8_FLS_UpdataBuf = NULL;
			}
			
			//**************************************
			//Get Language and Printout Layout Index
			//'$'(Start) + 2(Language) + 2(LCD Screen Index) + '!'(End)
			//**************************************
			update_info.ui8Lang = ASCII2HEX(pui8USBUpdataBuf + 1, 2);
			update_info.ui8Scrn_Prt_FldIdx = ASCII2HEX(pui8USBUpdataBuf + 3, 2);
			UARTprintf("USB Printer Update: Language: %d, and Printerout Field Index: %d\n", update_info.ui8Lang, update_info.ui8Scrn_Prt_FldIdx);
			
			//************************
			//Logo		->	4096 Byte
			//Others	->	2048 Byte
			//************************
			uint16_t malloc_size = (update_info.ui8Scrn_Prt_FldIdx == PRT_LOGO)? FLASH_SECTOR:FLASH_EIGHT_PAGE;			
			pui8_FLS_UpdataBuf = malloc(malloc_size * sizeof(uint8_t));
			memset(pui8_FLS_UpdataBuf, 'F', malloc_size);
			ui32USBDataMoveOffset = 0;
			USB_svc_set_CB(screen_printout_upload_cb);
			
			ui8USBModeSub = SUSB_PRT_DATA;
			USBSendByte("&", 1);
		}
		else if(ui8USBModeSub == SUSB_PRT_DATA){
			UARTprintf("USB Printer Update Data Get\n");
			//************************************************************
			////'$'(Start) + 96(Content) + '!'(End)
			//Handle pui8USBUpdataBuf; Remove $(first) and !(last) characters
			//************************************************************
			//ASCII -> HEX
			//0xFF -> 255
			USBDataBufASCII2HEX(&pui8USBUpdataBuf[1], pui8USBUpdataBuf, (USB_PRT_UPDATE_LEN - 2)/2);
			//MOVE DATA(pui8USBUpdataBuf -> pui8_FLS_UpdataBuf)
			USBDataMove(pui8USBUpdataBuf, (pui8_FLS_UpdataBuf + ui32USBDataMoveOffset), (USB_PRT_UPDATE_LEN - 2)/2);
			//Offset
			ui32USBDataMoveOffset = ui32USBDataMoveOffset + ((USB_PRT_UPDATE_LEN - 2)/2);

			USBSendByte("&", 1);
		}
		else if(ui8USBModeSub == SUSB_PRT_END){
			UARTprintf("USB Printer Update Data End\n");
			//****************************
			//Ready to write data to flash
			//****************************
			USBPrtExtMemICUpdate(pui8_FLS_UpdataBuf, update_info.ui8Lang, update_info.ui8Scrn_Prt_FldIdx);

			free(pui8_FLS_UpdataBuf);
			pui8_FLS_UpdataBuf = NULL;

			PrinterStart(update_info.ui8Scrn_Prt_FldIdx, STB_12_34_56, 1.5, 1, (update_info.ui8Scrn_Prt_FldIdx == PRT_LOGO)? 3:4);	PrinterPrintSpace(20);
			
			USBSendByte("#", 1);
			ui8USBMode = USB_READY;

		}
	}
	//******************
	//PRINTOUT READ MODE
	//******************
	else if(ui8USBMode == USB_PRT_READ){
		//*****************************
		//Renew Printout Database Index
		//*****************************
		if(ui8USBModeSub == SUSB_PRT_START){
			//**************************************
			//Get Language and Printout Layout Index
			//**************************************
			update_info.ui8Lang = ASCII2HEX(pui8USBUpdataBuf + 1, 2);
			update_info.ui8Scrn_Prt_FldIdx = ASCII2HEX(pui8USBUpdataBuf + 3, 2);
			UARTprintf("USB Printer Read: Language: %d, and Printerout Field Index: %d\n", update_info.ui8Lang, update_info.ui8Scrn_Prt_FldIdx);
			
			USBSendByte("&", 1);
			ui8USBModeSub = SUSB_PRT_END;
		}
		else if(ui8USBModeSub == SUSB_PRT_END){
			UARTprintf("USB Printer Read Data End\n");
	
			USBSendByte("#", 1);
	
			ui8USBMode = USB_READY;

			PrinterStart(update_info.ui8Scrn_Prt_FldIdx, STB_12_34_56, 1.5, 1, (update_info.ui8Scrn_Prt_FldIdx == PRT_LOGO)? 3:4);		PrinterPrintSpace(20);
		}
	}
	//***************
	//LCD UPDATE MODE
	//***************
	else if(ui8USBMode == USB_LCD_UP){
		//*******************************
		//Renew LCD Screen Datebase Index
		//*******************************
		if(ui8USBModeSub == SUSB_LCD_STRART){
			
			if(pui8_FLS_UpdataBuf != NULL){
				free(pui8_FLS_UpdataBuf);
				pui8_FLS_UpdataBuf = NULL;
			}

			//**************************************
			//Get Language and Printout Layout Index
			//**************************************
			update_info.ui8Lang = ASCII2HEX(pui8USBUpdataBuf + 1, 2);
			update_info.ui8Scrn_Prt_FldIdx = ASCII2HEX(pui8USBUpdataBuf + 3, 2);
			UARTprintf("USB Lcd Update: Language: %d, and Lcd Screen Index: %d\n", update_info.ui8Lang, update_info.ui8Scrn_Prt_FldIdx);
			
			//***************
			//Memory Allcoate
			//Get 1024 byte
			//***************
			pui8_FLS_UpdataBuf = malloc(FLASH_FOUR_PAGE * sizeof(uint8_t));
			memset(pui8_FLS_UpdataBuf, 'F', FLASH_FOUR_PAGE);
			
			USB_svc_set_CB(screen_printout_upload_cb);
			
			ui8USBModeSub = SUSB_LCD_DATA;
			USBSendByte("&", 1);

		}
		else if(ui8USBModeSub == SUSB_LCD_DATA){
			UARTprintf("USB LCD Update Data Get\n");
			//*****************************************************************
			//Handle pui8_FLS_UpdataBuf; Remove $(first) and !(last) characters
			//*****************************************************************
			//ASCII -> HEX
			//0xFF -> 255
			USBDataBufASCII2HEX(&pui8USBUpdataBuf[1], pui8USBUpdataBuf, (USB_LCD_UPDATE_LEN - 2)/2);
			//MOVE DATA(pui8USBUpdataBuf -> pui8_FLS_UpdataBuf)
			USBDataMove(pui8USBUpdataBuf, pui8_FLS_UpdataBuf, (USB_LCD_UPDATE_LEN - 2)/2);
						
			USBSendByte("&", 1);
		}
		else if(ui8USBModeSub == SUSB_LCD_END){
			UARTprintf("USB LCD Update Data End\n");

			USBLCDScreenUpdate((LANGUAGE) update_info.ui8Lang, update_info.ui8Scrn_Prt_FldIdx, pui8_FLS_UpdataBuf, ((USB_LCD_UPDATE_LEN - 2)/2));
			
			free(pui8_FLS_UpdataBuf);
			pui8_FLS_UpdataBuf = NULL;

			USBSendByte("#", 1);
			ui8USBMode = USB_READY;

		}
	}
	//*************
	//LCD READ MODE
	//*************
	else if(ui8USBMode == USB_LCD_READ){
		//*******************************
		//Renew LCD Screen Datebase Index
		//*******************************
		if(ui8USBModeSub == SUSB_LCD_STRART){
			//**************************************
			//Get Language and Printout Layout Index
			//**************************************
			update_info.ui8Lang = ASCII2HEX(pui8USBUpdataBuf + 1, 2);
			update_info.ui8Scrn_Prt_FldIdx = ASCII2HEX(pui8USBUpdataBuf + 3, 2);
			UARTprintf("USB Lcd Read: Language: %d, and Lcd Screen Index: %d\n", update_info.ui8Lang, update_info.ui8Scrn_Prt_FldIdx);

			USBSendByte("&", 1);
			ui8USBModeSub = SUSB_LCD_END;
		}
		else if(ui8USBModeSub == SUSB_LCD_END){
			UARTprintf("USB LCD Read Data End\n");
	
			USBLCDScreenRead((LANGUAGE) update_info.ui8Lang, update_info.ui8Scrn_Prt_FldIdx);
	
			USBSendByte("#", 1);
			ui8USBMode = USB_READY;
		}
	}
	//****************************
	//External voltage calibration
	//Internal voltage calibration
	//VOLTAGE METER KMODE
	//****************************
	else if(ui8USBMode == USB_EXTV_CAL||ui8USBMode == USB_INTV_CAL||ui8USBMode == USB_KVM||ui8USBMode == USB_KAM||ui8USBMode == USB_KVL||ui8USBMode == USB_KTEMP){		
		USBVoltageCalibration(ui8USBMode,pui8USBUpdataBuf);

		ui8USBMode = USB_READY;
	}
	else if(ui8USBMode == USB_KCCA||ui8USBMode == USB_KIR){
		USBIRCCACalibration(ui8USBMode,pui8USBUpdataBuf);

		ui8USBMode = USB_READY;
	}
	else if(ui8USBMode == USB_SN){
		if(ui8USBModeSub == USB_SN_READY){
			if(!memcmp("C@",pui8USBUpdataBuf,2)){
				ui8USBModeSub = USB_SN_C_WRITE;
				USBSendByte("C@", 2);
			}
			else if (!memcmp("P@",pui8USBUpdataBuf,2)){
				ui8USBModeSub = USB_SN_P_WRITE;
				USBSendByte("P@", 2);
			}
			else if(!memcmp("c@",pui8USBUpdataBuf,2)){				
				USBSNPC(info->SNC,sizeof(info->SNC));
				ui8USBMode = USB_READY; 		
			}
			else if(!memcmp("p@",pui8USBUpdataBuf,2)){		
				USBSNPC(info->SNP,sizeof(info->SNP));
				ui8USBMode = USB_READY; 		
			}
			else{
				USBSendByte("!@", 2);
				ui8USBMode = USB_READY; 		
			}
		}
		else if(ui8USBModeSub == USB_SN_C_WRITE){
			uint8_t len = sizeof(info->SNC);
			//Reset Internal Serial Numbrt
			memset(info->SNC, ' ', len);
			
			//Move CMD into g_SNInfo
			memcpy(info->SNC, pui8USBUpdataBuf, len);
			
			//Wirte data into internal EEPROM
			save_system_info(info->SNC, len);

			USBSendByte("#@", 2);
			ui8USBMode = USB_READY; 		
		}
		else if(ui8USBModeSub == USB_SN_P_WRITE){
			uint8_t len = sizeof(info->SNP);
			//Reset Internal Serial Numbrt
			memset(info->SNP, ' ', len);
			
			//Move CMD into g_SNInfo
			memcpy(info->SNP, pui8USBUpdataBuf, len);
			
			//Wirte data into internal EEPROM
			save_system_info(info->SNP, len);

			USBSendByte("#@", 2);
			ui8USBMode = USB_READY; 		

		}
		else{
			USBSendByte("!@", 2);
			ui8USBMode = USB_READY; 		
		}
	}
	else if(ui8USBMode == USB_WIFI){
		MFC_TD* MT = Get_MFC_TEST_DATA();
		if(ui8USBModeSub == USB_WIFI_FUNCTION_CHECK_READY){
			if(!memcmp("ver@",pui8USBUpdataBuf,4)){
				ui8USBModeSub = USB_WIFI_VER;
				USB_get_module_version_flg = USB_get_WIFI_ver(5);
			}
			else if (!memcmp("t@",pui8USBUpdataBuf,2)){
				ui8USBModeSub = USB_WIFI_LOOPBACK;
				module_test_flag = USB_module_test_timer_setup(5);
				wifi_module_test();
			}
			else if(!memcmp("writemfcssidpw@",pui8USBUpdataBuf,15)){
				ui8USBModeSub = USB_SSID_PW_WRITE;
				USBSendByte("ready@", 6);
			}
			else if(!memcmp("readmfcssidpw@",pui8USBUpdataBuf,14)){
				char buf[SSID_LEN+PW_LEN] = {0};
				snprintf(buf,sizeof(buf),"%s,%s@",MT->SSID,MT->SSID_PW);
				USBSendByte((uint8_t*)buf, strlen(buf));
				ui8USBMode = USB_READY;
				
			}
			else{
				USBSendByte("!@", 2);
				ui8USBMode = USB_READY;
			}
		}
		else if(ui8USBModeSub == USB_SSID_PW_WRITE){
			// SSID+','+PW+'@'
			char *temp = strstr((char*)pui8USBUpdataBuf,",");
			if(temp == NULL){
				USBSendByte("!@", 2);
			}
			else{
				memset(MT->SSID, 0, SSID_LEN);
				memcpy(MT->SSID,pui8USBUpdataBuf,(strlen((char*)pui8USBUpdataBuf)-strlen(temp)));
				*temp = '\0';
				char* PW = temp+1;
				memset(MT->SSID_PW, 0, PW_LEN);
				memcpy(MT->SSID_PW,PW,(strlen(PW)-1));
				
				if(!save_MFC_TEST_DATA(MT->SSID, sizeof(MT->SSID))){
					USBSendByte("savessiderr@", 8);
				}
				else{
					if(!save_MFC_TEST_DATA(MT->SSID_PW, sizeof(MT->SSID_PW))){
						USBSendByte("savepwerr@", 8);
					}
					else USBSendByte("#@", 2);
				}
			}
			ui8USBMode = USB_READY;
		}
		else{
			USBSendByte("!@", 2);
			ui8USBMode = USB_READY;
		}
	}
	else if(ui8USBMode == USB_SCANNER){
		if(ui8USBModeSub == USB_SCANNER_READY){			
			if(!memcmp("trigger@",pui8USBUpdataBuf,8)){
				ui8USBModeSub = USB_SCANNER_TRiGGER;
				SCAN_ON;
				USB_scanner_alloc_Len = SCANNER_RX_BUFFER_SIZE;
				module_test_flag = USB_module_test_timer_setup(0);				
				USBSendByte("#@", 2);
			}
			else if(!memcmp("getrdmd@",pui8USBUpdataBuf,8)){
				ui8USBModeSub = USB_SCANNER_GET_READING_MODE;
				SCAN_ON;
				USB_scanner_alloc_Len = scanner_send_cmd(get_rd_mode);
				module_test_flag = USB_module_test_timer_setup(4);
			}
			else if(!memcmp("scan@",pui8USBUpdataBuf,5)){
				ui8USBModeSub = USB_SCANNER_SCAN;
				SCAN_ON;
				USB_scanner_alloc_Len = SCANNER_RX_BUFFER_SIZE;
				module_test_flag = USB_module_test_timer_setup(4);
			}
			else{
				USBSendByte("!@", 2);
				ui8USBMode = USB_READY;
			}
		}
	}
//20191121 ADD
    else if(ui8USBMode == USB_FLASH){
        uint32_t write_len =  (USB_FLS_idx % 4096 == 0)? 4096 : USB_FLS_idx % 4096;

        uint32_t *CRC = (uint32_t*)pui8USBUpdataBuf;

        uint32_t i;
        for(i = 0 ; i < write_len / 4 ; i++){
            USB_FLS_CRC_cal ^= CRC[i];
        }


        UARTprintf("ui8USBMode: %d, add: %x, len: %d\n", ui8USBMode, USB_FLS_start_add + USB_FLS_idx, write_len );
        ExtFlsWrite(USB_FLS_start_add + USB_FLS_idx - write_len, pui8USBUpdataBuf, write_len);


        if(USB_FLS_idx == USB_FLS_total_len){
            ui8USBMode = USB_READY;

            if(USB_FLS_CRC_cal == USB_FLS_CRC){
            UARTprintf("USB write FLS complete: %d / %d\n", USB_FLS_idx, USB_FLS_total_len);
            UARTprintf("CRC: %x, %x\n", USB_FLS_CRC_cal, USB_FLS_CRC);
                USBSendByte("rs@", 3);
            }
            else{
                UARTprintf("USB write FLS CRC fail\n");
                UARTprintf("FxxK Error #2!!!!\n");
                UARTprintf("CRC: %x, %x\n", USB_FLS_CRC_cal, USB_FLS_CRC);
                USBSendByte("!@", 2);
            }
        }
        else if(USB_FLS_idx > USB_FLS_total_len){
            ui8USBMode = USB_READY;
            UARTprintf("USB write FLS length error: %d / %d\n", USB_FLS_idx, USB_FLS_total_len );

            UARTprintf("FxxK Error #3!!!!\n");
            USBSendByte("!@", 2);
        }
        else{
            USBSendByte("rs@", 3);
            return;     //skip free(pui8USBUpdataBuf) call
        }
    }
	else{
		USBSendByte("!@", 2);
		ui8USBMode = USB_READY;
	}
	//**************
	//Release Memory
	//**************
	//if(blMemAllocGet == false)
	//{
	free(pui8USBUpdataBuf);
	pui8USBUpdataBuf = NULL;
	//}	
	need_display = true;

}



void USB_routine(){
	if(!USBConnect())
	{
		ROM_SysCtlReset();
		while(1);
	}

	if(need_display){
	    need_display = false;
	    USB_DISPLAY();
	}

	if(pui8USBUpdataBuf == NULL)
	{
		//****************
		//Memory Allocate
		//****************
		ui32CMDLen = USBStorageAllocater(ui8USBMode, ui8USBModeSub);		
		pui8USBUpdataBuf = malloc(ui32CMDLen * sizeof(uint8_t));
		memset(pui8USBUpdataBuf, 0, ui32CMDLen);

	}
	if(USB_get_module_version_flg){
		if(USB_get_module_version_timer){
			unregister_timer(USB_get_module_version_timer); 
			USB_get_module_version_timer = NULL;
		}
	}
	if(module_test_flag){
		if(USB_module_test_timer){
			unregister_timer(USB_module_test_timer); 
			USB_module_test_timer = NULL;
		}
	}

	
	if(USB_widget.parameter){
		USB_CMD_PROCESS();
		USB_widget.parameter = 0;
	}

	if(ui8USBMode == USB_DETECT_VOLTAGE){
		float fVBuf;
		// 1st scan
		BattCoreScanVoltage(&fVBuf);

		if((fVBuf >= 6 && fVBuf <= 6.5) || (fVBuf >= 12 && fVBuf <= 13)){
			//	Delay and re-scan 
			delay_ms(1000);

			// 2nd scan
			BattCoreScanVoltage(&fVBuf);
			if(fVBuf >= 6 && fVBuf <= 6.5){
				USBSendByte("1@",2);
			}
			else if(fVBuf >= 12 && fVBuf <= 13){
				USBSendByte("3@",2);				
			}
			else return;
		}						
		else return;
	}
	else if(ui8USBMode == USB_WIFI){
		if(ui8USBModeSub == USB_WIFI_FUNCTION_CHECK_READY || ui8USBModeSub == USB_SSID_PW_WRITE) return;
		if(ui8USBModeSub == USB_WIFI_VER){
			switch(USB_get_module_version_flg){
			case 0:	return;
			case 1:
				USBSendByte(WIFI_VER, strlen((char*)WIFI_VER));
				USBSendByte("@",1);
				break;
			case 100:
				// version length issue
				USBSendByte("WverA0@", 7);
				break;
			case 101:
				// initial error
				USBSendByte("WverA1@", 7);
				break;
			
			case 105:
				// version check timeout
				USBSendByte("WverA5@", 7);
				break;
			}
		}
		else{
			switch(module_test_flag){
			case 0:	return;
			case 1:
				USBSendByte("#@", 2);
				break;
			case 100:
				// version length issue
				USBSendByte("WmdtA0@", 7);
				break;
			case 101:
				// data error
				USBSendByte("WmdtA1@", 7);
				break;
			case 105:
				// version check timeout
				USBSendByte("WmdtA5@", 7);
				break;
			}
		}
	}
	else if(ui8USBMode == USB_SCANNER){
		if(ui8USBModeSub == USB_SCANNER_READY) return;
		else if(ui8USBModeSub == USB_SCANNER_TRiGGER || ui8USBModeSub == USB_SCANNER_SCAN){
			if(module_test_flag == 0) return;
			if(module_test_flag == 1){
				SCANNER_BUZZ;
				USBSendByte(USB_SCANNER_RECEIVED, strlen((char*)USB_SCANNER_RECEIVED));
			}
			else if(module_test_flag == 105){
				USBSendByte("scannerTO@", 10);
			}
			else{
				USBSendByte("!@", 2);
			}
			SCAN_OFF;
		}
		else{// (ui8USBModeSub == USB_SCANNER_GET_READING_MODE)
			if(module_test_flag == 0) return;
			if(module_test_flag == 1){
				SCANNER_BUZZ;
				// 2020/02/19 scanner module replaced: MIR3+ phase-out
				// Copy MIR3+ module respnose for module coexistence
				// And no need to modify smartlab PC software

				// Check MIR3+(HW V00) response:
				//Function ID: 0xA6(get reading mode), param: 0x00(trigger on/off), checksum: 0x84
				//uint8_t rsp[] = {0x06, 0x02, 0x22, 0x01, 0xA6, 0x00, 0x03, 0x84. '@'};
				//Function ID: 0xA6(get reading mode), param: 0x01(good-read off), checksum: 0x85
				uint8_t rsp[] = {0x06, 0x02, 0x22, 0x01, 0xA6, 0x01, 0x03, 0x85, '@'};
				
				// Check N3680(HW V01) response:
				// TODO:
				// Just make sure scanner module Rx and TX working is fine.
				// For the further function upgrade by the command sequence.
				if(scanner_msg_to_string(get_rd_mode, USB_SCANNER_RECEIVED, sizeof(USB_SCANNER_RECEIVED))){
					USBSendByte(rsp, sizeof(rsp));
				}
				else{
					USBSendByte("!@", 2);
				}
			}
			else if(module_test_flag == 105){
				USBSendByte("scannerTO@", 10);
			}
			else{
				USBSendByte("!@", 2);
			}
			SCAN_OFF;
		}
	}
	else return;
	
	ui8USBMode = USB_READY;
	USB_DISPLAY();
}




