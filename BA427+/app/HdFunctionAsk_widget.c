//*****************************************************************************
//
// Source File: HdFunctionAsk_widget.c
// Description: ask is it Normal or HD battery
//
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 08/12/20     David.Wang     Created file.
//
//
// =============================================================================
//
//
// $Header:   $
// $DateTime: $
// $Author:   $
//
//
// Copyright (c) 2020 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#include "Widgetmgr.h"

void HdFunctionAsk_UP_DOWN();
void HdFunctionAsk_LEFT();
void HdFunctionAsk_RIGHT();
void HdFunctionAsk_display();
void HdFunctionAsk_SELECT();
void HdFunctionAsk_ROUTINE();
void HdFunctionAsk_Init();

widget* HdFunctionAsk_sub_W[] = {
        &BatterySel_widget,
        &HdFunctionSelect_widget,
};



widget HdFunctionAsk_widget = {
        .Key_UP = HdFunctionAsk_UP_DOWN,
        .Key_DOWN = HdFunctionAsk_UP_DOWN,
        .Key_RIGHT = HdFunctionAsk_RIGHT,
        .Key_LEFT = HdFunctionAsk_LEFT,
        .Key_SELECT = HdFunctionAsk_SELECT,
        .routine = HdFunctionAsk_ROUTINE,
        .service = 0,
        .initial = HdFunctionAsk_Init,
        .display = HdFunctionAsk_display,
        .subwidget = HdFunctionAsk_sub_W,
        .number_of_widget = sizeof(HdFunctionAsk_sub_W)/4,
        .index = 0,
        .name = "HdFunctionAsk",
};


void HdFunctionAsk_Init(){
    HdFunctionAsk_widget.index = 0;
}

void HdFunctionAsk_LEFT(){
    Widget_init(manual_test); // RETURN TO MainMenu
}

void HdFunctionAsk_RIGHT(){

}

void HdFunctionAsk_ROUTINE(){

}

void HdFunctionAsk_display(){
        LCD_display(Truck_Group_Test, HdFunctionAsk_widget.parameter^0x01);  //20200812 DW MASK. Original program definition

}

void HdFunctionAsk_UP_DOWN(){
    HdFunctionAsk_widget.parameter ^= 0x01;
    HdFunctionAsk_display();
}


void HdFunctionAsk_SELECT(){
    SYS_PAR* par = Get_system_parameter();        // 20200819 DW EDIT
    TEST_DATA* TD = Get_test_data();              // 20200819 DW eDIT


    if( HdFunctionAsk_widget.parameter ){
        Move_widget(HdFunctionAsk_sub_W[1],0);    // select battery
    }
    else{

        TD->PackTestNum = 1;                                                  // normal bat test flag for one bat test
        TD->PackTestCount = 0;                                                // pack test count register clear

        par->BAT_test_par.PackTestNum = TD->PackTestNum;                      // save to eeprom
        par->BAT_test_par.PackTestCount = TD->PackTestCount;                  // save to eeprom
        save_system_parameter(&par->BAT_test_par, sizeof(par->BAT_test_par)); // save to eeprom

        Move_widget(HdFunctionAsk_sub_W[0],0);                                // return to NORMAL BAT TEST

    }
}



