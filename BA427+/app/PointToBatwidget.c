//*****************************************************************************
//
// Source File: PointToBatwidget.c
// Description: Point to battery widget including Click(Push to new menu entry to stack)/
//				Right/Left(Pop out stack)/Up/Down function body.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 21/06/16     Vincent.T      Created file.
// 10/08/16     Vincent.T      1. #include "..\service/testrecordsvc.h"
//                             2. global variable WHICH_TEST to access test type.
//                             3. Modify PointToBatClick(); Get battery temperature before IR test.
// 01/13/17     Vincent.T      1. Modify PointToBatClick(); to skip this process when doing IR test.
// 01/19/17     Vincent.T      1. Modify PointToBatClick(); to skip sliding effect.
// 03/24/17     Vincent.T      1. Modify PointToBatClick(); to make sure march spec.
// 09/21/17 	Jerry.W		   Rewrite Widget for new software architecture
// 09/30/17 	Jerry.W		   1. edit P_counter cycle for animation speed adjust
// 09/11/18     Henry.Y        Add PointToBAT_service/PointToBAT_LEFT.
// 10/17/18     Henry.Y        IR test add point to battery flow.
// 12/07/18     Henry.Y        New display api.
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/app/PointToBatwidget.c#1 $
// $DateTime: 2017/11/27 10:38:22 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#include "Widgetmgr.h"

void PointToBAT_DISPLAY();
void PointToBAT_SELECT();

widget* PointToBAT_sub_W[] = {
		&CheckRipple_widget,
};

widget PointToBAT_widget = {
		.Key_UP = 0,
		.Key_DOWN = 0,
		.Key_RIGHT = 0,
		.Key_LEFT = 0,
		.Key_SELECT = PointToBAT_SELECT,
		.routine = 0,
		.service = 0,
		.display = PointToBAT_DISPLAY,
		.initial = 0,
		.subwidget = PointToBAT_sub_W,
		.number_of_widget = sizeof(PointToBAT_sub_W)/4,
		.index = 0,
		.name = "PointToBAT",
};

void PointToBAT_DISPLAY(){
    SYS_PAR* par = Get_system_parameter();

    TEMP_led_ON ;

    ST7565ClearScreen(BUFFER1);
    ST7565Refresh(BUFFER1);
    LCD_String_ext(16, get_Language_String(S_POINT_TO_BATTERY), Align_Central);
    LCD_String_ext(32, get_Language_String(S_PRESS_ENTER), Align_Central);
    LCD_Arrow(Arrow_enter);
}

void PointToBAT_SELECT(){
	TEST_DATA* TD = Get_test_data();
	GetTemp(&TD->BodyTestingTemp, &TD->ObjTestingTemp);

	TEMP_led_OFF ;

	Move_widget(PointToBAT_sub_W[0],0);
}


