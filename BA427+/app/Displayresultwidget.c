//*****************************************************************************
//
// Source File: Displayresultwidget.c
// Description: Display test results and press Enter to go to 'Print result' page.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 10/26/15     Henry.Y        Created file.
// 10/30/15     William.H      Modify comment of "Print Result" screen instead of "Test In Vehicle" screen.
// 02/15/16     William.H      Add RO / VIN virtual keyboard input method @ DisplayResultClick().
// 02/16/16     Vincent.T      1. Add RO/VIN reset routine
//                             2. global variables g_RoVkybdTextEditStr, g_VINVkybdTextEditStr
// 03/30/16     Vincent.T      1. Modify DisplayResultClick(); to handle mutination language case.
// 01/13/17     Vincent.T      1. global variables g_RoVinVKybdChar
//                             2. g_VINVkybdTextEditStr[8 -> 12]
//                             3. Modify DisplayResultClick();(Defult '0' -> 'OK' in RO#/VIN#)
// 03/27/17     Vincent.T      1. Modify DisplayResultClick(); to clear g_RoVkybdTextEditStr( 4 -> 8)
// 09/21/17 	Jerry.W		   Rewrite Widget for new software architecture
// 09/26/17 	Jerry.W		   move JudgetResltStrOnDisplay and SelRatingStrOnDisplay function from Widgetmgr.c into this file
// 03/23/18     Henry.Y        modify next widget to VIN input.
// 09/11/18     Henry.Y        Remove DisplayResult_UP/DisplayResult_DOWN. Add DisplayResult_LEFT.
// 10/22/18     Henry.Y        (__BO__)Change "good & recharge" backlight color to orange
// 12/07/18     Henry.Y        New display api.
// 08/11/20     David.Wang     1. add save record BATTERY_SARTSTOP_RECORD_T record
//                             2. Skip VIN CODE WIDGET TO PRINT WIDGET
// 10/16/20     David.Wang     Msak Regarge time message for BT2200_HD_BO
// 20201205     David Wang     modify scerrn to same as BT2010
//
// =============================================================================
//
//
// $Header:   $
// $DateTime: $
// $Author:   $
//
//
// Copyright (c) 2020 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#include "Widgetmgr.h"
enum{
    page0_result_v_rating = 0,
    page3_SOC,
    page4_SOH,
    page_num,
    page1_IR,
    page2_temperature,
};

static TYPE_TEXT_T result_type[] = {{TR_GOODnPASS, S_GOOD_n_PASS},
                                    {TR_GOODnRRCHARGE, S_GOOD_n_RECHARGE},
                                    {TR_RECHARGEnRETEST, S_RECHARGE_RETEST},
                                    {TR_BADnREPLACE, S_BAD_n_REPLACE},
                                    {TR_BAD_CELL_REPLACE, S_BADCELL_REPLACE}};

void DisplayResult_DISPLAY();
void DisplayResult_SELECT();
void DisplayResult_initail();
void DisplayResult_LEFT();
void DisplayResult_RIGHT();

widget* DisplayResult_sub_W[] = {
		&Test_Code_widget,
};

widget DisplayResult_widget = {
		.Key_UP = 0,
		.Key_DOWN = 0,
		.Key_RIGHT = DisplayResult_RIGHT,
		.Key_LEFT = DisplayResult_LEFT,
		.Key_SELECT = DisplayResult_SELECT,
		.routine = 0,
		.service = 0,
		.initial = DisplayResult_initail,
		.display = DisplayResult_DISPLAY,
		.subwidget = DisplayResult_sub_W,
		.number_of_widget = sizeof(DisplayResult_sub_W)/4,
		.index = 0,
		.name = "DisplayResult",
};
        
uint32_t get_result_str_idx(uint8_t result)
{
    uint8_t idx = 0;
    uint8_t i;
   
    uint8_t array_size = sizeof(result_type)/sizeof(result_type[0]);
    for(i = 0; i < array_size; i++){
        if(result_type[i].type == result){
            idx = i;
            break;
        }
    }
    return result_type[idx].text_index;
}

void DisplayResult_initail()
{
    SYS_PAR* par = Get_system_parameter();
    SYS_INFO* info = Get_system_info();
	TEST_DATA* TD = Get_test_data();
	
    uint8_t Rating = (par->WHICH_TEST == Start_Stop_Test)? par->SS_test_par.SSSelectRating : par->BAT_test_par.SelectRating;
    if(Rating == JIS){
        Rating = get_JIS_model_rating(par->BAT_test_par.BTJIS_RgBatSelect);
    }
    
    // BA427 encode version: same as BT2010_2200
    // WARNING/TODO: no time to revert older BA427 assembly coding! use BT2010_2200 encode rules!
    //voltage/set CCA/test CCA/ test result
    TESTCODE_T data = { .fvol = TD->battery_test_voltage,
                        .ui8TestResult = TD->Judge_Result, 
                        .ui8CCARating = Rating,
                        .ui16SetCCA = TD->SetCapacityVal,
                        .ui16TestCCA = (uint16_t)TD->Measured_CCA,
                        .fTemp = TD->ObjTestingTemp,
                        .ui16Year = TD->year,
                        .ui8Month =TD->month, 
                        .ui8Day = TD->date,
                        .hour = TD->hour,
                        .minute = TD->min};
    TestCodeGenerater(TD->TestCode, sizeof(TD->TestCode), &data);
}

void DisplayResult_LEFT(){
    DisplayResult_widget.parameter = (!DisplayResult_widget.parameter)? page_num-1:DisplayResult_widget.parameter-1;
    DisplayResult_DISPLAY();
}


void DisplayResult_RIGHT(){
    DisplayResult_widget.parameter = (DisplayResult_widget.parameter+1)%page_num;
    DisplayResult_DISPLAY();
}


void DisplayResult_DISPLAY()
{
    TEST_DATA* TD = Get_test_data();
    SYS_PAR* par = Get_system_parameter();
    char buf[20];
    
    uint8_t rating = par->BAT_test_par.SelectRating;
    if(rating == JIS){
        rating = get_JIS_model_rating(par->BAT_test_par.BTJIS_RgBatSelect);
    }
    
    ST7565ClearScreen(BUFFER1);
    ST7565Refresh(BUFFER1);
    switch(DisplayResult_widget.parameter){
    case page0_result_v_rating:
        LCD_String_ext( 16, get_Language_String(get_result_str_idx(TD->Judge_Result)), Align_Central );
        sprintf( buf, "%.2fV", TD->battery_test_voltage );
        sprintf( buf+strlen(buf), "%*d%s", 15-4-strlen(buf),(uint32_t)TD->Measured_CCA, get_Language_String(get_rating_str_idx(rating)));
        LCD_String_ext(32, buf, Align_Central);
        break;
    case page1_IR:
        LCD_String_ext( 16, get_Language_String(S_IR), Align_Central );
        if(TD->IR > IR_USER_MODE){
            sprintf(buf, "--.--m%s",DISPLAY_OHM);
        }
        else{
            sprintf(buf, "%5.2fm%s",TD->IR,DISPLAY_OHM);
        }
        LCD_String_ext(32, buf, Align_Central);
        break;
    case page2_temperature:{
        float ctemp = TD->ObjTestingTemp;
        float ftemp = (ctemp*9)/5+32;
        LCD_String_ext( 16, get_Language_String(S_TEMPERTURE), Align_Central );
        if(ctemp > TEMP_HIGH){
            ctemp = TEMP_HIGH;
            ftemp = (ctemp*9)/5+32;
            sprintf(buf, "%s %3d°F/%3d°C",get_Language_String(S_OVER), (uint16_t)ftemp, (uint16_t)ctemp);
        }
        else if(ctemp < TEMP_LOW){
            ctemp = TEMP_LOW;
            ftemp = (ctemp*9)/5+32;
            sprintf(buf, "%s %d°F/%d°C",get_Language_String(S_UNDER), (int16_t)ftemp, (int16_t)ctemp);
        }
        else{
            sprintf(buf, "%3d°F / %3d°C",(int16_t)ftemp, (int16_t)ctemp);
        }
        char *degree = "°";
        char *temp;
        temp = strstr(buf, degree);
        sprintf(temp, "~%s", temp+strlen(degree));
        temp = strstr(temp+1, degree);
        sprintf(temp, "~%s", temp+strlen(degree));
        LCD_String_ext(32, buf, Align_Central);
        break;
    }
    case page3_SOC:
        sprintf( buf, "SOC:%5.2fV", TD->battery_test_voltage );
        sprintf( buf+strlen(buf), "%*d%%", 15-strlen(buf) , (uint32_t)TD->SOC );
        LCD_String_ext(16, buf, Align_Central);
        LCD_SOC_SOH(TD->SOC);
        break;
    case page4_SOH:
        sprintf( buf, "SOH:%4d%s", (uint32_t)TD->Measured_CCA,get_Language_String(get_rating_str_idx(rating)));
        sprintf( buf+strlen(buf), "%*d%%", 15-strlen(buf) , (uint32_t)TD->SOH );
        LCD_String_ext(16, buf, Align_Central);
        LCD_SOC_SOH(TD->SOH);
        break;
    }
    LCD_Arrow(Arrow_enter | Arrow_RL);
}


void DisplayResult_SELECT(){
    Move_widget(DisplayResult_sub_W[0],0);
}


#if 0
//BT2010_2200
void DisplayResult_LEFT(){
    if(DisplayResult_widget.index >= 2) DisplayResult_widget.index = 0;
    else DisplayResult_widget.index++;
    DisplayResult_DISPLAY();
}


void DisplayResult_RIGHT(){
    if(DisplayResult_widget.index <= 0) DisplayResult_widget.index = 2;
    else DisplayResult_widget.index--;
    DisplayResult_DISPLAY();
}


void DisplayResult_initail(){ // prepare the string for display // generate test code

    SYS_PAR* par = Get_system_parameter();
    SYS_INFO* info = Get_system_info();

    uint8_t l = strlen((const char*)strFWver);
	TEST_DATA* TD = Get_test_data();

	DisplayResult_widget.index = 0;

	if(par->WHICH_TEST == IR_Test) return;
	
    uint8_t Rating = (par->WHICH_TEST == Start_Stop_Test)? par->SS_test_par.SSSelectRating : par->BAT_test_par.SelectRating;

	if(Rating == JIS){
	    if(pg_JIS[TD->JISCapSelect].itemNum>60 && pg_JIS[TD->JISCapSelect].itemNum<69){
	        Rating = DIN;
	    }
	    else{
	        Rating = SAE;
	    }
	}
	TestCodeGenerater(TD->TestCode, TD->battery_test_voltage, TD->Judge_Result, Rating, TD->SetCapacityVal,
						(uint16_t)TD->Measured_CCA, TD->ObjTestingTemp, TD->year, TD->month, TD->date, TD->hour, TD->min);



}

void DisplayResult_DISPLAY(){

    TEST_DATA* TD = Get_test_data();
	char* TR;
	char buf[20];
    // Clear screen
    ST7565ClearScreen(BUFFER1);
    ST7565Refresh(BUFFER1);

	//******** Page1 *****************************
    if(DisplayResult_widget.index==0){
        // Battery test result show
        switch (TD->Judge_Result){
        case TR_GOODnRRCHARGE:
            TR = get_Language_String(S_GOOD_n_RECHARGE);
	        break;
        case TR_BADnREPLACE:
            TR = get_Language_String(S_BAD_n_REPLACE);
            break;
        case TR_RECHARGEnRETEST:
            TR = get_Language_String(S_RECHARGE_RETEST);
            break;
        case TR_BAD_CELL_REPLACE:
            TR = get_Language_String(S_BADCELL_REPLACE);
            break;
	    case TR_GOODnPASS:
	    default:
	        TR = get_Language_String(S_GOOD_n_PASS);
	        break;
        }
	    LCD_String_ext( 16, TR, Align_Central );

	// Bat vol + test cca
    sprintf( buf, "%.2fV", TD->battery_test_voltage );
    sprintf( buf+strlen(buf), "%*dCCA", 15-4-strlen(buf) , (uint32_t)TD->Measured_CCA );
    LCD_String_ext(32, buf, Align_Central);
    }
    //****** Page2 *****************************
    if(DisplayResult_widget.index==1){
    // BAT CCA + SOH
    sprintf( buf, "SOH:%4dCCA", (uint32_t)TD->Measured_CCA );
    sprintf( buf+strlen(buf), "%*d%%", 15-strlen(buf) , (uint32_t)TD->SOH );
    LCD_String_ext(16, buf, Align_Central);
    // SOH percentage
    LCD_SOC_SOH(TD->SOH);
    }
	//****** Page3 *****************************
    if(DisplayResult_widget.index==2){
        // BAT VOL + SOC
    sprintf( buf, "SOC:%5.2fV", TD->battery_test_voltage );
    sprintf( buf+strlen(buf), "%*d%%", 15-strlen(buf) , (uint32_t)TD->SOC );
    LCD_String_ext(16, buf, Align_Central);
    // SOC percentage
    LCD_SOC_SOH(TD->SOC);
    }
    LCD_Arrow(Arrow_enter | Arrow_RL);
}


void DisplayResult_SELECT(){
    SYS_INFO* info = Get_system_info();
    if(info->pre_test_state == 2){
        info->pre_test_state = 0;
        save_system_info(&info->pre_test_state, sizeof(info->pre_test_state));
    }
	Move_widget(DisplayResult_sub_W[1],0);

}
#endif





