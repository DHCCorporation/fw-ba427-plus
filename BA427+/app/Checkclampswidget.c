//*****************************************************************************
//
// Source File: Checkclampswidget.c
// Description: Check Clamps and pop up the warning message if clamps are not ready.
//              Press Enter to exit the popup message and turn back to Main Menu.
//
//
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 10/22/15     Henry.Y	       Created file.
// 04/06/16     Vincent.T      1. Modify CheckClampsClick(); to handle multination language case.
// 09/26/17 	Jerry.W		   Rewrite Widget for new software architecture
// 09/11/18     Henry.Y        Use system error to replace global variables.
// 12/07/18     Henry.Y        New display api.
// 20201205     D.W            Modify screen to same as BT2010
//
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/app/Checkclampswidget.c#6 $
// $DateTime: 2017/09/26 17:08:19 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#include "Widgetmgr.h"


void CheckClamps_SELECT();
void CheckClamps_display();
void CheckClamps_routine();


widget* CheckClamps_sub_W[] = {
};

widget CheckClamps_widget = {
		.Key_UP = 0,
		.Key_DOWN = 0,
		.Key_RIGHT = 0,
		.Key_LEFT = 0,
		.Key_SELECT = CheckClamps_SELECT,
		.routine = CheckClamps_routine,
		.service = 0,
		.initial = 0,
		.display = CheckClamps_display,
		.subwidget = CheckClamps_sub_W,
		.number_of_widget = sizeof(CheckClamps_sub_W)/4,
		.index = 0,
		.no_messgae = 1,
		.name = "CheckClamps",
};


void CheckClamps_SELECT(){
	Widget_init(manual_test);
}

void CheckClamps_display(){
	//LCD_display(CHECK_CLAMP,0);
    // Clear screen
    ST7565ClearScreen(BUFFER1);
    ST7565Refresh(BUFFER1);
    // Write SCREEN
    LCD_String_ext(16,get_Language_String(S_CHECK_CLAMPS) , Align_Central);
}

void CheckClamps_routine(){
	if(!(Clamp_err & get_system_error())){

		Return_pre_widget();
	}
}


