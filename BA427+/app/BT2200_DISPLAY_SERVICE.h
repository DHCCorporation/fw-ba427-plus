//*****************************************************************************
//
// Source File: BT2200_DISPLAY_SERVICE.h
// Description: New display api for BT2200 application layer.
//
//
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 11/30/18     Henry.Y        Create file.
// 08/03/20     David.Wang     Some pictures of sys test changed to text description
// 08/06/20     David.Wang     Add start ask voltage high check screen
// 08/26/20     David.Wang     Add advised action widget show words
// 10/20/20     David Wang      Diesel engine flow add new screen
// =============================================================================
//
//
// $Header$
// $DateTime$
// $Author$
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#ifndef APP_BT2200_DISPLAY_SERVICE_H_
#define APP_BT2200_DISPLAY_SERVICE_H_

#include "../service/lcdsvc.h"
#include "../service/UTF8_WBsvc.h"
#include "../service/SystemInfosvs.h"
#include "../service/testrecordsvc.h"
#include "../service/langaugesvc.h"
#include "Widgetmgr.h"

typedef enum{
	LOGO = 0,
	MAIN,	
	SCANNER,	
	SCANNER_SCANNING,
	SCANNER_RESULT,
	SCANNER_INVALID,
	SCANNER_INVALID_SCANNER_MODE,
	SS_BATTERY_TYPE,
	BT_BATTERY_TYPE,
	SELECT_RATING,
	SET_CAPACITY,
	JIS_CAPACITY,
	JUMP_START_POST,
	CAR_BRAND,
	CAR_MODEL,
	CAR_VERSION,
	TESTING_PROCESSING_ARROW,
	POINT_TO_BATTERY,
	VOLTAGE_UNSTABLE,
	SURFACE_CHARGE,
	BT_SS_TEST_RESULT,
	IR_TEST_RESULT,
	INSERT_VIN_MANUALLY,
	ABORT_VIN_INPUT,
	VIN_INPUT_VIRTUAL_KEYBOARD,
	INVALID_VIN,
	TEST_CODE,
	PRINT_RESULT,
	PRINT_SUCCESSFULLY,
	PRINTING,
	DATE_TIME_SET,
	SEC_SET,
	VIRTUAL_KB_ABC,
	VIRTUAL_KB_abc,
	VIRTUAL_KB_123,
	VIRTUAL_KB_SYM,
	SYS_TURN_OFF_LOAD,
	SYS_SWITCH_OFF,
	SYS_SWITCH_ON,
	SYS_CRANKING,
	SYS_CRANKING_RESULT,
	SYS_CRANKING_LOWEST_RESULT,
	SYS_IDLE_TEST_RESULT,
	TURN_ON_LOAD,
	RIPPLE_TEST,
	RIPPLE_TEST_RESULT,
	SYS_LOAD_TEST,
	SYS_LOAD_TEST_RESULT,
	SYS_TEST_RESULT,
	CONNECT_TO_BAT,
	BATTERY_IN_VEHICLE,
	TEST_IN_VEHICLE,
	IS_6V_BATTERY,
	IS_BATTERY_CHARGED,
	VM_AM,
	SETTING,
	LANGUAGE_SELECT,
	VERSION,
	BACKLIGHT_SETTING,
	MEMORY_ERASE,
	TIME_ZONE,
	TEST_COUNTER,
	TEST_COUNTER_PRINT,
	TEST_COUNTER_ERASE,
	ABORT,
	VOLTAGE_HIGH,
	CHECK_CLAMP,
	LOAD_ERROR,
	NO_PAPER,
	WARNING_CLAMPS_ON,
	REPLACE_INTERNAL_BATTERY,
	OTA_UPGRADE,
	OTA_UPGRADE_CANCEL,
	OTA_CHECK_VERSION,
	OTA_UPGRADING,
	OTA_DOWNLOADING,
	WIFI_SETTING,
	WIFI_POWER,
	WIFI_LIST,
	WIFI_CHECK_CONNECTION,
	WIFI_CONNECTING,
	WIFI_CONNECTION_LIMIT,
	WIFI_CONNECTION_CONNECT,
	WIFI_CONNECTION_DISCONNECT,
	WIFI_REFRESH_LIST,
	USB,
	REGISTRATION,
	SCAN_QR_CODE,
	REGISTERING,
	SYS_START_ENGIN,               //20200803 DW EDIT
	SYS_MAKE_SOUR_ALL,             //20200803 DW EDIT
	SYS_LOADS_ARE_OFF,             //20200803 DW EDIT
	SYS_TURN_ON_LO,                //20200803 DW EDIT
	SYS_AND_PRESS_ENT,             //20200803 DW EDIT
	SYS_RUN_ENG_2500,              //20200803 DW EDIT
	AMVM_TURN_ON,                  //20200803 DW EDIT
	AMVM_TURN_ON_AMP,              //20200803 DW EDIT
	AMVM_AFTER_ATT,                //20200803 DW EDIT
	AMVM_ENTER_PROCEED,            //20200803 DW EDIT
	AMVM_TURN_OFF_AMP,             //20200803 DW EDIT
	AMVM_AMP_METER,                //20200803 DW EDIT
	IS_24V_SYSTEM,                 //20200806 DW EDIT
	TEST_EACH_BAT_SEP,             //20200806 DW EDIT
	CONNECT_TO_12V_BAT,            //20200806 DW EDIT
	IS_DIESEL_ENGINE,              //20200807 DW EDIT
	REV_ENGINE_FOR,                //20200807 DW EDIT
	Truck_Group_Test,              //20200813 DW EDIT
    Of_Bat_In_Par,                 //20200814 DW EDIT
    SEP_PACK_CONNECT_BAT1,         //20200817 DW EDIT
    SEP_PACK_CONNECT_BAT2,         //20200817 DW EDIT
    SEP_PACK_CONNECT_BAT3,         //20200817 DW EDIT
    SEP_PACK_CONNECT_BAT4,         //20200817 DW EDIT
    SEP_PACK_CONNECT_BAT5,         //20200817 DW EDIT
    SEP_PACK_CONNECT_BAT6,         //20200817 DW EDIT
    GR_TEST_BAT1_START,            //20200818 DW EDIT
    GR_TEST_BAT2_START,            //20200818 DW EDIT
    GR_TEST_BAT3_START,            //20200818 DW EDIT
    GR_TEST_BAT4_START,            //20200818 DW EDIT
    GR_TEST_BAT5_START,            //20200818 DW EDIT
    GR_TEST_BAT6_START,            //20200818 DW EDIT
    Pack_Report,                   //20200819 DW EDIT
    BAT_NUM,                       //20200819 DW EDIT
    BT_PACK_TEST_RESULT,           //20200820 DW EDIT
    ADVISED_ACTION,                //20200826 DW EDIT    Add advised action widget show words
    CHECK_CONNECT,                 //20200826 DW EDIT    Add advised action widget show words
    PRESS_ENTER,                   //20200826 DW EDIT    Add advised action widget show words
    Please_Enter_Start,            //20201020 DW edit    Diesel engine flow add new screen

}BT2200_LCD_t;

typedef enum{
	P_LOGO = 0,
	P_TEST_REPORT,
	P_TEST_TYPE,
	P_TEST_TYPE_BT,
	P_TEST_TYPE_SS,
	P_TEST_TYPE_SYS,
	P_TEST_TYPE_IR,
	
	P_TEST_RESULT,
	P_BATTERY_TYPE,
	P_VOLTAGE,
	P_RATED,
	P_MEASURED_CCA,
	P_MEASURED_IR,
	P_TEMPERATURE,
	P_STATE_OF_CHARGE,
	P_STATE_OF_HEALTH,
	P_TEST_CODE,
	P_BARCODE,
	P_VIN,
	P_NOTE,
	P_TEST_DATE,
	P_INFORMATION,
	P_SYS_STARTER,
	P_CHARGING_TEST,
	P_DIODE_TEST,
	P_TEST_COUNTER,
	P_SS_TEST_COUNTER,
	P_BAT_TEST_COUNTER,
	P_SYS_TEST_COUNTER,
	P_IR_TEST_COUNTER,
	P_LOAD_ON,	
	P_LOAD_OFF,
	P_HIGH,	
	P_LOW,
	P_NORMAL,	
	P_NO_DETECT,
	P_Warning_word,        // 20200821 DW EDIT
	P_Warning_word1,       // 20200824 DW EDIT
	P_Warning_word2,       // 20200824 DW EDIT
	P_Warning_word3,       // 20200824 DW EDIT
	P_BATTERY_NUM,         // 20200824 DW EDIT

}BT2200_PRINT_t;


typedef enum{
	FULL = 0,
	HALF
}FULL_HALF_PAGE;

typedef enum{
	LEFT_DOWN_ENT_UP = 2,
	LEFT_DOWN_ENT_UP_PAGE,
	LEFT_ENT,
	DOWM_ENT_UP_PAGE,
	DOWN_ENT_UP,
	LEFT,
	ENT,
	NONE,	
	LEFT_DOWN_ENT_UP_RIG_KB,
	LEFT_DOWN_ENT_UP_RIG_KB_SQUARE,
}BACKGROUNG_SET_T;

typedef struct
{
	const char* string;
	LANGUAGE	lan;
}Lang_abstract_t;

void LCDDrawCrankingVolt(SYSRANK CrankVolRank, uint8_t ui8Col);
void LCDDrawRipVolt(SYSRANK RipVRank, uint8_t ui8Col);

uint8_t DrawPageInfo(uint8_t Sel, uint8_t total_item_num, uint8_t num_per_page);
void Lcd_selection_antiwhite(tRectangle sTemp);
/*************************************************************************************************
 * @ Name		: LCD_display
 * @ Brief 		: tester screen display by designated flow.
 * @ Parameter 	: flow -- designated flow.
 * 				  selection -- selected option.
 * @ Return		: selected option coordinated, refer to tRectangle.
 *************************************************************************************************/
tRectangle LCD_display(BT2200_LCD_t flow, uint8_t selection);
tRectangle LCD_draw_string(BT2200_LCD_t flow, uint8_t selection, uint8_t y);

void Print_String_by_language(BT2200_PRINT_t item, uint8_t parameter);
LANGUAGE get_select_language(uint8_t parameter);
void show_print_battery_voltage(uint8_t y, float vol ,bool on_off);


#endif /* APP_BT2200_DISPLAY_SERVICE_H_ */
