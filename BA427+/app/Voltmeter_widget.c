/*
 * Voltmeter_widget.c
 *
 *  Created on: 2020撟�12���29�
 *      Author: ray.yuan
 */

#include "Widgetmgr.h"

#define UPDATE_SHOW_PERIOD 10//ms
#define OVER_RANGE_VOLT 40//V

static uint32_t DelayCounter = 0;

void Voltmeter_DISPLAY();
void Voltmeter_SELECT();
void Voltmeter_ROUTE();
void Voltmeter_Init();

widget* Voltmeter_sub_W[] = {

};

widget Voltmeter_widget = {
        .Key_UP = 0,
        .Key_DOWN = 0,
        .Key_RIGHT = 0,
        .Key_LEFT = 0,
        .Key_SELECT = Voltmeter_SELECT,
        .routine = Voltmeter_ROUTE,
        .service = 0,
        .initial = Voltmeter_Init,
        .display = Voltmeter_DISPLAY,
        .subwidget = Voltmeter_sub_W,
        .number_of_widget = sizeof(Voltmeter_sub_W)/4,
        .index = 0,
        .no_messgae = 1,
        .name = "Voltmeter",
};

static void Voltmeter_showvolt(){
    char buf[20];
    float fVMAM;
    uint32_t ADC;
    GetVM(&fVMAM,&ADC);

    // Reset FOR ZERO
    if(fVMAM < 0.50) fVMAM = 0;

    if(fVMAM>OVER_RANGE_VOLT){
        if(Voltmeter_widget.index==0){
            Voltmeter_widget.index = 1;
            Voltmeter_DISPLAY();
        }
    }
    else{
        if(Voltmeter_widget.index==1){
            Voltmeter_widget.index = 0;
            Voltmeter_DISPLAY();
        }
        sprintf(buf, "%5.2fV", fVMAM);
        LCD_String_ext(32,buf , Align_Central);


    }






}

void Voltmeter_Init(){
    Voltmeter_widget.index = 0;
}

void Voltmeter_DISPLAY(){
    // Clear screen
    ST7565ClearScreen(BUFFER1);
    ST7565Refresh(BUFFER1);
    if(Voltmeter_widget.index==1){
        LCD_String_ext(16,get_Language_String(S_OVER_RANGE) , Align_Central);
    }
    else{
        // Write SCREEN
        LCD_String_ext(16,get_Language_String(S_VOLTMETER) , Align_Central);

    }
    LCD_Arrow(Arrow_enter);


}
void Voltmeter_SELECT(){
    Return_pre_widget();
}
void Voltmeter_ROUTE(){
    if(DelayCounter == 0){
        DelayCounter = UPDATE_SHOW_PERIOD;
        Voltmeter_showvolt();
    }
    else{
        DelayCounter--;
    }


}

