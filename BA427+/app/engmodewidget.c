//*****************************************************************************
//
// Source File: engmodewidget.c
// Description: engineering mode
//
//
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 01/23/18     Henry.Y        Create file.
// 02/14/18     Henry.Y        engmode_DISPLAY() add flash version.
// 03/23/18 	Jerry.W		   add info->LCDcontrast saving.
// 10/22/18     Henry.Y        Add cloud account display and AM -5 switch on/off.
// 11/19/18     Henry.Y        Add internal voltage calibration and wifi related operation.
// 11/28/18     Henry.Y        Modify engmode WIFI data upload flow.
// 03/20/19     Henry.Y        Add new LCM contrast parameter adjustment(match HW bias1/9, \5x booster).
// 02/24/20     Henry.Y        Scanner control modification.
// =============================================================================
//
//
// $Header$
// $DateTime$
// $Author$
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#include "Widgetmgr.h"

Timer_t* get_module_version_timer;
volatile uint8_t get_module_version_counter;


uint8_t g_bENGmode = false;

void engmode_DOWN();
void engmode_UP();
void engmode_RIGHT();
void engmode_LEFT();
void engmode_SELECT();
void engmode_service();
void engmode_initail();
void engmode_DISPLAY();
void engmode_routine();
void engmode_initial();
void EngmodeAntiWhiteItem(uint8_t menuIdx);
void DigitAntiWhiteItem(uint8_t h_L, uint8_t digit);


typedef enum {
	FW = 100,
	LIST,
	CONTRAST = 0,
	TEMP,
	BACKLIGHT,
	PRINTER,	
	VOLTAGE_K,
	IR_CCA,
	BARCODE,
	ENG_WIFI,
	ENGMODE_LIST_NUM
}eng_mode;

typedef enum {
	OFF = 0,
	GET_VER,
	GET_M,
	SET_M,
	SCAN,
	sub_scanner_mode_num
}sub_scanner_mode;

typedef enum{	
	IR_6V = 0,
	CCA_6V,
	IR_12V,	
	CCA_12V
}sub_IR_CCA_calibration;

typedef enum{	
	V_6V = 0,
	V_12V,
	V_24V,
	V_VM,
	V_AM,
	V_INT,
	BACK,
	sub_VOLTAGE_calibration_num	
}sub_VOLTAGE_calibration;

typedef enum{	
	connect_n_upload = 0,
	connect_n_upload_dummy,
	smart_config,
	WIFI_BACK,
	sub_WIFI_function_num	
}sub_WIFI_function;

//
// Display string
//
//const XY_DISPLAY list_voltage_6V = {"<1. 6V/LOAD_M VOL.>", 0,2};
//const XY_DISPLAY list_voltage_12V = {"<2. 12V/LOAD_P VOL.>", 0,11};
//const XY_DISPLAY list_voltage_24V = {"<3. 24V VOLTAGE>", 0,20};
//const XY_DISPLAY list_voltage_VM = {"<4. VM>", 0,29};
//const XY_DISPLAY list_voltage_AM = {"<5. AM>", 0,38};
//const XY_DISPLAY list_voltage_INT = {"<6. INTERNAL VOLT.>", 0,47};
//const XY_DISPLAY list_voltage_back = {"<<<BACK", 0,56};

//const XY_DISPLAY list_LCDCONTRAST = {"<1. LCD CONTRAST>", 0,2}; const XY_DISPLAY list_ENG_WIFI = {"<8. WIFI>", 0,2};
//const XY_DISPLAY list_TEMPERATURE = {"<2. TEMPERATURE>", 0,11};
//const XY_DISPLAY list_BACKLIGHT = {"<3. BACKLIGHT>", 0,20};
//const XY_DISPLAY list_PRINTER = {"<4. PRINTER>", 0,29};
//const XY_DISPLAY list_VOLTAGE = {"<5. VOLTAGE CALI.>", 0,38};
//const XY_DISPLAY list_IRCCA = {"<6. IR/CCA CALI.>", 0,47};
//const XY_DISPLAY list_BARCODE = {"<7. BARCODE SETTING>", 0,56};

//const XY_DISPLAY strLCDContrast = 				{"< LCD Contrast >",0,31};

//const XY_DISPLAY strscanner_eng_title = 		{"<BARCODE ENGMODE>",0,0};
//const XY_DISPLAY strscanner_get_rd_mode = 		{"< GET READ MODE >",0,16};
//const XY_DISPLAY strscanner_set_rd_mode = 		{"< SET G-R MODE >",0,16};
//const XY_DISPLAY strscanner_tigger = 			{"< TRIGGER >",0,16};
//const XY_DISPLAY strscanner_OFF = 				{"< OFF >",0,16};
//const XY_DISPLAY strscanner_scan =	 			{"< SCAN >",0,16};
//const XY_DISPLAY strscanner_UL = 				{"-------------------------",0,32};
//const XY_DISPLAY strscanner_setting = 			{"SETTING...",0,40};
//const XY_DISPLAY module_version_getting = 		{"GET WIFI VERSION...",0,54};

//const XY_DISPLAY list_WIFI_connect_n_upload =		{"1.CONNECT&UPLOAD LAST",0,2};
//const XY_DISPLAY list_WIFI_connect_n_upload_dummy = {"2.CONNECT&UPLOAD DUMY",0,11};
//const XY_DISPLAY list_WIFI_smart_config = 			{"3.SMART CONFIG",0,20};
//const XY_DISPLAY list_WIFI_back = 					{"<<<BACK",0,29};

//
// variables - flow
//
static eng_mode engmode_mode_list = FW;
static uint8_t voltage_L_H = 0;//L:0 H:1 V:99
static uint8_t digit_loc = 0;
static uint8_t option_or_operation = 0;//option select: 0, operation:1
static uint8_t sub_IRCCA_mode = 0;
static uint8_t sub_voltage_mode = 0;
static uint8_t get_module_version = 0;
static uint32_t g_ui32buf;
static float g_fbuf;

//
// variables - wifi
//
static WIFI_CONN_T ENG_WIFI_STATUS = WIFI_disconnect;
static uint8_t sub_WIFI_mode = connect_n_upload;
static uint8_t ENG_WIFI_STATUS_UPDATE = 0;//0: NO
static SOC_STATE_t ENG_TCP_STAUTS;
static char ESP8266_FW_1[25] = {0x00};
//static char ESP8266_FW_2[25] = {0x00};
static uint8_t ENG_CNT_SSID[SSID_LEN] = {0x00};

//
// variables - scanner
//
static uint8_t ENG_SCANNER_RECEIVED[40] = {0};
static uint8_t eng_SC_alloc_Len = 0;
static bool eng_bSCANNERecompl = false;

void ENG_SCANNER_CB(bool flush, uint8_t data){
	 uint8_t ui8buf = 0;
	 static uint8_t RX_index = 0;
	 static bool bCR = false, bNL = false;
	 static uint8_t UART0_RX_buffer[40] = {0};
	 
	if(flush){
		RX_index = 0;
		bCR = false;
		bNL = false;
	}
	else{
		if(RX_index != eng_SC_alloc_Len){			
			ui8buf = data;//(unsigned char)ROM_UARTCharGetNonBlocking(SCANNER_UART_BASE);
			UART0_RX_buffer[RX_index] = ui8buf;
			RX_index++;
			
			if(ui8buf == 0x0D) bCR = true;
			if(ui8buf == 0x0A) bNL = true;
			if(bCR && bNL){
			
				memcpy(ENG_SCANNER_RECEIVED , UART0_RX_buffer , sizeof(ENG_SCANNER_RECEIVED));
				ENG_SCANNER_RECEIVED[RX_index - 2] = '\0';
				memset(UART0_RX_buffer, 0x20, sizeof(UART0_RX_buffer));
				eng_bSCANNERecompl = true;
				RX_index = 0;
				bCR = false;
				bNL = false;
			}
			else if(RX_index == eng_SC_alloc_Len){
				memcpy(ENG_SCANNER_RECEIVED , UART0_RX_buffer , sizeof(ENG_SCANNER_RECEIVED));
				memset(UART0_RX_buffer, 0x20, sizeof(UART0_RX_buffer));
				eng_bSCANNERecompl = true;
				RX_index = 0;
				bCR = false;
				bNL = false;
			}
		}
	}
 }

void version_timer_cb(void){
	if(get_module_version_timer){
		if(get_module_version_counter) get_module_version_counter--;
		else get_module_version = 1;
		UARTprintf("version_timer_cb, counter = %d\n\r", get_module_version_counter);
	}
}

void GET_WIFI_version(uint8_t*ver){
	get_module_version = 1;

	//uint8_t *FW1 = ver;
	char *temp = strstr((char*)ver,"&");
	if(temp == NULL) {
		if(ver == NULL){
		UARTprintf("error");
		snprintf(ESP8266_FW_1,sizeof(ESP8266_FW_1),"WIFI VERSION_1 FAIL");
			//snprintf(ESP8266_FW_2,sizeof(ESP8266_FW_2),"WIFI VERSION_2 FAIL");
		return;
	}
		else{
			memcpy(ESP8266_FW_1,ver,strlen((char*)ver));
			//snprintf(ESP8266_FW_2,sizeof(ESP8266_FW_2),"WIFI VERSION_2 NULL");
		}
	}
	else{
	memcpy(ESP8266_FW_1,ver,(strlen((char*)ver)-strlen(temp)));
	*temp = '\0';
		//char *FW2 = temp+1;
		//memcpy(ESP8266_FW_2,FW2,strlen(FW2));
	}
	//UARTprintf("WIFI Module FW1: %s\n", FW1);
	//UARTprintf("WIFI Module FW2: %s\n", FW2);
}

void ENG_try_ev_cb(void* reserve, uint8_t status);

Socket_t ENG_try_soc = {
		.port = 80,
		.pair = "www.google.com",
		.evn_cb = ENG_try_ev_cb,
		.rcv_cb = 0,
};

void ENG_try_ev_cb(void* reserve, uint8_t status){
	SYS_INFO* info = Get_system_info();
	UARTprintf("ev_cb : %d\n", status);	
	ENG_WIFI_STATUS_UPDATE = 1;

	if(status == Soc_TCP_connected){
		ENG_TCP_STAUTS = Soc_TCP_connected;
		TCP_disconnect(&ENG_try_soc);
	
		Time_adjust(info->Time_zone);
	}
	else{
		ENG_TCP_STAUTS = Soc_TCP_disconnected;
	}
}


void ENG_wifi_service_cb(uint32_t type){
	UARTprintf("\n\n######################\n");
	ENG_WIFI_STATUS_UPDATE = 1;
	ENG_WIFI_STATUS = (WIFI_CONN_T)type;
	
	switch(type){
	case WIFI_connected                    :
		UARTprintf("WIFI_connected\n");
		if(wifi_get_connected_AP(ENG_CNT_SSID, SSID_LEN)){
			if(engmode_mode_list == ENG_WIFI && option_or_operation == 0){
				uint16_t update_count = get_saved_record_count();
				char buf[70];
				if(update_count){
					sprintf(buf, "(ENGMODE)Data Upload. Space: %d/%d",update_count,MAX_BTSSTESTCOUNTER);
					message(buf);
					upload_saved_recod();
				}
				else{
					sprintf(buf, "(ENGMODE)No data to be upload.");
					message(buf);
				}
			}
		}
		else{
			if(ENG_CNT_SSID!=NULL){
				memset(ENG_CNT_SSID, 0x00, SSID_LEN);
			}
		}
		
		if(!TCP_connect(&ENG_try_soc,  false )){
			ENG_WIFI_STATUS = WIFI_disconnect;
			ENG_TCP_STAUTS = Soc_TCP_disconnected;
		}
		else{	
			ENG_TCP_STAUTS = Soc_TCP_connecting;
			ENG_WIFI_STATUS_UPDATE = 0;
		}	
		break;
	case WIFI_disconnect                   :
		UARTprintf("WIFI_disconnect\n");
		if(ENG_CNT_SSID!=NULL){
			memset(ENG_CNT_SSID, 0x00, SSID_LEN);
		}
		ENG_TCP_STAUTS = Soc_TCP_disconnected;
		break;

	case WIFI_off                          :
		UARTprintf("WIFI_off\n");
		break;
	case WIFI_connect_fail                 :
		UARTprintf("WIFI_connect_fail\n");
		if(ENG_CNT_SSID!=NULL){
			memset(ENG_CNT_SSID, 0x00, SSID_LEN);
		}
		ENG_TCP_STAUTS = Soc_TCP_disconnected;
		break;
	case WIFI_getting_IP:
		UARTprintf("WIFI_getting_IP\n");
		ENG_TCP_STAUTS = Soc_TCP_disconnected;
		break;
	case WIFI_module_fail                  :
		UARTprintf("WIFI_module_fail\n");
		ENG_TCP_STAUTS = Soc_TCP_disconnected;
		break;
	case WIFI_TX_buffer_over_flow          :
		UARTprintf("WIFI_TX_buffer_over_flow\n");
		ENG_TCP_STAUTS = Soc_TCP_disconnected;
		break;
	case WIFI_receive_command_error_format :
		UARTprintf("WIFI_receive_command_error_format\n");
		ENG_TCP_STAUTS = Soc_TCP_disconnected;
		break;
	case WIFI_receive_command_error_type   :
		UARTprintf("WIFI_receive_command_error_type\n");
		ENG_TCP_STAUTS = Soc_TCP_disconnected;
		break;
	}
	UARTprintf("######################\n\n");
}



widget engmode_widget = {
		.Key_UP = engmode_UP,
		.Key_DOWN = engmode_DOWN,
		.Key_RIGHT = engmode_RIGHT,
		.Key_LEFT = engmode_LEFT,
		.Key_SELECT = engmode_SELECT,
		.routine = engmode_routine,
		.service = engmode_service,
		.initial = engmode_initial,
		.display = engmode_DISPLAY,
		.subwidget = 0,
		.number_of_widget = 0,
		.index = 0,//for scanner set
		.name = "engmode",
};

void engmode_initial(){
	MFC_TD* MT = Get_MFC_TEST_DATA();
	set_upload_recod_mode(MFC_upload);
	
	set_scanner_cb(ENG_SCANNER_CB);

	wifi_config(1, ENG_wifi_service_cb);

	if(MT->SSID[0] != NULL && MT->SSID_PW[0] != NULL){					
		wifi_connnect_AP(MT->SSID, MT->SSID_PW);
	}

	
	Get_module_version(GET_WIFI_version);
	if(get_routine_switch()){
		Main_routine_switch(0);
	}

	set_language(en_US);
	if(get_module_version_timer) unregister_timer(get_module_version_timer);
	get_module_version_counter = 5;
	get_module_version_timer = register_timer(10,version_timer_cb);
	start_timer(get_module_version_timer);
}


void engmode_DISPLAY()
{
	uint8_t l,sLen;

	LANGUAGE LangIdx = get_language_setting();
	SYS_INFO* info = Get_system_info();
	tRectangle sTemp;
	
	// Clears the screen
	ST7565ClearScreen(BUFFER0);
	
	// Refresh the screen to see the results
	ST7565Refresh(BUFFER0);

	switch(engmode_mode_list){
	case FW:
		{
			// Add HW version
			uint8_t FLS_ver[21] = {0X00};
			snprintf((char*)FLS_ver,sizeof(FLS_ver),"HW VER: V%02d", (uint8_t)get_hardware_version());
			ST7565DrawStringx16(2, 54, FLS_ver, Font_System5x8);
			
			// firmware version
			ST7565DrawStringx16(2, 0, strFWver, Font_System5x8);
			
			// flash version
			memset(FLS_ver,0,sizeof(FLS_ver));
			A25L032ReadData(0, FLS_ver, sizeof(FLS_ver));			
			ST7565DrawStringx16(2, 9, FLS_ver, Font_System5x8);

			// upload account		
			{
#if !AWS_IOT_SERVICE
				char buf[20] = {0};
				snprintf(buf,20,CLOUD_ACCOUNT);
#else
				char* buf = "AWS IOT SERVICE";
#endif
				ST7565DrawStringx16(2, 18, (uint8_t*)buf, Font_System5x8);
			}
			// DHC SN			
			sLen = strlen((const char*)info->SNC);
			for(l = 0; l < sLen; l++){
				if(info->SNC[l] == '@') break;
				ST7565Draw6x8Char(2 + l*6, 27, (info->SNC[l] == NULL)? ' ':info->SNC[l], Font_System5x8);
			}
			// product SN			
			sLen = strlen((const char*)info->SNP);
			for(l = 0; l < sLen; l++){
				if(info->SNP[l] == '@') break;
				ST7565Draw6x8Char(2 + l*6, 36, (info->SNP[l] == NULL)? ' ':info->SNP[l], Font_System5x8);
			}
			
			// module version 1+2
			if(get_module_version){
				get_module_version = 0;
				if(*ESP8266_FW_1 == NULL){
					ST7650Yellow();
					snprintf(ESP8266_FW_1,sizeof(ESP8266_FW_1),"WIFI VERSION_1 FAIL");
					//snprintf(ESP8266_FW_2,sizeof(ESP8266_FW_2),"WIFI VERSION_2 FAIL");
				}
				ST7565DrawStringx16(2, 45, (uint8_t*)ESP8266_FW_1, Font_System5x8);				
				//ST7565DrawStringx16(2, 54, (uint8_t*)ESP8266_FW_2, Font_System5x8);
				
			}
			else{
				ST7565DrawStringx16(0, 45, "GET WIFI VERSION...", Font_System5x8);
			}		
			
			break;
		}
	case LIST:
		{
			ST7650Blue();
			if(engmode_widget.index/7 == 1){
				ST7565DrawStringx16(0, 2, "<8. WIFI>", Font_System5x8);
			}
			else{
				ST7565DrawStringx16(0,  2, "<1. LCD CONTRAST>", Font_System5x8);
				ST7565DrawStringx16(0, 11, "<2. TEMPERATURE>", Font_System5x8);
				ST7565DrawStringx16(0, 20, "<3. BACKLIGHT>", Font_System5x8);
				ST7565DrawStringx16(0, 29, "<4. PRINTER>", Font_System5x8);
				ST7565DrawStringx16(0, 38, "<5. VOLTAGE CALI.>", Font_System5x8);
				ST7565DrawStringx16(0, 47, "<6. IR/CCA CALI.>", Font_System5x8);
				ST7565DrawStringx16(0, 56, "<7. BARCODE SETTING>", Font_System5x8);
			}
			EngmodeAntiWhiteItem(engmode_widget.index);
			break;
		}
	case CONTRAST:
		{
			ST7650Blue();
			
			LcdDrawScreen(0, BUFFER0);

			sTemp.i16XMin = 42;
			sTemp.i16XMax = 85;
			
			sTemp.i16YMin = 0;
			sTemp.i16YMax = 23;
			ST7565RectReverse(&sTemp, HORIZ);

			char buf[20] = {0};
			sprintf(buf,"LCD Contrast:%0d",LCM_BOOSTER_CMD? info->bias9bter5x_LCDcontrast:info->LCDcontrast);

			ST7565DrawStringx16(0, 50, (uint8_t*)buf, Font_System5x8);			
			break;
		}
	case BARCODE:
		{
			ST7650Blue();
			ST7565DrawStringx16(0,  0, "<BARCODE ENGMODE>", Font_System5x8);			
			ST7565DrawStringx16(0, 32, "-------------------------", Font_System5x8);			

			if(engmode_widget.parameter == GET_M){
				ST7565DrawStringx16(0, 16, "< GET READ MODE >", Font_System5x8);
				ST7565DrawStringx16(0, 40, ENG_SCANNER_RECEIVED, Font_System5x8);
			}
			else if(engmode_widget.parameter == SET_M){
				ST7565DrawStringx16(0, 16, "< SET READ MODE >", Font_System5x8);
			}
			else if(engmode_widget.parameter == SCAN){
				ST7565DrawStringx16(0, 16, "< SCAN >", Font_System5x8);
					ST7565DrawStringx16(0, 40, ENG_SCANNER_RECEIVED, Font_System5x8);
				memset(ENG_SCANNER_RECEIVED, 0, sizeof(ENG_SCANNER_RECEIVED));
			}
			else if(engmode_widget.parameter == GET_VER){
				ST7565DrawStringx16(0, 16, "< GET MODULE VER >", Font_System5x8);
				ST7565DrawStringx16(0, 40, ENG_SCANNER_RECEIVED, Font_System5x8);
			}
			else{
				ST7565DrawStringx16(0, 16, "< OFF >", Font_System5x8);			
			}
			/*else{
				//Warranty decoder
			}*/
			
			break;
		}
	case VOLTAGE_K:
		{
			ST7650Blue();
			if(option_or_operation == 0){
				ST7565DrawStringx16(0,  2, "<1. 6V/LOAD_M VOL.>", Font_System5x8);
				ST7565DrawStringx16(0, 11, "<2. 12V/LOAD_P VOL.>", Font_System5x8);
				ST7565DrawStringx16(0, 20, "<3. 24V VOLTAGE>", Font_System5x8);
				ST7565DrawStringx16(0, 29, "<4. VM>", Font_System5x8);
				ST7565DrawStringx16(0, 38, "<5. AM>", Font_System5x8);
				ST7565DrawStringx16(0, 47, "<6. INTERNAL VOLT.>", Font_System5x8);				
				ST7565DrawStringx16(0, 56, "<<<BACK", Font_System5x8);				
				EngmodeAntiWhiteItem(sub_voltage_mode);
			}
			else{
				char ui8buf_L[20] = {0x00};
				char ui8buf_H[20] = {0x00};
				uint32_t ui32buf;
				float voltage;			
				TEST_DATA* TD = Get_test_data();
				
				
				if(sub_voltage_mode == V_6V){
					snprintf(ui8buf_L,sizeof(ui8buf_L)," 6V L:%6.3f,%d",info->V6_L,info->AD_1_5_6);
					snprintf(ui8buf_H,sizeof(ui8buf_H)," 6V H:%6.3f,%d",info->V6_H,info->AD_7);			
				}
				else if(sub_voltage_mode == V_12V){
					snprintf(ui8buf_L,sizeof(ui8buf_L),"12V L:%6.3f,%d",info->V12_L,info->AD_1_5_12);
					snprintf(ui8buf_H,sizeof(ui8buf_H),"12V H:%6.3f,%d",info->V12_H,info->AD_15);				
				}
				else if(sub_voltage_mode == V_24V){
					snprintf(ui8buf_L,sizeof(ui8buf_L),"24V L:%6.3f,%d",info->V24_L,info->AD_1_5_24);
					snprintf(ui8buf_H,sizeof(ui8buf_H),"24V H:%6.3f,%d",info->V24_H,info->AD_31);			
				}
				else if(sub_voltage_mode == V_VM){
					snprintf(ui8buf_L,sizeof(ui8buf_L)," VM L:%6.3f,%d",info->VMCalLvol,(uint16_t)info->VMCalLAD);
					snprintf(ui8buf_H,sizeof(ui8buf_H)," VM H:%6.3f,%d",info->VMCaHLvol,(uint16_t)info->VMCaHLAD);			
				}
				else if(sub_voltage_mode == V_AM){
					ST7565DrawStringx16(0, 0, "KEEP CLAMP VOL>9", Font_System5x8);
					snprintf(ui8buf_L,sizeof(ui8buf_L)," AM L:   %d,%d",(uint16_t)info->AMCalLvol,(uint16_t)info->AMCalLAD);
					snprintf(ui8buf_H,sizeof(ui8buf_H)," AM H:   %d,%d",(uint16_t)info->AMCalHvol,(uint16_t)info->AMCalHAD);
					if(info->AMCalHAD < info->AMCalLAD) ui8buf_H[8] = '-';
				}
				else if(sub_voltage_mode == V_INT){
					snprintf(ui8buf_L,sizeof(ui8buf_L),"INT V:  %d,%d",(uint16_t)(info->ADC_to_BAT_voltage*1000000),g_ui32buf);
				}
				else{
					return;
				}
				
				
				ST7565DrawStringx16(2, 11, (uint8_t*)ui8buf_L, Font_System5x8);
				if(sub_voltage_mode != V_INT){
					ST7565DrawStringx16(2, 21, (uint8_t*)ui8buf_H, Font_System5x8);
				}
				if(voltage_L_H == 99 || sub_voltage_mode == V_INT){
					char ui8buf[20] = {0x00};

					if(sub_voltage_mode == V_AM){
						BattCoreScanVoltage(&voltage);
						if(voltage < 9){		
							ST7650Yellow();
							sprintf(ui8buf,"A: ---.--,----");
						}
						else{
							GetAM(&voltage,&ui32buf);
							snprintf(ui8buf,sizeof(ui8buf),"A: %.3f,%d",voltage,ui32buf);
						}
					}
					else{					
						if(sub_voltage_mode == V_VM){
							GetVM(&voltage,&ui32buf);
						}
						else if(sub_voltage_mode == V_INT){
							ui32buf = g_ui32buf;
							voltage = g_fbuf;
						}
						else{
							BattCoreGetVoltage(&ui32buf,&voltage);
						}						
						snprintf(ui8buf,sizeof(ui8buf),"V: %.3f,%d",voltage,ui32buf);
					}
					ST7565DrawStringx16(2, 31, (uint8_t*)ui8buf, Font_System5x8);
				}
				DigitAntiWhiteItem(voltage_L_H,digit_loc);
			}
			break;
		}
	case ENG_WIFI:
	{
		if(option_or_operation == 0){
			ST7565DrawStringx16(0,  2, "1.CONNECT&UPLOAD LAST", Font_System5x8);					
			ST7565DrawStringx16(0, 11, "2.CONNECT&UPLOAD DUMY", Font_System5x8);					
			ST7565DrawStringx16(0, 20, "3.SMART CONFIG", Font_System5x8);					
			ST7565DrawStringx16(0, 29, "<<<BACK", Font_System5x8);					
			
			// product SN			
			sLen = strlen((const char*)info->SNP);
			for(l = 0; l < sLen; l++){
				if(info->SNP[l] == '@') break;
				ST7565Draw6x8Char(0 + l*6, 38, (info->SNP[l] == NULL)? ' ':info->SNP[l], Font_System5x8);
			}
			EngmodeAntiWhiteItem(sub_WIFI_mode);

			switch(ENG_WIFI_STATUS){
			case WIFI_connected:{		
					ST7650Blue();
					if(ENG_TCP_STAUTS == Soc_TCP_connected){
						ST7565DrawStringx16(1, 47, "WIFI CONNECTED", Font_System5x8);
					}
					else if(ENG_TCP_STAUTS == Soc_TCP_connecting){
						ST7565DrawStringx16(1, 47, "TCP CONNECTING", Font_System5x8);
					}
					else{
						ST7565DrawStringx16(1, 47, "LIMITED CONNECTION", Font_System5x8);
					}
					
					ST7565DrawStringx16(1, 56, "WIFI:", Font_System5x8);
					sLen = strlen((const char*)ENG_CNT_SSID);
					for(l = 0; l < sLen; l++){
						if(l == 15) break;
						ST7565Draw6x8Char((5+l)*6+1, 56, (l>12 && sLen > 15)? '.':ENG_CNT_SSID[l], Font_System5x8);
					}
					break;
				}
			case WIFI_getting_IP:{
					ST7565DrawStringx16(1, 47, "WIFI CONNECTING", Font_System5x8);					
					break;
				}
			case WIFI_disconnect:{		
					ST7650Yellow();
					ST7565DrawStringx16(1, 47, "WIFI DISCONNECTED", Font_System5x8);
					break;
				}
			case WIFI_off:{
					ST7650Red();
					ST7565DrawStringx16(1, 47, "WIFI OFF", Font_System5x8);
					break;
				}
			case WIFI_connect_fail:{
					ST7650Yellow();
					ST7565DrawStringx16(1, 47, "WIFI CONNECT FAIL", Font_System5x8);
					break;
				}
			case WIFI_module_fail:{
					ST7650Red();
					ST7565DrawStringx16(1, 47, "WIFI MODULE FAIL", Font_System5x8);
					break;
				}
			case WIFI_TX_buffer_over_flow:{
					ST7650Red();
					ST7565DrawStringx16(1, 47, "TX BUFFER OVERFLOW", Font_System5x8);
					break;
				}
			case WIFI_receive_command_error_format:{
					ST7650Red();
					ST7565DrawStringx16(1, 47, "COMMAND FORMAT ERROR", Font_System5x8);
					break;
				}
			case WIFI_receive_command_error_type:{
					ST7650Red();
					ST7565DrawStringx16(1, 47, "COMMAND TYPE ERROR", Font_System5x8);
					break;
				}
			default:{
					ST7650Red();
					ST7565DrawStringx16(1, 47, "WIFI UNKNOWN ISSUE", Font_System5x8);
					break;
				}
			}
			
		}
		else{
			ST7650Blue();
			if(sub_WIFI_mode == connect_n_upload){
				ST7565DrawStringx16(2, 11, "1.CONNECT&UPLOAD LAST", Font_System5x8);
				ST7565DrawStringx16(0, 21, "WAIT FOR UPLOADING..", Font_System5x8);
				delay_ms(2000);
				option_or_operation = 0;
				
			}
			else if(sub_WIFI_mode == connect_n_upload_dummy){		
				ST7565DrawStringx16(2, 11, "2.CONNECT&UPLOAD DUMY", Font_System5x8);
				ST7565DrawStringx16(0, 21, "WAIT FOR UPLOADING..", Font_System5x8);
				delay_ms(2000);
				option_or_operation = 0;
			}
			else if(sub_WIFI_mode == smart_config){
				ST7565DrawStringx16(2, 11, "3.SMART CONFIG", Font_System5x8);
				ST7565DrawStringx16(2, 21, "WAIT FOR SETTING...", Font_System5x8);
			}
			
		}
		
		break;	
	}
	}
}


void engmode_SELECT(){

	while(SelectBtn(0));//break if button released
	
	static uint8_t half_step = 0;//deal "ok!"
	SYS_INFO* info = Get_system_info();
	TEST_DATA* TD = Get_test_data();
	MFC_TD* MT = Get_MFC_TEST_DATA();
	
	switch(engmode_mode_list){
	case FW:
		{
			if(get_module_version_timer){
				unregister_timer(get_module_version_timer); 
				get_module_version_timer = NULL;
			}
			engmode_mode_list = LIST;
			engmode_DISPLAY();
			break;
		}
	case LIST:
		{
			engmode_mode_list = (eng_mode)engmode_widget.index;
			if(engmode_mode_list == VOLTAGE_K){
				option_or_operation = 0;
				sub_voltage_mode = V_6V;
			}
			else if(engmode_mode_list == ENG_WIFI){
				option_or_operation = 0;
				sub_WIFI_mode = connect_n_upload;
				if(wifi_get_status() == WIFI_connected){
					if(wifi_get_connected_AP(ENG_CNT_SSID, SSID_LEN)){
						if(!memcmp(MT->SSID,ENG_CNT_SSID,strlen((const char*)ENG_CNT_SSID))){
							goto NEXT;
						}
					}
					
					if(ENG_CNT_SSID!=NULL) memset(ENG_CNT_SSID, 0x00, SSID_LEN);
					wifi_config(0, ENG_wifi_service_cb);
					delay_ms(500);
					wifi_config(1, ENG_wifi_service_cb);
					if(MT->SSID[0] != NULL && MT->SSID_PW[0] != NULL){					
						wifi_connnect_AP(MT->SSID, MT->SSID_PW);
					}				
				}

				ENG_WIFI_STATUS = wifi_get_status();
			}
NEXT:
			engmode_DISPLAY();
			engmode_service();
			break;
		}
	case VOLTAGE_K:
		{
			if(option_or_operation == 0){
				if(sub_voltage_mode == BACK){
					engmode_mode_list = LIST;
				}
				else{
					option_or_operation = 1;
					if(sub_voltage_mode == V_6V) TD->SYSTEM_CHANNEL = MAP_6V;
					else if(sub_voltage_mode == V_24V) TD->SYSTEM_CHANNEL = MAP_24V;
					else TD->SYSTEM_CHANNEL = MAP_12V;					
					
					voltage_L_H = 0;
					digit_loc = 0;					
				}
			}
			else{
				if(sub_voltage_mode == V_INT) voltage_L_H = 99;
				if(voltage_L_H == 0){
					if(sub_voltage_mode == V_AM){//no work if AM issue
						if(info->AMCalLAD > 5000) return;
						info->AMCalLvol = 0;
					}
					voltage_L_H = 1;
					digit_loc = 0;
				}
				else if(voltage_L_H == 1){
					if(sub_voltage_mode == V_AM){//no work if AM issue
						if(info->AMCalHAD > 5000 || info->AMCalHAD < info->AMCalLAD) return;
					}
					voltage_L_H = 99;
					digit_loc = 0;
				}
				else{
					uint8_t error = false;
					// save coefficient
					if(sub_voltage_mode == V_6V){
						info->VLoadVL_MINS = info->V6_L;
						info->VLoadVH_MINS = info->V6_H;
						
						if(save_system_info(&info->AD_1_5_6, sizeof(info->AD_1_5_6)) == false) error = 1;
						if(save_system_info(&info->V6_L, sizeof(info->V6_L)) == false) error = 1;
						if(save_system_info(&info->AD_7, sizeof(info->AD_7)) == false) error = 1;
						if(save_system_info(&info->V6_H, sizeof(info->V6_H)) == false) error = 1;
						
						if(save_system_info(&info->VLoadADL_MINS, sizeof(info->VLoadADL_MINS)) == false) error = 1;
						if(save_system_info(&info->VLoadVL_MINS, sizeof(info->VLoadVL_MINS)) == false) error = 1;
						if(save_system_info(&info->VLoadADH_MINS, sizeof(info->VLoadADH_MINS)) == false) error = 1;
						if(save_system_info(&info->VLoadVH_MINS, sizeof(info->VLoadVH_MINS)) == false) error = 1;

					}
					else if(sub_voltage_mode == V_12V){
						info->VLoadVL_PLUS = info->V12_L;
						info->VLoadVH_PLUS = info->V12_H;
						
						if(save_system_info(&info->AD_1_5_12, sizeof(info->AD_1_5_12)) == false) error = 1;
						if(save_system_info(&info->V12_L, sizeof(info->V12_L)) == false) error = 1;
						if(save_system_info(&info->AD_15, sizeof(info->AD_15)) == false) error = 1;
						if(save_system_info(&info->V12_H, sizeof(info->V12_H)) == false) error = 1;
						
						if(save_system_info(&info->VLoadADL_PLUS, sizeof(info->VLoadADL_PLUS)) == false) error = 1;
						if(save_system_info(&info->VLoadVL_PLUS, sizeof(info->VLoadVL_PLUS)) == false) error = 1;
						if(save_system_info(&info->VLoadADH_PLUS, sizeof(info->VLoadADH_PLUS)) == false) error = 1;
						if(save_system_info(&info->VLoadVH_PLUS, sizeof(info->VLoadVH_PLUS)) == false) error = 1;
					}
					else if(sub_voltage_mode == V_24V){
						if(save_system_info(&info->AD_1_5_24, sizeof(info->AD_1_5_24)) == false) error = 1;
						if(save_system_info(&info->V24_L, sizeof(info->V24_L)) == false) error = 1;
						if(save_system_info(&info->AD_31, sizeof(info->AD_31)) == false) error = 1;
						if(save_system_info(&info->V24_H, sizeof(info->V24_H)) == false) error = 1;
					}					
					else if(sub_voltage_mode == V_VM){
						if(save_system_info(&info->VMCalLAD, sizeof(info->VMCalLAD)) == false) error = 1;
						if(save_system_info(&info->VMCalLvol, sizeof(info->VMCalLvol)) == false) error = 1;
						if(save_system_info(&info->VMCaHLAD, sizeof(info->VMCaHLAD)) == false) error = 1;
						if(save_system_info(&info->VMCaHLvol, sizeof(info->VMCaHLvol)) == false) error = 1;
					}
					else if(sub_voltage_mode == V_AM){
						if(save_system_info(&info->AMCalLAD, sizeof(info->VMCalLAD)) == false) error = 1;
						if(save_system_info(&info->AMCalLvol, sizeof(info->VMCalLvol)) == false) error = 1;
						if(save_system_info(&info->AMCalHAD, sizeof(info->VMCaHLAD)) == false) error = 1;
						if(save_system_info(&info->AMCalHvol, sizeof(info->VMCaHLvol)) == false) error = 1;
					}
					else if(sub_voltage_mode == V_INT){
						if(save_system_info(&info->ADC_to_BAT_voltage, sizeof(info->ADC_to_BAT_voltage)) == false) error = 1;
					}
					else{
						return;
					}
					
					if(error) while(1);
					option_or_operation = 0;
				}
			}
			engmode_DISPLAY();				
			break;
		}
	case BARCODE:
		if(engmode_widget.parameter != OFF){
			engmode_service();
		}
		else{
			engmode_mode_list = LIST;
			engmode_DISPLAY();
		}
		break;
	case CONTRAST:
		half_step^=0x01;
		if(half_step == 1){			
			ST7565DrawStringx16(105, 50, "OK!", Font_System5x8);			
		}
		else{			
			engmode_mode_list = TEMP;
			if(LCM_BOOSTER_CMD){
				save_system_info(&info->bias9bter5x_LCDcontrast, sizeof(info->bias9bter5x_LCDcontrast));
			}
			else{
				save_system_info(&info->LCDcontrast, sizeof(info->LCDcontrast));
			}
			engmode_service();
		}		
		break;
	case IR_CCA:
		break;
	case TEMP:
		engmode_mode_list = BACKLIGHT;
		engmode_service();
		break;
	case BACKLIGHT:
		engmode_mode_list = PRINTER;		
		engmode_service();
		break;
	case PRINTER:
		break;
	case ENG_WIFI:
	{
		if(option_or_operation == 0){
			if(sub_WIFI_mode == WIFI_BACK){
				engmode_mode_list = LIST;
			}
			else{
				if(sub_WIFI_mode != smart_config && ENG_WIFI_STATUS != WIFI_connected) return;
				option_or_operation = 1;
			}
		}
		else{
			// not define yet
		}
		engmode_service();
		engmode_DISPLAY();
		break;
	}
	
	}
}

void engmode_DOWN(){
	SYS_INFO* info = Get_system_info();
	TEST_DATA* TD = Get_test_data();
	float fbuf;
	
	if(engmode_mode_list == LIST){
		engmode_widget.index = (engmode_widget.index + 1)%ENGMODE_LIST_NUM;
		engmode_DISPLAY();	
	}
	else if(engmode_mode_list == VOLTAGE_K){
		if(option_or_operation == 0){
			sub_voltage_mode = (sub_voltage_mode+1)%sub_VOLTAGE_calibration_num;
		}
		else{
			fbuf = pow(10, ((-1)*digit_loc)+ ((sub_voltage_mode == V_AM)? 2:(sub_voltage_mode == V_INT)? -4:-1));
			
			if(sub_voltage_mode == V_6V){
				if(voltage_L_H == 0){
					info->V6_L = ((info->V6_L*1000) <= 1500)? 2.0:info->V6_L-fbuf;
				}
				else if(voltage_L_H == 1){
					info->V6_H = ((info->V6_H*1000) <= 6500)? 7.5:info->V6_H-fbuf;
				}
			}
			else if(sub_voltage_mode == V_12V){
				if(voltage_L_H == 0){
					info->V12_L = ((info->V12_L*1000) <= 1500)? 2.0:info->V12_L-fbuf;
				}
				else if(voltage_L_H == 1){
					info->V12_H = ((info->V12_H*1000) <= 15000)? 15.3:info->V12_H-fbuf;
				}
			}
			else if(sub_voltage_mode == V_24V){
				if(voltage_L_H == 0){
					info->V24_L = ((info->V24_L*1000) <= 1500)? 2.0:info->V24_L-fbuf;
				}
				else if(voltage_L_H == 1){
					info->V24_H = ((info->V24_H*1000) <= 30000)? 30.3:info->V24_H-fbuf;
				}
			}
			else if(sub_voltage_mode == V_VM){
				if(voltage_L_H == 0){
					info->VMCalLvol = ((info->VMCalLvol*1000) <= 4500)? 5.5:info->VMCalLvol-fbuf;
				}
				else{
					info->VMCaHLvol = ((info->VMCaHLvol*1000) <= 30000)? 30.3:info->VMCaHLvol-fbuf;
				}				
			}
			else if(sub_voltage_mode == V_AM){
				if(voltage_L_H){
					if(info->AMCalHAD < 5000 && info->AMCalHAD > info->AMCalLAD){
						info->AMCalHvol = ((info->AMCalHvol) <= 500)? 550:info->AMCalHvol-fbuf;
					}				
				}				
			}
			else if(sub_voltage_mode == V_INT){
				info->ADC_to_BAT_voltage = ((info->ADC_to_BAT_voltage*1000000) <= 2300)? 0.002500:info->ADC_to_BAT_voltage-fbuf;

			}
			else{
				return;
			}
		}		
		engmode_DISPLAY();	
	}
	else if(engmode_mode_list == ENG_WIFI){		
		if(option_or_operation == 0){
			sub_WIFI_mode = (sub_WIFI_mode+1)%sub_WIFI_function_num;
			engmode_DISPLAY();	
		}
	}
	else if(engmode_mode_list == CONTRAST){
		if(LCM_BOOSTER_CMD){
			info->bias9bter5x_LCDcontrast = (info->bias9bter5x_LCDcontrast == bias9bter5x_LCDcontrast_min)? bias9bter5x_LCDcontrast_max:info->bias9bter5x_LCDcontrast-1;
		}
		else{
			info->LCDcontrast = (info->LCDcontrast == LCDcontrast_min)? LCDcontrast_max:info->LCDcontrast-1;
		}
		ST7565SetBrightness(LCM_BOOSTER_CMD? (info->bias9bter5x_LCDcontrast):(info->LCDcontrast));
		engmode_DISPLAY();
	}
}



void engmode_UP(){
	SYS_INFO* info = Get_system_info();
	TEST_DATA* TD = Get_test_data();
	
	float fbuf;
	if(engmode_mode_list == LIST){
		engmode_widget.index = (engmode_widget.index == 0)? (ENGMODE_LIST_NUM-1):engmode_widget.index-1;
		engmode_DISPLAY();	
	}
	else if(engmode_mode_list == VOLTAGE_K){		
		if(option_or_operation == 0){
			sub_voltage_mode = (sub_voltage_mode == 0)? (sub_VOLTAGE_calibration_num-1):sub_voltage_mode-1;
		}
		else{
			fbuf = pow(10, ((-1)*digit_loc)+ ((sub_voltage_mode == V_AM)? 2:(sub_voltage_mode == V_INT)? -4:-1));
			
			if(sub_voltage_mode == V_6V){
				if(voltage_L_H == 0){
					info->V6_L = ((info->V6_L*1000) >= 2000)? 1.5:info->V6_L+fbuf;
				}
				else if(voltage_L_H == 1){
					info->V6_H = ((info->V6_H*1000) >= 7500)? 6.5:info->V6_H+fbuf;
				}
			}
			else if(sub_voltage_mode == V_12V){
				if(voltage_L_H == 0){
					info->V12_L = ((info->V12_L*1000) >= 2000)? 1.5:info->V12_L+fbuf;
				}
				else if(voltage_L_H == 1){
					info->V12_H = ((info->V12_H*1000) >= 15300)? 15.0:info->V12_H+fbuf;
				}
			}
			else if(sub_voltage_mode == V_24V){
				if(voltage_L_H == 0){
					info->V24_L = ((info->V24_L*1000) >= 2000)? 1.5:info->V24_L+fbuf;
				}
				else if(voltage_L_H == 1){
					info->V24_H = ((info->V24_H*1000) >= 30300)? 30.0:info->V24_H+fbuf;
				}
			}
			else if(sub_voltage_mode == V_VM){
				if(voltage_L_H == 0){
					info->VMCalLvol = ((info->VMCalLvol*1000) >= 5500)? 4.5:info->VMCalLvol+fbuf;
				}
				else{
					info->VMCaHLvol = ((info->VMCaHLvol*1000) >= 30300)? 30.0:info->VMCaHLvol+fbuf;
				}
			}
			else if(sub_voltage_mode == V_AM){
				if(voltage_L_H){
					if(info->AMCalHAD < 5000 && info->AMCalHAD > info->AMCalLAD){
						info->AMCalHvol = ((info->AMCalHvol) >= 550)? 500:info->AMCalHvol+fbuf;
					}				
				}				
			}
			else if(sub_voltage_mode == V_INT){
				info->ADC_to_BAT_voltage = ((info->ADC_to_BAT_voltage*1000000) >= 2500)? 0.002300:info->ADC_to_BAT_voltage+fbuf;
			}
			else{
				return;
			}
			
		}		
		engmode_DISPLAY();	
	}
	else if(engmode_mode_list == ENG_WIFI){		
		if(option_or_operation == 0){
			sub_WIFI_mode = (sub_WIFI_mode == 0)? (sub_WIFI_function_num-1):sub_WIFI_mode-1;
			engmode_DISPLAY();	
		}
	}
	else if(engmode_mode_list == CONTRAST){
		if(LCM_BOOSTER_CMD){
			info->bias9bter5x_LCDcontrast = (info->bias9bter5x_LCDcontrast == bias9bter5x_LCDcontrast_max)? bias9bter5x_LCDcontrast_min:info->bias9bter5x_LCDcontrast+1;
		}
		else{
			info->LCDcontrast = (info->LCDcontrast == LCDcontrast_max)? LCDcontrast_min:info->LCDcontrast+1;
		}
		ST7565SetBrightness(LCM_BOOSTER_CMD? (info->bias9bter5x_LCDcontrast):(info->LCDcontrast));
		engmode_DISPLAY();
	}
}

void engmode_RIGHT(){
	SYS_INFO* info = Get_system_info();
	if(engmode_mode_list == VOLTAGE_K){
		if(voltage_L_H != 99){
			if(sub_voltage_mode == V_AM && voltage_L_H == 0) return;
			
			digit_loc = (digit_loc == 2)? 0:digit_loc+1;
			engmode_DISPLAY();
		}		
		
	}
	else if(engmode_mode_list == BARCODE){
		memset(ENG_SCANNER_RECEIVED, 0, sizeof(ENG_SCANNER_RECEIVED));
		engmode_widget.parameter = (engmode_widget.parameter+1)%sub_scanner_mode_num;
		if(engmode_widget.parameter != SCAN) SCAN_OFF;
		engmode_DISPLAY();
	}

}

void engmode_LEFT(){
	SYS_INFO* info = Get_system_info();
	if(engmode_mode_list == VOLTAGE_K){
		if(voltage_L_H != 99){
			if(sub_voltage_mode == V_AM && voltage_L_H == 0) return;

			digit_loc = (digit_loc == 0)? 2:digit_loc-1;
			engmode_DISPLAY();
		}
		else return;
	}
	else if(engmode_mode_list == CONTRAST){		
		engmode_mode_list = LIST;
	}
	else if(engmode_mode_list == BARCODE){
		memset(ENG_SCANNER_RECEIVED, 0, sizeof(ENG_SCANNER_RECEIVED));
		engmode_widget.parameter = (engmode_widget.parameter == 0)? sub_scanner_mode_num-1:engmode_widget.parameter-1;
		if(engmode_widget.parameter != SCAN) SCAN_OFF;
	}
	else return;
	
	engmode_DISPLAY();
}

void engmode_routine(){
	TEST_DATA* TD = Get_test_data();
	SYS_INFO* info = Get_system_info();
	uint32_t ui32buf;
	float voltage;
	static uint16_t count = 0;
	if(get_module_version){
		if(get_module_version_timer){
			unregister_timer(get_module_version_timer); 
			get_module_version_timer = NULL;
		}
		if(engmode_mode_list == FW) engmode_DISPLAY();
		else get_module_version = 0;
	}
	
	
	if(engmode_mode_list == BARCODE){
		if(eng_bSCANNERecompl){
			eng_bSCANNERecompl = false;

			switch(engmode_widget.parameter){
			case GET_VER:
				{
					scanner_msg_to_string(get_version, ENG_SCANNER_RECEIVED, sizeof(ENG_SCANNER_RECEIVED));
					engmode_DISPLAY();
					break;
				}
			case GET_M:
				{
					scanner_msg_to_string(get_rd_mode, ENG_SCANNER_RECEIVED, sizeof(ENG_SCANNER_RECEIVED));
					engmode_DISPLAY();
					break;
				}
			case SET_M:
				break;
			case SCAN:
				SCANNER_BUZZ;
				SCAN_OFF;
				UARTprintf("[EE barcode test]:%s\n", ENG_SCANNER_RECEIVED);
				engmode_DISPLAY();
				break;
				}
		}
	}
	else if(engmode_mode_list == VOLTAGE_K){
		if(option_or_operation == 0) return;
		if(voltage_L_H != 99){			
			if(sub_voltage_mode == V_VM){
				VMGetAD(&voltage, 100);//100??????????????????????????????????????????????
				if(voltage_L_H == 0) info->VMCalLAD = voltage;
				else info->VMCaHLAD = voltage;
			}
			else if(sub_voltage_mode == V_AM){
				BattCoreScanVoltage(&voltage);
				if(voltage < 9){
					voltage = 9999;
				}
				else{
					AM_NEG5V_PIN_ON;
					AMGetAD(&voltage, 250);//250??????????????????????????????????????????
					AM_NEG5V_PIN_OFF;
				}
				
				if(voltage_L_H == 0){
					info->AMCalLAD = voltage;
					info->AMCalLvol = 0;
				}
				else info->AMCalHAD = voltage;				
			}
			else if(sub_voltage_mode == V_INT){
				BattCoreChkIntBatt(&g_ui32buf,&g_fbuf);
			}
			else{
				BattCoreGetVoltage(&ui32buf,&voltage);
				
				if(sub_voltage_mode == V_6V){
					if(voltage_L_H == 0){
						info->AD_1_5_6 = ui32buf;
						info->VLoadADL_MINS = ui32buf;
					}
					else{
						info->AD_7 = ui32buf;
						info->VLoadADH_MINS = ui32buf;
					}
				}
				else if(sub_voltage_mode == V_12V){
					if(voltage_L_H == 0){
						info->AD_1_5_12 = ui32buf;
						info->VLoadADL_PLUS = ui32buf;
					}
					else{
						info->AD_15 = ui32buf;
						info->VLoadADH_PLUS = ui32buf;
					}
				}
				else if(sub_voltage_mode == V_24V){
					if(voltage_L_H == 0) info->AD_1_5_24 = ui32buf;
					else info->AD_31 = ui32buf; 		
				}
			}			
		}
		count = (count > 200)? 0:count+1;
		if(count) return;
		
		
		engmode_DISPLAY();
	}
	else if(engmode_mode_list == IR_CCA){
		VOL_CH IR_CCAmode = (sub_IRCCA_mode == IR_6V || sub_IRCCA_mode == CCA_6V)? MAP_6V:MAP_12V;
		if(sub_IRCCA_mode == IR_6V || sub_IRCCA_mode == IR_12V){
			IREngMode(IR_CCAmode);
		}
		else{
			CCAEngMode(IR_CCAmode);
		}
		sub_IRCCA_mode = (sub_IRCCA_mode == CCA_12V)? IR_6V:sub_IRCCA_mode+1;
		if(sub_IRCCA_mode == IR_6V){
			engmode_mode_list = LIST;
			engmode_DISPLAY();
		}
	}
	else if(engmode_mode_list == ENG_WIFI){

		if(ENG_WIFI_STATUS_UPDATE){
			ENG_WIFI_STATUS_UPDATE = 0;
			option_or_operation = 0;
			engmode_DISPLAY();
		}

	}
}


void engmode_service(){
	SYS_INFO* info = Get_system_info();
	uint8_t ui8PrinterALine[24] = {0};
	memset(ui8PrinterALine, 'z'+4, 24);
	uint16_t year;
	uint8_t month,date,day,hour,min,sec;
	uint16_t update_count = get_saved_record_count();
	char buf[70];

	switch(engmode_mode_list){
	case ENG_WIFI:
	{
		if(!option_or_operation){			
			if(update_count){
				sprintf(buf, "(ENGMODE)Data Upload. Space: %d/%d",update_count,MAX_BTSSTESTCOUNTER);
				message(buf);
				upload_saved_recod();
			}
			else{
				sprintf(buf, "(ENGMODE)No data to be upload.");
				message(buf);
			}
			return;
		}
		
		if(sub_WIFI_mode == connect_n_upload){
			if(ENG_WIFI_STATUS != WIFI_connected) return;

			if(update_count){
				sprintf(buf, "(ENGMODE)Data Upload. Space: %d/%d",update_count,MAX_BTSSTESTCOUNTER);
				message(buf);
				upload_saved_recod();
			}
			else{
				sprintf(buf, "(ENGMODE)No data to be upload.");
				message(buf);
			}
		}
		else if(sub_WIFI_mode == connect_n_upload_dummy){
			if(ENG_WIFI_STATUS != WIFI_connected) return;

			ExtRTCRead( &year, &month, &date, &day, &hour, &min, &sec);
			BATTERY_SARTSTOP_RECORD_T record = {.Test_Type = Battery_Test,
												.Test_Time = {year, month, date, hour, min, sec,},
												.VIN = {'\0'},
												.RO = {'\0'},
												.Battery_Type = VRLA_GEL,
												.Voltage = 12.80,
												.Ratting = SAE,
												.CCA_Capacity = 500,
												.CCA_Measured = 500,
												.Judgement = TR_GOODnPASS,
												.SOH = 100,
												.SOC = 100,
												.RC_Set = 0,
												.RC_Measured = 0,
												.RC_judegment = RC_GOOD,
												.Test_code = {'\0'},
												.Temperature = 25.0};
			memcpy(record.VIN, "0123456789ABCDEFG", VININ_LEN1);
			memcpy(record.RO, "        ", ROIN_LEN);
			memcpy(record.Test_code, "DHC  FACTORY", TESTCODE_LEN);
			SaveTestRecords(Battery_Test,&record, sizeof(record), Wait_uplaod);
			
			update_count = get_saved_record_count();
			sprintf(buf, "(ENGMODE)Dummy Data create. Space: %d/%d",update_count,MAX_BTSSTESTCOUNTER);
			message(buf);
			upload_saved_recod();			
			
		}
		else{//sub_WIFI_mode == smart_config;
			ENG_WIFI_STATUS = WIFI_disconnect;
			ENG_WIFI_STATUS_UPDATE = 0;
			wifi_connnect_policy(conn_smart);					
		}
		break;
	}
	
	case BARCODE:
	{
		eng_bSCANNERecompl = false;
		switch(engmode_widget.parameter){
		case GET_VER:
			eng_SC_alloc_Len = scanner_send_cmd(get_version);
			break;
		case GET_M:
			eng_SC_alloc_Len = scanner_send_cmd(get_rd_mode);
			break;
		case SET_M:
			scanner_send_cmd(set_rd_mode);
			delay_ms(100);
			scanner_send_cmd(save_config);
			delay_ms(100);
			scanner_send_cmd(reboot);
			delay_ms(500);
			
			eng_SC_alloc_Len = scanner_send_cmd(get_rd_mode);
			engmode_widget.parameter = GET_M;
			break;
		case SCAN:
			eng_SC_alloc_Len = SCANNER_RX_BUFFER_SIZE;
			SCAN_ON;
			break;
		}
		
		break;
	}
	case IR_CCA:
		{
			sub_IRCCA_mode = IR_6V;
			break;
		}
	case TEMP:
		TempEngMode();
		enter = true;
		break;
	case BACKLIGHT:
		Check_backlight();
		enter = true;
		break;
	case PRINTER:
		PrinterPrintSpace(20);
		PrinterStartWB(ui8PrinterALine, STB_ALL, 2, 1, 20);
		PrinterPrintSpace(200);
		
		g_bENGmode = false;
		// return/move widget

		
		// FW reset
		ROM_SysCtlReset();
		while(1);
	
	default:
		break;
	}
}


void EngmodeAntiWhiteItem(uint8_t menuIdx)
{
	tRectangle sTemp;
	LANGUAGE LangIdx = get_language_setting();
	UARTprintf("EngmodeAntiWhiteItem(). menuIdx = %d.\n", menuIdx);

	sTemp.i16XMin = 0; sTemp.i16XMax = 127;

	sTemp.i16YMin = 0+(menuIdx%7)*9; sTemp.i16YMax = 9+(menuIdx%7)*9;

	ST7565RectReverse(&sTemp, VERTICAL);
}

void DigitAntiWhiteItem(uint8_t h_L, uint8_t digit)
{
	if(h_L == 99) return;
	tRectangle sTemp;
	LANGUAGE LangIdx = get_language_setting();
	UARTprintf("DigitAntiWhiteItem(). digit = %d.\n", digit);

	sTemp.i16XMin = 55 + (digit)*6;	sTemp.i16XMax = 61 + (digit)*6;
	sTemp.i16YMin = 10 + (h_L*9);	sTemp.i16YMax = 18 + (h_L*9);

	ST7565RectReverse(&sTemp, VERTICAL);
}





