//*****************************************************************************
//
// Source File: Pack_Test_Advised_widget.c
// Description: screen show " Advised action : check connect press enter "
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 08/26/20     David.Wang     Created file.
//
//
// ============================================================================
//
//
// $Header:   $
// $DateTime: $
// $Author:   $
//
//
// Copyright (c) 2020 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#include "Widgetmgr.h"


void Pack_Test_Advised_DISPLAY();
void Pack_Test_Advised_Select();
void Pack_Test_Advised_LEFT();

widget* Pack_Test_Advised_sub_W[] = {
        &PrintResult_widget,
        &AbortTest_widget,

};

widget Pack_Test_Advised_widget = {
        .Key_UP = 0,
        .Key_DOWN = 0,
        .Key_RIGHT = 0,
        .Key_LEFT = Pack_Test_Advised_LEFT,
        .Key_SELECT = Pack_Test_Advised_Select,
        .routine = 0,
        .service = 0,
        .display = Pack_Test_Advised_DISPLAY,
        .subwidget = Pack_Test_Advised_sub_W,
        .number_of_widget = sizeof(Pack_Test_Advised_sub_W)/4,
        .index = 0,
        .no_messgae = 1,
        .name = "Pack_Test_Advised",
};


void Pack_Test_Advised_LEFT(){

    Move_widget(Pack_Test_Advised_sub_W[1], 0);     // GO TO ABOUT WIDGET
}


void Pack_Test_Advised_Select(){

    Move_widget(Pack_Test_Advised_sub_W[0],0);       // GO TO PRINT WIDGET
}

void Pack_Test_Advised_DISPLAY(){

    LCD_display(ADVISED_ACTION, 0);                  // 20200826 dw Edit Warning words

}



