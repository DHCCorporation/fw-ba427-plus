/*
 * VINVKwidget.c
 *
 *  Created on: 2020�~12��23��
 *      Author: henry.yang
 */
#include "Widgetmgr.h"
#define SEL_DEFAULT 1

void VIN_DISPLAY();
void VIN_UP();
void VIN_DOWN();
void VIN_LEFT();
void VIN_RIGHT();
void VIN_SELECT();
void VIN_Routine();
void VIN_initail();

widget* VIN_sub_W[] = {
		&Email_widget,
};

widget VIN_widget = {
		.Key_UP = VIN_UP,
		.Key_DOWN = VIN_DOWN,
		.Key_RIGHT = VIN_RIGHT,
		.Key_LEFT = VIN_LEFT,
		.Key_SELECT = VIN_SELECT,
		.routine = VIN_Routine,
		.service = 0,
		.initial = VIN_initail,
		.display = VIN_DISPLAY,
		.subwidget = VIN_sub_W,
		.number_of_widget = sizeof(VIN_sub_W)/4,
		.index = 0,
		.name = "VIN_VK",
};
static uint8_t str_update = false;

void VIN_cb(char* data,uint8_t len)
{
	SYS_PAR* par = Get_system_parameter();
    str_update = true;
    memset(par->VIN_str,0,sizeof(par->VIN_str));
    memcpy(par->VIN_str, data, len);
    //UARTprintf("VIN_cb: %s\n", data);
}

void VIN_initail(void)
{
    str_update = false;
    VIN_widget.index = SEL_DEFAULT;
}

void VIN_DISPLAY(void)
{
    if(!VIN_widget.parameter){
        ST7565ClearScreen(BUFFER1);
        ST7565Refresh(BUFFER1);
        //LCD_String_ext(16, get_Language_String(S_ADD_VIN), Align_Left);
        LCD_String_ext2(32, get_Language_String((VIN_widget.index)? S_YES:S_NO), Align_Right, 2);
        LCD_Arrow(Arrow_enter | Arrow_RL);    
    }
    else{
        VK_char_num_display();
    }
}

void VIN_Routine(void)
{
    static uint16_t delay = 0;
    if(VIN_widget.parameter){
        if(str_update){            
            Move_widget(VIN_sub_W[0],0);
            return;
        }
        delay = (delay+1)%CURSOR_BLINK_T;
        if(!delay){
            VK_CN_Cursor_blink();
        }
    }
}

void VIN_UP(void)
{
    if(VIN_widget.parameter){
        VK_char_num_up();
    }
}

void VIN_DOWN(void)
{
    if(VIN_widget.parameter){
        VK_char_num_down();
    }
}

void VIN_LEFT(void)
{
    if(!VIN_widget.parameter){        
        VIN_widget.index^=1;
        VIN_DISPLAY();
    }
    else{
        VK_char_num_left();
    }
}

void VIN_RIGHT(void)
{
    if(!VIN_widget.parameter){
        VIN_widget.index^=1;
        VIN_DISPLAY();
    }
    else{
        VK_char_num_right();
    }
    
}

void VIN_SELECT(void)
{
    SYS_PAR* par = Get_system_parameter();
    if(!VIN_widget.parameter){
        if(VIN_widget.index){
            VIN_widget.parameter = 1;
            VK_INFO_T info = {//.title_str = get_Language_String(S_ENTER_VIN),
                              .underline = true,
                              .start_x = 0,
                              .start_y = 20,
                              .char_width = 7,
                              .len = sizeof(par->VIN_str),
                              .arr_flag = (Arrow_enter | Arrow_RL),// | ARROW_UP_DOWN
                              .cb = VIN_cb,};
            VK_char_num_init(&info);
            VIN_DISPLAY();
        }
        else{
            memset(par->VIN_str, 0, sizeof(par->VIN_str));
            Move_widget(VIN_sub_W[0],0);
        }
    }
    else{
        VK_char_num_sel();
    }
}




