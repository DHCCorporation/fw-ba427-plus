//*****************************************************************************
//
// Source File: Backlightsetwidget.c
// Description: Functions definition to control LCD Backlight setting.
//
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 10/07/15     William.H      Created file.
// 10/27/15     William.H      Add BackLightAnimation() funtion which has 10 levels of backlight setting. Each level has its own screen index.
// 03/24/16     Vincent.T      1. gloabal variable g_LangSelectIdx to get current language.
// 03/25/16     Vincent.T      1. Modify BackLightAnimation(); to handle multination language case.
//                             2. gloabal variable g_LangSelectIdx to get current language.
//                             3. Modify BackLightSetLeft(); to handle multination language case.
// 09/21/17 	Jerry.W		   Rewrite Widget for new software architecture
// 09/11/18     Henry.Y        Add Bar_display() to save the flash IC space.
// 10/23/18     Henry.Y        Use main_LcdBacklightConfig to setup brightness.
// 12/07/18     Henry.Y        New display api.
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/app/Backlightsetwidget.c#7 $
// $DateTime: 2017/09/22 11:16:32 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#include "Widgetmgr.h"


void BackLightSet_RIGHT();
void BackLightSet_LEFT();
void BackLightSet_display();
void BackLightSet_SELECT();

void Bar_display(bool on_off, uint8_t index);

widget* BackLightSet_sub_W[] = {
};

widget BackLightSet_widget = {
		.Key_UP = 0,
		.Key_DOWN = 0,
		.Key_RIGHT = BackLightSet_RIGHT,
		.Key_LEFT = BackLightSet_LEFT,
		.Key_SELECT = BackLightSet_SELECT,
		.routine = 0,
		.service = 0,
		.display = BackLightSet_display,
		.subwidget = BackLightSet_sub_W,
		.number_of_widget = sizeof(BackLightSet_sub_W)/4,
		.index = 0,
		.name = "BackLightSet",
		.Combo = Combo_right | Combo_left,
};

void BackLightSet_RIGHT(){
	SYS_INFO* info = Get_system_info();
	if(info->main_LcdBacklightConfig < 100){
		info->main_LcdBacklightConfig += 10;
		ST7650BacklightSet(info->main_LcdBacklightConfig);

	}
}

void BackLightSet_LEFT(){
	SYS_INFO* info = Get_system_info();
	if(info->main_LcdBacklightConfig > 0){
		info->main_LcdBacklightConfig -= 10;
		ST7650BacklightSet(info->main_LcdBacklightConfig);
	}
}

void BackLightSet_display(){
    ST7565ClearScreen(BUFFER1);
    ST7565Refresh(BUFFER1);

    LCD_String_ext(16,get_Language_String(S_BRIGHTNESS) , Align_Central);
    LCD_String_ext(32,get_Language_String(S_ADJUST) , Align_Central);
    LCD_Arrow(Arrow_enter | Arrow_RL);
}


void BackLightSet_SELECT(){
    SYS_INFO* info = Get_system_info();
    save_system_info(&info->main_LcdBacklightConfig, sizeof(info->main_LcdBacklightConfig));
    Return_pre_widget();
}




