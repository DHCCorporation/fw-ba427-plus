//*****************************************************************************
//
// Source File: Datetimesetwidget.h
// Description: Functions declaration to control Date and Time setting.
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 08/05/20     David.Wang     reated file.
// 08/05/20     David.Wang     Clock time rtc to manual operation
//
//
//
//
// ============================================================================
//
//
// $Header:   $
// $DateTime: $
// $Author:   $
//
//
// Copyright (c) 2020 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#ifndef _DATETIMESETWIDGET_H_
#define _DATETIMESETWIDGET_H_

#include <stdint.h>
#include <stdbool.h>
#include "driverlib/sysctl.h"
#include "../service\lcdsvc.h"
#include "../service\extrtcsvc.h"
#include "..\service/flashsvc.h"
#include "../driver\st7565_lcm.h"
#include "utils/uartstdio.h"

void DateTimeDown(uint16_t *pui16Dat, uint8_t ui8DateTimeIdx);
void DateTimeUp(uint16_t *pui16Dat, uint8_t ui8DateTimeIdx);
void ExtRTCAntiWhiteItem(uint8_t ui8DateTimeIdx);
void ExtRTCShowDateTime(LANGUAGE LangIdx, bool blDateTimeSetMode);
void ExtRTCShow(LANGUAGE LangIdx, uint16_t *ui16DatBuf);
void ExtRTCSaved(uint16_t *ui16DatBuf);

#endif /* _DATETIMESETWIDGET_H_ */
