//*****************************************************************************
//
// Source File: Settingmenuwidget.c
// Description: Setting Menu widget including Click(Push to new menu entry to stack)/
//				Left(Pop out stack)/Up/Down function definition.
//				
//
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 09/17/15     William.H      Created file.
// 10/01/15     William.H      Re-arrange/remove Global varaible's usage due to we've used stack to behave the manu level selection.
//                             Respective global varaible by the respective widget file to be maintained by their own.
// 10/05/15     William.H      Check in BT2020 project which branched from LCDFontMaker(Pre-BT2020) by modifed related GPIOs.
// 10/08/15     William.H      Implement LCD Backlight Setting Menu.
// 10/27/15     William.H      To add BackLightAnimation() menu entry in Setting Menu Widget.
// 10/30/15     William.H      To fix the Internal Battery Low suddenly been presented scenario, then clear menu stack and back to Main Menu immediately will cause state currupt issue.
// 11/12/15     William.H      1. To implement the memory storage feature of 'Information' via vitual keyboard input. g_InfVkybdTextEditStr[128] to store input characters; g_InfVkybdLastCharPos to record the latest position.
//                             2. g_InfVkybdLastCharPos does not reset to zero when back to previos menu.
//                             3. g_InfVKybdChar = 34; g_InfVKybdLegacyChar = 34; The initial anti-white position of character is at 'OK' button when re-enter into 'Information' menu.
// 12/08/15     Vincent.T      1. Add Date Time Set Function
// 01/06/16     William.H      Modified SettingMenuClick() function in "Language Select" Menu, to meet the dynamic language set requested by customer.
// 01/12/16     Vincent.T      1. Add "Time setting" screen content
//                             2. Add "Clear memory" screen content
// 01/19/16     William.H      Add Bluetooth setting menu.
// 01/27/16     Henry.Y        Add global variable: g_BTServiceStart
// 03/24/16     Vincent.T      1. Modify SettingMenuAntiWhiteItem() to handle mutination case.
//                             2. #include "..\service/flashsvc.h"
//                             3. Modify SettingMenuUp(); to handle multination language case.
//                             4. Modify SettingMenuDown(); to handle multination language case.
// 03/25/16     Vincent.T      1. Modify SettingMenuClick(); to handle multination language case.
// 04/07/16     Vincent.T      1. Modify SettingMenuClick(); to draw memorized language item.
//                             2. Modify SettingMenuLeft(); to handle multination language case.
// 04/08/16     Vincent.T      1. Modify SettingMenuUp(); due to test counter.
//                             2. Modify SettingMenuDown(); due to test counter.
// 04/08/16     Vincent.T      1. Modify SettingMenuClick(); to handle test counter.
//                             2. #include "TestCounterwidget.h"
//                             3. global variable g_ui8TestCounterType.
//                             4. #include "TestCounterwidget.h"
// 04/11/16     Vincent.T      1. Modify SettingMenuAntiWhiteItem(); due to move "Test Counter" in Tools.
// 06/14/16     Vincent.T      1. Modify SettingMenuClick();(initialize txtEditChar)
// 10/13/16     Ian.C          1. Modify SettingMenuClick(). Add LangSelMenuDrawPageInfo(); to draw page information at bottom right.
// 03/08/17     Vincent.T      1. Modify SettingMenuClick(); character size 7x8 -> 5x8
// 09/21/17 	Jerry.W		   Rewrite Widget for new software architecture
// 09/21/17	 	Jerry.W		   add new sub-Widgets
// 09/28/17	 	Jerry.W		   add new sub-Widgets of Bluetooth_widget
// 02/14/18     Henry.Y        disable bluetooth function
// 09/11/18     Henry.Y        Add OTA_widget.
// 11/19/18     Henry.Y        Fix bug: Lock up/down button in Version page.
// 12/07/18     Henry.Y        New display api.
// 01/02/19     Henry.Y        New widget: Device registration.
// 09/17/20     David Wang     Modified version display screen
// 10/19/20     David Wang     ERASE item no used to del
//
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/app/Settingmenuwidget.c#8 $
// $DateTime: 2017/09/30 16:50:27 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#include "Widgetmgr.h"





void SettingMenu_UP();
void SettingMenu_DOWN();
void SettingMenu_LEFT();
void SettingMenu_SELECT();
void SettingMenu_display();
void SettingMenu_initial();

widget* SettingMenu_sub_W[] = {
		&BackLightSet_widget,
		&LangSel_widget,
		&DateTimeSet_widget,
		//&MemErase_widget,   //20201019 DW edit no uesd to mask
		&InVKbd_widget,
		//&Bluetooth_widget,
		//&TimeZone_widget,   //20200820 DW EDIT NO USED TO MASK
		&TestCounter_widget,
		//&WiFiSet_widget,    //20200820 DW EDIT NO USED TO MASK
		//&OTA_widget,        //20200820 DW EDIT NO USED TO MASK
#if AWS_IOT_SERVICE
		&devicereg_widget
#endif
};

widget SettingMenu_widget = {
		.Key_UP = SettingMenu_UP,
		.Key_DOWN = SettingMenu_DOWN,
		.Key_RIGHT = 0,
		.Key_LEFT = SettingMenu_LEFT,
		.Key_SELECT = SettingMenu_SELECT,
		.initial = SettingMenu_initial,
		.routine = 0,
		.service = 0,
		.display = SettingMenu_display,
		.subwidget = SettingMenu_sub_W,
		.number_of_widget = sizeof(SettingMenu_sub_W)/4,
		.index = 0,
		.name = "SettingMenu",
};
void SettingMenu_initial(){
	if(SettingMenu_widget.parameter == 2){
		SettingMenu_widget.parameter = 0;
		SettingMenu_widget.index = SettingMenu_widget.number_of_widget-1;
	}
}

void SettingMenu_UP(){
	if(SettingMenu_widget.parameter == 0){
		SettingMenu_widget.index = (SettingMenu_widget.index == 0)? SettingMenu_widget.number_of_widget:SettingMenu_widget.index-1;
		SettingMenu_display();
	}
}

void SettingMenu_DOWN(){
	if(SettingMenu_widget.parameter == 0){
		SettingMenu_widget.index = (SettingMenu_widget.index + 1) % (SettingMenu_widget.number_of_widget + 1);
		SettingMenu_display();
	}
}

void SettingMenu_LEFT(){
	if(SettingMenu_widget.parameter == 0){
		Widget_init(manual_test);
	}
	else{
		SettingMenu_widget.parameter = 0;
		SettingMenu_display();		
	}	
}

void SettingMenu_SELECT(){
	if(SettingMenu_widget.parameter == 0){
		if(SettingMenu_widget.index != SettingMenu_widget.number_of_widget){
			Move_widget(SettingMenu_sub_W[SettingMenu_widget.index], 0);
		}
		else{
			SettingMenu_widget.parameter = 1;
			SettingMenu_display();
		}
	}
}

void SettingMenu_display(){
	SHOW_profile_t profile = {side_left, 2, false};
	tRectangle sTemp;
	

	if(SettingMenu_widget.parameter == 0){
		LCD_display(SETTING,SettingMenu_widget.index);
		DrawPageInfo(SettingMenu_widget.index,6,4);        // 7>6
	}
	else{
		sTemp = LCD_display(VERSION,0);
		char buf[25] = {0X00};
		char FLS_ver[25] = {0X00};
		
		//snprintf(buf,sizeof(buf),"FW: %s",strFWver+7);
		snprintf(buf,sizeof(buf),"FW:\n%s",strFWver);
		UTF8_LCM_show(2,sTemp.i16YMax+4, buf, 125, &profile);
		
		A25L032ReadData(0, (uint8_t*)buf, sizeof(buf));
		//snprintf(FLS_ver,sizeof(FLS_ver),"FLS: %s",buf+7);
		snprintf(FLS_ver,sizeof(FLS_ver),"FLS:\n%s",buf);
		UTF8_LCM_show(2,profile.region.y2+4, FLS_ver, 125, &profile);
	}

}





