//*****************************************************************************
//
// Source File: TimeZoneWidget.c
// Description: select/set time zone
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 03/22/18     Jerry.W        Created file.
// 09/11/18     Henry.Y       Use systeminfo data struct to replace the global variables.
// 11/19/18     Henry.Y        Up/down button add combo operation.
// 12/07/18     Henry.Y        New display api.
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_FCA/V00/app/TimeZoneWidget.c#2 $
// $DateTime: 2018/03/22 17:13:13 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#include "Widgetmgr.h"



int16_t Time_zone_local;

widget* TimeZone_sub_W[] = {

};


void TimeZone_display();
void TimeZone_SELECT();
void TimeZone_RIGHT();
void TimeZone_LEFT();
void TimeZone_INIT();

widget TimeZone_widget = {
		.Key_UP = 0,
		.Key_DOWN = 0,
		.Key_RIGHT = TimeZone_RIGHT,
		.Key_LEFT = TimeZone_LEFT,
		.Key_SELECT = TimeZone_SELECT,
		.routine = 0,
		.service = 0,
		.initial = TimeZone_INIT,
		.display = TimeZone_display,
		.subwidget = TimeZone_sub_W,
		.number_of_widget = sizeof(TimeZone_sub_W)/4,
		.index = 0,
		.name = "TimeZone",
		.Combo = Combo_right | Combo_left,
};

void TimeZone_INIT(){
	SYS_INFO* info = Get_system_info();
	Time_zone_local = info->Time_zone;
}

void show_value(){
	//SHOW_profile_t profile = {side_middle, 2, false};
	char UTC[20];
	sprintf(UTC, "UTC %+3d:%02d", Time_zone_local/60,(Time_zone_local> 0)? Time_zone_local%60 : (Time_zone_local*(-1))%60 );
	if(Time_zone_local < 0 && Time_zone_local / 60 == 0){
		char *temp = strstr(UTC, "+");
		*temp = '-';
	}
	LCD_String_ext(32,UTC , Align_Central);
}

void TimeZone_display(){
    ST7565ClearScreen(BUFFER1);
    ST7565Refresh(BUFFER1);

    //LCD_String_ext(16,get_Language_String(S_TIME_ZONE) , Align_Central);

    LCD_Arrow(Arrow_enter | Arrow_RL);

	show_value();
}

void TimeZone_SELECT(){
	SYS_INFO* info = Get_system_info();
	info->Time_zone = Time_zone_local;
	save_system_info(&info->Time_zone, sizeof(info->Time_zone));
	Time_adjust(info->Time_zone);
	Return_pre_widget();
}

void TimeZone_RIGHT(){
	
	Time_zone_local += 15;
	if(Time_zone_local > 12*60){
		Time_zone_local = -11*60;
	}
	show_value();
}

void TimeZone_LEFT(){
	
	Time_zone_local -= 15;
	if(Time_zone_local < -11*60){
		Time_zone_local = 12*60;
	}
	show_value();
}



