//*****************************************************************************
//
// Source File: Battypeselwidget.c
// Description: Battery Type Select widget including Click(or Right, push to new
//				menu entry to stack)/Left(Pop out stack)/Up/Down function
//				definition.
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 09/17/15     William.H      Created file.
// 10/01/15     William.H      Re-arrange/remove Global varaible's usage due to we've used stack to behave the manu level selection.
//                             Respective global varaible by the respective widget file to be maintained by their own.
// 10/05/15     William.H      Check in BT2020 project which branched from LCDFontMaker(Pre-BT2020) by modifed related GPIOs.
// 10/22/15     Henry.Y        Avoid the unexpected situation when battery test begins, we use MenuLvlStack_ClrStack and MenuLvlStack_Push
//                             in BATTypeSelLeft(uint8_t menuIdx) if user want back to Main Menu.
// 01/25/16     Vincent.T      1. if battery type is changed, then reset battery test parameters.
//                             2. global variables g_BatteryType, g_SelectRating, g_PrintResult
//                             3. global variables g_SetCapacityVal, g_JISCapSelect
//                             4. #include "../service\eepromsvc.h"
// 01/28/16     Vincent.T      1. global variable g_SSSelectRating
//                             2. global variable WHICH_TEST
//                             3. New SSBATTypeSelAntiWhiteItem(); body
//                             4. #include "../service\testrecordsvc.h"
// 01/29/16     Vincent.T      1. global variables: g_SSBatteryType, g_BTSetCapacityVal, g_SSSetCapacityVal
// 03/25/16     Vincent.T      1. Modify SSBATTypeSelAntiWhiteItem(); to handle multination language case.
// 03/28/16     Vincent.T      1. Modify BATTypeSelLeft(); to handle multination language case.
//                             2. New global variable g_LangSelectIdx to get language index.
//                             3. Modify BATTypeSelClick(); to handle multination language case.
// 01/19/17     Vincent.T      1. Modify BATTypeSelAntiWhiteItem(); and SSBATTypeSelAntiWhiteItem(); for 16x16 characters.
//                             2. Modify BATTypeSelClick(); to maintain same setting while battery type is changed.
// 02/10/17     Vincent.T      1. Modify BATTypeSelClick(); to maintain same setting while battery type is changed.
// 09/21/17 	Jerry.W		   Rewrite Widget for new software architecture
// 09/11/18     Henry.Y        Use systeminfo data struct to replace global variables.
// 12/07/18     Henry.Y        New display api.
// 20201203     DavidWang      Modify screen same to BT2010
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/app/Battypeselwidget.c#6 $
// $DateTime: 2017/09/22 11:17:08 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#include "Widgetmgr.h"
#define DEFAULT_BTYPE VRLA_GEL
#define DEFAULT_STYPE AGM_FLAT_PLATE


void BatterySel_init(void);

void BatterySel_SELECT();
void BatterySel_RIGHT();
void BatterySel_LEFT();
void BatterySel_display();
void BatterySel_UP();

static TYPE_TEXT_T BT_type[] = {{FLOODED, S_FLOODED},
                                   {AGM_FLAT_PLATE, S_AGM_FLAT_PLATE},
                                   {AGM_SPIRAL, S_AGM_SPIRAL},
                                   {VRLA_GEL, S_VRLA_GEL},
                                   {EFB, S_START_STOP}};
                         
static TYPE_TEXT_T SS_type[] = {{AGM_FLAT_PLATE, S_SS_AGM},
                                   {EFB, S_EFB}};

widget* BatterySel_sub_W[] = {
		&Selrating_widget
};

widget BatterySel_widget = {
		.Key_UP = BatterySel_UP,
		.Key_DOWN = 0,
		.Key_RIGHT = BatterySel_RIGHT,
		.Key_LEFT = BatterySel_LEFT,
		.Key_SELECT = BatterySel_SELECT,
		.routine = 0,
		.service = 0,
		.display = BatterySel_display,
		.initial = BatterySel_init,
		.subwidget = BatterySel_sub_W,
		.number_of_widget = sizeof(BatterySel_sub_W)/4,
		.index = 0,
		.name = "BatterySel",
};

uint32_t get_batype_str_idx(uint8_t testype, uint8_t batype)
{
    TYPE_TEXT_T* type_array;
    uint8_t idx;
    uint8_t i,array_size;
        
    if(testype == Battery_Test){
        type_array = BT_type;
        array_size = sizeof(BT_type)/sizeof(BT_type[0]);
    }
    else if(testype == Start_Stop_Test){
        type_array = SS_type;
        array_size = sizeof(SS_type)/sizeof(SS_type[0]);
    }
    else{
        while(1);
    }
    
    for(i = 0; i < array_size; i++){
        if(type_array[i].type == batype){
            idx = i;
            break;
        }
    }
    return type_array[idx].text_index;
}

void BatterySel_init(void)
{
    SYS_PAR* par = Get_system_parameter();
    TEST_DATA* TD = Get_test_data();
    uint8_t batype_size = sizeof(BT_type)/sizeof(BT_type[0]);
    if(TD->Isa6VBattery) batype_size -= 1;
        
    uint8_t i;
    for(i = 0; i < batype_size; i++){
        if(BT_type[i].type == DEFAULT_BTYPE){
            BatterySel_widget.index = i;
        }
        if(BT_type[i].type == par->BAT_test_par.BatteryType){
            BatterySel_widget.index = i;
            break;
        }
    } 
}

void BatterySel_display(){
    SYS_PAR* par = Get_system_parameter();
    ST7565ClearScreen(BUFFER1);
    ST7565Refresh(BUFFER1);

    if(!BatterySel_widget.parameter){
        LCD_String_ext(16,get_Language_String(S_BATTERY_TYPE) , Align_Left);
        LCD_String_ext(32, get_Language_String(BT_type[BatterySel_widget.index].text_index), Align_Central);
        LCD_Arrow(Arrow_enter | Arrow_RL);
    }
    else{
        LCD_String_ext(16,get_Language_String(S_SS_BAT_TYPE) , Align_Left);
        LCD_String_ext(32, get_Language_String(SS_type[BatterySel_widget.index].text_index), Align_Central);
        LCD_Arrow(Arrow_enter | Arrow_RL); // | Arrow_up
    }
}

void BatterySel_UP()
{
    uint8_t i,batype_size = sizeof(BT_type)/sizeof(BT_type[0]);
    if(BatterySel_widget.parameter){
        for(i = 0; i < batype_size; i++){
            if(BT_type[i].type == EFB){
                BatterySel_widget.index = i;
                break;
            }
        }
        BatterySel_widget.parameter = 0;
        BatterySel_display();
    }
}

void BatterySel_LEFT()
{
    SYS_PAR* par = Get_system_parameter();
    TEST_DATA* TD = Get_test_data();

    uint8_t batype_size;
    if(!BatterySel_widget.parameter){
        batype_size = sizeof(BT_type)/sizeof(BT_type[0]);
        if(TD->Isa6VBattery) batype_size -= 1;
    }
    else{
        batype_size = sizeof(SS_type)/sizeof(SS_type[0]);
    }
    BatterySel_widget.index = (!BatterySel_widget.index)? batype_size-1:BatterySel_widget.index-1;
    BatterySel_display();
}

void BatterySel_RIGHT()
{
    SYS_PAR* par = Get_system_parameter();
    TEST_DATA* TD = Get_test_data();
    
    uint8_t batype_size;
    if(!BatterySel_widget.parameter){
        batype_size = sizeof(BT_type)/sizeof(BT_type[0]);
        if(TD->Isa6VBattery) batype_size -= 1;
    }
    else{
        batype_size = sizeof(SS_type)/sizeof(SS_type[0]);
    }
    BatterySel_widget.index = (BatterySel_widget.index + 1)%batype_size;
    BatterySel_display();
}

void BatterySel_SELECT()
{
	SYS_PAR* par = Get_system_parameter();
	TEST_DATA* TD = Get_test_data();
    uint8_t batype_size;
    if(!BatterySel_widget.parameter){
        par->BAT_test_par.BatteryType = BT_type[BatterySel_widget.index].type;
        if(BT_type[BatterySel_widget.index].type == EFB){
            BatterySel_widget.parameter = 1;
            batype_size = sizeof(SS_type)/sizeof(SS_type[0]);

            uint8_t i;
            for(i = 0; i < batype_size; i++){
                if(SS_type[i].type == DEFAULT_STYPE){
                    BatterySel_widget.index = i;
                }
                if(SS_type[i].type == par->SS_test_par.SSBatteryType){
                    BatterySel_widget.index = i;
                    break;
                }
            }
            BatterySel_display();
            return;
        }
        else{
            par->WHICH_TEST = Battery_Test;
        }
    }
    else{
        par->WHICH_TEST = Start_Stop_Test;
        par->SS_test_par.SSBatteryType = SS_type[BatterySel_widget.index].type;
    }

    Move_widget(BatterySel_sub_W[0],0);
}


