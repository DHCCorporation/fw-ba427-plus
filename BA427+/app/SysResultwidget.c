//*****************************************************************************
//
// Source File: SysResultwidget.h
// Description: System test result widget including Click(Push to new menu entry to stack)/
//				Right/Left(Pop out stack)/Up/Down function definition.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 02/05/16     Vincent.T      Created file.
// 03/31/16     Vincent.T      1. Modify SysResultClick(); to handle multination language case.
// 09/21/17 	Jerry.W		   Rewrite Widget for new software architecture
// 03/23/18     Henry.Y        modify next widget to NumberVKBd_widget(VIN input)
// 09/11/18     Henry.Y        Add SysResult_LEFT.
// 10/23/18     Henry.Y        Make the processing graph only show one time at the widget begins.
// 12/07/18     Henry.Y        New display api.
// 08/11/20     David.Wang     1. add save record BATTERY_SARTSTOP_RECORD_T record
//                             2. Skip VIN CODE WIDGET TO PRINT WIDGET
//
// ============================================================================
//
//
// $Header$
// $DateTime$
// $Author$
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#include "Widgetmgr.h"


void SysResult_result();

void SysResult_Select();
void SysResult_UP_DOWN();

void SysResult_display();
void SysResult_Initial();
void SysResult_LEFT();


widget* SysResult_sub_W[] = {
	&NumberVKBd_widget,
	&AbortTest_widget,
	&PrintResult_widget,        // 20200811 DW EDIT FOR BT2200_HD_BO)USA
};

widget SysResult_widget = {
		.Key_UP = SysResult_UP_DOWN,
		.Key_DOWN = SysResult_UP_DOWN,
		.Key_RIGHT = 0,
		.Key_LEFT = SysResult_LEFT,
		.Key_SELECT = SysResult_Select,
		.routine = 0,
		.service = 0,
		.initial = SysResult_Initial,
		.display = SysResult_display,
		.subwidget = SysResult_sub_W,
		.number_of_widget = sizeof(SysResult_sub_W)/4,
		.index = 0,
		.no_messgae = 1,
		.name = "SysResult",
};
void SysResult_LEFT(){
	Move_widget(SysResult_sub_W[1], 0);
}

void SysResult_Select(){
	Move_widget(SysResult_sub_W[2], 0);     // 20200811 DW EDIT  SysResult_sub_W[0] >>  SysResult_sub_W[1]
}

void SysResult_UP_DOWN(){
	SysResult_widget.index ^= 1;
	SysResult_result();
}

void SysResult_Initial(){
	SYS_INFO* info = Get_system_info();
	TEST_DATA* TD = Get_test_data();

	SysResult_widget.index = 0;

    SYS_PAR* par = Get_system_parameter();

    //*****************************
	// Increase Test Counter
	//*****************************
	counter_increase(&info->SysTestCounter);
	save_system_info(&info->SysTestCounter, sizeof(info->SysTestCounter));	
	ExtRTCRead( &TD->year, &TD->month, &TD->date, &TD->day, &TD->hour, &TD->min, &TD->sec);

	//****************************
	// SYSTEM TEST RECORD SAVE
	//****************************

}



void SysResult_display(){
	uint8_t i;
	if(!SysResult_widget.parameter){
		for(i=0;i<4;i++){			
			LCD_display(TESTING_PROCESSING_ARROW,i&0x01);
			delay_ms(500);
		}
		SysResult_widget.parameter = 1;
	}

	SysResult_result();
}


void SysResult_result(){
	tRectangle sTemp;
	
	sTemp = LCD_display(SYS_TEST_RESULT,0);
	
	DrawPageInfo(SysResult_widget.index, 2, 1);
	LCD_draw_string(SYS_TEST_RESULT, SysResult_widget.index, sTemp.i16YMax+4);	
}


