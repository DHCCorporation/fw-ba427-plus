/*
 * ROVKwidget.c
 *
 *  Created on: 2020�~12��23��
 *      Author: henry.yang
 */
#include "Widgetmgr.h"
#define SEL_DEFAULT 1

void RO_DISPLAY();
void RO_UP();
void RO_DOWN();
void RO_LEFT();
void RO_RIGHT();
void RO_SELECT();
void RO_Routine();
void RO_initail();

widget* RO_sub_W[] = {
		&VIN_widget,
};

widget RO_widget = {
		.Key_UP = RO_UP,
		.Key_DOWN = RO_DOWN,
		.Key_RIGHT = RO_RIGHT,
		.Key_LEFT = RO_LEFT,
		.Key_SELECT = RO_SELECT,
		.routine = RO_Routine,
		.service = 0,
		.initial = RO_initail,
		.display = RO_DISPLAY,
		.subwidget = RO_sub_W,
		.number_of_widget = sizeof(RO_sub_W)/4,
		.index = 0,
		.name = "RO_VK",
};

static uint8_t str_update = false;

void RO_cb(char* data,uint8_t len)
{
	SYS_PAR* par = Get_system_parameter();
    str_update = true;
    memset(par->RO_str,0,sizeof(par->RO_str));
    memcpy(par->RO_str, data, len);
    //UARTprintf("RO_cb: %s\n", data);
}

void RO_initail(void)
{
    str_update = false;
    RO_widget.index = SEL_DEFAULT;
}

void RO_DISPLAY(void)
{
    if(!RO_widget.parameter){
        ST7565ClearScreen(BUFFER1);
        ST7565Refresh(BUFFER1);
        //LCD_String_ext(16, get_Language_String(S_ADD_RO), Align_Left);
        LCD_String_ext2(32, get_Language_String((RO_widget.index)? S_YES:S_NO), Align_Right, 2);
        LCD_Arrow(Arrow_enter | Arrow_RL);    
    }
    else{
        VK_char_num_display();
    }
}

void RO_Routine(void)
{
    static uint16_t delay = 0;
    if(RO_widget.parameter){
        if(str_update){            
            Move_widget(RO_sub_W[0],0);
            return;
        }
        delay = (delay+1)%CURSOR_BLINK_T;
        if(!delay){
            VK_CN_Cursor_blink();
        }
    }
}

void RO_UP(void)
{
    if(RO_widget.parameter){
        VK_char_num_up();
    }
}

void RO_DOWN(void)
{
    if(RO_widget.parameter){
        VK_char_num_down();
    }
}

void RO_LEFT(void)
{
    if(!RO_widget.parameter){        
        RO_widget.index^=1;
        RO_DISPLAY();
    }
    else{
        VK_char_num_left();
    }
}

void RO_RIGHT(void)
{
    if(!RO_widget.parameter){
        RO_widget.index^=1;
        RO_DISPLAY();
    }
    else{
        VK_char_num_right();
    }
    
}

void RO_SELECT(void)
{
    SYS_PAR* par = Get_system_parameter();
    if(!RO_widget.parameter){
        if(RO_widget.index){
            RO_widget.parameter = 1;
            VK_INFO_T info = {//.title_str = get_Language_String(S_ENTER_RO),
                              .underline = true,
                              .start_x = 0,
                              .start_y = 20,
                              .char_width = 7,
                              .len = sizeof(par->RO_str),
                              .arr_flag = (Arrow_enter | Arrow_RL),// | ARROW_UP_DOWN
                              .cb = RO_cb,};
            VK_char_num_init(&info);
            RO_DISPLAY();
        }
        else{
            memset(par->RO_str, 0, sizeof(par->RO_str));
            Move_widget(RO_sub_W[0],0);
        }
    }
    else{
        VK_char_num_sel();
    }
}


