//*****************************************************************************
//
// Source File: Selratingwidget.c
// Description: Select Rating widget including Click(or Right, push to new menu
//				entry to stack)/Left(Pop out stack)/Up/Down function definition.
//				
//
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 09/17/15     William.H      Created file.
// 10/01/15     William.H      Re-arrange/remove Global varaible's usage due to we've used stack to behave the manu level selection.
//                             Respective global varaible by the respective widget file to be maintained by their own.
// 10/05/15     William.H      Check in BT2020 project which branched from LCDFontMaker(Pre-BT2020) by modifed related GPIOs.
// 10/22/15     William.H      1. Change list order of 'SAE' and 'DIN' in Select Rating Widget.
//                             2. To load each 'Set Capacity' screen separately based on the selection result of 'Select Rating'. i.e. SAE(SelRatingWWidget) -> SAE(SetCapWidget)...etc.
// 10/27/15     William.H      Change 'SAE' to 'CCA / SAE' in Select Rating Widget. The reverse region needs to modify. X-axis: 0 ~ 55, Y-axis: 10 ~ 19.
// 11/04/15     Vincent.T      1. Add #include Setcapwidget.h
//                             2. Set g_MaxCCASet Depends on battery type
//                             3. Add #include battcoresvc.h
// 11/05/15     Vincent.T      1. Change g_SetCapacityVal when CCA rating is changed.
//                             2. New local variabls to get cca rating last time
// 01/25/16     Vincent.T      1. Modify part code to get right set cca val. conversion.(after memory functions are implemented)
//                             2. #include "..\service/eepromsvc.h"
// 01/28/16     Vincent.T      1. global variable WHICH_TEST
//                             2. #include "../service\testrecordsvc.h"
//                             3. Modify SelRatingLeft(); for start-stop test
//                             4. global variable g_SSBatteryType
//                             5. JIS global variable: pg_JIS, g_JISSSBatType, NUM_JIS_BATTERY_TYPE
// 01/29/16     Vincent.T      1. global variables for JIS: NUM_JIS_BT, NUM_JIS_SS
//                             2. global variables g_BTJISCapSelect, g_SSJISCapSelect
//                             3. global variables g_SelectRating, g_SSSelectRating
//                             4. global variables: g_BTSetCapacityVal, g_SSSetCapacityVal(To seperate SS, BT)
// 03/25/16     Vincent.T      1. Modify SelRatingLeft(); to handle multination language case.
// 03/28/16     Vincent.T      1. Modify SelRatingAntiWhiteItem(); to handle multination language case.
//                             2. Modify SelRatingClick(); to handle multination language case.
//                             3. Modify SelRatingLeft(); to handke multination language case.
// 06/14/16     Vincent.T      1. Modify different cca max. capacity value between cca ratings.
// 06/24/16     Vincent.T      1. Modify JISDLlist_Search_by_ItemNum(); input parameters.
// 01/13/17     Vincent.T      1. Modify SelRatingClick(); CCA every step form 1 to 5;
// 01/19/17     Vincent.T      1. Modify SelRatingAntiWhiteItem(); for 16x16 characters.
// 09/21/17 	Jerry.W		   Rewrite Widget for new software architecture
// 11/22/17     Henry.Y        Set #define of CCA max and min. value.
// 05/03/18     Henry.Y        add EN1 and EN2.
// 12/07/18     Henry.Y        New display api.
// 20201203     D.W.           Modify screen same to BT2010
//
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/app/Selratingwidget.c#1 $
// $DateTime: 2017/11/27 10:38:22 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#include "Widgetmgr.h"

void Selrating_SELECT();
void Selrating_UP();
void Selrating_LEFT();
void Selrating_RIGHT();
void Selrating_display();
void Selrating_initial();

static TYPE_TEXT_T sel_rating[] = {{DIN, S_DIN},
                                        {IEC, S_IEC},
                                        {SAE, S_CCA},
                                        {MCA, S_MCA},
                                        {CA_MCA, S_CA},
                                        {EN2, S_EN},
                                        {JIS, S_JIS}};


widget* Selrating_sub_W[] = {
		&SetCapNormal_widget,
		&JISCapSel_widget
};

widget Selrating_widget = {
		.Key_UP = Selrating_UP,
		.Key_DOWN = 0,
		.Key_RIGHT = Selrating_RIGHT,
		.Key_LEFT = Selrating_LEFT,
		.Key_SELECT = Selrating_SELECT,
		.routine = 0,
		.service = 0,
		.display = Selrating_display,
		.initial = Selrating_initial,
		.subwidget = Selrating_sub_W,
		.number_of_widget = sizeof(Selrating_sub_W)/4,
		.index = 0,
		.name = "Selrating",
};
        
uint32_t get_rating_str_idx(uint8_t rating)
{
    uint8_t idx;
    uint8_t i,array_size = sizeof(sel_rating)/sizeof(sel_rating[0]);
    
    for(i = 0; i < array_size; i++){
        if(sel_rating[i].type == rating){
            idx = i;
            break;
        }
    }
    return sel_rating[idx].text_index;
}

void Selrating_initial(){
    SYS_PAR* par = Get_system_parameter();
    TEST_DATA* TD = Get_test_data();
    uint8_t rating_size = sizeof(sel_rating)/sizeof(sel_rating[0]);
   
    if(par->WHICH_TEST == Start_Stop_Test || \
        (par->WHICH_TEST == Battery_Test && TD->SYSTEM_CHANNEL == MAP_6V)) {
        rating_size -= 1;
    }
    
    uint8_t i;
    for(i = 0; i < rating_size; i++){
        Selrating_widget.index = i;
        if(sel_rating[i].type == par->BAT_test_par.SelectRating) break;
    }
}

void Selrating_display()
{
    SYS_PAR* par = Get_system_parameter();
    TEST_DATA* TD = Get_test_data();

    ST7565ClearScreen(BUFFER1);
    ST7565Refresh(BUFFER1);
    LCD_String_ext(16,get_Language_String(S_SELECT_RATING) , Align_Left);

    char rating_display[4+1] = {0};
    uint16_t target_size = strlen(get_Language_String(sel_rating[Selrating_widget.index].text_index));
    if(target_size > sizeof(rating_display)-1){// just in case
        while(1);
    }
    memset(rating_display, ' ', sizeof(rating_display)-1);
    memcpy(rating_display, get_Language_String(sel_rating[Selrating_widget.index].text_index), target_size);
    LCD_String_ext(32, rating_display, Align_Right);
    LCD_Arrow(Arrow_enter | Arrow_RL);// | Arrow_up
}

void Selrating_LEFT(){
    SYS_PAR* par = Get_system_parameter();
    TEST_DATA* TD = Get_test_data();
    uint8_t rating_size = sizeof(sel_rating)/sizeof(sel_rating[0]);
   
    if(par->WHICH_TEST == Start_Stop_Test || \
        (par->WHICH_TEST == Battery_Test && TD->SYSTEM_CHANNEL == MAP_6V)) {
        rating_size -= 1;
    }
    Selrating_widget.index = (!Selrating_widget.index)? rating_size-1:Selrating_widget.index-1;
    Selrating_display();
}

void Selrating_RIGHT(){
	SYS_PAR* par = Get_system_parameter();
	TEST_DATA* TD = Get_test_data();
    uint8_t rating_size = sizeof(sel_rating)/sizeof(sel_rating[0]);
   
    if(par->WHICH_TEST == Start_Stop_Test || \
        (par->WHICH_TEST == Battery_Test && TD->SYSTEM_CHANNEL == MAP_6V)) {
        rating_size -= 1;
    }
    Selrating_widget.index = (Selrating_widget.index + 1)%rating_size;
    Selrating_display();
}

void Selrating_UP(){
	Return_pre_widget();
}

void Selrating_SELECT(){
	SYS_PAR* par = Get_system_parameter();
    par->BAT_test_par.SelectRating = sel_rating[Selrating_widget.index].type;
    par->BAT_test_par.SelectRating_last = par->BAT_test_par.SelectRating;
    if(par->WHICH_TEST == Start_Stop_Test) {
        par->SS_test_par.SSSelectRating = par->BAT_test_par.SelectRating;
        par->SS_test_par.SSSelectRating_last = par->SS_test_par.SSSelectRating;
    }
    if(sel_rating[Selrating_widget.index].type != JIS){
        Move_widget(Selrating_sub_W[0],0);
    }
    else{
        Move_widget(Selrating_sub_W[1],0);
    }
}





