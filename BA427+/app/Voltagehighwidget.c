//*****************************************************************************
//
// Source File: Voltagehighwidget.c
// Description: Enter battery test and extern voltage is too high. There will
//					pop up "Voltage High, please click OK back to the menu.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 10/21/15     Henry.Y	       Created file.
// 03/30/16     Vincent.T      1. VoltageHighClick(); to handle multination language case.
// 09/21/17 	Jerry.W		   Rewrite Widget for new software architecture
// 12/07/18     Henry.Y        New display api.
// 09/09/20     David.Wang     Enter key change to left key. when return to main menu
// 20201203     DW             MODIFY SCREEN SAME TO BT2010
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/app/Voltagehighwidget.c#6 $
// $DateTime: 2017/09/22 11:23:12 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#include "Widgetmgr.h"

void VoltageHigh_LEFT();
void VoltageHigh_display();

widget* VOLTAGEHIGH_sub_W[] = {
        NULL,
};

widget VOLTAGEHIGH_widget = {
		.Key_UP = 0,
		.Key_DOWN = 0,
		.Key_RIGHT = 0,
		.Key_LEFT = 0,
		.Key_SELECT = 0,
		.routine = 0,
		.service = 0,
		.display = VoltageHigh_display,
		.subwidget = 0,
		.number_of_widget = sizeof(VOLTAGEHIGH_sub_W)/4,
		.index = 0,
		.no_messgae = 1,
		.name = "VOLTAGEHIGH",
};

void VoltageHigh_display(){

    ST7565ClearScreen(BUFFER1);
    ST7565Refresh(BUFFER1);

	LCD_String_ext(16, get_Language_String(S_VOLTAGE_HIGH), Align_Central);
}
