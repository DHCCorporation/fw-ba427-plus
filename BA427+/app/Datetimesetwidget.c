//*****************************************************************************
//
// Source File: Datetimesetwidget.c
// Description: Functions definition to control Date and Time setting.
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 12/07/15     Vincent.T      Created file.
// 01/12/16     Vicnent.T      Modify data/time coordinates
// 01/20/16     Vincent.T      Bug fixed when clock IC is failed to read.
// 02/01/16     Vincent.T      ExtRTCSaved(); Saved Completly! -> OK!
// 02/19/16     Vincent.T      1. Add animation.
// 03/26/16     Vincent.T      1. Modify ExtRTCShow(); to handle multination language case.
//                             2. Modify ExtRTCShowDateTime(); to handle multination language case.
//                             3. Modify ExtRTCAntiWhiteItem(); to handel multination language case.
// 06/26/16     Vincent.T      1. Modify antiwhite block for data, time setting selection(for better looking)
// 11/15/16     Ian.C          1. Modify ExtRTCShow(); Change the position of date & time for multi-language.
// 12/08/16     Vincent.T      1. Modify ExtRTCAntiWhiteItem(); to correct antiwhite items.
// 01/19/17     Vincent.T      1. Modify ExtRTCAntiWhiteItem(); for 16x16 characters.
// 09/21/17 	Jerry.W		   Rewrite Widget for new software architecture
// 03/22/18     Jerry.W        temporary mask Key_UP,Key_DOWN,Key_RIGHT and ExtRTCSaved() in DateTimeSet_SELECT() for FCA sample.
// 12/07/18     Henry.Y        New display api. Disable select action.
// 08/04/20     David.Wang     wifi update changed to manual update
// 08/05/20     David.Wang     Clock time rtc to manual operation
// ============================================================================
//
//
// $Header:   $
// $DateTime: $
// $Author:   $
//
//
// Copyright (c) 2020 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#include "Widgetmgr.h"
#include "Datetimesetwidget.h"

int8_t DateTimeIdx = 0;
char buf[12] = {0};
uint16_t ui16DatBuf[6] = {0x00};


void DateTimeSet_RIGHT();     // 20200804 DW EDIT
void DateTimeSet_LEFT();
void DateTimeSet_SELECT();    // 20200804 DW EDIT
void DateTimeSet_display();
void DateTimeSet_initial();   // 20200804 DW EDIT
void DateTimeSet_Routine();


widget* DateTimeSet_sub_W[] = {
};

widget DateTimeSet_widget = {
		.Key_UP = 0,                          // 20200804 DW EDIT
		.Key_DOWN = 0,                        // 20200804 DW EDIT
		.Key_RIGHT = DateTimeSet_RIGHT,       // 20200804 DW EDIT
		.Key_LEFT = DateTimeSet_LEFT,
		.Key_SELECT = DateTimeSet_SELECT,      // 20200804 DW EDIT
		.routine = DateTimeSet_Routine,
		.service = 0,
		.display = DateTimeSet_display,
		.initial = DateTimeSet_initial,                    //20190618 DW ADD NEW
		.subwidget = DateTimeSet_sub_W,
		.number_of_widget = sizeof(DateTimeSet_sub_W)/4,
		.index = 0,
		.name = "DateTimeSet",
		.Combo = Combo_left | Combo_right,
};

void DateTimeSet_initial(){
    DateTimeIdx = -1;
    DateTimeSet_widget.parameter = 1;
    TEST_DATA* TD = Get_test_data();
    ExtRTCRead(  &TD->year, &TD->month, &TD->date, &TD->day, &TD->hour, &TD->min, &TD->sec );
    //***************************
    // Date Coellection
    //***************************
    ui16DatBuf[0] = TD->year;
    ui16DatBuf[1] = TD->month;
    ui16DatBuf[2] = TD->date;
    ui16DatBuf[3] = TD->hour;
    ui16DatBuf[4] = TD->min;
    ui16DatBuf[5] = TD->sec;
}

void DateTimeSet_RIGHT(){
    if(DateTimeIdx < 0) return;
    DateTimeUp(ui16DatBuf, DateTimeIdx);
    DateTimeSet_display();
}

void DateTimeSet_LEFT(){
    if(DateTimeIdx < 0) return;
    DateTimeDown(ui16DatBuf, DateTimeIdx);
    DateTimeSet_display();
}

void DateTimeSet_SELECT(){

    if(DateTimeIdx < 5)
    {
        DateTimeIdx++;
        DateTimeSet_display();
    }
    else{
        ExtRTCSaved(ui16DatBuf);
        Return_pre_widget();
    }
}


void DateTimeSet_display(){
    char date[20];

    ST7565ClearScreen(BUFFER1);
    ST7565Refresh(BUFFER1);

    LCD_Arrow(Arrow_enter);

    sprintf(date, "%d/%02d/%02d  ", ui16DatBuf[0], ui16DatBuf[1], ui16DatBuf[2]);
    LCD_String_ext(16,date , Align_Central);
    sprintf(date, "%02d:%02d:%02d", ui16DatBuf[3], ui16DatBuf[4], ui16DatBuf[5]);
    LCD_String_ext(32,date , Align_Central);
    if(DateTimeIdx < 0) return;
    uint32_t s_lit[] = {S_ADJUST_YEAR, S_ADJUST_MONTH, S_ADJUST_DAY, S_ADJUST_HOUR, S_ADJUST_MINUTE, S_ADJUST_SECOND};
    LCD_String_ext(48,get_Language_String(s_lit[DateTimeIdx]) , Align_Left);
    LCD_Arrow(Arrow_enter|Arrow_RL);
}

void DateTimeSet_Routine(){
    static uint32_t counter = 0;

    if(DateTimeIdx < 0){
        counter++;
        if(counter > 20){
            counter = 0;
            TEST_DATA* TD = Get_test_data();
            ExtRTCRead(  &TD->year, &TD->month, &TD->date, &TD->day, &TD->hour, &TD->min, &TD->sec );
            ui16DatBuf[0] = TD->year;
            ui16DatBuf[1] = TD->month;
            ui16DatBuf[2] = TD->date;
            ui16DatBuf[3] = TD->hour;
            ui16DatBuf[4] = TD->min;
            ui16DatBuf[5] = TD->sec;
            DateTimeSet_display();
        }
    }
}

void DateTimeDown(uint16_t *pui16Dat, uint8_t DateTimeIdx)
{
    pui16Dat[DateTimeIdx] = pui16Dat[DateTimeIdx] - 1;
    switch(DateTimeIdx)
    {
    case 0:
        //
        // year
        //
        if(pui16Dat[0] < 2020)
        {
            pui16Dat[0] = 2050;
        }
        break;
    case 1:
        //
        // month
        //
        if(pui16Dat[1] < 1)
        {
            pui16Dat[1] = 12;
        }
        else if(pui16Dat[1] > 12)
        {
            pui16Dat[1] = 1;
        }
        break;
    case 2:
        //
        // date
        //
        if(pui16Dat[2] < 1)
        {
            pui16Dat[2] = 31;
        }

        break;
    case 3:
        //
        // hour
        //
        if(pui16Dat[3] == 0xFFFF)
        {
            pui16Dat[3] = 23;
        }
        break;
    case 4:
        //
        // min
        //
        if(pui16Dat[4] == 0xFFFF)
        {
            pui16Dat[4] = 59;
        }
        break;
    case 5:
        //
        // sec
        //
        if(pui16Dat[5] == 0xFFFF)
        {
            pui16Dat[5] = 59;
        }
        break;
    default:
        break;
    }

    //*************
    // Date Check
    //*************
    if(((pui16Dat[0] % 4) == 0) && (pui16Dat[1] == 2))
    {
        if(pui16Dat[2] > 29)
        {
            pui16Dat[2] = 29;
        }
    }
    else if(pui16Dat[1] == 2)
    {
        if(pui16Dat[2] > 28)
        {
            pui16Dat[2] = 28;
        }
    }
    else if((pui16Dat[1] == 1) || (pui16Dat[1] == 3) || (pui16Dat[1] == 5) || (pui16Dat[1] == 7) || (pui16Dat[1] == 8) || (pui16Dat[1] == 10) || (pui16Dat[1] == 12))
    {
        if(pui16Dat[2] > 31)
        {
            pui16Dat[2] = 31;
        }
    }
    else
    {
        if(pui16Dat[2] > 30)
        {
            pui16Dat[2] = 30;
        }
    }
}

void DateTimeUp(uint16_t *pui16Dat, uint8_t DateTimeIdx)
{
    pui16Dat[DateTimeIdx] = pui16Dat[DateTimeIdx] + 1;
    switch(DateTimeIdx)
    {
    case 0:
        //
        // year
        //
        if(pui16Dat[0] > 2050)
        {
            pui16Dat[0] = 2020;
        }
        break;
    case 1:
        //
        // month
        //
        if(pui16Dat[1] > 12)
        {
            pui16Dat[1] = 1;
        }
        break;
    case 2:
        //
        // date
        //
        //*************
        // Date Check
        //*************
        if(((pui16Dat[0] % 4) == 0) && (pui16Dat[1] == 2))
        {
            if(pui16Dat[2] > 29)
            {
                pui16Dat[2] = 1;
            }
        }
        else if(pui16Dat[1] == 2)
        {
            if(pui16Dat[2] > 28)
            {
                pui16Dat[2] = 1;
            }
        }
        else if((pui16Dat[1] == 1) || (pui16Dat[1] == 3) || (pui16Dat[1] == 5) || (pui16Dat[1] == 7) || (pui16Dat[1] == 8) || (pui16Dat[1] == 10) || (pui16Dat[1] == 12))
        {
            if(pui16Dat[2] > 31)
            {
                pui16Dat[2] = 1;
            }
        }
        else
        {
            if(pui16Dat[2] > 30)
            {
                pui16Dat[2] = 1;
            }
        }

        break;
    case 3:
        //
        // hour
        //
        if(pui16Dat[3] > 23)
        {
            pui16Dat[3] = 0;
        }
        break;
    case 4:
        //
        // min
        //
        if(pui16Dat[4] > 59)
        {
            pui16Dat[4] = 0;
        }
        break;
    case 5:
        //
        // sec
        //
        if(pui16Dat[5] > 59)
        {
            pui16Dat[5] = 0;
        }
        break;
    default:
        break;
    }

    if(DateTimeIdx != 3)
    {
        //*************
        // Date Check
        //*************
        if(((pui16Dat[0] % 4) == 0) && (pui16Dat[1] == 2))
        {
            if(pui16Dat[2] > 29)
            {
                pui16Dat[2] = 29;
            }
        }
        else if(pui16Dat[1] == 2)
        {
            if(pui16Dat[2] > 28)
            {
                pui16Dat[2] = 28;
            }
        }
        else if((pui16Dat[1] == 1) || (pui16Dat[1] == 3) || (pui16Dat[1] == 5) || (pui16Dat[1] == 7) || (pui16Dat[1] == 8) || (pui16Dat[1] == 10) || (pui16Dat[1] == 12))
        {
            if(pui16Dat[2] > 31)
            {
                pui16Dat[2] = 31;
            }
        }
        else
        {
            if(pui16Dat[2] > 30)
            {
                pui16Dat[2] = 30;
            }
        }
    }
}

void ExtRTCSaved(uint16_t *ui16DatBuf)
{
    ExtRTCWrite(ui16DatBuf[0], ui16DatBuf[1], ui16DatBuf[2], 0, ui16DatBuf[3], ui16DatBuf[4], ui16DatBuf[5]);

}



