//*****************************************************************************
//
// Source File: Message_set_app.h
// Description: to manage the message set for each languages.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 11/30/18     Jerry.W        Created file.
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/driver/wifi_driver.h#2 $
// $DateTime: 2017/12/18 16:17:54 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#ifndef APP_MESSAGE_SET_APP_H_
#define APP_MESSAGE_SET_APP_H_
#include "../service/langaugesvc.h"

typedef enum{
	Software_available,
	Insert_VIN,
	Turn_on_WIFI,
	Download_complete,
	upgrade_available,
	Natwork_unstable,
	Software_up_to_date,
	Fail_to_cloud,
	Check_connection,
	Store_result,
	Uploading_result,
	Result_uploaded,
	Upload_failed,
	WIFI_connected_find_result,
	Memory_full,
	Data_uploaded,
	Data_seved,
	Regarge_time,
	For_accuracy,
	unregistered_device_plz_register,
	invalid_code,
	network_unstable_fail_to_register,
	register_fail_code,
	register_successfully,
	fail_to_upload_unregister,
	unregister_result_stored,
}Message_list_t;

void Message_by_language(Message_list_t item, uint32_t parameter, LANGUAGE lang);



#endif /* APP_MESSAGE_SET_APP_H_ */
