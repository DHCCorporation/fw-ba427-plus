//*****************************************************************************
//
// Source File: ResetCounterwidget.c
// Description: Handle no paper event when printing including Click(Push to new menu entry
//				to stack)/Left(Pop out stack)/Up/Down function body.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 03/09/16     Vincent.T      Created file.
// 03/31/16     Vincent.T      1. Modify ResetCounterClick(); to handle multination language case.
//                             2. Modify ResetCounterShow(); to handle multination language case.
// 06/14/16     Vincent.T      1. #include "TestCounterwidget.h"
//                             2. #include "Printresultwidget.h"
//                             3. global variables g_PrintResult and  ui8TestCounterYesNo
//                             4. Modify ResetCounterClick();(No paper event -> print Yes/NO)
// 09/21/17 	Jerry.W		   Rewrite Widget for new software architecture
// 11/23/17     Jerry.W        1. add functionality of countdown for printer mode to shut down
// 12/07/18     Henry.Y        New display api.
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/app/ResetCounterwidget.c#1 $
// $DateTime: 2017/11/27 10:38:22 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#include "Widgetmgr.h"

void ResetCounter_DISPLAY();
void ResetCounter_SELECT();
void ResetCounter_routine();
void ResetCounter_LEFT_RIGHT();
void ResetCounter_INIT();

widget* ResetCounter_sub_W[] = {
};

widget ResetCounter_widget = {
		.Key_UP = 0,
		.Key_DOWN = 0,
		.Key_RIGHT = ResetCounter_LEFT_RIGHT,
		.Key_LEFT = ResetCounter_LEFT_RIGHT,
		.Key_SELECT = ResetCounter_SELECT,
		.routine = 0,
		.service = 0,
		.initial = ResetCounter_INIT,
		.display = ResetCounter_DISPLAY,
		.subwidget = ResetCounter_sub_W,
		.number_of_widget = sizeof(ResetCounter_sub_W)/4,
		.index = 0,
		.name = "ResetCounter",
};

void ResetCounter_INIT(){
    ResetCounter_widget.index = 1;
}

void ResetCounter_LEFT_RIGHT(){
    ResetCounter_widget.index ^= 1;
    ResetCounter_DISPLAY();
}

void ResetCounter_SELECT()
{
    SYS_INFO* info = Get_system_info();
    if(ResetCounter_widget.index == 1){
        info->BatTestCounter = 0;
        info->SysTestCounter = 0;
        info->SSTestCounter = 0;
        save_system_info(&info->BatTestCounter, sizeof(info->BatTestCounter));
        save_system_info(&info->SysTestCounter, sizeof(info->SysTestCounter));
        save_system_info(&info->SSTestCounter, sizeof(info->SSTestCounter));
    }
	Return_pre_widget();
}

void ResetCounter_DISPLAY()
{
    ST7565ClearScreen(BUFFER1);
    ST7565Refresh(BUFFER1);

    LCD_Arrow(Arrow_enter | Arrow_RL);

    LCD_String_ext(16,get_Language_String(S_RESET) , Align_Central);
    if(ResetCounter_widget.index == 1){
        LCD_String_ext2(32,get_Language_String(S_YES) , Align_Right, 2);
    }
    else{
        LCD_String_ext2(32,get_Language_String(S_NO) , Align_Right, 2);
    }
}




