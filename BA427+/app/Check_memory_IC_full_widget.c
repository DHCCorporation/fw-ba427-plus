/*
 * Check_memory_IC_full_widget.c
 *
 *  Created on: 2020年12月23日
 *      Author: ray.yuan
 */

#include "Widgetmgr.h"

void ChkMemICFull_DISPLAY();
void ChkMemICFull_SELECT();

widget* ChkMemICFull_sub_W[] = {

};

widget ChkMemICFull_widget = {
        .Key_UP = 0,
        .Key_DOWN = 0,
        .Key_RIGHT = 0,
        .Key_LEFT = 0,
        .Key_SELECT = ChkMemICFull_SELECT,
        .routine = 0,
        .service = 0,
        .initial = 0,
        .display = ChkMemICFull_DISPLAY,
        .subwidget = ChkMemICFull_sub_W,
        .number_of_widget = sizeof(ChkMemICFull_sub_W)/4,
        .index = 0,
        .no_messgae = 1,
        .name = "ChkMemICFull",
};

void ChkMemICFull_DISPLAY(){
    // Clear screen
    ST7565ClearScreen(BUFFER1);
    ST7565Refresh(BUFFER1);
    // Write SCREEN
    LCD_String_ext(0,get_Language_String(S_MEMORY_FULL_1) , Align_Central);
    LCD_String_ext(16,get_Language_String(S_MEMORY_FULL_2) , Align_Central);
    LCD_String_ext(32,get_Language_String(S_MEMORY_FULL_3) , Align_Central);
    LCD_String_ext(48,get_Language_String(S_PRESS_ENTER) , Align_Central);
}

void ChkMemICFull_SELECT()
{
    Return_pre_widget();    
}


