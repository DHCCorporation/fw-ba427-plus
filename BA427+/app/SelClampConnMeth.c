//*****************************************************************************
//
// Source File: SelClampConnMeth.c
// Description: Select Clamp Connection Method widget to choice "Direct Connect" or "Jumper Post" condition. 
//				If select "Direct Connect", then directly go to "CheckRipple_widget".
//				If select "Jumper Post", then go to "SelCarModel_widget".
//				
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 03/22/18     William.H      Created file.
// 03/23/18     Jerry.W        SelClampConnMeth_UP_DOWN() for up and down button.
//                             SelClampConnMeth_DISPLAY() force trans type of SelClampConnMeth_widget.index
//                             Correct option of SelClampConnMeth_UP_DOWN()
// 09/03/18     Henry.Y        global variable g_SelClampConnMeth merged with TEST_DATA*.
// 09/11/18     Henry.Y        Adjust test flow PointToBAT_widget and CheckRipple_widget if direct connect selected.
// 12/07/18     Henry.Y        New display api.
// =============================================================================
//
//
// $Header: $
// $DateTime: $
// $Author: $
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#include "Widgetmgr.h"

void SelClampConnMeth_DISPLAY();
void SelClampConnMeth_UP_DOWN();
void SelClampConnMeth_LEFT();
void SelClampConnMeth_SELECT();


widget* SelClampConnMeth_sub_W[] = {
		&SelCarModel_widget,
		&PointToBAT_widget
};

widget SelClampConnMeth_widget = {
		.Key_UP = SelClampConnMeth_UP_DOWN,
		.Key_DOWN = SelClampConnMeth_UP_DOWN,
		.Key_RIGHT = 0,
		.Key_LEFT = SelClampConnMeth_LEFT,
		.Key_SELECT = SelClampConnMeth_SELECT,
		.routine = 0,
		.service = 0,
		.display = SelClampConnMeth_DISPLAY,
		.subwidget = SelClampConnMeth_sub_W,
		.number_of_widget = sizeof(SelClampConnMeth_sub_W)/4,
		.index = 0,
		.name = "SelClampConnMeth",
};


void SelClampConnMeth_DISPLAY()
{
	LCD_display(TEST_IN_VEHICLE, SelClampConnMeth_widget.index);
}


void SelClampConnMeth_UP_DOWN()
{
	SelClampConnMeth_widget.index ^= 0x01;
	SelClampConnMeth_DISPLAY();
}



void SelClampConnMeth_LEFT()
{
	Return_pre_widget();
}

void SelClampConnMeth_SELECT()
{
	TEST_DATA* TD = Get_test_data();
	SYS_PAR* par = Get_system_parameter();
	TD->SelClampConnMeth = SelClampConnMeth_widget.index;

	if(SelClampConnMeth_widget.index){ // 1: JUMPER_POST --> SelCarModel_widget
		Move_widget(SelClampConnMeth_sub_W[0],0);
	}
	else{	// 0: DIRECT_CONNECT --> test
		resistance_compensation(0);
		Move_widget(SelClampConnMeth_sub_W[1],0);
	}
}

