//*****************************************************************************
//
// Source File: PowerLowWidget.c
// Description: Both internal and external battery voltage are low
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 11/14/17     Jerry.W	       Created file.
// 09/11/18     Henry.Y        Use system error flag to replace the global variables.
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/app/Checkintbatwidget.c#6 $
// $DateTime: 2017/09/22 11:17:33 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#include "Widgetmgr.h"

void PowerLow_DISPLAY();
void PowerLow_Service();
void PowerLow_Routine();

widget* PowerLow_sub_W[] = {

};

widget PowerLow_widget = {
		.Key_UP = 0,
		.Key_DOWN = 0,
		.Key_RIGHT = 0,
		.Key_LEFT = 0,
		.Key_SELECT = 0,
		.routine = PowerLow_Routine,
		.service = PowerLow_Service,
		.initial = 0,
		.display = PowerLow_DISPLAY,
		.subwidget = PowerLow_sub_W,
		.number_of_widget = sizeof(PowerLow_sub_W)/4,
		.index = 0,
		.no_messgae = 1,
		.name = "PowerLow",
};

static uint16_t PowerLow_counter;
void PowerLow_Timer_callback();
const char e_Power_Low[8] =  	{0x04,	0x04,	0xFF,	0x22,	0x04,	0x02,	0xFF,	0xFF};
extern char e_BTP_TO[8];

void PowerLow_DISPLAY(){
	ST7565ClearScreen(BUFFER0);
	ST7565Refresh(BUFFER0);
	LANGUAGE LangIdx = get_language_setting();
	set_language(en_US);
	LcdDrawScreen( 90, BUFFER0);
	set_language(LangIdx);
}

void PowerLow_Service(){
	PowerLow_counter = 60;
	BTUARTSend(e_Power_Low, sizeof(e_Power_Low));
	set_countdown_callback(PowerLow_Timer_callback, PowerLow_counter);
}

void PowerLow_Routine(){
	float external_voltage;
	BattCoreScanVoltage(&external_voltage);
	if(external_voltage > 8){
		ROM_TimerDisable(TIMER0_BASE, TIMER_A);
		clear_system_error(Power_low);
		clear_system_error(printer_error);
		BT_POWER_ON;
		BT_RESET_HIGH;
		PWMEnable();
		Return_pre_widget();
		return;
	}
	if(!PowerLow_counter){
		if(BluetoothConnected){
			e_BTP_TO[2] = 0x01;
			BTUARTSend(e_BTP_TO, sizeof(e_BTP_TO));
			ROM_SysCtlDelay(get_sys_clk()/3/1000);
			while(BluetoothConnected);
		}
		ROM_TimerDisable(TIMER0_BASE, TIMER_A);
		IntBat_SW_OFF;
		PWMDisable();
		BT_RESET_LOW;
		BT_POWER_OFF;
	}
}

void PowerLow_Timer_callback(){
	PowerLow_counter--;
	ST7565Draw6x8Char(115 -7, 50, (PowerLow_counter/100)? (PowerLow_counter/100)%10 +0x30 : ' ', Font_System5x8);
	ST7565Draw6x8Char(115, 50, (PowerLow_counter/10)? (PowerLow_counter/10)%10 +0x30 : ' ', Font_System5x8);
	ST7565Draw6x8Char(115 + 7, 50, (PowerLow_counter)%10 +0x30, Font_System5x8);
}
