//*****************************************************************************
//
// Source File: Mainmenuwidget.c
// Description: Main Menu widget including Click(Push to new menu entry to stack)/
//				Right/Left(Pop out stack)/Up/Down function definition.
//				
//
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 09/16/15     William.H      Created file.
// 10/01/15     William.H      Re-arrange/remove Global varaible's usage due to we've used stack to behave the manu level selection.
//                             Respective global varaible by the respective widget file to be maintained by their own.
// 10/05/15     William.H      Check in BT2020 project which branched from LCDFontMaker(Pre-BT2020) by modifed related GPIOs.
// 10/21/15     Henry.Y        Enter into "VOLTAGEHIGH" entry when the external voltage exceeds the range of 12V channel.
// 10/30/15     William.H      To update some screens: a). "Voltage High" b). "Is it a 6V Battery?"
// 11/04/15     Vincent.T      1.#include "mapsvc.h"
//                             2.SYSTEM_CHANNEL refer to VOL_CH
// 01/04/16     Vincent.T      1. Add Test Counter func.
//                             2. #include "TestCounterwidget.h"
// 01/27/16     Henry.Y        1. #include "..\service/btsvc.h", 
//                                #include "..\service/testrecordsvc.h", 
//                                #include "..\driver/btuart_iocfg.h"
//                             2. global variables: g_BluetoothEnabled, WHICH_TEST, g_bService_status
//                             3. Use 'if(g_BluetoothEnabled && !ROM_GPIOPinRead(BT_CONN_STATUS_BASE, BT_CONN_STATUS_PIN) && g_bService_status)' to judge the test is requested by mobile(APP).
// 01/28/16     Vincent.T      1. Add Start-stop test entry of main menu
//                             2. global variable g_SSBatteryType to get battery type of start-stop test
// 02/02/16     Vincent.T      1. Add System test routine.
// 02/17/16     Vincent.T      1. Add voltage high in start-stop test routine.
// 02/18/16     Vincent.T      1. Add Bluetooth connection routine for Start-stop
// 03/07/16     Vincent.T      1. Add global variable g_ui8TestCounterType
// 03/25/16     Vincent.T      1. Modify MainMenuClick(); to handle multination language case.
// 04/08/16     Vincent.T      1. Remove g_ui8TestCounterType due to IR test
//                             2. Modify MainMenuClick(); due to IR test.
// 05/19/16     Vincent.T      1. Get temperature before system test routine.(aka. modfiy MainMenuClick())
//                             2. #include "..\service/tempsvc.h"
//                             3. global variable gbSysTemp(Temperature for system test)
// 06/21/16     Vincent.T      1. g_ui8MainMenu to access the lastest selected test icon.
//                             2. Modify MainMenuClick(); to memory the selected lastest test icon.
//                             3. #include "..\service/eepromsvc.h"
// 06/28/16     Vincent.T      1. New global variable fSysObjTemp(sensed temperature after system test icon clicked)
// 11/15/16     Ian.C          1. extern g_LangSelectIdx for multi-language.
//                             2. Modify BTClear_Status(); to BTClear_Status(g_LangSelectIdx); for multi-language.
// 01/13/17     Vincent.T      1. New function MainMenuTip(); to show main meun icon meaning.
//                             2. Modify MainMenuAntiWhiteItem();(add MainMenuTip())
//                             3. Modify MainMenuClick(); to skip "6V Battery?", when doing IR test.
//                             4. Modify MainMenuRight();(add MainMenuTip())
//                             5. Modify MainMenuUp();(add MainMenuTip())
//                             6. Modify MainMenuDowun();(add MainMenuTip())
// 01/19/17     Vincent.T      1. Modify MainMenuTip(); "IR" -> "IR."
// 02/17/17     Vincent.T      1. Modify MainMenuTip()' "STARTER TEST" -> "SYSTEM TEST"
// 03/22/17     Vincent.T      1. Modify MainMenuClick();(if volt. > 32 -> voltage high)
// 09/21/17     Jerry.W	       Rewrite Widget for new software architecture
// 09/26/17	    Jerry.W	       1. remove the use of gbSysTemp to reduce the usage of extern variable.
//	                           2. fix the error order of string in MainMenuTip.
// 11/02/17     Jerry.W		   1. fix bug by adding "static" before "uint8_t DelayCounter"
// 11/06/17     Henry.Y        1. Add voltage measure of start stop test and IR test in MainMenuClick().
//                             2. Modify test threshold of system test in MainMenuClick().
// 11/13/17     Jerry.W        1. Modify the direction of next widget for Hyundai
// 11/23/17     Jerry.W        1. add initail function for switching in form bluetooth printer mode
// 02/14/18     Henry.Y        AM/VM add clamps check.
// 03/22/18     Henry.Y        modify system flow, enter into scannerwidget at test beginning.
// 05/18/18     Jerry.W        move ST7650Blue() to initial for WIFI status blinking.
// 09/11/18     Henry.Y        Modify test flow of the test. Use systeminfo to replace the global variables.
// 12/07/18     Henry.Y        New display api.
// 03/20/19     Henry.Y        Rounddown display voltage to the second decimal place.
// 07/29/20     David.Wang     Delete IR test , StartStop test function
// 08/06/20     David.Wang     Add start ask voltage high check screen
// 09/06/20                    BUG ID 16633 FIX move scanner widget in there
// =============================================================================
//
//
// $Header:   $
// $DateTime: $
// $Author:   $
//
//
// Copyright (c) 2020 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#include "Widgetmgr.h"
#define UPDATE_VIEW_PERIOD 10//ms

tRectangle sTemp;
static uint32_t DelayCounter = 0;

#if 0
typedef enum{
    MENU_StartStop_test,
    MENU_battery_test,
	MENU_system_test,
	MENU_setting,
	MENU_Test_counter,
	MENU_Language,
	MAIN_Date,
	MENU_BackLight,
    MENU_Information,
    MAIN_ITEM_NUM
}MENU;
#else//BA427+
typedef enum{
    MENU_battery_test = 0,
	MENU_system_test,
	MENU_setting,
    MAIN_ITEM_NUM
}MENU;

#endif



void MainMenuAntiWhiteItem(MENU menuIdx);
void MainMenuRight(uint8_t menuIdx);
void MainMenuLeft(uint8_t menuIdx);
void MainMenuUp(uint8_t menuIdx);
void MainMenuDown(uint8_t menuIdx);
void MainMenuTip(uint8_t menuIdx);

void MainMenud_LEFT();
void MainMenud_RIGHT();
void MainMenud_display();
void MainMenud_SELECT();
void MainMenud_ROUTINE();
void MainMenud_Init();

widget* MAINMENU_sub_W[] = {
		&VOLTAGEHIGH_widget,
		&IsIta6VBattery_widget,
		&BatterySel_widget,
		&CheckRipple_widget,
		&BackLightSet_widget,
		&LangSel_widget,
		&TestCounterConfirm_widget,
		&DateTimeSet_widget,
		&InVKbd_widget,
		&Configration_widget,
};



widget MAINMENU_widget = {
		.Key_UP = 0,
		.Key_DOWN = 0,
		.Key_RIGHT = MainMenud_RIGHT,
		.Key_LEFT = MainMenud_LEFT,
		.Key_SELECT = MainMenud_SELECT,
		.routine = MainMenud_ROUTINE,
		.service = 0,
		.initial = MainMenud_Init,
		.display = MainMenud_display,
		.subwidget = MAINMENU_sub_W,
		.number_of_widget = sizeof(MAINMENU_sub_W)/4,
		.index = 0,
		.name = "MAINMENU",
};


static void show_volt(){    
    char buf[20];
    float f_v;
    BattCoreScanVoltage(&f_v);
    sprintf(buf, "%5.2f %s", f_v, get_Language_String(S_MENU_VOLTS));
    LCD_String_ext(32,buf , Align_Central);
}

void MainMenud_Init(){
	SYS_PAR* par = Get_system_parameter();
	TEST_DATA* TD = Get_test_data();
	ST7650Blue();
    MAINMENU_widget.index = MENU_battery_test;
	TD->TestInVehicle = 0;
}


void MainMenud_ROUTINE(){
	if(DelayCounter == 0){
        DelayCounter = UPDATE_VIEW_PERIOD;
        if(!MAINMENU_widget.parameter){
            show_volt();
        }
	}
	else{
		DelayCounter--;
	}
	/*uint32_t ui32scale;
	float fvoltage;
	if(BattCoreChkIntBatt(&ui32scale, &fvoltage) == 1){
	    ROM_TimerDisable(TIMER0_BASE, TIMER_A); // prevent form callback, if internal Battery low event occure
	    Move_widget(&ChkIntBat_widget, 0);
	}*/
}

void MainMenud_display(){
    ST7565ClearScreen(BUFFER1);
    ST7565Refresh(BUFFER1);

    if(!MAINMENU_widget.parameter){        
        LCD_String_ext(16,get_Language_String(S_BATTERY_STATUS) , Align_Central);
        show_volt();
        LCD_String_ext(48,get_Language_String(S_PRESS_ENTER) , Align_Central);
    }
    else{
        LCD_String_ext(16,get_Language_String(S_BATTERY_TEST) , Align_Central);
        LCD_String_ext(32,get_Language_String(S_SYSTEM_TEST) , Align_Central);
        LCD_String_ext(48,get_Language_String(S_CONFIGURATION) , Align_Central);
        LCD_Select_Arrow(16*(MAINMENU_widget.index+1));
    }
    //LCD_Arrow(Arrow_enter | Arrow_RL);
}

void MainMenud_LEFT(){
    if(!MAINMENU_widget.parameter) return;
	MAINMENU_widget.index = (MAINMENU_widget.index == 0)? MAIN_ITEM_NUM-1:MAINMENU_widget.index-1;
	MainMenud_display();
}

void MainMenud_RIGHT(){
    if(!MAINMENU_widget.parameter) return;
	MAINMENU_widget.index = (MAINMENU_widget.index+1)%MAIN_ITEM_NUM;
	MainMenud_display();
}


void MainMenud_SELECT(){
	SYS_PAR* par = Get_system_parameter();
	TEST_DATA* TD = Get_test_data();
	float external_voltage = 0.0;
    
    if(!MAINMENU_widget.parameter){
        MAINMENU_widget.parameter = 1;
    	MainMenud_display();
    }
    else{        
        BattCoreScanVoltage(&external_voltage);

        switch((MENU)MAINMENU_widget.index){
        case MENU_battery_test:
        {
            par->WHICH_TEST = Battery_Test;
            if(TD->SYSTEM_CHANNEL == MAP_24V){
                Move_widget(MAINMENU_sub_W[0],0);
            }
            else{
                Move_widget(MAINMENU_sub_W[1],0);
            }
            break;
        }

        case MENU_system_test:
        {
            MAINMENU_widget.index = MENU_battery_test;        
            float fBodyTemp;
            GetTemp( &fBodyTemp, &TD->SysObjTemp);
            par->WHICH_TEST = System_Test;
            Move_widget(MAINMENU_sub_W[1],0);
            break;
        }
        case MENU_setting:
        {
            MAINMENU_widget.index = MENU_battery_test;        
            UARTprintf("configuration");
            Move_widget(MAINMENU_sub_W[9],0);
            break;
        }
        }
    }
}



