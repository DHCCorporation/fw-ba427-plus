//*****************************************************************************
//
// Source File: AltLoadVoltwidget.c
// Description: Alt. load votlage test result widget including Click(Push to new menu entry to stack)/
//				Right/Left(Pop out stack)/Up/Down function body
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 02/04/16     Vincent.T      Created file.
// 09/21/17 	Jerry.W		   Rewrite Widget for new software architecture
// 05/18/18     Jerry.W        Add message in widget structure.
// 09/11/18     Henry.Y        Add ALTLoadVol_display/ALTLoadVol_LEFT.
// 12/07/18     Henry.Y        New display api.
// 03/20/19     Henry.Y        Rounddown test voltage to the second decimal place.
// ============================================================================

// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/app/AltLoadVoltwidget.c#7 $
// $DateTime: 2017/09/22 11:16:12 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.


#include "Widgetmgr.h"

void ALTLoadVol_Select();
void ALTLoadVol_Service();
void ALTLoadVol_display();


widget* ALTLoadVol_sub_W[] = {
        &PrintResult_widget, //RO_widget,
};

widget ALTLoadVol_widget = {
		.Key_UP = 0,
		.Key_DOWN = 0,
		.Key_RIGHT = 0,
		.Key_LEFT = 0,
		.Key_SELECT = ALTLoadVol_Select,
		.routine = 0,
		.service = ALTLoadVol_Service,
		.display = ALTLoadVol_display,
		.subwidget = ALTLoadVol_sub_W,
		.number_of_widget = sizeof(ALTLoadVol_sub_W)/4,
		.index = 0,
		.no_messgae = 1,
		.name = "ALTLoadVol",
};


void ALTLoadVol_display(){
    TEST_DATA* TD = Get_test_data();
    if(ALTLoadVol_widget.parameter == 1){
        ST7565ClearScreen(BUFFER1);
        ST7565Refresh(BUFFER1);
	    LCD_String_ext(16,get_Language_String(S_ALT_LOAD_VOLTS) , Align_Central);
	    char buf[10];
	    uint16_t result[] = {S_LOW, S_NORMAL, S_HIGH};
	    sprintf(buf, "%.2fV", TD->LoadVol );
	    LCD_String_ext2(32,buf , Align_Left, 2);
	    LCD_String_ext2(48,get_Language_String(result[TD->AltLoadVResult-LOW]) , Align_Right, 2);
	}
    else if(ALTLoadVol_widget.parameter == 2){
        ST7565ClearScreen(BUFFER1);
        ST7565Refresh(BUFFER1);
        LCD_String_ext(16,get_Language_String(S_TEST_OVER_TURN) , Align_Central);
        LCD_String_ext(32,get_Language_String(S_OFF_LOAD_n_ENGINE) , Align_Central);
    }
    LCD_Arrow(Arrow_enter);
}


void ALTLoadVol_Service(){
	if(ALTLoadVol_widget.parameter == 0){
		float fval;
		TEST_DATA* TD = Get_test_data();
		TD->AltLoadVResult = AltLoadVoltTest(&fval);
		TD->LoadVol = floating_point_rounding(fval, 2, ffloor);
		ALTLoadVol_widget.parameter = 1;
		ALTLoadVol_display();
	}
}

void ALTLoadVol_Select(void)
{
    SYS_INFO* info = Get_system_info();
    TEST_DATA* TD = Get_test_data();

    if(ALTLoadVol_widget.parameter == 1){
        ALTLoadVol_widget.parameter = 2;
        ALTLoadVol_display();

        counter_increase(&info->SysTestCounter);
        save_system_info(&info->SysTestCounter, sizeof(info->SysTestCounter));
        ExtRTCRead( &TD->year, &TD->month, &TD->date, &TD->day, &TD->hour, &TD->min, &TD->sec);
    }
    else if(ALTLoadVol_widget.parameter == 2){
        info->pre_test_state = 0;
        save_system_info(&info->pre_test_state, sizeof(info->pre_test_state));
        Move_widget(ALTLoadVol_sub_W[0], 0);
    }

}



