//*****************************************************************************
//
// Source File: Ripple15Secwidget.c
// Description: Measuring ripple for ALT load on in system test.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 09/21/17 	Jerry.W	       Created file.
// 09/21/17 	Jerry.W		   Rewrite Widget for new software architecture
// 09/30/17 	Jerry.W		   1. add Ripple15Sec_Routine(), and move all the work in Ripple15Sec_Timercallback() into it.
//								  Ripple15Sec_widget.parameter is defined as 1 for countdown operation, 2 for timeout
// 09/11/18     Henry.Y        Modify operation flow and display.
// 12/07/18     Henry.Y        New display api.
// 03/20/19     Henry.Y        Random test voltage to the second decimal place.
// 08/03/20     David Wang     Some pictures of sys test changed to text description
// 10/19/20     David Wang     ripple 15sec count screen flash to fix
// 20201127     David Wang     Load on test countdown 15se word modify & need to flash
//
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/app/Ripple15Secwidget.c#6 $
// $DateTime: 2017/09/30 16:44:56 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#include "Widgetmgr.h"
uint16_t Display_Refresh_15SEC = 0;

void Ripple15Sec_DISPLAY();
void Ripple15Sec_Select();
void Ripple15Sec_Timercallback();
void Ripple15Sec_Service();
void Ripple15Sec_Routine();

widget* Ripple15Sec_sub_W[] = {
		&ALTLoadVol_widget,
		&AbortTest_widget
};

widget Ripple15Sec_widget = {
		.Key_UP = 0,
		.Key_DOWN = 0,
		.Key_RIGHT = 0,
		.Key_LEFT = 0,
		.Key_SELECT = Ripple15Sec_Select,
		.routine = Ripple15Sec_Routine,
		.service = Ripple15Sec_Service,
		.display = Ripple15Sec_DISPLAY,
		.subwidget = Ripple15Sec_sub_W,
		.number_of_widget = sizeof(Ripple15Sec_sub_W)/4,
		.index = 0,
		.no_messgae = 1,
		.name = "Ripple15Sec",
};



void Ripple15Sec_DISPLAY(){
	ST7565ClearScreen(BUFFER1);
	ST7565Refresh(BUFFER1);
	if(Ripple15Sec_widget.parameter == 0){

	    LCD_String_ext(16,get_Language_String(S_RUN_ENGINE_UP_TO) , Align_Central);
	    LCD_String_ext(32,get_Language_String(S_2500_RPM_15SEC) , Align_Central);
	}
	else if(Ripple15Sec_widget.parameter == 1){

	    LCD_String_ext(16,get_Language_String(S_RUN_ENGINE_UP_TO) , Align_Central);
	    char buf[20];
	    sprintf(buf, "%s", get_Language_String(S_2500_RPM_15SEC));
	    char* sec = strstr(buf, "1");
	    sec[0] = ('0' + (g_15SecCDCounter/10));
	    sec[1] = ((g_15SecCDCounter%10) + '0');
	    LCD_String_ext(32,buf , Align_Central);

	}	
	else{/*if(Ripple15Sec_widget.parameter == 2)*/
	    TEST_DATA* TD = Get_test_data();
	    if(TD->RipVResult == NO_DETECT){
	        LCD_String_ext(16,get_Language_String(S_NO_RIPPLE_DETECT) , Align_Central);
	        LCD_String_ext(32,get_Language_String(S_AND_PRESS_ENTER) , Align_Central);
	    }
	    else{
	        LCD_String_ext(16,get_Language_String(S_RIPPLE_DETECTED) , Align_Central);
	        char buf[10];
	        uint16_t result[] = {S_LOW, S_NORMAL, S_HIGH};
	        sprintf(buf, "%.2fV", TD->ripple );
	        LCD_String_ext2(32,buf , Align_Left, 2);
	        LCD_String_ext2(48,get_Language_String(result[TD->RipVResult-LOW]) , Align_Right, 2);
	    }
	    LCD_Arrow(Arrow_enter);
	}
}

void Ripple15Sec_Select(){
	if(Ripple15Sec_widget.parameter == 2){
		Main_routine_switch(1);
		Ripple15Sec_widget.index = 0;
		Move_widget(Ripple15Sec_sub_W[0], 0);
	}
}

void Ripple15Sec_Service(){
	TEST_DATA* TD = Get_test_data();
	if(Ripple15Sec_widget.parameter == 0){
		if(TD->RipVResult = SysRipTest(&TD->ripple)){
			TD->ripple = floating_point_rounding(TD->ripple, 2, ffloor);
			//
			// Ripple detected!
			// No need to contdown 15 sec
			//
			Ripple15Sec_widget.parameter = 2;		
			Ripple15Sec_DISPLAY();
		}
		else{
			//
			// Countdonw 15 sec
			//
			set_countdown_callback(Ripple15Sec_Timercallback, 15);
			Ripple15Sec_widget.parameter = 1;
			if(get_routine_switch()){
				Main_routine_switch(0);
			}
		}
	}
	else if(Ripple15Sec_widget.parameter == 1){		
		if(!g_15SecCDCounter){
			Ripple15Sec_widget.parameter = 2;			
			Main_routine_switch(1);
			Ripple15Sec_DISPLAY();
		}
		else{
			if(get_routine_switch()){
				Main_routine_switch(0);
			}
		}		
	}	
}

void Ripple15Sec_Routine(){
	uint8_t ui8RipVResultTemp;
	TEST_DATA* TD = Get_test_data();
	float frippleTemp;
	
	if(Ripple15Sec_widget.parameter == 1){		
		if(get_routine_switch()){
			Main_routine_switch(0);
		}
		if(!g_15SecCDCounter){
			Ripple15Sec_widget.parameter = 2;
			TD->ripple = floating_point_rounding(TD->ripple, 2, ffloor);
			Main_routine_switch(1);
		}
		else{
			ui8RipVResultTemp = SysRipTest(&frippleTemp);
			if((ui8RipVResultTemp >= TD->RipVResult) || (frippleTemp >= TD->ripple)){
				//
				// Recursive loop
				//
				TD->ripple = frippleTemp;
				TD->RipVResult = ui8RipVResultTemp;
			}
		}

	    Ripple15Sec_DISPLAY();

	}
	else if(Ripple15Sec_widget.parameter == 2){	
		if(get_routine_switch() == 0){
			Main_routine_switch(1);
		}
	}

}

void Ripple15Sec_Timercallback(){
	/*if(Ripple15Sec_widget.parameter != 2){
		Ripple15Sec_widget.parameter = 1;
	}*/
}


