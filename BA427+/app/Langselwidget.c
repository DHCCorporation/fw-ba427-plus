//*****************************************************************************
//
// Source File: Langselwidget.c
// Description: Language select widget including Click(decide the language set)/ 
//				Left(Pop out stack)/Up/Down function definition.
//				
//
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 09/17/15     William.H      Created file.
// 10/01/15     William.H      Re-arrange/remove Global varaible's usage due to we've used stack to behave the manu level selection.
//                             Respective global varaible by the respective widget file to be maintained by their own.
// 10/05/15     William.H      Check in BT2020 project which branched from LCDFontMaker(Pre-BT2020) by modifed related GPIOs.
// 01/06/15     William.H      1. Define multi-language set array - g_LangSet[].
//                             2. Define radio button structure array(ex: LangMenuFont_radioUnChk_nor) and bitmap code(ex: radioUnChkCode_nor).
//                             3. Define Language menu blank's structure array and bitmap code.
//                             4. Define Language menu font's structure array and bitmap code.
//                             5. Implement LangSelDrawRadioBtn() function to draw the radio button.
//                             6. Create LangSelClrMenuList() and LangSelDrawMenuList() functions to draw the language menu list.
//                             7. Modified LangSelMenuAntiWhiteItem() function to meet the dynamic language set requested by customer.
//                             8. Modified LangSelMenuUp() and LangSelMenuDown() functions to meet the dynamic language set requested by customer.
// 03/24/16     Vincent.T      1. gloabal variable LangIdx  to get current language.
//                             2. Handle multinational language switch case.
// 04/07/16     Vincnet.T      1. Modify g_LangSet[] to make all content same with LANGUAGE
// 10/13/16     Ian.C          1. Add LangSelMenuDrawPageInfo(); to draw page information at bottom right.
// 01/13/17     Vincent.T      1. Modify langMenuCode[];(zh_CHS}: Chinese Simplified; ru_RU: Russian)
// 03/22/17     Vincent.T      1. New Language group: __BT2100_WW__
//                             2. Modify LangSelMenuDrawPageInfo(); due to new Language group;
// 09/21/17 	Jerry.W		   Rewrite Widget for new software architecture
// 09/25/17 	Jerry.W		   modify display for flexible set of g_LangSet
// 11/24/17     Jerry.W        1. Modify language option for Hyundai project
// 10/17/18     Henry.Y        Add new identifier: __BO__ and branch version.
// 12/07/18     Henry.Y        New display api.
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/app/Langselwidget.c#1 $
// $DateTime: 2017/11/27 10:38:22 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#include "Widgetmgr.h"






void LangSel_RIGHT();
void LangSel_LEFT();
void LangSel_SELECT();
void LangSel_display();
void LangSel_initial();

widget* LangSel_sub_W[] = {
};

widget LangSel_widget = {
		.Key_UP = 0,
		.Key_DOWN = 0,
		.Key_RIGHT = LangSel_RIGHT,
		.Key_LEFT = LangSel_LEFT,
		.Key_SELECT = LangSel_SELECT,
		.routine = 0,
		.service = 0,
		.initial = LangSel_initial,
		.display = LangSel_display,
		.subwidget = LangSel_sub_W,
		.number_of_widget = sizeof(LangSel_sub_W)/4,
		.index = 0,
		.name = "LangSel",
};

typedef struct{
    LANGUAGE lang;
    uint32_t lang_string_idx;
}LAN_LIST;

static LAN_LIST List[] = {
        {en_US, S_ENGLISH},
        {es_ES, S_SPANISH},
        {de_DE, S_GERMAN},
        {it_IT, S_ITALIAN},
        {pt_PT, S_PORTUGUESE},
        {fr_FR, S_FRENCH},
        {nd_ND, S_NEDERLANDS},
};



void LangSel_initial(){
	LANGUAGE LangIdx = get_language_setting();
	uint8_t i;
	for(i = 0; i < number_total_language; i++){
		if(List[i].lang == LangIdx){
			LangSel_widget.index = i;
		}
	}
}

void LangSel_RIGHT(){
    LangSel_widget.index = (LangSel_widget.index+1)%number_total_language;

	LangSel_display();
}

void LangSel_LEFT(){
	LangSel_widget.index = (LangSel_widget.index == 0)? (number_total_language-1):LangSel_widget.index-1;

    LangSel_display();
}


void LangSel_SELECT(){
    set_language(List[LangSel_widget.index].lang);
    SYS_INFO* info = Get_system_info();
    info->Language_Select = List[LangSel_widget.index].lang;
    save_system_info(&info->Language_Select, sizeof(info->Language_Select));
    Return_pre_widget();
}

void LangSel_display(){
    ST7565ClearScreen(BUFFER1);
    ST7565Refresh(BUFFER1);

    LCD_String_ext(16,get_Language_String(S_SELECT) , Align_Central);

    LCD_Arrow(Arrow_enter | Arrow_RL);

    LCD_String_ext(32,get_Language_String(List[LangSel_widget.index].lang_string_idx) , Align_Central);
}





