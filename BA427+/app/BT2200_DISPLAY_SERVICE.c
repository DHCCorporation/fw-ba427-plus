//*****************************************************************************
//
// Source File: BT2200_DISPLAY_SERVICE.c
// Description: New display api for BT2200 application layer.
//
//
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 11/30/18     Henry.Y        Create file.
// 12/14/18     Henry.Y        Add FRANCAIS.
// 12/17/18     Henry.Y        Add ESPAÑOL/PORTUGUÊS.
// 01/30/19     Henry.Y        Add new translation about device registration.
// 03/29/19     Henry.Y        Add show_print_battery_voltage() display clamps voltage.
// 07/30/20     David.Wang     Delete IR test , StartStop test function
// 08/03/20     David.Wang     1.Some pictures of sys test changed to text description
//                             2.VM/AM meter add reminder on/off screen
// 08/06/20     David.Wang     1. add bat test 24v system ask page
// 08/07/20     David.Wang     Diesel engine ask 40sec count down
// 08/14/20     David.Wang     Add HD function
// 08/17/20     David.Wang     Add show one by one pack battery test txt
// 08/19/20     David.Wang     Add pack test all bat result.
// 08/26/20     David.Wang     Add advised action widget show words
// 09/06/20     David.wang     1. Modify the pattern combination of the abort widget button
//                             2. Modify pack test bat select widget button pattern combination　2  => LEFT_DOWN_ENT_UP_PAGE2
//                             3. TEST COUNTER button pattern combination LEFT_DOWN_ENT_UP_PAGE2 => 2 ( LEFT_DOWN_ENT_UP ) NO PAGE
//                             4. PACK TEST #2 Basemap button pattern replacement 7>>4
//                             5. WARNING_CLAMPS_ON Basemap button pattern replacement ENTER >> LEFT + ENTER
//                             6. Print again Basemap button pattern replacement
// 09/08/20     David.Wang     Sys test Graphic display replaced by text prompt
// 10/16/20     David.Wang     1. # OF BATTERY IN PARALLEL => PACK TEST
//                             2. Add a blank before option text : "BATTERY x 2" >> " BATTERY x 2"
//                             3. RATING :　SAE > CCA/SAE
//                             4. CCA/SAE onlyshow cca unit for set capacity in BT2000_HD_BO
//                             5. bug fix en_AMVM_TURN_OFF_AMP[] = "PRESS TURN OFF"; >> "PLEASE TURN OFF";
//                             6. Cranking voltage : MODIFY words NO DETECTED => NOT DETECTED
//                             7. SYS TEST : START TEST >> CARKING TEST
//                             8. SYS TEST : RIPPLE >> RIPPLE VOLTAGE
// 10/19/20                    9. ripplr 15sec count screen flash to fix
//                             10. Surface charge turn headlights countdown 15sec Picture to text
//                             11. ERASE item no used to del
// 10/20/20     David Wang     12. Diesel engine flow add new screen.
//                             13. Enlarged print fonts
// 10/21/20     David Wang     14. French and Spanish translation changes
// 20201127     David Wang     Load on test countdown 15se word modify & need to flash
//
// =============================================================================
//
//
// $Header$
// $DateTime$
// $Author$
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#include "BT2200_DISPLAY_SERVICE.h"


#if ENGLISH_en
// LCD
const char en_language[] = 				"ENGLISH";
const char en_battery[] =			    "BATTERY";
const char en_start_stop_test[] =		"START-STOP TEST";
const char en_battery_test[] =			"BATTERY TEST";
const char en_system_test[] =			"SYSTEM TEST";
const char en_ir_test[] =				"IR. TEST";
const char en_v_a_meter[] = 		    "V./A. METER";
const char en_setting[] =				"SETTING";
const char en_test_in_vehicle[] =		"TEST IN VEHICLE";
const char en_direct_connect[] =		"DIRECT CONNECT";
const char en_jump_start_post[] =		"JUMP START POST";
const char en_brand[] = 		  	    "BRAND:";
const char en_model[] = 			    "MODEL:";
const char en_version[] =				"VERSION";
const char en_test_result[] =			"TEST RESULT";
const char en_soh[] =					"SOH:";
const char en_soc[] =					"SOC:";
const char en_cca[] =					"CCA:";
const char en_voltage[] =				"VOLTAGE:";
const char en_good_n_pass[] = 		    "GOOD & PASS";
const char en_good_n_recharge[] = 	    "GOOD & RECHARGE";
const char en_bad_n_replace[] =			"BAD & REPLACE";
const char en_caution[] =				"MARGINAL BATTERY";               //20201111 DW EDIT CAUTION => MARGINAL BATTERY
const char en_recharge_n_retest[] =		"RECHARGE & RETEST";
const char en_bad_cell_replace[] =		"BAD CELL REPLACE";
const char en_battery_type[] =			"BATTERY TYPE";
const char en_flooded[] =				"FLOODED";
const char en_agmf[] =					"AGM FLAT PLATE";
const char en_agms[] =					"AGM SPIRAL";
const char en_vrla[] =					"VRLA / GEL";
const char en_select_rating[] = 	    "SELECT RATING";
const char en_sae[] =					"CCA/SAE";      // 20201016 SAE > CCA/SAE
const char en_cap_cca[]=                "CCA";          // 20201016 dw only show cca unit for set capacity in BT2000_HD_BO
const char en_en[] =					"EN";
const char en_en2[] =					"EN";           // 20201016 EN2=>EN
const char en_iec[] =					"IEC";
const char en_jis[] =					"JIS";
const char en_din[] =					"DIN";
const char en_ca[] =                    "CA";           // 20201016 ADD CA RATING
const char en_set_capacity[] =			"SET CAPACITY";
const char en_back_light[] =			"LCD BACKLIGHT";
const char en_language_select[] =		"LANGUAGE SELECT";
const char en_clock[] = 			    "CLOCK";
const char en_clear_memory[] =			"ERASE DATA";         // 20200820 DW EDIT CLEAR MOMORY > ERASE DATA
const char en_information[] =			"INFORMATION";
const char en_time_zone[] = 		    "TIME ZONE";
const char en_test_counter[] =			"TEST COUNTER";
const char en_wifi[] =					"WIFI";
const char en_abort[] = 			    "ABORT";
const char en_directly_connect_to_battery[] =	"DIRECT CONNECT TO BATTERY";
const char en_insert_vin[] =			"INSERT VIN CODE\nMANUALLY?";
const char en_invalid_vin[] =			"INVALID VIN, RESULT NOT\nSUITABLE FOR WARRANTY";	
const char en_input_vin[] = 			"INPUT VIN";
const char en_enter_vin[] = 			"ENTER COMPLETE VIN";
const char en_test_not_suitable[] =     "TEST NOT SUITABLE FOR\nWARRANTY, CONTINUE?";
const char en_yes[] =					"YES";
const char en_no[] =					"NO";
const char en_print_result[] =			"PRINT RESULT";
const char en_print_successful[] =		"PRINTING SUCCESSFUL,\nPRINT AGAIN?";

const char en_check_clamp[] =			"CHECK CLAMP";
const char en_insert_change_aa[] =		"INSERT OR CHANGE\nAA BATTERIES";
const char en_load_error[] =			"LOAD ERROR";
const char en_voltage_high[] =			"VOLTAGE HIGH\nCHECK SYSTEM";
const char en_voltage_unstable[] =		"VOLTAGE UNSTABLE\nWAIT FOR STABLE";
const char en_is_it_6v[] =				"IS IT A 6V BATTERY?";
const char en_is_battery_charge[] =     "IS BATTERY CHARGED?";
const char en_battery_in_vehicle[] =	"BATTERY IN VEHICLE";
const char en_ss_test[] =				"SS. TEST";
const char en_bat_test[] =				"BAT. TEST";
const char en_sys_test[] =				"SYS. TEST";
const char en_ir_t[] =					"IR. TEST";
const char en_print[] = 			    "PRINT";
const char en_print_q[] =				"PRINT?";
const char en_erase[] = 			    "ERASE?";
const char en_memory_erase[] =			"MEMORY ERASE?";	
const char en_on[] =					"ON";
const char en_off[] =					"OFF";	
const char en_date[] =					"DATE:";
const char en_time[] =					"TIME:";
const char en_efb[] =					"EFB";
const char en_measured[] =				"MEASURED:";
const char en_cranking_voltage[] =		"CRANKING VOLTAGE";
const char en_lowest_voltage[] =		"LOWEST VOLTAGE";
const char en_high[] =					"HIGH";
const char en_normal[] =				"NORMAL";
const char en_low[] =					"LOW";
const char en_no_detect[] = 		    "NOT DETECTED";        // 20201016 DW MODIFY NO DETECTED => NOT DETECTED
const char en_idle_voltage[] =			"ALT. IDLE VOLTAGE";
const char en_1000_rpm[] =				"1000 x R/MIN";
const char en_sec[] =					"SEC ";
const char en_ripple_detected[] =		"RIPPLE DETECTED";
const char en_ripple_voltage[] =		"RIPPLE VOLTAGE";
const char en_load_voltage[] =			"ALT. LOAD VOLTAGE";
const char en_point_to_battery_press_enter[] =		"POINT TO BATTERY\nPRESS ENTER";
const char en_ota[] =					"OTA UPGRADE";
const char en_test_code[] = 		"TEST CODE";
const char en_connection_status[] = "CONNECTION STATUS";
const char en_scan_vin_code[] = 	"SCAN VIN CODE?";
const char en_vin_code[] =				"VIN CODE";
const char en_invalid_vin_rescan[] =	"INVALID VIN CODE\nRESCAN";
const char en_wifi_power[] =			"WIFI POWER:";
const char en_wifi_list[] = 		"WIFI LIST";
const char en_downloading[] =			"DOWNLOADING...";
const char en_upgrade_cancel[] =		"UPGRADE CANCEL?";
const char en_upgrading[] = 		"UPGRADING...";
const char en_check_version[] = 	"CHECK VERSION";	
const char en_refresh_list[] =			"*REFRESH LIST";	
const char en_limited_connection[] =	"LIMITED CONNECTION";
const char en_wifi_connected[] =		"WIFI CONNECTED";
const char en_connecting[] =			"CONNECTING...";
const char en_wifi_disconnect[] =		"WIFI DISCONNECTED";
const char en_check_connection[] =		"CHECK CONNECTION...";
const char en_keep_clamps_connect[] =	"WARNING: KEEP CLAMPS\nCONNECTED TO BATTERY.";
//const char en_skip[] =				"SKIP";
const char en_registration[] =          "REGISTRATION";
const char en_scan_qr_code[] =          "SCAN QR CODE?";
const char en_registering[] =           "REGISTERING...";
const char en_off_load[] =              "TURN OFF LOADS";                        // 20200803 DW some pictures of sys test changed to text description
const char en_start_engine[] =          "START ENGINE";                          // 20200803 DW some pictures of sys test changed to text description
const char en_make_sure_all[] =         "MAKE SURE ALL";                         // 20200803 DW some pictures of sys test changed to text description
const char en_load_are_off[] =          "LOADS ARE OFF";                         // 20200803 DW some pictures of sys test changed to text description
const char en_turn_on_lo[] =            "TURN ON LOADS";                         // 20200803 DW some pictures of sys test changed to text description
const char en_and_press_ent[] =         "AND PRESS ENTER";                       // 20200803 DW some pictures of sys test changed to text description
const char en_turn_eng_2500[] =         "REV ENGINE";
const char en_AMVM_TURN_ON_AMP[] =      "TURN ON AMP METER";                     // 20200803 DW some pictures of sys test changed to text description
const char en_AMVM_AFTER_ATT[] =        "AFTER ATTACHING";                       // 20200803 DW some pictures of sys test changed to text description
const char en_AMVM_ENTER_PROCEED[] =    "(ENTER TO PROCEED)";                    // 20200803 DW some pictures of sys test changed to text description
const char en_AMVM_TURN_OFF_AMP[] =     "PLEASE TURN OFF";                       // 20201016 DW BUG FIX PRESS >> PLEASE
const char en_AMVM_AMP_METER[] =        "AMP METER";                             // 20200803 DW some pictures of sys test changed to text description
const char en_is_24v_system[] =         "IS THIS A 24V SYSTEM?";                 // 20200806 DW some pictures of BAT test changed to text description
const char en_test_each_bat_sep[] =     "TEST EACH BATTERY SEPARATELY";          // 20200806 DW some pictures of BAT test changed to text description
const char en_connect_to_12v_bat[] =    "CONNECT TO A 12V BATTERY";              // 20200806 DW some pictures of BAT test changed to text description
const char en_is_diesel_engine[] =      "IS IT A DIESEL ENGINE";                 // 20200807 DW diesel engine ask 40sec count down
const char en_rev_engine_for[] =        "REV ENGINE FOR";                        // 20200807 DW diesel engine ask 40sec count down
const char en_Truck_Group_Test[] =      "PACK TEST?";                            // 20200813 DW HD FUNCTION ASK
const char en_Of_Bat_In_Par[] =         "PACK TEST";                             // 20201016 DW # OF BATTERY IN PARALLEL => PACK TEST
const char en_Bat_2[] =                 "BATTERY x 2";                           // 20200814 DW EDIT SELECT PACK BAT Quantity
const char en_Bat_3[] =                 "BATTERY x 3";                           // 20200814 DW EDIT SELECT PACK BAT Quantity
const char en_Bat_4[] =                 "BATTERY x 4";                           // 20200814 DW EDIT SELECT PACK BAT Quantity
const char en_Bat_5[] =                 "BATTERY x 5";                           // 20200814 DW EDIT SELECT PACK BAT Quantity
const char en_Bat_6[] =                 "BATTERY x 6";                           // 20200814 DW EDIT SELECT PACK BAT Quantity
const char en_GOOD_PACK[] =             "GOOD PACK";                             // 20200817 DW EDIT SELECT PACK BAT Quantity
const char en_CHECK_PACK[] =            "CHECK PACK";                            // 20200817 DW EDIT SELECT PACK BAT Quantity
const char en_SEP_PACK_CONNECT_BAT1[] = "SEPARATE PACK CONNECT ONLY BATTERY #1"; // 20200817 DW EDIT SELECT PACK BAT Quantity
const char en_SEP_PACK_CONNECT_BAT2[] = "SEPARATE PACK CONNECT ONLY BATTERY #2"; // 20200817 DW EDIT SELECT PACK BAT Quantity
const char en_SEP_PACK_CONNECT_BAT3[] = "SEPARATE PACK CONNECT ONLY BATTERY #3"; // 20200817 DW EDIT SELECT PACK BAT Quantity
const char en_SEP_PACK_CONNECT_BAT4[] = "SEPARATE PACK CONNECT ONLY BATTERY #4"; // 20200817 DW EDIT SELECT PACK BAT Quantity
const char en_SEP_PACK_CONNECT_BAT5[] = "SEPARATE PACK CONNECT ONLY BATTERY #5"; // 20200817 DW EDIT SELECT PACK BAT Quantity
const char en_SEP_PACK_CONNECT_BAT6[] = "SEPARATE PACK CONNECT ONLY BATTERY #6"; // 20200817 DW EDIT SELECT PACK BAT Quantity
const char en_GR_TEST_BAT1_START[] =    "PACK TEST BATTERY #1 START TEST";
const char en_GR_TEST_BAT2_START[] =    "PACK TEST BATTERY #2 START TEST";
const char en_GR_TEST_BAT3_START[] =    "PACK TEST BATTERY #3 START TEST";
const char en_GR_TEST_BAT4_START[] =    "PACK TEST BATTERY #4 START TEST";
const char en_GR_TEST_BAT5_START[] =    "PACK TEST BATTERY #5 START TEST";
const char en_GR_TEST_BAT6_START[] =    "PACK TEST BATTERY #6 START TEST";
const char en_Pack_Report[] =           "PACK REPORT";                           // 20200819 DW EDIT
const char en_BAT_NUM[] =               "BATTERY #";                             // 20200819 DW EDIT
const char en_ad_ac_press_enter[] =     "ADVISED ACTION:\nCHECK CONNECTION\nPRESS ENTER";//20200826 dw Edit Add advised action widget show words
const char en_SYS_TURN_OFF_LOAD[] =     "TURN OFF\nLOADS & ENGINE";              //20200908 DW EDIT Sys test Graphic display replaced by text prompt
const char en_Turn_Headlight_list[] =   "TURN HEADLIGHTS ON FOR";                //20201019 DW EDIT Sys test Graphic display replaced by text prompt
const char en_please_en_start_list[] =  "PRESS ENTER TO START";
// printer
//const char en_prt_agmf[] =              "AGM FLAT PLATE";
//const char en_prt_agms[] =              "AGM SPIRAL";
const char en_prt_battery_test[] =        "== BATTERY TEST ==";
//	const char en_prt_bt[] =              "BAT. TEST:";
//	const char en_prt_bad_cell[] =        "BAD CELL REPLACE";
//	const char en_prt_bad_n_replace[] =   "BAD & REPLACE";
const char en_prt_by[] =                  "BY:";
//	const char en_prt_caution[] =         "CAUTION";
const char en_prt_charging_test[] =       "CHARGING TEST";
const char en_prt_note[] =                "NOTE:";
//	const char en_prt_test_code[] =       "TEST CODE";
const char en_prt_diode_ripple[] =        "RIPPLE VOLTAGE";       // 20201016 DW EDIT DIODE RIPPLE >> RIPPLE VOLTAGE FOR BT2200_HD_BO
//	const char en_prt_efb[] =             "EFB";
//	const char en_prt_flood[] =           "FLOODED";
//	const char en_prt_good_n_pass[] =     "GOOD & PASS";
//	const char en_prt_good_n_recharge[] = "GOOD & RECHARGE";
//	const char en_prt_high[] =            "HIGH";
const char en_prt_ir_test[] =             "== IR. TEST ==";
//	const char en_prt_ir[] =              "IR. TEST:";
const char en_prt_jis[] =                 "JIS:";
const char en_prt_load_off[] =            "LOAD OFF";
const char en_prt_load_on[] =             "LOAD ON";
//	const char en_prt_low[] =             "LOW";
//	const char en_prt_measured[] =        "MEASURED:";
//	const char en_prt_no_detect[] =       "NO DETECTED";
//	const char en_prt_normal[] =          "NORMAL";
const char en_prt_rated[] =               "RATED:";
//const char en_prt_recharge_n_retest[] = "RECHARGE & RETEST";
const char en_prt_vin_num[] =             "VIN NUMBER:";
const char en_prt_soc[] =                 "STATE OF CHARGE";
const char en_prt_soh[] =                 "STATE OF HEALTH";
//	const char en_prt_ss[] =              "SS. TEST:";
const char en_prt_starter_test[] =        "CRANKING VOLTAGE";                        // 20201020 DW  WORD MODIFY START TEST >> CRANKING TEST
const char en_prt_start_stop[] =          "== START-STOP TEST ==";
//	const char en_prt_sys[] =             "SYS. TEST:";
const char en_prt_temp[] =                "TEMPERATURE:";
//	const char en_prt_test_counter[] =    "TEST COUNTER";
const char en_prt_test_date[] =           "TEST DATE:";
const char en_prt_test_report[] =         "TEST REPORT";
//	const char en_prt_voltage[] =         "VOLTAGE:";
//	const char en_prt_vrla[] =            "VRLA / GEL";
const char en_pack_test[] =               "==PACK TEST==";                          // 20200821 DW EDIT
const char en_warning_word1[] =           "==================";                     // 20200821 dw edit
const char en_warning_word2[] =           "ADVISED ACTION: ";                       // 20200821 dw edit
const char en_warning_word3[] =           "CHECK CONNECTION";                       // 20200821 dw edit
const char en_P_BATTERT_NUM[]=            "BATTERY #";                              // 20200824 DW EDIT

#endif

#if DEUTSCH_en
// LCD
const char de_language[] = "DEUTSCH";
const char de_battery[]=			"BATTERIE";
const char de_start_stop_test[]=	"START STOP TEST";
const char de_battery_test[]=		"BATTERIE TEST";
const char de_system_test[]=		"SYSTEM TEST";
const char de_ir_test[]=			"IR. TEST";
const char de_v_a_meter[]=		"S./A. METER";
const char de_setting[]=			"EINSTELLUNGEN";
const char de_test_in_vehicle[]=	"IM AUTO";
const char de_direct_connect[]=	"DIREKT VERBINDEN";
const char de_jump_start_post[]=	"FRÜHSTART-KLEMME";
const char de_brand[]=			"MARKE:";
const char de_model[]=			"MODELL:";
const char de_version[]=			"VERSION";
const char de_test_result[]=		"PRÜFUNGSRESULTATE";
const char de_voltage[]=			"SPANNUNG:";
const char de_good_n_pass[]=		"GUT & BESTANDEN";
const char de_good_n_recharge[]=	"GUT & AUFLADEN";
const char de_bad_n_replace[]=		"DEFEKT / ERSETZEN";
const char de_caution[]=			"ACHTUNG";
const char de_recharge_n_retest[]=	"LADEN & PRUEFEN";
const char de_bad_cell_replace[]=	"ZELLE DEFEKT";
const char de_battery_type[]=		"BATTERIE TYP";
const char de_flooded[]=			"FLUESSIG-BATT";
const char de_agmf[]=				"AGM PLATTE";
const char de_agms[]=				"AGM SPIRALE";
const char de_select_rating[]=	"NORM AUSWAEHLEN";
const char de_set_capacity[]=		"KAPAZITAET CCA";
const char de_back_light[]=		"LCD HINTERGRUNDLICHT";
const char de_language_select[]=	"SPRACHE AUSWAHLEN";
const char de_clock[]=			"UHR";
const char de_clear_memory[]=		"SPEICHER LÖSCHEN";
const char de_information[]=		"INFORMATION";
const char de_time_zone[]=		"ZEITZONE";
const char de_test_counter[]=		"TEST-ZAHLER";
const char de_abort[]=			"ABBRECHEN";
const char de_directly_connect_to_battery[]=	"DIREKTVERBINDUNG\nMIT BATTERIE";
const char de_insert_vin[]=				"VIN-CODE\nMAN. EINGEBEN?";
const char de_test_not_suitable[]=		"PRÜF. F. GAR. N.\nGEEIGN. FORTF.?";
const char de_yes[]=						"JA";
const char de_no[]=						"NEIN";
const char de_print_result[]=				"TEST DRUCKEN";
const char de_print_successful[]=			"DRUCKEN OK.\nERNEUT DRUCKEN?";
const char de_check_clamp[]=				"KLEMME PRUFEN";
const char de_insert_change_aa[]=			"AA-BATT. EINS.\nOD. AUSWCH.";
const char de_load_error[]=				"LOAD ERROR";
const char de_voltage_high[]=				"HOCHSPANNUNG";
const char de_voltage_unstable[]=			"SPANNUNG NICHT STABIL\nWARTEN BIS STABIL";
const char de_is_it_6v[]=					"WIRE EINE 6V-BATTERIE VERWENDET?";
const char de_is_battery_charge[]=		"IST DIE BATTERIE AUFGELADEN?";
const char de_battery_in_vehicle[]=		"BATTERIE IM FAHRZEUG";
const char de_ss_test[]=					"SS. PRÜF";
const char de_bat_test[]=					"BATT. TEST";
const char de_sys_test[]=					"SYS. TEST";
const char de_ir_t[] =					"IR. TEST";
const char de_print[]=				"DRUCKEN";
const char de_print_q[]=					"DRUCKEN?";
const char de_erase[]=					"LÖSCHEN?";
const char de_memory_erase[]=				"DEN SPEICHER\nLÖSCHEN?";
const char de_on[] = 				"AUF";
const char de_off[] =				"AUS";
const char de_date[]=						"DATUM:";
const char de_time[]=						"ZEIT:";
const char de_invalid_vin[]=				"UNG. VIN, RES.F.\nGAR.UNGEEIGN.";
const char de_input_vin[]=				"VIN EINGEBEN";
const char de_enter_vin[]=				"VOLLSTÄNDIGE\nVIN EINGEBEN";
const char de_measured[]=					"GEMESSEN:";
const char de_cranking_voltage[]=			"ANLASSSPANNUNG";
const char de_lowest_voltage[]=			"NIEDRIGSTE SPANNUNG";
const char de_high[]=						"HOCH";
const char de_normal[]=					"NORMAL";
const char de_low[]=						"NIEDRIG";
const char de_no_detect[]=				"NICHTS ENTDECKT";
const char de_idle_voltage[]=				"ALT. LEERLAUFSPANNUNGEN";
const char de_sec[]=						"SEC";
const char de_ripple_detected[]=			"DIODEN TEST";
const char de_ripple_voltage[]=			"BRUMMSPANNUNG";
const char de_load_voltage[]=				"ALT. LADESPANNUNGEN";
const char de_point_to_battery_press_enter[]=			"AUF BATT.RICHTEN\nEINGABEDRÜCKEN";
const char de_ota[]=						"OTA AKTUALISIEREN";
const char de_test_code[]=				"PRÜFCODE";
const char de_connection_status[]=		"VERBINDUNGSSTATUS";
const char de_scan_vin_code[]=			"DEN VIN-CODE\nSCANNEN?";
const char de_vin_code[]=					"VIN-CODE";
const char de_invalid_vin_rescan[]=		"UNGÜLTIG. NEUSC.\nD. VIN-CODES";
const char de_wifi_power[]=				"WIFI-STROMZUFUHR:";
const char de_wifi_list[]=				"WIFI-LISTE";
const char de_downloading[]=				"WIRD HERUNTERGELADEN...";
const char de_upgrade_cancel[]=			"AKTUALISIERUNG\nLÖSCHEN?";
const char de_upgrading[]=				"WIRD AKTUALISIERT...";
const char de_check_version[]=			"DIE VERSION ÜBERPRÜFEN";
const char de_refresh_list[] =	"*LISTE AKTUALISIEREN";
const char de_limited_connection[]=		"BEGRENZTE VERBINDUNG";
const char de_wifi_connected[]=			"WIFI ANGESCHLOSSEN";
const char de_connecting[]=				"VERBINDUNG WIRD\nHERGESTELLT...";
const char de_wifi_disconnect[]=			"WIFI-VERB. WURDE\nABGEBROCHEN";
const char de_check_connection[]=			"VERBINDUNG WIRD\nÜBERPRÜFT...";
const char de_keep_clamps_connect[]=		"WARN.: KLEM.AN\nBATT. VERBDN HLT";
//const char de_skip[]=						"ÜBERSPRINGEN";
const char de_registration[] = "REGISTRIERUNG";
const char de_scan_qr_code[] = "DEN QR-CODE SCANNEN?";
const char de_registering[] = "REGISTRIERUNG...";


// printer
const char de_prt_battery_test[] = "== BATTERIEPRUFUNG ==";
const char de_prt_by[] = "DURCH:";
const char de_prt_charging_test[] = "LADETEST";
const char de_prt_note[] = "HINWEIS:";
const char de_prt_diode_ripple[] = "DIODE-RIPPLE";
const char de_prt_ir_test[] = "== IR. TEST ==";
const char de_prt_load_off[] = "LAST AUS";
const char de_prt_load_on[] = "LAST EIN";
const char de_prt_rated[] = "BEWERTET:";
const char de_prt_vin_num[] = "VIN NUMMER:";
const char de_prt_soc[] = "LADEZUSTAND";
const char de_prt_soh[] = "ZUSTAND";
const char de_prt_starter_test[] = "ANLSSER-TEST";
const char de_prt_start_stop[] = "==TEST STARTEN/BEENDEN==";
const char de_prt_temp[] = "TEMPERATUR:";
const char de_prt_test_date[] = "TESTDATUM:";
const char de_prt_test_report[] = "TESTBERICHT";

#endif

#if ITALIAN_en
// LCD
const char it_language[] = "ITALIAN";
const char it_battery[]=				"BATTERIA";
const char it_start_stop_test[]=		"TEST START STOP";
const char it_battery_test[]=			"PROVA BATTERIA";
const char it_system_test[]=			"TEST IMPIANTO";
const char it_ir_test[]=				"TEST IR.";
const char it_v_a_meter[]=			"T./A. METRO";
const char it_setting[]=				"IMPOSTAZIONI";
const char it_test_in_vehicle[]=		"TEST SU VEICOLO";
const char it_direct_connect[]=		"COLLEGAMENTO DIRETTO";
const char it_jump_start_post[]=		"COLLEG. ESTERNO\nJUMP START";
const char it_brand[]=				"CASA AUTOMOBILISTICA:";
const char it_model[]=				"MODELLO:";
const char it_version[]=				"VERSIONE";
const char it_test_result[]=			"RISULTATI TEST";
const char it_voltage[]=				"VOLTAGGIO:";
const char it_good_n_pass[]=			"BATT. EFFICIENTE";
const char it_good_n_recharge[]=		"BUONA & RICARICA";
const char it_bad_n_replace[]=			"DIFETT. SOSTIT";
const char it_caution[]=				"ATTENZIONE";
const char it_recharge_n_retest[]=		"RICARICA. RITESTA";
const char it_bad_cell_replace[]=		"CELLA DIF. SOST.";
const char it_battery_type[]=			"TIPO BATTERIA";
const char it_flooded[]=				"LIQUIDO STANDARD";
const char it_agmf[]=					"AGM FLAT PLATE";
const char it_agms[]=					"AGM SPIRALE";
const char it_select_rating[]=		"SEL. STANDARD";
const char it_set_capacity[]=			"SEL. CAPACITA";
const char it_back_light[]=			"RETROILLUMINAZIONE LCD";
const char it_language_select[]=		"SELEZIONE LINGUA";
const char it_clock[]=				"OROLOGIO";
const char it_clear_memory[]=			"CANCELLA MEMORIA";
const char it_information[]=			"INFORMATIONI";
const char it_time_zone[]=			"FUSO ORARIO";
const char it_test_counter[]=			"CONTATORE TEST";
const char it_abort[]=				"INTERROMPERE";
const char it_directly_connect_to_battery[]=	"CONNESSIONE\nDIRETTA BATTERIA";
const char it_insert_vin[]=			"INSERIRE CODICE\nVIN MANUALMENTE?";
const char it_test_not_suitable[]=	"NO GARANZIA,\nCONTINUARE?";
const char it_yes[]=					"SI";
const char it_no[]=					"NO";
const char it_print_result[]=			"STAMPA TEST";
const char it_print_successful[]=		"STAMPA OK,\nRISTAMPARE?";
const char it_check_clamp[]=			"CONTROLLO MORSETTO";
const char it_insert_change_aa[]=		"INSERIRE/SOSTIT.\nBATTERIE AA";
const char it_load_error[]=			"CCA FUORILIMITE O COLLEG ERRATO";
const char it_voltage_high[]=			"TENSIONE ALTA";
const char it_voltage_unstable[]=		"TENSIONE NON STABILE\nATTENDERE STABILZZAZIONE";
const char it_is_it_6v[]=				"E UNA BATTERIA 6V?";
const char it_is_battery_charge[]=	"LA BATTERIA E CARICA?";
const char it_battery_in_vehicle[]=	"BATTERIA SU VEICOLO?";
const char it_ss_test[]=				"TEST SS.";
const char it_bat_test[]=				"TEST BAT.";
const char it_sys_test[]=				"TEST SIST.";
const char it_ir_t[] =					"TEST IR.";
const char it_print[]=				"STAMPA";
const char it_print_q[]=				"STAMPA?";
const char it_erase[]=				"ELIMINA?";
const char it_memory_erase[]=			"CANCELLAZIONE\nMEMORIA?";
const char it_on[] =					"SOPRA";
const char it_off[] =					"OFF";
const char it_date[]=					"DATA:";
const char it_time[]=					"ORA:";
const char it_invalid_vin[]=			"VIN NON COMPATIB\nCON GARANZIA";
const char it_input_vin[]=			"IMMETTERE VIN";
const char it_enter_vin[]=			"IMMETTERE CODICE\nVIN COMPLETO";
const char it_measured[]=				"MISURAZIONE:";
const char it_cranking_voltage[]=		"TENS. AVVIAMENTO";
const char it_lowest_voltage[]=		"TENSIONE MINIMA";
const char it_high[]=					"ELEVATO";
const char it_normal[]=				"NORMALE";
const char it_low[]=					"BASSA";
const char it_no_detect[]=			"NON RILEVATO";
const char it_idle_voltage[]=			"ALT. TENSION AL MINIMO";
const char it_sec[]=					"SEC";
const char it_ripple_detected[]=		"SEGNALE ALTERN";
const char it_ripple_voltage[]=		"OSCILL. TENSIONE";
const char it_load_voltage[]=			"ALT. TENSIONE DI CARICO";
const char it_point_to_battery_press_enter[]=	"PUNTARE BATTERIA\nPREMERE INVIO";
const char it_ota[]=					"AGGIORNAMENTO OTA";
const char it_test_code[]=			"CODICE TEST";
const char it_connection_status[]=	"STATO CONNESSIONE";
const char it_scan_vin_code[]=		"SCANNERIZZARE CODICE VIN?";
const char it_vin_code[]=				"CODICE VIN";
const char it_invalid_vin_rescan[]=	"CODICE VIN NON\nVALIDO, RIPETERE";
const char it_wifi_power[]=			"SEGNALE WIFI:";
const char it_wifi_list[]=			"LISTA WIFI";
const char it_downloading[]=			"DOWNLOAD IN CORSO...";
const char it_upgrade_cancel[]=		"ANNULLARE\nAGGIORNAMENTO?";
const char it_upgrading[]=			"AGGIORNAMENTO\nIN CORSO...";
const char it_check_version[]=		"CONTROLLARE\nVERSIONE";
const char it_refresh_list[] = 			"*AGGIORNA LISTA";
const char it_limited_connection[]=	"CONNESSIONE LIMITATA";
const char it_wifi_connected[]=		"WIFI CONNESSO";
const char it_connecting[]=			"CONNESSIONE IN CORSO...";
const char it_wifi_disconnect[]=		"WIFI DISCONNESSO";
const char it_check_connection[]=		"CONTROLLO CONNESSIONE...";
const char it_keep_clamps_connect[]=	"AVVISO: NON\nDISCONNETT.PINZE";
//const char it_skip[]=					"SALTA";
const char it_registration[] = "REGISTRAZIONE";
const char it_scan_qr_code[] = "SCANNERIZZARE QR CODE?";
const char it_registering[] = "REGISTRAZIONE IN CORSO...";


// printer
const char it_prt_battery_test[] = "== TEST BATTERIA ==";
const char it_prt_by[] = "DI:";
const char it_prt_charging_test[] = "TEST CARICAMENTO";
const char it_prt_note[] = "AVVISO:";
const char it_prt_diode_ripple[] = "OSCILLAZIONE DIODO";
const char it_prt_ir_test[] = "== TEST IR. ==";
const char it_prt_load_off[] = "CARICO OFF";
const char it_prt_load_on[] = "CARICO ON";
const char it_prt_rated[] = "NORMINALE:";
const char it_prt_vin_num[] = "VIN NUMERO:";
const char it_prt_soc[] = "STATO DI CARICA";
const char it_prt_soh[] = "CONDIZIONI DI SALUTE";
const char it_prt_starter_test[] = "TEST AVVIATORE";
const char it_prt_start_stop[] = "==AVVIA FERNA TEST==";
const char it_prt_temp[] = "TEMPERATUR:";
const char it_prt_test_date[] = "DATA DEL TEST:";
const char it_prt_test_report[] = "RAPPORTO TEST";

#endif

#if FRANCAIS_en
// LCD
const char fr_language[] =              "FRANCAIS";
const char fr_battery[]=				"BATTERIE";
const char fr_start_stop_test[]=		"TEST START STOP";
const char fr_battery_test[]=			"TEST BATTERIE";
const char fr_system_test[]=			"TEST DU SYSTEME";
const char fr_ir_test[]=				"TEST IR.";
const char fr_v_a_meter[]=			    "MÈTRE de V./A.";
const char fr_setting[]=				"RÉGLAGES";                          // 20201021 DW "PARAMETRAGE" => "RÉGLAGES"
const char fr_test_in_vehicle[]=		"TEST DANS VÉHIC";
const char fr_direct_connect[]=		    "CONNECTION DIRECTE";
const char fr_jump_start_post[]=		"POSTE DÉMARRAGE\nCOMMANDÉ";
const char fr_brand[]=				    "MARQUE:";                          // 20201021 DW "MARQUE:" => "MARQUE :"
const char fr_model[]=				    "MODÈLE:";                          // 20201021 DW "MODÈLE:" => "MODÈLE :"
const char fr_version[]=				"VERSION";
const char fr_test_result[]=			"RÉSULTATS DU TEST";                 // 20201021 DW "LES RESULTATS DU TEST" => "RÉSULTATS DU TEST"
const char fr_soh[] =                   "SOH:";
const char fr_soc[] =                   "SOC:";
const char fr_cca[] =                   "CCA:";
const char fr_voltage[]=				"TENSION:";
const char fr_good_n_pass[]=			"BONNE BATTERIE";
const char fr_good_n_recharge[]=		"OK ET À RECHARGER";                 // 20201021 DW "OK ET A RECHARGER" => "OK ET À RECHARGER"
const char fr_bad_n_replace[]=			"A REMPLACER";
const char fr_caution[]=				"MARGINALE";                         // 20201111 DW ATTENTION => MARGINALE
const char fr_recharge_n_retest[]=		"RECHARG ET RETEST";                 // 20201021 DW "RECHARGER & RETESTER" => "RECHARG ET RETEST"
const char fr_bad_cell_replace[]=		"CEL DÉF. À REMPL";                  // 20201021 DW "CEL DEF. A REMPL" => "CEL DÉF. À REMPL"
const char fr_battery_type[]=			"TYPE DE BATTERIE";
const char fr_flooded[]=				"LIQUIDE STANDARD";
const char fr_agmf[]=					"AGM PLAQUE PLANE";
const char fr_agms[]=					"AGM SPIRALE";
const char fr_vrla[] =                  "VRLA/GEL";                          // 20201021 DW French and Spanish translation changes "VRLA / GEL" => "VRLA/GEL"
const char fr_select_rating[]=		    "CHOIX DE NORME";
const char fr_set_capacity[]=			"ENT. CAPACITÉ";                     // 20201021 DW French and Spanish translation changes "ENT. CAPASITE" => "ENT. CAPACITÉ"
const char fr_back_light[]=			    "RÉTRO-ÉCLAIRAGE ACL";               // 20201021 DW French and Spanish translation changes "RETRO-ECLAIRAGE LCD" => "RÉTRO-ÉCLAIRAGE ACL"
const char fr_language_select[]=		"CHOISIR LA LANGUE";
const char fr_clock[]=				    "HORLOGE";
const char fr_clear_memory[]=			"VIDE MEMOIRE";
const char fr_information[]=			"INFORMATION";
const char fr_time_zone[]=		    	"FUSEAU HORAIRE";
const char fr_test_counter[]=			"COMPETUR TEST";                     // 20201111 DW EDIT COMPTEUR => COMPETUR
const char fr_abort[]=				    "ABANDONNER";
const char fr_directly_connect_to_battery[]=	"CONNEXION DIRECT\nÀ LA BATTERIE";
const char fr_insert_vin[]=			    "INSÉRER MANUEL\nCODE VIN?";//TODO
const char fr_test_not_suitable[]=	    "TEST INADAPTÉ\nGARANTIE RESTER?";
const char fr_yes[]=					"OUI";
const char fr_no[]=					    "NON";
const char fr_print_result[]=			"IMPRIMER TEST";
const char fr_print_successful[]=		"IMPRESSION OK,\nRÉIMPRIMER?";
const char fr_check_clamp[]=			"VÉRIF JOINT";                       // 20201021 DW French and Spanish translation changes "VERIF JOINT" => "VÉRIF JOINT"
const char fr_insert_change_aa[]=		"INSÉRER/CHANGER\nLES PILES AA";
const char fr_load_error[]=			    "ERREUR CHARGE";
const char fr_voltage_high[]=			"TENSION ÉLEVÉE\nVÉRIF SYSTÈME";
const char fr_voltage_unstable[]=		"TENSION INSTABLE\nATTENDRE STABLE";
const char fr_is_it_6v[]=				"BATTERIE 6 V?";                     // 20201021 DW French and Spanish translation changes "BATTERIE 6V?" => "BATTERIE 6 V?"
const char fr_is_battery_charge[]=	    "LA BATTERIE EST-ELLE CHARGÉE?";
const char fr_battery_in_vehicle[]=	    "BATTERIE DANS\nUN VÉHICULE";
const char fr_ss_test[]=				"TEST SS.";
const char fr_bat_test[]=				"TESTE DE BAT.";                     // 20201111 DW TEST BAT. => TESTE DE  BAT.
const char fr_sys_test[]=				"TESTE DU SYS.";                     // 20201111 DW TEST SYS. => TESTE DU SYS.
const char fr_ir_t[] =					"TEST IR.";
const char fr_print[]=				    "IMPRIMER";
const char fr_print_q[]=				"IMPRIMER?";
const char fr_erase[]=				    "EFFACER?";
const char fr_memory_erase[]=			"EFFACEMENT DE\nLA MÉMOIRE?";
const char fr_on[] =					"SUR";
const char fr_off[] =					"DE";
const char fr_date[]=					"DATE:";                            // 20201021 DW French and Spanish translation changes "DATE:" => "DATE :"
const char fr_time[]=					"HEURE:";                           // 20201021 DW French and Spanish translation changes "L'HEURE:" => "HEURE :"
const char fr_invalid_vin[]=			"VIN NUL RÉSULTAT\nNON GARANTIE";    // TODO
const char fr_input_vin[]=			    "VIN D'ENTRÉE";//TODO
const char fr_enter_vin[]=			    "ENTRER LE VIN COMPLET";//TODO
const char fr_measured[]=				"MESURE:";                           // 20201021 DW French and Spanish translation changes "MESURE:" => "MESURE :"
const char fr_cranking_voltage[]=		"VOLTS DÉMARRAGE";                   // 20201021 DW French and Spanish translation changes "VOLTS DEMARRAGE" => "VOLTS DÉMARRAGE"
const char fr_lowest_voltage[]=		    "TENSION LA PLUS BASSE";
const char fr_high[]=					"HAUT";
const char fr_normal[]=				    "NORMAL";
const char fr_low[]=					"BAS";
const char fr_no_detect[]=			    "NON DÉTECTÉ";                      // 20201111 DW PAS DETECTE => NON DÉTECTÉ
const char fr_idle_voltage[]=			"ALT. TENSION DE MARCHE À VIDE";    // 20201021 DW French and Spanish translation changes "ALT. TENSION DE MARCHE A VIDE" => "ALT. TENSION DE MARCHE À VIDE"
const char fr_sec[]=					"S ";
const char fr_ripple_detected[]=		"TENSION REDR OK";
const char fr_ripple_voltage[]=		    "ONDUL. TENSION";
const char fr_load_voltage[]=			"ALT. TENSION DE CHARGE";
const char fr_point_to_battery_press_enter[]=	"POINT BATTERIE\nAPPUYEZ ENTRÉE";
const char fr_ota[]=					"MISE À NIVEAU DE L'OTA";
const char fr_test_code[]=			    "TEST CODE";
const char fr_connection_status[]=	    "STATUT DE CONNEXION";
const char fr_scan_vin_code[]=		    "NUMÉRISER LE CODE VIN?";//TODO
const char fr_vin_code[]=				"CODE VIN";//TODO
const char fr_invalid_vin_rescan[]=	    "CODE VIN NON\nVALIDE RESCANNEZ";//TODO
const char fr_wifi_power[]=			    "WIFI POWER:";
const char fr_wifi_list[]=			    "LISTE WIFI";
const char fr_downloading[]=			"TÉLÉCHARGEMENT...";
const char fr_upgrade_cancel[]=		    "MISE À JOUR ANNULER?";
const char fr_upgrading[]=			    "MISE À JOUR ...";
const char fr_check_version[]=		    "VERIFIER LA VERSION";
const char fr_refresh_list[] = 			"*RAFRAÎCHIR LA LISTE";
const char fr_limited_connection[]=	    "CONNEXION LIMITÉE";
const char fr_wifi_connected[]=		    "WIFI CONNECTÉ";
const char fr_connecting[]=			    "CONNEXION...";
const char fr_wifi_disconnect[]=		"WIFI DÉCONNECTÉ";
const char fr_check_connection[]=		"VÉRIFIEZ LA\nCONNEXION...";
const char fr_keep_clamps_connect[]=	"GARDER PINCE\nLIÉ À BATTERIE";
//const char fr_skip[]=					"PASSER";
const char fr_registration[] =          "ENREGISTREMENT";
const char fr_scan_qr_code[] =          "SCANNER QR CODE?";
const char fr_registering[] =           "ENREGISTREMENT EN COURS....";
const char fr_off_load[] =              "COUPEZ CONSOMM";                        // 20200803 DW some pictures of sys test changed to text description
const char fr_start_engine[] =          "DEMARRER MOTEUR";                       // 20200803 DW some pictures of sys test changed to text description
const char fr_make_sure_all[] =         "VÉRIFIER SU LES";                       // 20201021 DW edit "VERIFIER SI LES"=>"VÉRIFIER SU LES"
const char fr_load_are_off[] =          "CONSOMM. COUPES";                       // 20201021 DW edit "CONSOMM. COUPES"=>"CONSOMM. COUPES"
const char fr_turn_on_lo[] =            "ACT. LES CHARGES";                      // 20201021 DW edit "ACT. LES CHARGES"=>"ACT. LES CHARGES"
const char fr_and_press_ent[] =         "APPUYEZ ENTRÉE";                        // 20201021 DW edit "APPUYEZ ENTER"=>"APPUYEZ ENTRÉE"
const char fr_turn_eng_2500[] =         "REV ENGINE";
const char fr_AMVM_TURN_ON_AMP[] =      "ACTIV AMPMÈTRE";
const char fr_AMVM_AFTER_ATT[] =        "APRÈS JOINT";
const char fr_AMVM_ENTER_PROCEED[] =    "(ENT POUR CONTIN)";
const char fr_AMVM_TURN_OFF_AMP[] =     "ÉTEINDRE";
const char fr_AMVM_AMP_METER[] =        "AMPÈREMÈTRE";
const char fr_is_24v_system[] =         "SYSTÈME 24 V?";
const char fr_test_each_bat_sep[] =     "TESTER BATTERIES SÉPARÉMENT";
const char fr_connect_to_12v_bat[] =    "CONNECTÉ À BATTERIE 24 V";
const char fr_is_diesel_engine[] =      "MOTEUR DIÉSEL?";
const char fr_rev_engine_for[] =        "FONC MOTEUR";
const char fr_Truck_Group_Test[] =      "ENS TEST?";
const char fr_Of_Bat_In_Par[] =         "ENS TEST";
const char fr_Bat_2[] =                 "BATTERIE x 2";
const char fr_Bat_3[] =                 "BATTERIE x 3";
const char fr_Bat_4[] =                 "BATTERIE x 4";
const char fr_Bat_5[] =                 "BATTERIE x 5";
const char fr_Bat_6[] =                 "BATTERIE x 6";
const char fr_GOOD_PACK[] =             "BON ENSEMBLE";
const char fr_CHECK_PACK[] =            "VÉRIF ENSEMBLE";
const char fr_SEP_PACK_CONNECT_BAT1[] = "SÉPAR ENSEMBLE CONNEX SEUL BATTERIE NO 1";
const char fr_SEP_PACK_CONNECT_BAT2[] = "SÉPAR ENSEMBLE CONNEX SEUL BATTERIE NO 2";
const char fr_SEP_PACK_CONNECT_BAT3[] = "SÉPAR ENSEMBLE CONNEX SEUL BATTERIE NO 3";
const char fr_SEP_PACK_CONNECT_BAT4[] = "SÉPAR ENSEMBLE CONNEX SEUL BATTERIE NO 4";
const char fr_SEP_PACK_CONNECT_BAT5[] = "SÉPAR ENSEMBLE CONNEX SEUL BATTERIE NO 5";
const char fr_SEP_PACK_CONNECT_BAT6[] = "SÉPAR ENSEMBLE CONNEX SEUL BATTERIE NO 6";
const char fr_GR_TEST_BAT1_START[] =    "ENS TEST BATTERIE\nNO 1 DÉMAR TEST";
const char fr_GR_TEST_BAT2_START[] =    "ENS TEST BATTERIE\nNO 2 DÉMAR TEST";
const char fr_GR_TEST_BAT3_START[] =    "ENS TEST BATTERIE\nNO 3 DÉMAR TEST";
const char fr_GR_TEST_BAT4_START[] =    "ENS TEST BATTERIE\nNO 4 DÉMAR TEST";
const char fr_GR_TEST_BAT5_START[] =    "ENS TEST BATTERIE\nNO 5 DÉMAR TEST";
const char fr_GR_TEST_BAT6_START[] =    "ENS TEST BATTERIE\nNO 6 DÉMAR TEST";
const char fr_Pack_Report[] =           "REPORT ENSEMBLE";
const char fr_BAT_NUM[] =               "BATTERIE NO";
const char fr_ad_ac_press_enter[] =     "ACTION CONS :\nVÉRIF CONNEX\nAPPUYEZ ENTRÉE";
const char fr_SYS_TURN_OFF_LOAD[] =     "ÉTEINDRE LES CHARGES\nET LE MOTEUR";
const char fr_Turn_Headlight_list[] =   "ALLUMER LES LUMIÈRES";
const char fr_please_en_start_list[] =  "INSÉRER DÉMARRER";
// printer
const char fr_prt_battery_test[] =  "==TEST BATTERIE==";
const char fr_prt_by[] =            "PAR:";
const char fr_prt_charging_test[] = "TEST CHARGE";
const char fr_prt_note[] =          "REMARQUE:";
const char fr_prt_diode_ripple[] =  "ONDUL. TENSION";               //20201110 dw edit ONDULATION DE LA DIODE => ONDUL. TENSION
const char fr_prt_ir_test[] =       "==TEST IR.==";
const char fr_prt_load_off[] =      "CHARGE OFF";
const char fr_prt_load_on[] =       "CHARGE ON";
const char fr_prt_rated[] =         "NOMINAL:";
const char fr_prt_vin_num[] =       "VIN NUMERO:";
const char fr_prt_soc[] =           "ÉTAT DE CHARGE";
const char fr_prt_soh[] =           "ÉTAT DE SANTÉ";
const char fr_prt_starter_test[] =  "VOLTS DÉMARRAGE";              //20201111 DW EDIT TEST DEMARRAGE => VOLTS DÉMARRAGE
const char fr_prt_start_stop[] =    "==DEMARRER ARRETER TEST==";
const char fr_prt_temp[] =          "TEMPÉRATURE:";
const char fr_prt_test_date[] =     "DATE TEST:";
const char fr_prt_test_report[] =   "RAPPORT TEST";
const char fr_pack_test[] =         "==ENS TEST==";
const char fr_warning_word1[] =     "==================";
const char fr_warning_word2[] =     "ACTION CONS :";
const char fr_warning_word3[] =     "VÉRIF CONNEX";                //20201111 DW EDIT CHECK COMMECTION
const char fr_P_BATTERT_NUM[]=      "PILE NO ";                    //20201111 DW EDIT VÉRIF CONNEX => PILE NO
const char fr_prt_jis[] =           "JIS:";
#endif

#if ESPANOL_en
// LCD
const char es_language[] = 				"ESPAÑOL";
const char es_battery[]=				"BATERÍA";                                   // 20201021 DW "BATERIA" => BATERÍA
const char es_start_stop_test[]=		"TEST START STOP";
const char es_battery_test[]=			"PRUEBA BATERÍA";                            // 20201111 DW PRUEBA DE BATERÍA => PRUEBA BATERÍA
const char es_system_test[]=			"PRUEBA SISTEMA";                            // 20201111 DW PRUEBA DEL SISTEMA => PRUEBA SISTEMA
const char es_ir_test[]=				"TEST IR.";
const char es_v_a_meter[]=				"MEDIDOR de V./A.";                          // 20201021 DW "V./A. METRO" => MEDIDOR de V./A.
const char es_setting[]=				"CONFIGURACIÓN";                             // 20201021 DW "PROGRAMAR" => "CONFIGURACIÓN"
const char es_test_in_vehicle[]=		"TEST EN AUTO";
const char es_direct_connect[]=			"CONEXIÓN DIRECTA";
const char es_jump_start_post[]=		"TERMINAL DE ARRANQUE";
const char es_brand[]=					"MARCA:";
const char es_model[]=					"MODELO:";
const char es_version[]=				"VERSIÓN";
const char es_test_result[]=			"RESULTADOS DE PRUEBA";
const char es_voltage[]=				"VOLTAJE:";
const char es_good_n_pass[]=			"BUENA BATERÍA";                             // 20201021 DW "BUENA" => "BUENA BATERÍA"
const char es_good_n_recharge[]=		"BUENA - RECARGUE";                          // 20201021 DW "BUENA-RECARGAR" => "BUENA - RECARGUE"
const char es_bad_n_replace[]=			"MALA - REEMPLACE";
const char es_caution[]=				"MARGINAL";                                  // 20201111 DW PRECAUCIÓN => MARGINAL
const char es_recharge_n_retest[]=		"RECARGUE Y PROBAR";
const char es_bad_cell_replace[]=		"DEFECTUOSA - REEMPLACE";                    // 20201111 DW CELDA DEFECTUOSA - REEMPLACE => DEFECTUOSA - REEMPLACE
const char es_battery_type[]=			"TIPO DE BATERIA";
const char es_flooded[]=				"HÚMEDA";                                    // 20201021 DW "LIQUIDO ESTANDAR" => "HÚMEDA"
const char es_agmf[]=					"PLACA PLANA AGM";                           // 20201021 DW "AGM PLANA" => "PLACA PLANA AGM"
const char es_agms[]=					"ESPIRAL AGM";                               // 20201021 DW "AGM ESPIRAL" => "ESPIRAL AGM"
const char es_select_rating[]=			"SELECCIONE RANGO";                          // 20201111 DW SELECCIONE CAPACIDAD NOMINAL => SELECCIONE RANGO
const char es_set_capacity[]=			"DEFINA LA CAPACIDAD";                       // 20201021 DW "PONGA CAPACIDAD" => "DEFINA LA CAPACIDAD"
const char es_back_light[]=				"LUZ DE FONDO LCD";                          // 20201021 DW "LUZ DE FONDO DEL LCD" => "LUZ DE FONDO LCD"
const char es_language_select[]=		"SELECCIONE IDIOMA";                         // 20201021 DW "SELECCIONAR IDIOMAS" => "SELECCIONE IDIOMA"
const char es_clock[]=					"RELOJ";
const char es_clear_memory[]=			"DESPEJAR MEMORIA";
const char es_information[]=			"INFORMACIÓN";                               // 20201021 DW "INFORMACION" => "INFORMACIÓN"
const char es_time_zone[]=				"ZONA HORARIA";
const char es_test_counter[]=			"CONTADOR DE PRUEBAS";                       // 20201021 DW "CONTADOR DE PRUEBA" => "CONTADOR DE PRUEBAS"
const char es_abort[]=					"ABANDONAR";                                 // 20201021 DW "CANCELAR" => "ABANDONAR"
const char es_directly_connect_to_battery[]=	"CONEXIÓN DIRECTA\nA LA BATERÍA";
const char es_insert_vin[]=				"INTRO. EL CÓD.\nVIN MANUALMENTE?";
const char es_test_not_suitable[]=		"PRUEBA-NO-GARANTÍA,\nCONTINUAR?";
const char es_yes[]=					"SI";
const char es_no[]=						"NO";
const char es_print_result[]=			"IMPRIMIR RESULTADO";                        // 20201021 DW "IMPRIMIR TEST" => "IMPRIMIR RESULTADO"
const char es_print_successful[]=		"IMPRESIÓN OK IMPRIMIR\nOTRA VEZ?";         // 20201021 DW "IMPRI. OK, IMPRIMIR\nOTRA VEZ?" => "IMPRESIÓN OK ¿IMPRIMIR\nOTRA VEZ?"
const char es_check_clamp[]=			"COMPRUEBE PINZA";                           // 20201021 DW "SUJETADOR" => "COMPRUEBE PINZA"
const char es_insert_change_aa[]=		"PONER O CAMBIAR\nBATERÍAS AA";
const char es_load_error[]=				"ERROR DE CARGA";
const char es_voltage_high[]=			"ALTO VOLTAJE\nCOMPRUEBE SISTEMA";
const char es_voltage_unstable[]=		"VOLTAJE INESTABLE\nESPERE ESTABILIDAD";
const char es_is_it_6v[]=				"ES UNA BAT. DE 6V?";                       // 20201021 DW "ES UNA BAT. DE 6V?" => "¿ES UNA BAT. DE 6V?"
const char es_is_battery_charge[]=		"LA BATERÍA ESTA CARGADA?";                 // 20201021 DW "ESTA LA BATERIA CARGADA?" => "¿LA BATERÍA ESTA CARGADA?"
const char es_battery_in_vehicle[]=		"BATERÍA EN VEHÍCULO?";                     // 20201021 DW "BATERÍA EN EL\nVEHÍCULO" => "¿BATERÍA EN VEHÍCULO?"
const char es_ss_test[]=				"E/A. PRUEBA";
const char es_bat_test[]=				"PRUEBA BAT.";                              // 20201021 DW "BAT. PRUEBA" => "PRUEBA BAT.:"
const char es_sys_test[]=				"PRUEBA SIST.";                             // 20201021 DW "SIS. PRUEBA" => "PRUEBA SIST.:"
const char es_ir_t[] =					"TEST IR.";
const char es_print[]=					"IMPRIMIR";
const char es_print_q[]=				"IMPRIMIR?";                                // 20201021 DW "IMPRIMIR?" => "¿IMPRIMIR?"
const char es_erase[]=					"BORRAR?";                                  // 20201021 DW "ELIMINAR?" => "¿BORRAR?"
const char es_memory_erase[]=			"BORRAR MEMORIA?";
const char es_on[] =					"ENCENDIDO";                                 // 20201021 DW "EN" => "ENCENDIDO"
const char es_off[] =					"APAGADO";
const char es_date[]=					"FECHA:";
const char es_time[]=					"HORA:";
const char es_invalid_vin[]=			"VIN INVALIDO,\nRESULT-NO-GARANTIA";
const char es_input_vin[]=				"INTRODUCIR VIN";
const char es_enter_vin[]=				"INTRODUCIR EL\nVIN COMPLETO";
const char es_measured[]=				"MEDIDO:";
const char es_cranking_voltage[]=		"VOLTAJE DE ARRANQUE";                      // 20201021 DW "VOLTIOS ARRANQUE" => "VOLTAJE DE ARRANQUE"
const char es_lowest_voltage[]=			"VOLTAJE MÁS BAJO";
const char es_high[]=					"ALTO";
const char es_normal[]=					"NORMAL";
const char es_low[]=					"BAJO";
const char es_no_detect[]=				"NO DETECTADO";
const char es_idle_voltage[]=			"ALT. VOLTAJE INACTIVO";                    // 20201110 DW "ALT. VOLTAJE AL RALENTI" => ALT. VOLTAJE INACTIVO
const char es_sec[]=					"SEG. ";                                    // 20201021 DW "SEGUNDO" => "SEGUNDOS"
const char es_ripple_detected[]=		"CORRIENTE DE RIZADO DETECTADA";            // 20201021 DW "RIZADO DETECTADO" => "CORRIENTE DE RIZADO DETECTADA"
const char es_ripple_voltage[]=			"VOLTAJE DE RIZADO";                        // 20201021 DW "VOLTS RIZADO" => "VOLTAJE DE RIZADO"
const char es_load_voltage[]=			"ALT. VOLTAJE DE CARGA";                    // 20201021 DW "ALT. VOLTAJE DE CARGAS" => "ALT. VOLTAJE DE CARGA"
const char es_point_to_battery_press_enter[]=	"SEÑALE LA BAT.\nPRESIONE ENTER";
const char es_ota[]=					"ACTUALIZACIÓN OTA";
const char es_test_code[]=				"CÓDIGO DE PRUEBA";
const char es_connection_status[]=		"ESTADO DE LA CONEXIÓN";
const char es_scan_vin_code[]=			"ESCANEAR CÓDIGO VIN?";
const char es_vin_code[]=				"CÓDIGO VIN";
const char es_invalid_vin_rescan[]=		"FALLA CÓD. VIN,\nESCANEAR";
const char es_wifi_power[]=				"PODER WIFI:";
const char es_wifi_list[]=				"LISTA DE WIFI";
const char es_downloading[]=			"DESCARGANDO...";
const char es_upgrade_cancel[]=			"CANCELAR CARGA?";
const char es_upgrading[]=				"CARGANDO...";
const char es_check_version[]=			"VERIFICANDON\nLA VERSIÓN";
const char es_refresh_list[] = 			"*REFRESCAR LISTA";
const char es_limited_connection[]=		"CONEXIÓN LIMITADA";
const char es_wifi_connected[]=			"WIFI CONECTADO";
const char es_connecting[]=				"CONECTANDO...";
const char es_wifi_disconnect[]=		"WIFI DESCONECTADO";
const char es_check_connection[]=		"VERIFICAR LA\nCONEXIÓN...";
const char es_keep_clamps_connect[]=	"ADVERTENCIA: MANTENGA\nLAS PINZAS CONECTADAS\nA LA BATERÍA";
//const char es_skip[]=					"OMITIR";
const char es_registration[] =          "REGISTRO";
const char es_scan_qr_code[] =          "SCANEAR CÓDIGO QR?";
const char es_registering[] =           "REGISTRANDO...";
const char es_off_load[] =              "APAGUE LAS CARGAS";
const char es_start_engine[] =          "ARRANQUE MOTOR";
const char es_make_sure_all[] =         "COMPRUEBE QUE TODAS";
const char es_load_are_off[] =          "LAS CARGAS ESTÉN APAGADAS";
const char es_turn_on_lo[] =            "APAGUE LAS CARGAS Y";
const char es_and_press_ent[] =         "PRESIONE ENTER";
const char es_turn_eng_2500[] =         "REV ENGINE";
const char es_AMVM_TURN_ON_AMP[] =      "ENCIENDE EL AMPERÍMETRO";
const char es_AMVM_AFTER_ATT[] =        "DESPUÉS DE CONECTAR";
const char es_AMVM_ENTER_PROCEED[] =    "(ENTRAR PARA CONTINUAR)";
const char es_AMVM_TURN_OFF_AMP[] =     "APAGUE";
const char es_AMVM_AMP_METER[] =        "AMPERÍMETRO";
const char es_is_24v_system[] =         "ES UN SIST. DE 24 V?";
const char es_test_each_bat_sep[] =     "PROBAR CADA BATERÍA POR SEPARADO";
const char es_connect_to_12v_bat[] =    "CONECTE A UNA BATERÍA 12V";
const char es_is_diesel_engine[] =      "ES UN MOTOR DIESEL?";
const char es_rev_engine_for[] =        "ACELERE MOTOR POR";
const char es_Truck_Group_Test[] =      "PRUEBA DE PACK?";
const char es_Of_Bat_In_Par[] =         "PRUEBA DE PACK";
const char es_Bat_2[] =                 "BATERIA x 2";
const char es_Bat_3[] =                 "BATERIA x 3";
const char es_Bat_4[] =                 "BATERIA x 4";
const char es_Bat_5[] =                 "BATERIA x 5";
const char es_Bat_6[] =                 "BATERIA x 6";
const char es_GOOD_PACK[] =             "BUEN PACK";
const char es_CHECK_PACK[] =            "COMPRUEBE EL PACK DE BATERÍAS";
const char es_SEP_PACK_CONNECT_BAT1[] = "SEPARE PACK SOLO CONECTAR BATERÍA NO1";
const char es_SEP_PACK_CONNECT_BAT2[] = "SEPARE PACK SOLO CONECTAR BATERÍA NO2";
const char es_SEP_PACK_CONNECT_BAT3[] = "SEPARE PACK SOLO CONECTAR BATERÍA NO3";
const char es_SEP_PACK_CONNECT_BAT4[] = "SEPARE PACK SOLO CONECTAR BATERÍA NO4";
const char es_SEP_PACK_CONNECT_BAT5[] = "SEPARE PACK SOLO CONECTAR BATERÍA NO5";
const char es_SEP_PACK_CONNECT_BAT6[] = "SEPARE PACK SOLO CONECTAR BATERÍA NO6";
const char es_GR_TEST_BAT1_START[] =    "PRUEBA DE PACK BATERÍA NO 1 INICIAR PRUEBA";
const char es_GR_TEST_BAT2_START[] =    "PRUEBA DE PACK BATERÍA NO 2 INICIAR PRUEBA";
const char es_GR_TEST_BAT3_START[] =    "PRUEBA DE PACK BATERÍA NO 3 INICIAR PRUEBA";
const char es_GR_TEST_BAT4_START[] =    "PRUEBA DE PACK BATERÍA NO 4 INICIAR PRUEBA";
const char es_GR_TEST_BAT5_START[] =    "PRUEBA DE PACK BATERÍA NO 5 INICIAR PRUEBA";
const char es_GR_TEST_BAT6_START[] =    "PRUEBA DE PACK BATERÍA NO 6 INICIAR PRUEBA";
const char es_Pack_Report[] =           "REPORTE DE PACK";
const char es_BAT_NUM[] =               "BATERÍA NO";
const char es_ad_ac_press_enter[] =     "ACCIÓN SUGERIDA:\nREVISAR CONEXIÓN\nPRESIONE ENTER";
const char es_SYS_TURN_OFF_LOAD[] =     "APAGUE LAS CARGAS\nY EL MOTOR";
const char es_Turn_Headlight_list[] =   "ENCIENDE LAS LUCES";
const char es_please_en_start_list[] =  "PRESIONE ENTER\nPARA COMENZAR";
// printer
const char es_prt_battery_test[] = 		"==PRUEBA DE BATERÍA==";
const char es_prt_by[] = 				"POR:";                              // 20201111 DW NOTE: => POR:
const char es_prt_charging_test[] = 	"PRUEBA DE CARGA";
const char es_prt_note[] = 				"NOTA:";                             // 20201111 DW AVISO: => NOTA:
const char es_prt_diode_ripple[] = 		"VOLTAJE DE RIZADO";                 // 20201111 DW EDIT DIODO RIZADO => VOLTAJE DE RIZADO
const char es_prt_ir_test[] = 			"==VERIFICACION IR==";
const char es_prt_load_off[] = 			"CARGA APAGADA";
const char es_prt_load_on[] = 			"CARGA ENCENDIDA";
const char es_prt_rated[] = 			"CAPACIDAD NOMINAL:";
const char es_prt_vin_num[] = 			"VIN NUMERO:";
const char es_prt_soc[] = 				"ESTADO DE LA CARGA";
const char es_prt_soh[] = 				"ESTADO DE SALUD";
const char es_prt_starter_test[] = 		"VOLTAJE DE ARRANQUE";               // 20201111 DW EDIT PRUEBA DE ENCENDIDO => VOLTAJE DE ARRANQUE
const char es_prt_start_stop[] = 		"==INICIAR DETENER VERIFICACION==";
const char es_prt_temp[] = 				"TEMPERATURA:";
const char es_prt_test_date[] = 		"FECHA DE LA PRUEBA:";
const char es_prt_test_report[] = 		"REPORTE DE PRUEBA";
const char es_pack_test[] =             "==PRUEBA DE PACK==";
const char es_warning_word1[] =         "==================";
const char es_warning_word2[] =         "ACCIÓN SUGERIDA:";
const char es_warning_word3[] =         "COMPRUEBE CONEXIÓN";
const char es_P_BATTERT_NUM[]=          "BATERÍA NO ";

#endif

#if PORTUGUES_en
// LCD
const char pt_language[] = "PORTUGUÊS";
const char pt_battery[]=				"BATERIA";
const char pt_start_stop_test[]=		"TEST START STOP";
const char pt_battery_test[]=			"TESTE de BATERIA";
const char pt_system_test[]=			"TESTE DO SISTEMA";
const char pt_ir_test[]=				"IR. TESTE";
const char pt_v_a_meter[]=				"V./A. METRO";
const char pt_setting[]=				"PROGRAMACOES";
const char pt_test_in_vehicle[]=		"TESTE VEICULO";
const char pt_direct_connect[]=			"LIGAÇÃO DIRETA";
const char pt_jump_start_post[]=		"ARRANQUE DE PARTIDA";
const char pt_brand[]=					"MARCA:";
const char pt_model[]=					"MODELO:";
const char pt_version[]=				"VERSÃO";
const char pt_test_result[]=			"RESULTADO DE PROVA";
const char pt_voltage[]=				"VOLTAGEM:";
const char pt_good_n_pass[]=			"BOA & PASSA";
const char pt_good_n_recharge[]=		"BOA & CARREGUE";
const char pt_bad_n_replace[]=			"RUIM & SUBSTITUA";
const char pt_caution[]=				"CUIDADO";
const char pt_recharge_n_retest[]=		"CARREGUE & TESTE";
const char pt_bad_cell_replace[]=		"CEL. RUIM-SUBST.";
const char pt_battery_type[]=			"TIPO DE BATERIA";
const char pt_flooded[]=				"LIQUIDO STANDARD";
const char pt_agmf[]=					"AGM PLANA";
const char pt_agms[]=					"AGM ESPIRAL";
const char pt_select_rating[]=			"SELEC. PADRAO";
const char pt_set_capacity[]=			"CAPACIDADE";
const char pt_back_light[]=				"LUZ DE FUNDO DO LCD";
const char pt_language_select[]=		"SELECAO DE IDIOMAS";
const char pt_clock[]=					"RELÓGIO";
const char pt_clear_memory[]=			"DALIR A MEMORIA";
const char pt_information[]=			"INFORMACAO";
const char pt_time_zone[]=				"FUSO HORÁRIO";
const char pt_test_counter[]=			"CONTADOR TESTE";const char pt_abort[]=					"CANCELAR";
const char pt_directly_connect_to_battery[]=	"LIGAÇÃO DIRETA\nÀ BATERIA";
const char pt_insert_vin[]=				"INSERIR CÓDIGO\nVIN MANUALMENTE?";//TODO
const char pt_test_not_suitable[]=		"TEST INADQUADO P\nGARANTIA, CONT?";
const char pt_yes[]=					"SIM";
const char pt_no[]=						"NAO";
const char pt_print_result[]=			"IMPRIMIR TESTE";
const char pt_print_successful[]=		"IMP. COM ÈXITO,\nIMP. NOVAMENTE?";
const char pt_check_clamp[]=			"CHECAR A BRACADEIRA";
const char pt_insert_change_aa[]=		"INSIRA OU MUDE\nAS PILHAS AA";
const char pt_load_error[]=				"ERROR DE CARGA";
const char pt_voltage_high[]=			"ALTA VOLTAGEM";
const char pt_voltage_unstable[]=		"VOLTAGEM INSTAVEL\nESPERAR PARA ESTABILIZAR";
const char pt_is_it_6v[]=				"ISSO E UMA BATERIA 6V?";
const char pt_is_battery_charge[]=		"A BATTERIA ESTA CARREGADA?";
const char pt_battery_in_vehicle[]=		"BATERIA NO VEÍCULO";
const char pt_ss_test[]=				"PROVA C/P.";
const char pt_bat_test[]=				"PROVA BAT.";
const char pt_sys_test[]=				"PROVA SIS.";
const char pt_ir_t[] =					"IR. TESTE";
const char pt_print[]=					"IMPRIMIR";
const char pt_print_q[]=				"IMPRIMIR?";
const char pt_erase[]=					"APAGAR?";
const char pt_memory_erase[]=			"APAGAR MEMÓRIA?";
const char pt_on[] =					"EM";
const char pt_off[] =					"FORA";
const char pt_date[]=					"DATA:";
const char pt_time[]=					"HORA:";
const char pt_invalid_vin[]=			"VIN INVÁL, INADQ P GARANTIA";//TODO
const char pt_input_vin[]=				"INSIRA VIN";//TODO 
const char pt_enter_vin[]=				"INSIRA VIN COMPLETO";//TODO 
const char pt_measured[]=				"MEDIDO:";
const char pt_cranking_voltage[]=		"VOLTS DE PARTIDA";
const char pt_lowest_voltage[]=			"VOLTAGEM MÍNIMA";
const char pt_high[]=					"ALTO";
const char pt_normal[]=					"NORMALE";
const char pt_low[]=					"BAIXO";
const char pt_no_detect[]=				"NAO DETETADO";
const char pt_idle_voltage[]=			"ALT. VOLTS AO RALENTI";
const char pt_sec[]=					"SEC";
const char pt_ripple_detected[]=		"RIPPLE PRESENTE";
const char pt_ripple_voltage[]=			"TENSÃO RIPPLE";
const char pt_load_voltage[]=			"ALT. VOLT. DE CARREGAMENTO";//TODO too long
const char pt_point_to_battery_press_enter[]=	"APONTE P/ BATERIA\nPRESSIONE ENTER";
const char pt_ota[]=					"UPGRADE OTA";
const char pt_test_code[]=				"CÓDIGO DE TESTE";
const char pt_connection_status[]=		"ESTADO DA CONEXÃO";
const char pt_scan_vin_code[]=			"DIGITALIZE CÓDIGO VIN?";//TODO
const char pt_vin_code[]=				"CÓDIGO VIN";//TODO
const char pt_invalid_vin_rescan[]=		"CÓD VIN INVÁL.\nREDIGITALIZE";//TODO
const char pt_wifi_power[]=				"WIFI:";
const char pt_wifi_list[]=				"LISTA WIFI";
const char pt_downloading[]=			"A DESCARREGAR...";
const char pt_upgrade_cancel[]=			"CANCELAR UPGRADE?";
const char pt_upgrading[]=				"A FAZER UPGRADE...";
const char pt_check_version[]=			"VERIFIQUE A VERSÃO";
const char pt_refresh_list[] = 			"*ATUALIZE LISTA";
const char pt_limited_connection[]=		"CONEXÃO LIMITADA";
const char pt_wifi_connected[]=			"WIFI CONETADA";
const char pt_connecting[]=				"A CONETAR...";
const char pt_wifi_disconnect[]=		"WIFI DESCONETADA";
const char pt_check_connection[]=		"VERIFIQUE A CONEXÃO...";
const char pt_keep_clamps_connect[]=	"N: MANT. GRAMPO/\nBATERIA CONETADO";
//const char pt_skip[]=					"SALTAR";
const char pt_registration[] = "REGISTO";
const char pt_scan_qr_code[] = "LER CÓDIGO QR?";
const char pt_registering[] = "A REGISTAR...";


// printer
const char pt_prt_battery_test[] = 		"==TESTE DA BATERIA==";
const char pt_prt_by[] = 				"DE:";
const char pt_prt_charging_test[] = 	"TESTE DE CARREGAMENTO";
const char pt_prt_note[] = 				"ATENCAO:";
const char pt_prt_diode_ripple[] = 		"OSCILACAO DO DIODO";
const char pt_prt_ir_test[] = 			"==TEST DA IR.==";
const char pt_prt_load_off[] = 			"DESCARREGAR";
const char pt_prt_load_on[] = 			"CARREGAR";
const char pt_prt_rated[] = 			"AVALIADO:";
const char pt_prt_vin_num[] = 			"VIN NUMERO:";
const char pt_prt_soc[] = 				"ESTADO DA CARGA";
const char pt_prt_soh[] = 				"ESTADO DE SAUDE";
const char pt_prt_starter_test[] = 		"TEST DE INICIO";
const char pt_prt_start_stop[] = 		"==TESTE START STOP==";
const char pt_prt_temp[] = 				"TEMPERATURA:";
const char pt_prt_test_date[] = 		"DATA DE TESTE:";
const char pt_prt_test_report[] = 		"RELAORIO DO TESTE";
#endif

#if NEDERLANDS_en
// LCD
const char ned_language[] = "NEDERLANDS";
const char ned_battery[]=				"ACCU";
const char ned_start_stop_test[]=		"START-STOP TEST";
const char ned_battery_test[]=			"ACCUTEST";
const char ned_system_test[]=			"SYSTEEMTEST";
const char ned_ir_test[]=				"IR. TEST";
const char ned_v_a_meter[]=			"V./A. METER";
const char ned_setting[]=				"INSTELLING";
const char ned_test_in_vehicle[]=		"TEST IN VOERTUIG";
const char ned_direct_connect[]=		"RECHTSTREEKSE VERBINDING";
const char ned_jump_start_post[]=		"JUMP START POST";
const char ned_brand[]=				"MERK:";
const char ned_model[]=				"MODEL:";
const char ned_version[]=				"VERSIE";
const char ned_test_result[]=			"TESTRESULTAAT";
const char ned_voltage[]=				"SPANNING:";
const char ned_good_n_pass[]=			"GOED EN GESLAAGD";
const char ned_good_n_recharge[]=		"GOED EN OPLADEN";
const char ned_bad_n_replace[]=			"SLECHT EN VERVANGEN";
const char ned_caution[]=				"VOORZICHTIG";
const char ned_recharge_n_retest[]=		"OPLADEN EN OPNIEUW TESTEN";
const char ned_bad_cell_replace[]=		"SLECHTE CEL VERVANGEN";
const char ned_battery_type[]=			"ACCUTYPE";
const char ned_flooded[]=				"NAT";
const char ned_agmf[]=					"AGM PLATTE PLAAT";
const char ned_agms[]=					"AGM SPIRAAL";
const char ned_select_rating[]=			"SELECTEER CATEGORIE";
const char ned_set_capacity[]=			"CAPACITEIT INSTELLEN";
const char ned_back_light[]=			"LCD-VERLICHTING";
const char ned_language_select[]=		"TAALKEUZE";
const char ned_clock[]=					"KLOK";
const char ned_clear_memory[]=			"GEHEUGEN WISSEN";
const char ned_information[]=			"INFORMATIE";
const char ned_time_zone[]=				"TIJDZONE";
const char ned_test_counter[]=			"TESTTELLER";
const char ned_abort[]=					"AFBREKEN";
const char ned_directly_connect_to_battery[]=	"RECHTSTREEKSE VERBINDING MET ACCU";
const char ned_insert_vin[]=			"VIN-CODE HANDMATIG INVOEREN?";
const char ned_test_not_suitable[]=		"TEST ONGESCHIKT VOOR GARANTIE, DOORGAAN?";
const char ned_yes[]=					"JA";
const char ned_no[]=					"NEE";
const char ned_print_result[]=			"RESULTAAT AFDRUKKEN";
const char ned_print_successful[]=		"AFDRUKKEN GESLAAGD, NOGMAALS AFDRUKKEN?";
const char ned_check_clamp[]=			"KLEM CONTROLEREN";
const char ned_insert_change_aa[]=		"AA-BATTERIJEN PLAATSEN OF VERVANGEN";
const char ned_load_error[]=			"BELASTINGFOUT";
const char ned_voltage_high[]=			"SPANNING HOOG";
const char ned_voltage_unstable[]=		"SPANNING INSTABIEL\nWACHT TOT STABIEL";
const char ned_is_it_6v[]=				"IS HET EEN 6V-ACCU?";
const char ned_is_battery_charge[]=		"IS ACCU GELADEN?";
const char ned_battery_in_vehicle[]=	"ACCU IN VOERTUIG";
const char ned_ss_test[]=				"SS. TEST";
const char ned_bat_test[]=				"ACCU TEST";
const char ned_sys_test[]=				"SYS. TEST";
const char ned_ir_t[] =					"IR. TEST";
const char ned_print[]=					"AFDRUKKEN";
const char ned_print_q[]=				"AFDRUKKEN?";
const char ned_erase[]=					"WISSEN?";
const char ned_memory_erase[]=			"GEHEUGEN WISSEN?";
const char ned_on[] =					"AAN";
const char ned_off[] =					"UIT";
const char ned_date[]=					"DATUM:";
const char ned_time[]=					"TIJD:";
const char ned_invalid_vin[]=			"ONGELDIGE VIN, RESULTAAT ONGESCHIKT VOOR GARANTIE";
const char ned_input_vin[]=				"VIN INVOEREN";
const char ned_enter_vin[]=				"VOLLEDIGE VIN INVOEREN";
const char ned_measured[]=				"GEMETEN:";
const char ned_cranking_voltage[]=		"STARTSPANNING";
const char ned_lowest_voltage[]=		"LAAGSTE SPANNING";
const char ned_high[]=					"HOOG";
const char ned_normal[]=				"NORMAAL";
const char ned_low[]=					"LAAG";
const char ned_no_detect[]=				"NIET GEDETECTEERD";
const char ned_idle_voltage[]=			"ALT. SPANNING ONBELAST";
const char ned_sec[]=					"SEC";
const char ned_ripple_detected[]=		"RIMPEL GEDETECTEERD";
const char ned_ripple_voltage[]=		"RIMPELSP.";
const char ned_load_voltage[]=			"ALT. SPANNING BELAST";
const char ned_point_to_battery_press_enter[]=	"OP ACCU RICHTEN\nOP ENTER DRUKKEN";
const char ned_ota[]=					"OTA-UPGRADE";
const char ned_test_code[]=				"CODE TESTEN";
const char ned_connection_status[]=		"VERBINDINGSSTATUS";
const char ned_scan_vin_code[]=			"VIN-CODE SCANNEN?";
const char ned_vin_code[]=				"VIN-CODE";
const char ned_invalid_vin_rescan[]=	"ONGELDIGE VIN-CODE\nHERSCANNEN";
const char ned_wifi_power[]=			"WIFI-VERMOGEN:";
const char ned_wifi_list[]=				"WIFI-LIJST";
const char ned_downloading[]=			"DOWNLOADEN...";
const char ned_upgrade_cancel[]=		"UPGRADE ANNULEREN?";
const char ned_upgrading[]=				"BEZIG MET UPGRADE...";
const char ned_check_version[]=			"VERSIE CONTROLEREN";
const char ned_refresh_list[] = 		"*LIJST VERNIEUWEN";
const char ned_limited_connection[]=	"BEPERKTE VERBINDING";
const char ned_wifi_connected[]=		"WIFI VERBONDEN";
const char ned_connecting[]=			"VERBINDING MAKEN...";
const char ned_wifi_disconnect[]=		"WIFI-VERBINDING VERBROKEN";
const char ned_check_connection[]=		"VERBINDING CONTROLEREN...";
const char ned_keep_clamps_connect[]=	"WAARSCHUWING: HOUD KLEMMEN OP ACCU AANGESLOTEN.";
//const char ned_skip[]=					"OVERSLAAN";
const char ned_registration[] = "REGISTRATIE";
const char ned_scan_qr_code[] = "QR-CODE SCANNEN?";
const char ned_registering[] = "REGISTREREN....";


// printer
const char ned_prt_battery_test[] = 		"==ACCUTEST==";
const char ned_prt_by[] = 					"DOOR:";
const char ned_prt_charging_test[] = 		"LAADTEST";
const char ned_prt_note[] = 				"OPMERKING:";
const char ned_prt_diode_ripple[] =			"DIODERIMPEL";
const char ned_prt_ir_test[] = 				"==IR. TEST==";
const char ned_prt_load_off[] = 			"BELASTING UIT";
const char ned_prt_load_on[] =				"BELASTING AAN";
const char ned_prt_rated[] = 				"NOMINAAL:";
const char ned_prt_vin_num[] = 				"VIN-NUMMER:";
const char ned_prt_soc[] =					"LAADSTATUS";
const char ned_prt_soh[] = 					"GEZONDHEIDSSTATUS";
const char ned_prt_starter_test[] = 		"STARTERTEST";
const char ned_prt_start_stop[] = 			"==TEST STARTEN-STOPPEN==";
const char ned_prt_temp[] = 				"TEMPERATUUR:";
const char ned_prt_test_date[] = 			"TESTDATUM:";
const char ned_prt_test_report[] = 			"TESTRAPPORT";
#endif
#if DANSK_en
// LCD
const char dan_language[] = 			"DANSK";
const char dan_battery[]=				"BATTERI";
const char dan_start_stop_test[]=		"START-STOP-TEST";
const char dan_battery_test[]=			"BATTERITEST";
const char dan_system_test[]=			"SYSTEMTEST";
const char dan_ir_test[]=				"IM- TEST";
const char dan_v_a_meter[]=				"V/A- MÅLER";
const char dan_setting[]=				"INDSTILLING";
const char dan_test_in_vehicle[]=		"TEST I KØRETØJ";
const char dan_direct_connect[]=		"DIREKTE FORBINDELSE";
const char dan_jump_start_post[]=		"BRUG AF STARTKABEL";
const char dan_brand[]=					"MÆRKE:";
const char dan_model[]=					"MODEL:";
const char dan_version[]=				"VERSION";
const char dan_test_result[]=			"TESTRESULTAT";
const char dan_voltage[]=				"SPÆNDING:";
const char dan_good_n_pass[]=			"GODT OG BESTÅET";
const char dan_good_n_recharge[]=		"GODT OG GENOPLAD";
const char dan_bad_n_replace[]=			"DÅRLIGT OG UDSKIFT";
const char dan_caution[]=				"FORSIGTIG";
const char dan_recharge_n_retest[]=		"GENOPLAD OG TEST IGEN";
const char dan_bad_cell_replace[]=		"DÅRLIG CELLE - SKAL UDSKIFTES";
const char dan_battery_type[]=			"BATTERITYPE";
const char dan_flooded[]=				"DRUKNET";
const char dan_agmf[]=					"FLAD AGM-PLADE";
const char dan_agms[]=					"AGM-SPIRAL";
const char dan_select_rating[]=			"VÆLG MÆRKEANGIVELSE";
const char dan_set_capacity[]=			"INDSTIL KAPACITET";
const char dan_back_light[]=			"LCD- BAGGRUNDSBELYSNING";
const char dan_language_select[]=		"VÆLG SPROG";
const char dan_clock[]=					"UR";
const char dan_clear_memory[]=			"RYD HUKOMMELSE";
const char dan_information[]=			"INFORMATION";
const char dan_time_zone[]=				"TIDSZONE";
const char dan_test_counter[]=			"TESTTÆLLER";
const char dan_abort[]=					"AFBRYD";
const char dan_directly_connect_to_battery[]=	"FORBIND DIREKTE TIL BATTERI";
const char dan_insert_vin[]=			"VIL DU INDSÆTTE STELNUMMER MANUELT?";
const char dan_test_not_suitable[]=		"TEST IKKE EGNET TIL GARANTI, VIL DU FORTSÆTTE?";
const char dan_yes[]=					"JA";
const char dan_no[]=					"NEJ";
const char dan_print_result[]=			"UDSKRIV RESULTAT";
const char dan_print_successful[]=		"UDSKRIVNING GENNEMFØRT, VIL DU UDSKRIVE IGEN?";
const char dan_check_clamp[]=			"KONTROLLÉR KLEMME";
const char dan_insert_change_aa[]=		"INDSÆT ELLER UDSKIFT AA-BATTERIER";
const char dan_load_error[]=			"BELASTNINGSFEJL";
const char dan_voltage_high[]=			"SPÆNDING ER HØJ";
const char dan_voltage_unstable[]=		"SPÆNDING ER IKKE STABIL\nVENT PÅ STABILITET";
const char dan_is_it_6v[]=				"ER ET 6 V BATTERI?";
const char dan_is_battery_charge[]=		"ER BATTERI OPLADET?";
const char dan_battery_in_vehicle[]=	"BATTERI I KØRETØJ";
const char dan_ss_test[]=				"SS.- TEST";
const char dan_bat_test[]=				"BAT.- TEST";
const char dan_sys_test[]=				"SYS.- TEST";
const char dan_ir_t[] =					"IM- TEST";
const char dan_print[]=					"UDSKRIV";
const char dan_print_q[]=				"VIL DU UDSKRIVE?";
const char dan_erase[]=					"VIL DU SLETTE?";
const char dan_memory_erase[]=			"VIL DU SLETTE HUKOMMELSE?";
const char dan_on[] =					"TIL";
const char dan_off[] =					"FRA";
const char dan_date[]=					"DATO:";
const char dan_time[]=					"TID:";
const char dan_invalid_vin[]=			"UGYLDIGT STELNUMMER, RESULTAT IKKE EGNET TIL GARANTI";
const char dan_input_vin[]=				"ANGIV STELNUMMER";
const char dan_enter_vin[]=				"ANGIV FULDT STELNUMMER";
const char dan_measured[]=				"MÅLT:";
const char dan_cranking_voltage[]=		"STARTSPÆNDING";
const char dan_lowest_voltage[]=		"LAVESTE SPÆNDING";
const char dan_high[]=					"HØJ";
const char dan_normal[]=				"NORMAL";
const char dan_low[]=					"LAV";
const char dan_no_detect[]=				"IKKE DETEKTERET";
const char dan_idle_voltage[]=			"VEKS. SPÆNDING I TOM GANG";
const char dan_sec[]=					"SEK.";
const char dan_ripple_detected[]=		"SVINGNING DETEKTERET";
const char dan_ripple_voltage[]=		"SVINGNINGSSPÆNDING";
const char dan_load_voltage[]=			"VEKS. BELASTNINGSSPÆNDING";
const char dan_point_to_battery_press_enter[]=	"PEG PÅ BATTERI\nTRYK PÅ ENTER";
const char dan_ota[]=					"OTA-OPGRADERING";
const char dan_test_code[]=				"TESTKODE";
const char dan_connection_status[]=		"FORBINDELSESSTATUS";
const char dan_scan_vin_code[]=			"VIL DU SCANNE STELNUMMERKODE?";
const char dan_vin_code[]=				"STELNUMMERKODE";
const char dan_invalid_vin_rescan[]=	"UGYLDIG STELNUMMERKODE\nSCAN IGEN";
const char dan_wifi_power[]=			"WI-FI-STRØM:";
const char dan_wifi_list[]=				"WI-FI-LISTE";
const char dan_downloading[]=			"DOWNLOADER...";
const char dan_upgrade_cancel[]=		"VIL DU ANNULLERE OPGRADERING?";
const char dan_upgrading[]=				"OPGRADERER...";
const char dan_check_version[]=			"KONTROLLÉR VERSION";
const char dan_refresh_list[] = 		"*OPDATER LISTE";
const char dan_limited_connection[]=	"BEGRÆNSET FORBINDELSE";
const char dan_wifi_connected[]=		"WI-FI FORBUNDET";
const char dan_connecting[]=			"OPRETTER FORBINDELSE...";
const char dan_wifi_disconnect[]=		"WI-FI AFBRUDT";
const char dan_check_connection[]=		"KONTROLLÉR FORBINDELSE...";
const char dan_keep_clamps_connect[]=	"ADVARSEL: LAD KLEMMERNE VÆRE FORBUNDET TIL BATTERIET.";
//const char dan_skip[]=					"SPRING OVER";
const char dan_registration[] = "REGISTRERING";
const char dan_scan_qr_code[] = "SCAN QR-KODE?";
const char dan_registering[] = "REGISTRERER ...";


// printer
const char dan_prt_battery_test[] = 		"==BATTERITEST==";
const char dan_prt_by[] = 					"AF:";
const char dan_prt_charging_test[] = 		"OPLADNINGSTEST";
const char dan_prt_note[] = 				"BEMÆRK!:";
const char dan_prt_diode_ripple[] =			"DIODESVINGNING";
const char dan_prt_ir_test[] = 				"==IM- TEST==";
const char dan_prt_load_off[] = 			"BELASTNING FRA";
const char dan_prt_load_on[] =				"BELASTNING TIL";
const char dan_prt_rated[] = 				"MÆRKET:";
const char dan_prt_vin_num[] = 				"STELNUMMER:";
const char dan_prt_soc[] =					"STATUS FOR OPLADNING";
const char dan_prt_soh[] = 					"STATUS FOR TILSTAND";
const char dan_prt_starter_test[] = 		"STARTERTEST";
const char dan_prt_start_stop[] = 			"==START-STOP-TEST==";
const char dan_prt_temp[] = 				"TEMPERATUR:";
const char dan_prt_test_date[] = 			"TESTDATO:";
const char dan_prt_test_report[] = 			"TESTRAPPORT";
#endif
#if NORSK_en
// LCD
const char nor_language[] = 			"NORSK";
const char nor_battery[]=				"BATTERI";
const char nor_start_stop_test[]=		"START-STOPP-TEST";
const char nor_battery_test[]=			"BATTERITEST";
const char nor_system_test[]=			"SYSTEMTEST";
const char nor_ir_test[]=				"IR. TEST";
const char nor_v_a_meter[]=				"V./A. MÅLER";
const char nor_setting[]=				"INNSTILLING";
const char nor_test_in_vehicle[]=		"TEST I KJØRETØY";
const char nor_direct_connect[]=		"DIREKTE TILKOBLING";
const char nor_jump_start_post[]=		"JUMP-START ETTER";
const char nor_brand[]=					"MERKE:";
const char nor_model[]=					"MODELL:";
const char nor_version[]=				"VERSJON";
const char nor_test_result[]=			"TESTRESULTAT";
const char nor_voltage[]=				"SPENNING:";
const char nor_good_n_pass[]=			"BRA OG BESTÅTT";
const char nor_good_n_recharge[]=		"BRA OG LAD OPP";
const char nor_bad_n_replace[]=			"DÅRLIG OG BYTT UT";
const char nor_caution[]=				"FORSIKTIG";
const char nor_recharge_n_retest[]=		"LAD OPP OG TEST PÅ NYTT";
const char nor_bad_cell_replace[]=		"SKIFT UT DÅRLIG BATTERI";
const char nor_battery_type[]=			"BATTERITYPE";
const char nor_flooded[]=				"OVERSVØMMET";
const char nor_agmf[]=					"AGM FLAT PLATE";
const char nor_agms[]=					"AGM-SPIRAL";
const char nor_select_rating[]=			"VELG RANGERING";
const char nor_set_capacity[]=			"ANGI KAPASITET";
const char nor_back_light[]=			"LCD-BAKGRUNNSLYS";
const char nor_language_select[]=		"SPRÅKVALG";
const char nor_clock[]=					"KLOKKE";
const char nor_clear_memory[]=			"SLETT MINNE";
const char nor_information[]=			"INFORMASJON";
const char nor_time_zone[]=				"TIDSSONE";
const char nor_test_counter[]=			"TESTTELLER";
const char nor_abort[]=					"AVBRYT";
const char nor_directly_connect_to_battery[]=	"DIREKTE TILKOBLING TIL BATTERI";
const char nor_insert_vin[]=			"SETTE INN VIN-KODE MANUELT?";
const char nor_test_not_suitable[]=		"TEST ER IKKE EGNET FOR GARANTI. FORTSETTE?";
const char nor_yes[]=					"JA";
const char nor_no[]=					"NEI";
const char nor_print_result[]=			"SKRIV UT RESULTAT";
const char nor_print_successful[]=		"UTSKRIFTEN VAR VELLYKKET. SKRIVE UT PÅ NYTT?";
const char nor_check_clamp[]=			"KONTROLLER KLEMME";
const char nor_insert_change_aa[]=		"SETT INN ELLER BYTT AA-BATTERIER";
const char nor_load_error[]=			"INNLASTINGSFEIL";
const char nor_voltage_high[]=			"HØY SPENNING";
const char nor_voltage_unstable[]=		"USTABIL SPENNING\nVENTER PÅ STABILITET";
const char nor_is_it_6v[]=				"ER DET ET 6V-BATTERI?";
const char nor_is_battery_charge[]=		"ER BATTERIET LADET?";
const char nor_battery_in_vehicle[]=	"BATTERI I KJØRETØY";
const char nor_ss_test[]=				"SS. TEST";
const char nor_bat_test[]=				"BAT. TEST";
const char nor_sys_test[]=				"SYS. TEST";
const char nor_ir_t[] =					"IR. TEST";
const char nor_print[]=					"SKRIV UT";
const char nor_print_q[]=				"SKRIVE UT?";
const char nor_erase[]=					"SLETTE?";
const char nor_memory_erase[]=			"SLETTE MINNE?";
const char nor_on[] =					"PÅ";
const char nor_off[] =					"AV";
const char nor_date[]=					"DATO:";
const char nor_time[]=					"TIDSPUNKT:";
const char nor_invalid_vin[]=			"UGYLDIG VIN. RESULT ER IKKE EGNET FOR GARANTI";
const char nor_input_vin[]=				"ANGI VIN";
const char nor_enter_vin[]=				"INNSKRIVING AV VIN ER FULLFØRT";
const char nor_measured[]=				"MÅLT:";
const char nor_cranking_voltage[]=		"STARTSPENNING";
const char nor_lowest_voltage[]=		"LAVEST SPENNING";
const char nor_high[]=					"HØY";
const char nor_normal[]=				"NORMAL";
const char nor_low[]=					"LAV";
const char nor_no_detect[]=				"IKKE OPPDAGET";
const char nor_idle_voltage[]=			"ALT. TOMGANGSSPENNING";
const char nor_sec[]=					"SEK.";
const char nor_ripple_detected[]=		"RIPPEL OPPDAGET";
const char nor_ripple_voltage[]=		"RIPPELSPENNING.";
const char nor_load_voltage[]=			"ALT. LASTSPENNING";
const char nor_point_to_battery_press_enter[]=	"PEK PÅ BATTERI\nTRYKK PÅ ENTER";
const char nor_ota[]=					"OTA-OPPGRADERING";
const char nor_test_code[]=				"TESTKODE";
const char nor_connection_status[]=		"TILKOBLINGSSTATUS";
const char nor_scan_vin_code[]=			"SKANNE VIN-KODE?";
const char nor_vin_code[]=				"VIN-KODE";
const char nor_invalid_vin_rescan[]=	"UGYLDIG VIN-KODE\nSCANN PÅ NYTT";
const char nor_wifi_power[]=			"WI-FI-STRØM:";
const char nor_wifi_list[]=				"WI-FI-LISTE";
const char nor_downloading[]=			"LASTER NED ...";
const char nor_upgrade_cancel[]=		"AVBRYT OPPGRADERING?";
const char nor_upgrading[]=				"OPPGRADERER ...";
const char nor_check_version[]=			"KONTROLLER VERSJON";
const char nor_refresh_list[] = 		"*OPPDATER LISTE";
const char nor_limited_connection[]=	"BEGRENSET TILKOBLING";
const char nor_wifi_connected[]=		"WI-FI ER TILKOBLET";
const char nor_connecting[]=			"KOBLER TIL ...";
const char nor_wifi_disconnect[]=		"WI-FI ER FRAKOBLET";
const char nor_check_connection[]=		"SJEKK FORBINDELSEN ...";
const char nor_keep_clamps_connect[]=	"ADVARSEL: BEHOLD KLEMMER TILKOBLET BATTERIET.";
//const char nor_skip[]=					"HOPP OVER";
const char nor_registration[] = "REGISTRERING";
const char nor_scan_qr_code[] = "SKANN QR-KODE?";
const char nor_registering[] = "REGISTRERER ...";


// printer
const char nor_prt_battery_test[] = 		"==BATTERITEST==";
const char nor_prt_by[] = 					"AV:";
const char nor_prt_charging_test[] = 		"LADETEST";
const char nor_prt_note[] = 				"MERK:";
const char nor_prt_diode_ripple[] =			"DIODERIPPEL";
const char nor_prt_ir_test[] = 				"==IR. TEST==";
const char nor_prt_load_off[] = 			"LAST AV";
const char nor_prt_load_on[] =				"LAST PÅ";
const char nor_prt_rated[] = 				"GRADERT:";
const char nor_prt_vin_num[] = 				"VIN-NUMMER:";
const char nor_prt_soc[] =					"LADESTATUS";
const char nor_prt_soh[] = 					"HELSESTATUS";
const char nor_prt_starter_test[] = 		"STARTTEST";
const char nor_prt_start_stop[] = 			"==START-STOPP-TEST==";
const char nor_prt_temp[] = 				"TEMPERATUR:";
const char nor_prt_test_date[] = 			"TESTDATO:";
const char nor_prt_test_report[] = 			"TESTRAPPORT";
#endif
#if SOOMI_en
// LCD
const char soo_language[] =				"SOOMI";
const char soo_battery[]=				"AKKU";
const char soo_start_stop_test[]=		"START-STOP-TESTI";
const char soo_battery_test[]=			"AKUN TESTAUS";
const char soo_system_test[]=			"JÄRJESTELMÄTESTI";
const char soo_ir_test[]=				"IR- TESTAUS";
const char soo_v_a_meter[]=				"V/A- MITTARI";
const char soo_setting[]=				"ASETUS";
const char soo_test_in_vehicle[]=		"TESTAUS AJONEUVOSSA";
const char soo_direct_connect[]=		"SUORA LIITÄNTÄ";
const char soo_jump_start_post[]=		"APUKÄYNNISTYSNAPA";
const char soo_brand[]=					"MERKKI:";
const char soo_model[]=					"MALLI:";
const char soo_version[]=				"VERSIO";
const char soo_test_result[]=			"TESTITULOS";
const char soo_voltage[]=				"JÄNNITE:";
const char soo_good_n_pass[]=			"HYVÄ, KUNNOSSA";
const char soo_good_n_recharge[]=		"HYVÄ, LATAA";
const char soo_bad_n_replace[]=			"HUONO, VAIHDA";
const char soo_caution[]=				"HUOMIO";
const char soo_recharge_n_retest[]=		"LATAA JA TESTAA UUDELLEEN";
const char soo_bad_cell_replace[]=		"HUONO KENNO, VAIHDA";
const char soo_battery_type[]=			"AKUN TYYPPI";
const char soo_flooded[]=				"NESTE";
const char soo_agmf[]=					"AGM FLAT PLATE";
const char soo_agms[]=					"AGM SPIRAL";
const char soo_select_rating[]=			"VALITSE LUOKITUS";
const char soo_set_capacity[]=			"ASETA KAPASITEETTI";
const char soo_back_light[]=			"LCD-TAUSTAVALO";
const char soo_language_select[]=		"KIELEN VALINTA";
const char soo_clock[]=					"KELLO";
const char soo_clear_memory[]=			"TYHJENNÄ MUISTI";
const char soo_information[]=			"TIEDOT";
const char soo_time_zone[]=				"AIKAVYÖHYKE";
const char soo_test_counter[]=			"TESTILASKURI";
const char soo_abort[]=					"KESKEYTÄ";
const char soo_directly_connect_to_battery[]=	"SUORA KYTKENTÄ AKKUUN";
const char soo_insert_vin[]=			"ANNA VIN-KOODI MANUAALISESTI?";
const char soo_test_not_suitable[]=		"TESTI EI SOVI TAKUUSEEN, JATKETAANKO?";
const char soo_yes[]=					"KYLLÄ";
const char soo_no[]=					"EI";
const char soo_print_result[]=			"TULOSTA TULOS";
const char soo_print_successful[]=		"TULOSTUS ONNISTUI, TULOSTA UUDELLEEN?";
const char soo_check_clamp[]=			"TARKISTA LIITIN";
const char soo_insert_change_aa[]=		"LISÄÄ TAI VAIHDA AA-PARISTOT";
const char soo_load_error[]=			"LATAUSVIRHE";
const char soo_voltage_high[]=			"SUURI JÄNNITE";
const char soo_voltage_unstable[]=		"JÄNNITE EPÄVAKAA\nODOTA VAKAATA";
const char soo_is_it_6v[]=				"ONKO TÄMÄ 6 V AKKU?";
const char soo_is_battery_charge[]=		"ONKO AKKU LADATTU?";
const char soo_battery_in_vehicle[]=	"AJONEUVOSSA AKKU";
const char soo_ss_test[]=				"SS- TESTAUS";
const char soo_bat_test[]=				"AKUN TESTAUS";
const char soo_sys_test[]=				"JÄRJ.- TESTAUS";
const char soo_ir_t[] =					"IR- TESTAUS";
const char soo_print[]=					"TULOSTA";
const char soo_print_q[]=				"TULOSTA?";
const char soo_erase[]=					"TYHJENNÄ?";
const char soo_memory_erase[]=			"MUISTIN TYHJENNYS?";
const char soo_on[] =					"KÄYTÖSSÄ";
const char soo_off[] =					"EI KÄYTÖSSÄ";
const char soo_date[]=					"PVM:";
const char soo_time[]=					"AIKA:";
const char soo_invalid_vin[]=			"VÄÄRÄ VIN, TULOS EI SOVI TAKUUSEEN";
const char soo_input_vin[]=				"ANNA VIN";
const char soo_enter_vin[]=				"ANNA KOKO VIN";
const char soo_measured[]=				"MITATTU:";
const char soo_cranking_voltage[]=		"KÄYNNISTYSJÄNNITE";
const char soo_lowest_voltage[]=		"PIENIN JÄNNITE";
const char soo_high[]=					"SUURI";
const char soo_normal[]=				"NORMAALI";
const char soo_low[]=					"PIENI";
const char soo_no_detect[]=				"EI HAVAITTU";
const char soo_idle_voltage[]=			"LATURIN JOUTOKÄYNTIJÄNNITE";
const char soo_sec[]=					"S";
const char soo_ripple_detected[]=		"AALTOISUUTTA HAVAITTU";
const char soo_ripple_voltage[]=		"AALTOISUUSJÄNNITE";
const char soo_load_voltage[]=			"LATURIN LÄHTÖJÄNNITE";
const char soo_point_to_battery_press_enter[]=	"OSOITA AKKUA\nPAINA ENTER";
const char soo_ota[]=					"OTA-PÄIVITYS";
const char soo_test_code[]=				"TESTIKOODI";
const char soo_connection_status[]=		"LIITÄNTÄTILA";
const char soo_scan_vin_code[]=			"LUE VIN-KOODI?";
const char soo_vin_code[]=				"VIN-KOODI";
const char soo_invalid_vin_rescan[]=	"VIRHEELLINEN VIN-KOODI\nLUE UUDELLEEN";
const char soo_wifi_power[]=			"WIFI-VIRTA:";
const char soo_wifi_list[]=				"WIFI-LUETTELO";
const char soo_downloading[]=			"LADATAAN...";
const char soo_upgrade_cancel[]=		"PERUUTA PÄIVITYS?";
const char soo_upgrading[]=				"PÄIVITETÄÄN...";
const char soo_check_version[]=			"TARKISTA VERSIO";
const char soo_refresh_list[] = 		"*PÄIVITÄ LUETTELO";
const char soo_limited_connection[]=	"RAJOITETTU YHTEYS";
const char soo_wifi_connected[]=		"WIFI YHDISTETTY";
const char soo_connecting[]=			"YHDISTETÄÄN...";
const char soo_wifi_disconnect[]=		"WIFI KATKAISTU";
const char soo_check_connection[]=		"TARKISTA YHTEYS...";
const char soo_keep_clamps_connect[]=	"VAROITUS: PIDÄ LIITTIMET KYTKETTYINÄ AKKUUN.";
//const char soo_skip[]=				"OHITA";
const char soo_registration[] = "REKISTERÖINTI";
const char soo_scan_qr_code[] = "LUE QR-KOODI?";
const char soo_registering[] = "REKISTERÖIDÄÄN...";


// printer
const char soo_prt_battery_test[] = 		"==AKUN TESTAUS==";
const char soo_prt_by[] = 					"TEHNYT:";
const char soo_prt_charging_test[] = 		"LATAUSTESTI";
const char soo_prt_note[] = 				"HUOMAUTUS:";
const char soo_prt_diode_ripple[] =			"DIODIN AALTOISUUS";
const char soo_prt_ir_test[] = 				"==IR- TESTAUS==";
const char soo_prt_load_off[] = 			"KUORMA POIS";
const char soo_prt_load_on[] =				"KUORMA PÄÄLLÄ";
const char soo_prt_rated[] = 				"LUOKITUS:";
const char soo_prt_vin_num[] = 				"VIN-NUMERO:";
const char soo_prt_soc[] =					"VARAUSTILA";
const char soo_prt_soh[] = 					"KUNTO";
const char soo_prt_starter_test[] = 		"KÄYNNISTYSMOOTTORIN TESTAUS";
const char soo_prt_start_stop[] = 			"==START-STOP-TESTAUS==";
const char soo_prt_temp[] = 				"LÄMPÖTILA:";
const char soo_prt_test_date[] = 			"TESTIPVM:";
const char soo_prt_test_report[] = 			"TESTIRAPORTTI";
#endif
#if SVENSKA_en
// LCD
const char sve_language[] = "SVENSKA";
const char sve_battery[]=				"BATTERI";
const char sve_start_stop_test[]=		"START-STOPP TEST";
const char sve_battery_test[]=			"BATTERITEST";
const char sve_system_test[]=			"SYSTEMTEST";
const char sve_ir_test[]=				"IR. TEST";
const char sve_v_a_meter[]=				"V./A. METER";
const char sve_setting[]=				"INSTÄLLNING";
const char sve_test_in_vehicle[]=		"TESTA I FORDON";
const char sve_direct_connect[]=		"DIREKTANSLUTNING";
const char sve_jump_start_post[]=		"STARTHJÄLPSPOST";
const char sve_brand[]=					"MÄRKE:";
const char sve_model[]=					"MODELL:";
const char sve_version[]=				"VERSION";
const char sve_test_result[]=			"TESTRESULTAT";
const char sve_voltage[]=				"SPÄNNING:";
const char sve_good_n_pass[]=			"BRA OCH GODKÄNT";
const char sve_good_n_recharge[]=		"BRA OCH LADDA";
const char sve_bad_n_replace[]=			"DÅLIGT OCH BYT UT";
const char sve_caution[]=				"OBS!";
const char sve_recharge_n_retest[]=		"LADDA OCH TESTA IGEN";
const char sve_bad_cell_replace[]=		"BYT UT DÅLIG CELL";
const char sve_battery_type[]=			"BATTERITYP";
const char sve_flooded[]=				"VÄTSKEFYLLT";
const char sve_agmf[]=					"AGM FLAT PLATE";
const char sve_agms[]=					"AGM-SPIRAL";
const char sve_select_rating[]=			"VÄLJ KLASS";
const char sve_set_capacity[]=			"STÄLL IN KAPACITET";
const char sve_back_light[]=			"LCD- BAKGRUNDSBELYSNING";
const char sve_language_select[]=		"VÄLJ SPRÅK";
const char sve_clock[]=					"KLOCKA";
const char sve_clear_memory[]=			"RENSA MINNE";
const char sve_information[]=			"INFORMATION";
const char sve_time_zone[]=				"TIDSZON";
const char sve_test_counter[]=			"TESTRÄKNARE";
const char sve_abort[]=					"AVBRYT";
const char sve_directly_connect_to_battery[]=	"DIREKTANSLUTNING TILL BATTERI";
const char sve_insert_vin[]=			"INFOGA VIN-KOD MANUELLT?";
const char sve_test_not_suitable[]=		"TEST INTE LÄMPLIGT FÖR GARANTI, FORTSÄTT?";
const char sve_yes[]=					"JA";
const char sve_no[]=					"NEJ";
const char sve_print_result[]=			"SKRIV UT RESULTAT";
const char sve_print_successful[]=		"UTSKRIFTEN ÄR KLAR, SKRIV UT IGEN?";
const char sve_check_clamp[]=			"KONTROLLERA KLÄMMA";
const char sve_insert_change_aa[]=		"SÄTT I ELLER BYT AA-BATTERIER";
const char sve_load_error[]=			"LADDNINGSFEL";
const char sve_voltage_high[]=			"HÖG SPÄNNING";
const char sve_voltage_unstable[]=		"INSTABIL SPÄNNING\nVÄNTA PÅ STABILITET";
const char sve_is_it_6v[]=				"ÄR DET ETT 6V-BATTERI?";
const char sve_is_battery_charge[]=		"ÄR BATTERIET LADDAT?";
const char sve_battery_in_vehicle[]=	"BATTERI I FORDON";
const char sve_ss_test[]=				"SS. TEST";
const char sve_bat_test[]=				"BAT. TEST";
const char sve_sys_test[]=				"SYS. TEST";
const char sve_ir_t[] =					"IR. TEST";
const char sve_print[]=					"SKRIV UT";
const char sve_print_q[]=				"SKRIV UT?";
const char sve_erase[]=					"RADERA?";
const char sve_memory_erase[]=			"RENSA MINNE?";
const char sve_on[] =					"PÅ";
const char sve_off[] =					"AV";
const char sve_date[]=					"DATUM:";
const char sve_time[]=					"TID:";
const char sve_invalid_vin[]=			"OGILTIG VIN-KOD, RESULTAT INTE LÄMPLIGT FÖR GARANTI";
const char sve_input_vin[]=				"INFOGA VIN-KOD";
const char sve_enter_vin[]=				"ANGE FULLSTÄNDIG VIN-KOD";
const char sve_measured[]=				"UPPMÄTT:";
const char sve_cranking_voltage[]=		"VEVSPÄNNING";
const char sve_lowest_voltage[]=		"LÄGSTA SPÄNNING";
const char sve_high[]=					"HÖG";
const char sve_normal[]=				"NORMAL";
const char sve_low[]=					"LÅG";
const char sve_no_detect[]=				"INGEN UPPTÄCKT";
const char sve_idle_voltage[]=			"ALT. TOMGÅNGSSPÄNNING";
const char sve_sec[]=					"SEK";
const char sve_ripple_detected[]=		"RIPPEL UPPTÄCKT";
const char sve_ripple_voltage[]=		"RIPPELSPÄNNING";
const char sve_load_voltage[]=			"ALT. LADDNINGSSPÄNNING";
const char sve_point_to_battery_press_enter[]=	"PUNKT TILL BATTERI\nTRYCK PÅ ENTER";
const char sve_ota[]=					"OTA-UPPGRADERING";
const char sve_test_code[]=				"TESTKOD";
const char sve_connection_status[]=		"ANSLUTNINGSSTATUS";
const char sve_scan_vin_code[]=			"SKANNA VIN-KOD?";
const char sve_vin_code[]=				"VIN-KOD";
const char sve_invalid_vin_rescan[]=	"OGILTIG VIN-KOD\nSKANNA IGEN";
const char sve_wifi_power[]=			"WIFI-EFFEKT:";
const char sve_wifi_list[]=				"WIFI-LISTA";
const char sve_downloading[]=			"HÄMTAR ...";
const char sve_upgrade_cancel[]=		"AVBRYT UPPGRADERING?";
const char sve_upgrading[]=				"UPPGRADERAR ...";
const char sve_check_version[]=			"KONTROLLERA VERSION";
const char sve_refresh_list[] = 		"*UPPDATERA LISTA";
const char sve_limited_connection[]=	"BEGRÄNSAD ANSLUTNING";
const char sve_wifi_connected[]=		"WIFI-ANSLUTNING";
const char sve_connecting[]=			"ANSLUTER ...";
const char sve_wifi_disconnect[]=		"WIFI FRÅNKOPPLAD";
const char sve_check_connection[]=		"KONTROLLERA ANSLUTNING ...";
const char sve_keep_clamps_connect[]=	"VARNING: HÅLL KLÄMMORNA ANSLUTNA TILL BATTERIET.";
//const char sve_skip[]=				"HOPPA ÖVER";
const char sve_registration[] = "REGISTRERING";
const char sve_scan_qr_code[] = "SKANNA QR-KOD?";
const char sve_registering[] = "REGISTRERAR...";


// printer
const char sve_prt_battery_test[] = 		"==BATTERITEST==";
const char sve_prt_by[] = 					"AV:";
const char sve_prt_charging_test[] = 		"LADDNINGSTEST";
const char sve_prt_note[] = 				"OBS!";
const char sve_prt_diode_ripple[] =			"DIODRIPPEL";
const char sve_prt_ir_test[] = 				"==IR. TEST==";
const char sve_prt_load_off[] = 			"LADDNING AV";
const char sve_prt_load_on[] =				"LADDNING PÅ";
const char sve_prt_rated[] = 				"KLASS:";
const char sve_prt_vin_num[] = 				"VIN-NUMMER:";
const char sve_prt_soc[] =					"LADDNINGSTILLSTÅND";
const char sve_prt_soh[] = 					"HÄLSOTILLSTÅND";
const char sve_prt_starter_test[] = 		"STARTTEST";
const char sve_prt_start_stop[] = 			"==STARTA-STOPPA TEST==";
const char sve_prt_temp[] = 				"TEMPERATUR:";
const char sve_prt_test_date[] = 			"TESTDATUM:";
const char sve_prt_test_report[] = 			"TESTRAPPORT";
#endif
#if CESTINA_en
// LCD
const char ces_language[] = 			"CESTINA";
const char ces_battery[]=				"BATERIE";
const char ces_start_stop_test[]=		"START-STOP TEST";
const char ces_battery_test[]=			"TEST BATERIE";
const char ces_system_test[]=			"TEST SYSTÉMU";
const char ces_ir_test[]=				"IR. TEST";
const char ces_v_a_meter[]=				"V./A. MĚŘIČ";
const char ces_setting[]=				"NASTAVENÍ";
const char ces_test_in_vehicle[]=		"TEST VE VOZIDLE";
const char ces_direct_connect[]=		"PŘÍMÉ PŘIPOJENÍ";
const char ces_jump_start_post[]=		"pomocné startovací zařízení";
const char ces_brand[]=					"ZNAČKA:";
const char ces_model[]=					"MODEL:";
const char ces_version[]=				"VERZE";
const char ces_test_result[]=			"VÝSLEDEK TESTU";
const char ces_voltage[]=				"NAPĚTÍ:";
const char ces_good_n_pass[]=			"dobrá & v pořádku";
const char ces_good_n_recharge[]=		"dobrá & dobít";
const char ces_bad_n_replace[]=			"špatná & vyměnit";
const char ces_caution[]=				"UPOZORNĚNÍ";
const char ces_recharge_n_retest[]=		"dobít & znovu otestovat";
const char ces_bad_cell_replace[]=		"špatný článek baterie, vyměnit";
const char ces_battery_type[]=			"TYP BATERIE";
const char ces_flooded[]=				"ZAPLAVENÉ DESKY";
const char ces_agmf[]=					"AGM PLOCHÁ DESKA";
const char ces_agms[]=					"AGM SVIN. ELEKT.";
const char ces_select_rating[]=			"ZVOLIT NORMU DOBÍJENÍ";
const char ces_set_capacity[]=			"NASTAVIT KAPACITU";
const char ces_back_light[]=			"LCD PODSVÍCENÍ";
const char ces_language_select[]=		"VÝBĚR JAZYKA";
const char ces_clock[]=					"HODINY";
const char ces_clear_memory[]=			"VYMAZÁNÍ PAMĚTI";
const char ces_information[]=			"INFORMACE";
const char ces_time_zone[]=				"ČASOVÉ PÁSMO";
const char ces_test_counter[]=			"TESTOVACÍ ČÍTAČ";
const char ces_abort[]=					"PŘERUŠIT";
const char ces_directly_connect_to_battery[]=	"PŘÍMÉ PŘIPOJENÍ K BATERII";
const char ces_insert_vin[]=			"VLOŽIT VIN KÓD MANUÁLNĚ?";
const char ces_test_not_suitable[]=		"TEST NENÍ VHODNÝ PRO ZÁRUKU, POKRAČOVAT?";
const char ces_yes[]=					"ANO";
const char ces_no[]=					"NE";
const char ces_print_result[]=			"VYTISKNOUT VÝSLEDEK";
const char ces_print_successful[]=		"TISK ÚSPĚŠNÝ, TISKNOUT ZNOVU?";
const char ces_check_clamp[]=			"ZKONTROLOVAT SVORKU";
const char ces_insert_change_aa[]=		"VLOŽIT NEBO VYMĚNIT AA BATERIE";
const char ces_load_error[]=			"CHYBA ZÁTĚŽE";
const char ces_voltage_high[]=			"VYSOKÉ NAPĚTÍ";
const char ces_voltage_unstable[]=		"NESTABILNÍ NAPĚTÍ\nČEKAT NA STABILNÍ";
const char ces_is_it_6v[]=				"JE TO 6V BATERIE?";
const char ces_is_battery_charge[]=		"JE BATERIE NABITA?";
const char ces_battery_in_vehicle[]=	"BATERIE VE VOZIDLE";
const char ces_ss_test[]=				"SS. TEST";
const char ces_bat_test[]=				"BAT. TEST";
const char ces_sys_test[]=				"SYS. TEST";
const char ces_ir_t[] =					"IR. TEST";
const char ces_print[]=					"TISK";
const char ces_print_q[]=				"VYTISKNOUT?";
const char ces_erase[]=					"VYMAZAT?";
const char ces_memory_erase[]=			"VYMAZAT PAMĚŤ?";
const char ces_on[] =					"ZAP.";
const char ces_off[] =					"VYP.";
const char ces_date[]=					"DATUM:";
const char ces_time[]=					"ČAS:";
const char ces_invalid_vin[]=			"NEPLATNÝ VIN, VÝSLEDEK NENÍ VHODNÝ PRO ZÁRUKU";
const char ces_input_vin[]=				"VSTUPNÍ VIN(ZADAT VIN)";
const char ces_enter_vin[]=				"ZADAT KOMPLETNÍ VIN";
const char ces_measured[]=				"NAMĚŘENO:";
const char ces_cranking_voltage[]=		"SPOUŠTĚCÍ NAPĚTÍ";
const char ces_lowest_voltage[]=		"NEJNIŽŠÍ NAPĚTÍ";
const char ces_high[]=					"VYSOKÉ";
const char ces_normal[]=				"NORMÁLNÍ";
const char ces_low[]=					"NÍZKÁ";
const char ces_no_detect[]=				"NEDETEKOVÁNO";
const char ces_idle_voltage[]=			"ALT. NAPĚTÍ NAPRÁZDNO";
const char ces_sec[]=					"SEK";
const char ces_ripple_detected[]=		"ZVLNĚNÍ DETEKOVÁNO";
const char ces_ripple_voltage[]=		"NAPĚTÍ ZVLNĚNÍ";
const char ces_load_voltage[]=			"ALT. NAPĚTÍ PŘI ZATÍŽENÍ";
const char ces_point_to_battery_press_enter[]=	"SMĚROVAT K BATERII\nSTISKNOUT ENTER";
const char ces_ota[]=					"OTA UPGRADE";
const char ces_test_code[]=				"TESTOVACÍ KÓD";
const char ces_connection_status[]=		"STAV PŘIPOJENÍ";
const char ces_scan_vin_code[]=			"NASKENOVAT VIN KÓD?";
const char ces_vin_code[]=				"VIN KÓD";
const char ces_invalid_vin_rescan[]=	"NEPLATNÝ VIN KÓD\nZNOVU NASKENOVAT";
const char ces_wifi_power[]=			"NAPÁJENÍ WI-FI:";
const char ces_wifi_list[]=				"SEZNAM WIFI";
const char ces_downloading[]=			"STAHUJI...";
const char ces_upgrade_cancel[]=		"UPGRADE ZRUŠIT?";
const char ces_upgrading[]=				"UPGRADOVÁNÍ...";
const char ces_check_version[]=			"ZKONTROLOVAT VERZI";
const char ces_refresh_list[] = 		"*OBNOVIT SEZNAM";
const char ces_limited_connection[]=	"OMEZENÉ PŘIPOJENÍ";
const char ces_wifi_connected[]=		"WI-FI PŘIPOJENA";
const char ces_connecting[]=			"PŘIPOJUJI...";
const char ces_wifi_disconnect[]=		"WI-FI ODPOJENA";
const char ces_check_connection[]=		"ZKONTROLOVAT PŘIPOJENÍ...";
const char ces_keep_clamps_connect[]=	"VAROVÁNÍ: MĚJTE SVORKY PŘIPOJENÉ K BATERII.";
//const char ces_skip[]=				"PŘESKOČIT";
const char ces_registration[] = "REGISTRACE";
const char ces_scan_qr_code[] = "SKENOVAT QR KÓD?";
const char ces_registering[] = "REGISTROVÁNÍ...";


// printer
const char ces_prt_battery_test[] = 		"==TEST BATERIE==";
const char ces_prt_by[] = 					"Kdo provede:";
const char ces_prt_charging_test[] = 		"TEST NABÍJENÍ";
const char ces_prt_note[] = 				"POZNÁMKA:";
const char ces_prt_diode_ripple[] =			"ZVLNĚNÍ DIODOVÉHO USMĚRŇOVAČE";
const char ces_prt_ir_test[] = 				"==IR. TEST==";
const char ces_prt_load_off[] = 			"ZÁTĚŽ VYPNUTA";
const char ces_prt_load_on[] =				"ZÁTĚŽ ZAPNUTA";
const char ces_prt_rated[] = 				"HODNOCENO:";
const char ces_prt_vin_num[] = 				"ČÍSLO VIN:";
const char ces_prt_soc[] =					"STAV NABITÍ";
const char ces_prt_soh[] = 					"ZDRAVOTNÍ STAV";
const char ces_prt_starter_test[] = 		"TEST STARTÉRU";
const char ces_prt_start_stop[] = 			"==START-STOP TEST==";
const char ces_prt_temp[] = 				"TEPLOTA:";
const char ces_prt_test_date[] = 			"DATUM TESTU:";
const char ces_prt_test_report[] = 			"ZPRÁVA O TESTU";
#endif
#if LIETUVIU_K_en
// LCD
const char lie_language[] = "LIETUVIŲ K.";
const char lie_battery[]=				"AKUMULIATORIAUS";
const char lie_start_stop_test[]=		"TEST. PAL.-ST.";
const char lie_battery_test[]=			"AKUM. TESTAVIMAS";
const char lie_system_test[]=			"SIST. TESTAVIMAS";
const char lie_ir_test[]=				"VV TESTAVIMAS";
const char lie_v_a_meter[]=				"ĮT./ST. MATUOKLIS";
const char lie_setting[]=				"NUSTATYMAS";
const char lie_test_in_vehicle[]=		"TESTAVIMAS TRANSPORTO PRIEMONĖJE";
const char lie_direct_connect[]=		"TIESIOGINĖ JUNGTIS";
const char lie_jump_start_post[]=		"IŠORINIO UŽVEDIMO STULPELIS";
const char lie_brand[]=					"MARKĖ:";
const char lie_model[]=					"MODELIS:";
const char lie_version[]=				"VERSIJA";
const char lie_test_result[]=			"TESTAVIMO REZULTATAS";
const char lie_voltage[]=				"ĮTAMPA:";
const char lie_good_n_pass[]=			"GERAS, TEIGIAMAS REZULTATAS";
const char lie_good_n_recharge[]=		"GERAS, REIKIA ĮKRAUTI";
const char lie_bad_n_replace[]=			"BLOGAS, PAKEISTI";
const char lie_caution[]=				"PERSPĖJIMAS";
const char lie_recharge_n_retest[]=		"ĮKRAUTI IR PAKARTOTINAI TESTUOTI";
const char lie_bad_cell_replace[]=		"BLOGAS, PAKEISTI KAMERĄ";
const char lie_battery_type[]=			"AKUMULIATORIAUS TIPAS";
const char lie_flooded[]=				"SKYSTO ELEKTROLITO";
const char lie_agmf[]=					"AGM STIKLO PLUOŠTO";
const char lie_agms[]=					"AGM SPIRALINIS";
const char lie_select_rating[]=			"PASIRINKITE KLASĘ";
const char lie_set_capacity[]=			"NUSTATYKITE GALIĄ";
const char lie_back_light[]=			"LCD APŠVIETIMAS";
const char lie_language_select[]=		"KALBOS PASIRINKIMAS";
const char lie_clock[]=					"LAIKRODIS";
const char lie_clear_memory[]=			"ATMINTIES VALYMAS";
const char lie_information[]=			"INFORMACIJA";
const char lie_time_zone[]=				"LAIKO JUOSTA";
const char lie_test_counter[]=			"TESTAVIMO SKAITIKLIS";
const char lie_abort[]=					"NUTRAUKTI";
const char lie_directly_connect_to_battery[]=	"TIESIOGINĖ JUNGTIS SU AKUMULIATORIUMI";
const char lie_insert_vin[]=			"ĮVESTI VIN KODĄ RANKINIU BŪDU?";
const char lie_test_not_suitable[]=		"NETIKAMAS GARANTIJAI TESTAVIMAS, TĘSTI?";
const char lie_yes[]=					"TAIP";
const char lie_no[]=					"NE";
const char lie_print_result[]=			"SPAUSDINTI REZULTATUS";
const char lie_print_successful[]=		"SĖKMINGAI IŠSPAUSDINTA, SPAUSDINTI DAR KARTĄ?";
const char lie_check_clamp[]=			"PATIKRINKITE GNYBTUS";
const char lie_insert_change_aa[]=		"ĮDĖKITE ARBA PAKEISKITE AA BATERIJAS";
const char lie_load_error[]=			"ĮKĖLIMO KLAIDA";
const char lie_voltage_high[]=			"AUKŠTA ĮTAMPA";
const char lie_voltage_unstable[]=		"NESTABILI ĮTAMPA\nPALAUKITE, KOL TAPS STABILI";
const char lie_is_it_6v[]=				"AR TAI 6 V AKUMULIATORIUS?";
const char lie_is_battery_charge[]=		"AR AKUMULIATORIUS ĮKRAUTAS";
const char lie_battery_in_vehicle[]=	"AKUMULIATORIUS TRANSPORTO PRIEMONĖJE";
const char lie_ss_test[]=				"SS. TESTAVIMAS";
const char lie_bat_test[]=				"AKUM. TESTAVIMAS";
const char lie_sys_test[]=				"SIST. TESTAVIMAS";
const char lie_ir_t[] =					"VV TESTAVIMAS";
const char lie_print[]=					"SPAUSDINIMAS";
const char lie_print_q[]=				"SPAUSDINTI?";
const char lie_erase[]=					"IŠTRINTI?";
const char lie_memory_erase[]=			"IŠTRINTI ATMINTĮ?";
const char lie_on[] =					"ĮJUNGTAS";
const char lie_off[] =					"IŠJUNGTAS";
const char lie_date[]=					"DATA:";
const char lie_time[]=					"LAIKAS:";
const char lie_invalid_vin[]=			"NETINKAMAS VIN, REZULTATAS NETINKAMAS GARANTIJAI";
const char lie_input_vin[]=				"ĮVESKITE VIN";
const char lie_enter_vin[]=				"ĮVESKITE VISĄ VIN";
const char lie_measured[]=				"PAMATUOTA:";
const char lie_cranking_voltage[]=		"PERAVIMO ĮTAMPA";
const char lie_lowest_voltage[]=		"ŽEMIAUSIA ĮTAMPA";
const char lie_high[]=					"AUKŠTA";
const char lie_normal[]=				"NORMALI";
const char lie_low[]=					"ŽEMA";
const char lie_no_detect[]=				"NEAPTIKTA";
const char lie_idle_voltage[]=			"ALT. ĮTAMPA TUŠČ. EIGA";
const char lie_sec[]=					"SEK.";
const char lie_ripple_detected[]=		"APTIKTA PULSACIJA";
const char lie_ripple_voltage[]=		"PULSACIJOS ĮTAM.";
const char lie_load_voltage[]=			"ALT. APKROVOS ĮTAMPA";
const char lie_point_to_battery_press_enter[]=	"NUKREIPKITE Į AKUMULIATORIŲ\nPASPAUSKITE ENTER";
const char lie_ota[]=					"OTA NAUJINIMAS";
const char lie_test_code[]=				"TESTAVIMO KODAS";
const char lie_connection_status[]=		"JUNGTIES BŪKLĖ";
const char lie_scan_vin_code[]=			"NUSKAITYTI VIN KODĄ?";
const char lie_vin_code[]=				"VIN KODAS";
const char lie_invalid_vin_rescan[]=	"NETINKAMAS VIN KODAS\nNUSKAITYTI IŠ NAUJO";
const char lie_wifi_power[]=			"WI-FI:";
const char lie_wifi_list[]=				"WI-FI SĄRAŠAS";
const char lie_downloading[]=			"ATSISIUNČIAMA...";
const char lie_upgrade_cancel[]=		"NAUJINIMAS ATŠAUKTAS?";
const char lie_upgrading[]=				"NAUJINAMA...";
const char lie_check_version[]=			"TIKRINKTI VERSIJĄ";
const char lie_refresh_list[] = 		"*ATNAUJINKITE SĄRAŠĄ";
const char lie_limited_connection[]=	"RIBOTAS RYŠYS";
const char lie_wifi_connected[]=		"PRIJUNGTAS WI-FI";
const char lie_connecting[]=			"JUNGIAMASI...";
const char lie_wifi_disconnect[]=		"WI-FI ATJUNGTAS";
const char lie_check_connection[]=		"TIKRINAMAS RYŠYS...";
const char lie_keep_clamps_connect[]=	"ĮSPĖJIMAS: LAIKYKITE GNYBTUS PRIJUNGTUS PRIE AKUMULIATORIAUS.";
//const char lie_skip[]=				"PRALEISTI";
const char lie_registration[] = "REGISTRACIJA";
const char lie_scan_qr_code[] = "NUSK. BR. KODĄ?";
const char lie_registering[] = "REGISTRUOJAMA...";


// printer
const char lie_prt_battery_test[] = 		"==AKUMULIATORIAUS TESTAVIMAS==";
const char lie_prt_by[] = 					"PAGAL:";
const char lie_prt_charging_test[] = 		"ĮKROVIMO TESTAVIMAS";
const char lie_prt_note[] = 				"PASTABA:";
const char lie_prt_diode_ripple[] =			"DIODŲ PULSACIJA";
const char lie_prt_ir_test[] = 				"==VV TESTAVIMAS==";
const char lie_prt_load_off[] = 			"APKROVA IŠJUNGTA";
const char lie_prt_load_on[] =				"APKROVA ĮJUNGTA";
const char lie_prt_rated[] = 				"NOMINALI:";
const char lie_prt_vin_num[] = 				"VIN NUMERIS:";
const char lie_prt_soc[] =					"ĮKROVIMO BŪSENA";
const char lie_prt_soh[] = 					"SVEIKATOS BŪSENA";
const char lie_prt_starter_test[] = 		"STARTERIO TESTAVIMAS";
const char lie_prt_start_stop[] = 			"==TESTAVIMO PALEIDIMAS-SUSTABDYMAS==";
const char lie_prt_temp[] = 				"TEMPERATŪRA:";
const char lie_prt_test_date[] = 			"TESTAVIMO DATA:";
const char lie_prt_test_report[] = 			"TESTAVIMO ATASKAITA";
#endif
#if POLSKI_en
// LCD
const char pol_language[] = "POLSKI";
const char pol_battery[]=				"AKUMULATOR";
const char pol_start_stop_test[]=		"TEST START-STOP";
const char pol_battery_test[]=			"TEST AKUMULATORA";//"TEST AKUMULATORA";//TODO
const char pol_system_test[]=			"TEST SYSTEMU";
const char pol_ir_test[]=				"TEST IR.";
const char pol_v_a_meter[]=				"V/A MIERNIK";
const char pol_setting[]=				"USTAWIENIA";
const char pol_test_in_vehicle[]=		"TEST W POJEŹDZIE";
const char pol_direct_connect[]=		"POŁĄCZENIE BEZPOŚREDNIE";
const char pol_jump_start_post[]=		"KABLE ROZRUCHOWE";
const char pol_brand[]=					"MARKA:";
const char pol_model[]=					"MODEL:";
const char pol_version[]=				"WERSJA";
const char pol_test_result[]=			"WYNIK TESTU";
const char pol_voltage[]=				"NAPIĘCIE:";
const char pol_good_n_pass[]=			"DOBRY I TEST ZDANY";
const char pol_good_n_recharge[]=		"DOBRY I DOŁADOWAĆ";
const char pol_bad_n_replace[]=			"ZŁY I WYMIENIĆ";
const char pol_caution[]=				"PRZESTROGA";
const char pol_recharge_n_retest[]=		"DOŁADOWAĆ I PRZETESTOWAĆ PONOWNIE";
const char pol_bad_cell_replace[]=		"WYMIENIĆ ZŁE OGNIWO";
const char pol_battery_type[]=			"TYP AKUMULATORA";
const char pol_flooded[]=				"ZALANY";
const char pol_agmf[]=					"PŁASKI AGM";
const char pol_agms[]=					"SPIRALNY AGM";
const char pol_select_rating[]=			"WYBÓR KLASY";
const char pol_set_capacity[]=			"USTAWIENIE POJEMNOŚCI";
const char pol_back_light[]=			"PODŚWIETLENIE LCD";
const char pol_language_select[]=		"WYBÓR JĘZYKA";
const char pol_clock[]=					"ZEGAR";
const char pol_clear_memory[]=			"KASOWANIE PAMIĘCI";
const char pol_information[]=			"INFORMACJE";
const char pol_time_zone[]=				"STREFA CZASOWA";
const char pol_test_counter[]=			"LICZNIK TESTÓW";
const char pol_abort[]=					"PRZERWAĆ";
const char pol_directly_connect_to_battery[]=	"POŁĄCZENIE BEZPOŚREDNIE DO AKUMULATORA";
const char pol_insert_vin[]=			"WPROWADZIĆ NUMER VIN RĘCZNIE?";
const char pol_test_not_suitable[]=		"TEST POZAGWARANCYJNY, KONTYNUOWAĆ?";
const char pol_yes[]=					"TAK";
const char pol_no[]=					"NIE";
const char pol_print_result[]=			"WYDRUKOWAĆ WYNIK";
const char pol_print_successful[]=		"WYDRUK ZAKOŃCZONY. WYDRUKOWAĆ PONOWNIE?";
const char pol_check_clamp[]=			"SPRAWDZIĆ ZACISK";
const char pol_insert_change_aa[]=		"WŁOŻYĆ LUB WYMIENIĆ BATERIE AA";
const char pol_load_error[]=			"BŁĄD ŁADOWANIA";
const char pol_voltage_high[]=			"WYSOKIE NAPIĘCIE";
const char pol_voltage_unstable[]=		"NIESTABILNE NAPIĘCIE\nPOCZEKAĆ NA USTABILIZOWANIE";
const char pol_is_it_6v[]=				"CZY TO AKUMULATOR 6 V?";
const char pol_is_battery_charge[]=		"CZY AKUMULATOR JEST NAŁADOWANY?";
const char pol_battery_in_vehicle[]=	"AKUMULATOR W POJEŹDZIE";
const char pol_ss_test[]=				"TEST SS.";
const char pol_bat_test[]=				"TEST AKUM.";
const char pol_sys_test[]=				"TEST SYS.";
const char pol_ir_t[] =					"TEST IR.";
const char pol_print[]=					"WYDRUK";
const char pol_print_q[]=				"WYDRUKOWAĆ?";
const char pol_erase[]=					"SKASOWAĆ?";
const char pol_memory_erase[]=			"SKASOWAĆ PAMIĘĆ?";
const char pol_on[] =					"WŁ.";
const char pol_off[] =					"WYŁ.";
const char pol_date[]=					"DATA:";
const char pol_time[]=					"GODZINA:";
const char pol_invalid_vin[]=			"NIEPRAWIDŁOWY VIN, WYNIK POZAGWARANCYJNY";
const char pol_input_vin[]=				"WPROWADZIĆ VIN";
const char pol_enter_vin[]=				"WPROWADZIĆ CAŁY VIN";
const char pol_measured[]=				"ZMIERZONE:";
const char pol_cranking_voltage[]=		"NAPIĘCIE ROZRUCHOWE";
const char pol_lowest_voltage[]=		"NAJNIŻSZE NAPIĘCIE";
const char pol_high[]=					"WYSOKIE";
const char pol_normal[]=				"NORMALNE";
const char pol_low[]=					"NISKIE";
const char pol_no_detect[]=				"NIE WYKRYTO";
const char pol_idle_voltage[]=			"NAPIĘCIE JAŁOWE ALTER.";
const char pol_sec[]=					"S";
const char pol_ripple_detected[]=		"WYKRYTO WAHNIĘCIE";
const char pol_ripple_voltage[]=		"NAP. WAHNIĘCIA";
const char pol_load_voltage[]=			"NAPIĘCIE OBCIĄŻENIA ALTER.";
const char pol_point_to_battery_press_enter[]=	"SKIEROWAĆ NA AKUMULATOR\nNACISNĄĆ ENTER";
const char pol_ota[]=					"UAKTUALNIENIE OTA";
const char pol_test_code[]=				"KOD TESTU";
const char pol_connection_status[]=		"STAN POŁĄCZENIA";
const char pol_scan_vin_code[]=			"ZESKANOWAĆ NUMER VIN?";
const char pol_vin_code[]=				"NUMER VIN";
const char pol_invalid_vin_rescan[]=	"NIEPRAWIDŁOWY NUMER VIN\nZESKANOWAĆ PONOWNIE";
const char pol_wifi_power[]=			"SIEĆ WIFI:";
const char pol_wifi_list[]=				"LISTA SIECI WIFI";
const char pol_downloading[]=			"POBIERANIE...";
const char pol_upgrade_cancel[]=		"ANULOWAĆ UAKTUALNIENIE?";
const char pol_upgrading[]=				"UAKTUALNIANIE";
const char pol_check_version[]=			"SPRAWDZANIE WERSJI";
const char pol_refresh_list[] = 		"*ODŚWIEŻYĆ LISTĘ";
const char pol_limited_connection[]=	"POŁĄCZENIE OGRANICZONE";
const char pol_wifi_connected[]=		"PODŁĄCZONA SIEĆ WIFI";
const char pol_connecting[]=			"ŁĄCZENIE...";
const char pol_wifi_disconnect[]=		"ODŁĄCZONA SIEĆ WIFI";
const char pol_check_connection[]=		"SPRAWDZANIE POŁĄCZENIA...";
const char pol_keep_clamps_connect[]=	"OSTRZEŻENIE: ZACISKI POZOSTAW PODŁĄCZONE DO AKUMULATORA.";
//const char pol_skip[]=				"POMINĄĆ";
const char pol_registration[] = "REJESTRACJA";
const char pol_scan_qr_code[] = "SKANOWAĆ KOD QR?";
const char pol_registering[] = "REJESTROWANIE...";


// printer
const char pol_prt_battery_test[] = 		"==TEST AKUMULATORA==";
const char pol_prt_by[] = 					"DO:";
const char pol_prt_charging_test[] = 		"TEST ŁADOWANIA";
const char pol_prt_note[] = 				"UWAGA:";
const char pol_prt_diode_ripple[] =			"DIODA WAHNIĘCIA";
const char pol_prt_ir_test[] = 				"==TEST IR.==";
const char pol_prt_load_off[] = 			"OBCIĄŻENIE WYŁ.";
const char pol_prt_load_on[] =				"OBCIĄŻENIE WŁ.";
const char pol_prt_rated[] = 				"KLASA:";
const char pol_prt_vin_num[] = 				"NUMER VIN:";
const char pol_prt_soc[] =					"STAN NAŁADOWANIA";
const char pol_prt_soh[] = 					"STAN";
const char pol_prt_starter_test[] = 		"TEST ROZRUSZNIKA";
const char pol_prt_start_stop[] = 			"==TEST AKUMULATORA START STOP==";
const char pol_prt_temp[] = 				"TEMPERATURA:";
const char pol_prt_test_date[] = 			"DATA TESTU:";
const char pol_prt_test_report[] = 			"RAPORT Z TESTU";
#endif
#if SLOVENCINA_en
// LCD
const char slo_language[] = "SLOVENČINA";
const char slo_battery[]=				"AKUMULÁTOR";
const char slo_start_stop_test[]=		"TEST ŠTART-STOP";
const char slo_battery_test[]=			"TEST AKUMULÁT.";
const char slo_system_test[]=			"TEST SYSTÉMU";
const char slo_ir_test[]=				"TEST VO";
const char slo_v_a_meter[]=				"VOLT- AMPÉRMETER";
const char slo_setting[]=				"NASTAVENIE";
const char slo_test_in_vehicle[]=		"TEST VO VOZIDLE";
const char slo_direct_connect[]=		"PRIAME PRIPOJENIE";
const char slo_jump_start_post[]=		"KOLÍK STRMÉHO ŠTARTU";
const char slo_brand[]=					"ZNAČKA:";
const char slo_model[]=					"MODEL:";
const char slo_version[]=				"VERZIA";
const char slo_test_result[]=			"VÝSLEDOK TESTU";
const char slo_voltage[]=				"NAPÄTIE:";
const char slo_good_n_pass[]=			"DOBRÝ, V PORIADKU";
const char slo_good_n_recharge[]=		"DOBRÝ, DOBIŤ";
const char slo_bad_n_replace[]=			"ZLÝ, VYMENIŤ";
const char slo_caution[]=				"UPOZORNENIE";
const char slo_recharge_n_retest[]=		"DOBIŤ A ZOPAKOVAŤ TEST";
const char slo_bad_cell_replace[]=		"VYMENIŤ CHYBNÝ ČLÁNOK";
const char slo_battery_type[]=			"TYP AKUMULÁTORA";
const char slo_flooded[]=				"BEŽNÝ";
const char slo_agmf[]=					"AGM S PL. DOSK.";
const char slo_agms[]=					"AGM ŠPIRÁLOVÝ";
const char slo_select_rating[]=			"VÝBER KLASIFIKÁCIE";
const char slo_set_capacity[]=			"NASTAVENIE KAPACITY";
const char slo_back_light[]=			"PODSVIETENIE LCD";
const char slo_language_select[]=		"VÝBER JAZYKA";
const char slo_clock[]=					"HODINY";
const char slo_clear_memory[]=			"VYMAZANIE PAMÄTE";
const char slo_information[]=			"INFORMÁCIE";
const char slo_time_zone[]=				"ČASOVÉ PÁSMO";
const char slo_test_counter[]=			"POČÍTADLO TESTOV";
const char slo_abort[]=					"ZRUŠIŤ";
const char slo_directly_connect_to_battery[]=	"PRIAME PRIPOJENIE K AKUMULÁTORU";
const char slo_insert_vin[]=			"VLOŽIŤ KÓD VIN MANUÁLNE?";
const char slo_test_not_suitable[]=		"TEST NIE JE VHODNÝ PRE ZÁRUKU. POKRAČOVAŤ?";
const char slo_yes[]=					"ÁNO";
const char slo_no[]=					"NIE";
const char slo_print_result[]=			"TLAČIŤ VÝSLEDOK";
const char slo_print_successful[]=		"TLAČ ÚSPEŠNÁ. TLAČIŤ ZNOVU?";
const char slo_check_clamp[]=			"SKONTROLOVAŤ SVORKU";
const char slo_insert_change_aa[]=		"VLOŽIŤ ALEBO VYMENIŤ BATÉRIE AA";
const char slo_load_error[]=			"CHYBA ZAŤAŽENIA";
const char slo_voltage_high[]=			"VYSOKÉ NAPÄTIE";
const char slo_voltage_unstable[]=		"NESTABILNÉ NAPÄTIE\nPOČKAŤ NA STABILNÉ";
const char slo_is_it_6v[]=				"IDE O 6 V AKUMULÁTOR?";
const char slo_is_battery_charge[]=		"JE AKUMULÁTOR NABITÝ?";
const char slo_battery_in_vehicle[]=	"AKUMULÁTOR VO VOZIDLE";
const char slo_ss_test[]=				"TEST SS";
const char slo_bat_test[]=				"TEST AKUM.";
const char slo_sys_test[]=				"TEST SYS.";
const char slo_ir_t[] =					"TEST VO";
const char slo_print[]=					"TLAČ";
const char slo_print_q[]=				"TLAČIŤ?";
const char slo_erase[]=					"VYMAZAŤ?";
const char slo_memory_erase[]=			"VYMAZAŤ PAMÄŤ?";
const char slo_on[] =					"ZAP.";
const char slo_off[] =					"VYP.";
const char slo_date[]=					"DÁTUM:";
const char slo_time[]=					"ČAS:";
const char slo_invalid_vin[]=			"NESPRÁVNY VIN, VÝSLEDOK NIE JE VHODNÝ PRE ZÁRUKU";
const char slo_input_vin[]=				"ZADAJTE VIN";
const char slo_enter_vin[]=				"ZADAJTE ÚPLNÝ VIN";
const char slo_measured[]=				"ODMERANÉ:";
const char slo_cranking_voltage[]=		"ŠTARTOVACIE NAPÄTIE";
const char slo_lowest_voltage[]=		"NAJNIŽŠIE NAPÄTIE";
const char slo_high[]=					"VYSOKÉ";
const char slo_normal[]=				"NORMÁLNE";
const char slo_low[]=					"NÍZKE";
const char slo_no_detect[]=				"NEZISTENÉ";
const char slo_idle_voltage[]=			"STRIED. VOĽNOBEŽNÉ NAPÄTIE";
const char slo_sec[]=					"SEK.";
const char slo_ripple_detected[]=		"ZVLNENIE ZISTENÉ";
const char slo_ripple_voltage[]=		"ZVLNENÉ NAPÄTIE";
const char slo_load_voltage[]=			"STRIED. NAPÄTIE PRI ZAŤAŽENÍ";
const char slo_point_to_battery_press_enter[]=	"NAMIERTE NA AKUMULÁTOR\nSTLAČTE ENTER";
const char slo_ota[]=					"AKTUALIZÁCIA OTA";
const char slo_test_code[]=				"KÓD TESTU";
const char slo_connection_status[]=		"STAV PRIPOJENIA";
const char slo_scan_vin_code[]=			"SKENOVAŤ KÓD VIN?";
const char slo_vin_code[]=				"KÓD VIN";
const char slo_invalid_vin_rescan[]=	"NEPLATNÝ KÓD VIN\nSKENOVAŤ ZNOVU";
const char slo_wifi_power[]=			"WI-FI:";
const char slo_wifi_list[]=				"ZOZNAM SIETÍ WI-FI";
const char slo_downloading[]=			"PREBERÁ SA...";
const char slo_upgrade_cancel[]=		"ZRUŠIŤ AKTUALIZÁCIU?";
const char slo_upgrading[]=				"AKTUALIZUJE SA...";
const char slo_check_version[]=			"KONTROLA VERZIE";
const char slo_refresh_list[] = 		"*OBNOVIŤ ZOZNAM";
const char slo_limited_connection[]=	"OBMEDZENÉ PRIPOJENIE";
const char slo_wifi_connected[]=		"PRIPOJENÉ K SIETI WI-FI";
const char slo_connecting[]=			"PRIPÁJA SA...";
const char slo_wifi_disconnect[]=		"ODPOJENÁ SIEŤ WI-FI";
const char slo_check_connection[]=		"KONTROLA PRIPOJENIA...";
const char slo_keep_clamps_connect[]=	"VAROVANIE: NECHAJTE SVORKY PRIPOJENÉ K AKUMULÁTORU.";
//const char slo_skip[]=				"PRESKOČIŤ";
const char slo_registration[] = "REGISTRÁCIA";
const char slo_scan_qr_code[] = "SKENOVAŤ KÓD QR?";
const char slo_registering[] = "REGISTRUJE SA...";


// printer
const char slo_prt_battery_test[] = 		"==TEST AKUMULÁTORA==";
const char slo_prt_by[] = 					"POMOCOU:";
const char slo_prt_charging_test[] = 		"TEST NABÍJANIA";
const char slo_prt_note[] = 				"POZNÁMKA:";
const char slo_prt_diode_ripple[] =			"ZVLNENIE DIÓDY";
const char slo_prt_ir_test[] = 				"==TEST VO==";
const char slo_prt_load_off[] = 			"ZAŤAŽENIE VYPNUTÉ";
const char slo_prt_load_on[] =				"ZAŤAŽENIE ZAPNUTÉ";
const char slo_prt_rated[] = 				"MENOVITÉ:";
const char slo_prt_vin_num[] = 				"ČÍSLO VIN:";
const char slo_prt_soc[] =					"STAV NABITIA";
const char slo_prt_soh[] = 					"CELKOVÝ STAV";
const char slo_prt_starter_test[] = 		"TEST ŠTARTÉRA";
const char slo_prt_start_stop[] = 			"==TEST ŠTART-STOP==";
const char slo_prt_temp[] = 				"TEPLOTA:";
const char slo_prt_test_date[] = 			"DÁTUM TESTU:";
const char slo_prt_test_report[] = 			"SPRÁVA O TESTE";
#endif
#if SLOVENSCINA_en
// LCD
const char sls_language[] = "SLOVENŠČINA";
const char sls_battery[]=				"AKUMULATOR";
const char sls_start_stop_test[]=		"ZAČ./UST. PRESK.";
const char sls_battery_test[]=			"PRESKUS AKUMUL.";
const char sls_system_test[]=			"PRESKUS SISTEMA";
const char sls_ir_test[]=				"IR. PRESKUS";
const char sls_v_a_meter[]=				"V./A. MERILNIK";
const char sls_setting[]=				"NASTAVITEV";
const char sls_test_in_vehicle[]=		"PRESKUS V VOZILU";
const char sls_direct_connect[]=		"NEPOSREDNA POVEZAVA";
const char sls_jump_start_post[]=		"TERMINAL ZA PRISILNI ZAGON";
const char sls_brand[]=					"ZNAMKA:";
const char sls_model[]=					"MODEL:";
const char sls_version[]=				"RAZLIČICA";
const char sls_test_result[]=			"REZULTAT PRESKUSA";
const char sls_voltage[]=				"NAPETOST:";
const char sls_good_n_pass[]=			"DOBRO IN OPRAVLJENO";
const char sls_good_n_recharge[]=		"DOBRO IN PONOVNO POLNJENJE";
const char sls_bad_n_replace[]=			"SLABO IN ZAMENJAVA";
const char sls_caution[]=				"POZOR";
const char sls_recharge_n_retest[]=		"PONOVNO POLNJENJE IN PONOVNI PRESKUS";
const char sls_bad_cell_replace[]=		"ZAMENJAJTE SLABO CELICO";
const char sls_battery_type[]=			"VRSTA AKUMULATORJA";
const char sls_flooded[]=				"POPLAVLJENO";
const char sls_agmf[]=					"PLOSKA PLOŠČICA AGM";
const char sls_agms[]=					"CIKLIČNI AGM";
const char sls_select_rating[]=			"IZBERITE NAZIVNO MOČ";
const char sls_set_capacity[]=			"NASTAVI ZMOGLJIVOST";
const char sls_back_light[]=			"OSVETLITEV LCD-ZASLONA";
const char sls_language_select[]=		"IZBIRA JEZIKA";
const char sls_clock[]=					"URA";
const char sls_clear_memory[]=			"POČISTI POMNILNIK";
const char sls_information[]=			"INFORMACIJE";
const char sls_time_zone[]=				"ČASOVNI PAS";
const char sls_test_counter[]=			"ŠTEVEC PRESKUSOV";
const char sls_abort[]=					"PREKINI";
const char sls_directly_connect_to_battery[]=	"NEPOSREDNA PRIKLJUČITEV NA AKUMULATOR";
const char sls_insert_vin[]=			"ŽELITE ROČNO VNESTI KODO VIN?";
const char sls_test_not_suitable[]=		"PRESKUS NI PRIMEREN ZA GARANCIJO. ŽELITE NADALJEVATI?";
const char sls_yes[]=					"DA";
const char sls_no[]=					"NE";
const char sls_print_result[]=			"NATISNI REZULTAT";
const char sls_print_successful[]=		"TISKANJE JE USPELO. ŽELITE ZNOVA NATISNITI?";
const char sls_check_clamp[]=			"PREVERITE SPONKO";
const char sls_insert_change_aa[]=		"VSTAVITE ALI ZAMENJAJTE BATERIJE AA";
const char sls_load_error[]=			"NAPAKA OBREMENITVE";
const char sls_voltage_high[]=			"VISOKA NAPETOST";
const char sls_voltage_unstable[]=		"NESTABILNA NAPETOST\nPOČAKAJTE NA STABILNO";
const char sls_is_it_6v[]=				"ALI JE 6 V AKUMULATOR?";
const char sls_is_battery_charge[]=		"ALI JE AKUMULATOR NAPOLNJEN?";
const char sls_battery_in_vehicle[]=	"AKUMULATOR V VOZILU";
const char sls_ss_test[]=				"SS. PRESKUS";
const char sls_bat_test[]=				"AKUM. PRESKUS";
const char sls_sys_test[]=				"SIS. PRESKUS";
const char sls_ir_t[] =					"IR. PRESKUS";
const char sls_print[]=					"NATISNI";
const char sls_print_q[]=				"ŽELITE NATISNITI?";
const char sls_erase[]=					"ŽELITE IZBRISATI?";
const char sls_memory_erase[]=			"ŽELITE IZBRISATI POMNILNIK?";
const char sls_on[] =					"VKLOPI";
const char sls_off[] =					"IZKLOPI";
const char sls_date[]=					"DATUM:";
const char sls_time[]=					"URA:";
const char sls_invalid_vin[]=			"NEVELJAVNA KODA VIN, REZULTAT NI PRIMEREN ZA GARANCIJO";
const char sls_input_vin[]=				"VNESITE VIN";
const char sls_enter_vin[]=				"VNESITE CELOTNO KODO VIN";
const char sls_measured[]=				"IZMERJENA:";
const char sls_cranking_voltage[]=		"NAPETOST PRI ZAGONU";
const char sls_lowest_voltage[]=		"NAJNIŽJA NAPETOST";
const char sls_high[]=					"VISOKA";
const char sls_normal[]=				"OBIČAJNA";
const char sls_low[]=					"NIZKA";
const char sls_no_detect[]=				"NI ZAZNANA";
const char sls_idle_voltage[]=			"IZMEN. NAPETOST PRI PROSTEM TEKU";
const char sls_sec[]=					"SEK";
const char sls_ripple_detected[]=		"ZAZNANO VALOVANJE";
const char sls_ripple_voltage[]=		"VALOVANJE NAPETOSTI";
const char sls_load_voltage[]=			"IZMEN. OBREMENILNA NAPETOST";
const char sls_point_to_battery_press_enter[]=	"TOČKA DO AKUMULATORJA\nPRITISNITE ENTER";
const char sls_ota[]=					"NADGRADNJO OTA";
const char sls_test_code[]=				"KODA PRESKUSA";
const char sls_connection_status[]=		"STANJE POVEZAVE";
const char sls_scan_vin_code[]=			"ŽELITE OPTIČNO PREBRATI KODO VIN?";
const char sls_vin_code[]=				"KODA VIN";
const char sls_invalid_vin_rescan[]=	"NEVELJAVNA KODA VIN\nZNOVA PREGLEJ";
const char sls_wifi_power[]=			"WI-FI:";
const char sls_wifi_list[]=				"SEZNAM OMREŽIJ WI-FI";
const char sls_downloading[]=			"PRENAŠANJE ...";
const char sls_upgrade_cancel[]=		"ŽELITE PREKLICATI NADGRADNJO?";
const char sls_upgrading[]=				"NADGRAJEVANJE ...";
const char sls_check_version[]=			"PREVERITE RAZLIČICO";
const char sls_refresh_list[] = 		"*OSVEŽI SEZNAM";
const char sls_limited_connection[]=	"OMEJENA POVEZAVA";
const char sls_wifi_connected[]=		"POVEZAVA Z OMREŽJEM WI-FI JE VZPOSTAVLJENA";
const char sls_connecting[]=			"VZPOSTAVLJANJE POVEZAVE ...";
const char sls_wifi_disconnect[]=		"POVEZAVA Z OMREŽJEM WI-FI JE PREKINJENA";
const char sls_check_connection[]=		"PREVERI POVEZAVO ...";
const char sls_keep_clamps_connect[]=	"OPOZORILO: SPONKE NAJ BODO PRIKLJUČENE NA AKUMULATOR";
//const char sls_skip[]=				"PRESKOČI";
const char sls_registration[] = "REGISTRACIJA";
const char sls_scan_qr_code[] = "SKENIR. KODO QR?";
const char sls_registering[] = "REGISTRIRANJE...";


// printer
const char sls_prt_battery_test[] = 		"==PRESKUS AKUMULATORJA==";
const char sls_prt_by[] = 					"AVTOR:";
const char sls_prt_charging_test[] = 		"PRESKUS POLNJENJA";
const char sls_prt_note[] = 				"OPOMBA:";
const char sls_prt_diode_ripple[] =			"VALOVANJE DIODE";
const char sls_prt_ir_test[] = 				"==IR. PRESKUS==";
const char sls_prt_load_off[] = 			"IZKLOPLJENA OBREMENITEV";
const char sls_prt_load_on[] =				"VKLOPLJENA OBREMENITEV";
const char sls_prt_rated[] = 				"NAZIVNA:";
const char sls_prt_vin_num[] = 				"ŠTEVILKA VIN:";
const char sls_prt_soc[] =					"STANJE POLNJENJA";
const char sls_prt_soh[] = 					"USTREZNOST STANJA";
const char sls_prt_starter_test[] = 		"PRESKUS ZAGANJALNIKA";
const char sls_prt_start_stop[] = 			"==ZAČETEK/USTAVITEV PRESKUSA==";
const char sls_prt_temp[] = 				"TEMPERATURA:";
const char sls_prt_test_date[] = 			"DATUM PRESKUSA:";
const char sls_prt_test_report[] = 			"POROČILO O PRESKUSU";
#endif
#if TURKU_en
// LCD
const char tur_language[] = "TURKU";
const char tur_battery[]=				"AKÜ";
const char tur_start_stop_test[]=		"BAŞLA-DUR TESTİ";
const char tur_battery_test[]=			"AKÜ TESTİ";
const char tur_system_test[]=			"SİSTEM TESTİ";
const char tur_ir_test[]=				"IR. TESTİ";
const char tur_v_a_meter[]=				"V./A. ÖLÇER";
const char tur_setting[]=				"AYAR";
const char tur_test_in_vehicle[]=		"ARAÇTA TEST";
const char tur_direct_connect[]=		"DOĞRUDAN BAĞLANTI";
const char tur_jump_start_post[]=		"TAKVİYEYLE ÇALIŞTIRMA KUTUP BAŞI";
const char tur_brand[]=					"MARKA:";
const char tur_model[]=					"MODEL:";
const char tur_version[]=				"SÜRÜM";
const char tur_test_result[]=			"TEST SONUCU";
const char tur_voltage[]=				"VOLTAJ:";
const char tur_good_n_pass[]=			"İYİ DURUMDA-BAŞARILI";
const char tur_good_n_recharge[]=		"İYİ DURUMDA-ŞARJ GEREKLİ";
const char tur_bad_n_replace[]=			"KÖTÜ DURUMDA-DEĞİŞTİRİN";
const char tur_caution[]=				"DİKKAT";
const char tur_recharge_n_retest[]=		"ŞARJ EDİN-YENİDEN TEST EDİN";
const char tur_bad_cell_replace[]=		"HÜCRE KÖTÜ DURUMDA-DEĞİŞTİRİN";
const char tur_battery_type[]=			"AKÜ TÜRÜ";
const char tur_flooded[]=				"TAŞMALI";
const char tur_agmf[]=					"DÜZ PLAKALI AGM";
const char tur_agms[]=					"SPİRAL AGM";
const char tur_select_rating[]=			"GÜÇ SINIFLANDIRMASI SEÇİMİ";
const char tur_set_capacity[]=			"KAPASİTE AYARI";
const char tur_back_light[]=			"LCD ARKA IŞIĞI";
const char tur_language_select[]=		"DİL SEÇİMİ";
const char tur_clock[]=					"SAAT";
const char tur_clear_memory[]=			"BELLEĞİ TEMİZLE";
const char tur_information[]=			"BİLGİLER";
const char tur_time_zone[]=				"SAAT DİLİMİ";
const char tur_test_counter[]=			"TEST SAYACI";
const char tur_abort[]=					"ÇIKIŞ";
const char tur_directly_connect_to_battery[]=	"AKÜYE DOĞRUDAN BAĞLANTI";
const char tur_insert_vin[]=			"VIN KODU MANUEL OLARAK GİRİLSİN Mİ?";
const char tur_test_not_suitable[]=		"TEST GARANTİ KAPSAMI İÇİN UYGUN DEĞİL, DEVAM EDİLSİN Mİ?";
const char tur_yes[]=					"EVET";
const char tur_no[]=					"HAYIR";
const char tur_print_result[]=			"SONUCU YAZDIR";
const char tur_print_successful[]=		"YAZDIRMA BAŞARILI, YENİDEN YAZDIRILSIN MI?";
const char tur_check_clamp[]=			"MAŞA KONTROLÜ";
const char tur_insert_change_aa[]=		"AA PİL YERLEŞTİRİN VEYA PİLLERİ DEĞİŞTİRİN";
const char tur_load_error[]=			"YÜK HATASI";
const char tur_voltage_high[]=			"YÜKSEK VOLTAJ";
const char tur_voltage_unstable[]=		"KARARSIZ VOLTAJ\nKARARLI DURUM İÇİN BEKLEYİN";
const char tur_is_it_6v[]=				"6 V'LUK AKÜ MÜ?";
const char tur_is_battery_charge[]=		"AKÜ ŞARJ EDİLDİ Mİ?";
const char tur_battery_in_vehicle[]=	"AKÜ ARAÇTA";
const char tur_ss_test[]=				"SS. TESTİ";
const char tur_bat_test[]=				"AKÜ TESTİ";
const char tur_sys_test[]=				"SİS. TESTİ";
const char tur_ir_t[] =					"IR. TESTİ";
const char tur_print[]=					"YAZDIR";
const char tur_print_q[]=				"YAZDIRILSIN MI?";
const char tur_erase[]=					"SİLİNSİN Mİ?";
const char tur_memory_erase[]=			"BELLEK SİLİNSİN Mİ?";
const char tur_on[] =					"AÇIK";
const char tur_off[] =					"KAPALI";
const char tur_date[]=					"TARİH:";
const char tur_time[]=					"SAAT:";
const char tur_invalid_vin[]=			"GEÇERSİZ VIN, SONUÇ GARANTİ KAPSAMI İÇİN UYGUN DEĞİL";
const char tur_input_vin[]=				"GİRİŞ VIN'I";
const char tur_enter_vin[]=				"VIN'I EKSİKSİZ GİRİN";
const char tur_measured[]=				"ÖLÇÜLDÜ:";
const char tur_cranking_voltage[]=		"MARŞ VOLTAJI";
const char tur_lowest_voltage[]=		"EN DÜŞÜK VOLTAJ";
const char tur_high[]=					"YÜKSEK";
const char tur_normal[]=				"NORMAL";
const char tur_low[]=					"DÜŞÜK";
const char tur_no_detect[]=				"TESPİT EDİLMEDİ";
const char tur_idle_voltage[]=			"ALT. RÖLANTİ VOLTAJI";
const char tur_sec[]=					"SANİYE";
const char tur_ripple_detected[]=		"DALGALANMA TESPİT EDİLDİ";
const char tur_ripple_voltage[]=		"DALGALANMA VOLT.";
const char tur_load_voltage[]=			"ALT. YÜK VOLTAJI";
const char tur_point_to_battery_press_enter[]=	"AKÜYE DOĞRULTUN\nENTER TUŞUNA BASIN";
const char tur_ota[]=					"OTA YÜKSELTMESİ";
const char tur_test_code[]=				"TEST KODU";
const char tur_connection_status[]=		"BAĞLANTI DURUMU";
const char tur_scan_vin_code[]=			"VIN KODU TARANSIN MI?";
const char tur_vin_code[]=				"VIN KODU";
const char tur_invalid_vin_rescan[]=	"GEÇERSİZ VIN KODU\nYENİDEN TARA";
const char tur_wifi_power[]=			"WIFI GÜCÜ:";
const char tur_wifi_list[]=				"WIFI LİSTESİ";
const char tur_downloading[]=			"KARŞIDAN YÜKLENİYOR...";
const char tur_upgrade_cancel[]=		"YÜKSELTME İPTAL EDİLSİN Mİ?";
const char tur_upgrading[]=				"YÜKSELTİLİYOR...";
const char tur_check_version[]=			"SÜRÜMÜ KONTROL ET";
const char tur_refresh_list[] = 		"*LİSTEYİ YENİLE";
const char tur_limited_connection[]=	"SINIRLI BAĞLANTI";
const char tur_wifi_connected[]=		"WIFI BAĞLANTISI KURULDU";
const char tur_connecting[]=			"BAĞLANIYOR...";
const char tur_wifi_disconnect[]=		"WIFI BAĞLANTISI KESİLDİ";
const char tur_check_connection[]=		"BAĞLANTI KONTROL EDİLİYOR...";
const char tur_keep_clamps_connect[]=	"UYARI: MAŞALARI AKÜYE BAĞLI TUTUN.";
//const char tur_skip[]=				"ATLA";
const char tur_registration[] = "KAYIT";
const char tur_scan_qr_code[] = "QR KODU TARAMA?";
const char tur_registering[] = "KAYDEDİLİYOR...";


// printer
const char tur_prt_battery_test[] = 		"==AKÜ TESTİ==";
const char tur_prt_by[] = 					"İŞLEM SORUMLUSU:";
const char tur_prt_charging_test[] = 		"ŞARJ TESTİ";
const char tur_prt_note[] = 				"NOT:";
const char tur_prt_diode_ripple[] =			"DİYOT DALGALANMASI";
const char tur_prt_ir_test[] = 				"==IR. TESTİ==";
const char tur_prt_load_off[] = 			"YÜK KAPALI";
const char tur_prt_load_on[] =				"YÜK AÇIK";
const char tur_prt_rated[] = 				"NOMİNAL:";
const char tur_prt_vin_num[] = 				"VIN NUMARASI:";
const char tur_prt_soc[] =					"ŞARJ DURUMU";
const char tur_prt_soh[] = 					"GENEL DURUM";
const char tur_prt_starter_test[] = 		"MARŞ MOTORU TESTİ";
const char tur_prt_start_stop[] = 			"==BAŞLATMA-DURDURMA TESTİ==";
const char tur_prt_temp[] = 				"SICAKLIK:";
const char tur_prt_test_date[] = 			"TEST TARİHİ:";
const char tur_prt_test_report[] = 			"TEST RAPORU";
#endif
#if EESTI_en
// LCD
const char ees_language[] = "EESTI";
const char ees_battery[]=				"AKU";
const char ees_start_stop_test[]=		"KÄIV-PEATA TEST";
const char ees_battery_test[]=			"AKUTEST";
const char ees_system_test[]=			"SÜSTEEMI TEST";
const char ees_ir_test[]=				"IR. TEST";
const char ees_v_a_meter[]=				"V./A. MEETER";
const char ees_setting[]=				"SEADISTUS";
const char ees_test_in_vehicle[]=		"TEST SÕIDUKIS";
const char ees_direct_connect[]=		"OTSEÜHENDUS";
const char ees_jump_start_post[]=		"KÄIVITUSKLEMM";
const char ees_brand[]=					"MARK:";
const char ees_model[]=					"MUDEL:";
const char ees_version[]=				"VERSIOON";
const char ees_test_result[]=			"TESTI TULEMUS";
const char ees_voltage[]=				"PINGE:";
const char ees_good_n_pass[]=			"HEA JA LÄBITUD";
const char ees_good_n_recharge[]=		"HEA JA LAADIDA";
const char ees_bad_n_replace[]=			"HALB JA ASENDADA";
const char ees_caution[]=				"ETTEVAATUST";
const char ees_recharge_n_retest[]=		"LAADIDA JA UUESTI TESTIDA";
const char ees_bad_cell_replace[]=		"HALB ELEMENT ASENDADA";
const char ees_battery_type[]=			"AKU TÜÜP";
const char ees_flooded[]=				"AVATUD";
const char ees_agmf[]=					"AGM LAMEPLAAT";
const char ees_agms[]=					"AGM SPIRAAL";
const char ees_select_rating[]=			"VALI LIIGITUSSÜSTEEM";
const char ees_set_capacity[]=			"MÄÄRA MAHTUVUS";
const char ees_back_light[]=			"LCD-TAUSTVALGUS";
const char ees_language_select[]=		"KEELE VALIMINE";
const char ees_clock[]=					"KELL";
const char ees_clear_memory[]=			"MÄLU TÜHJENDAMINE";
const char ees_information[]=			"TEAVE";
const char ees_time_zone[]=				"AJAVÖÖND";
const char ees_test_counter[]=			"TESTILOENDUR";
const char ees_abort[]=					"KATKESTADA";
const char ees_directly_connect_to_battery[]=	"ÜHENDADA OTSE AKUGA";
const char ees_insert_vin[]=			"SISESTADA VIN-KOOD KÄSITSI?";
const char ees_test_not_suitable[]=		"TEST POLE GARANTII-KÕLBLIK. JÄTKATA?";
const char ees_yes[]=					"JAH";
const char ees_no[]=					"EI";
const char ees_print_result[]=			"PRINTIDA TULEMUS";
const char ees_print_successful[]=		"PRINDITUD. PRINTIDA UUESTI?";
const char ees_check_clamp[]=			"KONTROLLIDA KLAMBRIT";
const char ees_insert_change_aa[]=		"SISESTADA VÕI VAHETADA AA AKUD";
const char ees_load_error[]=			"LAADIMISTÕRGE";
const char ees_voltage_high[]=			"KÕRGE PINGE";
const char ees_voltage_unstable[]=		"EBASTABIILNE PINGE\nOODATA STABIILSUST";
const char ees_is_it_6v[]=				"KAS ON 6 V AKU";
const char ees_is_battery_charge[]=		"KAS AKU LAETUD";
const char ees_battery_in_vehicle[]=	"AKU SÕIDUKIS";
const char ees_ss_test[]=				"SS. TEST";
const char ees_bat_test[]=				"AKU TEST";
const char ees_sys_test[]=				"SÜS. TEST";
const char ees_ir_t[] =					"IR. TEST";
const char ees_print[]=					"PRINTIMINE";
const char ees_print_q[]=				"PRINTIDA?";
const char ees_erase[]=					"KUSTUTADA?";
const char ees_memory_erase[]=			"KUSTUTADA MÄLU?";
const char ees_on[] =					"SEES";
const char ees_off[] =					"VÄLJAS";
const char ees_date[]=					"KUUPÄEV:";
const char ees_time[]=					"KELLAAEG:";
const char ees_invalid_vin[]=			"SOBIMATU VIN, TULEMUS POLE GARANTIIKÕLBLIK";
const char ees_input_vin[]=				"SISESTA VIN";
const char ees_enter_vin[]=				"SISESTA TÄIELIK VIN";
const char ees_measured[]=				"MÕÕDETUD:";
const char ees_cranking_voltage[]=		"KÄIVITUSPINGE";
const char ees_lowest_voltage[]=		"MADALAIM PINGE";
const char ees_high[]=					"KÕRGE";
const char ees_normal[]=				"NORMAALNE";
const char ees_low[]=					"MADAL";
const char ees_no_detect[]=				"EI TUVASTATUD";
const char ees_idle_voltage[]=			"GEN. LISAKOORMUSETA PINGE";
const char ees_sec[]=					"SEK";
const char ees_ripple_detected[]=		"TUVASTATUD PULSATSIOON";
const char ees_ripple_voltage[]=		"PULSATSIOONI V.";
const char ees_load_voltage[]=			"GEN. LAADIMISPINGE";
const char ees_point_to_battery_press_enter[]=	"SUUNA AKULE\nVAJUTA ENTER";
const char ees_ota[]=					"OTA UUENDUS";
const char ees_test_code[]=				"TESTI KOOD";
const char ees_connection_status[]=		"ÜHENDUSE OLEK";
const char ees_scan_vin_code[]=			"SKANNIDA VIN-KOOD?";
const char ees_vin_code[]=				"VIN-KOOD";
const char ees_invalid_vin_rescan[]=	"SOBIMATU VIN-KOOD\nSKANNI UUESTI";
const char ees_wifi_power[]=			"WIFI TOIDE:";
const char ees_wifi_list[]=				"WIFI LOEND";
const char ees_downloading[]=			"ALLALAADIMINE...";
const char ees_upgrade_cancel[]=		"UUENDUS TÜHISTADA?";
const char ees_upgrading[]=				"UUENDAMINE...";
const char ees_check_version[]=			"KONTROLLI VERSIOONI";
const char ees_refresh_list[] = 		"*VÄRSKENDA LOEND";
const char ees_limited_connection[]=	"PIIRATUD ÜHENDUS";
const char ees_wifi_connected[]=		"WIFI ÜHENDATUD";
const char ees_connecting[]=			"ÜHENDAMINE...";
const char ees_wifi_disconnect[]=		"WIFI KATKESTATUD";
const char ees_check_connection[]=		"ÜHENDUSE KONTROLLIMINE...";
const char ees_keep_clamps_connect[]=	"HOIATUS! JÄTKE KLAMBRID AKUGA ÜHENDATUKS.";
//const char ees_skip[]=				"JÄTA VAHELE";
const char ees_registration[] = "REGISTREERIMINE";
const char ees_scan_qr_code[] = "SKANNI QR-KOOD?";
const char ees_registering[] = "REGISTREERIM...";

// printer
const char ees_prt_battery_test[] = 		"==AKUTEST==";
const char ees_prt_by[] = 					"ALUS:";
const char ees_prt_charging_test[] = 		"LAADIMISTEST";
const char ees_prt_note[] = 				"MÄRKUS:";
const char ees_prt_diode_ripple[] =			"DIOODI PULSATSIOON";
const char ees_prt_ir_test[] = 				"==IR. TEST==";
const char ees_prt_load_off[] = 			"KOORMUS VÄLJAS";
const char ees_prt_load_on[] =				"KOORMUS SEES";
const char ees_prt_rated[] = 				"LIIGITUS:";
const char ees_prt_vin_num[] = 				"VIN-NUMBER:";
const char ees_prt_soc[] =					"LAENGU OLEK";
const char ees_prt_soh[] = 					"SEISUNDI OLEK";
const char ees_prt_starter_test[] = 		"STARTERI TEST";
const char ees_prt_start_stop[] = 			"==KÄIVITA-PEATA TEST==";
const char ees_prt_temp[] = 				"TEMPERATUUR:";
const char ees_prt_test_date[] = 			"TESTI KUUPÄEV:";
const char ees_prt_test_report[] = 			"TESTI ARUANNE";
#endif
#if LATVIESU_en
// LCD
const char lat_language[] = "LATVIESU";
const char lat_battery[]=				"AKUMULATORS";
const char lat_start_stop_test[]=		"IESL-IZSL. PĀRB.";
const char lat_battery_test[]=			"AKUMULAT. PĀRB.";
const char lat_system_test[]=			"SISTĒMAS PĀRB.";
const char lat_ir_test[]=				"IR. PĀRBAUDE";
const char lat_v_a_meter[]=				"V./A. MĒRĪTĀJS";
const char lat_setting[]=				"IESTATĪŠANA";
const char lat_test_in_vehicle[]=		"PĀRBAUDE TRANSPORTLĪDZEKLĪ";
const char lat_direct_connect[]=		"TIEŠS SAVIENOJUMS";
const char lat_jump_start_post[]=		"PIESTARTĒŠANAS PIESLĒGVIETA";
const char lat_brand[]=					"RAŽOTĀJS:";
const char lat_model[]=					"MODELIS:";
const char lat_version[]=				"VERSIJA";
const char lat_test_result[]=			"PĀRBAUDES REZULTĀTS";
const char lat_voltage[]=				"SPRIEGUMS:";
const char lat_good_n_pass[]=			"LABI UN NOKĀRTOTS";
const char lat_good_n_recharge[]=		"LABI UN UZLĀDĒT";
const char lat_bad_n_replace[]=			"SLIKTI UN NOMAINĪT";
const char lat_caution[]=				"UZMANĪBU";
const char lat_recharge_n_retest[]=		"UZLĀDĒT UN PĀRBAUDĪT VĒLREIZ";
const char lat_bad_cell_replace[]=		"SLIKTS ELEMENTS NOMAINĪT";
const char lat_battery_type[]=			"AKUMULATORA TIPS";
const char lat_flooded[]=				"KLASISKS";
const char lat_agmf[]=					"AGM PLAKANS";
const char lat_agms[]=					"AGM SPIRĀLVEIDA";
const char lat_select_rating[]=			"IZVĒLĒTIES NOMINĀLVĒRTĪBU";
const char lat_set_capacity[]=			"IESTATĪT KAPACITĀTI";
const char lat_back_light[]=			"LCD APGAISMOJUMS";
const char lat_language_select[]=		"VALODAS IZVĒLE";
const char lat_clock[]=					"PULKSTENIS";
const char lat_clear_memory[]=			"NOTĪRĪT ATMIŅU";
const char lat_information[]=			"INFORMĀCIJA";
const char lat_time_zone[]=				"LAIKA JOSLA";
const char lat_test_counter[]=			"PĀRBAUDES SKAITĪTĀJS";
const char lat_abort[]=					"ATCELT";
const char lat_directly_connect_to_battery[]=	"TIEŠS SAVIENOJUMS AR AKUMULATORU";
const char lat_insert_vin[]=			"VAI IEVIETOT VIN KODU MANUĀLI?";
const char lat_test_not_suitable[]=		"PĀRBAUDE NAV PIEMĒROTA GARANTIJAI, VAI TURPINĀT?";
const char lat_yes[]=					"JĀ";
const char lat_no[]=					"NĒ";
const char lat_print_result[]=			"DRUKĀT REZULTĀTU";
const char lat_print_successful[]=		"DRUKĀŠANA SEKMĪGA, VAI DRUKĀT VĒLREIZ";
const char lat_check_clamp[]=			"PĀRBAUDIET SPAILI";
const char lat_insert_change_aa[]=		"IEVIETOJIET VAI NOMAINIET AA BATERIJAS";
const char lat_load_error[]=			"IELĀDES KĻŪDA";
const char lat_voltage_high[]=			"AUGSTS SPRIEGUMS";
const char lat_voltage_unstable[]=		"NESTABILS SPRIEGUMS\nUZGAIDIET, LĪDZ STABILIZĒJAS";
const char lat_is_it_6v[]=				"VAI ŠIS IR 6V AKUMULATORS";
const char lat_is_battery_charge[]=		"VAI AKUMULATORS IR UZLĀDĒTS";
const char lat_battery_in_vehicle[]=	"AKUMULATORS TRANSPORTLĪDZEKLĪ";
const char lat_ss_test[]=				"SS. PĀRBAUDE";
const char lat_bat_test[]=				"AK. PĀRBAUDE";
const char lat_sys_test[]=				"SIS. PĀRBAUDE";
const char lat_ir_t[] =					"IR. PĀRBAUDE";
const char lat_print[]=					"DRUKĀT";
const char lat_print_q[]=				"DRUKĀT?";
const char lat_erase[]=					"DZĒST?";
const char lat_memory_erase[]=			"DZĒST ATMIŅU?";
const char lat_on[] =					"IESL.";
const char lat_off[] =					"IZSL.";
const char lat_date[]=					"DATUMS:";
const char lat_time[]=					"LAIKS:";
const char lat_invalid_vin[]=			"NEDERĪGS VIN, REZULTĀTS NAV PIEMĒROTS GARANTIJAI";
const char lat_input_vin[]=				"IEVADIET VIN";
const char lat_enter_vin[]=				"IEVADIET PILNU VIN";
const char lat_measured[]=				"IZMĒRĪTS:";
const char lat_cranking_voltage[]=		"STARTĒŠANAS SPRIEGUMS";
const char lat_lowest_voltage[]=		"ZEMĀKAIS SPRIEGUMS";
const char lat_high[]=					"AUGSTS";
const char lat_normal[]=				"NORMĀLS";
const char lat_low[]=					"ZEMS";
const char lat_no_detect[]=				"NAV NOTEIKTS";
const char lat_idle_voltage[]=			"ALT. TUKŠG. SPRIEGUMS";
const char lat_sec[]=					"SEK.";
const char lat_ripple_detected[]=		"NOTEIKTA PULSĀCIJA";
const char lat_ripple_voltage[]=		"PULSĀC. SPRIEG.";
const char lat_load_voltage[]=			"ALT. SLODZES SPRIEGUMS";
const char lat_point_to_battery_press_enter[]=	"NORĀDĪT UZ AKUMULATORU\nNOSPIEDIET ENTER";
const char lat_ota[]=					"OTA JAUNINĀŠANA";
const char lat_test_code[]=				"PĀRBAUDES KODS";
const char lat_connection_status[]=		"SAVIENOJUMA STATUSS";
const char lat_scan_vin_code[]=			"SKENĒT VIN KODU?";
const char lat_vin_code[]=				"VIN KODS";
const char lat_invalid_vin_rescan[]=	"NEDERĪGS VIN KODS\nSKENĒT VĒLREIZ";
const char lat_wifi_power[]=			"WIFI JAUDA:";
const char lat_wifi_list[]=				"WIFI SARAKSTS";
const char lat_downloading[]=			"LEJUPIELĀDE...";
const char lat_upgrade_cancel[]=		"ATCELT JAUNINĀŠANU?";
const char lat_upgrading[]=				"JAUNINĀŠANA...";
const char lat_check_version[]=			"PĀRBAUDĪT VERSIJU";
const char lat_refresh_list[] = 		"*ATSVAIDZINĀT SARAKSTU";
const char lat_limited_connection[]=	"IEROBEŽOTS SAVIENOJUMS";
const char lat_wifi_connected[]=		"WIFI SAVIENOTS";
const char lat_connecting[]=			"SAVIENOŠANA...";
const char lat_wifi_disconnect[]=		"WIFI ATVIENOTS";
const char lat_check_connection[]=		"PĀRBAUDA SAVIENOJUMU...";
const char lat_keep_clamps_connect[]=	"BRĪDINĀJUMS! SAGLABĀJIET SPAIĻU SAVIENOJUMU AR AKUMULATORU.";
//const char lat_skip[]=				"IZLAIST";
const char lat_registration[] = "REĢISTRĀCIJA";
const char lat_scan_qr_code[] = "SKENĒT QR KODU?";
const char lat_registering[] = "REĢISTRĒ...";


// printer
const char lat_prt_battery_test[] = 		"==AKUMULATORA PĀRBAUDE==";
const char lat_prt_by[] = 					"PĒC:";
const char lat_prt_charging_test[] = 		"LĀDĒŠANAS PĀRBAUDE";
const char lat_prt_note[] = 				"PIEZĪME:";
const char lat_prt_diode_ripple[] =			"DIODES PULSĀCIJA";
const char lat_prt_ir_test[] = 				"==IR. PĀRBAUDE==";
const char lat_prt_load_off[] = 			"BEZ SLODZES";
const char lat_prt_load_on[] =				"AR SLODZI";
const char lat_prt_rated[] = 				"NOMINĀLS:";
const char lat_prt_vin_num[] = 				"VIN NUMURS:";
const char lat_prt_soc[] =					"UZLĀDES STĀVOKLIS";
const char lat_prt_soh[] = 					"DARBSPĒJAS STĀVOKLIS";
const char lat_prt_starter_test[] = 		"STARTERA PĀRBAUDE";
const char lat_prt_start_stop[] = 			"==IESL.-IZSL. PĀRBAUDE==";
const char lat_prt_temp[] = 				"TEMPERATŪRA:";
const char lat_prt_test_date[] = 			"PĀRBAUDES DATUMS:";
const char lat_prt_test_report[] = 			"PĀRBAUDES ATSKAITE";
#endif
#if LIMBA_ROMANA_en
// LCD
const char lim_language[] = "LIMBA ROMANA";
const char lim_battery[]=				"BATERIE";
const char lim_start_stop_test[]=		"TEST START-STOP";
const char lim_battery_test[]=			"TESTARE BATERIE";
const char lim_system_test[]=			"TESTARE SISTEM";
const char lim_ir_test[]=				"TEST IR.";
const char lim_v_a_meter[]=				"V./A. METRU";
const char lim_setting[]=				"SETARE";
const char lim_test_in_vehicle[]=		"TEST ÎN VEHICUL";
const char lim_direct_connect[]=		"CONECTARE DIRECTĂ";
const char lim_jump_start_post[]=		"BORNĂ DE PORNIRE";
const char lim_brand[]=					"MARCĂ:";
const char lim_model[]=					"MODEL:";
const char lim_version[]=				"VERSIUNE";
const char lim_test_result[]=			"REZULTAT TEST";
const char lim_voltage[]=				"TENSIUNE:";
const char lim_good_n_pass[]=			"STARE BUNĂ, TEST REUȘIT";
const char lim_good_n_recharge[]=		"STARE BUNĂ, REÎNCĂRCAȚI";
const char lim_bad_n_replace[]=			"DEFECTĂ, ÎNLOCUIȚI";
const char lim_caution[]=				"ATENȚIE";
const char lim_recharge_n_retest[]=		"REÎNCĂRCAȚI ȘI RETESTAȚI";
const char lim_bad_cell_replace[]=		"CELULĂ DEFECTĂ, ÎNLOCUIȚI";
const char lim_battery_type[]=			"TIP BATERIE";
const char lim_flooded[]=				"UMEDĂ";
const char lim_agmf[]=					"AGM PLĂCI PLATE";
const char lim_agms[]=					"AGM SPIRALĂ";
const char lim_select_rating[]=			"SELECTARE INDICATIV";
const char lim_set_capacity[]=			"SETARE CAPACITATE";
const char lim_back_light[]=			"RETROILUMINARE LCD";
const char lim_language_select[]=		"SELECTARE LIMBĂ";
const char lim_clock[]=					"CEAS";
const char lim_clear_memory[]=			"GOLIRE MEMORIE";
const char lim_information[]=			"INFORMAȚII";
const char lim_time_zone[]=				"FUS ORAR";
const char lim_test_counter[]=			"CONTOR TEST";
const char lim_abort[]=					"ABANDON";
const char lim_directly_connect_to_battery[]=	"CONECTARE DIRECTĂ LA BATERIE";
const char lim_insert_vin[]=			"INTRODUCEȚI MANUAL CODUL VIN?";
const char lim_test_not_suitable[]=		"TEST NEPOTRIVIT PT. GARANȚIE; CONTINUAȚI?";
const char lim_yes[]=					"DA";
const char lim_no[]=					"NU";
const char lim_print_result[]=			"IMPRIMARE REZULTAT";
const char lim_print_successful[]=		"IMPRIMARE REALIZATĂ, IMPRIMAȚI DIN NOU?";
const char lim_check_clamp[]=			"VERIFICARE CLEMĂ";
const char lim_insert_change_aa[]=		"INTRODUCEȚI SAU SCHIMBAȚI BATERIILE AA";
const char lim_load_error[]=			"EROARE LA ÎNCĂRCARE";
const char lim_voltage_high[]=			"TENSIUNE MARE";
const char lim_voltage_unstable[]=		"TENSIUNE INSTABILĂ\nAȘTEPTARE STABILIZARE";
const char lim_is_it_6v[]=				"ESTE O BATERIE DE 6 V?";
const char lim_is_battery_charge[]=		"ESTE ÎNCĂRCATĂ BATERIA?";
const char lim_battery_in_vehicle[]=	"BATERIA ESTE ÎN VEHICUL?";
const char lim_ss_test[]=				"TEST SS.";
const char lim_bat_test[]=				"TEST BAT.";
const char lim_sys_test[]=				"TEST SIS.";
const char lim_ir_t[] =					"TEST IR.";
const char lim_print[]=					"IMPRIMARE";
const char lim_print_q[]=				"IMPRIMAȚI?";
const char lim_erase[]=					"ȘTERGEȚI?";
const char lim_memory_erase[]=			"ȘTERGEȚI MEMORIA?";
const char lim_on[] =					"PORNIT";
const char lim_off[] =					"OPRIT";
const char lim_date[]=					"DATA:";
const char lim_time[]=					"ORA:";
const char lim_invalid_vin[]=			"VIN INCORECT, REZULTAT NEPOTRIVIT PT. GARANȚIE";
const char lim_input_vin[]=				"INTRODUCERE VIN";
const char lim_enter_vin[]=				"INTRODUCEȚI CODUL VIN COMPLET";
const char lim_measured[]=				"MĂSURAT:";
const char lim_cranking_voltage[]=		"TENSIUNE DEMARAJ";
const char lim_lowest_voltage[]=		"TENSIUNE MINIMĂ";
const char lim_high[]=					"MARE";
const char lim_normal[]=				"NORMALĂ";
const char lim_low[]=					"MICĂ";
const char lim_no_detect[]=				"NEDETECTATĂ";
const char lim_idle_voltage[]=			"TENSIUNE ALT. RALANTI";
const char lim_sec[]=					"SEC";
const char lim_ripple_detected[]=		"PULSAȚIE DETECTATĂ";
const char lim_ripple_voltage[]=		"TENSIUNE PULSAȚIE";
const char lim_load_voltage[]=			"TENSIUNE ALT. ÎNCĂRCARE";
const char lim_point_to_battery_press_enter[]=	"ÎNDREPTAȚI SPRE BATERIE\nAPĂSAȚI PE ENTER";
const char lim_ota[]=					"UPGRADE PRIN REȚEA";
const char lim_test_code[]=				"COD TEST";
const char lim_connection_status[]=		"STARE CONEXIUNE";
const char lim_scan_vin_code[]=			"SCANAȚI CODUL VIN?";
const char lim_vin_code[]=				"COD VIN";
const char lim_invalid_vin_rescan[]=	"COD VIN INCORECT\nRESCANARE";
const char lim_wifi_power[]=			"STARE WIFI:";
const char lim_wifi_list[]=				"LISTĂ WIFI";
const char lim_downloading[]=			"SE DESCARCĂ...";
const char lim_upgrade_cancel[]=		"ANULAȚI UPGRADE-UL?";
const char lim_upgrading[]=				"SE FACE UPGRADE...";
const char lim_check_version[]=			"VERIFICARE VERSIUNE";
const char lim_refresh_list[] = 		"*REÎMPROSPĂTAȚI LISTA";
const char lim_limited_connection[]=	"CONEXIUNE LIMITATĂ";
const char lim_wifi_connected[]=		"WIFI CONECTAT";
const char lim_connecting[]=			"SE CONECTEAZĂ...";
const char lim_wifi_disconnect[]=		"WIFI DECONECTAT";
const char lim_check_connection[]=		"SE VERIFICĂ CONEXIUNEA...";
const char lim_keep_clamps_connect[]=	"AVERTISMENT: ȚINEȚI CLEMELE CONECTATE LA BATERIE.";
//const char lim_skip[]=				"IGNORARE";
const char lim_registration[] = "ÎNREGISTRARE";
const char lim_scan_qr_code[] = "SCANAȚI COD QR?";
const char lim_registering[] = "ÎNREGISTRARE...";


// printer
const char lim_prt_battery_test[] = 		"==TESTARE BATERIE==";
const char lim_prt_by[] = 					"DE:";
const char lim_prt_charging_test[] = 		"TEST ÎNCĂRCARE";
const char lim_prt_note[] = 				"NOTĂ:";
const char lim_prt_diode_ripple[] =			"PULSAȚIE DIODĂ";
const char lim_prt_ir_test[] = 				"==TEST IR.==";
const char lim_prt_load_off[] = 			"FĂRĂ ÎNCĂRCARE";
const char lim_prt_load_on[] =				"CU ÎNCĂRCARE";
const char lim_prt_rated[] = 				"VALOARE NOMINALĂ:";
const char lim_prt_vin_num[] = 				"NUMĂR VIN:";
const char lim_prt_soc[] =					"STARE ÎNCĂRCARE";
const char lim_prt_soh[] = 					"STARE FUNCȚIONARE";
const char lim_prt_starter_test[] = 		"TEST DEMARAJ";
const char lim_prt_start_stop[] = 			"==TESTARE SISTEM START-STOP==";
const char lim_prt_temp[] = 				"TEMPERATURĂ:";
const char lim_prt_test_date[] = 			"DATĂ TEST:";
const char lim_prt_test_report[] = 			"RAPORT TEST";
#endif
#if PUSSIJI_en
// LCD
const char pus_language[] = "PUŠŠÍJI";
const char pus_battery[]=				"АККУМУЛЯТОР";
const char pus_start_stop_test[]=		"ТЕСТ СТАРТ-СТОП";
const char pus_battery_test[]=			"ТЕСТ АККУМУЛЯТ.";
const char pus_system_test[]=			"СИСТЕМНЫЙ ТЕСТ";
const char pus_ir_test[]=				"ВС ТЕСТ";
const char pus_v_a_meter[]=				"BOЛЬT-AMПEP METP";
const char pus_setting[]=				"НАСТРОЙКА";
const char pus_test_in_vehicle[]=		"ТЕСТ В АВТОМОБИЛЕ";
const char pus_direct_connect[]=		"ПРЯМОЕ ПОДКЛЮЧЕНИЕ";
const char pus_jump_start_post[]=		"ЗАПУСК ОТ ВНЕШНЕГО ИСТОЧНИКА";
const char pus_brand[]=					"МАРКА:";
const char pus_model[]=					"МОДЕЛЬ:";
const char pus_version[]=				"ВЕРСИЯ";
const char pus_test_result[]=			"РЕЗУЛЬТАТ ТЕСТА";
const char pus_voltage[]=				"НАПРЯЖЕНИЕ:";
const char pus_good_n_pass[]=			"ХОРОШЕЕ, ПРОЙДЕНО";
const char pus_good_n_recharge[]=		"ХОРОШЕЕ, ПОДЗАРЯДКА";
const char pus_bad_n_replace[]=			"ПЛОХОЕ, ЗАМЕНА";
const char pus_caution[]=				"ВНИМАНИЕ!";
const char pus_recharge_n_retest[]=		"ПОДЗАРЯДКА И ПОВТОРНЫЙ ТЕСТ";
const char pus_bad_cell_replace[]=		"ПЛОХАЯ ЯЧЕЙКА, ЗАМЕНА";
const char pus_battery_type[]=			"ТИП АККУМУЛЯТОРА";
const char pus_flooded[]=				"ЭЛЕКТРОЛИТНЫЙ";
const char pus_agmf[]=					"ГЕЛЕВЫЙ ПЛОСКИЙ";
const char pus_agms[]=					"ГЕЛЕВЫЙ СПИРАЛЬНЫЙ";
const char pus_select_rating[]=			"ВЫБОР НОМИНАЛА";
const char pus_set_capacity[]=			"ЗАДАТЬ ЕМКОСТЬ";
const char pus_back_light[]=			"ПОДСВЕТКА ЖК-ЭКРАНА";
const char pus_language_select[]=		"ВЫБОР ЯЗЫКА";
const char pus_clock[]=					"ЧАСЫ";
const char pus_clear_memory[]=			"ОЧИСТКА ПАМЯТИ";
const char pus_information[]=			"ИНФОРМАЦИЯ";
const char pus_time_zone[]=				"ЧАСОВОЙ ПОЯС";
const char pus_test_counter[]=			"СЧЕТЧИК ТЕСТОВ";
const char pus_abort[]=					"ПРЕРВАТЬ";
const char pus_directly_connect_to_battery[]=	"ПРЯМОЕ ПОДКЛЮЧЕНИЕ К АККУМУЛЯТОРУ";
const char pus_insert_vin[]=			"ВСТАВИТЬ VIN-КОД ВРУЧНУЮ?";
const char pus_test_not_suitable[]=		"ТЕСТ НЕ СООТВЕТСТВУЕТ ГАРАНТИИ, ПРОДОЛЖИТЬ?";
const char pus_yes[]=					"ДА";
const char pus_no[]=					"НЕТ";
const char pus_print_result[]=			"НАПЕЧАТАТЬ РЕЗУЛЬТАТ";
const char pus_print_successful[]=		"ПЕЧАТЬ ВЫПОЛНЕНА УСПЕШНО, НАПЕЧАТАТЬ ЕЩЕ РАЗ?";
const char pus_check_clamp[]=			"ПРОВЕРИТЬ ЛАМПУ";
const char pus_insert_change_aa[]=		"ВСТАВЬТЕ ИЛИ ЗАМЕНИТЕ БАТАРЕЙКИ АА";
const char pus_load_error[]=			"ОШИБКА ЗАГРУЗКИ";
const char pus_voltage_high[]=			"ВЫСОКОЕ НАПРЯЖЕНИЕ";
const char pus_voltage_unstable[]=		"НЕСТАБИЛЬНОЕ НАПРЯЖЕНИЕ\nОЖИДАНИЕ СТАБИЛЬНОСТИ";
const char pus_is_it_6v[]=				"ЭТО АККУМУЛЯТОР 6В";
const char pus_is_battery_charge[]=		"АККУМУЛЯТОР ЗАРЯЖЕН";
const char pus_battery_in_vehicle[]=	"АККУМУЛЯТОР В АВТОМОБИЛЕ";
const char pus_ss_test[]=				"СС. ТЕСТ";
const char pus_bat_test[]=				"АКК. ТЕСТ";
const char pus_sys_test[]=				"СИС. ТЕСТ";
const char pus_ir_t[] =					"ВС ТЕСТ";
const char pus_print[]=					"ПЕЧАТЬ";
const char pus_print_q[]=				"ПЕЧАТЬ?";
const char pus_erase[]=					"СТЕРЕТЬ?";
const char pus_memory_erase[]=			"СТЕРЕТЬ ПАМЯТЬ?";
const char pus_on[] =					"ВКЛ";
const char pus_off[] =					"ВЫКЛ";
const char pus_date[]=					"ДАТА:";
const char pus_time[]=					"ВРЕМЯ:";
const char pus_invalid_vin[]=			"НЕДОПУСТИМЫЙ VIN, РЕЗУЛЬТАТ НЕ СООТВЕТСТВУЕТ ГАРАНТИИ";
const char pus_input_vin[]=				"ВВОД VIN";
const char pus_enter_vin[]=				"ВВЕДИТЕ ПОЛНЫЙ VIN";
const char pus_measured[]=				"ИЗМЕРЕНО:";
const char pus_cranking_voltage[]=		"ПУСКОВОЕ НАПРЯЖ.";//TODO
const char pus_lowest_voltage[]=		"САМОЕ НИЗКОЕ НАПРЯЖЕНИЕ";
const char pus_high[]=					"ВЫСОКОЕ";
const char pus_normal[]=				"ОБЫЧНОЕ";
const char pus_low[]=					"НИЗКОЕ";
const char pus_no_detect[]=				"НЕ ОБНАРУЖЕНО";
const char pus_idle_voltage[]=			"НАПР ХОЛ ЗАПУСКА";//TODO
const char pus_sec[]=					"ВТОР";
const char pus_ripple_detected[]=		"ОБНАРУЖЕНО ПУЛЬСИРОВАНИЕ";
const char pus_ripple_voltage[]=		"ПУЛЬС. НАПР.";
const char pus_load_voltage[]=			"АЛЬТ. НАПРЯЖЕНИЕ НАГРУЗКИ";
const char pus_point_to_battery_press_enter[]=	"УКАЗАНИЕ НА АККУМУЛЯТОР\nНАЖМИТЕ ВВОД";
const char pus_ota[]=					"ОБНОВЛЕНИЕ ПО БЕСПРОВОДНОЙ СВЯЗИ";
const char pus_test_code[]=				"КОД ТЕСТА";
const char pus_connection_status[]=		"СОСТОЯНИЕ ПОДКЛЮЧЕНИЯ";
const char pus_scan_vin_code[]=			"ОТСКАНИРОВАТЬ VIN-КОД?";
const char pus_vin_code[]=				"VIN-КОД";
const char pus_invalid_vin_rescan[]=	"НЕДОПУСТИМЫЙ VIN-КОД\nПОВТОРНОЕ СКАНИРОВАНИЕ";
const char pus_wifi_power[]=			"ПИТАНИЕ WIFI:";
const char pus_wifi_list[]=				"СПИСОК WIFI";
const char pus_downloading[]=			"ЗАГРУЗКА...";
const char pus_upgrade_cancel[]=		"ОТМЕНИТЬ ОБНОВЛЕНИЕ?";
const char pus_upgrading[]=				"ОБНОВЛЕНИЕ...";
const char pus_check_version[]=			"ПРОВЕРИТЬ ВЕРСИЮ";
const char pus_refresh_list[] = 		"*ОБНОВИТЬ СПИСОК";
const char pus_limited_connection[]=	"ОГРАНИЧЕННОЕ ПОДКЛЮЧЕНИЕ";
const char pus_wifi_connected[]=		"СЕТЬ WIFI ПОДКЛЮЧЕНА";
const char pus_connecting[]=			"ПОДКЛЮЧЕНИЕ...";
const char pus_wifi_disconnect[]=		"СЕТЬ WIFI ОТКЛЮЧЕНА";
const char pus_check_connection[]=		"ПРОВЕРКА ПОДКЛЮЧЕНИЯ...";
const char pus_keep_clamps_connect[]=	"ПРЕДУПРЕЖДЕНИЕ! ОСТАВЬТЕ КЛЕММЫ НА АККУМУЛЯТОРЕ.";
//const char pus_skip[]=				"ПРОПУСТИТЬ";
const char pus_registration[] = "РЕГИСТРАЦИЯ";
const char pus_scan_qr_code[] = "СКАНИР. QR-КОД?";
const char pus_registering[] = "РЕГИСТРАЦИЯ...";


// printer
const char pus_prt_battery_test[] = 		"==ТЕСТ АККУМУЛЯТОРА==";
const char pus_prt_by[] = 					"От:";
const char pus_prt_charging_test[] = 		"ТЕСТ ПОДЗАРЯДКИ";
const char pus_prt_note[] = 				"ПРИМЕЧАНИЕ:";
const char pus_prt_diode_ripple[] =			"ПУЛЬС. ДИОД";
const char pus_prt_ir_test[] = 				"==ВС ТЕСТ==";
const char pus_prt_load_off[] = 			"БЕЗ НАГРУЗКИ";
const char pus_prt_load_on[] =				"С НАГРУЗКОЙ";
const char pus_prt_rated[] = 				"НОМИНАЛ:";
const char pus_prt_vin_num[] = 				"VIN-НОМЕР:";
const char pus_prt_soc[] =					"СОСТОЯНИЕ ПОДЗАРЯДКИ";
const char pus_prt_soh[] = 					"СОСТОЯНИЕ РАБОТОСПОСОБНОСТИ";
const char pus_prt_starter_test[] = 		"ТЕСТ СТАРТЕРА";
const char pus_prt_start_stop[] = 			"==ТЕСТ НА ЗАПУСК/ОСТАНОВКУ==";
const char pus_prt_temp[] = 				"ТЕМПЕРАТУРА:";
const char pus_prt_test_date[] = 			"ДАТА ТЕСТА:";
const char pus_prt_test_report[] = 			"ОТЧЕТ ПО ТЕСТУ";
#endif
#if EAAHNIKA_en
// LCD
const char eaa_language[] = "ΕΛΛΗΝΙΚΑ";
const char eaa_battery[]=				"METPHΣH";
const char eaa_start_stop_test[]=		"ΜΠΑΤ. START-STOP";
const char eaa_battery_test[]=			"ΜΕΤΡΗΣΗ ΜΠΑΤ.";
const char eaa_system_test[]=			"ΕΛΕΓΧΟΣ ΣΥΣΤ.";
const char eaa_ir_test[]=				"ΜΕΤΡΗΣΗ IR.";
const char eaa_v_a_meter[]=				"ΜΕΤΡΗΣΗ V./A.";
const char eaa_setting[]=				"ΡΥΘΜΙΣΗ";
const char eaa_test_in_vehicle[]=		"ΜΕΤΡΗΣΗ ΣΤΟ ΟΧΗΜΑ";
const char eaa_direct_connect[]=		"ΑΜΕΣΗ ΣΥΝΔΕΣΗ";
const char eaa_jump_start_post[]=		"ΘΕΣΗ ΕΚΚΙΝΗΣΗΣ ΜΕ ΚΑΛΩΔΙΑ";
const char eaa_brand[]=					"ΜΑΡΚΑ:";
const char eaa_model[]=					"ΜΟΝΤΕΛΟ:";
const char eaa_version[]=				"ΕΚΔΟΣΗ";
const char eaa_test_result[]=			"ΑΠΟΤΕΛΕΣΜΑ ΜΕΤΡΗΣΗΣ";
const char eaa_voltage[]=				"ΤΑΣΗ:";
const char eaa_good_n_pass[]=			"ΚΑΛΗ ΚΑΙ ΕΠΑΡΚΗΣ";
const char eaa_good_n_recharge[]=		"ΚΑΛΗ ΚΑΙ ΕΠΑΝΑΦΟΡΤΙΣΗ";
const char eaa_bad_n_replace[]=			"ΚΑΚΗ ΚΑΙ ΑΝΤΙΚΑΤΑΣΤΑΣΗ";
const char eaa_caution[]=				"ΠΡΟΣΟΧΗ";
const char eaa_recharge_n_retest[]=		"ΕΠΑΝΑΛΗΨΗ ΦΟΡΤΙΣΗΣ ΚΑΙ ΜΕΤΡΗΣΗΣ";
const char eaa_bad_cell_replace[]=		"ΚΑΚΗ - ΑΝΤΙΚΑΤΑΣΤΑΣΗ ΣΤΟΙΧΕΙΟΥ";
const char eaa_battery_type[]=			"ΤΥΠΟΣ ΜΠΑΤΑΡΙΑΣ";
const char eaa_flooded[]=				"FLOODED";
const char eaa_agmf[]=					"AGM FLAT PLATE";
const char eaa_agms[]=					"AGM SPIRAL";
const char eaa_select_rating[]=			"ΕΠΙΛΟΓΗ ΠΙΣΤΟΠΟΙΗΣΗΣ";
const char eaa_set_capacity[]=			"ΟΡΙΣΜΟΣ ΧΩΡΗΤΙΚΟΤΗΤΑΣ";
const char eaa_back_light[]=			"ΟΠΙΣΘΙΟΣ ΦΩΤΙΣΜΟΣ LCD";
const char eaa_language_select[]=		"ΕΠΙΛΟΓΗ ΓΛΩΣΣΑΣ";
const char eaa_clock[]=					"ΡΟΛΟΪ";
const char eaa_clear_memory[]=			"ΕΚΚΑΘΑΡΙΣΗ ΜΝΗΜΗΣ";
const char eaa_information[]=			"ΠΛΗΡΟΦΟΡΙΕΣ";
const char eaa_time_zone[]=				"ΖΩΝΗ ΩΡΑΣ";
const char eaa_test_counter[]=			"KATAΓPAΦEAΣ METPHΣHΣ";
const char eaa_abort[]=					"ΔΙΑΚΟΠΗ";
const char eaa_directly_connect_to_battery[]=	"ΑΜΕΣΗ ΣΥΝΔΕΣΗ ΣΤΗΝ ΜΠΑΤΑΡΙΑ";
const char eaa_insert_vin[]=			"ΧΕΙΡΟΚΙΝΗΤΗ ΕΙΣΑΓΩΓΗ ΚΩΔΙΚΟΥ VIN;";
const char eaa_test_not_suitable[]=		"H METPHΣH ΔEN ENΔEΙKNYTAΙ ΓΙA EΓΓYHΣH. ΘEΛETE NA ΣYNEXΙΣETE;";
const char eaa_yes[]=					"ΝΑΙ";
const char eaa_no[]=					"ΟΧΙ";
const char eaa_print_result[]=			"ΕΚΤΥΠΩΣΗ ΑΠΟΤΕΛΕΣΜΑΤΟΣ";
const char eaa_print_successful[]=		"Η ΕΚΤΥΠΩΣΗ ΟΛΟΚΛΗΡΩΘΗΚΕ ΜΕ ΕΠΙΤΥΧΙΑ. ΕΠΑΝΑΛΗΨΗ ΕΚΤΥΠΩΣΗΣ;";
const char eaa_check_clamp[]=			"ΕΛΕΓΧΟΣ ΣΦΙΓΚΤΗΡΑ";
const char eaa_insert_change_aa[]=		"ΤΟΠΟΘΕΤΗΣΗ Ή ΑΛΛΑΓΗ ΜΠΑΤΑΡΙΩΝ AA";
const char eaa_load_error[]=			"ΣΦΑΛΜΑ ΦΟΡΤΩΣΗΣ";
const char eaa_voltage_high[]=			"ΥΨΗΛΗ ΤΑΣΗ";
const char eaa_voltage_unstable[]=		"ΜΗ ΣΤΑΘΕΡΗ ΤΑΣΗ\nΑΝΑΜΟΝΗ ΓΙΑ ΣΤΑΘΕΡΗ ΤΑΣΗ";
const char eaa_is_it_6v[]=				"ΕΙΝΑΙ ΜΠΑΤΑΡΙΑ 6 V;";
const char eaa_is_battery_charge[]=		"ΕΙΝΑΙ Η ΜΠΑΤΑΡΙΑ ΦΟΡΤΙΣΜΕΝΗ;";
const char eaa_battery_in_vehicle[]=	"ΥΠΑΡΧΕΙ ΜΠΑΤΑΡΙΑ ΣΤΟ ΟΧΗΜΑ";
const char eaa_ss_test[]=				"ΜΕΤΡΗΣΗ SS.";
const char eaa_bat_test[]=				"ΜΕΤΡΗΣΗ ΜΠΑΤ.";
const char eaa_sys_test[]=				"ΕΛΕΓΧΟΣ ΣΥΣΤ.";
const char eaa_ir_t[] =					"ΜΕΤΡΗΣΗ IR.";
const char eaa_print[]=					"ΕΚΤΥΠΩΣΗ";
const char eaa_print_q[]=				"ΕΚΤΥΠΩΣΗ;";
const char eaa_erase[]=					"ΔΙΑΓΡΑΦΗ;";
const char eaa_memory_erase[]=			"ΔΙΑΓΡΑΦΗ ΜΝΗΜΗΣ;";
const char eaa_on[] =					"ΕΝΕΡΓΟΠΟΙΗΘΗΚΕ";
const char eaa_off[] =					"ΑΠΕΝΕΡΓΟΠΟΙΗΘΗΚΕ";
const char eaa_date[]=					"ΗΜΕΡΟΜΗΝΙΑ:";
const char eaa_time[]=					"ΩΡΑ:";
const char eaa_invalid_vin[]=			"ΜΗ ΕΓΚΥΡΟΣ ΚΩΔΙΚΟΣ VIN. ΤΟ ΑΠΟΤΕΛΕΣΜΑ ΔΕΝ ΕΝΔΕΙΚΝΥΤΑΙ ΓΙΑ ΕΓΓΥΗΣΗ";
const char eaa_input_vin[]=				"ΕΙΣΑΓΩΓΗ VIN";
const char eaa_enter_vin[]=				"ΕΙΣΑΓΩΓΗ ΠΛΗΡΟΥΣ ΚΩΔΙΚΟΥ VIN";
const char eaa_measured[]=				"ΜΕΤΡΗΣΗ:";
const char eaa_cranking_voltage[]=		"ΤΑΣΗ ΕΚΚΙΝΗΣΗΣ";
const char eaa_lowest_voltage[]=		"ΧΑΜΗΛΟΤΕΡΗ ΤΑΣΗ";
const char eaa_high[]=					"ΥΨΗΛΗ";
const char eaa_normal[]=				"ΚΑΝΟΝΙΚΗ";
const char eaa_low[]=					"ΧΑΜΗΛΗ";
const char eaa_no_detect[]=				"ΔΕΝ ΑΝΙΧΝΕΥΤΗΚΕ";
const char eaa_idle_voltage[]=			"ΕΝΑΛΛ. ΤΑΣΗ ΡΕΛΑΝΤΙ";
const char eaa_sec[]=					"ΔΕΥΤ.";
const char eaa_ripple_detected[]=		"ΔΙΑΚΥΜΑΝΣΗ ΠΟΥ ΑΝΙΧΝΕΥΤΗΚΕ";
const char eaa_ripple_voltage[]=		"ΤΑΣΗ ΔΙΑΚΥΜΑΝΣΗΣ";
const char eaa_load_voltage[]=			"ΕΝΑΛΛ. ΤΑΣΗ ΦΟΡΤΙΟΥ";
const char eaa_point_to_battery_press_enter[]=	"ΣΤΟΧΕΥΣΤΕ ΤΗΝ ΜΠΑΤΑΡΙΑ\nΠΑΤΗΣΤΕ ENTER";
const char eaa_ota[]=					"ΑΝΑΒΑΘΜΙΣΗ OTA";
const char eaa_test_code[]=				"ΚΩΔΙΚΟΣ ΜΕΤΡΗΣΗΣ";
const char eaa_connection_status[]=		"ΚΑΤΑΣΤΑΣΗ ΣΥΝΔΕΣΗΣ";
const char eaa_scan_vin_code[]=			"ΣΑΡΩΣΗ ΚΩΔΙΚΟΥ VIN;";
const char eaa_vin_code[]=				"ΚΩΔΙΚΟΣ VIN";
const char eaa_invalid_vin_rescan[]=	"ΜΗ ΕΓΚΥΡΟΣ ΚΩΔΙΚΟΣ VIN\nΕΠΑΝΑΛΗΨΗ ΣΑΡΩΣΗΣ";
const char eaa_wifi_power[]=			"WIFI:";
const char eaa_wifi_list[]=				"ΛΙΣΤΑ WIFI";
const char eaa_downloading[]=			"ΛΗΨΗ...";
const char eaa_upgrade_cancel[]=		"ΑΚΥΡΩΣΗ ΑΝΑΒΑΘΜΙΣΗΣ;";
const char eaa_upgrading[]=				"ΑΝΑΒΑΘΜΙΣΗ...";
const char eaa_check_version[]=			"ΕΛΕΓΧΟΣ ΕΚΔΟΣΗΣ";
const char eaa_refresh_list[] = 		"*ΑΝΑΝΕΩΣΗ ΛΙΣΤΑΣ";
const char eaa_limited_connection[]=	"ΠΕΡΙΟΡΙΣΜΕΝΗ ΣΥΝΔΕΣΗ";
const char eaa_wifi_connected[]=		"ΤΟ WIFI ΣΥΝΔΕΘΗΚΕ";
const char eaa_connecting[]=			"ΣΥΝΔΕΣΗ...";
const char eaa_wifi_disconnect[]=		"ΤΟ WIFI ΑΠΟΣΥΝΔΕΘΗΚΕ";
const char eaa_check_connection[]=		"ΕΛΕΓΧΟΣ ΣΥΝΔΕΣΗΣ...";
const char eaa_keep_clamps_connect[]=	"ΠΡΟΕΙΔΟΠΟΙΗΣΗ: ΚΡΑΤΗΣΤΕ ΤΟΥΣ ΣΦΙΓΚΤΗΡΕΣ ΣΥΝΔΕΔΕΜΕΝΟΥΣ ΣΤΗΝ ΜΠΑΤΑΡΙΑ.";
//const char eaa_skip[]=				"ΠΑΡΑΛΕΙΨΗ";
const char eaa_registration[] = "ΕΓΓΡΑΦΗ";
const char eaa_scan_qr_code[] = "ΣΑΡΩΣΗ ΚΩΔ. QR;";
const char eaa_registering[] = "ΕΓΓΡΑΦΗ...";


// printer
const char eaa_prt_battery_test[] = 		"==ΜΕΤΡΗΣΗ ΜΠΑΤΑΡΙΑΣ==";
const char eaa_prt_by[] = 					"ΑΠΟ:";
const char eaa_prt_charging_test[] = 		"ΕΛΕΓΧΟΣ ΦΟΡΤΙΣΗΣ";
const char eaa_prt_note[] = 				"ΣΗΜΕΙΩΣΗ:";
const char eaa_prt_diode_ripple[] =			"ΔΙΑΚΥΜΑΝΣΗ ΔΙΟΔΟΥ";
const char eaa_prt_ir_test[] = 				"==ΜΕΤΡΗΣΗ IR.==";
const char eaa_prt_load_off[] = 			"ΦΟΡΤΙΟ ΑΝΕΝΕΡΓΟ";
const char eaa_prt_load_on[] =				"ΦΟΡΤΙΟ ΕΝΕΡΓΟ";
const char eaa_prt_rated[] = 				"ΠΙΣΤΟΠΟΙΗΣΗ:";
const char eaa_prt_vin_num[] = 				"ΑΡΙΘΜΟΣ VIN:";
const char eaa_prt_soc[] =					"ΚΑΤΑΣΤΑΣΗ ΦΟΡΤΙΣΗΣ";
const char eaa_prt_soh[] = 					"ΚΑΤΑΣΤΑΣΗ ΕΥΡΥΘΜΗΣ ΛΕΙΤΟΥΡΓΙΑΣ";
const char eaa_prt_starter_test[] = 		"ΔΟΚΙΜΗ ΕΚΚΙΝΗΤΗ";
const char eaa_prt_start_stop[] = 			"==ΜΕΤΡΗΣΗ START-STOP==";
const char eaa_prt_temp[] = 				"ΘΕΡΜΟΚΡΑΣΙΑ:";
const char eaa_prt_test_date[] = 			"ΗΜΕΡΟΜΗΝΙΑ ΜΕΤΡΗΣΗΣ:";
const char eaa_prt_test_report[] = 			"ΑΝΑΦΟΡΑ ΜΕΤΡΗΣΗΣ";
#endif

Lang_abstract_t language_list[] = {
#if ENGLISH_en
	{en_language, en_US},
#endif
#if ITALIAN_en
	{it_language, it_IT},
#endif
#if DEUTSCH_en
	{de_language, de_DE},
#endif
#if ESPANOL_en
	{es_language, es_ES},	
#endif
#if FRANCAIS_en
	{fr_language, fr_FR},
#endif
#if PORTUGUES_en
	{pt_language, pt_PT},
#endif
#if NEDERLANDS_en
	{ned_language,nd_ND},
#endif
#if DANSK_en
	{dan_language,dk_DK},
#endif
#if NORSK_en
	{nor_language,nl_NW},
#endif
#if SOOMI_en
	{soo_language,sm_FN},
#endif
#if SVENSKA_en
	{sve_language,sv_SW},
#endif
#if CESTINA_en
	{ces_language,cs_CZ},
#endif
#if LIETUVIU_K_en
	{lie_language,lt_LT},
#endif
#if POLSKI_en
	{pol_language,pl_PL},
#endif
#if SLOVENCINA_en
	{slo_language,sl_SL},
#endif
#if SLOVENSCINA_en
	{sls_language,sls_SLJ},
#endif
#if TURKU_en
	{tur_language,tk_TR},
#endif
#if EESTI_en
	{ees_language,ee_EE},
#endif
#if LATVIESU_en
	{lat_language,la_LA},
#endif
#if LIMBA_ROMANA_en
	{lim_language,lm_ML},
#endif
#if PUSSIJI_en
	{pus_language,pu_RS},
#endif
#if EAAHNIKA_en
	{eaa_language,ea_GK},
#endif
};

const char *start_stop_test_list[] = {
#if ENGLISH_en
en_start_stop_test,
#endif
#if FRANCAIS_en
fr_start_stop_test,
#endif
#if DEUTSCH_en
de_start_stop_test,
#endif
#if ESPANOL_en
es_start_stop_test,
#endif
#if ITALIAN_en
it_start_stop_test,
#endif
#if PORTUGUES_en
pt_start_stop_test,
#endif
#if NEDERLANDS_en
ned_start_stop_test,
#endif
#if DANSK_en
dan_start_stop_test,
#endif
#if NORSK_en
nor_start_stop_test,
#endif
#if SOOMI_en
soo_start_stop_test,
#endif
#if SVENSKA_en
sve_start_stop_test,
#endif
#if CESTINA_en
ces_start_stop_test,
#endif
#if LIETUVIU_K_en
lie_start_stop_test,
#endif
#if POLSKI_en
pol_start_stop_test,
#endif
#if SLOVENCINA_en
slo_start_stop_test,
#endif
#if SLOVENSCINA_en
sls_start_stop_test,
#endif
#if TURKU_en
tur_start_stop_test,
#endif
#if EESTI_en
ees_start_stop_test,
#endif
#if LATVIESU_en
lat_start_stop_test,
#endif
#if LIMBA_ROMANA_en
lim_start_stop_test,
#endif
#if PUSSIJI_en
pus_start_stop_test,
#endif
#if EAAHNIKA_en
eaa_start_stop_test,
#endif
};

const char *battery_test_list[] = {
#if ENGLISH_en
en_battery_test,
#endif
#if FRANCAIS_en
fr_battery_test,
#endif
#if DEUTSCH_en
de_battery_test,
#endif
#if ESPANOL_en
es_battery_test,
#endif
#if ITALIAN_en
it_battery_test,
#endif
#if PORTUGUES_en
pt_battery_test,
#endif
#if NEDERLANDS_en
ned_battery_test,
#endif
#if DANSK_en
dan_battery_test,
#endif
#if NORSK_en
nor_battery_test,
#endif
#if SOOMI_en
soo_battery_test,
#endif
#if SVENSKA_en
sve_battery_test,
#endif
#if CESTINA_en
ces_battery_test,
#endif
#if LIETUVIU_K_en
lie_battery_test,
#endif
#if POLSKI_en
pol_battery_test,
#endif
#if SLOVENCINA_en
slo_battery_test,
#endif
#if SLOVENSCINA_en
sls_battery_test,
#endif
#if TURKU_en
tur_battery_test,
#endif
#if EESTI_en
ees_battery_test,
#endif
#if LATVIESU_en
lat_battery_test,
#endif
#if LIMBA_ROMANA_en
lim_battery_test,
#endif
#if PUSSIJI_en
pus_battery_test,
#endif
#if EAAHNIKA_en
eaa_battery_test,
#endif
};
const char *system_test_list[] = {
#if ENGLISH_en
en_system_test,
#endif
#if FRANCAIS_en
fr_system_test,
#endif
#if DEUTSCH_en
de_system_test,
#endif
#if ESPANOL_en
es_system_test,
#endif
#if ITALIAN_en
it_system_test,
#endif
#if PORTUGUES_en
pt_system_test,
#endif
#if NEDERLANDS_en
ned_system_test,
#endif
#if DANSK_en
dan_system_test,
#endif
#if NORSK_en
nor_system_test,
#endif
#if SOOMI_en
soo_system_test,
#endif
#if SVENSKA_en
sve_system_test,
#endif
#if CESTINA_en
ces_system_test,
#endif
#if LIETUVIU_K_en
lie_system_test,
#endif
#if POLSKI_en
pol_system_test,
#endif
#if SLOVENCINA_en
slo_system_test,
#endif
#if SLOVENSCINA_en
sls_system_test,
#endif
#if TURKU_en
tur_system_test,
#endif
#if EESTI_en
ees_system_test,
#endif
#if LATVIESU_en
lat_system_test,
#endif
#if LIMBA_ROMANA_en
lim_system_test,
#endif
#if PUSSIJI_en
pus_system_test,
#endif
#if EAAHNIKA_en
eaa_system_test,
#endif
};

const char *ir_test_list[] = {
#if ENGLISH_en
en_ir_test,
#endif
#if FRANCAIS_en
fr_ir_test,
#endif
#if DEUTSCH_en
de_ir_test,
#endif
#if ESPANOL_en
es_ir_test,
#endif
#if ITALIAN_en
it_ir_test,
#endif
#if PORTUGUES_en
pt_ir_test,
#endif
#if NEDERLANDS_en
ned_ir_test,
#endif
#if DANSK_en
dan_ir_test,
#endif
#if NORSK_en
nor_ir_test,
#endif
#if SOOMI_en
soo_ir_test,
#endif
#if SVENSKA_en
sve_ir_test,
#endif
#if CESTINA_en
ces_ir_test,
#endif
#if LIETUVIU_K_en
lie_ir_test,
#endif
#if POLSKI_en
pol_ir_test,
#endif
#if SLOVENCINA_en
slo_ir_test,
#endif
#if SLOVENSCINA_en
sls_ir_test,
#endif
#if TURKU_en
tur_ir_test,
#endif
#if EESTI_en
ees_ir_test,
#endif
#if LATVIESU_en
lat_ir_test,
#endif
#if LIMBA_ROMANA_en
lim_ir_test,
#endif
#if PUSSIJI_en
pus_ir_test,
#endif
#if EAAHNIKA_en
eaa_ir_test,
#endif
};
const char *v_a_meter_list[] = {
#if ENGLISH_en
en_v_a_meter,
#endif
#if FRANCAIS_en
fr_v_a_meter,
#endif
#if DEUTSCH_en
de_v_a_meter,
#endif
#if ESPANOL_en
es_v_a_meter,
#endif
#if ITALIAN_en
it_v_a_meter,
#endif
#if PORTUGUES_en
pt_v_a_meter,
#endif
#if NEDERLANDS_en
ned_v_a_meter,
#endif
#if DANSK_en
dan_v_a_meter,
#endif
#if NORSK_en
nor_v_a_meter,
#endif
#if SOOMI_en
soo_v_a_meter,
#endif
#if SVENSKA_en
sve_v_a_meter,
#endif
#if CESTINA_en
ces_v_a_meter,
#endif
#if LIETUVIU_K_en
lie_v_a_meter,
#endif
#if POLSKI_en
pol_v_a_meter,
#endif
#if SLOVENCINA_en
slo_v_a_meter,
#endif
#if SLOVENSCINA_en
sls_v_a_meter,
#endif
#if TURKU_en
tur_v_a_meter,
#endif
#if EESTI_en
ees_v_a_meter,
#endif
#if LATVIESU_en
lat_v_a_meter,
#endif
#if LIMBA_ROMANA_en
lim_v_a_meter,
#endif
#if PUSSIJI_en
pus_v_a_meter,
#endif
#if EAAHNIKA_en
eaa_v_a_meter,
#endif
};
const char *setting_list[] = {
#if ENGLISH_en
en_setting,
#endif
#if FRANCAIS_en
fr_setting,
#endif
#if DEUTSCH_en
de_setting,
#endif
#if ESPANOL_en
es_setting,
#endif
#if ITALIAN_en
it_setting,
#endif
#if PORTUGUES_en
pt_setting,
#endif
#if NEDERLANDS_en
ned_setting,
#endif
#if DANSK_en
dan_setting,
#endif
#if NORSK_en
nor_setting,
#endif
#if SOOMI_en
soo_setting,
#endif
#if SVENSKA_en
sve_setting,
#endif
#if CESTINA_en
ces_setting,
#endif
#if LIETUVIU_K_en
lie_setting,
#endif
#if POLSKI_en
pol_setting,
#endif
#if SLOVENCINA_en
slo_setting,
#endif
#if SLOVENSCINA_en
sls_setting,
#endif
#if TURKU_en
tur_setting,
#endif
#if EESTI_en
ees_setting,
#endif
#if LATVIESU_en
lat_setting,
#endif
#if LIMBA_ROMANA_en
lim_setting,
#endif
#if PUSSIJI_en
pus_setting,
#endif
#if EAAHNIKA_en
eaa_setting,
#endif
};
const char *test_in_vehicle_list[] = {
#if ENGLISH_en
en_test_in_vehicle,
#endif
#if FRANCAIS_en
fr_test_in_vehicle,
#endif
#if DEUTSCH_en
de_test_in_vehicle,
#endif
#if ESPANOL_en
es_test_in_vehicle,
#endif
#if ITALIAN_en
it_test_in_vehicle,
#endif
#if PORTUGUES_en
pt_test_in_vehicle,
#endif
#if NEDERLANDS_en
ned_test_in_vehicle,
#endif
#if DANSK_en
dan_test_in_vehicle,
#endif
#if NORSK_en
nor_test_in_vehicle,
#endif
#if SOOMI_en
soo_test_in_vehicle,
#endif
#if SVENSKA_en
sve_test_in_vehicle,
#endif
#if CESTINA_en
ces_test_in_vehicle,
#endif
#if LIETUVIU_K_en
lie_test_in_vehicle,
#endif
#if POLSKI_en
pol_test_in_vehicle,
#endif
#if SLOVENCINA_en
slo_test_in_vehicle,
#endif
#if SLOVENSCINA_en
sls_test_in_vehicle,
#endif
#if TURKU_en
tur_test_in_vehicle,
#endif
#if EESTI_en
ees_test_in_vehicle,
#endif
#if LATVIESU_en
lat_test_in_vehicle,
#endif
#if LIMBA_ROMANA_en
lim_test_in_vehicle,
#endif
#if PUSSIJI_en
pus_test_in_vehicle,
#endif
#if EAAHNIKA_en
eaa_test_in_vehicle,
#endif
};
const char *direct_connect_list[] = {
#if ENGLISH_en
en_direct_connect,
#endif
#if FRANCAIS_en
fr_direct_connect,
#endif
#if DEUTSCH_en
de_direct_connect,
#endif
#if ESPANOL_en
es_direct_connect,
#endif
#if ITALIAN_en
it_direct_connect,
#endif
#if PORTUGUES_en
pt_direct_connect,
#endif
#if NEDERLANDS_en
ned_direct_connect,
#endif
#if DANSK_en
dan_direct_connect,
#endif
#if NORSK_en
nor_direct_connect,
#endif
#if SOOMI_en
soo_direct_connect,
#endif
#if SVENSKA_en
sve_direct_connect,
#endif
#if CESTINA_en
ces_direct_connect,
#endif
#if LIETUVIU_K_en
lie_direct_connect,
#endif
#if POLSKI_en
pol_direct_connect,
#endif
#if SLOVENCINA_en
slo_direct_connect,
#endif
#if SLOVENSCINA_en
sls_direct_connect,
#endif
#if TURKU_en
tur_direct_connect,
#endif
#if EESTI_en
ees_direct_connect,
#endif
#if LATVIESU_en
lat_direct_connect,
#endif
#if LIMBA_ROMANA_en
lim_direct_connect,
#endif
#if PUSSIJI_en
pus_direct_connect,
#endif
#if EAAHNIKA_en
eaa_direct_connect,
#endif
};
const char *jump_start_post_list[] = {
#if ENGLISH_en
en_jump_start_post,
#endif
#if FRANCAIS_en
fr_jump_start_post,
#endif
#if DEUTSCH_en
de_jump_start_post,
#endif
#if ESPANOL_en
es_jump_start_post,
#endif
#if ITALIAN_en
it_jump_start_post,
#endif
#if PORTUGUES_en
pt_jump_start_post,
#endif
#if NEDERLANDS_en
ned_jump_start_post,
#endif
#if DANSK_en
dan_jump_start_post,
#endif
#if NORSK_en
nor_jump_start_post,
#endif
#if SOOMI_en
soo_jump_start_post,
#endif
#if SVENSKA_en
sve_jump_start_post,
#endif
#if CESTINA_en
ces_jump_start_post,
#endif
#if LIETUVIU_K_en
lie_jump_start_post,
#endif
#if POLSKI_en
pol_jump_start_post,
#endif
#if SLOVENCINA_en
slo_jump_start_post,
#endif
#if SLOVENSCINA_en
sls_jump_start_post,
#endif
#if TURKU_en
tur_jump_start_post,
#endif
#if EESTI_en
ees_jump_start_post,
#endif
#if LATVIESU_en
lat_jump_start_post,
#endif
#if LIMBA_ROMANA_en
lim_jump_start_post,
#endif
#if PUSSIJI_en
pus_jump_start_post,
#endif
#if EAAHNIKA_en
eaa_jump_start_post,
#endif
};
const char *brand_list[] = {
#if ENGLISH_en
en_brand,
#endif
#if FRANCAIS_en
fr_brand,
#endif
#if DEUTSCH_en
de_brand,
#endif
#if ESPANOL_en
es_brand,
#endif
#if ITALIAN_en
it_brand,
#endif
#if PORTUGUES_en
pt_brand,
#endif
#if NEDERLANDS_en
ned_brand,
#endif
#if DANSK_en
dan_brand,
#endif
#if NORSK_en
nor_brand,
#endif
#if SOOMI_en
soo_brand,
#endif
#if SVENSKA_en
sve_brand,
#endif
#if CESTINA_en
ces_brand,
#endif
#if LIETUVIU_K_en
lie_brand,
#endif
#if POLSKI_en
pol_brand,
#endif
#if SLOVENCINA_en
slo_brand,
#endif
#if SLOVENSCINA_en
sls_brand,
#endif
#if TURKU_en
tur_brand,
#endif
#if EESTI_en
ees_brand,
#endif
#if LATVIESU_en
lat_brand,
#endif
#if LIMBA_ROMANA_en
lim_brand,
#endif
#if PUSSIJI_en
pus_brand,
#endif
#if EAAHNIKA_en
eaa_brand,
#endif
};
const char *model_list[] = {
#if ENGLISH_en
en_model,
#endif
#if FRANCAIS_en
fr_model,
#endif
#if DEUTSCH_en
de_model,
#endif
#if ESPANOL_en
es_model,
#endif
#if ITALIAN_en
it_model,
#endif
#if PORTUGUES_en
pt_model,
#endif
#if NEDERLANDS_en
ned_model,
#endif
#if DANSK_en
dan_model,
#endif
#if NORSK_en
nor_model,
#endif
#if SOOMI_en
soo_model,
#endif
#if SVENSKA_en
sve_model,
#endif
#if CESTINA_en
ces_model,
#endif
#if LIETUVIU_K_en
lie_model,
#endif
#if POLSKI_en
pol_model,
#endif
#if SLOVENCINA_en
slo_model,
#endif
#if SLOVENSCINA_en
sls_model,
#endif
#if TURKU_en
tur_model,
#endif
#if EESTI_en
ees_model,
#endif
#if LATVIESU_en
lat_model,
#endif
#if LIMBA_ROMANA_en
lim_model,
#endif
#if PUSSIJI_en
pus_model,
#endif
#if EAAHNIKA_en
eaa_model,
#endif
};
const char *version_list[] = {
#if ENGLISH_en
en_version,
#endif
#if FRANCAIS_en
fr_version,
#endif
#if DEUTSCH_en
de_version,
#endif
#if ESPANOL_en
es_version,
#endif
#if ITALIAN_en
it_version,
#endif
#if PORTUGUES_en
pt_version,
#endif
#if NEDERLANDS_en
ned_version,
#endif
#if DANSK_en
dan_version,
#endif
#if NORSK_en
nor_version,
#endif
#if SOOMI_en
soo_version,
#endif
#if SVENSKA_en
sve_version,
#endif
#if CESTINA_en
ces_version,
#endif
#if LIETUVIU_K_en
lie_version,
#endif
#if POLSKI_en
pol_version,
#endif
#if SLOVENCINA_en
slo_version,
#endif
#if SLOVENSCINA_en
sls_version,
#endif
#if TURKU_en
tur_version,
#endif
#if EESTI_en
ees_version,
#endif
#if LATVIESU_en
lat_version,
#endif
#if LIMBA_ROMANA_en
lim_version,
#endif
#if PUSSIJI_en
pus_version,
#endif
#if EAAHNIKA_en
eaa_version,
#endif
};
const char *test_result_list[] = {
#if ENGLISH_en
en_test_result,
#endif
#if FRANCAIS_en
fr_test_result,
#endif
#if DEUTSCH_en
de_test_result,
#endif
#if ESPANOL_en
es_test_result,
#endif
#if ITALIAN_en
it_test_result,
#endif
#if PORTUGUES_en
pt_test_result,
#endif
#if NEDERLANDS_en
ned_test_result,
#endif
#if DANSK_en
dan_test_result,
#endif
#if NORSK_en
nor_test_result,
#endif
#if SOOMI_en
soo_test_result,
#endif
#if SVENSKA_en
sve_test_result,
#endif
#if CESTINA_en
ces_test_result,
#endif
#if LIETUVIU_K_en
lie_test_result,
#endif
#if POLSKI_en
pol_test_result,
#endif
#if SLOVENCINA_en
slo_test_result,
#endif
#if SLOVENSCINA_en
sls_test_result,
#endif
#if TURKU_en
tur_test_result,
#endif
#if EESTI_en
ees_test_result,
#endif
#if LATVIESU_en
lat_test_result,
#endif
#if LIMBA_ROMANA_en
lim_test_result,
#endif
#if PUSSIJI_en
pus_test_result,
#endif
#if EAAHNIKA_en
eaa_test_result,
#endif
};


const char *battery_type_list[] = {
#if ENGLISH_en
en_battery_type,
#endif
#if FRANCAIS_en
fr_battery_type,
#endif
#if DEUTSCH_en
de_battery_type,
#endif
#if ESPANOL_en
es_battery_type,
#endif
#if ITALIAN_en
it_battery_type,
#endif
#if PORTUGUES_en
pt_battery_type,
#endif
#if NEDERLANDS_en
ned_battery_type,
#endif
#if DANSK_en
dan_battery_type,
#endif
#if NORSK_en
nor_battery_type,
#endif
#if SOOMI_en
soo_battery_type,
#endif
#if SVENSKA_en
sve_battery_type,
#endif
#if CESTINA_en
ces_battery_type,
#endif
#if LIETUVIU_K_en
lie_battery_type,
#endif
#if POLSKI_en
pol_battery_type,
#endif
#if SLOVENCINA_en
slo_battery_type,
#endif
#if SLOVENSCINA_en
sls_battery_type,
#endif
#if TURKU_en
tur_battery_type,
#endif
#if EESTI_en
ees_battery_type,
#endif
#if LATVIESU_en
lat_battery_type,
#endif
#if LIMBA_ROMANA_en
lim_battery_type,
#endif
#if PUSSIJI_en
pus_battery_type,
#endif
#if EAAHNIKA_en
eaa_battery_type,
#endif
};

const char *agmf_list[] = {
#if ENGLISH_en
en_agmf,
#endif
#if FRANCAIS_en
fr_agmf,
#endif
#if DEUTSCH_en
de_agmf,
#endif
#if ESPANOL_en
es_agmf,
#endif
#if ITALIAN_en
it_agmf,
#endif
#if PORTUGUES_en
pt_agmf,
#endif
#if NEDERLANDS_en
ned_agmf,
#endif
#if DANSK_en
dan_agmf,
#endif
#if NORSK_en
nor_agmf,
#endif
#if SOOMI_en
soo_agmf,
#endif
#if SVENSKA_en
sve_agmf,
#endif
#if CESTINA_en
ces_agmf,
#endif
#if LIETUVIU_K_en
lie_agmf,
#endif
#if POLSKI_en
pol_agmf,
#endif
#if SLOVENCINA_en
slo_agmf,
#endif
#if SLOVENSCINA_en
sls_agmf,
#endif
#if TURKU_en
tur_agmf,
#endif
#if EESTI_en
ees_agmf,
#endif
#if LATVIESU_en
lat_agmf,
#endif
#if LIMBA_ROMANA_en
lim_agmf,
#endif
#if PUSSIJI_en
pus_agmf,
#endif
#if EAAHNIKA_en
eaa_agmf,
#endif
};
const char *agms_list[] = {
#if ENGLISH_en
en_agms,
#endif
#if FRANCAIS_en
fr_agms,
#endif
#if DEUTSCH_en
de_agms,
#endif
#if ESPANOL_en
es_agms,
#endif
#if ITALIAN_en
it_agms,
#endif
#if PORTUGUES_en
pt_agms,
#endif
#if NEDERLANDS_en
ned_agms,
#endif
#if DANSK_en
dan_agms,
#endif
#if NORSK_en
nor_agms,
#endif
#if SOOMI_en
soo_agms,
#endif
#if SVENSKA_en
sve_agms,
#endif
#if CESTINA_en
ces_agms,
#endif
#if LIETUVIU_K_en
lie_agms,
#endif
#if POLSKI_en
pol_agms,
#endif
#if SLOVENCINA_en
slo_agms,
#endif
#if SLOVENSCINA_en
sls_agms,
#endif
#if TURKU_en
tur_agms,
#endif
#if EESTI_en
ees_agms,
#endif
#if LATVIESU_en
lat_agms,
#endif
#if LIMBA_ROMANA_en
lim_agms,
#endif
#if PUSSIJI_en
pus_agms,
#endif
#if EAAHNIKA_en
eaa_agms,
#endif
};
const char *select_rating_list[] = {
#if ENGLISH_en
en_select_rating,
#endif
#if FRANCAIS_en
fr_select_rating,
#endif
#if DEUTSCH_en
de_select_rating,
#endif
#if ESPANOL_en
es_select_rating,
#endif
#if ITALIAN_en
it_select_rating,
#endif
#if PORTUGUES_en
pt_select_rating,
#endif
#if NEDERLANDS_en
ned_select_rating,
#endif
#if DANSK_en
dan_select_rating,
#endif
#if NORSK_en
nor_select_rating,
#endif
#if SOOMI_en
soo_select_rating,
#endif
#if SVENSKA_en
sve_select_rating,
#endif
#if CESTINA_en
ces_select_rating,
#endif
#if LIETUVIU_K_en
lie_select_rating,
#endif
#if POLSKI_en
pol_select_rating,
#endif
#if SLOVENCINA_en
slo_select_rating,
#endif
#if SLOVENSCINA_en
sls_select_rating,
#endif
#if TURKU_en
tur_select_rating,
#endif
#if EESTI_en
ees_select_rating,
#endif
#if LATVIESU_en
lat_select_rating,
#endif
#if LIMBA_ROMANA_en
lim_select_rating,
#endif
#if PUSSIJI_en
pus_select_rating,
#endif
#if EAAHNIKA_en
eaa_select_rating,
#endif
};

const char *set_capacity_list[] = {
#if ENGLISH_en
en_set_capacity,
#endif
#if FRANCAIS_en
fr_set_capacity,
#endif
#if DEUTSCH_en
de_set_capacity,
#endif
#if ESPANOL_en
es_set_capacity,
#endif
#if ITALIAN_en
it_set_capacity,
#endif
#if PORTUGUES_en
pt_set_capacity,
#endif
#if NEDERLANDS_en
ned_set_capacity,
#endif
#if DANSK_en
dan_set_capacity,
#endif
#if NORSK_en
nor_set_capacity,
#endif
#if SOOMI_en
soo_set_capacity,
#endif
#if SVENSKA_en
sve_set_capacity,
#endif
#if CESTINA_en
ces_set_capacity,
#endif
#if LIETUVIU_K_en
lie_set_capacity,
#endif
#if POLSKI_en
pol_set_capacity,
#endif
#if SLOVENCINA_en
slo_set_capacity,
#endif
#if SLOVENSCINA_en
sls_set_capacity,
#endif
#if TURKU_en
tur_set_capacity,
#endif
#if EESTI_en
ees_set_capacity,
#endif
#if LATVIESU_en
lat_set_capacity,
#endif
#if LIMBA_ROMANA_en
lim_set_capacity,
#endif
#if PUSSIJI_en
pus_set_capacity,
#endif
#if EAAHNIKA_en
eaa_set_capacity,
#endif
};
const char *back_light_list[] = {
#if ENGLISH_en
en_back_light,
#endif
#if FRANCAIS_en
fr_back_light,
#endif
#if DEUTSCH_en
de_back_light,
#endif
#if ESPANOL_en
es_back_light,
#endif
#if ITALIAN_en
it_back_light,
#endif
#if PORTUGUES_en
pt_back_light,
#endif
#if NEDERLANDS_en
ned_back_light,
#endif
#if DANSK_en
dan_back_light,
#endif
#if NORSK_en
nor_back_light,
#endif
#if SOOMI_en
soo_back_light,
#endif
#if SVENSKA_en
sve_back_light,
#endif
#if CESTINA_en
ces_back_light,
#endif
#if LIETUVIU_K_en
lie_back_light,
#endif
#if POLSKI_en
pol_back_light,
#endif
#if SLOVENCINA_en
slo_back_light,
#endif
#if SLOVENSCINA_en
sls_back_light,
#endif
#if TURKU_en
tur_back_light,
#endif
#if EESTI_en
ees_back_light,
#endif
#if LATVIESU_en
lat_back_light,
#endif
#if LIMBA_ROMANA_en
lim_back_light,
#endif
#if PUSSIJI_en
pus_back_light,
#endif
#if EAAHNIKA_en
eaa_back_light,
#endif
};
const char *language_select_list[] = {
#if ENGLISH_en
en_language_select,
#endif
#if FRANCAIS_en
fr_language_select,
#endif
#if DEUTSCH_en
de_language_select,
#endif
#if ESPANOL_en
es_language_select,
#endif
#if ITALIAN_en
it_language_select,
#endif
#if PORTUGUES_en
pt_language_select,
#endif
#if NEDERLANDS_en
ned_language_select,
#endif
#if DANSK_en
dan_language_select,
#endif
#if NORSK_en
nor_language_select,
#endif
#if SOOMI_en
soo_language_select,
#endif
#if SVENSKA_en
sve_language_select,
#endif
#if CESTINA_en
ces_language_select,
#endif
#if LIETUVIU_K_en
lie_language_select,
#endif
#if POLSKI_en
pol_language_select,
#endif
#if SLOVENCINA_en
slo_language_select,
#endif
#if SLOVENSCINA_en
sls_language_select,
#endif
#if TURKU_en
tur_language_select,
#endif
#if EESTI_en
ees_language_select,
#endif
#if LATVIESU_en
lat_language_select,
#endif
#if LIMBA_ROMANA_en
lim_language_select,
#endif
#if PUSSIJI_en
pus_language_select,
#endif
#if EAAHNIKA_en
eaa_language_select,
#endif
};
const char *clock_list[] = {
#if ENGLISH_en
en_clock,
#endif
#if FRANCAIS_en
fr_clock,
#endif
#if DEUTSCH_en
de_clock,
#endif
#if ESPANOL_en
es_clock,
#endif
#if ITALIAN_en
it_clock,
#endif
#if PORTUGUES_en
pt_clock,
#endif
#if NEDERLANDS_en
ned_clock,
#endif
#if DANSK_en
dan_clock,
#endif
#if NORSK_en
nor_clock,
#endif
#if SOOMI_en
soo_clock,
#endif
#if SVENSKA_en
sve_clock,
#endif
#if CESTINA_en
ces_clock,
#endif
#if LIETUVIU_K_en
lie_clock,
#endif
#if POLSKI_en
pol_clock,
#endif
#if SLOVENCINA_en
slo_clock,
#endif
#if SLOVENSCINA_en
sls_clock,
#endif
#if TURKU_en
tur_clock,
#endif
#if EESTI_en
ees_clock,
#endif
#if LATVIESU_en
lat_clock,
#endif
#if LIMBA_ROMANA_en
lim_clock,
#endif
#if PUSSIJI_en
pus_clock,
#endif
#if EAAHNIKA_en
eaa_clock,
#endif
};
const char *clear_memory_list[] = {
#if ENGLISH_en
en_clear_memory,
#endif
#if FRANCAIS_en
fr_clear_memory,
#endif
#if DEUTSCH_en
de_clear_memory,
#endif
#if ESPANOL_en
es_clear_memory,
#endif
#if ITALIAN_en
it_clear_memory,
#endif
#if PORTUGUES_en
pt_clear_memory,
#endif
#if NEDERLANDS_en
ned_clear_memory,
#endif
#if DANSK_en
dan_clear_memory,
#endif
#if NORSK_en
nor_clear_memory,
#endif
#if SOOMI_en
soo_clear_memory,
#endif
#if SVENSKA_en
sve_clear_memory,
#endif
#if CESTINA_en
ces_clear_memory,
#endif
#if LIETUVIU_K_en
lie_clear_memory,
#endif
#if POLSKI_en
pol_clear_memory,
#endif
#if SLOVENCINA_en
slo_clear_memory,
#endif
#if SLOVENSCINA_en
sls_clear_memory,
#endif
#if TURKU_en
tur_clear_memory,
#endif
#if EESTI_en
ees_clear_memory,
#endif
#if LATVIESU_en
lat_clear_memory,
#endif
#if LIMBA_ROMANA_en
lim_clear_memory,
#endif
#if PUSSIJI_en
pus_clear_memory,
#endif
#if EAAHNIKA_en
eaa_clear_memory,
#endif
};
const char *information_list[] = {
#if ENGLISH_en
en_information,
#endif
#if FRANCAIS_en
fr_information,
#endif
#if DEUTSCH_en
de_information,
#endif
#if ESPANOL_en
es_information,
#endif
#if ITALIAN_en
it_information,
#endif
#if PORTUGUES_en
pt_information,
#endif
#if NEDERLANDS_en
ned_information,
#endif
#if DANSK_en
dan_information,
#endif
#if NORSK_en
nor_information,
#endif
#if SOOMI_en
soo_information,
#endif
#if SVENSKA_en
sve_information,
#endif
#if CESTINA_en
ces_information,
#endif
#if LIETUVIU_K_en
lie_information,
#endif
#if POLSKI_en
pol_information,
#endif
#if SLOVENCINA_en
slo_information,
#endif
#if SLOVENSCINA_en
sls_information,
#endif
#if TURKU_en
tur_information,
#endif
#if EESTI_en
ees_information,
#endif
#if LATVIESU_en
lat_information,
#endif
#if LIMBA_ROMANA_en
lim_information,
#endif
#if PUSSIJI_en
pus_information,
#endif
#if EAAHNIKA_en
eaa_information,
#endif
};
const char *time_zone_list[] = {
#if ENGLISH_en
en_time_zone,
#endif
#if FRANCAIS_en
fr_time_zone,
#endif
#if DEUTSCH_en
de_time_zone,
#endif
#if ESPANOL_en
es_time_zone,
#endif
#if ITALIAN_en
it_time_zone,
#endif
#if PORTUGUES_en
pt_time_zone,
#endif
#if NEDERLANDS_en
ned_time_zone,
#endif
#if DANSK_en
dan_time_zone,
#endif
#if NORSK_en
nor_time_zone,
#endif
#if SOOMI_en
soo_time_zone,
#endif
#if SVENSKA_en
sve_time_zone,
#endif
#if CESTINA_en
ces_time_zone,
#endif
#if LIETUVIU_K_en
lie_time_zone,
#endif
#if POLSKI_en
pol_time_zone,
#endif
#if SLOVENCINA_en
slo_time_zone,
#endif
#if SLOVENSCINA_en
sls_time_zone,
#endif
#if TURKU_en
tur_time_zone,
#endif
#if EESTI_en
ees_time_zone,
#endif
#if LATVIESU_en
lat_time_zone,
#endif
#if LIMBA_ROMANA_en
lim_time_zone,
#endif
#if PUSSIJI_en
pus_time_zone,
#endif
#if EAAHNIKA_en
eaa_time_zone,
#endif
};

const char *abort_list[] = {
#if ENGLISH_en
en_abort,
#endif
#if FRANCAIS_en
fr_abort,
#endif
#if DEUTSCH_en
de_abort,
#endif
#if ESPANOL_en
es_abort,
#endif
#if ITALIAN_en
it_abort,
#endif
#if PORTUGUES_en
pt_abort,
#endif
#if NEDERLANDS_en
ned_abort,
#endif
#if DANSK_en
dan_abort,
#endif
#if NORSK_en
nor_abort,
#endif
#if SOOMI_en
soo_abort,
#endif
#if SVENSKA_en
sve_abort,
#endif
#if CESTINA_en
ces_abort,
#endif
#if LIETUVIU_K_en
lie_abort,
#endif
#if POLSKI_en
pol_abort,
#endif
#if SLOVENCINA_en
slo_abort,
#endif
#if SLOVENSCINA_en
sls_abort,
#endif
#if TURKU_en
tur_abort,
#endif
#if EESTI_en
ees_abort,
#endif
#if LATVIESU_en
lat_abort,
#endif
#if LIMBA_ROMANA_en
lim_abort,
#endif
#if PUSSIJI_en
pus_abort,
#endif
#if EAAHNIKA_en
eaa_abort,
#endif
};
const char *direct_connect_to_battery_list[] = {
#if ENGLISH_en
en_directly_connect_to_battery,
#endif
#if FRANCAIS_en
fr_directly_connect_to_battery,
#endif
#if DEUTSCH_en
de_directly_connect_to_battery,
#endif
#if ESPANOL_en
es_directly_connect_to_battery,
#endif
#if ITALIAN_en
it_directly_connect_to_battery,
#endif
#if PORTUGUES_en
pt_directly_connect_to_battery,
#endif
#if NEDERLANDS_en
ned_directly_connect_to_battery,
#endif
#if DANSK_en
dan_directly_connect_to_battery,
#endif
#if NORSK_en
nor_directly_connect_to_battery,
#endif
#if SOOMI_en
soo_directly_connect_to_battery,
#endif
#if SVENSKA_en
sve_directly_connect_to_battery,
#endif
#if CESTINA_en
ces_directly_connect_to_battery,
#endif
#if LIETUVIU_K_en
lie_directly_connect_to_battery,
#endif
#if POLSKI_en
pol_directly_connect_to_battery,
#endif
#if SLOVENCINA_en
slo_directly_connect_to_battery,
#endif
#if SLOVENSCINA_en
sls_directly_connect_to_battery,
#endif
#if TURKU_en
tur_directly_connect_to_battery,
#endif
#if EESTI_en
ees_directly_connect_to_battery,
#endif
#if LATVIESU_en
lat_directly_connect_to_battery,
#endif
#if LIMBA_ROMANA_en
lim_directly_connect_to_battery,
#endif
#if PUSSIJI_en
pus_directly_connect_to_battery,
#endif
#if EAAHNIKA_en
eaa_directly_connect_to_battery,
#endif
};
const char *insert_vin_list[] = {
#if ENGLISH_en
en_insert_vin,
#endif
#if FRANCAIS_en
fr_insert_vin,
#endif
#if DEUTSCH_en
de_insert_vin,
#endif
#if ESPANOL_en
es_insert_vin,
#endif
#if ITALIAN_en
it_insert_vin,
#endif
#if PORTUGUES_en
pt_insert_vin,
#endif
#if NEDERLANDS_en
ned_insert_vin,
#endif
#if DANSK_en
dan_insert_vin,
#endif
#if NORSK_en
nor_insert_vin,
#endif
#if SOOMI_en
soo_insert_vin,
#endif
#if SVENSKA_en
sve_insert_vin,
#endif
#if CESTINA_en
ces_insert_vin,
#endif
#if LIETUVIU_K_en
lie_insert_vin,
#endif
#if POLSKI_en
pol_insert_vin,
#endif
#if SLOVENCINA_en
slo_insert_vin,
#endif
#if SLOVENSCINA_en
sls_insert_vin,
#endif
#if TURKU_en
tur_insert_vin,
#endif
#if EESTI_en
ees_insert_vin,
#endif
#if LATVIESU_en
lat_insert_vin,
#endif
#if LIMBA_ROMANA_en
lim_insert_vin,
#endif
#if PUSSIJI_en
pus_insert_vin,
#endif
#if EAAHNIKA_en
eaa_insert_vin,
#endif
};
const char *test_not_suitable_list[] = {
#if ENGLISH_en
en_test_not_suitable,
#endif
#if FRANCAIS_en
fr_test_not_suitable,
#endif
#if DEUTSCH_en
de_test_not_suitable,
#endif
#if ESPANOL_en
es_test_not_suitable,
#endif
#if ITALIAN_en
it_test_not_suitable,
#endif
#if PORTUGUES_en
pt_test_not_suitable,
#endif
#if NEDERLANDS_en
ned_test_not_suitable,
#endif
#if DANSK_en
dan_test_not_suitable,
#endif
#if NORSK_en
nor_test_not_suitable,
#endif
#if SOOMI_en
soo_test_not_suitable,
#endif
#if SVENSKA_en
sve_test_not_suitable,
#endif
#if CESTINA_en
ces_test_not_suitable,
#endif
#if LIETUVIU_K_en
lie_test_not_suitable,
#endif
#if POLSKI_en
pol_test_not_suitable,
#endif
#if SLOVENCINA_en
slo_test_not_suitable,
#endif
#if SLOVENSCINA_en
sls_test_not_suitable,
#endif
#if TURKU_en
tur_test_not_suitable,
#endif
#if EESTI_en
ees_test_not_suitable,
#endif
#if LATVIESU_en
lat_test_not_suitable,
#endif
#if LIMBA_ROMANA_en
lim_test_not_suitable,
#endif
#if PUSSIJI_en
pus_test_not_suitable,
#endif
#if EAAHNIKA_en
eaa_test_not_suitable,
#endif
};
const char *yes_list[] = {
#if ENGLISH_en
en_yes,
#endif
#if FRANCAIS_en
fr_yes,
#endif
#if DEUTSCH_en
de_yes,
#endif
#if ESPANOL_en
es_yes,
#endif
#if ITALIAN_en
it_yes,
#endif
#if PORTUGUES_en
pt_yes,
#endif
#if NEDERLANDS_en
ned_yes,
#endif
#if DANSK_en
dan_yes,
#endif
#if NORSK_en
nor_yes,
#endif
#if SOOMI_en
soo_yes,
#endif
#if SVENSKA_en
sve_yes,
#endif
#if CESTINA_en
ces_yes,
#endif
#if LIETUVIU_K_en
lie_yes,
#endif
#if POLSKI_en
pol_yes,
#endif
#if SLOVENCINA_en
slo_yes,
#endif
#if SLOVENSCINA_en
sls_yes,
#endif
#if TURKU_en
tur_yes,
#endif
#if EESTI_en
ees_yes,
#endif
#if LATVIESU_en
lat_yes,
#endif
#if LIMBA_ROMANA_en
lim_yes,
#endif
#if PUSSIJI_en
pus_yes,
#endif
#if EAAHNIKA_en
eaa_yes,
#endif
};
const char *no_list[] = {
#if ENGLISH_en
en_no,
#endif
#if FRANCAIS_en
fr_no,
#endif
#if DEUTSCH_en
de_no,
#endif
#if ESPANOL_en
es_no,
#endif
#if ITALIAN_en
it_no,
#endif
#if PORTUGUES_en
pt_no,
#endif
#if NEDERLANDS_en
ned_no,
#endif
#if DANSK_en
dan_no,
#endif
#if NORSK_en
nor_no,
#endif
#if SOOMI_en
soo_no,
#endif
#if SVENSKA_en
sve_no,
#endif
#if CESTINA_en
ces_no,
#endif
#if LIETUVIU_K_en
lie_no,
#endif
#if POLSKI_en
pol_no,
#endif
#if SLOVENCINA_en
slo_no,
#endif
#if SLOVENSCINA_en
sls_no,
#endif
#if TURKU_en
tur_no,
#endif
#if EESTI_en
ees_no,
#endif
#if LATVIESU_en
lat_no,
#endif
#if LIMBA_ROMANA_en
lim_no,
#endif
#if PUSSIJI_en
pus_no,
#endif
#if EAAHNIKA_en
eaa_no,
#endif
};
const char *print_result_list[] = {
#if ENGLISH_en
en_print_result,
#endif
#if FRANCAIS_en
fr_print_result,
#endif
#if DEUTSCH_en
de_print_result,
#endif
#if ESPANOL_en
es_print_result,
#endif
#if ITALIAN_en
it_print_result,
#endif
#if PORTUGUES_en
pt_print_result,
#endif
#if NEDERLANDS_en
ned_print_result,
#endif
#if DANSK_en
dan_print_result,
#endif
#if NORSK_en
nor_print_result,
#endif
#if SOOMI_en
soo_print_result,
#endif
#if SVENSKA_en
sve_print_result,
#endif
#if CESTINA_en
ces_print_result,
#endif
#if LIETUVIU_K_en
lie_print_result,
#endif
#if POLSKI_en
pol_print_result,
#endif
#if SLOVENCINA_en
slo_print_result,
#endif
#if SLOVENSCINA_en
sls_print_result,
#endif
#if TURKU_en
tur_print_result,
#endif
#if EESTI_en
ees_print_result,
#endif
#if LATVIESU_en
lat_print_result,
#endif
#if LIMBA_ROMANA_en
lim_print_result,
#endif
#if PUSSIJI_en
pus_print_result,
#endif
#if EAAHNIKA_en
eaa_print_result,
#endif
};
const char *print_successfully_list[] = {
#if ENGLISH_en
en_print_successful,
#endif
#if FRANCAIS_en
fr_print_successful,
#endif
#if DEUTSCH_en
de_print_successful,
#endif
#if ESPANOL_en
es_print_successful,
#endif
#if ITALIAN_en
it_print_successful,
#endif
#if PORTUGUES_en
pt_print_successful,
#endif
#if NEDERLANDS_en
ned_print_successful,
#endif
#if DANSK_en
dan_print_successful,
#endif
#if NORSK_en
nor_print_successful,
#endif
#if SOOMI_en
soo_print_successful,
#endif
#if SVENSKA_en
sve_print_successful,
#endif
#if CESTINA_en
ces_print_successful,
#endif
#if LIETUVIU_K_en
lie_print_successful,
#endif
#if POLSKI_en
pol_print_successful,
#endif
#if SLOVENCINA_en
slo_print_successful,
#endif
#if SLOVENSCINA_en
sls_print_successful,
#endif
#if TURKU_en
tur_print_successful,
#endif
#if EESTI_en
ees_print_successful,
#endif
#if LATVIESU_en
lat_print_successful,
#endif
#if LIMBA_ROMANA_en
lim_print_successful,
#endif
#if PUSSIJI_en
pus_print_successful,
#endif
#if EAAHNIKA_en
eaa_print_successful,
#endif
};
const char *check_clamp_list[] = {
#if ENGLISH_en
en_check_clamp,
#endif
#if FRANCAIS_en
fr_check_clamp,
#endif
#if DEUTSCH_en
de_check_clamp,
#endif
#if ESPANOL_en
es_check_clamp,
#endif
#if ITALIAN_en
it_check_clamp,
#endif
#if PORTUGUES_en
pt_check_clamp,
#endif
#if NEDERLANDS_en
ned_check_clamp,
#endif
#if DANSK_en
dan_check_clamp,
#endif
#if NORSK_en
nor_check_clamp,
#endif
#if SOOMI_en
soo_check_clamp,
#endif
#if SVENSKA_en
sve_check_clamp,
#endif
#if CESTINA_en
ces_check_clamp,
#endif
#if LIETUVIU_K_en
lie_check_clamp,
#endif
#if POLSKI_en
pol_check_clamp,
#endif
#if SLOVENCINA_en
slo_check_clamp,
#endif
#if SLOVENSCINA_en
sls_check_clamp,
#endif
#if TURKU_en
tur_check_clamp,
#endif
#if EESTI_en
ees_check_clamp,
#endif
#if LATVIESU_en
lat_check_clamp,
#endif
#if LIMBA_ROMANA_en
lim_check_clamp,
#endif
#if PUSSIJI_en
pus_check_clamp,
#endif
#if EAAHNIKA_en
eaa_check_clamp,
#endif
};
const char *insert_change_aa_list[] = {
#if ENGLISH_en
en_insert_change_aa,
#endif
#if FRANCAIS_en
fr_insert_change_aa,
#endif
#if DEUTSCH_en
de_insert_change_aa,
#endif
#if ESPANOL_en
es_insert_change_aa,
#endif
#if ITALIAN_en
it_insert_change_aa,
#endif
#if PORTUGUES_en
pt_insert_change_aa,
#endif
#if NEDERLANDS_en
ned_insert_change_aa,
#endif
#if DANSK_en
dan_insert_change_aa,
#endif
#if NORSK_en
nor_insert_change_aa,
#endif
#if SOOMI_en
soo_insert_change_aa,
#endif
#if SVENSKA_en
sve_insert_change_aa,
#endif
#if CESTINA_en
ces_insert_change_aa,
#endif
#if LIETUVIU_K_en
lie_insert_change_aa,
#endif
#if POLSKI_en
pol_insert_change_aa,
#endif
#if SLOVENCINA_en
slo_insert_change_aa,
#endif
#if SLOVENSCINA_en
sls_insert_change_aa,
#endif
#if TURKU_en
tur_insert_change_aa,
#endif
#if EESTI_en
ees_insert_change_aa,
#endif
#if LATVIESU_en
lat_insert_change_aa,
#endif
#if LIMBA_ROMANA_en
lim_insert_change_aa,
#endif
#if PUSSIJI_en
pus_insert_change_aa,
#endif
#if EAAHNIKA_en
eaa_insert_change_aa,
#endif
};
const char *load_error_list[] = {
#if ENGLISH_en
en_load_error,
#endif
#if FRANCAIS_en
fr_load_error,
#endif
#if DEUTSCH_en
de_load_error,
#endif
#if ESPANOL_en
es_load_error,
#endif
#if ITALIAN_en
it_load_error,
#endif
#if PORTUGUES_en
pt_load_error,
#endif
#if NEDERLANDS_en
ned_load_error,
#endif
#if DANSK_en
dan_load_error,
#endif
#if NORSK_en
nor_load_error,
#endif
#if SOOMI_en
soo_load_error,
#endif
#if SVENSKA_en
sve_load_error,
#endif
#if CESTINA_en
ces_load_error,
#endif
#if LIETUVIU_K_en
lie_load_error,
#endif
#if POLSKI_en
pol_load_error,
#endif
#if SLOVENCINA_en
slo_load_error,
#endif
#if SLOVENSCINA_en
sls_load_error,
#endif
#if TURKU_en
tur_load_error,
#endif
#if EESTI_en
ees_load_error,
#endif
#if LATVIESU_en
lat_load_error,
#endif
#if LIMBA_ROMANA_en
lim_load_error,
#endif
#if PUSSIJI_en
pus_load_error,
#endif
#if EAAHNIKA_en
eaa_load_error,
#endif
};
const char *voltage_high_list[] = {
#if ENGLISH_en
en_voltage_high,
#endif
#if FRANCAIS_en
fr_voltage_high,
#endif
#if DEUTSCH_en
de_voltage_high,
#endif
#if ESPANOL_en
es_voltage_high,
#endif
#if ITALIAN_en
it_voltage_high,
#endif
#if PORTUGUES_en
pt_voltage_high,
#endif
#if NEDERLANDS_en
ned_voltage_high,
#endif
#if DANSK_en
dan_voltage_high,
#endif
#if NORSK_en
nor_voltage_high,
#endif
#if SOOMI_en
soo_voltage_high,
#endif
#if SVENSKA_en
sve_voltage_high,
#endif
#if CESTINA_en
ces_voltage_high,
#endif
#if LIETUVIU_K_en
lie_voltage_high,
#endif
#if POLSKI_en
pol_voltage_high,
#endif
#if SLOVENCINA_en
slo_voltage_high,
#endif
#if SLOVENSCINA_en
sls_voltage_high,
#endif
#if TURKU_en
tur_voltage_high,
#endif
#if EESTI_en
ees_voltage_high,
#endif
#if LATVIESU_en
lat_voltage_high,
#endif
#if LIMBA_ROMANA_en
lim_voltage_high,
#endif
#if PUSSIJI_en
pus_voltage_high,
#endif
#if EAAHNIKA_en
eaa_voltage_high,
#endif
};
const char *voltage_unstable_list[] = {
#if ENGLISH_en
en_voltage_unstable,
#endif
#if FRANCAIS_en
fr_voltage_unstable,
#endif
#if DEUTSCH_en
de_voltage_unstable,
#endif
#if ESPANOL_en
es_voltage_unstable,
#endif
#if ITALIAN_en
it_voltage_unstable,
#endif
#if PORTUGUES_en
pt_voltage_unstable,
#endif
#if NEDERLANDS_en
ned_voltage_unstable,
#endif
#if DANSK_en
dan_voltage_unstable,
#endif
#if NORSK_en
nor_voltage_unstable,
#endif
#if SOOMI_en
soo_voltage_unstable,
#endif
#if SVENSKA_en
sve_voltage_unstable,
#endif
#if CESTINA_en
ces_voltage_unstable,
#endif
#if LIETUVIU_K_en
lie_voltage_unstable,
#endif
#if POLSKI_en
pol_voltage_unstable,
#endif
#if SLOVENCINA_en
slo_voltage_unstable,
#endif
#if SLOVENSCINA_en
sls_voltage_unstable,
#endif
#if TURKU_en
tur_voltage_unstable,
#endif
#if EESTI_en
ees_voltage_unstable,
#endif
#if LATVIESU_en
lat_voltage_unstable,
#endif
#if LIMBA_ROMANA_en
lim_voltage_unstable,
#endif
#if PUSSIJI_en
pus_voltage_unstable,
#endif
#if EAAHNIKA_en
eaa_voltage_unstable,
#endif
};
const char *is_it_6v_list[] = {
#if ENGLISH_en
en_is_it_6v,
#endif
#if FRANCAIS_en
fr_is_it_6v,
#endif
#if DEUTSCH_en
de_is_it_6v,
#endif
#if ESPANOL_en
es_is_it_6v,
#endif
#if ITALIAN_en
it_is_it_6v,
#endif
#if PORTUGUES_en
pt_is_it_6v,
#endif
#if NEDERLANDS_en
ned_is_it_6v,
#endif
#if DANSK_en
dan_is_it_6v,
#endif
#if NORSK_en
nor_is_it_6v,
#endif
#if SOOMI_en
soo_is_it_6v,
#endif
#if SVENSKA_en
sve_is_it_6v,
#endif
#if CESTINA_en
ces_is_it_6v,
#endif
#if LIETUVIU_K_en
lie_is_it_6v,
#endif
#if POLSKI_en
pol_is_it_6v,
#endif
#if SLOVENCINA_en
slo_is_it_6v,
#endif
#if SLOVENSCINA_en
sls_is_it_6v,
#endif
#if TURKU_en
tur_is_it_6v,
#endif
#if EESTI_en
ees_is_it_6v,
#endif
#if LATVIESU_en
lat_is_it_6v,
#endif
#if LIMBA_ROMANA_en
lim_is_it_6v,
#endif
#if PUSSIJI_en
pus_is_it_6v,
#endif
#if EAAHNIKA_en
eaa_is_it_6v,
#endif
};
const char *is_battery_charge_list[] = {
#if ENGLISH_en
en_is_battery_charge,
#endif
#if FRANCAIS_en
fr_is_battery_charge,
#endif
#if DEUTSCH_en
de_is_battery_charge,
#endif
#if ESPANOL_en
es_is_battery_charge,
#endif
#if ITALIAN_en
it_is_battery_charge,
#endif
#if PORTUGUES_en
pt_is_battery_charge,
#endif
#if NEDERLANDS_en
ned_is_battery_charge,
#endif
#if DANSK_en
dan_is_battery_charge,
#endif
#if NORSK_en
nor_is_battery_charge,
#endif
#if SOOMI_en
soo_is_battery_charge,
#endif
#if SVENSKA_en
sve_is_battery_charge,
#endif
#if CESTINA_en
ces_is_battery_charge,
#endif
#if LIETUVIU_K_en
lie_is_battery_charge,
#endif
#if POLSKI_en
pol_is_battery_charge,
#endif
#if SLOVENCINA_en
slo_is_battery_charge,
#endif
#if SLOVENSCINA_en
sls_is_battery_charge,
#endif
#if TURKU_en
tur_is_battery_charge,
#endif
#if EESTI_en
ees_is_battery_charge,
#endif
#if LATVIESU_en
lat_is_battery_charge,
#endif
#if LIMBA_ROMANA_en
lim_is_battery_charge,
#endif
#if PUSSIJI_en
pus_is_battery_charge,
#endif
#if EAAHNIKA_en
eaa_is_battery_charge,
#endif
};
const char *battery_in_vehicle_list[] = {
#if ENGLISH_en
en_battery_in_vehicle,
#endif
#if FRANCAIS_en
fr_battery_in_vehicle,
#endif
#if DEUTSCH_en
de_battery_in_vehicle,
#endif
#if ESPANOL_en
es_battery_in_vehicle,
#endif
#if ITALIAN_en
it_battery_in_vehicle,
#endif
#if PORTUGUES_en
pt_battery_in_vehicle,
#endif
#if NEDERLANDS_en
ned_battery_in_vehicle,
#endif
#if DANSK_en
dan_battery_in_vehicle,
#endif
#if NORSK_en
nor_battery_in_vehicle,
#endif
#if SOOMI_en
soo_battery_in_vehicle,
#endif
#if SVENSKA_en
sve_battery_in_vehicle,
#endif
#if CESTINA_en
ces_battery_in_vehicle,
#endif
#if LIETUVIU_K_en
lie_battery_in_vehicle,
#endif
#if POLSKI_en
pol_battery_in_vehicle,
#endif
#if SLOVENCINA_en
slo_battery_in_vehicle,
#endif
#if SLOVENSCINA_en
sls_battery_in_vehicle,
#endif
#if TURKU_en
tur_battery_in_vehicle,
#endif
#if EESTI_en
ees_battery_in_vehicle,
#endif
#if LATVIESU_en
lat_battery_in_vehicle,
#endif
#if LIMBA_ROMANA_en
lim_battery_in_vehicle,
#endif
#if PUSSIJI_en
pus_battery_in_vehicle,
#endif
#if EAAHNIKA_en
eaa_battery_in_vehicle,
#endif
};
const char *print_list[] = {
#if ENGLISH_en
en_print,
#endif
#if FRANCAIS_en
fr_print,
#endif
#if DEUTSCH_en
de_print,
#endif
#if ESPANOL_en
es_print,
#endif
#if ITALIAN_en
it_print,
#endif
#if PORTUGUES_en
pt_print,
#endif
#if NEDERLANDS_en
ned_print,
#endif
#if DANSK_en
dan_print,
#endif
#if NORSK_en
nor_print,
#endif
#if SOOMI_en
soo_print,
#endif
#if SVENSKA_en
sve_print,
#endif
#if CESTINA_en
ces_print,
#endif
#if LIETUVIU_K_en
lie_print,
#endif
#if POLSKI_en
pol_print,
#endif
#if SLOVENCINA_en
slo_print,
#endif
#if SLOVENSCINA_en
sls_print,
#endif
#if TURKU_en
tur_print,
#endif
#if EESTI_en
ees_print,
#endif
#if LATVIESU_en
lat_print,
#endif
#if LIMBA_ROMANA_en
lim_print,
#endif
#if PUSSIJI_en
pus_print,
#endif
#if EAAHNIKA_en
eaa_print,
#endif
};
const char *print_q_list[] = {
#if ENGLISH_en
en_print_q,
#endif
#if FRANCAIS_en
fr_print_q,
#endif
#if DEUTSCH_en
de_print_q,
#endif
#if ESPANOL_en
es_print_q,
#endif
#if ITALIAN_en
it_print_q,
#endif
#if PORTUGUES_en
pt_print_q,
#endif
#if NEDERLANDS_en
ned_print_q,
#endif
#if DANSK_en
dan_print_q,
#endif
#if NORSK_en
nor_print_q,
#endif
#if SOOMI_en
soo_print_q,
#endif
#if SVENSKA_en
sve_print_q,
#endif
#if CESTINA_en
ces_print_q,
#endif
#if LIETUVIU_K_en
lie_print_q,
#endif
#if POLSKI_en
pol_print_q,
#endif
#if SLOVENCINA_en
slo_print_q,
#endif
#if SLOVENSCINA_en
sls_print_q,
#endif
#if TURKU_en
tur_print_q,
#endif
#if EESTI_en
ees_print_q,
#endif
#if LATVIESU_en
lat_print_q,
#endif
#if LIMBA_ROMANA_en
lim_print_q,
#endif
#if PUSSIJI_en
pus_print_q,
#endif
#if EAAHNIKA_en
eaa_print_q,
#endif
};
const char *erase_list[] = {
#if ENGLISH_en
en_erase,
#endif
#if FRANCAIS_en
fr_erase,
#endif
#if DEUTSCH_en
de_erase,
#endif
#if ESPANOL_en
es_erase,
#endif
#if ITALIAN_en
it_erase,
#endif
#if PORTUGUES_en
pt_erase,
#endif
#if NEDERLANDS_en
ned_erase,
#endif
#if DANSK_en
dan_erase,
#endif
#if NORSK_en
nor_erase,
#endif
#if SOOMI_en
soo_erase,
#endif
#if SVENSKA_en
sve_erase,
#endif
#if CESTINA_en
ces_erase,
#endif
#if LIETUVIU_K_en
lie_erase,
#endif
#if POLSKI_en
pol_erase,
#endif
#if SLOVENCINA_en
slo_erase,
#endif
#if SLOVENSCINA_en
sls_erase,
#endif
#if TURKU_en
tur_erase,
#endif
#if EESTI_en
ees_erase,
#endif
#if LATVIESU_en
lat_erase,
#endif
#if LIMBA_ROMANA_en
lim_erase,
#endif
#if PUSSIJI_en
pus_erase,
#endif
#if EAAHNIKA_en
eaa_erase,
#endif
};
const char *memory_erase_list[] = {
#if ENGLISH_en
en_memory_erase,
#endif
#if FRANCAIS_en
fr_memory_erase,
#endif
#if DEUTSCH_en
de_memory_erase,
#endif
#if ESPANOL_en
es_memory_erase,
#endif
#if ITALIAN_en
it_memory_erase,
#endif
#if PORTUGUES_en
pt_memory_erase,
#endif
#if NEDERLANDS_en
ned_memory_erase,
#endif
#if DANSK_en
dan_memory_erase,
#endif
#if NORSK_en
nor_memory_erase,
#endif
#if SOOMI_en
soo_memory_erase,
#endif
#if SVENSKA_en
sve_memory_erase,
#endif
#if CESTINA_en
ces_memory_erase,
#endif
#if LIETUVIU_K_en
lie_memory_erase,
#endif
#if POLSKI_en
pol_memory_erase,
#endif
#if SLOVENCINA_en
slo_memory_erase,
#endif
#if SLOVENSCINA_en
sls_memory_erase,
#endif
#if TURKU_en
tur_memory_erase,
#endif
#if EESTI_en
ees_memory_erase,
#endif
#if LATVIESU_en
lat_memory_erase,
#endif
#if LIMBA_ROMANA_en
lim_memory_erase,
#endif
#if PUSSIJI_en
pus_memory_erase,
#endif
#if EAAHNIKA_en
eaa_memory_erase,
#endif
};
const char *date_list[] = {
#if ENGLISH_en
en_date,
#endif
#if FRANCAIS_en
fr_date,
#endif
#if DEUTSCH_en
de_date,
#endif
#if ESPANOL_en
es_date,
#endif
#if ITALIAN_en
it_date,
#endif
#if PORTUGUES_en
pt_date,
#endif
#if NEDERLANDS_en
ned_date,
#endif
#if DANSK_en
dan_date,
#endif
#if NORSK_en
nor_date,
#endif
#if SOOMI_en
soo_date,
#endif
#if SVENSKA_en
sve_date,
#endif
#if CESTINA_en
ces_date,
#endif
#if LIETUVIU_K_en
lie_date,
#endif
#if POLSKI_en
pol_date,
#endif
#if SLOVENCINA_en
slo_date,
#endif
#if SLOVENSCINA_en
sls_date,
#endif
#if TURKU_en
tur_date,
#endif
#if EESTI_en
ees_date,
#endif
#if LATVIESU_en
lat_date,
#endif
#if LIMBA_ROMANA_en
lim_date,
#endif
#if PUSSIJI_en
pus_date,
#endif
#if EAAHNIKA_en
eaa_date,
#endif
};
const char *time_list[] = {
#if ENGLISH_en
en_time,
#endif
#if FRANCAIS_en
fr_time,
#endif
#if DEUTSCH_en
de_time,
#endif
#if ESPANOL_en
es_time,
#endif
#if ITALIAN_en
it_time,
#endif
#if PORTUGUES_en
pt_time,
#endif
#if NEDERLANDS_en
ned_time,
#endif
#if DANSK_en
dan_time,
#endif
#if NORSK_en
nor_time,
#endif
#if SOOMI_en
soo_time,
#endif
#if SVENSKA_en
sve_time,
#endif
#if CESTINA_en
ces_time,
#endif
#if LIETUVIU_K_en
lie_time,
#endif
#if POLSKI_en
pol_time,
#endif
#if SLOVENCINA_en
slo_time,
#endif
#if SLOVENSCINA_en
sls_time,
#endif
#if TURKU_en
tur_time,
#endif
#if EESTI_en
ees_time,
#endif
#if LATVIESU_en
lat_time,
#endif
#if LIMBA_ROMANA_en
lim_time,
#endif
#if PUSSIJI_en
pus_time,
#endif
#if EAAHNIKA_en
eaa_time,
#endif
};
const char *invalid_vin_list[] = {
#if ENGLISH_en
en_invalid_vin,
#endif
#if FRANCAIS_en
fr_invalid_vin,
#endif
#if DEUTSCH_en
de_invalid_vin,
#endif
#if ESPANOL_en
es_invalid_vin,
#endif
#if ITALIAN_en
it_invalid_vin,
#endif
#if PORTUGUES_en
pt_invalid_vin,
#endif
#if NEDERLANDS_en
ned_invalid_vin,
#endif
#if DANSK_en
dan_invalid_vin,
#endif
#if NORSK_en
nor_invalid_vin,
#endif
#if SOOMI_en
soo_invalid_vin,
#endif
#if SVENSKA_en
sve_invalid_vin,
#endif
#if CESTINA_en
ces_invalid_vin,
#endif
#if LIETUVIU_K_en
lie_invalid_vin,
#endif
#if POLSKI_en
pol_invalid_vin,
#endif
#if SLOVENCINA_en
slo_invalid_vin,
#endif
#if SLOVENSCINA_en
sls_invalid_vin,
#endif
#if TURKU_en
tur_invalid_vin,
#endif
#if EESTI_en
ees_invalid_vin,
#endif
#if LATVIESU_en
lat_invalid_vin,
#endif
#if LIMBA_ROMANA_en
lim_invalid_vin,
#endif
#if PUSSIJI_en
pus_invalid_vin,
#endif
#if EAAHNIKA_en
eaa_invalid_vin,
#endif
};
const char *input_vin_list[] = {
#if ENGLISH_en
en_input_vin,
#endif
#if FRANCAIS_en
fr_input_vin,
#endif
#if DEUTSCH_en
de_input_vin,
#endif
#if ESPANOL_en
es_input_vin,
#endif
#if ITALIAN_en
it_input_vin,
#endif
#if PORTUGUES_en
pt_input_vin,
#endif
#if NEDERLANDS_en
ned_input_vin,
#endif
#if DANSK_en
dan_input_vin,
#endif
#if NORSK_en
nor_input_vin,
#endif
#if SOOMI_en
soo_input_vin,
#endif
#if SVENSKA_en
sve_input_vin,
#endif
#if CESTINA_en
ces_input_vin,
#endif
#if LIETUVIU_K_en
lie_input_vin,
#endif
#if POLSKI_en
pol_input_vin,
#endif
#if SLOVENCINA_en
slo_input_vin,
#endif
#if SLOVENSCINA_en
sls_input_vin,
#endif
#if TURKU_en
tur_input_vin,
#endif
#if EESTI_en
ees_input_vin,
#endif
#if LATVIESU_en
lat_input_vin,
#endif
#if LIMBA_ROMANA_en
lim_input_vin,
#endif
#if PUSSIJI_en
pus_input_vin,
#endif
#if EAAHNIKA_en
eaa_input_vin,
#endif
};
const char *enter_vin_list[] = {
#if ENGLISH_en
en_enter_vin,
#endif
#if FRANCAIS_en
fr_enter_vin,
#endif
#if DEUTSCH_en
de_enter_vin,
#endif
#if ESPANOL_en
es_enter_vin,
#endif
#if ITALIAN_en
it_enter_vin,
#endif
#if PORTUGUES_en
pt_enter_vin,
#endif
#if NEDERLANDS_en
ned_enter_vin,
#endif
#if DANSK_en
dan_enter_vin,
#endif
#if NORSK_en
nor_enter_vin,
#endif
#if SOOMI_en
soo_enter_vin,
#endif
#if SVENSKA_en
sve_enter_vin,
#endif
#if CESTINA_en
ces_enter_vin,
#endif
#if LIETUVIU_K_en
lie_enter_vin,
#endif
#if POLSKI_en
pol_enter_vin,
#endif
#if SLOVENCINA_en
slo_enter_vin,
#endif
#if SLOVENSCINA_en
sls_enter_vin,
#endif
#if TURKU_en
tur_enter_vin,
#endif
#if EESTI_en
ees_enter_vin,
#endif
#if LATVIESU_en
lat_enter_vin,
#endif
#if LIMBA_ROMANA_en
lim_enter_vin,
#endif
#if PUSSIJI_en
pus_enter_vin,
#endif
#if EAAHNIKA_en
eaa_enter_vin,
#endif
};
const char *cranking_voltage_list[] = {
#if ENGLISH_en
en_cranking_voltage,
#endif
#if FRANCAIS_en
fr_cranking_voltage,
#endif
#if DEUTSCH_en
de_cranking_voltage,
#endif
#if ESPANOL_en
es_cranking_voltage,
#endif
#if ITALIAN_en
it_cranking_voltage,
#endif
#if PORTUGUES_en
pt_cranking_voltage,
#endif
#if NEDERLANDS_en
ned_cranking_voltage,
#endif
#if DANSK_en
dan_cranking_voltage,
#endif
#if NORSK_en
nor_cranking_voltage,
#endif
#if SOOMI_en
soo_cranking_voltage,
#endif
#if SVENSKA_en
sve_cranking_voltage,
#endif
#if CESTINA_en
ces_cranking_voltage,
#endif
#if LIETUVIU_K_en
lie_cranking_voltage,
#endif
#if POLSKI_en
pol_cranking_voltage,
#endif
#if SLOVENCINA_en
slo_cranking_voltage,
#endif
#if SLOVENSCINA_en
sls_cranking_voltage,
#endif
#if TURKU_en
tur_cranking_voltage,
#endif
#if EESTI_en
ees_cranking_voltage,
#endif
#if LATVIESU_en
lat_cranking_voltage,
#endif
#if LIMBA_ROMANA_en
lim_cranking_voltage,
#endif
#if PUSSIJI_en
pus_cranking_voltage,
#endif
#if EAAHNIKA_en
eaa_cranking_voltage,
#endif
};
const char *lowest_voltage_list[] = {
#if ENGLISH_en
en_lowest_voltage,
#endif
#if FRANCAIS_en
fr_lowest_voltage,
#endif
#if DEUTSCH_en
de_lowest_voltage,
#endif
#if ESPANOL_en
es_lowest_voltage,
#endif
#if ITALIAN_en
it_lowest_voltage,
#endif
#if PORTUGUES_en
pt_lowest_voltage,
#endif
#if NEDERLANDS_en
ned_lowest_voltage,
#endif
#if DANSK_en
dan_lowest_voltage,
#endif
#if NORSK_en
nor_lowest_voltage,
#endif
#if SOOMI_en
soo_lowest_voltage,
#endif
#if SVENSKA_en
sve_lowest_voltage,
#endif
#if CESTINA_en
ces_lowest_voltage,
#endif
#if LIETUVIU_K_en
lie_lowest_voltage,
#endif
#if POLSKI_en
pol_lowest_voltage,
#endif
#if SLOVENCINA_en
slo_lowest_voltage,
#endif
#if SLOVENSCINA_en
sls_lowest_voltage,
#endif
#if TURKU_en
tur_lowest_voltage,
#endif
#if EESTI_en
ees_lowest_voltage,
#endif
#if LATVIESU_en
lat_lowest_voltage,
#endif
#if LIMBA_ROMANA_en
lim_lowest_voltage,
#endif
#if PUSSIJI_en
pus_lowest_voltage,
#endif
#if EAAHNIKA_en
eaa_lowest_voltage,
#endif
};
const char *idle_voltage_list[] = {
#if ENGLISH_en
en_idle_voltage,
#endif
#if FRANCAIS_en
fr_idle_voltage,
#endif
#if DEUTSCH_en
de_idle_voltage,
#endif
#if ESPANOL_en
es_idle_voltage,
#endif
#if ITALIAN_en
it_idle_voltage,
#endif
#if PORTUGUES_en
pt_idle_voltage,
#endif
#if NEDERLANDS_en
ned_idle_voltage,
#endif
#if DANSK_en
dan_idle_voltage,
#endif
#if NORSK_en
nor_idle_voltage,
#endif
#if SOOMI_en
soo_idle_voltage,
#endif
#if SVENSKA_en
sve_idle_voltage,
#endif
#if CESTINA_en
ces_idle_voltage,
#endif
#if LIETUVIU_K_en
lie_idle_voltage,
#endif
#if POLSKI_en
pol_idle_voltage,
#endif
#if SLOVENCINA_en
slo_idle_voltage,
#endif
#if SLOVENSCINA_en
sls_idle_voltage,
#endif
#if TURKU_en
tur_idle_voltage,
#endif
#if EESTI_en
ees_idle_voltage,
#endif
#if LATVIESU_en
lat_idle_voltage,
#endif
#if LIMBA_ROMANA_en
lim_idle_voltage,
#endif
#if PUSSIJI_en
pus_idle_voltage,
#endif
#if EAAHNIKA_en
eaa_idle_voltage,
#endif
};

const char *sec_list[] = {
#if ENGLISH_en
en_sec,
#endif
#if FRANCAIS_en
fr_sec,
#endif
#if DEUTSCH_en
de_sec,
#endif
#if ESPANOL_en
es_sec,
#endif
#if ITALIAN_en
it_sec,
#endif
#if PORTUGUES_en
pt_sec,
#endif
#if NEDERLANDS_en
ned_sec,
#endif
#if DANSK_en
dan_sec,
#endif
#if NORSK_en
nor_sec,
#endif
#if SOOMI_en
soo_sec,
#endif
#if SVENSKA_en
sve_sec,
#endif
#if CESTINA_en
ces_sec,
#endif
#if LIETUVIU_K_en
lie_sec,
#endif
#if POLSKI_en
pol_sec,
#endif
#if SLOVENCINA_en
slo_sec,
#endif
#if SLOVENSCINA_en
sls_sec,
#endif
#if TURKU_en
tur_sec,
#endif
#if EESTI_en
ees_sec,
#endif
#if LATVIESU_en
lat_sec,
#endif
#if LIMBA_ROMANA_en
lim_sec,
#endif
#if PUSSIJI_en
pus_sec,
#endif
#if EAAHNIKA_en
eaa_sec,
#endif
};
const char *ripple_detected_list[] = {
#if ENGLISH_en
en_ripple_detected,
#endif
#if FRANCAIS_en
fr_ripple_detected,
#endif
#if DEUTSCH_en
de_ripple_detected,
#endif
#if ESPANOL_en
es_ripple_detected,
#endif
#if ITALIAN_en
it_ripple_detected,
#endif
#if PORTUGUES_en
pt_ripple_detected,
#endif
#if NEDERLANDS_en
ned_ripple_detected,
#endif
#if DANSK_en
dan_ripple_detected,
#endif
#if NORSK_en
nor_ripple_detected,
#endif
#if SOOMI_en
soo_ripple_detected,
#endif
#if SVENSKA_en
sve_ripple_detected,
#endif
#if CESTINA_en
ces_ripple_detected,
#endif
#if LIETUVIU_K_en
lie_ripple_detected,
#endif
#if POLSKI_en
pol_ripple_detected,
#endif
#if SLOVENCINA_en
slo_ripple_detected,
#endif
#if SLOVENSCINA_en
sls_ripple_detected,
#endif
#if TURKU_en
tur_ripple_detected,
#endif
#if EESTI_en
ees_ripple_detected,
#endif
#if LATVIESU_en
lat_ripple_detected,
#endif
#if LIMBA_ROMANA_en
lim_ripple_detected,
#endif
#if PUSSIJI_en
pus_ripple_detected,
#endif
#if EAAHNIKA_en
eaa_ripple_detected,
#endif
};
const char *ripple_voltage_list[] = {
#if ENGLISH_en
en_ripple_voltage,
#endif
#if FRANCAIS_en
fr_ripple_voltage,
#endif
#if DEUTSCH_en
de_ripple_voltage,
#endif
#if ESPANOL_en
es_ripple_voltage,
#endif
#if ITALIAN_en
it_ripple_voltage,
#endif
#if PORTUGUES_en
pt_ripple_voltage,
#endif
#if NEDERLANDS_en
ned_ripple_voltage,
#endif
#if DANSK_en
dan_ripple_voltage,
#endif
#if NORSK_en
nor_ripple_voltage,
#endif
#if SOOMI_en
soo_ripple_voltage,
#endif
#if SVENSKA_en
sve_ripple_voltage,
#endif
#if CESTINA_en
ces_ripple_voltage,
#endif
#if LIETUVIU_K_en
lie_ripple_voltage,
#endif
#if POLSKI_en
pol_ripple_voltage,
#endif
#if SLOVENCINA_en
slo_ripple_voltage,
#endif
#if SLOVENSCINA_en
sls_ripple_voltage,
#endif
#if TURKU_en
tur_ripple_voltage,
#endif
#if EESTI_en
ees_ripple_voltage,
#endif
#if LATVIESU_en
lat_ripple_voltage,
#endif
#if LIMBA_ROMANA_en
lim_ripple_voltage,
#endif
#if PUSSIJI_en
pus_ripple_voltage,
#endif
#if EAAHNIKA_en
eaa_ripple_voltage,
#endif
};
const char *load_voltage_list[] = {
#if ENGLISH_en
en_load_voltage,
#endif
#if FRANCAIS_en
fr_load_voltage,
#endif
#if DEUTSCH_en
de_load_voltage,
#endif
#if ESPANOL_en
es_load_voltage,
#endif
#if ITALIAN_en
it_load_voltage,
#endif
#if PORTUGUES_en
pt_load_voltage,
#endif
#if NEDERLANDS_en
ned_load_voltage,
#endif
#if DANSK_en
dan_load_voltage,
#endif
#if NORSK_en
nor_load_voltage,
#endif
#if SOOMI_en
soo_load_voltage,
#endif
#if SVENSKA_en
sve_load_voltage,
#endif
#if CESTINA_en
ces_load_voltage,
#endif
#if LIETUVIU_K_en
lie_load_voltage,
#endif
#if POLSKI_en
pol_load_voltage,
#endif
#if SLOVENCINA_en
slo_load_voltage,
#endif
#if SLOVENSCINA_en
sls_load_voltage,
#endif
#if TURKU_en
tur_load_voltage,
#endif
#if EESTI_en
ees_load_voltage,
#endif
#if LATVIESU_en
lat_load_voltage,
#endif
#if LIMBA_ROMANA_en
lim_load_voltage,
#endif
#if PUSSIJI_en
pus_load_voltage,
#endif
#if EAAHNIKA_en
eaa_load_voltage,
#endif
};
const char *point_to_battery_list[] = {
#if ENGLISH_en
en_point_to_battery_press_enter,
#endif
#if FRANCAIS_en
fr_point_to_battery_press_enter,
#endif
#if DEUTSCH_en
de_point_to_battery_press_enter,
#endif
#if ESPANOL_en
es_point_to_battery_press_enter,
#endif
#if ITALIAN_en
it_point_to_battery_press_enter,
#endif
#if PORTUGUES_en
pt_point_to_battery_press_enter,
#endif
#if NEDERLANDS_en
ned_point_to_battery_press_enter,
#endif
#if DANSK_en
dan_point_to_battery_press_enter,
#endif
#if NORSK_en
nor_point_to_battery_press_enter,
#endif
#if SOOMI_en
soo_point_to_battery_press_enter,
#endif
#if SVENSKA_en
sve_point_to_battery_press_enter,
#endif
#if CESTINA_en
ces_point_to_battery_press_enter,
#endif
#if LIETUVIU_K_en
lie_point_to_battery_press_enter,
#endif
#if POLSKI_en
pol_point_to_battery_press_enter,
#endif
#if SLOVENCINA_en
slo_point_to_battery_press_enter,
#endif
#if SLOVENSCINA_en
sls_point_to_battery_press_enter,
#endif
#if TURKU_en
tur_point_to_battery_press_enter,
#endif
#if EESTI_en
ees_point_to_battery_press_enter,
#endif
#if LATVIESU_en
lat_point_to_battery_press_enter,
#endif
#if LIMBA_ROMANA_en
lim_point_to_battery_press_enter,
#endif
#if PUSSIJI_en
pus_point_to_battery_press_enter,
#endif
#if EAAHNIKA_en
eaa_point_to_battery_press_enter,
#endif
};
const char *ota_list[] = {
#if ENGLISH_en
en_ota,
#endif
#if FRANCAIS_en
fr_ota,
#endif
#if DEUTSCH_en
de_ota,
#endif
#if ESPANOL_en
es_ota,
#endif
#if ITALIAN_en
it_ota,
#endif
#if PORTUGUES_en
pt_ota,
#endif
#if NEDERLANDS_en
ned_ota,
#endif
#if DANSK_en
dan_ota,
#endif
#if NORSK_en
nor_ota,
#endif
#if SOOMI_en
soo_ota,
#endif
#if SVENSKA_en
sve_ota,
#endif
#if CESTINA_en
ces_ota,
#endif
#if LIETUVIU_K_en
lie_ota,
#endif
#if POLSKI_en
pol_ota,
#endif
#if SLOVENCINA_en
slo_ota,
#endif
#if SLOVENSCINA_en
sls_ota,
#endif
#if TURKU_en
tur_ota,
#endif
#if EESTI_en
ees_ota,
#endif
#if LATVIESU_en
lat_ota,
#endif
#if LIMBA_ROMANA_en
lim_ota,
#endif
#if PUSSIJI_en
pus_ota,
#endif
#if EAAHNIKA_en
eaa_ota,
#endif
};
const char *connection_status_list[] = {
#if ENGLISH_en
en_connection_status,
#endif
#if FRANCAIS_en
fr_connection_status,
#endif
#if DEUTSCH_en
de_connection_status,
#endif
#if ESPANOL_en
es_connection_status,
#endif
#if ITALIAN_en
it_connection_status,
#endif
#if PORTUGUES_en
pt_connection_status,
#endif
#if NEDERLANDS_en
ned_connection_status,
#endif
#if DANSK_en
dan_connection_status,
#endif
#if NORSK_en
nor_connection_status,
#endif
#if SOOMI_en
soo_connection_status,
#endif
#if SVENSKA_en
sve_connection_status,
#endif
#if CESTINA_en
ces_connection_status,
#endif
#if LIETUVIU_K_en
lie_connection_status,
#endif
#if POLSKI_en
pol_connection_status,
#endif
#if SLOVENCINA_en
slo_connection_status,
#endif
#if SLOVENSCINA_en
sls_connection_status,
#endif
#if TURKU_en
tur_connection_status,
#endif
#if EESTI_en
ees_connection_status,
#endif
#if LATVIESU_en
lat_connection_status,
#endif
#if LIMBA_ROMANA_en
lim_connection_status,
#endif
#if PUSSIJI_en
pus_connection_status,
#endif
#if EAAHNIKA_en
eaa_connection_status,
#endif
};
const char *scan_vin_code_list[] = {
#if ENGLISH_en
en_scan_vin_code,
#endif
#if FRANCAIS_en
fr_scan_vin_code,
#endif
#if DEUTSCH_en
de_scan_vin_code,
#endif
#if ESPANOL_en
es_scan_vin_code,
#endif
#if ITALIAN_en
it_scan_vin_code,
#endif
#if PORTUGUES_en
pt_scan_vin_code,
#endif
#if NEDERLANDS_en
ned_scan_vin_code,
#endif
#if DANSK_en
dan_scan_vin_code,
#endif
#if NORSK_en
nor_scan_vin_code,
#endif
#if SOOMI_en
soo_scan_vin_code,
#endif
#if SVENSKA_en
sve_scan_vin_code,
#endif
#if CESTINA_en
ces_scan_vin_code,
#endif
#if LIETUVIU_K_en
lie_scan_vin_code,
#endif
#if POLSKI_en
pol_scan_vin_code,
#endif
#if SLOVENCINA_en
slo_scan_vin_code,
#endif
#if SLOVENSCINA_en
sls_scan_vin_code,
#endif
#if TURKU_en
tur_scan_vin_code,
#endif
#if EESTI_en
ees_scan_vin_code,
#endif
#if LATVIESU_en
lat_scan_vin_code,
#endif
#if LIMBA_ROMANA_en
lim_scan_vin_code,
#endif
#if PUSSIJI_en
pus_scan_vin_code,
#endif
#if EAAHNIKA_en
eaa_scan_vin_code,
#endif
};
const char *vin_code_list[] = {
#if ENGLISH_en
en_vin_code,
#endif
#if FRANCAIS_en
fr_vin_code,
#endif
#if DEUTSCH_en
de_vin_code,
#endif
#if ESPANOL_en
es_vin_code,
#endif
#if ITALIAN_en
it_vin_code,
#endif
#if PORTUGUES_en
pt_vin_code,
#endif
#if NEDERLANDS_en
ned_vin_code,
#endif
#if DANSK_en
dan_vin_code,
#endif
#if NORSK_en
nor_vin_code,
#endif
#if SOOMI_en
soo_vin_code,
#endif
#if SVENSKA_en
sve_vin_code,
#endif
#if CESTINA_en
ces_vin_code,
#endif
#if LIETUVIU_K_en
lie_vin_code,
#endif
#if POLSKI_en
pol_vin_code,
#endif
#if SLOVENCINA_en
slo_vin_code,
#endif
#if SLOVENSCINA_en
sls_vin_code,
#endif
#if TURKU_en
tur_vin_code,
#endif
#if EESTI_en
ees_vin_code,
#endif
#if LATVIESU_en
lat_vin_code,
#endif
#if LIMBA_ROMANA_en
lim_vin_code,
#endif
#if PUSSIJI_en
pus_vin_code,
#endif
#if EAAHNIKA_en
eaa_vin_code,
#endif
};
const char *invalid_vin_rescan_list[] = {
#if ENGLISH_en
en_invalid_vin_rescan,
#endif
#if FRANCAIS_en
fr_invalid_vin_rescan,
#endif
#if DEUTSCH_en
de_invalid_vin_rescan,
#endif
#if ESPANOL_en
es_invalid_vin_rescan,
#endif
#if ITALIAN_en
it_invalid_vin_rescan,
#endif
#if PORTUGUES_en
pt_invalid_vin_rescan,
#endif
#if NEDERLANDS_en
ned_invalid_vin_rescan,
#endif
#if DANSK_en
dan_invalid_vin_rescan,
#endif
#if NORSK_en
nor_invalid_vin_rescan,
#endif
#if SOOMI_en
soo_invalid_vin_rescan,
#endif
#if SVENSKA_en
sve_invalid_vin_rescan,
#endif
#if CESTINA_en
ces_invalid_vin_rescan,
#endif
#if LIETUVIU_K_en
lie_invalid_vin_rescan,
#endif
#if POLSKI_en
pol_invalid_vin_rescan,
#endif
#if SLOVENCINA_en
slo_invalid_vin_rescan,
#endif
#if SLOVENSCINA_en
sls_invalid_vin_rescan,
#endif
#if TURKU_en
tur_invalid_vin_rescan,
#endif
#if EESTI_en
ees_invalid_vin_rescan,
#endif
#if LATVIESU_en
lat_invalid_vin_rescan,
#endif
#if LIMBA_ROMANA_en
lim_invalid_vin_rescan,
#endif
#if PUSSIJI_en
pus_invalid_vin_rescan,
#endif
#if EAAHNIKA_en
eaa_invalid_vin_rescan,
#endif
};

const char *wifi_list[] = {
#if ENGLISH_en
en_wifi,
#endif
#if FRANCAIS_en
en_wifi,
#endif
#if DEUTSCH_en
en_wifi,
#endif
#if ESPANOL_en
en_wifi,
#endif
#if ITALIAN_en
en_wifi,
#endif
#if PORTUGUES_en
en_wifi,
#endif
#if NEDERLANDS_en
en_wifi,
#endif
#if DANSK_en
en_wifi,
#endif
#if NORSK_en
en_wifi,
#endif
#if SOOMI_en
en_wifi,
#endif
#if SVENSKA_en
en_wifi,
#endif
#if CESTINA_en
en_wifi,
#endif
#if LIETUVIU_K_en
en_wifi,
#endif
#if POLSKI_en
en_wifi,
#endif
#if SLOVENCINA_en
en_wifi,
#endif
#if SLOVENSCINA_en
en_wifi,
#endif
#if TURKU_en
en_wifi,
#endif
#if EESTI_en
en_wifi,
#endif
#if LATVIESU_en
en_wifi,
#endif
#if LIMBA_ROMANA_en
en_wifi,
#endif
#if PUSSIJI_en
en_wifi,
#endif
#if EAAHNIKA_en
en_wifi,
#endif
};


const char *wifi_power_list[] = {
#if ENGLISH_en
en_wifi_power,
#endif
#if FRANCAIS_en
fr_wifi_power,
#endif
#if DEUTSCH_en
de_wifi_power,
#endif
#if ESPANOL_en
es_wifi_power,
#endif
#if ITALIAN_en
it_wifi_power,
#endif
#if PORTUGUES_en
pt_wifi_power,
#endif
#if NEDERLANDS_en
ned_wifi_power,
#endif
#if DANSK_en
dan_wifi_power,
#endif
#if NORSK_en
nor_wifi_power,
#endif
#if SOOMI_en
soo_wifi_power,
#endif
#if SVENSKA_en
sve_wifi_power,
#endif
#if CESTINA_en
ces_wifi_power,
#endif
#if LIETUVIU_K_en
lie_wifi_power,
#endif
#if POLSKI_en
pol_wifi_power,
#endif
#if SLOVENCINA_en
slo_wifi_power,
#endif
#if SLOVENSCINA_en
sls_wifi_power,
#endif
#if TURKU_en
tur_wifi_power,
#endif
#if EESTI_en
ees_wifi_power,
#endif
#if LATVIESU_en
lat_wifi_power,
#endif
#if LIMBA_ROMANA_en
lim_wifi_power,
#endif
#if PUSSIJI_en
pus_wifi_power,
#endif
#if EAAHNIKA_en
eaa_wifi_power,
#endif
};
const char *wifi_list_list[] = {
#if ENGLISH_en
en_wifi_list,
#endif
#if FRANCAIS_en
fr_wifi_list,
#endif
#if DEUTSCH_en
de_wifi_list,
#endif
#if ESPANOL_en
es_wifi_list,
#endif
#if ITALIAN_en
it_wifi_list,
#endif
#if PORTUGUES_en
pt_wifi_list,
#endif
#if NEDERLANDS_en
ned_wifi_list,
#endif
#if DANSK_en
dan_wifi_list,
#endif
#if NORSK_en
nor_wifi_list,
#endif
#if SOOMI_en
soo_wifi_list,
#endif
#if SVENSKA_en
sve_wifi_list,
#endif
#if CESTINA_en
ces_wifi_list,
#endif
#if LIETUVIU_K_en
lie_wifi_list,
#endif
#if POLSKI_en
pol_wifi_list,
#endif
#if SLOVENCINA_en
slo_wifi_list,
#endif
#if SLOVENSCINA_en
sls_wifi_list,
#endif
#if TURKU_en
tur_wifi_list,
#endif
#if EESTI_en
ees_wifi_list,
#endif
#if LATVIESU_en
lat_wifi_list,
#endif
#if LIMBA_ROMANA_en
lim_wifi_list,
#endif
#if PUSSIJI_en
pus_wifi_list,
#endif
#if EAAHNIKA_en
eaa_wifi_list,
#endif
};
const char *downloading_list[] = {
#if ENGLISH_en
en_downloading,
#endif
#if FRANCAIS_en
fr_downloading,
#endif
#if DEUTSCH_en
de_downloading,
#endif
#if ESPANOL_en
es_downloading,
#endif
#if ITALIAN_en
it_downloading,
#endif
#if PORTUGUES_en
pt_downloading,
#endif
#if NEDERLANDS_en
ned_downloading,
#endif
#if DANSK_en
dan_downloading,
#endif
#if NORSK_en
nor_downloading,
#endif
#if SOOMI_en
soo_downloading,
#endif
#if SVENSKA_en
sve_downloading,
#endif
#if CESTINA_en
ces_downloading,
#endif
#if LIETUVIU_K_en
lie_downloading,
#endif
#if POLSKI_en
pol_downloading,
#endif
#if SLOVENCINA_en
slo_downloading,
#endif
#if SLOVENSCINA_en
sls_downloading,
#endif
#if TURKU_en
tur_downloading,
#endif
#if EESTI_en
ees_downloading,
#endif
#if LATVIESU_en
lat_downloading,
#endif
#if LIMBA_ROMANA_en
lim_downloading,
#endif
#if PUSSIJI_en
pus_downloading,
#endif
#if EAAHNIKA_en
eaa_downloading,
#endif
};
const char *upgrade_cancel_list[] = {
#if ENGLISH_en
en_upgrade_cancel,
#endif
#if FRANCAIS_en
fr_upgrade_cancel,
#endif
#if DEUTSCH_en
de_upgrade_cancel,
#endif
#if ESPANOL_en
es_upgrade_cancel,
#endif
#if ITALIAN_en
it_upgrade_cancel,
#endif
#if PORTUGUES_en
pt_upgrade_cancel,
#endif
#if NEDERLANDS_en
ned_upgrade_cancel,
#endif
#if DANSK_en
dan_upgrade_cancel,
#endif
#if NORSK_en
nor_upgrade_cancel,
#endif
#if SOOMI_en
soo_upgrade_cancel,
#endif
#if SVENSKA_en
sve_upgrade_cancel,
#endif
#if CESTINA_en
ces_upgrade_cancel,
#endif
#if LIETUVIU_K_en
lie_upgrade_cancel,
#endif
#if POLSKI_en
pol_upgrade_cancel,
#endif
#if SLOVENCINA_en
slo_upgrade_cancel,
#endif
#if SLOVENSCINA_en
sls_upgrade_cancel,
#endif
#if TURKU_en
tur_upgrade_cancel,
#endif
#if EESTI_en
ees_upgrade_cancel,
#endif
#if LATVIESU_en
lat_upgrade_cancel,
#endif
#if LIMBA_ROMANA_en
lim_upgrade_cancel,
#endif
#if PUSSIJI_en
pus_upgrade_cancel,
#endif
#if EAAHNIKA_en
eaa_upgrade_cancel,
#endif
};
const char *upgrading_list[] = {
#if ENGLISH_en
en_upgrading,
#endif
#if FRANCAIS_en
fr_upgrading,
#endif
#if DEUTSCH_en
de_upgrading,
#endif
#if ESPANOL_en
es_upgrading,
#endif
#if ITALIAN_en
it_upgrading,
#endif
#if PORTUGUES_en
pt_upgrading,
#endif
#if NEDERLANDS_en
ned_upgrading,
#endif
#if DANSK_en
dan_upgrading,
#endif
#if NORSK_en
nor_upgrading,
#endif
#if SOOMI_en
soo_upgrading,
#endif
#if SVENSKA_en
sve_upgrading,
#endif
#if CESTINA_en
ces_upgrading,
#endif
#if LIETUVIU_K_en
lie_upgrading,
#endif
#if POLSKI_en
pol_upgrading,
#endif
#if SLOVENCINA_en
slo_upgrading,
#endif
#if SLOVENSCINA_en
sls_upgrading,
#endif
#if TURKU_en
tur_upgrading,
#endif
#if EESTI_en
ees_upgrading,
#endif
#if LATVIESU_en
lat_upgrading,
#endif
#if LIMBA_ROMANA_en
lim_upgrading,
#endif
#if PUSSIJI_en
pus_upgrading,
#endif
#if EAAHNIKA_en
eaa_upgrading,
#endif
};
const char *check_version_list[] = {
#if ENGLISH_en
en_check_version,
#endif
#if FRANCAIS_en
fr_check_version,
#endif
#if DEUTSCH_en
de_check_version,
#endif
#if ESPANOL_en
es_check_version,
#endif
#if ITALIAN_en
it_check_version,
#endif
#if PORTUGUES_en
pt_check_version,
#endif
#if NEDERLANDS_en
ned_check_version,
#endif
#if DANSK_en
dan_check_version,
#endif
#if NORSK_en
nor_check_version,
#endif
#if SOOMI_en
soo_check_version,
#endif
#if SVENSKA_en
sve_check_version,
#endif
#if CESTINA_en
ces_check_version,
#endif
#if LIETUVIU_K_en
lie_check_version,
#endif
#if POLSKI_en
pol_check_version,
#endif
#if SLOVENCINA_en
slo_check_version,
#endif
#if SLOVENSCINA_en
sls_check_version,
#endif
#if TURKU_en
tur_check_version,
#endif
#if EESTI_en
ees_check_version,
#endif
#if LATVIESU_en
lat_check_version,
#endif
#if LIMBA_ROMANA_en
lim_check_version,
#endif
#if PUSSIJI_en
pus_check_version,
#endif
#if EAAHNIKA_en
eaa_check_version,
#endif
};
const char *limited_connection_list[] = {
#if ENGLISH_en
en_limited_connection,
#endif
#if FRANCAIS_en
fr_limited_connection,
#endif
#if DEUTSCH_en
de_limited_connection,
#endif
#if ESPANOL_en
es_limited_connection,
#endif
#if ITALIAN_en
it_limited_connection,
#endif
#if PORTUGUES_en
pt_limited_connection,
#endif
#if NEDERLANDS_en
ned_limited_connection,
#endif
#if DANSK_en
dan_limited_connection,
#endif
#if NORSK_en
nor_limited_connection,
#endif
#if SOOMI_en
soo_limited_connection,
#endif
#if SVENSKA_en
sve_limited_connection,
#endif
#if CESTINA_en
ces_limited_connection,
#endif
#if LIETUVIU_K_en
lie_limited_connection,
#endif
#if POLSKI_en
pol_limited_connection,
#endif
#if SLOVENCINA_en
slo_limited_connection,
#endif
#if SLOVENSCINA_en
sls_limited_connection,
#endif
#if TURKU_en
tur_limited_connection,
#endif
#if EESTI_en
ees_limited_connection,
#endif
#if LATVIESU_en
lat_limited_connection,
#endif
#if LIMBA_ROMANA_en
lim_limited_connection,
#endif
#if PUSSIJI_en
pus_limited_connection,
#endif
#if EAAHNIKA_en
eaa_limited_connection,
#endif
};
const char *wifi_connected_list[] = {
#if ENGLISH_en
en_wifi_connected,
#endif
#if FRANCAIS_en
fr_wifi_connected,
#endif
#if DEUTSCH_en
de_wifi_connected,
#endif
#if ESPANOL_en
es_wifi_connected,
#endif
#if ITALIAN_en
it_wifi_connected,
#endif
#if PORTUGUES_en
pt_wifi_connected,
#endif
#if NEDERLANDS_en
ned_wifi_connected,
#endif
#if DANSK_en
dan_wifi_connected,
#endif
#if NORSK_en
nor_wifi_connected,
#endif
#if SOOMI_en
soo_wifi_connected,
#endif
#if SVENSKA_en
sve_wifi_connected,
#endif
#if CESTINA_en
ces_wifi_connected,
#endif
#if LIETUVIU_K_en
lie_wifi_connected,
#endif
#if POLSKI_en
pol_wifi_connected,
#endif
#if SLOVENCINA_en
slo_wifi_connected,
#endif
#if SLOVENSCINA_en
sls_wifi_connected,
#endif
#if TURKU_en
tur_wifi_connected,
#endif
#if EESTI_en
ees_wifi_connected,
#endif
#if LATVIESU_en
lat_wifi_connected,
#endif
#if LIMBA_ROMANA_en
lim_wifi_connected,
#endif
#if PUSSIJI_en
pus_wifi_connected,
#endif
#if EAAHNIKA_en
eaa_wifi_connected,
#endif
};
const char *connecting_list[] = {
#if ENGLISH_en
en_connecting,
#endif
#if FRANCAIS_en
fr_connecting,
#endif
#if DEUTSCH_en
de_connecting,
#endif
#if ESPANOL_en
es_connecting,
#endif
#if ITALIAN_en
it_connecting,
#endif
#if PORTUGUES_en
pt_connecting,
#endif
#if NEDERLANDS_en
ned_connecting,
#endif
#if DANSK_en
dan_connecting,
#endif
#if NORSK_en
nor_connecting,
#endif
#if SOOMI_en
soo_connecting,
#endif
#if SVENSKA_en
sve_connecting,
#endif
#if CESTINA_en
ces_connecting,
#endif
#if LIETUVIU_K_en
lie_connecting,
#endif
#if POLSKI_en
pol_connecting,
#endif
#if SLOVENCINA_en
slo_connecting,
#endif
#if SLOVENSCINA_en
sls_connecting,
#endif
#if TURKU_en
tur_connecting,
#endif
#if EESTI_en
ees_connecting,
#endif
#if LATVIESU_en
lat_connecting,
#endif
#if LIMBA_ROMANA_en
lim_connecting,
#endif
#if PUSSIJI_en
pus_connecting,
#endif
#if EAAHNIKA_en
eaa_connecting,
#endif
};
const char *wifi_disconnect_list[] = {
#if ENGLISH_en
en_wifi_disconnect,
#endif
#if FRANCAIS_en
fr_wifi_disconnect,
#endif
#if DEUTSCH_en
de_wifi_disconnect,
#endif
#if ESPANOL_en
es_wifi_disconnect,
#endif
#if ITALIAN_en
it_wifi_disconnect,
#endif
#if PORTUGUES_en
pt_wifi_disconnect,
#endif
#if NEDERLANDS_en
ned_wifi_disconnect,
#endif
#if DANSK_en
dan_wifi_disconnect,
#endif
#if NORSK_en
nor_wifi_disconnect,
#endif
#if SOOMI_en
soo_wifi_disconnect,
#endif
#if SVENSKA_en
sve_wifi_disconnect,
#endif
#if CESTINA_en
ces_wifi_disconnect,
#endif
#if LIETUVIU_K_en
lie_wifi_disconnect,
#endif
#if POLSKI_en
pol_wifi_disconnect,
#endif
#if SLOVENCINA_en
slo_wifi_disconnect,
#endif
#if SLOVENSCINA_en
sls_wifi_disconnect,
#endif
#if TURKU_en
tur_wifi_disconnect,
#endif
#if EESTI_en
ees_wifi_disconnect,
#endif
#if LATVIESU_en
lat_wifi_disconnect,
#endif
#if LIMBA_ROMANA_en
lim_wifi_disconnect,
#endif
#if PUSSIJI_en
pus_wifi_disconnect,
#endif
#if EAAHNIKA_en
eaa_wifi_disconnect,
#endif
};
const char *check_connection_list[] = {
#if ENGLISH_en
en_check_connection,
#endif
#if FRANCAIS_en
fr_check_connection,
#endif
#if DEUTSCH_en
de_check_connection,
#endif
#if ESPANOL_en
es_check_connection,
#endif
#if ITALIAN_en
it_check_connection,
#endif
#if PORTUGUES_en
pt_check_connection,
#endif
#if NEDERLANDS_en
ned_check_connection,
#endif
#if DANSK_en
dan_check_connection,
#endif
#if NORSK_en
nor_check_connection,
#endif
#if SOOMI_en
soo_check_connection,
#endif
#if SVENSKA_en
sve_check_connection,
#endif
#if CESTINA_en
ces_check_connection,
#endif
#if LIETUVIU_K_en
lie_check_connection,
#endif
#if POLSKI_en
pol_check_connection,
#endif
#if SLOVENCINA_en
slo_check_connection,
#endif
#if SLOVENSCINA_en
sls_check_connection,
#endif
#if TURKU_en
tur_check_connection,
#endif
#if EESTI_en
ees_check_connection,
#endif
#if LATVIESU_en
lat_check_connection,
#endif
#if LIMBA_ROMANA_en
lim_check_connection,
#endif
#if PUSSIJI_en
pus_check_connection,
#endif
#if EAAHNIKA_en
eaa_check_connection,
#endif
};
const char *keep_clamps_connect_list[] = {
#if ENGLISH_en
en_keep_clamps_connect,
#endif
#if FRANCAIS_en
fr_keep_clamps_connect,
#endif
#if DEUTSCH_en
de_keep_clamps_connect,
#endif
#if ESPANOL_en
es_keep_clamps_connect,
#endif
#if ITALIAN_en
it_keep_clamps_connect,
#endif
#if PORTUGUES_en
pt_keep_clamps_connect,
#endif
#if NEDERLANDS_en
ned_keep_clamps_connect,
#endif
#if DANSK_en
dan_keep_clamps_connect,
#endif
#if NORSK_en
nor_keep_clamps_connect,
#endif
#if SOOMI_en
soo_keep_clamps_connect,
#endif
#if SVENSKA_en
sve_keep_clamps_connect,
#endif
#if CESTINA_en
ces_keep_clamps_connect,
#endif
#if LIETUVIU_K_en
lie_keep_clamps_connect,
#endif
#if POLSKI_en
pol_keep_clamps_connect,
#endif
#if SLOVENCINA_en
slo_keep_clamps_connect,
#endif
#if SLOVENSCINA_en
sls_keep_clamps_connect,
#endif
#if TURKU_en
tur_keep_clamps_connect,
#endif
#if EESTI_en
ees_keep_clamps_connect,
#endif
#if LATVIESU_en
lat_keep_clamps_connect,
#endif
#if LIMBA_ROMANA_en
lim_keep_clamps_connect,
#endif
#if PUSSIJI_en
pus_keep_clamps_connect,
#endif
#if EAAHNIKA_en
eaa_keep_clamps_connect,
#endif
};

const char *prt_battery_test[] = {
#if ENGLISH_en
en_prt_battery_test,
#endif
#if FRANCAIS_en
fr_prt_battery_test,
#endif
#if DEUTSCH_en
de_prt_battery_test,
#endif
#if ESPANOL_en
es_prt_battery_test,
#endif
#if ITALIAN_en
it_prt_battery_test,
#endif
#if PORTUGUES_en
pt_prt_battery_test,
#endif
#if NEDERLANDS_en
ned_prt_battery_test,
#endif
#if DANSK_en
dan_prt_battery_test,
#endif
#if NORSK_en
nor_prt_battery_test,
#endif
#if SOOMI_en
soo_prt_battery_test,
#endif
#if SVENSKA_en
sve_prt_battery_test,
#endif
#if CESTINA_en
ces_prt_battery_test,
#endif
#if LIETUVIU_K_en
lie_prt_battery_test,
#endif
#if POLSKI_en
pol_prt_battery_test,
#endif
#if SLOVENCINA_en
slo_prt_battery_test,
#endif
#if SLOVENSCINA_en
sls_prt_battery_test,
#endif
#if TURKU_en
tur_prt_battery_test,
#endif
#if EESTI_en
ees_prt_battery_test,
#endif
#if LATVIESU_en
lat_prt_battery_test,
#endif
#if LIMBA_ROMANA_en
lim_prt_battery_test,
#endif
#if PUSSIJI_en
pus_prt_battery_test,
#endif
#if EAAHNIKA_en
eaa_prt_battery_test,
#endif
};
const char *bat_test_list[] = {
#if ENGLISH_en
en_bat_test,
#endif
#if FRANCAIS_en
fr_bat_test,
#endif
#if DEUTSCH_en
de_bat_test,
#endif
#if ESPANOL_en
es_bat_test,
#endif
#if ITALIAN_en
it_bat_test,
#endif
#if PORTUGUES_en
pt_bat_test,
#endif
#if NEDERLANDS_en
ned_bat_test,
#endif
#if DANSK_en
dan_bat_test,
#endif
#if NORSK_en
nor_bat_test,
#endif
#if SOOMI_en
soo_bat_test,
#endif
#if SVENSKA_en
sve_bat_test,
#endif
#if CESTINA_en
ces_bat_test,
#endif
#if LIETUVIU_K_en
lie_bat_test,
#endif
#if POLSKI_en
pol_bat_test,
#endif
#if SLOVENCINA_en
slo_bat_test,
#endif
#if SLOVENSCINA_en
sls_bat_test,
#endif
#if TURKU_en
tur_bat_test,
#endif
#if EESTI_en
ees_bat_test,
#endif
#if LATVIESU_en
lat_bat_test,
#endif
#if LIMBA_ROMANA_en
lim_bat_test,
#endif
#if PUSSIJI_en
pus_bat_test,
#endif
#if EAAHNIKA_en
eaa_bat_test,
#endif
};
const char *bad_cell_replace_list[] = {
#if ENGLISH_en
en_bad_cell_replace,
#endif
#if FRANCAIS_en
fr_bad_cell_replace,
#endif
#if DEUTSCH_en
de_bad_cell_replace,
#endif
#if ESPANOL_en
es_bad_cell_replace,
#endif
#if ITALIAN_en
it_bad_cell_replace,
#endif
#if PORTUGUES_en
pt_bad_cell_replace,
#endif
#if NEDERLANDS_en
ned_bad_cell_replace,
#endif
#if DANSK_en
dan_bad_cell_replace,
#endif
#if NORSK_en
nor_bad_cell_replace,
#endif
#if SOOMI_en
soo_bad_cell_replace,
#endif
#if SVENSKA_en
sve_bad_cell_replace,
#endif
#if CESTINA_en
ces_bad_cell_replace,
#endif
#if LIETUVIU_K_en
lie_bad_cell_replace,
#endif
#if POLSKI_en
pol_bad_cell_replace,
#endif
#if SLOVENCINA_en
slo_bad_cell_replace,
#endif
#if SLOVENSCINA_en
sls_bad_cell_replace,
#endif
#if TURKU_en
tur_bad_cell_replace,
#endif
#if EESTI_en
ees_bad_cell_replace,
#endif
#if LATVIESU_en
lat_bad_cell_replace,
#endif
#if LIMBA_ROMANA_en
lim_bad_cell_replace,
#endif
#if PUSSIJI_en
pus_bad_cell_replace,
#endif
#if EAAHNIKA_en
eaa_bad_cell_replace,
#endif
};
const char *bad_n_replace_list[] = {
#if ENGLISH_en
en_bad_n_replace,
#endif
#if FRANCAIS_en
fr_bad_n_replace,
#endif
#if DEUTSCH_en
de_bad_n_replace,
#endif
#if ESPANOL_en
es_bad_n_replace,
#endif
#if ITALIAN_en
it_bad_n_replace,
#endif
#if PORTUGUES_en
pt_bad_n_replace,
#endif
#if NEDERLANDS_en
ned_bad_n_replace,
#endif
#if DANSK_en
dan_bad_n_replace,
#endif
#if NORSK_en
nor_bad_n_replace,
#endif
#if SOOMI_en
soo_bad_n_replace,
#endif
#if SVENSKA_en
sve_bad_n_replace,
#endif
#if CESTINA_en
ces_bad_n_replace,
#endif
#if LIETUVIU_K_en
lie_bad_n_replace,
#endif
#if POLSKI_en
pol_bad_n_replace,
#endif
#if SLOVENCINA_en
slo_bad_n_replace,
#endif
#if SLOVENSCINA_en
sls_bad_n_replace,
#endif
#if TURKU_en
tur_bad_n_replace,
#endif
#if EESTI_en
ees_bad_n_replace,
#endif
#if LATVIESU_en
lat_bad_n_replace,
#endif
#if LIMBA_ROMANA_en
lim_bad_n_replace,
#endif
#if PUSSIJI_en
pus_bad_n_replace,
#endif
#if EAAHNIKA_en
eaa_bad_n_replace,
#endif
};
const char *prt_by[] = {
#if ENGLISH_en
en_prt_by,
#endif
#if FRANCAIS_en
fr_prt_by,
#endif
#if DEUTSCH_en
de_prt_by,
#endif
#if ESPANOL_en
es_prt_by,
#endif
#if ITALIAN_en
it_prt_by,
#endif
#if PORTUGUES_en
pt_prt_by,
#endif
#if NEDERLANDS_en
ned_prt_by,
#endif
#if DANSK_en
dan_prt_by,
#endif
#if NORSK_en
nor_prt_by,
#endif
#if SOOMI_en
soo_prt_by,
#endif
#if SVENSKA_en
sve_prt_by,
#endif
#if CESTINA_en
ces_prt_by,
#endif
#if LIETUVIU_K_en
lie_prt_by,
#endif
#if POLSKI_en
pol_prt_by,
#endif
#if SLOVENCINA_en
slo_prt_by,
#endif
#if SLOVENSCINA_en
sls_prt_by,
#endif
#if TURKU_en
tur_prt_by,
#endif
#if EESTI_en
ees_prt_by,
#endif
#if LATVIESU_en
lat_prt_by,
#endif
#if LIMBA_ROMANA_en
lim_prt_by,
#endif
#if PUSSIJI_en
pus_prt_by,
#endif
#if EAAHNIKA_en
eaa_prt_by,
#endif
};
const char *caution_list[] = {
#if ENGLISH_en
en_caution,
#endif
#if FRANCAIS_en
fr_caution,
#endif
#if DEUTSCH_en
de_caution,
#endif
#if ESPANOL_en
es_caution,
#endif
#if ITALIAN_en
it_caution,
#endif
#if PORTUGUES_en
pt_caution,
#endif
#if NEDERLANDS_en
ned_caution,
#endif
#if DANSK_en
dan_caution,
#endif
#if NORSK_en
nor_caution,
#endif
#if SOOMI_en
soo_caution,
#endif
#if SVENSKA_en
sve_caution,
#endif
#if CESTINA_en
ces_caution,
#endif
#if LIETUVIU_K_en
lie_caution,
#endif
#if POLSKI_en
pol_caution,
#endif
#if SLOVENCINA_en
slo_caution,
#endif
#if SLOVENSCINA_en
sls_caution,
#endif
#if TURKU_en
tur_caution,
#endif
#if EESTI_en
ees_caution,
#endif
#if LATVIESU_en
lat_caution,
#endif
#if LIMBA_ROMANA_en
lim_caution,
#endif
#if PUSSIJI_en
pus_caution,
#endif
#if EAAHNIKA_en
eaa_caution,
#endif
};
const char *prt_charging_test[] = {
#if ENGLISH_en
en_prt_charging_test,
#endif
#if FRANCAIS_en
fr_prt_charging_test,
#endif
#if DEUTSCH_en
de_prt_charging_test,
#endif
#if ESPANOL_en
es_prt_charging_test,
#endif
#if ITALIAN_en
it_prt_charging_test,
#endif
#if PORTUGUES_en
pt_prt_charging_test,
#endif
#if NEDERLANDS_en
ned_prt_charging_test,
#endif
#if DANSK_en
dan_prt_charging_test,
#endif
#if NORSK_en
nor_prt_charging_test,
#endif
#if SOOMI_en
soo_prt_charging_test,
#endif
#if SVENSKA_en
sve_prt_charging_test,
#endif
#if CESTINA_en
ces_prt_charging_test,
#endif
#if LIETUVIU_K_en
lie_prt_charging_test,
#endif
#if POLSKI_en
pol_prt_charging_test,
#endif
#if SLOVENCINA_en
slo_prt_charging_test,
#endif
#if SLOVENSCINA_en
sls_prt_charging_test,
#endif
#if TURKU_en
tur_prt_charging_test,
#endif
#if EESTI_en
ees_prt_charging_test,
#endif
#if LATVIESU_en
lat_prt_charging_test,
#endif
#if LIMBA_ROMANA_en
lim_prt_charging_test,
#endif
#if PUSSIJI_en
pus_prt_charging_test,
#endif
#if EAAHNIKA_en
eaa_prt_charging_test,
#endif
};
const char *prt_note[] = {
#if ENGLISH_en
en_prt_note,
#endif
#if FRANCAIS_en
fr_prt_note,
#endif
#if DEUTSCH_en
de_prt_note,
#endif
#if ESPANOL_en
es_prt_note,
#endif
#if ITALIAN_en
it_prt_note,
#endif
#if PORTUGUES_en
pt_prt_note,
#endif
#if NEDERLANDS_en
ned_prt_note,
#endif
#if DANSK_en
dan_prt_note,
#endif
#if NORSK_en
nor_prt_note,
#endif
#if SOOMI_en
soo_prt_note,
#endif
#if SVENSKA_en
sve_prt_note,
#endif
#if CESTINA_en
ces_prt_note,
#endif
#if LIETUVIU_K_en
lie_prt_note,
#endif
#if POLSKI_en
pol_prt_note,
#endif
#if SLOVENCINA_en
slo_prt_note,
#endif
#if SLOVENSCINA_en
sls_prt_note,
#endif
#if TURKU_en
tur_prt_note,
#endif
#if EESTI_en
ees_prt_note,
#endif
#if LATVIESU_en
lat_prt_note,
#endif
#if LIMBA_ROMANA_en
lim_prt_note,
#endif
#if PUSSIJI_en
pus_prt_note,
#endif
#if EAAHNIKA_en
eaa_prt_note,
#endif
};
const char *test_code_list[] = {
#if ENGLISH_en
en_test_code,
#endif
#if FRANCAIS_en
fr_test_code,
#endif
#if DEUTSCH_en
de_test_code,
#endif
#if ESPANOL_en
es_test_code,
#endif
#if ITALIAN_en
it_test_code,
#endif
#if PORTUGUES_en
pt_test_code,
#endif
#if NEDERLANDS_en
ned_test_code,
#endif
#if DANSK_en
dan_test_code,
#endif
#if NORSK_en
nor_test_code,
#endif
#if SOOMI_en
soo_test_code,
#endif
#if SVENSKA_en
sve_test_code,
#endif
#if CESTINA_en
ces_test_code,
#endif
#if LIETUVIU_K_en
lie_test_code,
#endif
#if POLSKI_en
pol_test_code,
#endif
#if SLOVENCINA_en
slo_test_code,
#endif
#if SLOVENSCINA_en
sls_test_code,
#endif
#if TURKU_en
tur_test_code,
#endif
#if EESTI_en
ees_test_code,
#endif
#if LATVIESU_en
lat_test_code,
#endif
#if LIMBA_ROMANA_en
lim_test_code,
#endif
#if PUSSIJI_en
pus_test_code,
#endif
#if EAAHNIKA_en
eaa_test_code,
#endif
};	
const char *prt_diode_ripple[] = {
#if ENGLISH_en
en_prt_diode_ripple,
#endif
#if FRANCAIS_en
fr_prt_diode_ripple,
#endif
#if DEUTSCH_en
de_prt_diode_ripple,
#endif
#if ESPANOL_en
es_prt_diode_ripple,
#endif
#if ITALIAN_en
it_prt_diode_ripple,
#endif
#if PORTUGUES_en
pt_prt_diode_ripple,
#endif
#if NEDERLANDS_en
ned_prt_diode_ripple,
#endif
#if DANSK_en
dan_prt_diode_ripple,
#endif
#if NORSK_en
nor_prt_diode_ripple,
#endif
#if SOOMI_en
soo_prt_diode_ripple,
#endif
#if SVENSKA_en
sve_prt_diode_ripple,
#endif
#if CESTINA_en
ces_prt_diode_ripple,
#endif
#if LIETUVIU_K_en
lie_prt_diode_ripple,
#endif
#if POLSKI_en
pol_prt_diode_ripple,
#endif
#if SLOVENCINA_en
slo_prt_diode_ripple,
#endif
#if SLOVENSCINA_en
sls_prt_diode_ripple,
#endif
#if TURKU_en
tur_prt_diode_ripple,
#endif
#if EESTI_en
ees_prt_diode_ripple,
#endif
#if LATVIESU_en
lat_prt_diode_ripple,
#endif
#if LIMBA_ROMANA_en
lim_prt_diode_ripple,
#endif
#if PUSSIJI_en
pus_prt_diode_ripple,
#endif
#if EAAHNIKA_en
eaa_prt_diode_ripple,
#endif
};

const char *flooded_list[] = {
#if ENGLISH_en
en_flooded,
#endif
#if FRANCAIS_en
fr_flooded,
#endif
#if DEUTSCH_en
de_flooded,
#endif
#if ESPANOL_en
es_flooded,
#endif
#if ITALIAN_en
it_flooded,
#endif
#if PORTUGUES_en
pt_flooded,
#endif
#if NEDERLANDS_en
ned_flooded,
#endif
#if DANSK_en
dan_flooded,
#endif
#if NORSK_en
nor_flooded,
#endif
#if SOOMI_en
soo_flooded,
#endif
#if SVENSKA_en
sve_flooded,
#endif
#if CESTINA_en
ces_flooded,
#endif
#if LIETUVIU_K_en
lie_flooded,
#endif
#if POLSKI_en
pol_flooded,
#endif
#if SLOVENCINA_en
slo_flooded,
#endif
#if SLOVENSCINA_en
sls_flooded,
#endif
#if TURKU_en
tur_flooded,
#endif
#if EESTI_en
ees_flooded,
#endif
#if LATVIESU_en
lat_flooded,
#endif
#if LIMBA_ROMANA_en
lim_flooded,
#endif
#if PUSSIJI_en
pus_flooded,
#endif
#if EAAHNIKA_en
eaa_flooded,
#endif
};
const char *good_n_pass_list[] = {
#if ENGLISH_en
en_good_n_pass,
#endif
#if FRANCAIS_en
fr_good_n_pass,
#endif
#if DEUTSCH_en
de_good_n_pass,
#endif
#if ESPANOL_en
es_good_n_pass,
#endif
#if ITALIAN_en
it_good_n_pass,
#endif
#if PORTUGUES_en
pt_good_n_pass,
#endif
#if NEDERLANDS_en
ned_good_n_pass,
#endif
#if DANSK_en
dan_good_n_pass,
#endif
#if NORSK_en
nor_good_n_pass,
#endif
#if SOOMI_en
soo_good_n_pass,
#endif
#if SVENSKA_en
sve_good_n_pass,
#endif
#if CESTINA_en
ces_good_n_pass,
#endif
#if LIETUVIU_K_en
lie_good_n_pass,
#endif
#if POLSKI_en
pol_good_n_pass,
#endif
#if SLOVENCINA_en
slo_good_n_pass,
#endif
#if SLOVENSCINA_en
sls_good_n_pass,
#endif
#if TURKU_en
tur_good_n_pass,
#endif
#if EESTI_en
ees_good_n_pass,
#endif
#if LATVIESU_en
lat_good_n_pass,
#endif
#if LIMBA_ROMANA_en
lim_good_n_pass,
#endif
#if PUSSIJI_en
pus_good_n_pass,
#endif
#if EAAHNIKA_en
eaa_good_n_pass,
#endif
};
const char *good_n_recharge_list[] = {
#if ENGLISH_en
en_good_n_recharge,
#endif
#if FRANCAIS_en
fr_good_n_recharge,
#endif
#if DEUTSCH_en
de_good_n_recharge,
#endif
#if ESPANOL_en
es_good_n_recharge,
#endif
#if ITALIAN_en
it_good_n_recharge,
#endif
#if PORTUGUES_en
pt_good_n_recharge,
#endif
#if NEDERLANDS_en
ned_good_n_recharge,
#endif
#if DANSK_en
dan_good_n_recharge,
#endif
#if NORSK_en
nor_good_n_recharge,
#endif
#if SOOMI_en
soo_good_n_recharge,
#endif
#if SVENSKA_en
sve_good_n_recharge,
#endif
#if CESTINA_en
ces_good_n_recharge,
#endif
#if LIETUVIU_K_en
lie_good_n_recharge,
#endif
#if POLSKI_en
pol_good_n_recharge,
#endif
#if SLOVENCINA_en
slo_good_n_recharge,
#endif
#if SLOVENSCINA_en
sls_good_n_recharge,
#endif
#if TURKU_en
tur_good_n_recharge,
#endif
#if EESTI_en
ees_good_n_recharge,
#endif
#if LATVIESU_en
lat_good_n_recharge,
#endif
#if LIMBA_ROMANA_en
lim_good_n_recharge,
#endif
#if PUSSIJI_en
pus_good_n_recharge,
#endif
#if EAAHNIKA_en
eaa_good_n_recharge,
#endif
};
const char *high_list[] = {
#if ENGLISH_en
en_high,
#endif
#if FRANCAIS_en
fr_high,
#endif
#if DEUTSCH_en
de_high,
#endif
#if ESPANOL_en
es_high,
#endif
#if ITALIAN_en
it_high,
#endif
#if PORTUGUES_en
pt_high,
#endif
#if NEDERLANDS_en
ned_high,
#endif
#if DANSK_en
dan_high,
#endif
#if NORSK_en
nor_high,
#endif
#if SOOMI_en
soo_high,
#endif
#if SVENSKA_en
sve_high,
#endif
#if CESTINA_en
ces_high,
#endif
#if LIETUVIU_K_en
lie_high,
#endif
#if POLSKI_en
pol_high,
#endif
#if SLOVENCINA_en
slo_high,
#endif
#if SLOVENSCINA_en
sls_high,
#endif
#if TURKU_en
tur_high,
#endif
#if EESTI_en
ees_high,
#endif
#if LATVIESU_en
lat_high,
#endif
#if LIMBA_ROMANA_en
lim_high,
#endif
#if PUSSIJI_en
pus_high,
#endif
#if EAAHNIKA_en
eaa_high,
#endif
};
const char *prt_ir_test[] = {
#if ENGLISH_en
en_prt_ir_test,
#endif
#if FRANCAIS_en
fr_prt_ir_test,
#endif
#if DEUTSCH_en
de_prt_ir_test,
#endif
#if ESPANOL_en
es_prt_ir_test,
#endif
#if ITALIAN_en
it_prt_ir_test,
#endif
#if PORTUGUES_en
pt_prt_ir_test,
#endif
#if NEDERLANDS_en
ned_prt_ir_test,
#endif
#if DANSK_en
dan_prt_ir_test,
#endif
#if NORSK_en
nor_prt_ir_test,
#endif
#if SOOMI_en
soo_prt_ir_test,
#endif
#if SVENSKA_en
sve_prt_ir_test,
#endif
#if CESTINA_en
ces_prt_ir_test,
#endif
#if LIETUVIU_K_en
lie_prt_ir_test,
#endif
#if POLSKI_en
pol_prt_ir_test,
#endif
#if SLOVENCINA_en
slo_prt_ir_test,
#endif
#if SLOVENSCINA_en
sls_prt_ir_test,
#endif
#if TURKU_en
tur_prt_ir_test,
#endif
#if EESTI_en
ees_prt_ir_test,
#endif
#if LATVIESU_en
lat_prt_ir_test,
#endif
#if LIMBA_ROMANA_en
lim_prt_ir_test,
#endif
#if PUSSIJI_en
pus_prt_ir_test,
#endif
#if EAAHNIKA_en
eaa_prt_ir_test,
#endif
};

const char *ir_t_list[] = {
#if ENGLISH_en
en_ir_t,
#endif
#if FRANCAIS_en
fr_ir_t,
#endif
#if DEUTSCH_en
de_ir_t,
#endif
#if ESPANOL_en
es_ir_t,
#endif
#if ITALIAN_en
it_ir_t,
#endif
#if PORTUGUES_en
pt_ir_t,
#endif
#if NEDERLANDS_en
ned_ir_t,
#endif
#if DANSK_en
dan_ir_t,
#endif
#if NORSK_en
nor_ir_t,
#endif
#if SOOMI_en
soo_ir_t,
#endif
#if SVENSKA_en
sve_ir_t,
#endif
#if CESTINA_en
ces_ir_t,
#endif
#if LIETUVIU_K_en
lie_ir_t,
#endif
#if POLSKI_en
pol_ir_t,
#endif
#if SLOVENCINA_en
slo_ir_t,
#endif
#if SLOVENSCINA_en
sls_ir_t,
#endif
#if TURKU_en
tur_ir_t,
#endif
#if EESTI_en
ees_ir_t,
#endif
#if LATVIESU_en
lat_ir_t,
#endif
#if LIMBA_ROMANA_en
lim_ir_t,
#endif
#if PUSSIJI_en
pus_ir_t,
#endif
#if EAAHNIKA_en
eaa_ir_t,
#endif
};


const char *prt_load_off[] = {
#if ENGLISH_en
en_prt_load_off,
#endif
#if FRANCAIS_en
fr_prt_load_off,
#endif
#if DEUTSCH_en
de_prt_load_off,
#endif
#if ESPANOL_en
es_prt_load_off,
#endif
#if ITALIAN_en
it_prt_load_off,
#endif
#if PORTUGUES_en
pt_prt_load_off,
#endif
#if NEDERLANDS_en
ned_prt_load_off,
#endif
#if DANSK_en
dan_prt_load_off,
#endif
#if NORSK_en
nor_prt_load_off,
#endif
#if SOOMI_en
soo_prt_load_off,
#endif
#if SVENSKA_en
sve_prt_load_off,
#endif
#if CESTINA_en
ces_prt_load_off,
#endif
#if LIETUVIU_K_en
lie_prt_load_off,
#endif
#if POLSKI_en
pol_prt_load_off,
#endif
#if SLOVENCINA_en
slo_prt_load_off,
#endif
#if SLOVENSCINA_en
sls_prt_load_off,
#endif
#if TURKU_en
tur_prt_load_off,
#endif
#if EESTI_en
ees_prt_load_off,
#endif
#if LATVIESU_en
lat_prt_load_off,
#endif
#if LIMBA_ROMANA_en
lim_prt_load_off,
#endif
#if PUSSIJI_en
pus_prt_load_off,
#endif
#if EAAHNIKA_en
eaa_prt_load_off,
#endif
};
const char *prt_load_on[] = {
#if ENGLISH_en
en_prt_load_on,
#endif
#if FRANCAIS_en
fr_prt_load_on,
#endif
#if DEUTSCH_en
de_prt_load_on,
#endif
#if ESPANOL_en
es_prt_load_on,
#endif
#if ITALIAN_en
it_prt_load_on,
#endif
#if PORTUGUES_en
pt_prt_load_on,
#endif
#if NEDERLANDS_en
ned_prt_load_on,
#endif
#if DANSK_en
dan_prt_load_on,
#endif
#if NORSK_en
nor_prt_load_on,
#endif
#if SOOMI_en
soo_prt_load_on,
#endif
#if SVENSKA_en
sve_prt_load_on,
#endif
#if CESTINA_en
ces_prt_load_on,
#endif
#if LIETUVIU_K_en
lie_prt_load_on,
#endif
#if POLSKI_en
pol_prt_load_on,
#endif
#if SLOVENCINA_en
slo_prt_load_on,
#endif
#if SLOVENSCINA_en
sls_prt_load_on,
#endif
#if TURKU_en
tur_prt_load_on,
#endif
#if EESTI_en
ees_prt_load_on,
#endif
#if LATVIESU_en
lat_prt_load_on,
#endif
#if LIMBA_ROMANA_en
lim_prt_load_on,
#endif
#if PUSSIJI_en
pus_prt_load_on,
#endif
#if EAAHNIKA_en
eaa_prt_load_on,
#endif
};
const char *low_list[] = {
#if ENGLISH_en
en_low,
#endif
#if FRANCAIS_en
fr_low,
#endif
#if DEUTSCH_en
de_low,
#endif
#if ESPANOL_en
es_low,
#endif
#if ITALIAN_en
it_low,
#endif
#if PORTUGUES_en
pt_low,
#endif
#if NEDERLANDS_en
ned_low,
#endif
#if DANSK_en
dan_low,
#endif
#if NORSK_en
nor_low,
#endif
#if SOOMI_en
soo_low,
#endif
#if SVENSKA_en
sve_low,
#endif
#if CESTINA_en
ces_low,
#endif
#if LIETUVIU_K_en
lie_low,
#endif
#if POLSKI_en
pol_low,
#endif
#if SLOVENCINA_en
slo_low,
#endif
#if SLOVENSCINA_en
sls_low,
#endif
#if TURKU_en
tur_low,
#endif
#if EESTI_en
ees_low,
#endif
#if LATVIESU_en
lat_low,
#endif
#if LIMBA_ROMANA_en
lim_low,
#endif
#if PUSSIJI_en
pus_low,
#endif
#if EAAHNIKA_en
eaa_low,
#endif
};
const char *measured_list[] = {
#if ENGLISH_en
en_measured,
#endif
#if FRANCAIS_en
fr_measured,
#endif
#if DEUTSCH_en
de_measured,
#endif
#if ESPANOL_en
es_measured,
#endif
#if ITALIAN_en
it_measured,
#endif
#if PORTUGUES_en
pt_measured,
#endif
#if NEDERLANDS_en
ned_measured,
#endif
#if DANSK_en
dan_measured,
#endif
#if NORSK_en
nor_measured,
#endif
#if SOOMI_en
soo_measured,
#endif
#if SVENSKA_en
sve_measured,
#endif
#if CESTINA_en
ces_measured,
#endif
#if LIETUVIU_K_en
lie_measured,
#endif
#if POLSKI_en
pol_measured,
#endif
#if SLOVENCINA_en
slo_measured,
#endif
#if SLOVENSCINA_en
sls_measured,
#endif
#if TURKU_en
tur_measured,
#endif
#if EESTI_en
ees_measured,
#endif
#if LATVIESU_en
lat_measured,
#endif
#if LIMBA_ROMANA_en
lim_measured,
#endif
#if PUSSIJI_en
pus_measured,
#endif
#if EAAHNIKA_en
eaa_measured,
#endif
};
const char *no_detect_list[] = {
#if ENGLISH_en
en_no_detect,
#endif
#if FRANCAIS_en
fr_no_detect,
#endif
#if DEUTSCH_en
de_no_detect,
#endif
#if ESPANOL_en
es_no_detect,
#endif
#if ITALIAN_en
it_no_detect,
#endif
#if PORTUGUES_en
pt_no_detect,
#endif
#if NEDERLANDS_en
ned_no_detect,
#endif
#if DANSK_en
dan_no_detect,
#endif
#if NORSK_en
nor_no_detect,
#endif
#if SOOMI_en
soo_no_detect,
#endif
#if SVENSKA_en
sve_no_detect,
#endif
#if CESTINA_en
ces_no_detect,
#endif
#if LIETUVIU_K_en
lie_no_detect,
#endif
#if POLSKI_en
pol_no_detect,
#endif
#if SLOVENCINA_en
slo_no_detect,
#endif
#if SLOVENSCINA_en
sls_no_detect,
#endif
#if TURKU_en
tur_no_detect,
#endif
#if EESTI_en
ees_no_detect,
#endif
#if LATVIESU_en
lat_no_detect,
#endif
#if LIMBA_ROMANA_en
lim_no_detect,
#endif
#if PUSSIJI_en
pus_no_detect,
#endif
#if EAAHNIKA_en
eaa_no_detect,
#endif
};
const char *normal_list[] = {
#if ENGLISH_en
en_normal,
#endif
#if FRANCAIS_en
fr_normal,
#endif
#if DEUTSCH_en
de_normal,
#endif
#if ESPANOL_en
es_normal,
#endif
#if ITALIAN_en
it_normal,
#endif
#if PORTUGUES_en
pt_normal,
#endif
#if NEDERLANDS_en
ned_normal,
#endif
#if DANSK_en
dan_normal,
#endif
#if NORSK_en
nor_normal,
#endif
#if SOOMI_en
soo_normal,
#endif
#if SVENSKA_en
sve_normal,
#endif
#if CESTINA_en
ces_normal,
#endif
#if LIETUVIU_K_en
lie_normal,
#endif
#if POLSKI_en
pol_normal,
#endif
#if SLOVENCINA_en
slo_normal,
#endif
#if SLOVENSCINA_en
sls_normal,
#endif
#if TURKU_en
tur_normal,
#endif
#if EESTI_en
ees_normal,
#endif
#if LATVIESU_en
lat_normal,
#endif
#if LIMBA_ROMANA_en
lim_normal,
#endif
#if PUSSIJI_en
pus_normal,
#endif
#if EAAHNIKA_en
eaa_normal,
#endif
};
const char *prt_rated[] = {
#if ENGLISH_en
en_prt_rated,
#endif
#if FRANCAIS_en
fr_prt_rated,
#endif
#if DEUTSCH_en
de_prt_rated,
#endif
#if ESPANOL_en
es_prt_rated,
#endif
#if ITALIAN_en
it_prt_rated,
#endif
#if PORTUGUES_en
pt_prt_rated,
#endif
#if NEDERLANDS_en
ned_prt_rated,
#endif
#if DANSK_en
dan_prt_rated,
#endif
#if NORSK_en
nor_prt_rated,
#endif
#if SOOMI_en
soo_prt_rated,
#endif
#if SVENSKA_en
sve_prt_rated,
#endif
#if CESTINA_en
ces_prt_rated,
#endif
#if LIETUVIU_K_en
lie_prt_rated,
#endif
#if POLSKI_en
pol_prt_rated,
#endif
#if SLOVENCINA_en
slo_prt_rated,
#endif
#if SLOVENSCINA_en
sls_prt_rated,
#endif
#if TURKU_en
tur_prt_rated,
#endif
#if EESTI_en
ees_prt_rated,
#endif
#if LATVIESU_en
lat_prt_rated,
#endif
#if LIMBA_ROMANA_en
lim_prt_rated,
#endif
#if PUSSIJI_en
pus_prt_rated,
#endif
#if EAAHNIKA_en
eaa_prt_rated,
#endif
};
const char *recharge_n_retest_list[] = {
#if ENGLISH_en
en_recharge_n_retest,
#endif
#if FRANCAIS_en
fr_recharge_n_retest,
#endif
#if DEUTSCH_en
de_recharge_n_retest,
#endif
#if ESPANOL_en
es_recharge_n_retest,
#endif
#if ITALIAN_en
it_recharge_n_retest,
#endif
#if PORTUGUES_en
pt_recharge_n_retest,
#endif
#if NEDERLANDS_en
ned_recharge_n_retest,
#endif
#if DANSK_en
dan_recharge_n_retest,
#endif
#if NORSK_en
nor_recharge_n_retest,
#endif
#if SOOMI_en
soo_recharge_n_retest,
#endif
#if SVENSKA_en
sve_recharge_n_retest,
#endif
#if CESTINA_en
ces_recharge_n_retest,
#endif
#if LIETUVIU_K_en
lie_recharge_n_retest,
#endif
#if POLSKI_en
pol_recharge_n_retest,
#endif
#if SLOVENCINA_en
slo_recharge_n_retest,
#endif
#if SLOVENSCINA_en
sls_recharge_n_retest,
#endif
#if TURKU_en
tur_recharge_n_retest,
#endif
#if EESTI_en
ees_recharge_n_retest,
#endif
#if LATVIESU_en
lat_recharge_n_retest,
#endif
#if LIMBA_ROMANA_en
lim_recharge_n_retest,
#endif
#if PUSSIJI_en
pus_recharge_n_retest,
#endif
#if EAAHNIKA_en
eaa_recharge_n_retest,
#endif
};
const char *prt_vin_num[] = {
#if ENGLISH_en
en_prt_vin_num,
#endif
#if FRANCAIS_en
fr_prt_vin_num,
#endif
#if DEUTSCH_en
de_prt_vin_num,
#endif
#if ESPANOL_en
es_prt_vin_num,
#endif
#if ITALIAN_en
it_prt_vin_num,
#endif
#if PORTUGUES_en
pt_prt_vin_num,
#endif
#if NEDERLANDS_en
ned_prt_vin_num,
#endif
#if DANSK_en
dan_prt_vin_num,
#endif
#if NORSK_en
nor_prt_vin_num,
#endif
#if SOOMI_en
soo_prt_vin_num,
#endif
#if SVENSKA_en
sve_prt_vin_num,
#endif
#if CESTINA_en
ces_prt_vin_num,
#endif
#if LIETUVIU_K_en
lie_prt_vin_num,
#endif
#if POLSKI_en
pol_prt_vin_num,
#endif
#if SLOVENCINA_en
slo_prt_vin_num,
#endif
#if SLOVENSCINA_en
sls_prt_vin_num,
#endif
#if TURKU_en
tur_prt_vin_num,
#endif
#if EESTI_en
ees_prt_vin_num,
#endif
#if LATVIESU_en
lat_prt_vin_num,
#endif
#if LIMBA_ROMANA_en
lim_prt_vin_num,
#endif
#if PUSSIJI_en
pus_prt_vin_num,
#endif
#if EAAHNIKA_en
eaa_prt_vin_num,
#endif
};
const char *prt_soc[] = {
#if ENGLISH_en
en_prt_soc,
#endif
#if FRANCAIS_en
fr_prt_soc,
#endif
#if DEUTSCH_en
de_prt_soc,
#endif
#if ESPANOL_en
es_prt_soc,
#endif
#if ITALIAN_en
it_prt_soc,
#endif
#if PORTUGUES_en
pt_prt_soc,
#endif
#if NEDERLANDS_en
ned_prt_soc,
#endif
#if DANSK_en
dan_prt_soc,
#endif
#if NORSK_en
nor_prt_soc,
#endif
#if SOOMI_en
soo_prt_soc,
#endif
#if SVENSKA_en
sve_prt_soc,
#endif
#if CESTINA_en
ces_prt_soc,
#endif
#if LIETUVIU_K_en
lie_prt_soc,
#endif
#if POLSKI_en
pol_prt_soc,
#endif
#if SLOVENCINA_en
slo_prt_soc,
#endif
#if SLOVENSCINA_en
sls_prt_soc,
#endif
#if TURKU_en
tur_prt_soc,
#endif
#if EESTI_en
ees_prt_soc,
#endif
#if LATVIESU_en
lat_prt_soc,
#endif
#if LIMBA_ROMANA_en
lim_prt_soc,
#endif
#if PUSSIJI_en
pus_prt_soc,
#endif
#if EAAHNIKA_en
eaa_prt_soc,
#endif
};
const char *prt_soh[] = {
#if ENGLISH_en
en_prt_soh,
#endif
#if FRANCAIS_en
fr_prt_soh,
#endif
#if DEUTSCH_en
de_prt_soh,
#endif
#if ESPANOL_en
es_prt_soh,
#endif
#if ITALIAN_en
it_prt_soh,
#endif
#if PORTUGUES_en
pt_prt_soh,
#endif
#if NEDERLANDS_en
ned_prt_soh,
#endif
#if DANSK_en
dan_prt_soh,
#endif
#if NORSK_en
nor_prt_soh,
#endif
#if SOOMI_en
soo_prt_soh,
#endif
#if SVENSKA_en
sve_prt_soh,
#endif
#if CESTINA_en
ces_prt_soh,
#endif
#if LIETUVIU_K_en
lie_prt_soh,
#endif
#if POLSKI_en
pol_prt_soh,
#endif
#if SLOVENCINA_en
slo_prt_soh,
#endif
#if SLOVENSCINA_en
sls_prt_soh,
#endif
#if TURKU_en
tur_prt_soh,
#endif
#if EESTI_en
ees_prt_soh,
#endif
#if LATVIESU_en
lat_prt_soh,
#endif
#if LIMBA_ROMANA_en
lim_prt_soh,
#endif
#if PUSSIJI_en
pus_prt_soh,
#endif
#if EAAHNIKA_en
eaa_prt_soh,
#endif
};
const char *ss_test_list[] = {
#if ENGLISH_en
en_ss_test,
#endif
#if FRANCAIS_en
fr_ss_test,
#endif
#if DEUTSCH_en
de_ss_test,
#endif
#if ESPANOL_en
es_ss_test,
#endif
#if ITALIAN_en
it_ss_test,
#endif
#if PORTUGUES_en
pt_ss_test,
#endif
#if NEDERLANDS_en
ned_ss_test,
#endif
#if DANSK_en
dan_ss_test,
#endif
#if NORSK_en
nor_ss_test,
#endif
#if SOOMI_en
soo_ss_test,
#endif
#if SVENSKA_en
sve_ss_test,
#endif
#if CESTINA_en
ces_ss_test,
#endif
#if LIETUVIU_K_en
lie_ss_test,
#endif
#if POLSKI_en
pol_ss_test,
#endif
#if SLOVENCINA_en
slo_ss_test,
#endif
#if SLOVENSCINA_en
sls_ss_test,
#endif
#if TURKU_en
tur_ss_test,
#endif
#if EESTI_en
ees_ss_test,
#endif
#if LATVIESU_en
lat_ss_test,
#endif
#if LIMBA_ROMANA_en
lim_ss_test,
#endif
#if PUSSIJI_en
pus_ss_test,
#endif
#if EAAHNIKA_en
eaa_ss_test,
#endif
};
const char *prt_starter_test[] = {
#if ENGLISH_en
en_prt_starter_test,
#endif
#if FRANCAIS_en
fr_prt_starter_test,
#endif
#if DEUTSCH_en
de_prt_starter_test,
#endif
#if ESPANOL_en
es_prt_starter_test,
#endif
#if ITALIAN_en
it_prt_starter_test,
#endif
#if PORTUGUES_en
pt_prt_starter_test,
#endif
#if NEDERLANDS_en
ned_prt_starter_test,
#endif
#if DANSK_en
dan_prt_starter_test,
#endif
#if NORSK_en
nor_prt_starter_test,
#endif
#if SOOMI_en
soo_prt_starter_test,
#endif
#if SVENSKA_en
sve_prt_starter_test,
#endif
#if CESTINA_en
ces_prt_starter_test,
#endif
#if LIETUVIU_K_en
lie_prt_starter_test,
#endif
#if POLSKI_en
pol_prt_starter_test,
#endif
#if SLOVENCINA_en
slo_prt_starter_test,
#endif
#if SLOVENSCINA_en
sls_prt_starter_test,
#endif
#if TURKU_en
tur_prt_starter_test,
#endif
#if EESTI_en
ees_prt_starter_test,
#endif
#if LATVIESU_en
lat_prt_starter_test,
#endif
#if LIMBA_ROMANA_en
lim_prt_starter_test,
#endif
#if PUSSIJI_en
pus_prt_starter_test,
#endif
#if EAAHNIKA_en
eaa_prt_starter_test,
#endif
};
const char *prt_start_stop[] = {
#if ENGLISH_en
en_prt_start_stop,
#endif
#if FRANCAIS_en
fr_prt_start_stop,
#endif
#if DEUTSCH_en
de_prt_start_stop,
#endif
#if ESPANOL_en
es_prt_start_stop,
#endif
#if ITALIAN_en
it_prt_start_stop,
#endif
#if PORTUGUES_en
pt_prt_start_stop,
#endif
#if NEDERLANDS_en
ned_prt_start_stop,
#endif
#if DANSK_en
dan_prt_start_stop,
#endif
#if NORSK_en
nor_prt_start_stop,
#endif
#if SOOMI_en
soo_prt_start_stop,
#endif
#if SVENSKA_en
sve_prt_start_stop,
#endif
#if CESTINA_en
ces_prt_start_stop,
#endif
#if LIETUVIU_K_en
lie_prt_start_stop,
#endif
#if POLSKI_en
pol_prt_start_stop,
#endif
#if SLOVENCINA_en
slo_prt_start_stop,
#endif
#if SLOVENSCINA_en
sls_prt_start_stop,
#endif
#if TURKU_en
tur_prt_start_stop,
#endif
#if EESTI_en
ees_prt_start_stop,
#endif
#if LATVIESU_en
lat_prt_start_stop,
#endif
#if LIMBA_ROMANA_en
lim_prt_start_stop,
#endif
#if PUSSIJI_en
pus_prt_start_stop,
#endif
#if EAAHNIKA_en
eaa_prt_start_stop,
#endif
};
const char *sys_test_list[] = {
#if ENGLISH_en
en_sys_test,
#endif
#if FRANCAIS_en
fr_sys_test,
#endif
#if DEUTSCH_en
de_sys_test,
#endif
#if ESPANOL_en
es_sys_test,
#endif
#if ITALIAN_en
it_sys_test,
#endif
#if PORTUGUES_en
pt_sys_test,
#endif
#if NEDERLANDS_en
ned_sys_test,
#endif
#if DANSK_en
dan_sys_test,
#endif
#if NORSK_en
nor_sys_test,
#endif
#if SOOMI_en
soo_sys_test,
#endif
#if SVENSKA_en
sve_sys_test,
#endif
#if CESTINA_en
ces_sys_test,
#endif
#if LIETUVIU_K_en
lie_sys_test,
#endif
#if POLSKI_en
pol_sys_test,
#endif
#if SLOVENCINA_en
slo_sys_test,
#endif
#if SLOVENSCINA_en
sls_sys_test,
#endif
#if TURKU_en
tur_sys_test,
#endif
#if EESTI_en
ees_sys_test,
#endif
#if LATVIESU_en
lat_sys_test,
#endif
#if LIMBA_ROMANA_en
lim_sys_test,
#endif
#if PUSSIJI_en
pus_sys_test,
#endif
#if EAAHNIKA_en
eaa_sys_test,
#endif
};
const char *prt_temp[] = {
#if ENGLISH_en
en_prt_temp,
#endif
#if FRANCAIS_en
fr_prt_temp,
#endif
#if DEUTSCH_en
de_prt_temp,
#endif
#if ESPANOL_en
es_prt_temp,
#endif
#if ITALIAN_en
it_prt_temp,
#endif
#if PORTUGUES_en
pt_prt_temp,
#endif
#if NEDERLANDS_en
ned_prt_temp,
#endif
#if DANSK_en
dan_prt_temp,
#endif
#if NORSK_en
nor_prt_temp,
#endif
#if SOOMI_en
soo_prt_temp,
#endif
#if SVENSKA_en
sve_prt_temp,
#endif
#if CESTINA_en
ces_prt_temp,
#endif
#if LIETUVIU_K_en
lie_prt_temp,
#endif
#if POLSKI_en
pol_prt_temp,
#endif
#if SLOVENCINA_en
slo_prt_temp,
#endif
#if SLOVENSCINA_en
sls_prt_temp,
#endif
#if TURKU_en
tur_prt_temp,
#endif
#if EESTI_en
ees_prt_temp,
#endif
#if LATVIESU_en
lat_prt_temp,
#endif
#if LIMBA_ROMANA_en
lim_prt_temp,
#endif
#if PUSSIJI_en
pus_prt_temp,
#endif
#if EAAHNIKA_en
eaa_prt_temp,
#endif
};
const char *test_counter_list[] = {
#if ENGLISH_en
en_test_counter,
#endif
#if FRANCAIS_en
fr_test_counter,
#endif
#if DEUTSCH_en
de_test_counter,
#endif
#if ESPANOL_en
es_test_counter,
#endif
#if ITALIAN_en
it_test_counter,
#endif
#if PORTUGUES_en
pt_test_counter,
#endif
#if NEDERLANDS_en
ned_test_counter,
#endif
#if DANSK_en
dan_test_counter,
#endif
#if NORSK_en
nor_test_counter,
#endif
#if SOOMI_en
soo_test_counter,
#endif
#if SVENSKA_en
sve_test_counter,
#endif
#if CESTINA_en
ces_test_counter,
#endif
#if LIETUVIU_K_en
lie_test_counter,
#endif
#if POLSKI_en
pol_test_counter,
#endif
#if SLOVENCINA_en
slo_test_counter,
#endif
#if SLOVENSCINA_en
sls_test_counter,
#endif
#if TURKU_en
tur_test_counter,
#endif
#if EESTI_en
ees_test_counter,
#endif
#if LATVIESU_en
lat_test_counter,
#endif
#if LIMBA_ROMANA_en
lim_test_counter,
#endif
#if PUSSIJI_en
pus_test_counter,
#endif
#if EAAHNIKA_en
eaa_test_counter,
#endif
};
const char *prt_test_date[] = {
#if ENGLISH_en
en_prt_test_date,
#endif
#if FRANCAIS_en
fr_prt_test_date,
#endif
#if DEUTSCH_en
de_prt_test_date,
#endif
#if ESPANOL_en
es_prt_test_date,
#endif
#if ITALIAN_en
it_prt_test_date,
#endif
#if PORTUGUES_en
pt_prt_test_date,
#endif
#if NEDERLANDS_en
ned_prt_test_date,
#endif
#if DANSK_en
dan_prt_test_date,
#endif
#if NORSK_en
nor_prt_test_date,
#endif
#if SOOMI_en
soo_prt_test_date,
#endif
#if SVENSKA_en
sve_prt_test_date,
#endif
#if CESTINA_en
ces_prt_test_date,
#endif
#if LIETUVIU_K_en
lie_prt_test_date,
#endif
#if POLSKI_en
pol_prt_test_date,
#endif
#if SLOVENCINA_en
slo_prt_test_date,
#endif
#if SLOVENSCINA_en
sls_prt_test_date,
#endif
#if TURKU_en
tur_prt_test_date,
#endif
#if EESTI_en
ees_prt_test_date,
#endif
#if LATVIESU_en
lat_prt_test_date,
#endif
#if LIMBA_ROMANA_en
lim_prt_test_date,
#endif
#if PUSSIJI_en
pus_prt_test_date,
#endif
#if EAAHNIKA_en
eaa_prt_test_date,
#endif
};
const char *prt_test_report[] = {
#if ENGLISH_en
en_prt_test_report,
#endif
#if FRANCAIS_en
fr_prt_test_report,
#endif
#if DEUTSCH_en
de_prt_test_report,
#endif
#if ESPANOL_en
es_prt_test_report,
#endif
#if ITALIAN_en
it_prt_test_report,
#endif
#if PORTUGUES_en
pt_prt_test_report,
#endif
#if NEDERLANDS_en
ned_prt_test_report,
#endif
#if DANSK_en
dan_prt_test_report,
#endif
#if NORSK_en
nor_prt_test_report,
#endif
#if SOOMI_en
soo_prt_test_report,
#endif
#if SVENSKA_en
sve_prt_test_report,
#endif
#if CESTINA_en
ces_prt_test_report,
#endif
#if LIETUVIU_K_en
lie_prt_test_report,
#endif
#if POLSKI_en
pol_prt_test_report,
#endif
#if SLOVENCINA_en
slo_prt_test_report,
#endif
#if SLOVENSCINA_en
sls_prt_test_report,
#endif
#if TURKU_en
tur_prt_test_report,
#endif
#if EESTI_en
ees_prt_test_report,
#endif
#if LATVIESU_en
lat_prt_test_report,
#endif
#if LIMBA_ROMANA_en
lim_prt_test_report,
#endif
#if PUSSIJI_en
pus_prt_test_report,
#endif
#if EAAHNIKA_en
eaa_prt_test_report,
#endif
};
const char *voltage_list[] = {
#if ENGLISH_en
en_voltage,
#endif
#if FRANCAIS_en
fr_voltage,
#endif
#if DEUTSCH_en
de_voltage,
#endif
#if ESPANOL_en
es_voltage,
#endif
#if ITALIAN_en
it_voltage,
#endif
#if PORTUGUES_en
pt_voltage,
#endif
#if NEDERLANDS_en
ned_voltage,
#endif
#if DANSK_en
dan_voltage,
#endif
#if NORSK_en
nor_voltage,
#endif
#if SOOMI_en
soo_voltage,
#endif
#if SVENSKA_en
sve_voltage,
#endif
#if CESTINA_en
ces_voltage,
#endif
#if LIETUVIU_K_en
lie_voltage,
#endif
#if POLSKI_en
pol_voltage,
#endif
#if SLOVENCINA_en
slo_voltage,
#endif
#if SLOVENSCINA_en
sls_voltage,
#endif
#if TURKU_en
tur_voltage,
#endif
#if EESTI_en
ees_voltage,
#endif
#if LATVIESU_en
lat_voltage,
#endif
#if LIMBA_ROMANA_en
lim_voltage,
#endif
#if PUSSIJI_en
pus_voltage,
#endif
#if EAAHNIKA_en
eaa_voltage,
#endif
};


const char *prt_test_report_list[] = {
#if ENGLISH_en
en_prt_test_report,
#endif
#if FRANCAIS_en
fr_prt_test_report,
#endif
#if DEUTSCH_en
de_prt_test_report,
#endif
#if ESPANOL_en
es_prt_test_report,
#endif
#if ITALIAN_en
it_prt_test_report,
#endif
#if PORTUGUES_en
pt_prt_test_report,
#endif
#if NEDERLANDS_en
ned_prt_test_report,
#endif
#if DANSK_en
dan_prt_test_report,
#endif
#if NORSK_en
nor_prt_test_report,
#endif
#if SOOMI_en
soo_prt_test_report,
#endif
#if SVENSKA_en
sve_prt_test_report,
#endif
#if CESTINA_en
ces_prt_test_report,
#endif
#if LIETUVIU_K_en
lie_prt_test_report,
#endif
#if POLSKI_en
pol_prt_test_report,
#endif
#if SLOVENCINA_en
slo_prt_test_report,
#endif
#if SLOVENSCINA_en
sls_prt_test_report,
#endif
#if TURKU_en
tur_prt_test_report,
#endif
#if EESTI_en
ees_prt_test_report,
#endif
#if LATVIESU_en
lat_prt_test_report,
#endif
#if LIMBA_ROMANA_en
lim_prt_test_report,
#endif
#if PUSSIJI_en
pus_prt_test_report,
#endif
#if EAAHNIKA_en
eaa_prt_test_report,
#endif
};

const char *off_list[] = {
#if ENGLISH_en
en_off,
#endif
#if FRANCAIS_en	
fr_off,
#endif
#if DEUTSCH_en
de_off,
#endif
#if ESPANOL_en
es_off,
#endif
#if ITALIAN_en
it_off,
#endif
#if PORTUGUES_en
pt_off,
#endif
#if NEDERLANDS_en
ned_off,
#endif
#if DANSK_en
dan_off,
#endif
#if NORSK_en
nor_off,
#endif
#if SOOMI_en
soo_off,
#endif
#if SVENSKA_en
sve_off,
#endif
#if CESTINA_en
ces_off,
#endif
#if LIETUVIU_K_en
lie_off,
#endif
#if POLSKI_en
pol_off,
#endif
#if SLOVENCINA_en
slo_off,
#endif
#if SLOVENSCINA_en
sls_off,
#endif
#if TURKU_en
tur_off,
#endif
#if EESTI_en
ees_off,
#endif
#if LATVIESU_en
lat_off,
#endif
#if LIMBA_ROMANA_en
lim_off,
#endif
#if PUSSIJI_en
pus_off,
#endif
#if EAAHNIKA_en
eaa_off,
#endif
};

const char *on_list[] = {
#if ENGLISH_en
en_on,
#endif
#if FRANCAIS_en	
fr_on,
#endif
#if DEUTSCH_en
de_on,
#endif
#if ESPANOL_en
es_on,
#endif
#if ITALIAN_en
it_on,
#endif
#if PORTUGUES_en
pt_on,
#endif
#if NEDERLANDS_en
ned_on,
#endif
#if DANSK_en
dan_on,
#endif
#if NORSK_en
nor_on,
#endif
#if SOOMI_en
soo_on,
#endif
#if SVENSKA_en
sve_on,
#endif
#if CESTINA_en
ces_on,
#endif
#if LIETUVIU_K_en
lie_on,
#endif
#if POLSKI_en
pol_on,
#endif
#if SLOVENCINA_en
slo_on,
#endif
#if SLOVENSCINA_en
sls_on,
#endif
#if TURKU_en
tur_on,
#endif
#if EESTI_en
ees_on,
#endif
#if LATVIESU_en
lat_on,
#endif
#if LIMBA_ROMANA_en
lim_on,
#endif
#if PUSSIJI_en
pus_on,
#endif
#if EAAHNIKA_en
eaa_on,
#endif
};

const char *wifi_refresh_list[] = {
#if ENGLISH_en
en_refresh_list,
#endif
#if FRANCAIS_en	
fr_refresh_list,
#endif
#if DEUTSCH_en
de_refresh_list,
#endif
#if ESPANOL_en
es_refresh_list,
#endif
#if ITALIAN_en
it_refresh_list,
#endif
#if PORTUGUES_en
pt_refresh_list,
#endif
#if NEDERLANDS_en
ned_refresh_list,
#endif
#if DANSK_en
dan_refresh_list,
#endif
#if NORSK_en
nor_refresh_list,
#endif
#if SOOMI_en
soo_refresh_list,
#endif
#if SVENSKA_en
sve_refresh_list,
#endif
#if CESTINA_en
ces_refresh_list,
#endif
#if LIETUVIU_K_en
lie_refresh_list,
#endif
#if POLSKI_en
pol_refresh_list,
#endif
#if SLOVENCINA_en
slo_refresh_list,
#endif
#if SLOVENSCINA_en
sls_refresh_list,
#endif
#if TURKU_en
tur_refresh_list,
#endif
#if EESTI_en
ees_refresh_list,
#endif
#if LATVIESU_en
lat_refresh_list,
#endif
#if LIMBA_ROMANA_en
lim_refresh_list,
#endif
#if PUSSIJI_en
pus_refresh_list,
#endif
#if EAAHNIKA_en
eaa_refresh_list,
#endif
};
const char *registration_list[] = {
#if ENGLISH_en
en_registration,
#endif
#if FRANCAIS_en	
fr_registration,
#endif
#if DEUTSCH_en
de_registration,
#endif
#if ESPANOL_en
es_registration,
#endif
#if ITALIAN_en
it_registration,
#endif
#if PORTUGUES_en
pt_registration,
#endif
#if NEDERLANDS_en
ned_registration,
#endif
#if DANSK_en
dan_registration,
#endif
#if NORSK_en
nor_registration,
#endif
#if SOOMI_en
soo_registration,
#endif
#if SVENSKA_en
sve_registration,
#endif
#if CESTINA_en
ces_registration,
#endif
#if LIETUVIU_K_en
lie_registration,
#endif
#if POLSKI_en
pol_registration,
#endif
#if SLOVENCINA_en
slo_registration,
#endif
#if SLOVENSCINA_en
sls_registration,
#endif
#if TURKU_en
tur_registration,
#endif
#if EESTI_en
ees_registration,
#endif
#if LATVIESU_en
lat_registration,
#endif
#if LIMBA_ROMANA_en
lim_registration,
#endif
#if PUSSIJI_en
pus_registration,
#endif
#if EAAHNIKA_en
eaa_registration,
#endif
};

const char *scan_qr_code_list[] = {
#if ENGLISH_en
en_scan_qr_code,
#endif
#if FRANCAIS_en	
fr_scan_qr_code,
#endif
#if DEUTSCH_en
de_scan_qr_code,
#endif
#if ESPANOL_en
es_scan_qr_code,
#endif
#if ITALIAN_en
it_scan_qr_code,
#endif
#if PORTUGUES_en
pt_scan_qr_code,
#endif
#if NEDERLANDS_en
ned_scan_qr_code,
#endif
#if DANSK_en
dan_scan_qr_code,
#endif
#if NORSK_en
nor_scan_qr_code,
#endif
#if SOOMI_en
soo_scan_qr_code,
#endif
#if SVENSKA_en
sve_scan_qr_code,
#endif
#if CESTINA_en
ces_scan_qr_code,
#endif
#if LIETUVIU_K_en
lie_scan_qr_code,
#endif
#if POLSKI_en
pol_scan_qr_code,
#endif
#if SLOVENCINA_en
slo_scan_qr_code,
#endif
#if SLOVENSCINA_en
sls_scan_qr_code,
#endif
#if TURKU_en
tur_scan_qr_code,
#endif
#if EESTI_en
ees_scan_qr_code,
#endif
#if LATVIESU_en
lat_scan_qr_code,
#endif
#if LIMBA_ROMANA_en
lim_scan_qr_code,
#endif
#if PUSSIJI_en
pus_scan_qr_code,
#endif
#if EAAHNIKA_en
eaa_scan_qr_code,
#endif
};
const char *registering_list[] = {
#if ENGLISH_en
en_registering,
#endif
#if FRANCAIS_en	
fr_registering,
#endif
#if DEUTSCH_en
de_registering,
#endif
#if ESPANOL_en
es_registering,
#endif
#if ITALIAN_en
it_registering,
#endif
#if PORTUGUES_en
pt_registering,
#endif
#if NEDERLANDS_en
ned_registering,
#endif
#if DANSK_en
dan_registering,
#endif
#if NORSK_en
nor_registering,
#endif
#if SOOMI_en
soo_registering,
#endif
#if SVENSKA_en
sve_registering,
#endif
#if CESTINA_en
ces_registering,
#endif
#if LIETUVIU_K_en
lie_registering,
#endif
#if POLSKI_en
pol_registering,
#endif
#if SLOVENCINA_en
slo_registering,
#endif
#if SLOVENSCINA_en
sls_registering,
#endif
#if TURKU_en
tur_registering,
#endif
#if EESTI_en
ees_registering,
#endif
#if LATVIESU_en
lat_registering,
#endif
#if LIMBA_ROMANA_en
lim_registering,
#endif
#if PUSSIJI_en
pus_registering,
#endif
#if EAAHNIKA_en
eaa_registering,
#endif
};

/* 20200803 DW some pictures of sys test changed to text description  */
const char *off_load_list[] = {
#if ENGLISH_en
en_off_load,
#endif
#if FRANCAIS_en
fr_off_load,
#endif
#if ESPANOL_en
es_off_load,
#endif
};
/* 20200803 DW some pictures of sys test changed to text description  */
const char *start_engine_list[] = {
#if ENGLISH_en
en_start_engine,
#endif
#if FRANCAIS_en
fr_start_engine,
#endif
#if ESPANOL_en
es_start_engine,
#endif
};
/* 20200803 DW some pictures of sys test changed to text description  */
const char *make_sure_all_list[] = {
#if ENGLISH_en
en_make_sure_all,
#endif
#if FRANCAIS_en
fr_make_sure_all,
#endif
#if ESPANOL_en
es_make_sure_all,
#endif
};
/* 20200803 DW some pictures of sys test changed to text description  */
const char *load_are_off_list[] = {
#if ENGLISH_en
en_load_are_off,
#endif
#if FRANCAIS_en
fr_load_are_off,
#endif
#if ESPANOL_en
es_load_are_off,
#endif
};
/* 20200803 DW some pictures of sys test changed to text description  */
const char *turn_on_lo_list[] = {
#if ENGLISH_en
en_turn_on_lo,
#endif
#if FRANCAIS_en
fr_turn_on_lo,
#endif
#if ESPANOL_en
es_turn_on_lo,
#endif
};
/* 20200803 DW some pictures of sys test changed to text description  */
const char *and_press_ent_list[] = {
#if ENGLISH_en
en_and_press_ent,
#endif
#if FRANCAIS_en
fr_and_press_ent,
#endif
#if ESPANOL_en
es_and_press_ent,
#endif
};
/* 20200803 DW some pictures of sys test changed to text description  */
const char *turn_eng_2500_list[] = {
#if ENGLISH_en
en_turn_eng_2500,
#endif
#if FRANCAIS_en
fr_turn_eng_2500,
#endif
#if ESPANOL_en
es_turn_eng_2500,
#endif
};
/* 20200803 DW VM/AM meter add reminder on/off screen :   */
const char *AMVM_TURN_ON_AMP_list[] = {
#if ENGLISH_en
en_AMVM_TURN_ON_AMP,
#endif
#if FRANCAIS_en
fr_AMVM_TURN_ON_AMP,
#endif
#if ESPANOL_en
es_AMVM_TURN_ON_AMP,
#endif
};
/* 20200803 DW VM/AM meter add reminder on/off screen :   */
const char *AMVM_AFTER_ATT_list[] = {
#if ENGLISH_en
en_AMVM_AFTER_ATT,
#endif
#if FRANCAIS_en
fr_AMVM_AFTER_ATT,
#endif
#if ESPANOL_en
es_AMVM_AFTER_ATT,
#endif
};
/* 20200803 DW VM/AM meter add reminder on/off screen :   */
const char *AMVM_ENTER_PROCEED_list[] = {
#if ENGLISH_en
en_AMVM_ENTER_PROCEED,
#endif
#if FRANCAIS_en
fr_AMVM_ENTER_PROCEED,
#endif
#if ESPANOL_en
es_AMVM_ENTER_PROCEED,
#endif
};
/* 20200803 DW VM/AM meter add reminder on/off screen :   */
const char *AMVM_TURN_OFF_AMP_list[] = {
#if ENGLISH_en
en_AMVM_TURN_OFF_AMP,
#endif
#if FRANCAIS_en
fr_AMVM_TURN_OFF_AMP,
#endif
#if ESPANOL_en
es_AMVM_TURN_OFF_AMP,
#endif
};
/* 20200803 DW VM/AM meter add reminder on/off screen :   */
const char *AMVM_AMP_METER_list[] = {
#if ENGLISH_en
en_AMVM_AMP_METER,
#endif
#if FRANCAIS_en
fr_AMVM_AMP_METER,
#endif
#if ESPANOL_en
es_AMVM_AMP_METER,
#endif
};
/* 20200806 DW IS 24V BAT   */
const char *is_24v_system_list[] = {
#if ENGLISH_en
en_is_24v_system,
#endif
#if FRANCAIS_en
fr_is_24v_system,
#endif
#if ESPANOL_en
es_is_24v_system,
#endif
};
/* 20200806 DW TEST EACH BAT SEPARATELY   */
const char *test_each_bat_sep_list[] = {
#if ENGLISH_en
en_test_each_bat_sep,
#endif
#if FRANCAIS_en
fr_test_each_bat_sep,
#endif
#if ESPANOL_en
es_test_each_bat_sep,
#endif
};
/* 20200806 DW CONNECT TO A 12V BATTERY   */
const char *connect_to_12v_bat_list[] = {
#if ENGLISH_en
en_connect_to_12v_bat,
#endif
#if FRANCAIS_en
fr_connect_to_12v_bat,
#endif
#if ESPANOL_en
es_connect_to_12v_bat,
#endif
};
/* 20200807 DW diesel engine ask   */
const char * is_diesel_engine_list[] = {
#if ENGLISH_en
en_is_diesel_engine,
#endif
#if FRANCAIS_en
fr_is_diesel_engine,
#endif
#if ESPANOL_en
es_is_diesel_engine,
#endif
};
/* 20200807 DW count down 40sec   */
const char *rev_engine_for_list[] = {
#if ENGLISH_en
en_rev_engine_for,
#endif
#if FRANCAIS_en
fr_rev_engine_for,
#endif
#if ESPANOL_en
es_rev_engine_for,
#endif
};

/* 20200813 DW HD   */
const char *Truck_Group_Test_list[] = {
#if ENGLISH_en
en_Truck_Group_Test,
#endif
#if FRANCAIS_en
fr_Truck_Group_Test,
#endif
#if ESPANOL_en
es_Truck_Group_Test,
#endif
};

/* 20200814 DW HD   */
const char *Of_Bat_In_Par_list[] = {
#if ENGLISH_en
en_Of_Bat_In_Par,
#endif
#if FRANCAIS_en
fr_Of_Bat_In_Par,
#endif
#if ESPANOL_en
es_Of_Bat_In_Par,
#endif
};

/* 20200814 DW HD   */
const char *Bat_2_list[] = {
#if ENGLISH_en
en_Bat_2,
#endif
#if FRANCAIS_en
fr_Bat_2,
#endif
#if ESPANOL_en
es_Bat_2,
#endif
};

/* 20200814 DW HD   */
const char *Bat_3_list[] = {
#if ENGLISH_en
en_Bat_3,
#endif
#if FRANCAIS_en
fr_Bat_3,
#endif
#if ESPANOL_en
es_Bat_3,
#endif
};
/* 20200814 DW HD   */
const char *Bat_4_list[] = {
#if ENGLISH_en
en_Bat_4,
#endif
#if FRANCAIS_en
fr_Bat_4,
#endif
#if ESPANOL_en
es_Bat_4,
#endif
};
/* 20200814 DW HD   */
const char *Bat_5_list[] = {
#if ENGLISH_en
en_Bat_5,
#endif
#if FRANCAIS_en
fr_Bat_5,
#endif
#if ESPANOL_en
es_Bat_5,
#endif
};

/* 20200814 DW HD   */
const char *Bat_6_list[] = {
#if ENGLISH_en
en_Bat_6,
#endif
#if FRANCAIS_en
fr_Bat_6,
#endif
#if ESPANOL_en
es_Bat_6,
#endif
};

/* 20200817 DW HD   */
const char *GOOD_PACK_list[] = {
#if ENGLISH_en
en_GOOD_PACK,
#endif
#if FRANCAIS_en
fr_GOOD_PACK,
#endif
#if ESPANOL_en
es_GOOD_PACK,
#endif
};

/* 20200817 DW HD   */
const char *CHECK_PACK_list[] = {
#if ENGLISH_en
en_CHECK_PACK,
#endif
#if FRANCAIS_en
fr_CHECK_PACK,
#endif
#if ESPANOL_en
es_CHECK_PACK,
#endif
};

/* 20200817 DW HD   */
const char *SEP_PACK_CONNECT_BAT1_list[] = {
#if ENGLISH_en
en_SEP_PACK_CONNECT_BAT1,
#endif
#if FRANCAIS_en
fr_SEP_PACK_CONNECT_BAT1,
#endif
#if ESPANOL_en
es_SEP_PACK_CONNECT_BAT1,
#endif
};

/* 20200817 DW HD   */
const char *SEP_PACK_CONNECT_BAT2_list[] = {
#if ENGLISH_en
en_SEP_PACK_CONNECT_BAT2,
#endif
#if FRANCAIS_en
fr_SEP_PACK_CONNECT_BAT2,
#endif
#if ESPANOL_en
es_SEP_PACK_CONNECT_BAT2,
#endif
};

/* 20200817 DW HD   */
const char *SEP_PACK_CONNECT_BAT3_list[] = {
#if ENGLISH_en
en_SEP_PACK_CONNECT_BAT3,
#endif
#if FRANCAIS_en
fr_SEP_PACK_CONNECT_BAT3,
#endif
#if ESPANOL_en
es_SEP_PACK_CONNECT_BAT3,
#endif
};

/* 20200817 DW HD   */
const char *SEP_PACK_CONNECT_BAT4_list[] = {
#if ENGLISH_en
en_SEP_PACK_CONNECT_BAT4,
#endif
#if FRANCAIS_en
fr_SEP_PACK_CONNECT_BAT4,
#endif
#if ESPANOL_en
es_SEP_PACK_CONNECT_BAT4,
#endif
};

/* 20200817 DW HD   */
const char *SEP_PACK_CONNECT_BAT5_list[] = {
#if ENGLISH_en
en_SEP_PACK_CONNECT_BAT5,
#endif
#if FRANCAIS_en
fr_SEP_PACK_CONNECT_BAT5,
#endif
#if ESPANOL_en
es_SEP_PACK_CONNECT_BAT5,
#endif
};

/* 20200817 DW HD   */
const char *SEP_PACK_CONNECT_BAT6_list[] = {
#if ENGLISH_en
en_SEP_PACK_CONNECT_BAT6,
#endif
#if FRANCAIS_en
fr_SEP_PACK_CONNECT_BAT6,
#endif
#if ESPANOL_en
es_SEP_PACK_CONNECT_BAT6,
#endif
};

/* 20200818 DW HD   */
const char *GR_TEST_BAT1_START_list[] = {
#if ENGLISH_en
en_GR_TEST_BAT1_START,
#endif
#if FRANCAIS_en
fr_GR_TEST_BAT1_START,
#endif
#if ESPANOL_en
es_GR_TEST_BAT1_START,
#endif
};
/* 20200818 DW HD   */
const char *GR_TEST_BAT2_START_list[] = {
#if ENGLISH_en
en_GR_TEST_BAT2_START,
#endif
#if FRANCAIS_en
fr_GR_TEST_BAT2_START,
#endif
#if ESPANOL_en
es_GR_TEST_BAT2_START,
#endif
};
/* 20200818 DW HD   */
const char *GR_TEST_BAT3_START_list[] = {
#if ENGLISH_en
en_GR_TEST_BAT3_START,
#endif
#if FRANCAIS_en
fr_GR_TEST_BAT3_START,
#endif
#if ESPANOL_en
es_GR_TEST_BAT3_START,
#endif
};
/* 20200818 DW HD   */
const char *GR_TEST_BAT4_START_list[] = {
#if ENGLISH_en
en_GR_TEST_BAT4_START,
#endif
#if FRANCAIS_en
fr_GR_TEST_BAT4_START,
#endif
#if ESPANOL_en
es_GR_TEST_BAT4_START,
#endif
};
/* 20200818 DW HD   */
const char *GR_TEST_BAT5_START_list[] = {
#if ENGLISH_en
en_GR_TEST_BAT5_START,
#endif
#if FRANCAIS_en
fr_GR_TEST_BAT5_START,
#endif
#if ESPANOL_en
es_GR_TEST_BAT5_START,
#endif
};
/* 20200818 DW HD   */
const char *GR_TEST_BAT6_START_list[] = {
#if ENGLISH_en
en_GR_TEST_BAT6_START,
#endif
#if FRANCAIS_en
fr_GR_TEST_BAT6_START,
#endif
#if ESPANOL_en
es_GR_TEST_BAT6_START,
#endif
};

/* 20200819 DW HD   */
const char *Pack_Report_list[] = {
#if ENGLISH_en
en_Pack_Report,
#endif
#if FRANCAIS_en
fr_Pack_Report,
#endif
#if ESPANOL_en
es_Pack_Report,
#endif
};

/* 20200819 DW HD   */
const char *BAT_NUM_list[] = {
#if ENGLISH_en
en_BAT_NUM,
#endif
#if FRANCAIS_en
fr_BAT_NUM,
#endif
#if ESPANOL_en
es_BAT_NUM,
#endif
};

/* 20200821 DW HD   */
const char *prt_pack_test[] = {
#if ENGLISH_en
en_pack_test,
#endif
#if FRANCAIS_en
fr_pack_test,
#endif
#if ESPANOL_en
es_pack_test,
#endif
};

/* 20200821 DW HD   */
const char *warning_word1_list[] = {
#if ENGLISH_en
en_warning_word1,
#endif
#if FRANCAIS_en
fr_warning_word1,
#endif
#if ESPANOL_en
es_warning_word1,
#endif
};

/* 20200821 DW HD   */
const char *warning_word2_list[] = {
#if ENGLISH_en
en_warning_word2,
#endif
#if FRANCAIS_en
fr_warning_word2,
#endif
#if ESPANOL_en
es_warning_word2,
#endif
};

/* 20200821 DW HD   */
const char *warning_word3_list[] = {
#if ENGLISH_en
en_warning_word3,
#endif
#if FRANCAIS_en
fr_warning_word3,
#endif
#if ESPANOL_en
es_warning_word3,
#endif
};

/* 20200824 DW HD   */
const char *P_BATTERT_NUM_list[] = {
#if ENGLISH_en
en_P_BATTERT_NUM,
#endif
#if FRANCAIS_en
fr_P_BATTERT_NUM,
#endif
#if ESPANOL_en
es_P_BATTERT_NUM,
#endif
};

/* 20200826 DW HD   */
const char *ad_ac_press_enter_list[] = {
#if ENGLISH_en
en_ad_ac_press_enter,
#endif
#if FRANCAIS_en
fr_ad_ac_press_enter,
#endif
#if ESPANOL_en
es_ad_ac_press_enter,
#endif
};

/* 20200908 DW HD TURN OFF LOADS & ENGINE   */
const char *SYS_TURN_OFF_LOAD_list[] = {
#if ENGLISH_en
en_SYS_TURN_OFF_LOAD,
#endif
#if FRANCAIS_en
fr_SYS_TURN_OFF_LOAD,
#endif
#if ESPANOL_en
es_SYS_TURN_OFF_LOAD,
#endif
};

/* 20201019 DW HD Turn Headlight ON FOR 15SEC   */
const char *Turn_Headlight_list[] = {
#if ENGLISH_en
en_Turn_Headlight_list,
#endif
#if FRANCAIS_en
fr_Turn_Headlight_list,
#endif
#if ESPANOL_en
es_Turn_Headlight_list,
#endif
};

/* 20201019 DW HD Turn Headlight ON FOR 15SEC   */
const char *please_en_start_list[] = {
#if ENGLISH_en
en_please_en_start_list,
#endif
#if FRANCAIS_en
fr_please_en_start_list,
#endif
#if ESPANOL_en
es_please_en_start_list,
#endif
};

/* 20201021 DW HD French and Spanish translation changes   */
const char *vrla_list[] = {
#if ENGLISH_en
en_vrla,
#endif
#if FRANCAIS_en
fr_vrla,
#endif
#if ESPANOL_en
en_vrla,
#endif
};

/* 20201021 DW HD French and Spanish translation changes   */
const char *soh_list[] = {
#if ENGLISH_en
en_soh,
#endif
#if FRANCAIS_en
fr_soh,
#endif
#if ESPANOL_en
en_soh,
#endif
};

/* 20201021 DW HD French and Spanish translation changes   */
const char *soc_list[] = {
#if ENGLISH_en
en_soc,
#endif
#if FRANCAIS_en
fr_soc,
#endif
#if ESPANOL_en
en_soc,
#endif
};

/* 20201021 DW HD French and Spanish translation changes   */
const char *cca_list[] = {
#if ENGLISH_en
en_cca,
#endif
#if FRANCAIS_en
fr_cca,
#endif
#if ESPANOL_en
en_cca,
#endif
};

/* 20201022 DW HD French and Spanish translation changes   */
const char *prt_jis_list[] = {
#if ENGLISH_en
en_prt_jis,
#endif
#if FRANCAIS_en
fr_prt_jis,
#endif
#if ESPANOL_en
en_prt_jis,
#endif
};



/**************************************************************************/
//
// Display Cranking voltage step by step to display waveform
// ui8CrankVNoWave: No detected.
// ui8CrankVLow: Low.
// ui8CrankVNormal: Normal.
//
//
/**************************************************************************/
const uint8_t ui8CrankVNoWave[] =
{
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,
		0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,
		0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,
		0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,
		0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,
		0x40,0x40,0x40,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00
};

const uint8_t ui8CrankVLow[] =
{
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,
		0x40,0x80,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x80,0x80,0x40,0x20,0x20,0x20,0x20,0x20,0x40,0x40,0x80,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x80,0x80,0x80,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0xC0,0x20,0x20,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,
		0x40,0x40,0x40,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x03,0x0C,0x30,0xC0,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x80,0x60,0x1C,
		0x02,0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x01,0x06,0x18,
		0x20,0x20,0x20,0x1C,0x03,0x00,0x00,0x00,0x03,0x04,0x0C,0x08,0x08,0x08,0x04,0x06,
		0x01,0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x03,0x04,0x08,0x10,0x10,0x10,0x08,0x06,0x01,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00
};

const uint8_t ui8CrankVNormal[] =
{
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0xC0,0x30,0x0C,0x02,0x02,0x02,0x0C,0x30,0xC0,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,0x40,
		0x40,0x80,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x80,0x60,0x1C,0x03,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x03,0x1C,0xE0,
		0x00,0x00,0x00,0x00,0x00,0xC0,0x30,0x0C,0x02,0x0C,0x30,0xC0,0x00,0x00,0x00,0x00,
		0x00,0xC0,0x30,0x08,0x04,0x08,0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x10,
		0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x10,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x03,0x0C,0x30,0xC0,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x80,0x60,0x1C,
		0x03,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x03,0x0C,0x10,0x0C,0x03,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x03,0x04,0x08,0x04,
		0x03,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x03,0x04,0x08,0x10,0x10,0x10,0x08,0x06,0x01,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00
};

void LCDDrawCrankingVolt(SYSRANK CrankVolRank, uint8_t ui8Col)
{
	switch(CrankVolRank)
	{
	case NO_DETECT:

		st7565DrawUprHalfScrnCrankVAnimation(ui8CrankVNoWave, ui8Col);

		break;
	case LOW:

		st7565DrawUprHalfScrnCrankVAnimation(ui8CrankVLow, ui8Col);

		break;
	case NORMAL:

		st7565DrawUprHalfScrnCrankVAnimation(ui8CrankVNormal, ui8Col);

		break;
	default:

		//ERROR!!!

		break;

	}
}

/**************************************************************************/
//
// Display Ripple voltage step by step to display waveform
// ui8RoippleVoWave: No detected.
// ui8RoippleVNormal: Normal.
// ui8RoippleVHigh: High.
//
/**************************************************************************/
const uint8_t ui8RoippleVoWave[] =
{
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x08,0x08,0x0C,
		0x04,0x04,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x04,0x04,
		0x04,0x04,0x04,0x0C,0x08,0x08,0x0C,0x04,0x04,0x02,0x02,0x02,0x02,0x02,0x02,0x02,
		0x02,0x02,0x02,0x02,0x02,0x04,0x04,0x04,0x04,0x04,0x0C,0x08,0x08,0x0C,0x04,0x04,
		0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x04,0x04,0x04,0x04,
		0x04,0x08,0x08,0x0C,0x04,0x04,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,0x02,
		0x02,0x02,0x04,0x04,0x04,0x04,0x04,0x0C,0x08,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00
};

const uint8_t ui8RoippleVNormal[] =
{
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x80,0x40,
		0x20,0x20,0x20,0x20,0x20,0x20,0x40,0x40,0x40,0x80,0x80,0x80,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x80,0x40,0x20,0x20,0x20,0x20,0x20,0x20,0x40,
		0x40,0x40,0x80,0x80,0x80,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x80,0x40,0x20,0x20,0x20,0x20,0x20,0x20,0x40,0x40,0x40,0x80,0x80,0x80,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x80,0x40,0x20,0x20,0x20,0x20,0x20,
		0x20,0x40,0x40,0x40,0x80,0x80,0x80,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x08,0x08,0x06,0x01,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x01,0x01,0x02,0x02,
		0x02,0x04,0x04,0x08,0x08,0x08,0x06,0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x01,0x01,0x02,0x02,0x02,0x04,0x04,0x08,0x08,0x08,0x06,
		0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x01,0x01,
		0x02,0x02,0x02,0x04,0x04,0x08,0x08,0x08,0x06,0x01,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x01,0x01,0x02,0x02,0x02,0x04,0x04,0x08,0x08,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00
};

const uint8_t ui8RoippleVHigh[] =
{
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x80,0x60,0x18,0x04,0x04,0x02,0x02,
		0x04,0x04,0x04,0x04,0x08,0x08,0x10,0x10,0x20,0x20,0x40,0x80,0x80,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x80,0x60,0x18,0x04,0x04,0x02,0x02,0x04,0x04,0x04,0x04,
		0x08,0x08,0x10,0x10,0x20,0x20,0x40,0x80,0x80,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x80,0x60,0x18,0x04,0x04,0x02,0x02,0x04,0x04,0x04,0x04,0x08,0x08,0x10,0x10,
		0x20,0x20,0x40,0x80,0x80,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x80,0x60,0x18,0x04,
		0x04,0x02,0x02,0x04,0x04,0x04,0x04,0x08,0x08,0x10,0x10,0x20,0x20,0x40,0x80,0x80,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x0C,0x03,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x01,0x01,0x02,
		0x02,0x04,0x04,0x08,0x0C,0x03,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x01,0x01,0x02,0x02,0x04,0x04,0x08,
		0x0C,0x03,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x01,0x01,0x02,0x02,0x04,0x04,0x0C,0x03,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x01,0x01,0x02,0x02,0x04,0x04,0x08,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00
};
void LCDDrawRipVolt(SYSRANK RipVRank, uint8_t ui8Col)
{
	switch(RipVRank)
	{
	case NO_DETECT:

		st7565DrawUprHalfScrnCrankVAnimation(ui8RoippleVoWave, ui8Col);

		break;
	case NORMAL:

		st7565DrawUprHalfScrnCrankVAnimation(ui8RoippleVNormal, ui8Col);

		break;
	case HIGH:

		st7565DrawUprHalfScrnCrankVAnimation(ui8RoippleVHigh, ui8Col);

		break;
	default:

		//ERROR!!!

		break;

	}
}

uint8_t DrawPageInfo(uint8_t Sel, uint8_t total_item_num, uint8_t num_per_page)
{
	tRectangle sTemp;
	uint8_t page;

	// draw total page
	page = ((total_item_num-1)/num_per_page)+1;
	switch(page)
	{
	case 1:
		sTemp.i16XMin = 119;		sTemp.i16XMax = 119;
		sTemp.i16YMin = 59;		sTemp.i16YMax = 59;
		ST7565RectReverse(&sTemp, VERTICAL);
		sTemp.i16XMin = 120;		sTemp.i16XMax = 120;
		sTemp.i16YMin = 58;		sTemp.i16YMax = 61;
		ST7565RectReverse(&sTemp, VERTICAL);
		sTemp.i16XMin = 119;		sTemp.i16XMax = 121;
		sTemp.i16YMin = 62;		sTemp.i16YMax = 62;
		ST7565RectReverse(&sTemp, VERTICAL);
		break;
	case 2:
		// draw 2
		sTemp.i16XMin = 118;		sTemp.i16XMax = 118;
		sTemp.i16YMin = 59;		sTemp.i16YMax = 59;
		ST7565RectReverse(&sTemp, VERTICAL);
		sTemp.i16XMin = 119;		sTemp.i16XMax = 120;
		sTemp.i16YMin = 58;		sTemp.i16YMax = 58;
		ST7565RectReverse(&sTemp, VERTICAL);
		sTemp.i16XMin = 121;		sTemp.i16XMax = 121;
		sTemp.i16YMin = 59;		sTemp.i16YMax = 59;
		ST7565RectReverse(&sTemp, VERTICAL);
		sTemp.i16XMin = 120;		sTemp.i16XMax = 120;
		sTemp.i16YMin = 60;		sTemp.i16YMax = 60;
		ST7565RectReverse(&sTemp, VERTICAL);
		sTemp.i16XMin = 119;		sTemp.i16XMax = 119;
		sTemp.i16YMin = 61;		sTemp.i16YMax = 61;
		ST7565RectReverse(&sTemp, VERTICAL);
		sTemp.i16XMin = 118;		sTemp.i16XMax = 121;
		sTemp.i16YMin = 62;		sTemp.i16YMax = 62;
		ST7565RectReverse(&sTemp, VERTICAL);
		break;
	case 3:
		// draw 3
		sTemp.i16XMin = 119;		sTemp.i16XMax = 119;
		sTemp.i16YMin = 59;		sTemp.i16YMax = 59;
		ST7565RectReverse(&sTemp, VERTICAL);
		sTemp.i16XMin = 120;		sTemp.i16XMax = 121;
		sTemp.i16YMin = 58;		sTemp.i16YMax = 58;
		ST7565RectReverse(&sTemp, VERTICAL);
		sTemp.i16XMin = 122;		sTemp.i16XMax = 122;
		sTemp.i16YMin = 59;		sTemp.i16YMax = 59;
		ST7565RectReverse(&sTemp, VERTICAL);
		sTemp.i16XMin = 121;		sTemp.i16XMax = 121;
		sTemp.i16YMin = 60;		sTemp.i16YMax = 60;
		ST7565RectReverse(&sTemp, VERTICAL);
		sTemp.i16XMin = 122;		sTemp.i16XMax = 122;
		sTemp.i16YMin = 61;		sTemp.i16YMax = 61;
		ST7565RectReverse(&sTemp, VERTICAL);
		sTemp.i16XMin = 120;		sTemp.i16XMax = 121;
		sTemp.i16YMin = 62;		sTemp.i16YMax = 62;
		ST7565RectReverse(&sTemp, VERTICAL);		
		sTemp.i16XMin = 119;		sTemp.i16XMax = 119;
		sTemp.i16YMin = 61;		sTemp.i16YMax = 61;
		ST7565RectReverse(&sTemp, VERTICAL);
		break;
	case 4:
		sTemp.i16XMin = 119;		sTemp.i16XMax = 119;
		sTemp.i16YMin = 60;		sTemp.i16YMax = 60;
		ST7565RectReverse(&sTemp, VERTICAL);
		sTemp.i16XMin = 120;		sTemp.i16XMax = 120;
		sTemp.i16YMin = 59;		sTemp.i16YMax = 59;
		ST7565RectReverse(&sTemp, VERTICAL);
		sTemp.i16XMin = 121;		sTemp.i16XMax = 121;
		sTemp.i16YMin = 58;		sTemp.i16YMax = 60;
		ST7565RectReverse(&sTemp, VERTICAL);
		sTemp.i16XMin = 119;		sTemp.i16XMax = 122;
		sTemp.i16YMin = 61;		sTemp.i16YMax = 61;
		ST7565RectReverse(&sTemp, VERTICAL);
		sTemp.i16XMin = 121;		sTemp.i16XMax = 121;
		sTemp.i16YMin = 62;		sTemp.i16YMax = 62;
		ST7565RectReverse(&sTemp, VERTICAL);
		break;		
	case 5:
		sTemp.i16XMin = 119;		sTemp.i16XMax = 121;
		sTemp.i16YMin = 58;		sTemp.i16YMax = 58;
		ST7565RectReverse(&sTemp, VERTICAL);
		sTemp.i16XMin = 119;		sTemp.i16XMax = 119;
		sTemp.i16YMin = 59;		sTemp.i16YMax = 60;
		ST7565RectReverse(&sTemp, VERTICAL);
		sTemp.i16XMin = 120;		sTemp.i16XMax = 121;
		sTemp.i16YMin = 60;		sTemp.i16YMax = 60;
		ST7565RectReverse(&sTemp, VERTICAL);
		sTemp.i16XMin = 121;		sTemp.i16XMax = 121;
		sTemp.i16YMin = 61;		sTemp.i16YMax = 61;
		ST7565RectReverse(&sTemp, VERTICAL);
		sTemp.i16XMin = 119;		sTemp.i16XMax = 120;
		sTemp.i16YMin = 62;		sTemp.i16YMax = 62;
		ST7565RectReverse(&sTemp, VERTICAL);
		break;
	case 6:
		sTemp.i16XMin = 121;		sTemp.i16XMax = 121;
		sTemp.i16YMin = 58;		sTemp.i16YMax = 58;
		ST7565RectReverse(&sTemp, VERTICAL);
		sTemp.i16XMin = 120;		sTemp.i16XMax = 120;
		sTemp.i16YMin = 59;		sTemp.i16YMax = 59;
		ST7565RectReverse(&sTemp, VERTICAL);
		sTemp.i16XMin = 119;		sTemp.i16XMax = 121;
		sTemp.i16YMin = 60;		sTemp.i16YMax = 60;
		ST7565RectReverse(&sTemp, VERTICAL);
		sTemp.i16XMin = 119;		sTemp.i16XMax = 119;
		sTemp.i16YMin = 61;		sTemp.i16YMax = 61;
		ST7565RectReverse(&sTemp, VERTICAL);
		sTemp.i16XMin = 122;		sTemp.i16XMax = 122;
		sTemp.i16YMin = 61;		sTemp.i16YMax = 61;
		ST7565RectReverse(&sTemp, VERTICAL);
		sTemp.i16XMin = 120;		sTemp.i16XMax = 121;
		sTemp.i16YMin = 62;		sTemp.i16YMax = 62;
		ST7565RectReverse(&sTemp, VERTICAL);
		break;
	}

	page = (Sel/num_per_page)+1;
	switch(page)
	{
	case 1:
		// draw 1
		sTemp.i16XMin = 109;		sTemp.i16XMax = 109;
		sTemp.i16YMin = 59;		sTemp.i16YMax = 59;
		ST7565RectReverse(&sTemp, VERTICAL);
		sTemp.i16XMin = 110;		sTemp.i16XMax = 110;
		sTemp.i16YMin = 58;		sTemp.i16YMax = 61;
		ST7565RectReverse(&sTemp, VERTICAL);
		sTemp.i16XMin = 109;		sTemp.i16XMax = 111;
		sTemp.i16YMin = 62;		sTemp.i16YMax = 62;
		ST7565RectReverse(&sTemp, VERTICAL);		
		break;
	case 2:
		// draw 2
		sTemp.i16XMin = 108;		sTemp.i16XMax = 108;
		sTemp.i16YMin = 59;		sTemp.i16YMax = 59;
		ST7565RectReverse(&sTemp, VERTICAL);
		sTemp.i16XMin = 109;		sTemp.i16XMax = 110;
		sTemp.i16YMin = 58;		sTemp.i16YMax = 58;
		ST7565RectReverse(&sTemp, VERTICAL);
		sTemp.i16XMin = 111;		sTemp.i16XMax = 111;
		sTemp.i16YMin = 59;		sTemp.i16YMax = 59;
		ST7565RectReverse(&sTemp, VERTICAL);
		sTemp.i16XMin = 110;		sTemp.i16XMax = 110;
		sTemp.i16YMin = 60;		sTemp.i16YMax = 60;
		ST7565RectReverse(&sTemp, VERTICAL);
		sTemp.i16XMin = 109;		sTemp.i16XMax = 109;
		sTemp.i16YMin = 61;		sTemp.i16YMax = 61;
		ST7565RectReverse(&sTemp, VERTICAL);
		sTemp.i16XMin = 108;		sTemp.i16XMax = 111;
		sTemp.i16YMin = 62;		sTemp.i16YMax = 62;
		ST7565RectReverse(&sTemp, VERTICAL);
		break;
	case 3:
		// draw 3
		sTemp.i16XMin = 108;		sTemp.i16XMax = 108;
		sTemp.i16YMin = 59;		sTemp.i16YMax = 59;
		ST7565RectReverse(&sTemp, VERTICAL);
		sTemp.i16XMin = 109;		sTemp.i16XMax = 110;
		sTemp.i16YMin = 58;		sTemp.i16YMax = 58;
		ST7565RectReverse(&sTemp, VERTICAL);
		sTemp.i16XMin = 111;		sTemp.i16XMax = 111;
		sTemp.i16YMin = 59;		sTemp.i16YMax = 59;
		ST7565RectReverse(&sTemp, VERTICAL);
		sTemp.i16XMin = 110;		sTemp.i16XMax = 110;
		sTemp.i16YMin = 60;		sTemp.i16YMax = 60;
		ST7565RectReverse(&sTemp, VERTICAL);
		sTemp.i16XMin = 111;		sTemp.i16XMax = 111;
		sTemp.i16YMin = 61;		sTemp.i16YMax = 61;
		ST7565RectReverse(&sTemp, VERTICAL);
		sTemp.i16XMin = 109;		sTemp.i16XMax = 110;
		sTemp.i16YMin = 62;		sTemp.i16YMax = 62;
		ST7565RectReverse(&sTemp, VERTICAL);		
		sTemp.i16XMin = 108;		sTemp.i16XMax = 108;
		sTemp.i16YMin = 61;		sTemp.i16YMax = 61;
		ST7565RectReverse(&sTemp, VERTICAL);
		break;
	case 4:
		sTemp.i16XMin = 108;		sTemp.i16XMax = 108;
		sTemp.i16YMin = 60;		sTemp.i16YMax = 60;
		ST7565RectReverse(&sTemp, VERTICAL);
		sTemp.i16XMin = 109;		sTemp.i16XMax = 109;
		sTemp.i16YMin = 59;		sTemp.i16YMax = 59;
		ST7565RectReverse(&sTemp, VERTICAL);
		sTemp.i16XMin = 110;		sTemp.i16XMax = 110;
		sTemp.i16YMin = 58;		sTemp.i16YMax = 60;
		ST7565RectReverse(&sTemp, VERTICAL);
		sTemp.i16XMin = 108;		sTemp.i16XMax = 111;
		sTemp.i16YMin = 61;		sTemp.i16YMax = 61;
		ST7565RectReverse(&sTemp, VERTICAL);
		sTemp.i16XMin = 110;		sTemp.i16XMax = 110;
		sTemp.i16YMin = 62;		sTemp.i16YMax = 62;
		ST7565RectReverse(&sTemp, VERTICAL);
		break;		
	case 5:
		sTemp.i16XMin = 109;		sTemp.i16XMax = 111;
		sTemp.i16YMin = 58;		sTemp.i16YMax = 58;
		ST7565RectReverse(&sTemp, VERTICAL);
		sTemp.i16XMin = 109;		sTemp.i16XMax = 109;
		sTemp.i16YMin = 59;		sTemp.i16YMax = 60;
		ST7565RectReverse(&sTemp, VERTICAL);
		sTemp.i16XMin = 110;		sTemp.i16XMax = 111;
		sTemp.i16YMin = 60;		sTemp.i16YMax = 60;
		ST7565RectReverse(&sTemp, VERTICAL);
		sTemp.i16XMin = 111;		sTemp.i16XMax = 111;
		sTemp.i16YMin = 61;		sTemp.i16YMax = 61;
		ST7565RectReverse(&sTemp, VERTICAL);
		sTemp.i16XMin = 109;		sTemp.i16XMax = 110;
		sTemp.i16YMin = 62;		sTemp.i16YMax = 62;
		ST7565RectReverse(&sTemp, VERTICAL);
		break;
	case 6:
		sTemp.i16XMin = 110;		sTemp.i16XMax = 110;
		sTemp.i16YMin = 58;		sTemp.i16YMax = 58;
		ST7565RectReverse(&sTemp, VERTICAL);
		sTemp.i16XMin = 109;		sTemp.i16XMax = 109;
		sTemp.i16YMin = 59;		sTemp.i16YMax = 59;
		ST7565RectReverse(&sTemp, VERTICAL);
		sTemp.i16XMin = 108;		sTemp.i16XMax = 110;
		sTemp.i16YMin = 60;		sTemp.i16YMax = 60;
		ST7565RectReverse(&sTemp, VERTICAL);
		sTemp.i16XMin = 108;		sTemp.i16XMax = 108;
		sTemp.i16YMin = 61;		sTemp.i16YMax = 61;
		ST7565RectReverse(&sTemp, VERTICAL);
		sTemp.i16XMin = 111;		sTemp.i16XMax = 111;
		sTemp.i16YMin = 61;		sTemp.i16YMax = 61;
		ST7565RectReverse(&sTemp, VERTICAL);
		sTemp.i16XMin = 109;		sTemp.i16XMax = 110;
		sTemp.i16YMin = 62;		sTemp.i16YMax = 62;
		ST7565RectReverse(&sTemp, VERTICAL);
		break;
	}

	return page;

}



void
Lcd_selection_antiwhite(tRectangle sTemp){
	ST7565RectReverse(&sTemp, VERTICAL);
}



tRectangle
LCD_display(BT2200_LCD_t flow, uint8_t selection){
	char buf[100],string_buf[50] = {0};
	memset(buf,0,100);
	uint8_t i;
	uint8_t x_start = 1,x_end = 127,y = 0;
	uint8_t title_button_y = 0,pre_option_button_y = 0,title_option_gap = 6, option_gap = 3;
	uint8_t option_num_offset = 0;
	uint8_t option_num_of_column = 4;
	uint8_t display_option_size,virtual_keyboard_set_index,screen_index;
	uint32_t temp;
	bool set_option_num = 0,option_anti_white = 0;
	bool virtual_keyboard_set = 0, refresh_screen_set = 1;
	FULL_HALF_PAGE full_half_page = FULL;
	LANGUAGE lang = get_language_setting();
	SHOW_profile_t profile = {side_middle, 2, false};
	TEST_DATA* TD = Get_test_data();
	SYS_INFO* info = Get_system_info();
	tRectangle sTemp = {0,0,0,0},anti_stemp;
	const char* option1 = NULL;const char* option2 = NULL;
	const char* option3 = NULL;const char* option4 = NULL;
	const char* option5 = NULL;const char* option6 = NULL;
	const char* option7 = NULL;const char* option8 = NULL;
	const char** display_option[] = {&option1,&option2,&option3,&option4,&option5,&option6,&option7,&option8};
	char num;

	ST7565ClearScreen(BUFFER1);
	switch(flow){
	case LOGO:
		screen_index = 1;
		break;
	case MAIN:
		screen_index = 0;
		break;
	case SELECT_RATING:
		screen_index = LEFT_DOWN_ENT_UP;
		memcpy(buf, select_rating_list[lang], UTF8_str_size(select_rating_list[lang]));	
		break;
	case SCANNER:
		y = 8;
		screen_index = LEFT_DOWN_ENT_UP;
		memcpy(buf, scan_vin_code_list[lang], UTF8_str_size(scan_vin_code_list[lang]));	
		break;
	case SCANNER_SCANNING:
		screen_index = (!selection)? 117:118;
		break;
	case SCANNER_RESULT:
		screen_index = LEFT_ENT;
		memcpy(buf, vin_code_list[lang], UTF8_str_size(vin_code_list[lang]));	
		break;
	case SCANNER_INVALID:
		y = 14;
		screen_index = LEFT;
		memcpy(buf, invalid_vin_rescan_list[lang], UTF8_str_size(invalid_vin_rescan_list[lang]));	
		break;
	case SCANNER_INVALID_SCANNER_MODE:
		screen_index = NONE;
		sprintf(buf,"INVALID SCANNER OPERATION MODE");
		break;	
	case ABORT:
		y = 14;
		screen_index = 2;     // 20200906 dw edit  up + enter + down => left + up + enter + down
		memcpy(buf, abort_list[lang], UTF8_str_size(abort_list[lang]));	
		break;
	case VOLTAGE_HIGH:
		y = 14;
		screen_index = 7;
		memcpy(buf, voltage_high_list[lang], UTF8_str_size(voltage_high_list[lang]));	
		break;
	case BT_BATTERY_TYPE:
	case SS_BATTERY_TYPE:
		title_option_gap = 4;	
#if SLOVENCINA_en
		if(lang == sl_SL) title_option_gap = 3;
#endif
		
		screen_index = LEFT_DOWN_ENT_UP;
		memcpy(buf, battery_type_list[lang], UTF8_str_size(battery_type_list[lang]));	
		break;		
	case SET_CAPACITY:
		screen_index = LEFT_DOWN_ENT_UP_RIG_KB_SQUARE;
		memcpy(buf, set_capacity_list[lang], UTF8_str_size(set_capacity_list[lang]));	
		break;
	case JIS_CAPACITY:		
		screen_index = LEFT_DOWN_ENT_UP_RIG_KB;
		memcpy(buf, set_capacity_list[lang], UTF8_str_size(set_capacity_list[lang]));	
		break;	
	case BATTERY_IN_VEHICLE:
		y = 8;	
		screen_index = LEFT_DOWN_ENT_UP;
		memcpy(buf, battery_in_vehicle_list[lang], UTF8_str_size(battery_in_vehicle_list[lang]));	
		break;
	case CONNECT_TO_BAT:
		screen_index = 28;
		memcpy(buf, direct_connect_to_battery_list[lang], UTF8_str_size(direct_connect_to_battery_list[lang]));	
		break;
	case IS_6V_BATTERY:
		y = 8;
		screen_index = LEFT_DOWN_ENT_UP;
		memcpy(buf, is_it_6v_list[lang], UTF8_str_size(is_it_6v_list[lang]));	
		break;	
	case IS_BATTERY_CHARGED:
		y = 8;
		screen_index = LEFT_DOWN_ENT_UP;
		memcpy(buf, is_battery_charge_list[lang], UTF8_str_size(is_battery_charge_list[lang]));	
		break;			
	case INSERT_VIN_MANUALLY:
		y = 6;	
		screen_index = LEFT_DOWN_ENT_UP;
		memcpy(buf, insert_vin_list[lang], UTF8_str_size(insert_vin_list[lang]));	
		break;

	case ABORT_VIN_INPUT:
		y = 4;
		
#if TURKU_en
		if(lang == tk_TR){
			y = 1;
			profile.row_gap = 1;
		}
#endif
#if LATVIESU_en
		if(lang == la_LA){
			y = 1;
			profile.row_gap = 1;
		}
#endif	
#if EAAHNIKA_en
		if(lang == ea_GK){
			profile.row_gap = 1;
		}
#endif

		title_option_gap = 4;
		screen_index = LEFT_DOWN_ENT_UP;
		memcpy(buf, test_not_suitable_list[lang], UTF8_str_size(test_not_suitable_list[lang]));	
		break;
		
	case VIN_INPUT_VIRTUAL_KEYBOARD:
		if(selection){
			refresh_screen_set = 0;
			virtual_keyboard_set = 1;
			virtual_keyboard_set_index = 76+selection%2;
		}
		else{
			screen_index = 76;
			y = 4;
#if DEUTSCH_en
			if(lang == de_DE) y = 0;
#endif
#if ESPANOL_en
			if(lang == es_ES) y = 0;
#endif
#if ITALIAN_en
			if(lang == it_IT) y = 0;
#endif
#if NORSK_en
			if(lang == nl_NW) y = 0;
#endif
#if SVENSKA_en
			if(lang == sv_SW) y = 0;
#endif
#if SLOVENSCINA_en
			if(lang == sls_SLJ) y = 0;
#endif
#if LIMBA_ROMANA_en
			if(lang == lm_ML) y = 0;
#endif
#if EAAHNIKA_en
			if(lang == ea_GK) y = 0;
#endif


		
			memcpy(buf, enter_vin_list[lang], UTF8_str_size(enter_vin_list[lang]));	
		}
		break;		
	case OTA_UPGRADE:
		y = 8;	
		screen_index = LEFT_DOWN_ENT_UP;
		memcpy(buf, ota_list[lang], UTF8_str_size(ota_list[lang]));	
		break;
	case OTA_UPGRADE_CANCEL:
		y = 8;
		screen_index = LEFT_DOWN_ENT_UP;
		memcpy(buf, upgrade_cancel_list[lang], UTF8_str_size(upgrade_cancel_list[lang]));	
		break;
	case OTA_CHECK_VERSION:
		y = 8;
		screen_index = NONE;
		memcpy(buf, check_version_list[lang], UTF8_str_size(check_version_list[lang]));	
		break;
	case OTA_UPGRADING:
		y = 8;
		screen_index = LEFT;
		memcpy(buf, upgrading_list[lang], UTF8_str_size(upgrading_list[lang]));	
		break;
	case OTA_DOWNLOADING:
		y = 6;
		screen_index = LEFT;
		memcpy(buf, downloading_list[lang], UTF8_str_size(downloading_list[lang]));	
		break;		
	case PRINT_RESULT:
		y = 8;
		screen_index = 2;    // 20200910 dw edit DOWN_ENT_UP=> left + DOWN_ENT_UP ;
		memcpy(buf, print_result_list[lang], UTF8_str_size(print_result_list[lang]));	
		break;
	case PRINT_SUCCESSFULLY:
		y = 5;
#if POLSKI_en
		if(lang == pl_PL){
			y = 1;
			profile.row_gap = 1;
			title_option_gap = 4;
		}
#endif
#if PUSSIJI_en
		if(lang == pu_RS){
			y = 1;
			profile.row_gap = 1;
			title_option_gap = 4;
		}
#endif
#if EAAHNIKA_en
		if(lang == ea_GK){
			y = 1;
			profile.row_gap = 1;
			title_option_gap = 4;
		}
#endif

	
		screen_index = 2;       // 20200906 dw edit Basemap button pattern replacement DOWN_ENT_UP >> DOWN_ENT_UP + left
		memcpy(buf, print_successfully_list[lang], UTF8_str_size(print_successfully_list[lang]));	
		break;
	case TESTING_PROCESSING_ARROW:
		screen_index = (selection == 0)? 34:35;
		break;
	case VOLTAGE_UNSTABLE:		
		y = 6;
#if PUSSIJI_en
	if(lang == pu_RS) y=2;
#endif
		screen_index = LEFT;
		memcpy(buf, voltage_unstable_list[lang], UTF8_str_size(voltage_unstable_list[lang]));	
		break;
	case CHECK_CLAMP:
		y = 44;
		screen_index = 52;
		temp = UTF8_str_size(check_clamp_list[lang]);
		memcpy(buf, check_clamp_list[lang], temp);	
		break;
	case REPLACE_INTERNAL_BATTERY:
		y = 42;
#if TURKU_en
		if(lang == tk_TR) y=39;
#endif
		screen_index = 53;
		temp = UTF8_str_size(insert_change_aa_list[lang]);
		memcpy(buf, insert_change_aa_list[lang], temp);	
		break;
	case INVALID_VIN:
		y = 4;
#if ITALIAN_en
	if(lang == it_IT) y = 10;
#endif
#if SVENSKA_en
	if(lang == sv_SW) profile.row_gap = 1;
#endif
#if CESTINA_en
	if(lang == cs_CZ){
		profile.row_gap = 1;
		y = 0;
	}
#endif
#if POLSKI_en
	if(lang == pl_PL){
		profile.row_gap = 1;
		title_option_gap = 4;
	}
#endif
#if SLOVENCINA_en
	if(lang == sl_SL){
		profile.row_gap = 1;
		y = 0;
		title_option_gap = 3;
	}
#endif
#if TURKU_en
	if(lang == tk_TR){
		profile.row_gap = 1;
		y = 0;
		title_option_gap = 2;
		option_gap = 2;
	}
#endif
#if LATVIESU_en
	if(lang == la_LA) profile.row_gap = 1;
#endif
#if PUSSIJI_en
	if(lang == pu_RS){
		profile.row_gap = 1;
		y = 0;
		title_option_gap = 3;
	}
#endif

		screen_index = DOWN_ENT_UP;
		temp = UTF8_str_size(invalid_vin_list[lang]);
		memcpy(buf, invalid_vin_list[lang], temp);	
		break;
	case SURFACE_CHARGE:
		y = 23;
		screen_index = 7;                      // 20201019 DW picture change to words : base button screen screen_index = 56 >> 7
		temp = UTF8_str_size(Turn_Headlight_list[lang]);   // 20201019 DW picture change to words : "ON" >> " TURN HEADLIGHTS ON FOR "
		memcpy(buf, Turn_Headlight_list[lang], temp);
		break;
	case DATE_TIME_SET:
		screen_index = 71;
		y = 42;
		snprintf(buf,sizeof(buf),"%04d/%02d/%02d  %02d:%02d:%02d",TD->year,TD->month,TD->date,TD->hour,TD->min,TD->sec);

		/*
		temp = UTF8_str_size(date_list[lang]);
		memcpy(buf, date_list[lang], temp);
		snprintf(buf+temp,sizeof(buf),"    ");
		temp += strlen(buf+temp);
		memcpy(buf+temp, time_list[lang],UTF8_str_size(time_list[lang]));
		*/
		break;
	case BACKLIGHT_SETTING:
		screen_index = 42;
		break;
	case BT_SS_TEST_RESULT:
		y = 0;
		profile.row_gap = 1;
		screen_index = LEFT_ENT;
		temp = UTF8_str_size(test_result_list[lang]);
		memcpy(buf, test_result_list[lang], temp);
		buf[temp] = '\n';
		switch(TD->Judge_Result){
		case TR_GOODnPASS:
			ST7650Green();
			memcpy(buf+temp+1, good_n_pass_list[lang], UTF8_str_size(good_n_pass_list[lang]));
			break;
		case TR_GOODnRRCHARGE:
			ST7650Green();
			memcpy(buf+temp+1, good_n_recharge_list[lang], UTF8_str_size(good_n_recharge_list[lang]));
			break;
		case TR_BADnREPLACE:
			ST7650Red();
			memcpy(buf+temp+1, bad_n_replace_list[lang], UTF8_str_size(bad_n_replace_list[lang]));
			break;
		case TR_CAUTION:			
			ST7650Yellow();
			memcpy(buf+temp+1, caution_list[lang], UTF8_str_size(caution_list[lang]));
			break;
		case TR_RECHARGEnRETEST:
			ST7650Yellow();
			memcpy(buf+temp+1, recharge_n_retest_list[lang], UTF8_str_size(recharge_n_retest_list[lang]));
			break;
		case TR_BAD_CELL_REPLACE:
			ST7650Red();
			memcpy(buf+temp+1, bad_cell_replace_list[lang], UTF8_str_size(bad_cell_replace_list[lang]));
			break;
        case TR_GOOD_PACK:
            ST7650Green();
            memcpy(buf+temp+1, GOOD_PACK_list[lang], UTF8_str_size(GOOD_PACK_list[lang]));
            break;
        case TR_CHECK_PACK:
            ST7650Red();
            memcpy(buf+temp+1, CHECK_PACK_list[lang], UTF8_str_size(CHECK_PACK_list[lang]));
            break;
		}
		break;
	case IR_TEST_RESULT:
		y = 4;
		screen_index = LEFT_ENT;
		memcpy(buf, test_result_list[lang], UTF8_str_size(test_result_list[lang]));
		break;
	case POINT_TO_BATTERY:
		y = 26;
#if POLSKI_en
		if(lang == pl_PL){			
			y = 25;
			profile.row_gap = 1;
		}
#endif
#if SLOVENCINA_en
		if(lang == sl_SL) profile.row_gap = 1;
#endif

		screen_index = (selection == 0)? 107:(selection == 1)? 108:109;
		memcpy(buf, point_to_battery_list[lang], UTF8_str_size(point_to_battery_list[lang]));
		break;
	case LOAD_ERROR:
		y = 23;
		screen_index = 4;    // 20200906 DW EDIT Modify the pattern combination of the LOAD ERROR widget button ENT >> LEFT + ENTER ;
		memcpy(buf, load_error_list[lang], UTF8_str_size(load_error_list[lang]));
		break;
	case TEST_CODE:
		screen_index = ENT;
		memcpy(buf, test_code_list[lang], UTF8_str_size(test_code_list[lang]));
		break;
	case VM_AM:
		screen_index = 67;//TODO/67 SLIDE
		break;
	case VIRTUAL_KB_ABC:
		if(selection){
			refresh_screen_set = 0;
			virtual_keyboard_set = 1;
			virtual_keyboard_set_index = 11+selection;
		}
		screen_index = 12;
		break;
	case VIRTUAL_KB_abc:
		if(selection){
			refresh_screen_set = 0;
			virtual_keyboard_set = 1;
			virtual_keyboard_set_index = 11+selection;
		}
		screen_index = 13;
		break;
	case VIRTUAL_KB_123:
		if(selection){
			refresh_screen_set = 0;
			virtual_keyboard_set = 1;
			virtual_keyboard_set_index = 11+selection;
		}
		screen_index = 14;
		break;
	case VIRTUAL_KB_SYM:
		if(selection){
			refresh_screen_set = 0;
			virtual_keyboard_set = 1;
			virtual_keyboard_set_index = 11+selection;
		}
		screen_index = 15;
		break;
	case NO_PAPER:
		screen_index = 80;
		break;
	case SYS_TURN_OFF_LOAD:
        profile.side = side_middle;
        y = 8;
	    screen_index = 7;
	    memcpy(buf, SYS_TURN_OFF_LOAD_list[lang], UTF8_str_size(SYS_TURN_OFF_LOAD_list[lang]));
	    break;
	case SYS_SWITCH_OFF:
		profile.side = side_left;
		y = 8;
		screen_index = 84;//TODO 84 SLIDE
		memcpy(buf, off_list[lang], UTF8_str_size(off_list[lang]));
		break;
	case SYS_SWITCH_ON:
		screen_index = 7;//TODO 7 SLIDE
		break;
	case SYS_CRANKING_RESULT:
		profile.side = side_left;
		y = 34;
#if PUSSIJI_en
		if(lang == pu_RS && TD->CrankVResult == NO_DETECT){
			y = 32;
			profile.row_gap = 1;
		}
#endif
		screen_index = LEFT_DOWN_ENT_UP;//TODO 86/87/88   SLIDE
		memcpy(buf, cranking_voltage_list[lang], UTF8_str_size(cranking_voltage_list[lang]));
		break;
	case SYS_CRANKING_LOWEST_RESULT:
		profile.side = side_left;
		y = 34;
		screen_index = LEFT_DOWN_ENT_UP;
		memcpy(buf, lowest_voltage_list[lang], UTF8_str_size(lowest_voltage_list[lang]));
		break;
	case SYS_IDLE_TEST_RESULT:
		profile.side = side_left;
		y = 34;
	
#if LIETUVIU_K_en
		if(lang == lt_LT) y = 31;
#endif	
#if SLOVENCINA_en
		if(lang == sl_SL) y = 32;
#endif	
		screen_index = 91;//TODO 91/92/93   SLIDE
		memcpy(buf, idle_voltage_list[lang], UTF8_str_size(idle_voltage_list[lang]));
		break;
	case TURN_ON_LOAD:
		//y = 8;
		screen_index = 4;   //20200803 DW EDIT 100 > 4
		//memcpy(buf, on_list[lang], UTF8_str_size(on_list[lang]));
		break;
	case RIPPLE_TEST:
		y = 36;
		//screen_index = (selection == 0)? 94:(selection == 1)? 95:96;//TODO/94/95/96 SLIDE
		//memcpy(buf, en_1000_rpm, UTF8_str_size(en_1000_rpm));
	    screen_index = 7; //TODO 4 SLIDE 20200803 DW EDIT
	    break;
	case RIPPLE_TEST_RESULT:
		profile.side = side_left;
		y = 34;
		screen_index = LEFT_ENT;//TODO/97/98/99 SLIDE
		memcpy(buf, ripple_voltage_list[lang], UTF8_str_size(ripple_voltage_list[lang]));
		break;
	case SYS_LOAD_TEST_RESULT:
		profile.side = side_left;
		y = 34;
		screen_index = 102;//TODO 102/103/104   SLIDE		
		memcpy(buf, load_voltage_list[lang], UTF8_str_size(load_voltage_list[lang]));
		break;		
    case IS_24V_SYSTEM: //20200806 DW EDIT
        y = 14;
        screen_index = 2;
        memcpy(buf, is_24v_system_list[lang], UTF8_str_size(is_24v_system_list[lang]));
        break;
    case TEST_EACH_BAT_SEP: //20200806 DW EDIT
        y = 14;
        screen_index = LEFT;
        memcpy(buf, test_each_bat_sep_list[lang], UTF8_str_size(test_each_bat_sep_list[lang]));
        break;
    case CONNECT_TO_12V_BAT: //20200806 DW EDIT
        y = 14;
        screen_index = LEFT;
        memcpy(buf, connect_to_12v_bat_list[lang], UTF8_str_size(connect_to_12v_bat_list[lang]));
        break;
    case IS_DIESEL_ENGINE: //20200807 DW EDIT
        y = 14;
        screen_index = 2;
        memcpy(buf, is_diesel_engine_list[lang], UTF8_str_size(is_diesel_engine_list[lang]));
        break;
    case Truck_Group_Test: //20200813 DW EDIT
        y = 14;
        screen_index = 2;
        memcpy(buf, Truck_Group_Test_list[lang], UTF8_str_size(Truck_Group_Test_list[lang]));
        break;
    case Of_Bat_In_Par:
        y = 8;
        if(lang == es_ES || lang == fr_FR) y = 0;
        screen_index = LEFT_DOWN_ENT_UP_PAGE;             // 20200906 dw edit　Modify pack test bat select widget button pattern combination　2  => LEFT_DOWN_ENT_UP_PAGE
        memcpy(buf, Of_Bat_In_Par_list[lang], UTF8_str_size(Of_Bat_In_Par_list[lang]));
        break;
    case SEP_PACK_CONNECT_BAT1: //20200817 DW EDIT
        y = 0;
        screen_index = 7;
        memcpy(buf, SEP_PACK_CONNECT_BAT1_list[lang], UTF8_str_size(SEP_PACK_CONNECT_BAT1_list[lang]));
        break;
    case SEP_PACK_CONNECT_BAT2: //20200817 DW EDIT
        y = 0;
        screen_index = 7;
        memcpy(buf, SEP_PACK_CONNECT_BAT2_list[lang], UTF8_str_size(SEP_PACK_CONNECT_BAT2_list[lang]));
        break;
    case SEP_PACK_CONNECT_BAT3: //20200817 DW EDIT
        y = 0;
        screen_index = 7;
        memcpy(buf, SEP_PACK_CONNECT_BAT3_list[lang], UTF8_str_size(SEP_PACK_CONNECT_BAT3_list[lang]));
        break;
    case SEP_PACK_CONNECT_BAT4: //20200817 DW EDIT
        y = 0;
        screen_index = 7;
        memcpy(buf, SEP_PACK_CONNECT_BAT4_list[lang], UTF8_str_size(SEP_PACK_CONNECT_BAT4_list[lang]));
        break;
    case SEP_PACK_CONNECT_BAT5: //20200817 DW EDIT
        y = 0;
        screen_index = 7;
        memcpy(buf, SEP_PACK_CONNECT_BAT5_list[lang], UTF8_str_size(SEP_PACK_CONNECT_BAT5_list[lang]));
        break;
    case SEP_PACK_CONNECT_BAT6: //20200817 DW EDIT
        y = 0;
        screen_index = 7;
        memcpy(buf, SEP_PACK_CONNECT_BAT6_list[lang], UTF8_str_size(SEP_PACK_CONNECT_BAT6_list[lang]));
        break;
    case GR_TEST_BAT1_START: //20200818 DW EDIT
        y = 0;
        screen_index = 4;
        memcpy(buf, GR_TEST_BAT1_START_list[lang], UTF8_str_size(GR_TEST_BAT1_START_list[lang]));
        break;
    case GR_TEST_BAT2_START: //20200818 DW EDIT
        y = 0;
        screen_index = 4;     // 20200916 DW Basemap button pattern replacement 7>>4
        memcpy(buf, GR_TEST_BAT2_START_list[lang], UTF8_str_size(GR_TEST_BAT2_START_list[lang]));
        break;
    case GR_TEST_BAT3_START: //20200818 DW EDIT
        y = 0;
        screen_index = 4;
        memcpy(buf, GR_TEST_BAT3_START_list[lang], UTF8_str_size(GR_TEST_BAT3_START_list[lang]));
        break;
    case GR_TEST_BAT4_START: //20200818 DW EDIT
        y = 0;
        screen_index = 4;
        memcpy(buf, GR_TEST_BAT4_START_list[lang], UTF8_str_size(GR_TEST_BAT4_START_list[lang]));
        break;
    case GR_TEST_BAT5_START: //20200818 DW EDIT
        y = 0;
        screen_index = 4;
        memcpy(buf, GR_TEST_BAT5_START_list[lang], UTF8_str_size(GR_TEST_BAT5_START_list[lang]));
        break;
    case GR_TEST_BAT6_START: //20200818 DW EDIT
        y = 0;
        screen_index = 4;
        memcpy(buf, GR_TEST_BAT6_START_list[lang], UTF8_str_size(GR_TEST_BAT6_START_list[lang]));
        break;
    case Pack_Report: //20200820 DW EDIT
        y = 0;
        screen_index = 2;     // BASE BUTTON SCREEN : LEFT + UP + ENTER + DOWN
        memcpy(buf, Pack_Report_list[lang], UTF8_str_size(Pack_Report_list[lang]));
        break;
    case BT_PACK_TEST_RESULT:    //20200820 DW EDIT
        y = 30;
        refresh_screen_set = 0;
        switch(TD->Judge_Result){
        case TR_GOODnPASS:
            ST7650Green();
            memcpy(buf, good_n_pass_list[lang], UTF8_str_size(good_n_pass_list[lang]));
            break;
        case TR_GOODnRRCHARGE:
            ST7650Green();
            memcpy(buf, good_n_recharge_list[lang], UTF8_str_size(good_n_recharge_list[lang]));
            break;
        case TR_BADnREPLACE:
            ST7650Red();
            memcpy(buf, bad_n_replace_list[lang], UTF8_str_size(bad_n_replace_list[lang]));
            break;
        case TR_CAUTION:
            ST7650Yellow();
            memcpy(buf, caution_list[lang], UTF8_str_size(caution_list[lang]));
            break;
        case TR_RECHARGEnRETEST:
            ST7650Yellow();
            memcpy(buf, recharge_n_retest_list[lang], UTF8_str_size(recharge_n_retest_list[lang]));
            break;
        case TR_BAD_CELL_REPLACE:
            ST7650Red();
            memcpy(buf, bad_cell_replace_list[lang], UTF8_str_size(bad_cell_replace_list[lang]));
            break;
        case TR_GOOD_PACK:
            ST7650Green();
            memcpy(buf, GOOD_PACK_list[lang], UTF8_str_size(GOOD_PACK_list[lang]));
            break;
        case TR_CHECK_PACK:
            ST7650Red();
            memcpy(buf, CHECK_PACK_list[lang], UTF8_str_size(CHECK_PACK_list[lang]));
            break;
        }
        break;
    case ADVISED_ACTION: //20200826 DW EDIT show advised action words
         y = 0;
         screen_index = 7;     // BASE BUTTON SCREEN : LEFT + UP + ENTER + DOWN
         memcpy(buf, ad_ac_press_enter_list[lang], UTF8_str_size(ad_ac_press_enter_list[lang]));
         break;
    case SYS_MAKE_SOUR_ALL:
         screen_index = 4;
         break;
    case Please_Enter_Start: // 20201020 dw Edit  Diesel engine flow add new screen
        y = 14;
        screen_index = 4;
        memcpy(buf, please_en_start_list[lang], UTF8_str_size(please_en_start_list[lang]));
        break;

        //
        //
	case SYS_TEST_RESULT:
		screen_index = LEFT_DOWN_ENT_UP_PAGE;
		memcpy(buf, test_result_list[lang], UTF8_str_size(test_result_list[lang]));
		break;
	case TEST_IN_VEHICLE:
		screen_index = LEFT_DOWN_ENT_UP;
#if LATVIESU_en
		if(lang == la_LA) title_option_gap = 3;
#endif
		memcpy(buf, test_in_vehicle_list[lang], UTF8_str_size(test_in_vehicle_list[lang]));
		break;
	case MEMORY_ERASE:		
		y = 8;
		screen_index = LEFT_DOWN_ENT_UP;
		memcpy(buf, memory_erase_list[lang], UTF8_str_size(memory_erase_list[lang]));
		break;
	case JUMP_START_POST:
#if LIETUVIU_K_en
	if(lang == lt_LT) profile.row_gap = 1;
#endif
		screen_index = LEFT_DOWN_ENT_UP_PAGE;
		memcpy(buf, jump_start_post_list[lang], UTF8_str_size(jump_start_post_list[lang]));
		break;
	case TIME_ZONE:
		screen_index = LEFT_DOWN_ENT_UP;
		memcpy(buf, time_zone_list[lang], UTF8_str_size(time_zone_list[lang]));
		break;
	case WARNING_CLAMPS_ON:		
		y = 6;
#if LATVIESU_en
		if(lang == la_LA) y = 3;
#endif
		screen_index = 4;        // 20200906 DW EDIT ENT >> 4 (LEFT + ENTER );
		memcpy(buf, keep_clamps_connect_list[lang], UTF8_str_size(keep_clamps_connect_list[lang]));
		break;
	case TEST_COUNTER:
#if LATVIESU_en
		if(lang == la_LA) title_option_gap = 4;
#endif	
		screen_index = 2;   // 20200906 dw edit button change LEFT_DOWN_ENT_UP_PAGE >> LEFT_DOWN_ENT_UP;
		memcpy(buf, test_counter_list[lang], UTF8_str_size(test_counter_list[lang]));
		break;
	case TEST_COUNTER_PRINT:
		screen_index = LEFT_DOWN_ENT_UP;
		temp = UTF8_str_size(test_counter_list[lang]);
		memcpy(buf, test_counter_list[lang], temp);
		buf[temp] = '\n';
		memcpy(buf+temp+1, print_q_list[lang], UTF8_str_size(print_q_list[lang]));		
		break;
	case TEST_COUNTER_ERASE:
		screen_index = LEFT_DOWN_ENT_UP;
		temp = UTF8_str_size(test_counter_list[lang]);
		memcpy(buf, test_counter_list[lang], temp);
		buf[temp] = '\n';
		memcpy(buf+temp+1, erase_list[lang], UTF8_str_size(erase_list[lang]));
		break;
	case PRINTING:		
		screen_index = (37+selection);
		break;
	case USB:
		screen_index = 74;
		break;
    case AMVM_TURN_ON:    // 20200803 DW EDIT
        screen_index = 4; // TODO/4 SLIDE LEFT & ENTER
        break;
	case WIFI_CHECK_CONNECTION:
		y = 14;
		screen_index = NONE;
		memcpy(buf, check_connection_list[lang], UTF8_str_size(check_connection_list[lang]));
		break;
	case WIFI_CONNECTION_CONNECT:
		screen_index = 112;
		profile.row_gap = 1;
		memcpy(buf, connection_status_list[lang], UTF8_str_size(connection_status_list[lang]));
		break;
	case WIFI_CONNECTION_LIMIT:
		screen_index = 113;
		profile.row_gap = 1;
		memcpy(buf, connection_status_list[lang], UTF8_str_size(connection_status_list[lang]));
		break;
	case WIFI_CONNECTION_DISCONNECT:
		screen_index = 114;
		profile.row_gap = 1;
		memcpy(buf, connection_status_list[lang], UTF8_str_size(connection_status_list[lang]));
		break;
	case WIFI_SETTING:
		title_option_gap = 3;
		screen_index = (info->WIFIPR)? LEFT_DOWN_ENT_UP:LEFT_ENT;
		memcpy(buf, wifi_list[lang], UTF8_str_size(wifi_list[lang]));
		break;
	case WIFI_LIST:
		screen_index = LEFT_DOWN_ENT_UP_PAGE;
		memcpy(buf, wifi_list_list[lang], UTF8_str_size(wifi_list_list[lang]));
		break;
	case WIFI_CONNECTING:		
		y = 14;
		screen_index = NONE;
		memcpy(buf, connecting_list[lang], UTF8_str_size(connecting_list[lang]));
		break;
	case SETTING:
#if DANSK_en
		if(lang == dk_DK){
			title_option_gap = 3;
			profile.row_gap = 1;
		}
#endif
#if TURKU_en
		if(lang == tk_TR) title_option_gap = 3;
#endif
#if SVENSKA_en
		if(lang == sv_SW){
			title_option_gap = 3;
			profile.row_gap = 1;
		}
#endif
#if CESTINA_en
		if(lang == cs_CZ) title_option_gap = 3;
#endif
#if SLOVENSCINA_en
		if(lang == sls_SLJ){
			title_option_gap = 3;
			profile.row_gap = 1;
		}
#endif
#if PUSSIJI_en
		if(lang == pu_RS){
			title_option_gap = 3;
			profile.row_gap = 1;
		}
#endif
#if EAAHNIKA_en
		if(lang == ea_GK){
			title_option_gap = 3;
			profile.row_gap = 1;
		}
#endif
		screen_index = LEFT_DOWN_ENT_UP_PAGE;
		memcpy(buf, setting_list[lang], UTF8_str_size(setting_list[lang]));
		break;
	case VERSION:
		screen_index = LEFT;
		memcpy(buf, version_list[lang], UTF8_str_size(version_list[lang]));
		break;
	case LANGUAGE_SELECT:
		screen_index = LEFT_DOWN_ENT_UP_PAGE;
		memcpy(buf, language_select_list[lang], UTF8_str_size(language_select_list[lang]));
		break;
	case SCAN_QR_CODE:
		y = 8;
		screen_index = LEFT_DOWN_ENT_UP;
		memcpy(buf, scan_qr_code_list[lang], UTF8_str_size(scan_qr_code_list[lang])); 
		break;
	case REGISTERING:
		y = 14;
		screen_index = NONE;
		memcpy(buf, registering_list[lang], UTF8_str_size(registering_list[lang]));
		break;
	}
	
	if(refresh_screen_set){
		LcdDrawScreen(screen_index, BUFFER1);
	}
	
	if(virtual_keyboard_set){
		ST7565ClearScreen(BUFFER0);
		LcdDrawScreenBuffer( virtual_keyboard_set_index, BUFFER0);
		ST7565DrawVirtualKybd(BUFFER0);
	}
	
	if(buf[0]) UTF8_LCM_show(x_start, y, buf, (x_end-x_start), &profile);
	sTemp.i16XMin = profile.region.x1;//x_start;
	sTemp.i16XMax = profile.region.x2;//x_end;
	sTemp.i16YMin = profile.region.y1;
	sTemp.i16YMax = profile.region.y2;
	title_button_y = (profile.region.y2+title_option_gap);//the end y of title region

	// set option
	switch(flow){
	case SCANNER:
	case ABORT:
	case BATTERY_IN_VEHICLE:
	case IS_6V_BATTERY:
	case IS_BATTERY_CHARGED:
	case INSERT_VIN_MANUALLY:
	case OTA_UPGRADE:
	case OTA_UPGRADE_CANCEL:
	case PRINT_RESULT:
	case PRINT_SUCCESSFULLY:
	case MEMORY_ERASE:
	case TEST_COUNTER_PRINT:
	case TEST_COUNTER_ERASE:
	case ABORT_VIN_INPUT:
	case SCAN_QR_CODE:
	case IS_24V_SYSTEM:                                 //20200806 DW Edit
	case IS_DIESEL_ENGINE:                              //20200807 dw edit
	case Truck_Group_Test:                              //20220813 dw Edit
		if(title_button_y < 28) title_button_y = 28;
		option_anti_white = true;
		profile.side = side_right;
		x_end = 101;
		option_gap = 5;
		option1 = yes_list[lang];
		option2 = no_list[lang];
		break;	
	case BT_BATTERY_TYPE:
		option_anti_white = true;
		profile.side = side_left;
		set_option_num = true;
		option_num_offset = (selection/4)*4;
		option1 = flooded_list[lang];
		option2 = agmf_list[lang];
		option3 = agms_list[lang];
		option4 = vrla_list[lang];                 // 20201021 David Wang BT2200_HD_BO "VRLA/GEL" => "VRLA / GEL"
		break;
	case SS_BATTERY_TYPE:
		option_anti_white = true;
		profile.side = side_left;
		set_option_num = true;
		option_num_offset = (selection/4)*4;	
		option1 = agmf_list[lang];
		option2 = en_efb;
		break;
	case SELECT_RATING:
		option_anti_white = true;
		profile.side = side_left;
		set_option_num = true;
		full_half_page = HALF;
		option_num_offset = (selection/8)*8;
#if TURKU_en
		if(lang == tk_TR) option_num_of_column = 3;
#endif
#if LATVIESU_en
		if(lang == la_LA) option_num_of_column = 3;
#endif
		x_end = 127;
		option1 = en_sae;
		option2 = en_ca;    // 20201016 EN CHANGE CA
		option3 = en_en2;
		option4 = en_iec;
		option5 = en_jis;
		option6 = en_din;
		break;
	case SET_CAPACITY:
		profile.side = side_right;
		x_end = 123;
		title_button_y = 36;
		if(selection == EN1) option1 = en_en;
		else if(selection == CA_MCA) option1 = en_ca;
		else if(selection == EN2) option1 = en_en2;
		else if(selection == IEC) option1 = en_iec;
		else if(selection == DIN) option1 = en_din;
		else option1 = en_cap_cca;                       // 20201016 dw only show cca unit for set capacity in BT2000_HD_BO
		break;	
	case INVALID_VIN:
		option_anti_white = true;
		profile.side = side_right;
		x_end = 120;		
#if CESTINA_en
		if(lang == cs_CZ) x_end = 125;
#endif
		option1 = input_vin_list[lang];
		option2 = abort_list[lang];
		break;
	case TEST_IN_VEHICLE:
		option_anti_white = true;
		profile.side = side_left;
		set_option_num = true;
		option_num_offset = (selection/4)*4;	
		option1 = direct_connect_list[lang];
		option2 = jump_start_post_list[lang];
		break;
	
	case WIFI_SETTING:
		profile.side = side_left;
		option_anti_white = true;
		temp = UTF8_str_size(wifi_power_list[lang]);
		memcpy(string_buf,wifi_power_list[lang],temp);
		string_buf[temp] = ' ';		
		memcpy(string_buf+temp+1,(info->WIFIPR)? on_list[lang]:off_list[lang],UTF8_str_size((info->WIFIPR)? on_list[lang]:off_list[lang]));
		option1 = string_buf;
		if(info->WIFIPR) option2 = wifi_list_list[lang];
		
		break;	
		
	case WIFI_CONNECTION_CONNECT:
	case WIFI_CONNECTION_LIMIT:		
		profile.side = side_middle;
		title_button_y = 36;
		option1 = wifi_list[lang];
		break;
	case SETTING:
		option_anti_white = true;
		profile.side = side_left;
		set_option_num = true;
		option_num_offset = (selection/4)*4;
		
		if(selection/4 == 0){
			option1 = back_light_list[lang];
			option2 = language_select_list[lang];
			option3 = clock_list[lang];
			option4 = information_list[lang];
		}
		else{
			option1 = test_counter_list[lang];
			option2 = version_list[lang];
		}
		break;

	case Of_Bat_In_Par: //20200814 DW EDIT
        option_anti_white = true;
        profile.side = side_left;
        set_option_num = true;

        option_num_offset = (selection/4)*4;

        if(selection/4 == 0){
            option1 = Bat_2_list[lang];
            option2 = Bat_3_list[lang];
            option3 = Bat_4_list[lang];
            option4 = Bat_5_list[lang];
        }
        else{
            option1 = Bat_6_list[lang];
        }
		break;

	default:
		goto out;
	}
	//***************************
	// Draw option
	//***************************
	display_option_size = sizeof(display_option)/4; 
	for(i = 0;i<display_option_size;i++){
		memset(buf,0,100);
		if(*display_option[i]){
			x_start = (full_half_page == FULL)? 1:(i/option_num_of_column == 0)? 1:64;
			y = (i%option_num_of_column == 0)? title_button_y:(pre_option_button_y + option_gap);
			
			if(set_option_num){
				num = ((option_num_offset+i+1) > 9)? ('A'+((option_num_offset+i)-9)):('0'+(option_num_offset+i)+1);
				sprintf(buf,"%c. ",num);
			}
			temp = strlen(buf);
			memcpy(buf+temp, *display_option[i], UTF8_str_size(*display_option[i]));
			UTF8_LCM_show(x_start, y, buf, (x_end-x_start), &profile);
			pre_option_button_y = profile.region.y2;
		}
		if((i+option_num_offset) == selection){
			anti_stemp.i16XMin = profile.region.x1 - ((option_anti_white)? 1:0);
			anti_stemp.i16XMax = profile.region.x2 + ((option_anti_white)? 1:0);
			anti_stemp.i16YMin = profile.region.y1-1;
			anti_stemp.i16YMax = profile.region.y2;
			if(option_anti_white) Lcd_selection_antiwhite(anti_stemp);
		}
	}	
	sTemp.i16XMin = profile.region.x1;
	sTemp.i16XMax = profile.region.x2;
	sTemp.i16YMin = profile.region.y1;
	sTemp.i16YMax = profile.region.y2;
	
out:
	//UARTprintf("LCD_display() return,Selection:%d\nx_min:%d\nx_max:%d\ny_min:%d\ny_max:%d\n",selection,sTemp.i16XMin,sTemp.i16XMax,sTemp.i16YMin,sTemp.i16YMax);
	return sTemp;

}



tRectangle LCD_draw_string(BT2200_LCD_t flow, uint8_t selection, uint8_t y)
{
	LANGUAGE LangIdx = get_language_setting();
	SHOW_profile_t profile = {side_left, 2, false};
	char buf[50];
	uint32_t temp;
	memset(buf,0,50);
	uint8_t x_start = 0,x_end = 127;
	tRectangle sTemp = {0,0,0,0};
	const char* string = NULL;
	TEST_DATA* TD = Get_test_data();
	SYS_PAR* par = Get_system_parameter();
	
	switch(flow){
	case MAIN:                                                       // 20200730 DW EDIT 6FUNCTION CHANGE 4FUNCTION
		if(selection == 0)	string = battery_test_list[LangIdx];
		else if(selection == 1)	string = system_test_list[LangIdx];
		else if(selection == 2) string = v_a_meter_list[LangIdx];
		else if(selection == 3) string = setting_list[LangIdx];
		else goto exit;		
		memcpy(buf, string, UTF8_str_size(string));
		UTF8_LCM_show(x_start, y, buf, (x_end-x_start), &profile);
		break;
	case SEC_SET:
		profile.side = side_middle;
		sprintf(buf," %d ",selection);
		temp = strlen(buf);
		memcpy(buf+temp, sec_list[LangIdx], UTF8_str_size(sec_list[LangIdx]));
		UTF8_LCM_show(x_start, y, buf, (x_end-x_start), &profile);		
		break;
	case BT_SS_TEST_RESULT:
		{
			bool voltage_display_at_new_line = true;
#if CESTINA_en
			if(LangIdx == cs_CZ && TD->Judge_Result == TR_BAD_CELL_REPLACE) voltage_display_at_new_line = false;
#endif
#if POLSKI_en
			if(LangIdx == pl_PL && TD->Judge_Result == TR_RECHARGEnRETEST) voltage_display_at_new_line = false;
#endif
#if SLOVENCINA_en
			if(LangIdx == sl_SL && TD->Judge_Result == TR_RECHARGEnRETEST) voltage_display_at_new_line = false;
#endif
#if TURKU_en
			if(LangIdx == tk_TR && (TD->Judge_Result == TR_CAUTION || TD->Judge_Result == TR_BAD_CELL_REPLACE)) y-=1;
#endif
#if LATVIESU_en
			if(LangIdx == la_LA && TD->Judge_Result == TR_CAUTION) y-=1;
#endif
#if LIMBA_ROMANA_en
			if(LangIdx == lm_ML && (TD->Judge_Result == TR_BAD_CELL_REPLACE || TD->Judge_Result == TR_RECHARGEnRETEST)) voltage_display_at_new_line = false;
#endif

			switch(( par->WHICH_TEST == Battery_Test )? par->BAT_test_par.SelectRating:par->SS_test_par.SSSelectRating){
            case EN1:
                snprintf(buf,sizeof(buf),"%s%3d%% %s %4d EN",soh_list[LangIdx],(uint8_t)TD->SOH,cca_list[LangIdx],(uint16_t)TD->Measured_CCA);
                break;
            case EN2:
                snprintf(buf,sizeof(buf),"%s%3d%% %s %4d EN",soh_list[LangIdx],(uint8_t)TD->SOH,cca_list[LangIdx],(uint16_t)TD->Measured_CCA);
                break;
            case IEC:
                snprintf(buf,sizeof(buf),"%s%3d%% %s %4d IEC",soh_list[LangIdx],(uint8_t)TD->SOH,cca_list[LangIdx],(uint16_t)TD->Measured_CCA);
                break;
            case DIN:
                snprintf(buf,sizeof(buf),"%s%3d%% %s %4d DIN",soh_list[LangIdx],(uint8_t)TD->SOH,cca_list[LangIdx],(uint16_t)TD->Measured_CCA);
                break;
            case CA_MCA:
                snprintf(buf,sizeof(buf),"%s%3d%% %s %4d CA",soh_list[LangIdx],(uint8_t)TD->SOH,cca_list[LangIdx],(uint16_t)TD->Measured_CCA);
                break;
            default:
                snprintf(buf,sizeof(buf),"%s%3d%% %s %4d CCA",soh_list[LangIdx],(uint8_t)TD->SOH,cca_list[LangIdx],(uint16_t)TD->Measured_CCA);
                break;
			}

			UTF8_LCM_show(x_start, y, buf, (x_end-x_start), &profile);			

			memset(buf,0,sizeof(buf));
			snprintf(buf,sizeof(buf),"%s%3d%% ",soc_list[LangIdx],(uint8_t)TD->SOC);
			if(voltage_display_at_new_line){
				UTF8_LCM_show(x_start, profile.region.y2+3, buf, (x_end-x_start), &profile);

				memset(buf,0,sizeof(buf));
				temp = UTF8_str_size(voltage_list[LangIdx]);
				memcpy(buf,voltage_list[LangIdx],temp);
			}
			else{
				temp = strlen(buf);
				memcpy(buf+temp,voltage_list[LangIdx],UTF8_str_size(voltage_list[LangIdx]));
				temp += UTF8_str_size(voltage_list[LangIdx]);
			}
			snprintf(buf+temp,sizeof(buf),"%5.2fV",TD->battery_test_voltage);
			UTF8_LCM_show(x_start, profile.region.y2+3, buf, (x_end-x_start), &profile);
			
			break;
		}
	case IR_TEST_RESULT:
		{
			TEST_DATA* TD = Get_test_data();
			string = voltage_list[LangIdx];
			temp = UTF8_str_size(string);
			memcpy(buf, string, temp);			
			snprintf(buf+temp,sizeof(buf),"  %5.2f V",TD->IRvoltage);			
			UTF8_LCM_show(x_start, y, buf, (x_end-x_start), &profile);			
			
			memset(buf,0,sizeof(buf));
			string = measured_list[LangIdx];
			temp = UTF8_str_size(string);
			memcpy(buf, string, temp);
			if((TD->IR > 999.99) || (TD->IR < 0.8)){
				snprintf(buf+temp,sizeof(buf)," ---.-- mΩ");
			}
			else{
				snprintf(buf+temp,sizeof(buf),"%6.2f mΩ",TD->IR);
			}
			UTF8_LCM_show(x_start, profile.region.y2+6, buf, (x_end-x_start), &profile);			
			break;
		}
	case CAR_BRAND:
		memcpy(buf, brand_list[LangIdx], UTF8_str_size(brand_list[LangIdx]));
		UTF8_LCM_show(x_start, y, buf, (x_end-x_start), &profile);
		break;
	case CAR_MODEL:
		memcpy(buf, model_list[LangIdx], UTF8_str_size(model_list[LangIdx]));
		UTF8_LCM_show(x_start, y, buf, (x_end-x_start), &profile);
		break;
	case CAR_VERSION:
		temp = UTF8_str_size(version_list[LangIdx]);
		memcpy(buf, version_list[LangIdx], temp);
		buf[temp] = ':';
		UTF8_LCM_show(x_start, y, buf, (x_end-x_start), &profile);
		break;
	case TEST_COUNTER:
		//if(selection == SSTEST) string = ss_test_list[LangIdx];
		if(selection == BATTEST) string = bat_test_list[LangIdx];
		else if(selection == SYSTEST) string = sys_test_list[LangIdx];
		//else if(selection == IRTESTCOUNTER) string = ir_t_list[LangIdx];
		else if(selection == PRINT) string = print_list[LangIdx];
		else goto exit;
		temp = UTF8_str_size(string);
		memcpy(buf, string,temp);
		if(selection!=PRINT) buf[temp] = ':';
		UTF8_LCM_show(x_start, y, buf, (x_end-x_start), &profile);
		break;
	case WIFI_SETTING:
		if(selection == 0){
			profile.side = side_right;
			string = off_list[LangIdx];
		}
		else if(selection == 1){
			profile.side = side_right;
			string = on_list[LangIdx];
		}
		else if(selection == 2)string = wifi_list_list[LangIdx];
		else goto exit;
		memcpy(buf, string, UTF8_str_size(string));
		UTF8_LCM_show(x_start, y, buf, (x_end-x_start), &profile);
		break;
	case WIFI_CONNECTING:
		memcpy(buf, connecting_list[LangIdx], UTF8_str_size(connecting_list[LangIdx]));
		UTF8_LCM_show(x_start, y, buf, (x_end-x_start), &profile);
		break;
	case WIFI_CONNECTION_CONNECT:
		profile.row_gap = 1;
		memcpy(buf, wifi_connected_list[LangIdx], UTF8_str_size(wifi_connected_list[LangIdx]));
		UTF8_LCM_show(x_start, y, buf, (x_end-x_start), &profile);
		break;
	case WIFI_CONNECTION_LIMIT:
		profile.row_gap = 1;
		memcpy(buf, limited_connection_list[LangIdx], UTF8_str_size(limited_connection_list[LangIdx]));
		UTF8_LCM_show(x_start, y, buf, (x_end-x_start), &profile);
		break;
	case WIFI_CONNECTION_DISCONNECT:
		profile.row_gap = 1;
		memcpy(buf, wifi_disconnect_list[LangIdx], UTF8_str_size(wifi_disconnect_list[LangIdx]));
		UTF8_LCM_show(x_start, y, buf, (x_end-x_start), &profile);
		break;
	case SYS_SWITCH_ON:          // 20200803 DW ADD
	    profile.side = side_middle;
	    memcpy(buf, off_load_list[LangIdx], UTF8_str_size(off_load_list[LangIdx]));
		UTF8_LCM_show(x_start, y, buf, (x_end-x_start), &profile);
		break;
	case SYS_CRANKING_RESULT:
		switch(TD->CrankVResult){
		case NO_DETECT:	string = no_detect_list[LangIdx];		break;
		case LOW:		string = low_list[LangIdx];				break;
		case NORMAL:	string = normal_list[LangIdx];			break;
		case HIGH:		string = high_list[LangIdx];			break;
		default:goto exit;
		}

		if(selection == 0){			
			if(TD->CrankVResult == NO_DETECT){
				profile.side = side_right;
				memcpy(buf,string,UTF8_str_size(string));
			}
			else{
				profile.side = side_middle;
				snprintf(buf,sizeof(buf),"%5.2f V  ",TD->CrankVol);
				temp = strlen(buf);
				memcpy(buf+temp,string,UTF8_str_size(string));
			}
		}
		else if(selection == 1){
			profile.side = side_right;
			snprintf(buf,sizeof(buf),"%5.2f V",TD->CrankVol);
		}
		else{
			profile.side = side_right;
			memcpy(buf,string,UTF8_str_size(string));
		}
		UTF8_LCM_show(x_start, y, buf, (x_end-x_start), &profile);
		break;
	case SYS_CRANKING_LOWEST_RESULT:
		profile.side = side_right;
		snprintf(buf,sizeof(buf),"%5.2f V",TD->Crank_vmin);
		UTF8_LCM_show(x_start, y, buf, (x_end-x_start), &profile);
		break;
	case SYS_IDLE_TEST_RESULT:
		switch(TD->AltIdleVResult){
		case NO_DETECT:	string = no_detect_list[LangIdx];		break;
		case LOW:		string = low_list[LangIdx];				break;
		case NORMAL:	string = normal_list[LangIdx];			break;
		case HIGH:		string = high_list[LangIdx];			break;
		default:goto exit;
		}
		if(selection == 0){			
			profile.side = side_middle;
			snprintf(buf,sizeof(buf),"%5.2f V  ",TD->IdleVol);
			temp = strlen(buf);
			memcpy(buf+temp,string,UTF8_str_size(string));
		}
		else if(selection == 1){
			profile.side = side_right;
			snprintf(buf,sizeof(buf),"%5.2f V",TD->IdleVol);
		}
		else{
			profile.side = side_right;
			memcpy(buf,string,UTF8_str_size(string));
		}
		UTF8_LCM_show(x_start, y, buf, (x_end-x_start), &profile);
		break;
	case RIPPLE_TEST_RESULT:
		switch(TD->RipVResult){
		case NO_DETECT:	string = no_detect_list[LangIdx];		break;
		case LOW:		string = low_list[LangIdx];				break;
		case NORMAL:	string = normal_list[LangIdx];			break;
		case HIGH:		string = high_list[LangIdx];			break;
		default:goto exit;
		}

		if(selection == 0){			
			if(TD->RipVResult == NO_DETECT){
				profile.side = side_right;
				memcpy(buf,string,UTF8_str_size(string));
			}
			else{
				profile.side = side_middle;
				snprintf(buf,sizeof(buf),"%5.2f V  ",TD->ripple);
				temp = strlen(buf);
				memcpy(buf+temp,string,UTF8_str_size(string));
			}
		}
		else if(selection == 1){
			profile.side = side_right;
			snprintf(buf,sizeof(buf),"%5.2f V",TD->ripple);
		}
		else{
			profile.side = side_right;
			memcpy(buf,string,UTF8_str_size(string));
		}
		UTF8_LCM_show(x_start, y, buf, (x_end-x_start), &profile);
		break;
	case SYS_LOAD_TEST_RESULT:
		switch(TD->AltLoadVResult){
		case NO_DETECT:	string = no_detect_list[LangIdx];		break;
		case LOW:		string = low_list[LangIdx];				break;
		case NORMAL:	string = normal_list[LangIdx];			break;
		case HIGH:		string = high_list[LangIdx];			break;
		default:goto exit;
		}
		if(selection == 0){			
			profile.side = side_middle;
			snprintf(buf,sizeof(buf),"%5.2f V  ",TD->LoadVol);
			temp = strlen(buf);
			memcpy(buf+temp,string,UTF8_str_size(string));
		}
		else if(selection == 1){
			profile.side = side_right;
			snprintf(buf,sizeof(buf),"%5.2f V",TD->LoadVol);
		}
		else{
			profile.side = side_right;
#if CESTINA_en
			if(LangIdx == cs_CZ) y -= 2;
#endif
#if SLOVENCINA_en
			if(LangIdx == sl_SL) y -= 2;
#endif
#if SLOVENSCINA_en
			if(LangIdx == sls_SLJ) y -= 2;
#endif

			memcpy(buf,string,UTF8_str_size(string));
		}
		UTF8_LCM_show(x_start, y, buf, (x_end-x_start), &profile);
		break;

	case SYS_TEST_RESULT:
		{
			uint8_t show_y = y;
			profile.row_gap = 1;
			if(!selection){
				memcpy(buf, cranking_voltage_list[LangIdx], UTF8_str_size(cranking_voltage_list[LangIdx]));
				UTF8_LCM_show(x_start, show_y, buf, (x_end-x_start), &profile);
				memset(buf,0,50);
				if(TD->CrankVResult == NO_DETECT){
					snprintf(buf,sizeof(buf),"--.-- V");
				}
				else{
					snprintf(buf,sizeof(buf),"%5.2f V",TD->CrankVol);
				}
				profile.side = side_right;
				show_y = ((profile.region.y2-profile.region.y1) > 12)? ((profile.region.y2+profile.region.y1)/2+3):(profile.region.y2+3);
				UTF8_LCM_show(2,show_y, buf, 125, &profile);	

				memset(buf,0,50);
				memcpy(buf, idle_voltage_list[LangIdx], UTF8_str_size(idle_voltage_list[LangIdx]));
				show_y = profile.region.y2+4;
				profile.side = side_left;
				UTF8_LCM_show(x_start, show_y, buf, (x_end-x_start), &profile);
				memset(buf,0,50);
				snprintf(buf,sizeof(buf),"%5.2f V",TD->IdleVol);
				profile.side = side_right;
				show_y = ((profile.region.y2-profile.region.y1) > 12)? ((profile.region.y2+profile.region.y1)/2+3):(profile.region.y2+3);		
#if SOOMI_en
				if(LangIdx == sm_FN) show_y = profile.region.y1;
#endif
				UTF8_LCM_show(2,show_y, buf, 125, &profile); 

			}
			else{
				memcpy(buf, ripple_voltage_list[LangIdx], UTF8_str_size(ripple_voltage_list[LangIdx]));
				UTF8_LCM_show(x_start, show_y, buf, (x_end-x_start), &profile);
				memset(buf,0,50);
				if(TD->RipVResult == NO_DETECT){
					snprintf(buf,sizeof(buf),"--.-- V");
				}
				else{
					snprintf(buf,sizeof(buf),"%5.2f V",TD->ripple);
				}
				profile.side = side_right;
				show_y = ((profile.region.y2-profile.region.y1) > 12)? ((profile.region.y2+profile.region.y1)/2+3):(profile.region.y2+3);
				UTF8_LCM_show(2,show_y, buf, 125, &profile); 
				
				memset(buf,0,50);
				memcpy(buf, load_voltage_list[LangIdx], UTF8_str_size(load_voltage_list[LangIdx]));
				profile.side = side_left;
				show_y = profile.region.y2+4;
#if CESTINA_en
				//if(LangIdx == cs_CZ) show_y = profile.region.y2+2;
#endif
				UTF8_LCM_show(x_start, show_y, buf, (x_end-x_start), &profile);
				memset(buf,0,50);
				snprintf(buf,sizeof(buf),"%5.2f V",TD->LoadVol);
				profile.side = side_right;
				show_y = ((profile.region.y2-profile.region.y1) > 12)? ((profile.region.y2+profile.region.y1)/2+3):(profile.region.y2+3);
#if DANSK_en
				if(LangIdx == dk_DK) show_y = profile.region.y1;
#endif
				UTF8_LCM_show(2, show_y, buf, 125, &profile); 
			}	
			break;
		}
	case LANGUAGE_SELECT:
		x_start = 12;
		memcpy(buf, language_list[selection].string, UTF8_str_size(language_list[selection].string));
		UTF8_LCM_show(x_start, y, buf, (x_end-x_start), &profile);
		break;
	case WIFI_REFRESH_LIST:
		memcpy(buf, wifi_refresh_list[LangIdx], UTF8_str_size(wifi_refresh_list[LangIdx]));		
		UTF8_LCM_show(x_start, y, buf, (x_end-x_start), &profile);
		break;
	case SURFACE_CHARGE:
		profile.side = side_middle;
		sprintf(buf," %d ",selection);
		temp = strlen(buf);
		memcpy(buf+temp, sec_list[LangIdx], UTF8_str_size(sec_list[LangIdx]));
		UTF8_LCM_show(x_start, y, buf, (x_end-x_start), &profile);		
		break;
    case SYS_START_ENGIN: // 20200803 DW EDIT
        profile.side = side_middle;
        memcpy(buf, start_engine_list[LangIdx], UTF8_str_size(start_engine_list[LangIdx]));
        UTF8_LCM_show(x_start, y, buf, (x_end-x_start), &profile);
        break;
    case SYS_MAKE_SOUR_ALL: // 20200803 DW EDIT
        profile.side = side_middle;
        memcpy(buf, make_sure_all_list[LangIdx], UTF8_str_size(make_sure_all_list[LangIdx]));
        UTF8_LCM_show(x_start, y, buf, (x_end-x_start), &profile);
        break;
    case SYS_LOADS_ARE_OFF: // 20200803 DW EDIT
        profile.side = side_middle;
        memcpy(buf, load_are_off_list[LangIdx], UTF8_str_size(load_are_off_list[LangIdx]));
        UTF8_LCM_show(x_start, y, buf, (x_end-x_start), &profile);
        break;
    case SYS_TURN_ON_LO: // 20200803 DW EDIT
        profile.side = side_middle;
        memcpy(buf, turn_on_lo_list[LangIdx], UTF8_str_size(turn_on_lo_list[LangIdx]));
        UTF8_LCM_show(x_start, y, buf, (x_end-x_start), &profile);
        break;
    case SYS_AND_PRESS_ENT: // 20200803 DW EDIT
        profile.side = side_middle;
        memcpy(buf, and_press_ent_list[LangIdx], UTF8_str_size(and_press_ent_list[LangIdx]));
        UTF8_LCM_show(x_start, y, buf, (x_end-x_start), &profile);
        break;
    case SYS_RUN_ENG_2500: // 20200803 DW EDIT
        profile.side = side_middle;
        memcpy(buf, turn_eng_2500_list[LangIdx], UTF8_str_size(turn_eng_2500_list[LangIdx]));
        UTF8_LCM_show(x_start, y, buf, (x_end-x_start), &profile);
        break;
    case AMVM_TURN_ON_AMP: // 20200803 DW EDIT
        profile.side = side_middle;
        memcpy(buf, AMVM_TURN_ON_AMP_list[LangIdx], UTF8_str_size(AMVM_TURN_ON_AMP_list[LangIdx]));
        UTF8_LCM_show(x_start, y, buf, (x_end-x_start), &profile);
        break;
    case AMVM_AFTER_ATT: // 20200803 DW EDIT
        profile.side = side_middle;
        memcpy(buf, AMVM_AFTER_ATT_list[LangIdx], UTF8_str_size(AMVM_AFTER_ATT_list[LangIdx]));
        UTF8_LCM_show(x_start, y, buf, (x_end-x_start), &profile);
        break;
    case AMVM_ENTER_PROCEED: // 20200803 DW EDIT
        profile.side = side_middle;
        memcpy(buf, AMVM_ENTER_PROCEED_list[LangIdx], UTF8_str_size(AMVM_ENTER_PROCEED_list[LangIdx]));
        UTF8_LCM_show(x_start, y, buf, (x_end-x_start), &profile);
        break;
    case AMVM_TURN_OFF_AMP: // 20200803 DW EDIT
        profile.side = side_middle;
        memcpy(buf, AMVM_TURN_OFF_AMP_list[LangIdx], UTF8_str_size(AMVM_TURN_OFF_AMP_list[LangIdx]));
        UTF8_LCM_show(x_start, y, buf, (x_end-x_start), &profile);
        break;
    case AMVM_AMP_METER: // 20200803 DW EDIT
        profile.side = side_middle;
        memcpy(buf, AMVM_AMP_METER_list[LangIdx], UTF8_str_size(AMVM_AMP_METER_list[LangIdx]));
        UTF8_LCM_show(x_start, y, buf, (x_end-x_start), &profile);
        break;
    case REV_ENGINE_FOR: //20200807 DW EDIT
        profile.side = side_middle;
        memcpy(buf, rev_engine_for_list[LangIdx], UTF8_str_size(rev_engine_for_list[LangIdx]));
        UTF8_LCM_show(x_start, y, buf, (x_end-x_start), &profile);
        break;
    case BAT_NUM:       //20200820 DW EDIT
        profile.side = side_middle;
        sprintf(buf,"%s %d",BAT_NUM_list[LangIdx],selection);
        //temp = strlen(buf);
        //memcpy(buf+temp, BAT_NUM_list[LangIdx], UTF8_str_size(BAT_NUM_list[LangIdx]));
        UTF8_LCM_show(x_start, y, buf, (x_end-x_start), &profile);
        break;
        //
	default:
		goto exit;
	}
	
	sTemp.i16XMin = x_start;
	sTemp.i16XMax = profile.region.x2;
	sTemp.i16YMin = profile.region.y1;
	sTemp.i16YMax = profile.region.y2;
exit:
	UARTprintf("LCD_draw_string() return,Selection:%d\nx_min:%d\nx_max:%d\ny_min:%d\ny_max:%d\n",selection,sTemp.i16XMin,sTemp.i16XMax,sTemp.i16YMin,sTemp.i16YMax);
	return sTemp;

}


LANGUAGE get_select_language(uint8_t parameter){
	return language_list[parameter].lan;
}



void Print_String_by_language(BT2200_PRINT_t item, uint8_t parameter){
	SHOW_profile_t profile = {.side = side_middle, .row_gap = 10 , .Print_spec = {2,3,1} };  // 20201020 DW Enlarged print fonts >> .Print_spec = {1,3,1} => {2,3,1}
	uint32_t temp;
	LANGUAGE lang = get_language_setting();
	char buf[300];
	memset(buf,0,300);

	SYS_PAR* par = Get_system_parameter();
	TEST_DATA* TD = Get_test_data();
	SYS_INFO* info = Get_system_info();

	switch(item){
	case P_LOGO:
		break;
	case P_TEST_REPORT:
		temp = UTF8_str_size(prt_test_report_list[lang]);
		memcpy(buf, prt_test_report_list[lang], temp);
		UTF8_Printer_show(buf, &profile);
		break;
	case P_TEST_TYPE:
		switch(par->WHICH_TEST){
			case Start_Stop_Test:
				temp = UTF8_str_size(prt_start_stop[lang]);
				memcpy(buf, prt_start_stop[lang], temp);
				break;
			case Battery_Test:
			    if( TD->PackTestNum >1 && TD->Judge_Result != TR_GOOD_PACK ){ // 20200821 dw edit BT2200_HD_BO_USA
			        temp = UTF8_str_size(prt_pack_test[lang]);                // pack test good pack
			        memcpy(buf, prt_pack_test[lang], temp);                   // end
			    }
			    else{
			        temp = UTF8_str_size(prt_battery_test[lang]);
				    memcpy(buf, prt_battery_test[lang], temp);
			    }
				break;
			/*case System_Test:
				temp = UTF8_str_size(prt_sys[lang]);
				memcpy(buf, prt_sys[lang], temp);
				break;*/
			case IR_Test:
				temp = UTF8_str_size(prt_ir_test[lang]);
				memcpy(buf, prt_ir_test[lang], temp);
				break;
		}
		UTF8_Printer_show(buf, &profile);
		break;
	case P_TEST_TYPE_BT:
	case P_TEST_TYPE_SS:
	case P_TEST_TYPE_SYS:
	case P_TEST_TYPE_IR:
		break;
	case P_TEST_RESULT:
		if(par->WHICH_TEST == Start_Stop_Test || par->WHICH_TEST == Battery_Test){
			switch(TD->Judge_Result){
			case TR_GOODnPASS:
				temp = UTF8_str_size(good_n_pass_list[lang]);
				memcpy(buf, good_n_pass_list[lang], temp);
				break;
			case TR_GOODnRRCHARGE:
				temp = UTF8_str_size(good_n_recharge_list[lang]);
				memcpy(buf, good_n_recharge_list[lang], temp);
				break;
			case TR_BADnREPLACE:
				temp = UTF8_str_size(bad_n_replace_list[lang]);
				memcpy(buf, bad_n_replace_list[lang], temp);
				break;
			case TR_CAUTION:
				temp = UTF8_str_size(caution_list[lang]);
				memcpy(buf, caution_list[lang], temp);
				break;
			case TR_RECHARGEnRETEST:
				temp = UTF8_str_size(recharge_n_retest_list[lang]);
				memcpy(buf, recharge_n_retest_list[lang], temp);
				break;
			case TR_BAD_CELL_REPLACE:
				temp = UTF8_str_size(bad_cell_replace_list[lang]);
				memcpy(buf, bad_cell_replace_list[lang], temp);
				break;
            case TR_GOOD_PACK:     //20200821   DW EDIT FOR BT2200_HD_BO_USA
                temp = UTF8_str_size(GOOD_PACK_list[lang]);
                memcpy(buf, GOOD_PACK_list[lang], temp);
                break;
            case TR_CHECK_PACK:     //20200821   DW EDIT FOR BT2200_HD_BO_USA
                temp = UTF8_str_size(CHECK_PACK_list[lang]);
                memcpy(buf, CHECK_PACK_list[lang], temp);
                break;
			}
			UTF8_Printer_show(buf, &profile);
		}
		break;
	case P_BATTERY_TYPE:
		switch(( par->WHICH_TEST == Battery_Test )? par->BAT_test_par.BatteryType:par->SS_test_par.SSBatteryType){
		case MAP_FLOODED:
			temp = UTF8_str_size(flooded_list[lang]);
			memcpy(buf, flooded_list[lang], temp);
			break;
		case MAP_AGM_FLAT_PLATE:
			temp = UTF8_str_size(agmf_list[lang]);
			memcpy(buf, agmf_list[lang], temp);
			break;
		case MAP_AGM_SPIRAL:
			temp = UTF8_str_size(agms_list[lang]);
			memcpy(buf, agms_list[lang], temp);
			break;
		case MAP_VRLA_GEL:
		    temp = UTF8_str_size(vrla_list[lang]);
		    memcpy(buf, vrla_list[lang], temp);
			break;
		case MAP_EFB:
			memcpy(buf, en_efb, UTF8_str_size(en_efb));
			break;
		default:
			//UARTprintf("PrintResultClick() ----> Error!\n");
			return;
		}
		UTF8_Printer_show(buf, &profile);
		break;
	case P_VOLTAGE:
		profile.side = side_left;
		temp = UTF8_str_size(voltage_list[lang]);
		memcpy(buf, voltage_list[lang], temp);
		UTF8_Printer_show(buf, &profile);
		/*profile.side = side_right;

		if(par->WHICH_TEST == Start_Stop_Test || par->WHICH_TEST == Battery_Test){
			sprintf(buf,"%5.2f V",TD->battery_test_voltage);
		}
		else{
			sprintf(buf,"%5.2f V",TD->IRvoltage);
		}
		UTF8_Printer_show(buf, &profile);*/
		break;
	case P_RATED:
		profile.side = side_left;
		if(((par->BAT_test_par.SelectRating == JIS)&&(par->WHICH_TEST == Battery_Test)) || ((par->WHICH_TEST == Start_Stop_Test)&&(par->SS_test_par.SSSelectRating == JIS))){
			memcpy(buf, prt_jis_list[lang], UTF8_str_size(en_prt_jis));
			UTF8_Printer_show(buf, &profile);
			/*profile.side = side_right;
			UTF8_Printer_show(pg_JIS[TD->JISCapSelect].batString, &profile);*/
		}
		else{
			temp = UTF8_str_size(prt_rated[lang]);
			memcpy(buf, prt_rated[lang], temp);
			UTF8_Printer_show(buf, &profile);
			/*switch(( par->WHICH_TEST == Battery_Test )? par->BAT_test_par.SelectRating:par->SS_test_par.SSSelectRating){
			case EN1:
				sprintf(buf,"%4d EN",TD->SetCapacityVal);
				break;
			case EN2:
				sprintf(buf,"%4d EN2",TD->SetCapacityVal);
				break;
			case IEC:
				sprintf(buf,"%4d IEC",TD->SetCapacityVal);
				break;
			case DIN:
				sprintf(buf,"%4d DIN",TD->SetCapacityVal);
				break;
			default:
				sprintf(buf,"%4d SAE",TD->SetCapacityVal);
				break;		
			}
			profile.side = side_right;
			UTF8_Printer_show(buf, &profile);*/			
		}
		break;
	case P_MEASURED_CCA:
	case P_MEASURED_IR:
		profile.side = side_left;
		temp = UTF8_str_size(measured_list[lang]);
		memcpy(buf, measured_list[lang], temp);
		UTF8_Printer_show(buf, &profile);
		/*profile.side = side_right;
		sprintf(buf,"%4d CCA",(uint16_t)TD->Measured_CCA);
		UTF8_Printer_show(buf, &profile);*/
		break;
	case P_TEMPERATURE:
		profile.side = side_left;
		temp = UTF8_str_size(prt_temp[lang]);
		memcpy(buf, prt_temp[lang], temp);
		UTF8_Printer_show(buf, &profile);
		//profile.side = side_right;
		//float TempF = ((TD->ObjTestingTemp * 9) / 5) + 32;
		//sprintf(buf,"%6.1f F/ %6.1f C",TempF,TD->ObjTestingTemp);
		//UTF8_Printer_show(buf, &profile);
		break;
	case P_STATE_OF_CHARGE:
		temp = UTF8_str_size(prt_soc[lang]);
		memcpy(buf, prt_soc[lang], temp);
		UTF8_Printer_show(buf, &profile);
		/*sprintf(buf,"%3d%%",(uint8_t)TD->SOC);
		UTF8_Printer_show(buf, &profile);*/
		break;
	case P_STATE_OF_HEALTH:
		temp = UTF8_str_size(prt_soh[lang]);
		memcpy(buf, prt_soh[lang], temp);
		UTF8_Printer_show(buf, &profile);
		/*sprintf(buf,"%3d%%",(uint8_t)TD->SOH);
		UTF8_Printer_show(buf, &profile);*/
		break;
	case P_TEST_CODE:
		temp = UTF8_str_size(test_code_list[lang]);
		memcpy(buf, test_code_list[lang], temp);
		UTF8_Printer_show(buf, &profile);
		break;
	case P_BARCODE:
		//UTF8_Printer_show((char*)TD->TestCode, &profile);
		break;
	case P_VIN:
		profile.side = side_left;
		temp = UTF8_str_size(prt_vin_num[lang]);
		memcpy(buf, prt_vin_num[lang], temp);
		UTF8_Printer_show(buf, &profile);
		//profile.side = side_right;
		//UTF8_Printer_show((char*)par->VIN_str, &profile);
		break;
	case P_NOTE:
		profile.side = side_left;
		temp = UTF8_str_size(prt_note[lang]);
		memcpy(buf, prt_note[lang], temp);
		UTF8_Printer_show(buf, &profile);
		break;
		
	case P_TEST_DATE:
		profile.side = side_left;
		temp = UTF8_str_size(prt_test_date[lang]);
		memcpy(buf, prt_test_date[lang], temp);
		UTF8_Printer_show(buf, &profile);
		/*PrinterPrintSpace(10);
		profile.side = side_right;
		sprintf(buf,"%04d/%02d/%02d %02d:%02d:%02d",TD->year,TD->month,TD->date,TD->hour,TD->min,TD->sec);
		UTF8_Printer_show(buf, &profile);*/
		break;
	case P_INFORMATION:		
		profile.side = side_left;
		temp = UTF8_str_size(prt_by[lang]);
		memcpy(buf, prt_by[lang], temp);
		UTF8_Printer_show(buf, &profile);
		break;
	case P_SYS_STARTER:
		temp = UTF8_str_size(prt_starter_test[lang]);
		memcpy(buf, prt_starter_test[lang], temp);
		UTF8_Printer_show(buf, &profile);
		{
			memset(buf,0,sizeof(buf));
			PrinterPrintSpace(10);
			char num[10] = {0};
			if(TD->CrankVResult == NO_DETECT){
				sprintf(num,"--.--V  ");					
			}
			else{
				sprintf(num,"%5.2fV  ",TD->CrankVol);
			}
			uint8_t num_len = strlen(num);
			memcpy(buf,num,num_len);
			if(TD->CrankVResult == LOW){
				temp = UTF8_str_size(low_list[lang]);
				memcpy(buf+num_len,low_list[lang],temp);
			}
			else if(TD->CrankVResult == NORMAL){
				temp = UTF8_str_size(normal_list[lang]);
				memcpy(buf+num_len,normal_list[lang],temp);
			}
			else{
				temp = UTF8_str_size(no_detect_list[lang]);
				memcpy(buf+num_len,no_detect_list[lang],temp);
			}
			UTF8_Printer_show(buf, &profile);			
		}
		break;
	case P_CHARGING_TEST:
		temp = UTF8_str_size(prt_charging_test[lang]);
		memcpy(buf, prt_charging_test[lang], temp);
		UTF8_Printer_show(buf, &profile);
		break;
	case P_DIODE_TEST:		
		temp = UTF8_str_size(prt_diode_ripple[lang]);
		memcpy(buf, prt_diode_ripple[lang], temp);
		UTF8_Printer_show(buf, &profile);

		{
			memset(buf,0,sizeof(buf));
			PrinterPrintSpace(10);
			char num[10] = {0};
			if(TD->RipVResult == NO_DETECT){
				sprintf(num,"--.--V  ");					
			}
			else{
				sprintf(num,"%5.2fV  ",TD->ripple);
			}
			uint8_t num_len = strlen(num);
			memcpy(buf,num,num_len);
			if(TD->RipVResult == HIGH){
				temp = UTF8_str_size(high_list[lang]);
				memcpy(buf+num_len,high_list[lang],temp);
			}
			else if(TD->RipVResult == NORMAL){
				temp = UTF8_str_size(normal_list[lang]);
				memcpy(buf+num_len,normal_list[lang],temp);
			}
			else{
				temp = UTF8_str_size(no_detect_list[lang]);
				memcpy(buf+num_len,no_detect_list[lang],temp);
			}
			UTF8_Printer_show(buf, &profile);			
		}
		break;
	case P_TEST_COUNTER:		
		temp = UTF8_str_size(test_counter_list[lang]);
		memcpy(buf, test_counter_list[lang], temp);
		UTF8_Printer_show(buf, &profile);
		break;
	case P_SS_TEST_COUNTER:
		profile.side = side_left;
		temp = UTF8_str_size(ss_test_list[lang]);
		memcpy(buf, ss_test_list[lang], temp);
		buf[temp] = ':';
		UTF8_Printer_show(buf, &profile);
		break;
	case P_BAT_TEST_COUNTER:
		profile.side = side_left;
		temp = UTF8_str_size(bat_test_list[lang]);
		memcpy(buf, bat_test_list[lang], temp);
		buf[temp] = ':';
		UTF8_Printer_show(buf, &profile);
		break;
	case P_SYS_TEST_COUNTER:
		profile.side = side_left;
		temp = UTF8_str_size(sys_test_list[lang]);
		memcpy(buf, sys_test_list[lang], temp);
		buf[temp] = ':';
		UTF8_Printer_show(buf, &profile);
		break;
	case P_IR_TEST_COUNTER:
		profile.side = side_left;
		temp = UTF8_str_size(ir_t_list[lang]);
		memcpy(buf, ir_t_list[lang], temp);
		buf[temp] = ':';
		UTF8_Printer_show(buf, &profile);
		break;
	
	case P_LOAD_ON:
		temp = UTF8_str_size(prt_load_on[lang]);
		memcpy(buf, prt_load_on[lang], temp);
		UTF8_Printer_show(buf, &profile);
		{
			memset(buf,0,sizeof(buf));
			PrinterPrintSpace(10);
			char num[10] = {0};
			sprintf(num,"%5.2fV  ",TD->LoadVol);
			uint8_t num_len = strlen(num);
			memcpy(buf,num,num_len);
			if(TD->AltLoadVResult == HIGH){
				temp = UTF8_str_size(high_list[lang]);
				memcpy(buf+num_len,high_list[lang],temp);
			}
			else if(TD->AltLoadVResult == NORMAL){
				temp = UTF8_str_size(normal_list[lang]);
				memcpy(buf+num_len,normal_list[lang],temp);
			}
			else{
				temp = UTF8_str_size(low_list[lang]);
				memcpy(buf+num_len,low_list[lang],temp);
			}
			UTF8_Printer_show(buf, &profile);			
		}		
		break;
	case P_LOAD_OFF:
		temp = UTF8_str_size(prt_load_off[lang]);
		memcpy(buf, prt_load_off[lang], temp);
		UTF8_Printer_show(buf, &profile);
		{
			memset(buf,0,sizeof(buf));
			PrinterPrintSpace(10);
			char num[10] = {0};
			sprintf(num,"%5.2fV  ",TD->IdleVol);
			uint8_t num_len = strlen(num);
			memcpy(buf,num,num_len);
			if(TD->AltIdleVResult == HIGH){
				temp = UTF8_str_size(high_list[lang]);
				memcpy(buf+num_len,high_list[lang],temp);
			}
			else if(TD->AltIdleVResult == NORMAL){
				temp = UTF8_str_size(normal_list[lang]);
				memcpy(buf+num_len,normal_list[lang],temp);
			}
			else{
				temp = UTF8_str_size(low_list[lang]);
				memcpy(buf+num_len,low_list[lang],temp);
			}
			UTF8_Printer_show(buf, &profile);			
		}
		break;
	case P_HIGH:		
		temp = UTF8_str_size(high_list[lang]);
		memcpy(buf, high_list[lang], temp);
		UTF8_Printer_show(buf, &profile);
		break;
	case P_LOW:		
		temp = UTF8_str_size(low_list[lang]);
		memcpy(buf, low_list[lang], temp);
		UTF8_Printer_show(buf, &profile);
		break;
	case P_NORMAL:		
		temp = UTF8_str_size(normal_list[lang]);
		memcpy(buf, normal_list[lang], temp);
		UTF8_Printer_show(buf, &profile);
		break;
	case P_NO_DETECT:
		temp = UTF8_str_size(no_detect_list[lang]);
		memcpy(buf, no_detect_list[lang], temp);
		UTF8_Printer_show(buf, &profile);
		break;
    //=== BT2200_HD_BO_USA ========//
	case P_Warning_word:
        temp = UTF8_str_size(warning_word1_list[lang]);
        memcpy(buf, warning_word1_list[lang], temp);
        UTF8_Printer_show(buf, &profile);
        break;
	case P_Warning_word1:
        temp = UTF8_str_size(warning_word2_list[lang]);
        memcpy(buf, warning_word2_list[lang], temp);
        UTF8_Printer_show(buf, &profile);
        break;
    case P_Warning_word2:
        temp = UTF8_str_size(warning_word3_list[lang]);
        memcpy(buf, warning_word3_list[lang], temp);
        UTF8_Printer_show(buf, &profile);
        break;
    case P_Warning_word3:
        temp = UTF8_str_size(warning_word1_list[lang]);
        memcpy(buf, warning_word1_list[lang], temp);
        UTF8_Printer_show(buf, &profile);
        break;
	case P_BATTERY_NUM:                                                       // 20200824 DW EDIT
        temp = UTF8_str_size(P_BATTERT_NUM_list[lang]);
        memcpy(buf, P_BATTERT_NUM_list[lang], temp);
        sprintf(buf,"==%s%d==",P_BATTERT_NUM_list[lang], parameter);
        UTF8_Printer_show(buf, &profile);
	    break;
	default:
		break;
	}
}

void show_print_battery_voltage(uint8_t y, float vol ,bool on_off){
	LANGUAGE LangIdx = get_language_setting();
	SHOW_profile_t profile = {side_middle, 2, false};
	char buf[50] = {0};
	uint32_t temp;
	uint8_t x_start = 0,x_end = 127;
	//static uint8_t pre_high = 0;
#if SOOMI_en
	if(LangIdx == sm_FN) y -= 1;
#endif
#if SVENSKA_en
	if(LangIdx == sv_SW) y -= 1;
#endif
#if CESTINA_en
	if(LangIdx == cs_CZ) y -= 2;
#endif
#if LIETUVIU_K_en
	if(LangIdx == lt_LT) y -= 1;
#endif
#if POLSKI_en
	if(LangIdx == pl_PL) y -= 1;
#endif
#if SLOVENCINA_en
	if(LangIdx == sl_SL) y -= 1;
#endif

	if(on_off){
		temp = UTF8_str_size(voltage_list[LangIdx]);
		memcpy(buf,voltage_list[LangIdx],temp);
		if(vol < 1.00){
			snprintf(buf+temp,sizeof(buf),"  %s", "<1.00V");
		}
		else{
			snprintf(buf+temp,sizeof(buf),"  %5.2fV",vol);
		}
		UTF8_LCM_show(x_start, y, buf, (x_end-x_start), &profile);	
	}
	else{
		tRectangle sRect;
		
		sRect.i16XMin = 0;
		sRect.i16XMax = 127;
		sRect.i16YMin = y-1;
		sRect.i16YMax = 55;
		
		GrContextForegroundSet(&g_sContext, 0);
		GrRectFill(&g_sContext, &sRect);
	}
}



