/*
 * VK_char_num.c
 *
 *  Created on: 2020�~12��22��
 *      Author: henry.yang
 */
#include "VKmgr.h"

#define DATA_LEN 40
#define DEFAULT_CHAR VK_KEY_ENTER
#define VK_LCD_INDEX 76


static const BUTTON_DATA _aButtonData[] = {
  {{23,40,31,48},       '0'},
  {{42,40,48,48},       '1'},
  {{61,40,69,48},       '2'},
  {{81,40,89,48},       '3'},
  {{101,40,109,48},     '4'},
  {{23,49,31,57},       '5'},
  {{42,49,50,57},       '6'},
  {{61,49,69,57},       '7'},
  {{81,49,89,57},       '8'},
  {{101,49,109,57},     '9'},
  {{115,47,127,55},     VK_KEY_BACKSPACE},//10
  {{2,54,19,61},        VK_KEY_Fn_ABC},//11
  {{112,55,125,61},     VK_KEY_ENTER},//12
  {{10,40,18,47},       'Q'},
  {{21,40,32,47},       'W'},
  {{35,40,42,47},       'E'},
  {{46,40,54,47},       'R'},
  {{57,40,65,47},       'T'},
  {{68,40,76,47},       'Y'},
  {{79,40,87,47},       'U'},
  {{91,40,95,47},       'I'},
  {{100,40,108,47},     'O'},
  {{113,40,120,47},     'P'},
  {{18,48,26,55},       'A'},
  {{30,48,37,55},       'S'},
  {{41,48,49,55},       'D'},
  {{53,48,60,55},       'F'},
  {{63,48,71,55},       'G'},
  {{74,48,82,55},       'H'},
  {{85,48,91,55},       'J'},
  {{95,48,103,55},      'K'},
  {{106,48,112,55},     'L'},
  {{115,47,127,55},     VK_KEY_BACKSPACE},//32
  {{2,55,16,61},        VK_KEY_Fn_123},//33
  {{30,56,37,63},       'Z'},
  {{41,56,49,63},       'X'},
  {{52,56,60,63},       'C'},
  {{63,56,71,63},       'V'},
  {{75,56,82,63},       'B'},
  {{85,56,93,63},       'N'},
  {{95,56,103,63},      'M'},
  {{112,55,125,61},     VK_KEY_ENTER},//41
  {{71,58,79,63},       ' '},//42
  {{52,58,60,63},       '-'},//43
};
static uint16_t ButtonData_size = sizeof(_aButtonData)/sizeof(BUTTON_DATA);

static uint8_t index;
static char vk_buf[DATA_LEN+1];
static VK_INFO_T VK_info;
static uint8_t keyboard_page;

void VKInputUpdate(void)
{
    uint8_t i, len = strlen(vk_buf);
    uint8_t start_x;
    for(i = 0; i < VK_info.len; i++){
        start_x = VK_info.start_x + (i)*VK_info.char_width;
        ST7565Draw6x8Char(start_x, VK_info.start_y, (i < len)? vk_buf[i]:' ', Font_System5x8);
    }
}

uint8_t VK_char_num_init(VK_INFO_T* vkinfo)
{
    memcpy(&VK_info, vkinfo, sizeof(VK_INFO_T));
    if(VK_info.len > (sizeof(vk_buf)-1)) return 0;
    if(!VK_info.start_x) VK_info.start_x = (128-(VK_info.len*VK_info.char_width))/2;
    memset(vk_buf, 0, sizeof(vk_buf));
    keyboard_page = 0;
    uint16_t i;
    for(i = 0; i < ButtonData_size; i++){
        if(_aButtonData[i].c == DEFAULT_CHAR){
            index = i;
            break;
        }
    }
    return 1;
}

void VK_char_num_display(void)
{
	SHOW_profile_t profile = {side_middle, 2, false};
    
    ST7565ClearScreen(BUFFER1);
    LcdDrawScreen(VK_LCD_INDEX+1, BUFFER1);
    
    ST7565ClearScreen(BUFFER0);
    LcdDrawScreenBuffer(VK_LCD_INDEX+keyboard_page, BUFFER0);
    ST7565DrawVirtualKybd(BUFFER0);
    
    if(VK_info.title_str) UTF8_LCM_show(0, 4, VK_info.title_str, 128, &profile);

    //Draw underline
    if(VK_info.underline){
        uint8_t i, start_x;
        for(i = 0; i < VK_info.len; i++){
            start_x = VK_info.start_x + (i)*VK_info.char_width;
            ST7565V128x64x1LineDrawH(0, start_x, start_x+4, VK_info.start_y+8, 1);
        }
    }
    VKInputUpdate();
    ST7565RectReverse(&_aButtonData[index].recs, VERTICAL);
        
    //LCD_Arrow(VK_info.arr_flag);
}

char VK_char_num_up(void)
{
    ST7565RectReverse(&_aButtonData[index].recs, VERTICAL);  
    uint8_t temp = index;
    if((index > 4) && (index < 10)){
        index -= 5;
    }
    else if(index == 10){
        index = 4;
    }
    else if(index == 11){
        index = 5;
    }
    else if(index == 12){
        index = 10;
    }
    else if((index > 22) && (index < 41)){
        index -= 10;    
    }
    else if(index == 41){
        index = 32;
    }
    else if(index == 42){
        index = 8;
    }
    else if(index == 43){
        index = 6;
    }
    UARTprintf("VK_char_num_up: %d to %d\n", temp, index);
    ST7565RectReverse(&_aButtonData[index].recs, VERTICAL);
    return _aButtonData[index].c;
}

char VK_char_num_down(void)
{
    ST7565RectReverse(&_aButtonData[index].recs, VERTICAL);
    uint8_t temp = index;
    if(index < 4){
        index += 5;
    }
    else if(index == 4){
        index = 9;
    }
    else if(index == 5){
        index = 11;
    }
    else if((index == 6) || (index == 7)){
        index = 43;
    }
    else if(index == 8){
        index = 42;
    }
    else if((index == 9)||(index == 10)){
        index = 12;
    }
    else if((index > 12) && (index < 31)){
        index += 10;
    }
    else if(index == 31 || index == 32){
        index = 41;
    }
    UARTprintf("VK_char_num_up: %d to %d\n", temp, index);
    ST7565RectReverse(&_aButtonData[index].recs, VERTICAL);
    return _aButtonData[index].c;
}

char VK_char_num_left(void)
{
    ST7565RectReverse(&_aButtonData[index].recs, VERTICAL);
    uint8_t temp = index;
    if(index == 0){
        index = 12;
    }
    else if(index == 12){
        index = 42;
    }
    else if(index == 13){
        index = 41;
    }
    else if(index == 42){
        index = 43;
    }
    else if(index == 43){
        index = 11;
    }
    else{
        index -= 1;
    }
    UARTprintf("VK_char_num_up: %d to %d\n", temp, index);
    ST7565RectReverse(&_aButtonData[index].recs, VERTICAL);
    return _aButtonData[index].c;
}

char VK_char_num_right(void)
{    
    ST7565RectReverse(&_aButtonData[index].recs, VERTICAL);
    uint8_t temp = index;
    if (index == 12){
        index = 11;
    }
    else if(index == 11){
        index = 43;
    }
    else if (index == 41){
        index = 13;
    }
    else if (index == 42){
        index = 12;
    }
    else if (index == 43){
        index = 42;
    }
    else{
        index += 1;
    }
    UARTprintf("VK_char_num_up: %d to %d\n", temp, index);
    ST7565RectReverse(&_aButtonData[index].recs, VERTICAL);
    return _aButtonData[index].c;
}
    
void VK_char_num_sel(void)
{
    uint16_t i;
    char refresh_key = 0;
    uint8_t search_offset = 0;
    
    switch(_aButtonData[index].c){
    case VK_KEY_Fn_ABC:
        keyboard_page = 1;
        refresh_key = VK_KEY_Fn_123;
        break;
    case VK_KEY_Fn_123:
        keyboard_page = 0;
        refresh_key = VK_KEY_Fn_ABC;
        break;
    case VK_KEY_BACKSPACE:
        i = strlen(vk_buf);
        if(i){
            vk_buf[i-1] = '\0';
        }
        VKInputUpdate();
        return;        
    case VK_KEY_ENTER:
        if(VK_info.cb) VK_info.cb(vk_buf, strlen(vk_buf));
        return;
    default:
        i = strlen(vk_buf);
        if(i < VK_info.len){
            vk_buf[i] = _aButtonData[index].c;
            VKInputUpdate();
            return;
        }
        else{
            index = (index < 12 || index > 41)? 12:41;
        }
        break;
    }

    if(refresh_key){
        for(i = search_offset; i < ButtonData_size; i++){
            if(_aButtonData[i].c == refresh_key){
                index = i;
                break;
            }
        }
    }
    VK_char_num_display();
}

void VK_CN_Cursor_blink(void)
{    
    if(strlen(vk_buf) >= VK_info.len) return;
    tRectangle rec;
    uint8_t len = strlen(vk_buf);
	rec.i16XMin = VK_info.start_x+len*VK_info.char_width;
	rec.i16XMax = rec.i16XMin+CURSOR_WIDTH-1;
	rec.i16YMin = VK_info.start_y;
	rec.i16YMax = rec.i16YMin+CURSOR_HEIGHT-1;
    ST7565RectReverse(&rec, VERTICAL);
}

