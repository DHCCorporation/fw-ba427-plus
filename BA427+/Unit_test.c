/*
 * Unit_test.c
 *
 *  Created on: 2020�~12��30��
 *      Author: jerry.wu
 */




#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "inc/hw_memmap.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/fpu.h"
#include "driverlib/rom.h"
#include "driverlib/timer.h"
#include "driver/board.h"
#include "driver/uartstdio.h"
#include "app/Widgetmgr.h"
#include "app/Datetimesetwidget.h"


void UT_Test_record_creat(){

    uint16_t Date[6],N = 0;
    uint32_t Test_type = Battery_Test;
    uint32_t i;

    memset(Date, 0, sizeof(Date));

    ExtRTCRead( &Date[0], (uint8_t*)&Date[1], (uint8_t*)&Date[2], (uint8_t*)&N, (uint8_t*)&Date[3], (uint8_t*)&Date[4], (uint8_t*)&Date[5]);

    for(i = 0 ; i < 249 ; i ++){
        BATTERY_SARTSTOP_RECORD_T record = {.Test_Type = (TEST_TYPE)Test_type,
                .Test_Time = {Date[0], Date[1], Date[2], Date[3], Date[4], Date[5]},
                .Battery_Type = VRLA_GEL,
                .Voltage = 12.34,
                .Ratting = IEC,
                .CCA_Capacity = 700,
                .CCA_Measured = 500,
                .IR = 12.34,
                .Judgement = TR_GOODnPASS,
                .SOH = 12,
                .SOC = 34,
                .Temperature = 12.34,
                .IsBatteryCharged = 1,
                .TestInVecicle = 1,
                .VIN = "VIN_1234",
                .RO = "RO_1234",
                .Email = "emial1234@dhc.com.tw",
                .Test_code = "Test code",
        };
        SaveTestRecords(Battery_Test,&record, sizeof(record), Wait_uplaod);
        Test_type ^= 1;

        SYSTEM_TEST_RECORD_T record_S = { .Test_Type = System_Test,
                .Test_Time = {Date[0], Date[1], Date[2], Date[3], Date[4], Date[5]},
                .Crank_result = NORMAL,
                .Crank_voltage = 12.34,
                .Idle_result = NORMAL,
                .Idle_voltage = 12.34,
                .Ripple_result = NORMAL,
                .Ripple_voltage = 12.34,
                .Loaded_result = NORMAL,
                .Loaded_voltage = 12.34,
                .Temperature = 12.34,
                .VIN = "VIN_1234",
                .RO = "RO_1234",
                .Email = "emial1234@dhc.com.tw",
        };
        SaveTestRecords(System_Test,&record_S, sizeof(record_S), Wait_uplaod);

        DateTimeUp(Date, 5);

    }


}






