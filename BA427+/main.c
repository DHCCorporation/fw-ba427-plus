//*****************************************************************************
//
// Source File: main.c
// Description: The main function to integrate all application and flow control for each entity.
//				
//
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 06/24/15     William.H      Created file.
// 10/01/15     William.H      Re-arrange/remove Global varaible's usage due to we've used stack to behave the manu level selection.
//                             Respective global varaible by the respective widget file to be maintained by their own.
//                             To adopt a stack by push/pop method to implement each menu level entry.
//                             To integrate all the BUTTON_PRESSED event: SELECT_BUTTON/UP_BUTTON/DOWN_BUTTON/LEFT_BUTTON/RIGHT_BUTTON, and respective control function into Widget Manager.c file. 
// 10/05/15     William.H      Check in BT2020 project which branched from LCDFontMaker(Pre-BT2020) by modifed related GPIOs.
// 10/08/15     William.H      1. EEPROM Access Service API's Definition. This service contains internal(6KB in MCU) and external eeprom memory access.
//                             2. PWM driver and service implementation.
//                             3. Implement LCD Backlight Setting Menu and add the menu entry into Setting Menu widget.
// 10/08/15     Henry.Y        Show external voltage on main menu screen.
// 10/14/15     Vincent.T      1. Add 25LC512(Ext-EEPROM IC driver)
//                             2. Add 25LC512(Ext-EEPROM IC service)
//                             3. Add EPR_25LC512HWInit in initializing process
// 10/15/15     Vincent.T      1. Add printer driver
//                             2. Add printer MP205HWInit in initializing process(delay 2s to charge C for A-buffer)
// 10/19/15     William.H      1. Add test entry in MENULEVEL enum @ Widgetmgr.h
//                                - CHKSURFACECHARGE /* 11: Check if Surface Charge exist */
//                                - COUNTDOWN15SEC   /* 12: 'Test In Vehicle' 15 sec count down */
//                                - TESTINVEHICLE    /* 13: Present 'Surface Charge', popup  ��Test In Vehicle�� */
//                             2. Add Timer0A module/Timer0AIntHandler() for 15 sec countdown of ��Surface Charge�� detect to turn on headlight when ��Test In Vehicle��.
//                             3. Add Clear Stack API - MenuLvlStack_ClrStack(), to deal with below scenario:
//                                - 24V Battery Type in Battery Test ==> Voltage High
//                                - INTERNAL_BAT() - Internal_bat_low ==> Power Low
//                                - Check_clamp()
//                                - CCA Load error
// 10/21/15     Henry.Y        1. Add test entry in MENULEVEL enum @ Widgetmgr.h
//                                - VOLTAGEHIGH		/* 14: Warning message - 'Voltage High'*/
//                             2. Add system channel judge at battery test beginning(Mainmenuwidget.c).
//                                If system 12V channel is full the warning message "VOLTAGE HIGH" will pop up.
// 10/22/15     Henry.Y        1. Add test entry in MENULEVEL enum @ Widgetmgr.h
//                                - ISITA6VBATTERY		/* 15: Question - 'Is a 6V battery?'*/
//                             2. Add 6V/12V battery selection at battery test beginning(Mainmenuwidget.c).
//                             3. Replace MenuLvlStack_Pop by MenuLvlStack_ClrStack and MenuLvlStack_Push in BATTypeSelLeft().
// 10/22/15     William.H      1. Change list order of 'SAE' and 'DIN' in Select Rating Widget.
//                             2. To load each 'Set Capacity' screen separately based on the selection result of 'Select Rating'. i.e. SAE(SelRatingWWidget) -> SAE(SetCapWidget)...etc.
//                             3. To modify the art knitting and circular in the edge frame of edit box, the up/down arrow's anti-white region need to be modified accordingly.
//                             4. The pseudo code to present when hit 'No' in 'Test in Vehicle?' widget, it will display 'Print Animation P1/P2/P3/P4', 
//	                          to a).clear stack, b).push 'Main Menu' back to stack, c).re-draw 'Main Menu' screen on LCD d). Anti-white item.
//                             5. Add right key event handle process be identical to click event process in TestInVehicleWidgetMsgProc().
// 10/23/15     Henry.Y        1. Add test entry in MENULEVEL enum @ Widgetmgr.h
//                                - CHKCLAMPS			/* 16: Warning message - 'Check clamps'*/
//                             2. There will monitoring external voltage continuously in while loop in main and pop up the warning message 'CHECK CLAMPS' if voltage is lower than 0.8V.
// 10/26/15     Vincent.T      1. Add mapsvc.c/h
//                             2. Add usbsvc.c/h
//                             3. Add printersvc.c/h
//                             4. Add USBPCModeSvc() function call in main loop
//                             5. modified flash.c/h
//                             6. modified H/L to control printer power
//                             7. modified usb_dev_serial.c
// 10/27/15     William.H      Add bootup animation with DHC logo via ST7565SlideUpBootupAnimation() API.
// 10/30/15     William.H      1. Modify "Print Result" screen to add-on "Judgetment result, SOH, SOC" fields.
//                             2. To process "Internal Battery Low" screen with blink scenario.
//                             3. To fix the Internal Battery Low suddenly been presented scenario, then clear menu stack and back to Main Menu immediately will cause state currupt issue. Add SystemErrHandler() function to handle/fix this issue.
//                             4. To update the "Check Clamp" screen with SystemErrHandler().
//                             5. To update the "Voltage Unstable. Wait for Stable" screen and show voltage in monitoring mode.
//                             6. Modify "Test In Vehicle" screen, to adopt 'Icon' instead of 'Yes/No' dialog.
//                             7. Add the "Turn On Headlights" screen to replace the string display.
//                             8. Add the "Is Battery Charged?" screen to replace the string display.
// 10/30/15     Vincent.T      1. Add wordbanksvc/printersvc
//                             2. Add extern global variable for printersvc access
// 11/02/15     Vincent.T      1. Change name of some global variables
//                             2. Modified data type of CCA_show
//                             3. Modified SOC calculation function call
// 11/04/15     Vincent.T      1. Modified Tested CCA and soh order
//                             2. Put BattCoreCalcSOH(&SOH, Measured_CCA) into BatJudgeMap();
// 11/13/15     Vincent.T      1. Add testrecordsvc.c/h
//                             2. Add new variable to count test number(Not be saved into internal eeprom yet)
//                             3. Extern global variable to access g_SetCapacityVal
// 11/18/15     Vincent.T      1. Test result "CCA:" is replaced by "Measured:"
//                             2. slow down voltage value fresh freq
// 11/18/15     Vincent.T      1. Add CLK_S35190AHWInit(); External Clock IC
//                             2. ExtRTCInit(); Check yyyy/mm/dd -> default: 2016/03/01
// 12/01/15     Vincent.T      1. Add tp337a_temp.c/.h
//                             2. Add tempsvc.c/.h
//                             1. Add TP337AHWInit();
// 12/03/15     Vincent.T      1. Add CCATempComp()
// 12/07/15     Vincent.T      1. Add KeyCombo in SETCAPNORMAL and DATETIMESET mode
//                             2. Add extern global variable g_ui8ButtonStates
//                             3. Add blDateTimeSetMode to enable/disable show date and time when menuLvl equals DATETIMESET
//                             4. Add ui8KeyComboThreshold to control KeyCombo event(or speed)
// 01/05/16     Vincent.T      1. Add Test counter: SS./Bat./Sys./all
// 01/08/16     Vincent.T      1. Add VMHWInit();
//                             2. Add #include "driver/vm.h"
// 01/11/16     Vincent.T      1. Add #include "app/VMAMwidget.h"
//                             2. Add VMAMshow() in main loop
// 01/12/16     Vincent.T      1. Add AMHW AMHWInit();
//                             2. Add #include "driver/am.h"
// 01/19/16     William.H      1. Add Timer1AIntHandler() - To define Bluetooth (1 sec) timer while in "turn on and disconnect" status.
//                             2. Initialize Bluetooth UART IOs configuration and enable Timer1A to blank the backlight in every 1 sec when BT still in disconnected mode and stop the timer when in connected mode.
//                             3. Bluetooth service start - It needs the special treatment to skip the "stack entry" checking due to stack has been pushed bunch of menu entries which is no longer staying in "BLUETOOTHSET" during BT service testing.
//                             4. Add Bluetooth On & Disconnect - the backlight blanking process (by Timer1A).
//                             5. Add "g_BTServiceStart" varabile in this condition check to avoid stack corrupt when "Internal battery capacity low!" happened.
// 01/19/16     Vincent.T      1. Modify SelRatingStrOnDisplay(): char strRatingJIS[] = "JIS" -> "SAE";
// 01/21/16     William.H      1. Add different bootup animation(): 
//                                The 2nd bootup animation: 1). 'DHC logo' move from right to left. 2). 'Battery & Energy Management Solution' move from right to left.
//                                The 3rd bootup animation: 1). 'DHC logo' move from right to left  2). 'Battery & Energy Management Solution' move from left to right.
//                             2. To define the Timer2A ISR which deal with one-shot timer in 30 sec to detect cranking voltage in system test.
// 01/22/16     Vincent.T      1. Do MP205HWInit(); before StartAnimation();
//                             2. Charge printer-C in StartAnimation();
// 01/25/16     Vincent.T      1. Call BTSaveTestParameter(); to save test parameters.
//                             2. #include "service/pwmsvc.h"
//                             3. Initialize LCD backlight setting
//                             4. global variable g_LcdBacklightConfig
// 01/27/16     Henry.Y        1. #include "service/btsvc.h"
//                             2. Add global variables: g_BTServiceStart, TX_buffer[], g_ucTX_index, Internal_bat_low, g_bCheck_clamps,
//                             3. New global variable: WHICH_TEST; To indicate which test be processed now.
//                             4. Add #define Transmit_Format
//                             5. Modify Timer0AIntHandler(); func. to separate the count down timer which applied in wireless test or manual operated.
//                             6. Modify Timer1AIntHandler(); func. to deal with the "BT connected" procedure by APP during the unexpected situation(Internal_bat_low or g_bCheck_clamps).
//                             7. Add Bluetooth service entrance to process the new arrived command(battery test and related only). 'if(g_BluetoothEnabled &&...){ if((Receive_command[0] == 0x01 ||...)){...}}'
//                             8. Add unexpected disconnect detection. 'if((!g_BTCntStatTimer1AEnabled) && g_BluetoothEnabled && ROM_GPIOPinRead(BT_CONN_STATUS_BASE, BT_CONN_STATUS_PIN))'
//                             9. Add 'if(menuLvl == BLUETOOTHSET)' to enable the Timer1A if bluetooth is enable but it is discnnected and Timer1A have not be enable.
//                             10. Modify 'if(BattCoreChkIntBatt(1))', 'if(menuLvl != CHKCLAMPS && BattCoreChkClamp(1))', add associated procedure if BLUETOOTHSET entry in stack.
//                             11. Add bluetooth related test procedure in 'if(menuLvl == CHKCLAMPS, CHECKRIPPLE, CHKSURFACECHARGE, COUNTDOWN15SEC and MEASURING)'
// 01/29/16     Vincent.T      1. add SSSaveTestParameter(); func. to save ss test parameters.
//                             2. add SSTestCounterInc(); func. to increase test counter.
//                             3. global variables: g_SSSelectRating, g_SSBatteryType, g_BTSetCapacityVal, gSSTSetCapacityVals
// 01/30/16     Vincent.T      1. Modify SelRatingStrOnDisplay(); to seperate SS and Battery Test
// 02/15/16     William.H      To deal with RO / VIN's input character's blink effect.
// 02/02/16     Vincent.T      1. Add System test(Cranking Voltage) routine
// 02/03/16     Vincent.T      2. Add System test(IDLE Voltage) routine
// 02/04/16     Vincent.T      1. Add System test(Ripple, Load Voltage) routine
// 02/05/16     Vincent.T      1. Add System test(Printout) routine
// 02/04/16     Vincent.T      1. Add System test(Ripple, Load Voltage) routine
// 02/05/16     Vincent.T      1. Add System test(Printout) routine
// 02/18/16     Vincent.T      1. Add Bluetooth for System test funcs.
// 03/03/16     Vincent.T      1. Add entry of EngMode.
// 03/08/16     Vincent.T      1. Add Vbus Detec func.
// 03/11/16     Vincent.T      1. New global variable gfObjTestingTemp to get testing temperature.(for printout)
// 03/21/16     Vincent.T      1. Add global variable gui8LCDcontrast to access LCD contrast.
// 03/24/16     Henry.Y        1. Modify BattCoreChkIntBatt(): Add adc and voltage test parameter. 
//                             2. Modify BattCoreGetVoltage(): Add adc test parameter.
// 03/25/16     Vincent.T      1. New global variable g_LangSelectIdx to get language index.
// 03/30/16     Vincent.T      1. Replace g_LangSelectIdx by g_LangSelectIdx;
//                             2. Delete if(menuLvl == TRANSTODISPLAY).........due to multination language case.
//                             3. Remove strSOH, strSOC, strShowCCA and strShowBTestV
//                             4. New global variables: gfMeasuredVolt
//                             5. Modify SelRatingStrOnDisplay(); to handle multination language case.
//                             6. #include "driver/smallfonts.h"
//                             7. Modify JudgetResltStrOnDisplay(); to handle backlight.
// 04/07/16    Vincent.T       1. Excute IntEepromSvcInit(), IntEepromSvcGetCoef(); beforeST7565HwInit();
// 04/11/16    Vincent.T       1. Add IR Test Routine.
// 04/13/16    Vincent.T       1. New global IRvoltage, fIRto access IR tested voltage.
// 04/18/16    Vincent.T       1. Modify StartAnimation(); to escape func. when usb event occured.
// 04/27/16    Vincent.T       1. New global variable battery_test_voltage.(original location in main loop, due to cross function access)
//                             2. The value of Judge_Result will bechanged when bluetooth is on and test result need to be confirmed.
//                             3. Save test parameters process in DisplayResultwidget.(Without bluetooth part)
// 05/20/16    Vincent.T       1. Save test parameters routine in if(menuLvl == DISPLAYRESULTS)...
// 06/14/16    Vincent.T       1. No need to pre-charge C.(StartAnimation();)
//                             2. when menuLvl = CHKINTBAT, power low pop out, trap MCU. 
// 06/21/16    Vincent.T       1. #include "app/PointToBatwidget.h"
//                             2. New global variable gfBodyTestingTemp to access value of the body part of temperature sensor.
//                             3. New global variable g_ui8MainMenu to access the lastest selected main menu icon.
//                             4. "POINTTOBATTERY" to instruct user how to get temperature value while operating BT2100.
//                             5. Speed-up btn event  in "DATETIMESET".
//                             6. Slow down animation speed in "ALTIDLEVOLRESULT".
// 06/24/16    Vincent.T       1. All measured cca rating -> CCA
// 06/28/16    Vincent.T       1. New global variables year, month, date, day, hour min and sec
//                             2. Read time after accessing GetCCA(); successful.
//                             3. global variable fSysObjTemp(sensed temperature after system test icon clicked)
//                             4. Read time before accessing SysTestRecord();
// 08/10/16    Vincent.T       1. Modify StartAnimation(); masked all delay to accelerate opening animation.
//                             2. When entering USB mode, LCD backlight color turns into blue.
//                             3. Modify SSTestRecord(), BTTestRecord and SysTestRecord() all input parameters, due to more input parameters need to be saved.
//                             4. Trap MCU when internal battery voltage is too low. If without this process could occur main menu and replace internal battery two screen switch constantly.
//                             5. When remote control tester, temperature info. only be saved as a test record.(No temperature compensation with remote control)
//                             6. "Point to battery" before doing IR test.
// 09/06/16    Vincent.T       1. Re-define BlueTooth protocol of "CAUTION", "Bad Cell Replace" and "Rechare & Retest"
//                             2. Add delay when Bluetooth disconnect(ROM_SysCtlDelay(get_sys_clk()/3);	//avoid LCD blinking)
//                             3. Implement 15 sec countdown when surface charge detected.
//                             4. Remove LCD process when doing "ALT. IDLE LOAD" test with Bluetooth Connected.
// 11/15/16    Ian.C           1. Modify BTClear_Status(); to BTClear_Status(g_LangSelectIdx); for multi-language.
// 11/17/16    Vincent.T       1. include #include "service/amsvc.h"
//                             2. New rotine for PCB_CHECK_MODE.(preprocessor; sparate application main and pcb_check_mode main))
// 11/23/16     Vincent.T      1. Marked sliding effect when menuLvl equals CRANKVDETEST;
// 12/02/16     Vincent.T      1. global variables for 3-wire compensation.(gf12IRH, gf12IRL, gf12IRH_COM and gf12IRL_COM)
//                             2. Modify code bellow if(menuLvl == IRTEST)...for 3-wire compensation
// 12/08/16     Vincent.T      1. Modify USB plug-in detection in StartAnimation();
//                             2. Modify USB plug-in detection in main();
// 01/06/17     Vincent.T      1. global variables for 3-wire compensation.(gf6IRH, gf6IRL, gf6IRH_COM and gf6IRL_COM)
//                             2. modify if(menuLvl == IRTEST)... for 3-wire compensation.
// 01/13/17     Vincent.T      1. global variables: gui16IRTestCounter, par->RO_str, par->VIN_str, g_ui8TestCode and g_PrtSetCapacityVal
//                             2. Modify StartAnimation(); to speedup booting time
//                             3. Modify main(); to modify voltage position on screen
//                             4. Change save test record timming
//                             5. Save IR Test in main routine
//                             6. Generate testcode in main();
//                             7. IR test routing change.(Skip Point to Bat. and "6V Battery?")
// 02/06/17     Henry.Y        1. Add smart lab calibration flow, go to AUTOCalModeSvc() before main menu accessed.
//                                Add defined symbol:smart_lab
//                                include: smartlabsvc.h, sluart_iocfg.h
//                                global variable: blKMode
//                                Modify: StartAnimation()
// 02/10/17     Vincent.T      1. Modify variable of MFC Mode Temperature Failt Range
//                             2. Average times of getcca();
// 02/17/17     Vincent.T      1. Average times of getcca();
// 03/01/17     Vincent.T      1. Modify the average time of getCCA();
// 03/08/17     Vincent.T      1. Exapnd par->RO_str[] 4-> 8
// 03/22/17     Vincent.T      1. Modify main();(voltage > 32 -> 31.99)
//                             2. Testing parameters save point changed.
// 03/27/17     Vincent.T      1. Modify main.c -> test USBConnect() func.
// 03/30/17     Vincent.T      1. Move voltage range detection from main.c to battcore.c
// 04/05/17     Vincent.T      1. Modify MFC Mode F/W Ver.
//                             2. New func. in MFC mode: 1. RTC check; 2. Printout area in flash-ic check
//                             3. Modify USB connection func.
// 04/10/17     Vincent.T      1. Modify judgment flash content in manufacture mode.
// 06/01/17     Henry.Y        FW version: BT2100_WW_V01.a/BT2100_MFC_V01.a
//                             Fix bug: predefined symbol[PCB_CHECK_MODE] USB plug-in not work.
//                             1. Modify: Not allow USB function work if bluetooth is enable and internal battery is low.
//                             2. bettery test/start stop test/IR test: Add temperature limitation to make sure it will match spec.
// 09/21/17 	Jerry.W		   Rewrite Widget for new software architecture
// 11/06/17     Henry.Y	       1. FW version: BT2100_WW_V02.b(NSA.)
//                             2. hide useless variable and arrange code.
// 11/13/17     Jerry.W        1. Add check internal battery action when return form USB connection.
//                             2. Add ROM_IntMasterDisable() privent from melfunction of intteruption
// 11/22/17     Henry.Y        1. New global variables: RCJudge_Result, g_ui8Inventory; remove gfMeasuredVolt.
//                             2. Change Measured_RC declaration type float -> uint16_t.
// 11/23/17     Jerry.W        1. Modify initail sequence for printer mode opening.
// 02/14/18     Henry.Y        flash_upgrade(): modified external flash layout.
// 03/23/18     Jerry.W        modify WHICH_TEST initial for unfinished system test with VIN code scaned.
// 05/18/18     Jerry.W        add message mechanism.
// 09/11/18     Henry.Y        Rename the project name: BT2200_MFC_V00.A/BT2200_BO_V00.A.
//                             Use systeminfo data struct to replace the global variables.
//                             Use system driver interface to replace global variable.
// 10/17/18     Henry.Y        Add new identifier: __BO__ and branch version: BT2200_WW_V00.A
// 11/19/18     Henry.Y        BT2200_MFC_V00.B/BT2200_BO_V00.J/BT2200_WW_V00.C.
// 12/07/18     Henry.Y        Disable BTHwInit(); New display api; Add routine: ExtFlsPoll();
// 12/18/18     Henry.Y        BT2200_BO_V00.O: New testing condition; New translation;
// 03/20/19     Henry.Y        1. Add new LCM contrast parameter adjustment(match HW bias1/9, \5x booster).
//                             2. fix enter button pressing time threshold.
//                             3. Add customized printing page recovered flow.
// 03/29/19     Henry.Y        BT2200_BO_V03.B: fix the bug and add some improvements.
// 07/27/20     David Wang     ECN 202007210001 BT2200_HD_BO_USA MODIFY
//                             1. Firmware modify => main.c
//                             2. LCM MOPAR LOGO DEL => main.c
// 07/28/20                    3. Test counter ( print & screen) modify => TestCounterConfirmWidget.c�BTestCounterwidget.c�Btestrecordsvc.h
// 07/29/20                    4. Add USB external flash upgrade => usbsvc.h�Busbsvc.c�BUSB_widget.c
// 07/30/20                    5. Main Menu screen modify >>
//                                a. Main menu pic modify : NO_0_v2 --> NO_0_v3
//                                b. Select function modify :Mainmenuwidget.c�BBT2200_DISPLAY_SERVICE.c
//                             6. add left button long press go back mainmenu function : main.c , button.c
//                             7. sys test specification modification : battcoresvc.c
//                             8. Some pictures of sys test changed to text description :
//                                BT2200_DISPLAY_SERVICE.c,BT2200_DISPLAY_SERVICE.h,CrankingVoltWidget.c,TurnOnLoadswidget.c,AltIdleVoltWidget.c,Ripple15Secwidget.c
//                             9. VM/AM meter add reminder on/off screen : VMAMwidget.c , BT2200_DISPLAY_SERVICE.c
// 08/05/20                   10. Clock time rtc to manual operation :  extrtcsvc.h , extrtcsvc.c , Datetimesetwidget.h , Datetimesetwidget.c
// 08/06/20                   11. Add bosch logo for satrt init : main.c
//                            12. Add start ask voltage high check screen : Widgetmgr.h , Is24vSystemCheck_widget.c , BT2200_DISPLAY_SERVICE.c , BT2200_DISPLAY_SERVICE.h , Mainmenuwidget.c
//                            13. Add SYS TEST PRINT 24V CAN'T PRINT Prompt : BT2200 can print for 24v bat. no modify
// 08/07/20                   14. Add SYS TEST DIESEL ENGINE : Diesel40Sec_widget.c , BT2200_DISPLAY_SERVICE.c , BT2200_DISPLAY_SERVICE.h , CrankingVoltWidget.c
//                            15. Close registered function :  cloud_config.h,
//                            16. Mask scanner function : Scannerwidget.c
//                            17. Change to BT2200_HD_MAX MAP : mapsvc.h , mapsvc.c
// 08/11/20                   18. Move A to B and delete vin code widget and jump directly to print widget :
//                                Battery test : Displayresultwidget.c
//                                System TEST  : SysResultwidget.h
// 08/12/20                   19. BAT TEST : Add PACK TEST NUM register : usbsvc.c , cloud.h
//                                SYS TEST : Modify UPLOAD DATAHD FUNCTION :usbsvc.c
// 08/14/20                   20. HD function - select pack number of batteries : HdFunctionSelect_widget.c , BT2200_DISPLAY_SERVICE.c , BT2200_DISPLAY_SERVICE.h
//                                            - add HD test flow : mapsvc.c , mapsvc.h , BT2200_DISPLAY_SERVICE.c , Check_Pack_widget.c ,
//                                            - re start ask into hd function : Ask_Do_HD_Function_widget.c ,
// 08/20/20                                   - ALL PACK BAT TEST MAP RESULT SHOW : Display_PackTestAllResult_widget.c
//                            21. Setting some option functions deleted : BT2200_DISPLAY_SERVICE.c , Settingmenuwidget.c , langaugesvc.h
// 08/21/20                   22. HD FUNCTION - GOOD PACK PRINT : BT2200_DISPLAY_SERVICE.c , Printresultwidget.c ,
//                                              BAD PACK PRINT  : Printresultwidget.c , BT2200_DISPLAY_SERVICE.c , BT2200_DISPLAY_SERVICE.h
//                            23. RD BUG FIX SELF -
//                                a. Bat test must to get temp then to load on : Restart_To_do_HDFunction_widget
//                                b. Pack test one by one must to check vol > 16.0v? : Restart_To_do_HDFunction_widget
//                                c. Check Pack test ptint fix : BT2200_DISPLAY_SERVICE.c , BT2200_DISPLAY_SERVICE.h , Printresultwidget.c
// 08/26/20                   24. Warrings fix : AltIdleVoltWidget.c , TurnOnLoadswidget.c , Diesel40Sec_widget.c , mapsvc.c
//                            25. Add advised action widget : BT2200_DISPLAY_SERVICE.h , BT2200_DISPLAY_SERVICE.c , Widgetmgr.h , Pack_Test_Advised_widget.c , Display_PackTestAllResult_widget.c
// 09/03/20    David.Wang     Fix little Kevin PC software bug : usbsvc.c
// 09/07/20                   Fix bug 16643 Business modification boot pattern : main.c
//                                    16662 print sys has vin code. Must del : Printresultwidget.c
//                                    16667 add bt test 6v battery test : MeasuringWidget.c , Scannerwidget.c
//                                    16670 Modify the pattern combination of the abort widget button : BT2200_DISPLAY_SERVICE.c
//                                    16658 Modify pack test bat select widget button pattern combination�@2   >> LEFT_DOWN_ENT_UP_PAGE2 : BT2200_DISPLAY_SERVICE.c
//                                    16666 In show " TEST EACH BATTERY SEPARATELY" screen can not to do enter select : Is24vSystemCheck_widget.c
//                                    16669 Modify the pattern combination of the LOAD ERROR widget button : BT2200_DISPLAY_SERVICE.c
//                                    16652/16655/16656 TEST COUNTER FUNCTION BUG :TestCounterConfirmWidget.c , testrecordsvc.h , BT2200_DISPLAY_SERVICE.c , TestCounterwidget.c
//                                    16633 MAINMENU LINE BAR ( SYS TEST TO SETTINT ) HAS WARRING : Mainmenuwidget.c
//                                    16676 PACK TEST INIT ASK FOR #2 SCREEN NO ENTER pattern : BT2200_DISPLAY_SERVICE.c
//                                    16677 WARNING_CLAMPS_ON Basemap button pattern replacement ENTER >> LEFT + ENTER : BT2200_DISPLAY_SERVICE.c
//                                    16678 Print again  Basemap button pattern replacement DOWN_ENT_UP >> DOWN_ENT_UP + left : BT2200_DISPLAY_SERVICE.c
// 09/08/20                           16679 MAP cannot test the caution : mapsvc.c ,
//                                    16683 SYS TEST STAR HAS RIPPLE , Graphic display replaced by text prompt : Ripple Detected? , ChecheRippleWidget.c ,AltIdleVoltWidget.c
//                                    16692 Modify SYS test DIESEL ENGINE check flow : Diesel40Sec_widget.c
// 09/09/20                           16737 voltage high widget enter key change to left key : BT2200_DISPLAY_SERVICE.c ,  Voltagehighwidget.c
// 09/10/20                           16736 voltage high words modify to flowchart :
//                                          a. voltage high words : Ripple Detected?
//                                          b. in vehicle & Surface charge remask to normal function , now is BT2200_BO_M FUNCTION :Testinvehiclewidget.c , ChkSurfaceCharge.c
//                                    16701 Print result base pattern add left key : BT2200_DISPLAY_SERVICE.c
//                                    16732 Test count print bug : TestCounterConfirmWidget.c
//                                    16753 Is 6v bat widget left key bug : Isita6vbatterywidget.c , Mainmenuwidget.c
//                                          ( move scanner widget in there)
//                                    16796 PRINT SCREEN CHECK CLAMP ENTER BUTTON TIME TO LONG : Printresultwidget.c
// 09/17/20    David.Wang             16899 Isita6vbatterywidget.c Restore the selected yes/no state of the last test => Isita6vbatterywidget.c
//                                    16617 Modified version display screen => Settingmenuwidget.c
// 10/16/20    David.Wang             ECN number
//                                    1) StartAnimation : 1sec change to 2sec => main.c
//                                    2) PACK TEST : # OF BATTERY IN PARALLEL >> PACK TEST => BT2200_DISPLAY_SERVICE.c
//                                    3) Add a blank before option text : "BATTERY x 2" >> " BATTERY x 2" => BT2200_DISPLAY_SERVICE.c
//                                    4) Select RATING CCA change CCA/SAE => BT2200_DISPLAY_SERVICE.c
//                                    5) Select RATING EN change CA/MCA => BT2200_DISPLAY_SERVICE.c , battcoresvc.h , AWS_IoT_svc.c , Printresultwidget.c
//                                    6) Select Capacity : mask CCAValConvert => Setcapwidget.c
//                                    7) Delete display charging time message => Displayresultwidget.c
//                                    8) V/A meter bug fix n_AMVM_TURN_OFF_AMP[] = "PRESS TURN OFF"; >> "PLEASE TURN OFF"; => Displayresultwidget.c
//                                    9) SYS TEST Cranking test : i. CRANKING VOLTAGE : NO DETECTED >> NOT DETECTED => CrankingVoltWidget.c
//                                                               ii. Print : START TEST >> CARKING VOLTAGE => BT2200_DISPLAY_SERVICE.c
//                                   10) SYS TEST Cranking test : i. Print : DIODE RIPLE >> RIPPLE VOLTAGE => BT2200_DISPLAY_SERVICE.c
// 10/19/20    David Wang            11) Ripplr 15sec count screen flash to fix => Ripple15Secwidget.c , BT2200_DISPLAY_SERVICE.c
//                                   12) Surface charge turn headlights countdown 15sec Picture to text => ChkSurfaceCharge.c , BT2200_DISPLAY_SERVICE.c
//                                   13) Setting ERASE item no used to del => Settingmenuwidget.c , BT2200_DISPLAY_SERVICE.c
// 10/20/20    David Wang            14) Diesel engine flow add new screen => Diesel40Sec_widget.c , BT2200_DISPLAY_SERVICE.h , BT2200_DISPLAY_SERVICE.c
//                                   15) Enlarged print fonts :
//                                         a. Test counter print => TestCounterConfirmWidget.c
//                                         b. Battery test print => Printresultwidget.c
//                                         c. System test print  => Printresultwidget.c
//                                         d. Check pack print   => Printresultwidget.c
//                                   16) Cranking vol test miss issue => CrankingVoltWidget.c
// 10/21/20     David Wang           17) French and Spanish translation changes
// 11/11/20     David Wang         BT2200HD_BO_V01.B MODIFY :
//                                   1) Bug id 17291 fix => BT2200_DISPLAY_SERVICE.c
//                                   2) Bug id 17287 fix => BT2200_DISPLAY_SERVICE.c
//                                   3) The second revision of the customer word list => BT2200_DISPLAY_SERVICE.c
//                                   4) Modification after the customer verifies the second sample => BT2200_DISPLAY_SERVICE.c , Printresultwidget.c
// 20201112     David Wang         BT2200HD_BO_V01.C MODIFY :
//                                   1) TURN OFF LOADS&ENGINE translation for french & spanish => BT2200_DISPLAY_SERVICE.c
// 20201127     David Wang         BT2200HD_BO_V02.A modify :
//                                   1) Test result rating : only cca to change ( cca/ca/en/din/iec..) => BT2200_DISPLAY_SERVICE.c
//                                   2) Pack test print result : measured rating must be ( cca/ca/en/din/iec..) ,but now is only cca => Printresultwidget.c
//                                   3) Ripple voltage condition modify for customer => battcoresvc.c
//                                   4) Early judgment will occur in less than 30 seconds cranking countdown => CrankingVoltWidget.c
//                                   5) Load on test countdown 15se word modify & need to flash => Ripple15Secwidget.c , BT2200_DISPLAY_SERVICE.c
//
// =============================================================================
//
//
// $Header:  $
// $DateTime:$
// $Author:  $
//
//
// Copyright (c) 2020 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include "inc/hw_memmap.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/fpu.h"
#include "driverlib/rom.h"
#include "driverlib/timer.h"
#include "driver/board.h"
#include "driver/uartstdio.h"
#include "app/Widgetmgr.h"
#include "Unit_test.h"


//*****************************************************************************
//
// Firmware version.
//
//*****************************************************************************
#if defined(PCB_CHECK_MODE)
uint8_t *strFWver = "BT2200_MFC_V01.A";
#elif defined(__BO__)
	#if RD_MODE
	uint8_t *strFWver = "BT2200_BO_V04.A_DEV";
	#else
	uint8_t *strFWver = "BA327_2200_CL_V00.E";
	#endif

#else
uint8_t *strFWver = "BT2200_WW_V00.I";
#endif

//strFLSver = "BT2200_FLS_BO_V00.A";

//*****************************************************************************
//
// The system clock frequency in Hz.
//
//*****************************************************************************


//*****************************************************************************
//
// Declare 15 sec countdown timer counter for "Turn Headlights On for 15 sec" 
// if "Test In Vehicle" is Yes when surface charge present.
//
//*****************************************************************************
int16_t g_15SecCDCounter = 15;


//*****************************************************************************
//
// Declare Timer1A global enabled flag related to Bluetooth connection status.
//
//*****************************************************************************
bool g_BTCntStatTimer1AEnabled = false;

//*****************************************************************************
//
// Declare Timer2A global timeout flag(30 sec) for cranking voltage detect in system test.
//
//*****************************************************************************
bool g_bCranking_timeout = false;

//*****************************************************************************
//
// global variable for ENTER ON mode.
//
//*****************************************************************************
bool g_bENTERON = false;
volatile bool dummy_system_off = false;

Timer_t* loc_timer;
Timer_t* button_timer;
volatile uint8_t counter;
volatile uint8_t button_counter;

widget* current_W;



void Soft_timer_cb(void){
	if(loc_timer){
		if(counter) counter--;
		else dummy_system_off = 1;
		UARTprintf("Soft timer Soft_timer_cb(), idle timer counter = %d\n\r", counter);
	}

	if(button_timer){
		if(button_counter) button_counter--;
		else dummy_system_off = 1;
		UARTprintf("Soft timer Soft_timer_cb(), button timer counter = %d\n\r", button_counter);
	}
}


void StartAnimation(void)
{
	SYS_INFO* info = Get_system_info();
	uint16_t i,count = 10000;              // BT2200 = 20000
	uint16_t press_counter = 0;


#if defined(NEUTRAL)
	// Print Animation P1
	//SHOW_profile_t profile = {side_middle, 2, false};
	//LCD_display(LOGO, 0);
	//UTF8_LCM_show(0,50, "SYSTEM ANALYZER", 128, &profile);

	LCD_String_ext(16, get_Language_String(S_BOOTUP), Align_Central);

#endif	

	for(i = 0; i < count; i++){

#if defined(NEUTRAL)
		//if(i > 1250) break;
#endif
		if(i == 1250) ST7650BacklightSet(info->main_LcdBacklightConfig);

		if(USBConnect()){
			set_system_mode(USB_mode);
			return;
		}
		
		if(MAP_GPIOPinRead(BUTTONS_GPIO_BASE, LEFT_BUTTON | SELECT_BUTTON | RIGHT_BUTTON) == (LEFT_BUTTON | RIGHT_BUTTON)){
			if(i < 1000){
				press_counter++;
			}
			else if(press_counter == i){
				//IntBat_SW_ON;
				g_bENTERON = true;
			}
		}
		else{
			press_counter = 0;
		}
		delay_ms(0.1);
	}
}



//*****************************************************************************
//
// To define cranking volt detect timer (30 sec) in system test.
// In Timer2A ISR, to deal with one-shot timer in 30 sec to detect cranking voltage.
//
//*****************************************************************************
void Timer2AIntHandler(void)
{
	// Clear the timer interrupt
	ROM_TimerIntClear(TIMER2_BASE, TIMER_TIMA_TIMEOUT);
	g_bCranking_timeout = true;
}



void change_string(uint8_t *s){
	while(*s == 'Z'){
		s++;
	}
	(*s)++;
}




#include "driverlib/flash.h"
extern void (* const g_pfnVectors[])(void);
void bootloader_upgrad(){
	uint32_t i;
	uint32_t* temp = (uint32_t*)0x80000;// TODO: need to update flash address(bootloader+image > 0x80000)
	char* string = get_bootloader_version();

	if(g_pfnVectors != 0 && (string == NULL) && (((*temp)&0xFFF00000) == 0x20000000)){
		UARTprintf("upgrading booloader\n");
		//move 0x80000 to 0x0000, 48Kbyte
		for(i = 0; i < 48*1024 ; i+= (16*1024)){
			UARTprintf("erase 0x%x\n", i);
			FlashErase(i);
			if(FlashProgram((uint32_t*)(0x80000+i), i, 16*1024) == -1){
				UARTprintf("write 0x%x fail\n", i);
			}
		}
		UARTprintf("upgrading finished\n");
	}
}




extern tContext g_sContext;
extern tDisplay g_sST7565V128x64x1;

void test_gra(){
	static bool initialed = false;
	tRectangle sRect;

	if(!initialed){
		initialed = true;
		GrContextInit(&g_sContext, &g_sST7565V128x64x1);
					}

	sRect.i16XMin = 0;
	sRect.i16YMin = 0;
	sRect.i16XMax = GrContextDpyWidthGet(&g_sContext) - 1;
	sRect.i16YMax = GrContextDpyHeightGet(&g_sContext) - 1;
	GrContextForegroundSet(&g_sContext, ClrBlack);
	GrContextBackgroundSet(&g_sContext, ClrWhite);
	GrRectFill(&g_sContext, &sRect);
	GrContextForegroundSet(&g_sContext, ClrWhite);
	GrContextFontSet(&g_sContext, g_psFontFixed6x8);
	GrStringDrawCentered(&g_sContext, "string1", -1,
			GrContextDpyWidthGet(&g_sContext) / 2, 5, 0);

	GrContextFontSet(&g_sContext, g_psFontCmtt12);
	GrStringDrawCentered(&g_sContext, "string1", -1,
			GrContextDpyWidthGet(&g_sContext) / 2, 18, 0);


							
	GrContextFontSet(&g_sContext, g_psFontCmtt30);
	GrStringDrawCentered(&g_sContext, "", -1,
			GrContextDpyWidthGet(&g_sContext) / 2, 40, 0);
}
	
Timer_t* Message_t;

void testing_handler(){
	uint8_t test = rand()%100;

	if(test < 15){
		up = true;
		UARTprintf("[Key] : UP\n");
	}
	else if(test < 25){
		left = true;
		UARTprintf("[Key] : LEFT\n");
	}
	else if(test < 30){
		down = true;
		UARTprintf("[Key] : DOWN\n");
	}
	else if(test < 40){
		right = true;
		UARTprintf("[Key] : RIGHT\n");
	}
	else if(test < 70){
		enter = true;
		UARTprintf("[Key] : ENTER\n");
	}
	else if(test > 97){
		message("message unit test");
		UARTprintf("[message]\n");
	}

}


void set_testing(){
	static bool testing = false;
	if(!testing){
		Message_t = register_timer(3, testing_handler);
		start_timer(Message_t);
	}
	else{
		unregister_timer(Message_t);
	}
	testing ^= 1;
}




/**********USB TEST********/

static bool bCR = false, bNL = false;
void main_bc_cb(bool flush, uint8_t data)
{
	 uint8_t ui8buf = 0;
	 static uint8_t RX_index = 0;
	 static uint8_t UART0_RX_buffer[150] = {0};
	 
	ui8buf = data;
	UART0_RX_buffer[RX_index] = ui8buf;
	RX_index++;
	if(RX_index > sizeof(UART0_RX_buffer)){
		USBSendByte("ERROR length!", 13);
		RX_index = 0;
		bCR = false;
		bNL = false;
		SCAN_OFF;
		return;
	}
	
	if(ui8buf == 0x0D) bCR = true;
	if(ui8buf == 0x0A) bNL = true;
	if(bCR && bNL){

		USBSendByte(UART0_RX_buffer, RX_index);
		RX_index = 0;
		bCR = false;
		bNL = false;
		SCAN_OFF;
	}
 }


void USB_rcv_cn(uint8_t* data, uint32_t len){
	uint32_t i;

	UARTprintf("USB_rcv_cn test[%d]\n", len);
	for(i = 0; i < len; i++){
		UARTprintf("0x%02x,", data[i]);
	}
	UARTprintf("\n");
	
	if(!memcmp(data, "trigon", 6)){
		UARTprintf("TRIGER ON\n");
		bCR = false;
		bNL = false;
		SCAN_ON;
	}
	else if(!memcmp(data, "trigoff", 7)){
		UARTprintf("TRIGER OFF\n");
		SCAN_OFF;
	}
	else if(!memcmp(data, "reboot", 6)){
		UARTprintf("POWER CYCLE\n");
		SCAN_PWR_CYL;
	}	
	else if(!memcmp(data, "eecfg", 5)){
		UARTprintf("e mode test\n");
		scanner_send_cmd(set_rd_mode);
		delay_ms(100);
		scanner_send_cmd(save_config);
		delay_ms(100);
		scanner_send_cmd(reboot);
		delay_ms(500);
		
		scanner_send_cmd(get_rd_mode);
	}
	else if(!memcmp(data, "default", 7)){
		scanner_send_cmd(set_default);
	}
	else{
		scanner_send((char*)data, len);
		//UARTprintf("Not defined operation\n");
	}
}


void USB_test_start(){
	set_scanner_cb(/*USB_SCANNER_CB*/main_bc_cb);

	USB_svc_set_CB(USB_rcv_cn);
	while(1){
		
		keybaord();
		
		if(enter){
			enter = false;
			bCR = false;
			bNL = false;
			SCAN_ON;
		}

		if(left){
			left = false;
			SCAN_OFF;
		}
	}
}

/**********USB TEST********/

void main(void)
{
	uint8_t ui8ButtonState, ui8ButtonChanged;
	uint32_t ui32Period;		// Timer0A timer period
	float fVBuf;
	bool pr = true;

	//
	// Enable lazy stacking for interrupt handlers.  This allows floating-point
	// instructions to be used within interrupt handlers, but at the expense of
	// extra stack usage.
	//
	ROM_IntMasterDisable();
	ROM_FPULazyStackingEnable();
	ROM_FPUEnable();

	system_clock_init();

	system_parameter_init();
	SYS_INFO* info = Get_system_info();
	SYS_PAR* par = Get_system_parameter();
	TEST_DATA* TD = Get_test_data();

	USBHwInit();

	set_language((LANGUAGE)info->Language_Select);

	//
	// Initialize the peripherals that we will be redirecting.
	//
	UARTHwInit();

	// Initial Hardware version
	hardware_version_initial();

	// 20171227
	SCANNERHwInit();

	PWMHWInit();

	//FlsSSIHwInit();
	ST7565HwInit(info->LCDRratio);

	ButtonsInit();

	ST7565SetBrightness(LCM_BOOSTER_CMD? (info->bias9bter5x_LCDcontrast):(info->LCDcontrast));

	BattCoreHwInit();

	CLK_S35190AHWInit();
	ExtRTCInit();

	EPR_25LC512HWInit();
	
#if !defined(PCB_CHECK_MODE)
	ExtFls_header_init();
#endif

	TP337AHWInit();

	// { Bluetooth UART IOs configuration
	//BTHwInit();
	// }	

	//
	// To define Timer2A (30 sec) for cranking volt detect in system test.
	// {
	ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER2);
	ROM_TimerConfigure(TIMER2_BASE, TIMER_CFG_ONE_SHOT);			// Full-width one-shot timer

	ROM_TimerLoadSet(TIMER2_BASE, TIMER_A, get_sys_clk()*30 - 1);	// Timer interrupt in 30 sec

	ROM_IntEnable(INT_TIMER2A);
	ROM_TimerIntEnable(TIMER2_BASE, TIMER_TIMA_TIMEOUT);
	// }

	// To define Timer0A (15 sec) for countdown 15 sec of "Turn Headlights On for 15 sec" if "Test In Vehicle" is Yes
	// when surface charge present.
	// {
	ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);
	ROM_TimerConfigure(TIMER0_BASE, TIMER_CFG_PERIODIC);			// Full-width periodic timer

	ui32Period = get_sys_clk();	// Timer interrupt in every 1 sec
	ROM_TimerLoadSet(TIMER0_BASE, TIMER_A, ui32Period -1);

	ROM_IntEnable(INT_TIMER0A);
	ROM_TimerIntEnable(TIMER0_BASE, TIMER_TIMA_TIMEOUT);
	ROM_IntMasterEnable();
	// }

	// Clears the screen  
	ST7565ClearScreen(BUFFER0);
	ST7565ClearScreen(BUFFER1);

	// Refresh the screen to see the results
	ST7565Refresh(BUFFER0);
	
	GrContextInit(&g_sContext, &g_sST7565V128x64x1);

	MP205HWInit();
	
	VMHWInit();
	AMHWInit();
	
	Temp_LED_HwInit();

	wifi_driver_IO_config();

	Socket_svc_init();
	
	wifi_config(info->WIFIPR, wifi_service_cb);

#if defined(PCB_CHECK_MODE)
	set_system_mode(MFC);
#else	

	ST7650BacklightSet(info->main_LcdBacklightConfig);

	set_system_mode(bootup_check);
	if(EngMode()){
		set_system_mode(engineer_mode);
	}
	else{
		
		//if(info->WIFIPR && !USBConnect()){
		//	wifi_config(info->WIFIPR, wifi_service_cb);
		//}

		StartAnimation();

	}
#endif

	BattCoreScanVoltage(&fVBuf);
	
	Time_adjust(info->Time_zone);

	//flash_upgrade();

	if(get_system_mode() == manual_test){
		if(info->pre_test_state){
			if(par->MainMenu < 4) par->WHICH_TEST = par->MainMenu;
			set_system_mode((info->pre_test_state == 1)? pre_tested:pre_printed);
			if(info->pre_test_state == 1) TD->Code_test = 1;
		}
	}


	Widget_init(get_system_mode());

	
	UARTprintf("sizeof SYS_INFO = %d\n", sizeof(SYS_INFO));
	UARTprintf("sizeof SYS_PAR = %d\n", sizeof(SYS_PAR));
	UARTprintf("sizeof BATTERY_SARTSTOP_RECORD_T = %d\n", sizeof(BATTERY_SARTSTOP_RECORD_T));
	UARTprintf("sizeof SYSTEM_TEST_RECORD_T = %d\n", sizeof(SYSTEM_TEST_RECORD_T));

	//message("this is N version");
#if AWS_IOT_SERVICE
	if((get_system_mode() == manual_test || get_system_mode() == MFC) && !info->device_registered){
		Message_by_language(unregistered_device_plz_register, 0, en_US);
	}
#endif

	/*
	 TEST	 TEST	 TEST	 TEST	 TEST	 TEST	 TEST	 TEST	 TEST
	 */
	//USB_test_start();

	while(1)
	{

		//**********************************************
		//USB plug-in -> USB main function: USBPCModeSvc
		//**********************************************
		if(get_system_mode() == manual_test || get_system_mode() == MFC){
			if(USBConnect()){
				wifi_config(false, wifi_service_cb);
			ST7650BacklightSet(info->main_LcdBacklightConfig);
				ST7650Blue();
				ROM_TimerDisable(TIMER0_BASE, TIMER_A);
				PWMEnable();
		
				set_system_mode(USB_mode);
				Widget_init(get_system_mode());
			}
		}

		//
		// Read the debounced state of the buttons.
		//
		ui8ButtonState = ButtonsPoll(&ui8ButtonChanged, 0);

		keybaord();
		
		if(get_widget_mode() == widget_normal || current_W->no_messgae == 1){
		if(BUTTON_PRESSED(UP_BUTTON, ui8ButtonState, ui8ButtonChanged)|| up)
		{
			up = false;
//			if(loc_timer){
//				counter = SOFT_T_IDLE_COUNTER;
//			}
			if(current_W->Key_UP){
			    current_W->Key_UP();
			    if(current_W->Combo & Combo_Up){
			        while(UpBtn(3)){
			            current_W->Key_UP();
			        }
			    }
			}

		}
		if(BUTTON_PRESSED(DOWN_BUTTON, ui8ButtonState, ui8ButtonChanged)|| down)
		{
			down = false;
//			if(loc_timer){
//				counter = SOFT_T_IDLE_COUNTER;
//			}
			if(current_W->Key_DOWN){
				current_W->Key_DOWN();
				if(current_W->Combo & Combo_down){
				    while(DownBtn(3)){
				        current_W->Key_DOWN();
				    }
				}
			}

		}
		if(BUTTON_PRESSED(LEFT_BUTTON, ui8ButtonState, ui8ButtonChanged)|| left)
		{
			left = false;
//			if(loc_timer){
//			    counter = SOFT_T_IDLE_COUNTER;
//			}
			if(current_W->Key_LEFT){
			    current_W->Key_LEFT();
			    uint32_t comb_count = 0;
			    if(current_W->Combo & Combo_left){
			        while(LeftBtn(3)){
			            if(comb_count >= 3 && current_W->Key_LEFT_SUPER){
			                current_W->Key_LEFT_SUPER();
			            }
			            else{
			                current_W->Key_LEFT();
			                comb_count++;
			            }
			        }
			    }
			}
			
		}
		if(BUTTON_PRESSED(RIGHT_BUTTON, ui8ButtonState, ui8ButtonChanged)|| right)
		{
			right = false;
//			if(loc_timer){
//				counter = SOFT_T_IDLE_COUNTER;
//			}
			if(current_W->Key_RIGHT){
			    current_W->Key_RIGHT();
			    uint32_t comb_count = 0;
			    if(current_W->Combo & Combo_right){
			        while(RightBtn(3)){
			            if(comb_count >= 3 && current_W->Key_RIGHT_SUPER){
			                current_W->Key_RIGHT_SUPER();
			            }
			            else{
			                current_W->Key_RIGHT();
			                comb_count++;
			            }
			        }
			    }
			}
		}
			if(BUTTON_PRESSED(SELECT_BUTTON, ui8ButtonState, ui8ButtonChanged)|!pr || enter)
			{
			    enter = false;
//				if(loc_timer){
//					counter = SOFT_T_IDLE_COUNTER;
//				}
				if(current_W->Key_SELECT){
					current_W->Key_SELECT();
	                uint32_t comb_count = 0;
	                if(current_W->Combo & Combo_enter){
	                    while(SelectBtn(3)){
	                        if(current_W->Combo & Combo_enter){
	                            if(comb_count >= 3 && current_W->Key_SELECT_SUPER){
	                            current_W->Key_SELECT_SUPER();
	                            }
	                            else{
	                            current_W->Key_SELECT();
	                            comb_count++;
	                            }
	                        }
	                    }
	                }

				}

			}
		}
		else if(get_widget_mode() == widget_message_wait){
			message_display(0);
			set_widget_mode(widget_message_show);
		}
		else if(get_widget_mode() == widget_message_show){
			if(BUTTON_PRESSED(DOWN_BUTTON, ui8ButtonState, ui8ButtonChanged)|| down){
				down = false;
				message_roll(message_down);
			}

			if(BUTTON_PRESSED(UP_BUTTON, ui8ButtonState, ui8ButtonChanged)|| up){
				up = false;
				message_roll(message_up);
			}

			message_routine();

			if(BUTTON_PRESSED(SELECT_BUTTON, ui8ButtonState, ui8ButtonChanged) || enter){
				enter = false;
				set_widget_mode(widget_normal);
				if(current_W->display){
					current_W->display();
				}
			}
		}

		Widget_routine();

        wifi_service_poll();   //20200730 DW MASK NO THIS FUNCTION
		HTTP_service_poll();   //20200730 DW MASK NO THIS FUNCTION
		AWS_IoT_poll();
		ExtFlsPoll();
		OTA_service_poll();

		if(test[0]){
			test[0] = false;

			static uint32_t y_test = 0;
			LCD_Select_Arrow(y_test);
			y_test+= 16;
			y_test %= 64;
		}

		if(test[1]){
		    test[1] = false;

		}

		if(test[2]){
			test[2] = false;
			info->TotalTestRecord = 0;
			info->TestRecordOffset = 0;
			save_system_info(&info->TestRecordOffset, sizeof(info->TestRecordOffset));
			save_system_info(&info->TotalTestRecord, sizeof(info->TotalTestRecord));
		}

		if(test[3]){
		    test[3] = false;
		    UT_Test_record_creat();
		}
	}
}




