//*****************************************************************************
//
// Source File: led_iocfg.c
// Description: temp board led GPIO Configuration.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 07/27/21     David.W        Created file.
// ============================================================================
//
// Copyright (c) 2021 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#include <stdbool.h>
#include <stdint.h>
#include "inc/hw_memmap.h"
#include "inc/hw_adc.h"
#include "inc/hw_types.h"
#include "driverlib/rom.h"
#include "driverlib/sysctl.h"
#include "driverlib/adc.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "led_iocfg.h"
#include "delaydrv.h"

void Temp_LED_HwInit(void)
{

    ROM_SysCtlPeripheralEnable(TEMP_led_PERIPHERAL);
    ROM_GPIOPinTypeGPIOOutput(TEMP_led_BASE, TEMP_led_pin);    // Output Direction

    TEMP_led_ON;

    delay_ms(500);

    TEMP_led_OFF;

}



