//*****************************************************************************
//
// Source File: wifi_driver.c
// Description: wifi module driver layer.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 12/19/17     Jerry.W        Created file.
// 05/18/18     Jerry.W        modify receiving handler for download => coudl have "TRAILER" in data section.
// 10/17/18     Jerry.W        add pull-down resistance on CTS pin.
// 11/20/18     Morgan         correct typo
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/driver/wifi_driver.c#1 $
// $DateTime: 2017/11/27 10:38:22 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#define WIFI_DRIVER_DEBUG 1

#if !WIFI_DRIVER_DEBUG
static void UARTprintf(const char *pcString, ...){}
#endif

#include "wifi_driver.h"
#include "delaydrv.h"
#include "mem_protect.h"


#define WIFI_POWER(on_off) ROM_GPIOPinWrite(WIFIUART_POWER_IOPORT_BASE, WIFI_POWER_PIN, (on_off)? 0x00 : WIFI_POWER_PIN )
#define WIFI_RESET_N(on_off) ROM_GPIOPinWrite(WIFIUART_CONTROL_RST_IOPORT_BASE, WIFI_RESET_N_PIN, (on_off)? 0x00 : WIFI_RESET_N_PIN )
#define WIFI_CONFIG(on_off) ROM_GPIOPinWrite(WIFIUART_CONTROL_SLP_IOPORT_BASE, WIFI_CONFIG_PIN, (!on_off)? WIFI_CONFIG_PIN : 0x00)

typedef enum{
	ON = 1,
	OFF = 0
}ON_OFF_T;


#define HEADER			0xF5
#define HEADER_ACK		0xF6
#define TRAILER			0xF3

#define MAX_CHANN		5


typedef enum{
	ACK_success = 1,
	ACK_len_error = 2,		//length > buffer size
	ACK_tailer_error = 3	//trailer position didn't match length
}ACK_T;


typedef struct{
	uint8_t Channel;
	void (*command_cb)(uint8_t *,uint16_t);
}CHANN;


CHANN Channel_list[MAX_CHANN];
uint8_t chan_num = 0;

static void (*error_cb)(uint8_t );



/* Package layout
 *  -----------------------------------------
 * | 1 byte | 1 byte  | 2 byte | length byte | 1 byte  |
 * | HEADER | channel | length |   payload   | TRAILER |
 *  -----------------------------------------
 */

/* ACK layout
 *  -------------------------------
 * |   1 byte   | 1 byte | 1 byte  |
 * | HEADER_ACK | ACK_T  | TRAILER |
 *  -------------------------------
 */


typedef struct com{
	volatile uint8_t state;
	uint8_t Channel;
	uint8_t *data;
	uint16_t len;
	volatile uint16_t sent_count;
	struct com *next;
}com_T;

enum {
	wait_to_send,
	sending,
	waiting_ACK,
	finished,
};

/*************** TX buffer *************/
volatile com_T* TX_COM_head = NULL;
volatile com_T COM_ACK;
static uint16_t total_COM_leng = 0;
static uint8_t ack_buffer[3] = {HEADER_ACK, 0x00, TRAILER};

/*************** RX buffer *************/
static uint8_t RX_buffer[WIFI_RX_BUFFER_SIZE];
volatile static uint16_t RX_index = 0, RX_length = 0;
volatile static uint8_t RX_com_chann;

/************** time out counter *******/
static uint8_t Waiting_counter = 0;
static uint8_t retransmit_time = 0;
static Timer_t * WIFI_driver_timer;

static bool WIFI_driver_enabled = false;

static void TX_send(void);
static void TX_check_ACK(void);
static void RX_ACK(ACK_T ack);
void WIFI_driver_Timer_f(void);


void wifi_driver_IO_config(){
	//enable io port clock
	ROM_SysCtlPeripheralEnable(WIFIUART_IOPORT_SYSCTL_PERIPH);
	ROM_SysCtlPeripheralEnable(WIFIUART_CONTROL_RST_IOPORT_SYSCTL_PERIPH);
	ROM_SysCtlPeripheralEnable(WIFIUART_CONTROL_SLP_IOPORT_SYSCTL_PERIPH);
	ROM_SysCtlPeripheralEnable(WIFIUART_POWER_IOPORT_SYSCTL_PERIPH);

	//set UART IO pin function
	ROM_GPIOPinConfigure(WIFI_RX_MUX_SEL);
	ROM_GPIOPinConfigure(WIFI_TX_MUX_SEL);
	ROM_GPIOPinConfigure(WIFI_RTS_MUX_SEL);
	ROM_GPIOPinConfigure(WIFI_CTS_MUX_SEL);
	ROM_GPIOPinTypeUART(WIFIUART_IOPORT_BASE, WIFI_RX_PIN | WIFI_TX_PIN | WIFI_RTS_PIN | WIFI_CTS_PIN);
	GPIOPadConfigSet(WIFIUART_IOPORT_BASE, WIFI_CTS_PIN, GPIO_STRENGTH_4MA, GPIO_PIN_TYPE_STD_WPD);

	//set control and power IO pin function
	ROM_GPIOPinTypeGPIOOutput(WIFIUART_CONTROL_RST_IOPORT_BASE , WIFI_RESET_N_PIN);
	ROM_GPIOPinTypeGPIOOutput(WIFIUART_CONTROL_SLP_IOPORT_BASE , WIFI_CONFIG_PIN);
	ROM_GPIOPinTypeGPIOOutput(WIFIUART_POWER_IOPORT_BASE , WIFI_POWER_PIN );

	WIFI_POWER(OFF);
}

void wifi_driver_init(uint8_t on_off){
	if(on_off){
		WIFI_driver_enabled = true;

		//module power on and reset_n off
		WIFI_POWER(ON);
		WIFI_RESET_N(OFF);
		WIFI_CONFIG(OFF);

		//enable UART module clock
		ROM_SysCtlPeripheralEnable(WIFIUART_MODULE_SYSCTL_PERIPH);


		//set baudrate
		ROM_UARTConfigSetExpClk(WIFIUART_BASE, get_sys_clk(), WIFI_UART_BAUDRATE, (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE));

		//set interrupt
		ROM_UARTIntEnable(WIFIUART_BASE, UART_INT_RX | UART_INT_RT);
		ROM_UARTIntEnable(WIFIUART_BASE, UART_INT_TX);
		ROM_UARTTxIntModeSet(WIFIUART_BASE, UART_TXINT_MODE_EOT);
		ROM_IntEnable(WIFI_BTUART_INT);

		//enable flow control
		UARTFlowControlSet(WIFIUART_BASE, UART_FLOWCONTROL_TX | UART_FLOWCONTROL_RX );


		//initial buffer
		COM_ACK.data = ack_buffer;
		COM_ACK.state = finished;
		
		if(!WIFI_driver_timer){
			WIFI_driver_timer = register_timer(1, WIFI_driver_Timer_f);
			start_timer(WIFI_driver_timer);
		}
	}
	else{
		WIFI_driver_enabled = false;

		//module power off and reset_n on
		WIFI_POWER(OFF);
		WIFI_RESET_N(ON);
		WIFI_CONFIG(OFF);


		//disable UART
		ROM_SysCtlPeripheralDisable(WIFIUART_MODULE_SYSCTL_PERIPH);
		
		if(WIFI_driver_timer){
			unregister_timer(WIFI_driver_timer);
			WIFI_driver_timer = NULL;
		}
	}
}



void wifi_driver_regist_error_cb(void (*err_cb)(uint8_t )){
	error_cb = err_cb;
}

bool wifi_driver_regist_com_cb(uint8_t channel,void (*com_cb)(uint8_t *,uint16_t)){
	uint8_t i;

	if(chan_num == MAX_CHANN) {
		return false;
	}

	for(i = 0 ; i < chan_num ; i++){
		if(Channel_list[i].Channel == channel){
			Channel_list[i].command_cb = com_cb;
			return true;
		}
	}

	Channel_list[chan_num].Channel = channel;
	Channel_list[chan_num].command_cb = com_cb;
	chan_num++;
	return true;
}


uint8_t wifi_driver_send_command(uint8_t chann,uint8_t * cmmd, uint16_t len){
	com_T *temp,*temp2;
	if((total_COM_leng + len > WIFI_TX_BUFFER_SIZE)||(WIFI_driver_enabled == false)){
		free(cmmd);
		return 1;
	}
	else{
		temp = malloc(sizeof(com_T));
		if(temp == NULL) {
			free(cmmd);
			return 1;
		}
		total_COM_leng += (len + sizeof(com_T));
		temp->Channel = chann;
		temp->data = cmmd;
		temp->len = len;
		temp->state = wait_to_send;
		temp->sent_count = 0;
		temp->next = NULL;
		IntMasterDisable();
		if(TX_COM_head == NULL){
			TX_COM_head = temp;
		}
		else{
			temp2 = (com_T *)TX_COM_head;
			while(temp2->next){
				temp2 = temp2->next;
			}
			temp2->next = temp;
		}
		TX_send();
		IntMasterEnable();
		return 0;
	}

}




void WIFI_UARTIntHandler(void){
	uint32_t ui32Status, i;
	ui32Status = UARTIntStatus(WIFIUART_BASE, true);

	UARTIntClear(WIFIUART_BASE, ui32Status);

	if(ui32Status & (UART_INT_RT | UART_INT_RX)){
		while(UARTCharsAvail(WIFIUART_BASE)){
			RX_buffer[RX_index] = UARTCharGetNonBlocking(WIFIUART_BASE);
			switch(RX_index){
			case 0 :
				if((RX_buffer[0] == HEADER)||(RX_buffer[0] == HEADER_ACK)){
					RX_index++;
				}
				else{
					UARTprintf("%c",RX_buffer[0]);
				}
				break;
			case 1 :
				RX_index++;
				break;
			case 2 :
				if(RX_buffer[0] == HEADER_ACK){
					TX_check_ACK();
					RX_index = 0;
				}
				else{
					RX_index++;
				}
				break;
			case 3 :
				if(RX_buffer[0] == HEADER){
					RX_length = *(uint16_t*)(RX_buffer + 2);
					if(RX_length + 4  < WIFI_RX_BUFFER_SIZE){
						RX_index++;
					}
					else{
						RX_ACK(ACK_len_error);
						UARTprintf("ACK_len_error:%d\n",RX_length);
						RX_index = 0;
					}
				}

				break;
			default :
				if(RX_index == RX_length + 4){
					if(RX_buffer[RX_index] != TRAILER){
						RX_ACK(ACK_tailer_error);
						UARTprintf("ACK_tailer_error\n");
					}
					else{
						RX_ACK(ACK_success);
						for(i = 0; i <= chan_num; i++){
							if(i == chan_num){
								error_cb(unregistered_channel);
							}
							else{
								if(Channel_list[i].Channel == RX_buffer[1]){
									if(Channel_list[i].command_cb) Channel_list[i].command_cb( RX_buffer+4, RX_length);
									break;
								}
						}
					}

					}
						RX_length = 0;
						RX_index = 0;
				    
				}
				else{
					RX_index++;
				}

				break;
			}


		}
	}



	if(ui32Status & UART_INT_TX){
		TX_send();
	}


}


static void TX_send(void){
	uint8_t temp;
	if((COM_ACK.state == sending)||(TX_COM_head&&(TX_COM_head->state == sending))){
		//continue to send data
		if(COM_ACK.state == sending){
			//sending ACK
			while(COM_ACK.sent_count != 3){
				if(UARTCharPutNonBlocking(WIFIUART_BASE, COM_ACK.data[COM_ACK.sent_count])){
					COM_ACK.sent_count++;
				}
				else{
					return;
				}
			}
			COM_ACK.state = finished;
		}
		else{
			//sending command
			while((TX_COM_head->sent_count - 5) != TX_COM_head->len){
				temp = (TX_COM_head->sent_count == 0)? HEADER:
						(TX_COM_head->sent_count == 1)? (TX_COM_head->Channel):
						(TX_COM_head->sent_count == 2)? (TX_COM_head->len)&0xFF:
						(TX_COM_head->sent_count == 3)? (TX_COM_head->len >> 8)&0xFF:
						(TX_COM_head->sent_count - 4 == TX_COM_head->len)? TRAILER: TX_COM_head->data[TX_COM_head->sent_count - 4];
				if(UARTCharPutNonBlocking(WIFIUART_BASE, temp)){
					TX_COM_head->sent_count++;
				}
				else{
					return;
				}
			}

			TX_COM_head->state = waiting_ACK;

		}
	}
	else{
		if(COM_ACK.state == wait_to_send){
			COM_ACK.state = sending;
			TX_send();
		}
		else if(TX_COM_head&&(TX_COM_head->state == wait_to_send)){
			TX_COM_head->state = sending;
			TX_send();
		}
	}

}

static void RX_ACK(ACK_T ack){
	COM_ACK.data[1] = ack;
	COM_ACK.sent_count = 0;
	COM_ACK.state = wait_to_send;
	TX_send();
}

static void TX_check_ACK(void){
	com_T *temp;
	if((RX_buffer[2] != TRAILER)||(RX_buffer[1] != ACK_success)){
		//communication fail send again
		if(TX_COM_head){
			TX_COM_head->sent_count = 0;
			TX_COM_head->state = wait_to_send;
		}
	}
	else{
		if(TX_COM_head&&(TX_COM_head->state == waiting_ACK)){
			temp = (com_T *)TX_COM_head;
			TX_COM_head = TX_COM_head->next;
			total_COM_leng -= (temp->len + sizeof(com_T));
			free(temp->data);
			free(temp);
			Waiting_counter = 0;
			retransmit_time = 0;

		}
	}
	TX_send();
}

//used for timeout retransmission
void WIFI_driver_Timer_f(void)
{
	if(TX_COM_head&&(TX_COM_head->state == waiting_ACK)){
		Waiting_counter++;
		if(Waiting_counter>10){
			TX_COM_head->sent_count = 0;
			TX_COM_head->state = wait_to_send;
			Waiting_counter = 0;
			retransmit_time++;
			if(retransmit_time > 10){
				if(error_cb){
					error_cb(module_no_response);
				}
			}
			TX_send();
		}
	}
}




