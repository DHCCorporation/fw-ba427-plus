/*
 * board.c
 *
 *  Created on: 2020�~2��27��
 *      Author: henry.yang
 */

#include "board.h"
#include "uartstdio.h"

static HWVer_t HW_Ver = hw_known;
static uint8_t HWVer_init = false;

static
void HWver_IOcfg(void)
{
    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);
	ROM_SysCtlPeripheralEnable(ADC_HWVER_PERIPHERAL);

	//
    // Configure the pins to be used as analog inputs.
    //
	ROM_GPIOPinTypeADC(ADC_HWVER_BASE, ADC_HWVER_PIN);

	//
    // Select the external reference for greatest accuracy.
    //
    ROM_ADCReferenceSet(ADC0_BASE, ADC_REF_EXT_3V);

	//
    // Disable ADC sequencers
    //
    ROM_ADCSequenceDisable(ADC0_BASE, 1);

	//
    // Initialize both ADC peripherals using sequencer 1 and processor trigger.
    //
    ROM_ADCSequenceConfigure(ADC0_BASE, 1, ADC_TRIGGER_PROCESSOR, 0);

	//
	//	The manner of changing the ADC speed has changed on TM4C devices from LM3S devices.  
	//	This change is not yet comprehended in TivaWare. As a result, the SysCtlADCSpeedSet function may not work correctly.  
	//	To ensure proper operation, remove any references to the SysCtlADCSpeedSet function and use the following instead.
	//
	//	(ps: http://e2e.ti.com/support/microcontrollers/tiva_arm/f/908/p/293858/1218708 or 
	//		 https://e2e.ti.com/support/microcontrollers/tiva_arm/f/908/t/331481)
	//	Original config code: SysCtlADCSpeedSet(SYSCTL_ADCSPEED_1MSPS);
	//
	HWREG(ADC0_BASE + ADC_O_PC) = (ADC_PC_SR_1M);
	
}

static
uint32_t GetHWVerADC(uint8_t ui8SampleNum)
{
	if(!HWVer_init){
		HWver_IOcfg();
		HWVer_init = true;
	}
	

	uint32_t temp = 0,buf;
	uint8_t ui8LopIdx = 0;

	ROM_ADCSequenceDisable(ADC0_BASE, 3);
	ROM_ADCSequenceConfigure(ADC0_BASE, 3, ADC_TRIGGER_PROCESSOR, 0);
	ROM_ADCSequenceStepConfigure(ADC0_BASE, 3, 0, ADC_HWVER_CH | ADC_CTL_IE | ADC_CTL_END);
	ROM_ADCSequenceEnable(ADC0_BASE, 3);

	for(ui8LopIdx = 0; ui8LopIdx < ui8SampleNum; ui8LopIdx++)
	{
		ROM_ADCIntClear(ADC0_BASE, 3);
		ROM_ADCProcessorTrigger(ADC0_BASE, 3);
		while(!ROM_ADCIntStatus(ADC0_BASE, 3, false));
		ROM_ADCSequenceDataGet(ADC0_BASE, 3, &buf);
		temp += buf;
	}

	//Average
	temp = temp / ui8SampleNum;
	return (temp);
}


HWVer_t get_hardware_version(void)
{
	return HW_Ver;
}

uint8_t hardware_version_initial(void)
{
	uint32_t temp = GetHWVerADC(10);
	
	if(temp < ADC_HWVer_V00){
		HW_Ver = hw_v00;
	}
	else if(temp < ADC_HWVer_V01){
		HW_Ver = hw_v01;
	}
	else{
		HW_Ver = hw_known;
	}

	UARTprintf("GetHWVerADC(): %d, HW version: %02d\n",temp,HW_Ver);
	
	return HW_Ver;
}









