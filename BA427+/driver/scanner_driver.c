//*****************************************************************************
//
// Source File: scanner_driver.c
// Description: Scanner hardware definition.
//
//
// Edit History:
//
// when 		who 		   what, where, why  
// -------- 	----------	   ------------------------------------------------
// 12/27/17 	Henry.Y 	   Create file.
// 09/11/18     Henry.Y        Use system driver interface to replace global variable.
//                             Add power switch pin definition.
// 02/24/20     Henry.Y        Scanner control modification.
// =============================================================================
//
//
// $Header$
// $DateTime$
// $Author$
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#ifndef DRIVER_SCANNER_IOCFG_C_
#define DRIVER_SCANNER_IOCFG_C_

#include "scanner_driver.h"
#include "delaydrv.h"






void SCANNERUartIOConfig(void){
	//
	// Enable the peripherals that we will be redirecting.
	// The UART itself needs to be enabled, as well as the GPIO port
	// containing the pins that will be used.
	//
	ROM_SysCtlPeripheralEnable(SCANNER_UART_MODULE_SYSCTL_PERIPH);
	ROM_SysCtlPeripheralEnable(SCANNER_UART_PORT_SYSCTL_PERIPH);

	//
	// Configure the GPIO pin muxing for the UART function.
	// This is only necessary if your part supports GPIO pin function muxing.
	// Study the data sheet to see which functions are allocated per pin.
	//
    ROM_GPIOPinConfigure(U0RX_MUX_SEL);
    ROM_GPIOPinConfigure(U0TX_MUX_SEL);

	//
	// Since GPIO P0 and P1 are used for the UART function, they must be
	// configured for use as a peripheral function (instead of GPIO).
	//
	ROM_GPIOPinTypeUART(SCANNER_UART_PORT_BASE, U0RX_PIN | U0TX_PIN);

	//
    // Set the default UART configuration.
    //
	ROM_UARTConfigSetExpClk(SCANNER_UART_BASE, get_sys_clk(), UART_BAUDRATE_SCANNER, SCANNER_UART_CONFIG);

	//
    // Configure and enable UART interrupts.
    //	
	ROM_UARTIntEnable(SCANNER_UART_BASE, UART_INT_RX | UART_INT_RT);	
	ROM_UARTIntEnable(SCANNER_UART_BASE, UART_INT_TX);

	//
    // Enable interrupts now that the application is ready to start.
    //
    ROM_IntEnable(SCANNER_UART_INT);
	
}

void SCANNERIOConfig(void){
	
	//power switch pin
	ROM_SysCtlPeripheralEnable(SCANNER_POWER_PORT_SYSCTL_PERIPH);
	ROM_GPIOPinTypeGPIOOutput(SCANNER_POWER_PORT_BASE, SCANNER_POWER_PIN);
	
	
	//BUZZER PIN
	ROM_SysCtlPeripheralEnable(SCANNER_BUZZER_PORT_SYSCTL_PERIPH);
	ROM_GPIOPinTypeGPIOOutput(SCANNER_BUZZER_PORT_BASE, SCANNER_BUZZER_PIN);
	GPIOPadConfigSet(SCANNER_BUZZER_PORT_BASE, SCANNER_BUZZER_PIN, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD);

	BUZZER_OFF;

	
	//TRIGGER PIN
	ROM_SysCtlPeripheralEnable(SCANNER_TRIGGER_PORT_SYSCTL_PERIPH);
	ROM_GPIOPinTypeGPIOOutput(SCANNER_TRIGGER_PORT_BASE, SCANNER_TRIGGER_PIN);
	GPIOPadConfigSet(SCANNER_TRIGGER_PORT_BASE, SCANNER_TRIGGER_PIN, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPU);
	
	scanner_switch(0);
}

void SCANNERHwInit(void){
	//
	// Config UART
	//
	SCANNERUartIOConfig();
	
	//
	// Config I/O
	//
	SCANNERIOConfig();
}

void scanner_switch(uint8_t on_off)
{
	HWVer_t hw_ver = get_hardware_version();
	if(hw_ver == hw_known){
		UARTprintf("scanner_switch fail, known hardware version");
		return;
	}

	if(on_off == 0){
		TRIGGER_OFF;
		delay_ms(5);

		if(hw_ver == 0){//Champtek MIR3+
			SCANNER_P_OFF;
		}
		else if(hw_ver == hw_v01){
			// Honeywell N3680 need power 3.3V always applied to the imager.
			SCANNER_P_ON;
		}
		else{//Known version
			SCANNER_P_OFF;
		}
	}
	else if(on_off == 1){
		if(hw_ver == hw_v00){
			// Champtek reading mode: good read off
			SCANNER_P_ON;
			TRIGGER_ON;
			delay_ms(5);
			TRIGGER_OFF;
		}
		else if(hw_ver == hw_v01){
			// Honeywell N3680 reading mode: trigger on/off
			SCANNER_P_ON;
			TRIGGER_ON;
		}
		else{//Known version
			// Champtek reading mode: good read off
			SCANNER_P_ON;
			TRIGGER_ON;
			delay_ms(5);
			TRIGGER_OFF;
		}
	}
	else if(on_off == 2){
		SCANNER_P_OFF;
		delay_ms(100);
		SCANNER_P_ON;
		TRIGGER_OFF;
	}
	else return;
}



#endif /* DRIVER_SCANNER_IOCFG_C_ */
