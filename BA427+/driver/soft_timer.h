//*****************************************************************************
//
// Source File: soft_timer.h
// Description: software timer
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 01/12/18     Jerry.W        Created file.
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/driver/wifi_driver.h#2 $
// $DateTime: 2017/12/18 16:17:54 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#ifndef __SOFT_TIMER_H__
#define __SOFT_TIMER_H__


#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include "driverlib/rom.h"
#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "driverlib/sysctl.h"
#include "driverlib/timer.h"
#include "uartstdio.h"
#include "driverlib/interrupt.h"
#include "mem_protect.h"


typedef struct{
	uint8_t period;
	uint8_t counter;
	bool started;
	void (*cb)(void);
}Timer_t;


/*************************************************************************************************
 * @ Name		: register_timer
 * @ Brief 		: register a period timer
 * @ Parameter 	: period -- periods in 0.1 second.
				  time_cb -- callback function triggered for every period
 *************************************************************************************************/
Timer_t* register_timer(uint8_t period, void (*time_cb)(void));


/*************************************************************************************************
 * @ Name		: unregister_timer
 * @ Brief 		: unregister a period timer
 *************************************************************************************************/
void unregister_timer(Timer_t* Timer);


/*************************************************************************************************
 * @ Name		: start_timer
 * @ Brief 		: start timer
 *************************************************************************************************/
void start_timer(Timer_t* Timer);




#endif
