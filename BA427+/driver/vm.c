//*****************************************************************************
//
// Source File: vm.c
// Description: Prototype for functions of voltage meter driver.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 01/07/16     Vincent.T      Created file.
// 06/14/16     Vincent.T      1. Modify VMGetAD();(ui32_t buf -> ui64_t buf)
// ============================================================================


#include "vm.h"
#include "inc/hw_types.h"
#include "driverlib/rom.h"
#include "driverlib/adc.h"
#include "uartstdio.h"

void VMIOConfig(void)
{
	UARTprintf("VMIOConfig()\n");

	//Voltage meter AD
	//Enable ADC0
	ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);
	ROM_ADCReferenceSet(ADC0_BASE, ADC_REF_EXT_3V);
	//Peripherial
	ROM_SysCtlPeripheralEnable(VM_PORT);
	//AD pin sel
	ROM_GPIOPinTypeADC(VM_BASE, VM_PIN);
}

void VMHWInit(void)
{
	UARTprintf("VMHWInit()\n");
	VMIOConfig();
}

void VMGetAD(float *pfDat, uint8_t ui8SampleNum)
{
	//UARTprintf("VMGetAD()\n");

	uint8_t ui8LopIdx = 0;
	uint32_t ui32VMTemp = 0;
	uint64_t ui64VMSum = 0;

	ROM_ADCSequenceDisable(ADC0_BASE, 3);
	ROM_ADCSequenceConfigure(ADC0_BASE, 3, ADC_TRIGGER_PROCESSOR, 0);
	ROM_ADCSequenceStepConfigure(ADC0_BASE, 3, 0, VM_CH | ADC_CTL_IE | ADC_CTL_END);
	ROM_ADCSequenceEnable(ADC0_BASE, 3);

	//Acc
	for(ui8LopIdx = 0; ui8LopIdx < ui8SampleNum; ui8LopIdx++)
	{
		ROM_ADCIntClear(ADC0_BASE, 3);
		ROM_ADCProcessorTrigger(ADC0_BASE, 3);
		while(!ROM_ADCIntStatus(ADC0_BASE, 3, false));
		ROM_ADCSequenceDataGet(ADC0_BASE, 3, &ui32VMTemp);
		ui64VMSum = ui64VMSum + ui32VMTemp;
	}

	//Average
	(*pfDat) = (float)(ui64VMSum / ui8SampleNum);
}


