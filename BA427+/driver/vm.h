//*****************************************************************************
//
// Source File: vm.h
// Description: Prototype for functions of voltage meter driver.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 01/07/16     Vincent.T      Created file.
// ============================================================================

#ifndef _VM_H_
#define _VM_H_

#include <stdbool.h>
#include <stdint.h>
#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "inc/hw_sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/sysctl.h"

// Voltage meter
#define VM_PORT SYSCTL_PERIPH_GPIOD
#define VM_BASE GPIO_PORTD_BASE
#define VM_PIN	GPIO_PIN_3
#define VM_CH	ADC_CTL_CH12

void VMIOConfig(void);
void VMHWInit(void);
void VMGetAD(float *pfDat, uint8_t ui8SampleNum);

#endif /* _VM_H_ */
