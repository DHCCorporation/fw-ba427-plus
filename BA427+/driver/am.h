//*****************************************************************************
//
// Source File: am.h
// Description: Prototype for functions of amper meter driver.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 01/12/16     Vincent.T      Created file.
// 03/02/16     Vincent.T      1. Add -5V controller
// 03/20/19     Henry.Y        -5V switch always on.
// ============================================================================

#ifndef _AM_H_
#define _AM_H_

#include <stdbool.h>
#include <stdint.h>
#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "inc/hw_sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/sysctl.h"
#include "delaydrv.h"

// Amper meter
#define AM_PORT SYSCTL_PERIPH_GPIOE
#define AM_BASE GPIO_PORTE_BASE
#define AM_PIN	GPIO_PIN_3
#define AM_CH	ADC_CTL_CH0

// -5V controller
#define NEG5V_PORT	SYSCTL_PERIPH_GPION
#define NEG5V_BASE	GPIO_PORTN_BASE
#define NEG5V_PIN	GPIO_PIN_3
#define AM_NEG5V_PIN_ON ROM_GPIOPinWrite(NEG5V_BASE, NEG5V_PIN, 0x00); delay_ms(2);
//#define AM_NEG5V_PIN_OFF ROM_GPIOPinWrite(NEG5V_BASE, NEG5V_PIN, 0xFF);
#define AM_NEG5V_PIN_OFF ROM_GPIOPinWrite(NEG5V_BASE, NEG5V_PIN, 0x00); // always on



void AMIOConfig(void);
void AMHWInit(void);
void AMGetAD(float *pfDat, uint8_t ui8SampleNum);

#endif /* _AM_H_ */
