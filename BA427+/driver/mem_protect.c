

#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include "driverlib/interrupt.h"
#include "uartstdio.h"

#define MEM_DEBUG 0
#define LOG_LENGTH	400

uint32_t dynamic_mem = 0;
uint32_t max_mem = 0;

typedef struct{
	void* add;
	uint16_t langth;
	uint16_t next_index;

}DY_MEM;



#if MEM_DEBUG
DY_MEM dy_mem_log[LOG_LENGTH] = {0};
uint16_t head_idx = 0, end_idx = 0;


uint16_t mem_log_fine_space(){
	uint16_t i;
	uint8_t block = 1;
	for(i = 0 ; i < LOG_LENGTH ; i++){
		if(dy_mem_log[i].add == 0){
			return i;
		}
	}

	UARTprintf("memory log full !!!!\n");

	while(block){}
	return 0;
}
#endif




void protect_free(void* ptr){
	uint8_t temp = 0;
	if(ptr){
		IntMasterDisable();
		uint32_t temp_len = *((uint32_t *)ptr - 1) - 1;
		dynamic_mem -= temp_len;
#if MEM_DEBUG
		uint8_t block = 1;
		UARTprintf("free %d Byte of %d\n", temp_len, dynamic_mem);	//
		uint16_t free_idx = head_idx;
		uint16_t last_idx = head_idx;
		while(dy_mem_log[free_idx].add != ptr){
			if(free_idx != end_idx){
				last_idx = free_idx;
				free_idx = dy_mem_log[free_idx].next_index;
			}
			else{
				UARTprintf("free error!!\n");
				while(block){};
			}
		}

		if(dy_mem_log[free_idx].langth != temp_len){
			UARTprintf("free error!!\n");
			while(block){};
		}

		UARTprintf("                     [mem] delete %d\n", free_idx);
		dy_mem_log[free_idx].add = 0;
		dy_mem_log[free_idx].langth = 0;

		if(free_idx == head_idx){
			head_idx = dy_mem_log[free_idx].next_index;
		}
		else if(free_idx == end_idx){
			end_idx = last_idx;
		}
		else{
			dy_mem_log[last_idx].next_index = dy_mem_log[free_idx].next_index;
		}

		dy_mem_log[free_idx].next_index = 0;

		while(dynamic_mem > max_mem){}
#endif

		free(ptr);
		IntMasterEnable();
	}
	else{
		temp = temp; //for debugging
	}

}


void *prtect_malloc(size_t _size){
	void* temp;
	uint32_t temp_len;
	IntMasterDisable();
	temp = malloc(_size);
	if(temp) {
		temp_len = *((uint32_t *)temp - 1) - 1;
		dynamic_mem += temp_len;

#if	MEM_DEBUG
		UARTprintf("malloc %d Byte of %d\n", temp_len, dynamic_mem);

		uint16_t newlog_idx = mem_log_fine_space();
		UARTprintf("                     [mem] add %d\n", newlog_idx);
		dy_mem_log[end_idx].next_index = newlog_idx;
		dy_mem_log[newlog_idx].add = temp;
		dy_mem_log[newlog_idx].langth = temp_len;
		end_idx = newlog_idx;
#endif
	}

	if(dynamic_mem > max_mem){
		max_mem = dynamic_mem;
	}
	IntMasterEnable();
	return temp;

}


void print_heap_usage(){

	UARTprintf("Heap max use %d byte\n", max_mem);
	UARTprintf("Heap used %d byte\n", dynamic_mem);

#if MEM_DEBUG
	uint16_t dy_mem_counter = 0,idx = head_idx;

	while(idx != end_idx){
		dy_mem_counter++;
		idx = dy_mem_log[idx].next_index;
	}

	if(dy_mem_log[head_idx].add != 0){
		dy_mem_counter++;
	}

	UARTprintf("dynamic memory count : %d\n", dy_mem_counter);
#endif
}


extern uint32_t __stack;
extern uint32_t __STACK_TOP;

void stack_init(){
	uint32_t *start;

	start = (uint32_t*)&start;
	start--;

	while(start >= &__stack){
		//UARTprintf("%x\n",start);
		*start = 0;
		start --;
	}
}

void get_stack_usage(){
	uint32_t *start = &__stack;
	while(*start == 0){
		start ++;
	}

	UARTprintf("stack usage = %d\n",  (&__STACK_TOP - start)*4);
}
