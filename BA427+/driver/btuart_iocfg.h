//*****************************************************************************
//
// Source File: btuart_iocfg.h
// Description: Bluetooth module IOs and UART GPIO Pins Definition.
//				
//
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 01/07/16     William.H      Created file.
// 04/06/16     Vincent.T      1. Add definiton of Bluetooth 3.3V control pin.
//                             2. Modify Connection status pin.
//                             3. Modify Reset pin.
// 04/05/17     Vincent.T      1. Modify BT_POWER_PERIPHERAL, BT_POWER_BASE and BT_POWER_PIN
// 06/01/17     Henry          FW version: BT2100_WW_V01.a
//                             Add bluetooth device name toggle I/O pin definition
// 11/23/17     Jerry.W        1. fix UART_BAUDRATE redefinition
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/driver/btuart_iocfg.h#1 $
// $DateTime: 2017/11/27 10:38:22 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#ifndef __BTUART_IOCFG_H__
#define __BTUART_IOCFG_H__

#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "driverlib/uart.h"

//*****************************************************************************
//
//  Bluetooth UART Peripherals and I/O Pin Definition
//
//  Below defines the following UART peripherals and I/O signals.  You must
//  review these and change as needed for your own board:
//   - UART peripheral
//   - GPIO Port P peripheral (for UART pins)
//   - U6RX - PP0
//   - U6TX - PP1
//
//*****************************************************************************

/* UART port */
#define BTUART_MODULE_SYSCTL_PERIPH	SYSCTL_PERIPH_UART6
#define BTUART_PORT_SYSCTL_PERIPH	SYSCTL_PERIPH_GPIOP
#define BTUART_PORT_BASE			GPIO_PORTP_BASE
#define BTUART_BASE					UART6_BASE

/* U0RX pin */
#define U6RX_PIN					GPIO_PIN_0
#define U6RX_MUX_SEL				GPIO_PP0_U6RX

/* U0TX pin */
//#define U6TX_PIN					GPIO_PIN_1
//#define U6TX_MUX_SEL				GPIO_PP1_U6TX

/* Baud rate */
#define UART_BAUDRATE_BT				115200

/* Data format for the port (number of data bits, number of stop bits, and parity). */
#define UART_CONFIG					(UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE)

/* UART6 interrupt */
#define BTUART_INT					INT_UART6
//*****************************************************************************
//
//	Bluetooth device name toggle I/O pin definition
//
//	- BT toggle device name I/O - PN2 (write, output pin)
//
//*****************************************************************************
#define BT_DN_PERIPHERAL			SYSCTL_PERIPH_GPION	
#define BT_DN_BASE					GPIO_PORTN_BASE
#define BT_DN_PIN					GPIO_PIN_2
#define DN_DHC						ROM_GPIOPinWrite(BT_DN_BASE, BT_DN_PIN, BT_DN_PIN)//DHC BLE Device/Batter Tester
#define DN_LOW						ROM_GPIOPinWrite(BT_DN_BASE, BT_DN_PIN, 0x00)//DHC BLE Device/Batter Tester

//*****************************************************************************
//
//	Bluetooth module connection status I/O pin definition
//
//	- BT connection status - PN1 (read, input pin)
//
//*****************************************************************************
#define BT_CONN_STATUS_PERIPHERAL	SYSCTL_PERIPH_GPION
#define BT_CONN_STATUS_BASE			GPIO_PORTN_BASE
#define BT_CONN_STATUS_PIN			GPIO_PIN_1

//*****************************************************************************
//
//	Bluetooth reset I/O pin definition
//
//	The reset pin is active low, LOW	-> deativate BT module,
//                              HIGH	-> activate BT module.
//
//	- BT reset I/O - PN0 (write, output pin)
//
//*****************************************************************************
#define BT_RESET_PERIPHERAL			SYSCTL_PERIPH_GPION
#define BT_RESET_BASE				GPIO_PORTN_BASE
#define BT_RESET_PIN				GPIO_PIN_0
#define BT_RESET_HIGH				ROM_GPIOPinWrite(BT_RESET_BASE, BT_RESET_PIN, BT_RESET_PIN)
#define BT_RESET_LOW				ROM_GPIOPinWrite(BT_RESET_BASE, BT_RESET_PIN, 0x00)

//*****************************************************************************
//  Bluetooth 3.3v
//  - low -> 3.3 on
//  - high -> 3.3 off
//*****************************************************************************

#define BT_POWER_PERIPHERAL			SYSCTL_PERIPH_GPIOH
#define BT_POWER_BASE				GPIO_PORTH_BASE
#define BT_POWER_PIN				GPIO_PIN_0
#define BT_POWER_ON				    ROM_GPIOPinWrite(BT_POWER_BASE, BT_POWER_PIN, 0x00)
#define BT_POWER_OFF				ROM_GPIOPinWrite(BT_POWER_BASE, BT_POWER_PIN, BT_POWER_PIN)


extern void BTHwInit(void);

#endif // __BTUART_IOCFG_H__
