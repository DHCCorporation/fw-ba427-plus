//*****************************************************************************
//
// Source File: pwm.c
// Description: Prototypes for the pwm driver.
//
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 10/08/15     Vincent T.     pwm.c -> GPIOs setting, enable, disable...
// 09/11/18     Henry.Y        Use system driver interface to replace global variable.
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/driver/pwm.c#5 $
// $DateTime: 2017/09/22 10:27:40 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************


#include "pwm.h"
#include "inc/hw_types.h"
#include "driverlib/rom.h"

#include <stdint.h>
#include <stdbool.h>
#include <math.h>
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/sysctl.h"
#include "driverlib/fpu.h"
#include "driverlib/gpio.h"
#include "driverlib/debug.h"
#include "driverlib/pwm.h"
#include "driverlib/pin_map.h"
#include "inc/hw_gpio.h"
#include "delaydrv.h"


void PWMIOConfig(void)
{
	//GPIO
	ROM_SysCtlPeripheralEnable(PWM_PORT);

	//PWM
	ROM_SysCtlPeripheralEnable(PWM_MODULE_PORT);
	ROM_PWMClockSet(PWM0_BASE,PWM_SYSCLK_DIV_64);
	ROM_GPIOPinConfigure(PWM_MOUDLE_PIN);
	ROM_GPIOPinTypePWM(PWM_BASE, PWM_PIN);
}

void PWMHWInit(void)
{
	uint32_t ui32Load = (((get_sys_clk() / 64) / PWM_DIV_64_FREQ) - 1);

	//*************************
	//Defalut Backlight Setting
	//*************************
	PWMIOConfig();
	ROM_PWMGenConfigure(PWM_MODULE_BASE, PWM_GEN_2, PWM_GEN_MODE_DOWN);
	//100Hz
	ROM_PWMGenPeriodSet(PWM_MODULE_BASE, PWM_GEN_2, ui32Load);
	//PWM0 -> 0% duty
	ROM_PWMPulseWidthSet(PWM_MODULE_BASE, PWM_OUT_4, (uint32_t)(ui32Load/100));
	
	//
	// Enable the outputs.
	//
	ROM_PWMOutputState(PWM_MODULE_BASE, PWM_OUT_4_BIT, true);
	//
	// Start the timers in generator 2.
	//
	ROM_PWMGenEnable(PWM_MODULE_BASE, PWM_GEN_2);
	
	//PWMDisable();
}

void PWMDisable(void)
{
	ROM_PWMOutputState(PWM_MODULE_BASE, PWM_OUT_4_BIT, false);
	ROM_PWMGenDisable(PWM_MODULE_BASE, PWM_GEN_2);
}

void PWMEnable(void)
{
	ROM_PWMOutputState(PWM_MODULE_BASE, PWM_OUT_4_BIT, true);
	ROM_PWMGenEnable(PWM_MODULE_BASE, PWM_GEN_2);
}

void PWMDutySet(uint8_t ui8Duty)
{
	uint32_t ui32PWMPeriod = (((get_sys_clk() / 64) / PWM_DIV_64_FREQ) - 1);
	float ui32PWMDuty = 0;
	//********************
	//Check uiDuty 0 ~ 100
	//********************
	if(ui8Duty >= 100)
	{
		ui8Duty = 99;
	}
	else if(ui8Duty <=1)
	{
	    PWMDisable();
	    return;
	}
	ui32PWMDuty = ((float)ui8Duty / 100);
	ui32PWMDuty = (float)ui32PWMPeriod * ui32PWMDuty;
	PWMDisable();
	ROM_PWMPulseWidthSet(PWM_MODULE_BASE, PWM_OUT_4, (uint32_t)ui32PWMDuty);
	PWMEnable();
}

