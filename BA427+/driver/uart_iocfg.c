//*****************************************************************************
//
// Source File: uart_iocfg.c
// Description: UART GPIO Configuration.
//				
//
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 07/03/15     William.H      Created file.
// 10/05/15     William.H      Check in BT2020 project which branched from LCDFontMaker(Pre-BT2020) by modifed related GPIOs.
// 03/22/16     Henry.Y        Layout "P-BT2100-VOO.b": change UART0 to UART7. 
// 04/08/16     Vincent.T      1. Modify UARTHwInit(); UARTStdioConfig(0, x, x) -> UARTStdioConfig(2, x, x);
// 09/11/18     Henry.Y        Use system driver interface to replace global variable.
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/driver/uart_iocfg.c#5 $
// $DateTime: 2017/09/22 10:27:40 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#include <stdbool.h>
#include <stdint.h>
#include "driverlib/uart.h"
#include "driverlib/rom.h"
#include "uartstdio.h"
#include "uart_iocfg.h"
#include "delaydrv.h"



void UARTIOConfig(void)
{
	//
	// Enable the peripherals that we will be redirecting.
	// The UART itself needs to be enabled, as well as the GPIO port
	// containing the pins that will be used.
	//
	ROM_SysCtlPeripheralEnable(UART_MODULE_SYSCTL_PERIPH);
	ROM_SysCtlPeripheralEnable(UART_PORT_SYSCTL_PERIPH);

	//
	// Configure the GPIO pin muxing for the UART function.
	// This is only necessary if your part supports GPIO pin function muxing.
	// Study the data sheet to see which functions are allocated per pin.
	//
    ROM_GPIOPinConfigure(U7RX_MUX_SEL);
    ROM_GPIOPinConfigure(U7TX_MUX_SEL);

	//
	// Since GPIO A0 and A1 are used for the UART function, they must be
	// configured for use as a peripheral function (instead of GPIO).
	//
	ROM_GPIOPinTypeUART(UART_PORT_BASE, U7RX_PIN | U7TX_PIN);
	
}



void UARTHwInit(void)
{
	UARTIOConfig();

	//
	// Initialize the UART for console I/O.
	//
	//UARTStdioConfig(2, 115200, get_sys_clk()); // In order to print out the debug message to Putty
	UARTStdioConfig(2, 921600, get_sys_clk()); // In order to print out the debug message to Putty
}

