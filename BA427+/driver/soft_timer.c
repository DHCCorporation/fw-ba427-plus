//*****************************************************************************
//
// Source File: soft_timer.c
// Description: software timer
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 01/12/18     Jerry.W        Created file.
// 02/14/18     Henry.Y        bug fix: make count of a unregister timer cause system crash.
// 09/11/18     Henry.Y        Use system driver interface to replace global variable.
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/driver/wifi_driver.h#2 $
// $DateTime: 2017/12/18 16:17:54 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#include "soft_timer.h"
#include "delaydrv.h"


typedef struct Timer_node_t{
	Timer_t* Timer;
	struct Timer_node_t* next;
}Timer_node_t;

Timer_node_t * Timer_list = NULL;

bool Timer_configed = false;

void HW_timer_config(){
	ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER1);
	ROM_TimerConfigure(TIMER1_BASE, TIMER_CFG_PERIODIC);

	ROM_TimerLoadSet(TIMER1_BASE, TIMER_A, get_sys_clk()/10 -1);	// Timer interrupt in 0.1 sec

	ROM_IntEnable(INT_TIMER1A);
	ROM_TimerIntEnable(TIMER1_BASE, TIMER_TIMA_TIMEOUT);
}

void HW_timer_enable(){
	if(!Timer_configed) HW_timer_config();
	ROM_TimerEnable(TIMER1_BASE, TIMER_A);
}

void HW_timer_disable(){
	ROM_TimerDisable(TIMER1_BASE, TIMER_A);
}


void add_to_timer_list(Timer_t* Timer){
	Timer_node_t* TN_temp;
	Timer_node_t* TN = malloc(sizeof(Timer_node_t));
	TN->Timer = Timer;
	TN->next = NULL;
	if(Timer_list){
		TN_temp = Timer_list;
		while(TN_temp->next){
			TN_temp = TN_temp->next;
		}
		TN_temp->next = TN;
	}
	else{
		Timer_list = TN;
		HW_timer_enable();
	}
}

void remove_from_timer_list(Timer_t* Timer){
	Timer_node_t* TN_temp = Timer_list;
	Timer_node_t* pre_TN;
	while(TN_temp){
		if(TN_temp->Timer == Timer) break;
		pre_TN = TN_temp;
		TN_temp = TN_temp->next;
	}
	if(TN_temp){
		if(TN_temp == Timer_list){
			Timer_list = TN_temp->next;
			
			if(!Timer_list) HW_timer_disable();
		}
		else{
			pre_TN->next = TN_temp->next;
		}
		free(TN_temp->Timer);
		free(TN_temp);
	}

}

Timer_t* register_timer(uint8_t period, void (*time_cb)(void)){
	Timer_t* Timer = malloc(sizeof(Timer_t));
	Timer->period = period;
	Timer->cb = time_cb;
	Timer->started = false;
	add_to_timer_list(Timer);

	return Timer;
}

void unregister_timer(Timer_t* Timer){
	Timer->started = false;
	remove_from_timer_list(Timer);
}

void start_timer(Timer_t* Timer){
	Timer->started = true;
	Timer->counter = (Timer->period - 1);
}

void Timer1AIntHandler(void)
{
	Timer_node_t* TN_temp, *TN;
	// Clear the timer interrupt
	ROM_TimerIntClear(TIMER1_BASE, TIMER_TIMA_TIMEOUT);
	TN = Timer_list;
	while(TN){
		if(TN->Timer->started){
			if(TN->Timer->counter){
				TN->Timer->counter--;
				TN = TN->next;
			}
			else{
				TN->Timer->counter = (TN->Timer->period - 1);
				TN_temp = TN;
				TN = TN->next;
				if(TN_temp->Timer->cb) TN_temp->Timer->cb();
			}
		}
		else{
			TN = TN->next;
		}
	}
}
