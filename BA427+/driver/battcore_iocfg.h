//*****************************************************************************
//
// Source File: battcore_iocfg.h
// Description: Battery Tester Core of related GPIO Pins Definition.
//				
//
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 10/01/15     William.H      Created file.
// 10/05/15     William.H      Check in BT2020 project which branched from LCDFontMaker(Pre-BT2020) by modifed related GPIOs.
// 10/08/15     William.H      Add for ADC Voltage Channel Peripherals and I/O Pin Definition.
// 10/08/15     Henry.Y        Implement new BT2020 ADC and GPIO. Remove 8V channel.
// 03/09/16     Vincent.T      1. Add Vload pin defination
// 06/14/16     Vincent.T      1. New Internal Battery Power Switch I/O Pin Definition
// 12/02/16     Vincent.T      1. New Load Protect circuit I/O Pin Definition.
// 12/30/16     Vincent.T      1. Add Pin Definition of Voltage between V-Load.(Seperate Pin)
// 03/01/17     Henry.Y        Add internal voltage gpio switch IntBat_SW_OFF/IntBat_SW_ON.
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/driver/battcore_iocfg.h#5 $
// $DateTime: 2017/09/22 10:27:40 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#ifndef __BATTCORE_IOCFG_H__
#define __BATTCORE_IOCFG_H__

#include "inc/hw_memmap.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"

//*****************************************************************************
//
//  CCA Load On 1 Peripherals and I/O Pin Definition
//
//  Below defines the following CCA Load On peripherals and I/O signals.  You must
//  review these and change as needed for your own board:
//   - CCA peripheral
//   - CCA base address of the peripheral (GPIO Port H peripheral)
//   - CCA load 1 PIN - PH3
//   - CCA load 1 protect PIN - PH2
//
//*****************************************************************************
#define CCA_load_1_PERIPHERAL		SYSCTL_PERIPH_GPIOH
#define CCA_load_1_BASE				GPIO_PORTH_BASE
#define CCA_load_1_pin				GPIO_PIN_3	//PH3
#define CCA_load_1_protect			GPIO_PIN_2	//PH2

#define CCA_load_1_ON				ROM_GPIOPinWrite(CCA_load_1_BASE, CCA_load_1_pin, CCA_load_1_pin)
#define CCA_load_1_OFF				ROM_GPIOPinWrite(CCA_load_1_BASE, CCA_load_1_pin, 0x00)
#define CCA_load_1_protect_ON		ROM_GPIOPinWrite(CCA_load_1_BASE, CCA_load_1_protect, CCA_load_1_protect)
#define CCA_load_1_protect_OFF		ROM_GPIOPinWrite(CCA_load_1_BASE, CCA_load_1_protect, 0x00)

//*****************************************************************************
//
//  CCA Load On 2 Peripherals and I/O Pin Definition
//
//  Below defines the following CCA Load On peripherals and I/O signals.  You must
//  review these and change as needed for your own board:
//   - CCA peripheral
//   - CCA base address of the peripheral (GPIO Port N peripheral)
//   - CCA load 2 PIN - PN5
//   - CCA load 2 protect PIN - PN4
//
//*****************************************************************************
#define CCA_load_2_PERIPHERAL		SYSCTL_PERIPH_GPION
#define CCA_load_2_BASE				GPIO_PORTN_BASE
#define CCA_load_2_pin				GPIO_PIN_5	//PN5
#define CCA_load_2_protect			GPIO_PIN_4	//PN4

#define CCA_load_2_ON				ROM_GPIOPinWrite(CCA_load_2_BASE, CCA_load_2_pin, CCA_load_2_pin)
#define CCA_load_2_OFF				ROM_GPIOPinWrite(CCA_load_2_BASE, CCA_load_2_pin, 0x00)
#define CCA_load_2_protect_ON		ROM_GPIOPinWrite(CCA_load_2_BASE, CCA_load_2_protect, CCA_load_2_protect)
#define CCA_load_2_protect_OFF		ROM_GPIOPinWrite(CCA_load_2_BASE, CCA_load_2_protect, 0x00)

//*****************************************************************************
//
//  ADC Voltage Channel Peripherals and I/O Pin Definition
//
//  Below defines the following ADC Voltage Channel peripherals and I/O signals.
//  You must review these and change as needed for your own board:
//   - ADC Voltage Channel peripheral
//   - ADC Voltage base address of the peripheral (GPIO Port D peripheral)
//   - 24V Channel PIN - PD0
//   - 12V Channel PIN - PD1
//   - 6V Channel PIN - PD2
//   - 8V Channel PIN - PD3
//
//*****************************************************************************
#define ADC_VOLTAGE_PERIPHERAL	SYSCTL_PERIPH_GPIOD
#define ADC_VOLTAGE_BASE		GPIO_PORTD_BASE
#define ADC_24V_CH				GPIO_PIN_0	//AIN15 PD0 
#define ADC_12V_CH				GPIO_PIN_1	//AIN14 PD1
#define ADC_6V_CH				GPIO_PIN_2	//AIN13 PD2
//#define ADC_8V_CH				GPIO_PIN_3	//AIN12 PD3

//*****************************************************************************
//
//  Internal Battery Peripherals and I/O Pin Definition
//
//	Below defines the following Internal Battery peripherals and I/O signals.
//	You must review these and change as needed for your own board:
//	 - Internal Battery ADC Channel peripheral
//	 - Internal Battery ADC Channel	base address of the peripheral (GPIO Port D peripheral)
//	 - Internal Battery Pin - PD6
//
//*****************************************************************************
#define ADC_VOLTAGE_BAT_PERIPHERAL	SYSCTL_PERIPH_GPIOD	//PD6 AIN5
#define ADC_VOLTAGE_BAT_BASE		GPIO_PORTD_BASE
#define ADC_BAT_CH					GPIO_PIN_6

//*****************************************************************************
//
// Delta voltage between load
//
// Peripherals: SYSCTL_PERIPH_GPIOE
// BASE       : GPIO_PORTE_BASE
// PIN        : GPIO_PIN_2
// AD Channel : ADC_CTL_CH1
//
//*****************************************************************************
#define ADC_VLOAD_PERIPHERAL		SYSCTL_PERIPH_GPIOE //
#define ADC_VLOAD_BASE				GPIO_PORTE_BASE		//
#define ADC_VLOAD_PLUS_PIN			GPIO_PIN_2			//
#define ADC_VLOAD_MINS_PIN			GPIO_PIN_0			//
#define ADC_VLOAD_PLUS_CH			ADC_CTL_CH1			//
#define ADC_VLOAD_MINS_CH			ADC_CTL_CH3			//

//*****************************************************************************
//
// - Internal Battery Power Switch I/O Pin Definition
//
//*****************************************************************************
#define IntBat_SW_PERIPHERAL		SYSCTL_PERIPH_GPIOH //
#define IntBat_SW_BASE				GPIO_PORTH_BASE		//
#define IntBat_SW_PIN				GPIO_PIN_1			//

#define IntBat_SW_ON				ROM_GPIOPinWrite(IntBat_SW_BASE, IntBat_SW_PIN, IntBat_SW_PIN)
#define IntBat_SW_OFF				ROM_GPIOPinWrite(IntBat_SW_BASE, IntBat_SW_PIN, 0x00)
//*****************************************************************************
//
// - Test Load and Test Load Protect Definition
//
//*****************************************************************************
#define TestLoad3_PERIPHERAL			SYSCTL_PERIPH_GPIOQ //
#define TestLoad3_BASE					GPIO_PORTQ_BASE		//
#define TestLoad3_PIN					GPIO_PIN_1			//
#define TestLoad3Protect_PIN			GPIO_PIN_2			//

#define TestLoad3_ON					ROM_GPIOPinWrite(TestLoad3_BASE, TestLoad3_PIN, TestLoad3_PIN)
#define TestLoad3_OFF					ROM_GPIOPinWrite(TestLoad3_BASE, TestLoad3_PIN, 0x00)
#define TestLoad3Protect_ON				ROM_GPIOPinWrite(TestLoad3_BASE, TestLoad3Protect_PIN, TestLoad3Protect_PIN)
#define TestLoad3Protect_OFF			ROM_GPIOPinWrite(TestLoad3_BASE, TestLoad3Protect_PIN, 0x00)

extern void BattCoreHwInit(void);

#endif

