//*****************************************************************************
//
// Source File: EEPROMdrv.h
// Description: internal EERPOM driver function.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 02/26/18     Jerry.W	       Created file.
// =============================================================================
//
//
// $Header: //depot/Tester/pb/service.h#0 $
// $DateTime: 2017/09/26 17:08:19 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#ifndef DRIVER_INEEPROMDRV_H_
#define DRIVER_INEEPROMDRV_H_

#include "stdint.h"
#include <stdbool.h>
#define byte1_mask	0x000000FF
#define byte2_mask	0x0000FF00
#define byte3_mask	0x00FF0000
#define byte4_mask	0xFF000000





/******************* EEPROM read/write functions ************************/
//																		//
//	return 1 : power error												//
//	return 2 : memory error												//
//																		//
/************************************************************************/
uint8_t EEPROM_word_write_safe(uint32_t data, uint32_t add, uint32_t mask);

uint8_t EEPROM_word_read_safe(uint32_t *data, uint32_t add);

uint8_t EEPROM_bytes_write_safe(uint8_t *data, uint32_t add, uint32_t length);

uint8_t EEPROM_bytes_read_safe(uint8_t *data, uint32_t add, uint32_t length);



#endif /* DRIVER_INEEPROMDRV_H_ */
