//*****************************************************************************
//
// Source File: buttons.c
// Description: Push buttons driver.
//
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 07/31/15     William.H      Created file.
// 10/05/15     William.H      Check in BT2020 project which branched from LCDFontMaker(Pre-BT2020) by modifed related GPIOs.
// 12/07/15     Vincent.T      Modified static uint8_t g_ui8ButtonStates -> uint8_t g_ui8ButtonStates
// 03/03/16     Vincent.T      1. func. body for Left/Select/Right event for EngMod.
// 06/14/16     Vincent.T      1. Modify LeftBtn();(Add Mode 3)
// 11/17/16     Vincent.T      1. Add New Func. UpBtn() and DownBtn();
// 11/06/17     Henry.Y	       1. Modify button short pressed sensitivity LeftBtn/SelectBtn/RightBtn()
// 11/19/18     Henry.Y	       Up/Down button add combo mode.
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/driver/buttons.c#5 $
// $DateTime: 2017/09/22 10:27:40 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#include <stdbool.h>
#include <stdint.h>
#include <stdbool.h>
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom_map.h"
#include "driverlib/sysctl.h"
#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "buttons.h"
#include "delaydrv.h"

//*****************************************************************************
//
//! \addtogroup buttons_api
//! @{
//
//*****************************************************************************

//*****************************************************************************
//
// Holds the current, debounced state of each button.  A 0 in a bit indicates
// that that button is currently pressed, otherwise it is released.
// We assume that we start with all the buttons released (though if one is
// pressed when the application starts, this will be detected).
//
//*****************************************************************************
uint8_t g_ui8ButtonStates = ALL_BUTTONS;

//*****************************************************************************
//
//! Polls the current state of the buttons and determines which have changed.
//!
//! \param pui8Delta points to a character that will be written to indicate
//! which button states changed since the last time this function was called.
//! This value is derived from the debounced state of the buttons.
//! \param pui8RawState points to a location where the raw button state will
//! be stored.
//!
//! This function should be called periodically by the application to poll the
//! pushbuttons.  It determines both the current debounced state of the buttons
//! and also which buttons have changed state since the last time the function
//! was called.
//!
//! In order for button debouncing to work properly, this function should be
//! caled at a regular interval, even if the state of the buttons is not needed
//! that often.
//!
//! If button debouncing is not required, the the caller can pass a pointer
//! for the \e pui8RawState parameter in order to get the raw state of the
//! buttons.  The value returned in \e pui8RawState will be a bit mask where
//! a 1 indicates the buttons is pressed.
//!
//! \return Returns the current debounced state of the buttons where a 1 in the
//! button ID's position indicates that the button is pressed and a 0
//! indicates that it is released.
//
//*****************************************************************************
uint8_t
ButtonsPoll(uint8_t *pui8Delta, uint8_t *pui8RawState)
{
	uint32_t ui32Delta;
	uint32_t ui32Data;
	static uint8_t ui8SwitchClockA = 0;
	static uint8_t ui8SwitchClockB = 0;
	//
	// Read the raw state of the push buttons.  Save the raw state
	// (inverting the bit sense) if the caller supplied storage for the
	// raw value.
	//
	ui32Data = (MAP_GPIOPinRead(BUTTONS_GPIO_BASE, ALL_BUTTONS));
	if(pui8RawState)
	{
		*pui8RawState = (uint8_t)~ui32Data;
	}

	//
	// Determine the switches that are at a different state than the debounced
	// state.
	//
	ui32Delta = ui32Data ^ g_ui8ButtonStates;

	//
	// Increment the clocks by one.
	//
	ui8SwitchClockA ^= ui8SwitchClockB;
	ui8SwitchClockB = ~ui8SwitchClockB;

	//
	// Reset the clocks corresponding to switches that have not changed state.
	//
	ui8SwitchClockA &= ui32Delta;
	ui8SwitchClockB &= ui32Delta;

	//
	// Get the new debounced switch state.
	//
	g_ui8ButtonStates &= ui8SwitchClockA | ui8SwitchClockB;
	g_ui8ButtonStates |= (~(ui8SwitchClockA | ui8SwitchClockB)) & ui32Data;

	//
	// Determine the switches that just changed debounced state.
	//
	ui32Delta ^= (ui8SwitchClockA | ui8SwitchClockB);

	//
	// Store the bit mask for the buttons that have changed for return to
	// caller.
	//
	if(pui8Delta)
	{
		*pui8Delta = (uint8_t)ui32Delta;
	}

	//
	// Return the debounced buttons states to the caller.  Invert the bit
	// sense so that a '1' indicates the button is pressed, which is a
	// sensible way to interpret the return value.
	//
	return(~g_ui8ButtonStates);
}

//*****************************************************************************
//
//! Initializes the GPIO pins used by the board pushbuttons.
//!
//! This function must be called during application initialization to
//! configure the GPIO pins to which the pushbuttons are attached.  It enables
//! the port used by the buttons and configures each button GPIO as an input
//! with a weak pull-up.
//!
//! \return None.
//
//*****************************************************************************
void
ButtonsInit(void)
{
	//
	// Enable the GPIO port to which the pushbuttons are connected.
	//
	MAP_SysCtlPeripheralEnable(BUTTONS_GPIO_PERIPH);

	//
	// Set each of the button GPIO pins as an input with a pull-up.
	//
	MAP_GPIODirModeSet(BUTTONS_GPIO_BASE, ALL_BUTTONS, GPIO_DIR_MODE_IN);
	MAP_GPIOPadConfigSet(BUTTONS_GPIO_BASE, ALL_BUTTONS,
			GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPU);
	//MAP_GPIOPadConfigSet(BUTTONS_GPIO_BASE, ALL_BUTTONS,
	//                     GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_OD);

	//
	// Initialize the debounced button state with the current state read from
	// the GPIO bank.
	//
	g_ui8ButtonStates = MAP_GPIOPinRead(BUTTONS_GPIO_BASE, ALL_BUTTONS);
}

//*****************************************************************************
//
// Protype for Left/Select/Right button event.
// Protype for Up/Down button event.
//
//*****************************************************************************
bool LeftBtn(uint8_t ui8Mode)
{
	uint32_t ui32debounce = 0;

	if(ui8Mode == 0)
	{
		//
		// return Btn event until released.
		//
		if(!MAP_GPIOPinRead(BUTTONS_GPIO_BASE, LEFT_BUTTON))
		{
			while(!MAP_GPIOPinRead(BUTTONS_GPIO_BASE, LEFT_BUTTON))
			{
				ui32debounce++;
				if(ui32debounce >= 1500)
				{
					ui32debounce = 1500;
				}
			}
		}
	}
	else if(ui8Mode == 1)
	{
		//
		// Combo Mode
		//
		if(!MAP_GPIOPinRead(BUTTONS_GPIO_BASE, LEFT_BUTTON))
		{
			while(!MAP_GPIOPinRead(BUTTONS_GPIO_BASE, LEFT_BUTTON))
			{
				ui32debounce++;
				if(ui32debounce >= 300000)
				{
					break;
				}
			}
		}
	}
	else if(ui8Mode == 2)
	{
		//
		// Combo Mode
		//
		if(!MAP_GPIOPinRead(BUTTONS_GPIO_BASE, LEFT_BUTTON))
		{
			while(!MAP_GPIOPinRead(BUTTONS_GPIO_BASE, LEFT_BUTTON))
			{
				ui32debounce++;
				if(ui32debounce >= 5000)
				{
					break;
				}
			}
		}
	}
	else if(ui8Mode == 3)
	{
		//
		// Fast Combo Mode
		//
		if(!MAP_GPIOPinRead(BUTTONS_GPIO_BASE, LEFT_BUTTON))
		{
			while(!MAP_GPIOPinRead(BUTTONS_GPIO_BASE, LEFT_BUTTON))
			{
				ui32debounce++;
				if(ui32debounce >= 100)   // 300 -> 100
				{
					return true;
				}
				delay_ms(1);
			}
			return false;
		}
	}

    else if(ui8Mode == 4)
    {
        //
        // Fast Combo Mode
        //
        if(!MAP_GPIOPinRead(BUTTONS_GPIO_BASE, LEFT_BUTTON))
        {
            while(!MAP_GPIOPinRead(BUTTONS_GPIO_BASE, LEFT_BUTTON))
            {
                ui32debounce++;
                if(ui32debounce >= 3000000)
                {
                    return true;
                }
            }
        }

    }

	if(ui8Mode == 4)
	{
        if(ui32debounce >= 3000000)
        {
            return true;
        }
        else
        {
            return false;
        }
	}
	else
	{
	    if( ui32debounce >= 150)
	    {
	        return true;
	    }
	    else
	    {
		    return false;
	    }
	}
}

bool SelectBtn(uint8_t ui8Mode)
{
	uint32_t ui32debounce = 0;

	if(ui8Mode == 0)
	{
		//
		// return Btn event until released.
		//
		if(!MAP_GPIOPinRead(BUTTONS_GPIO_BASE, SELECT_BUTTON))
		{
			while(!MAP_GPIOPinRead(BUTTONS_GPIO_BASE, SELECT_BUTTON))
			{
				ui32debounce++;
				if(ui32debounce >= 1500)
				{
					ui32debounce = 1500;
				}
			}
		}
	}
	else if(ui8Mode == 1)
	{
		//
		// Combo Mode
		//
		if(!MAP_GPIOPinRead(BUTTONS_GPIO_BASE, SELECT_BUTTON))
		{
			while(!MAP_GPIOPinRead(BUTTONS_GPIO_BASE, SELECT_BUTTON))
			{
				ui32debounce++;
				if(ui32debounce >= 300000)
				{
					break;
				}
			}
		}
	}
	else if(ui8Mode == 2)
	{
		//
		// Combo Mode
		//
		if(!MAP_GPIOPinRead(BUTTONS_GPIO_BASE, SELECT_BUTTON))
		{
			while(!MAP_GPIOPinRead(BUTTONS_GPIO_BASE, SELECT_BUTTON))
			{
				ui32debounce++;
				if(ui32debounce >= 5000)
				{
					break;
				}
			}
		}
	}
    else if(ui8Mode == 3)
    {
        //
        // Fast Combo Mode
        //
        if(!MAP_GPIOPinRead(BUTTONS_GPIO_BASE, SELECT_BUTTON))
        {
            while(!MAP_GPIOPinRead(BUTTONS_GPIO_BASE, SELECT_BUTTON))
            {
                ui32debounce++;
                if(ui32debounce >= 100)   // 300 -> 100
                {
                    return true;
                }
                delay_ms(1);
            }
            return false;
        }
    }

	if(ui32debounce >= 1500)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool RightBtn(uint8_t ui8Mode)
{
	uint32_t ui32debounce = 0;

	if(ui8Mode == 0)
	{
		//
		// return Btn event until released.
		//
		if(!MAP_GPIOPinRead(BUTTONS_GPIO_BASE, RIGHT_BUTTON))
		{
			while(!MAP_GPIOPinRead(BUTTONS_GPIO_BASE, RIGHT_BUTTON))
			{
				ui32debounce++;
				if(ui32debounce >= 1500)
				{
					ui32debounce = 1500;
				}
			}
		}
	}
	else if(ui8Mode == 1)
	{
		//
		// Combo Mode
		//
		if(!MAP_GPIOPinRead(BUTTONS_GPIO_BASE, RIGHT_BUTTON))
		{
			while(!MAP_GPIOPinRead(BUTTONS_GPIO_BASE, RIGHT_BUTTON))
			{
				ui32debounce++;
				if(ui32debounce >= 300000)
				{
					break;
				}
			}
		}
	}
	else if(ui8Mode == 2)
	{
		//
		// Combo Mode
		//
		if(!MAP_GPIOPinRead(BUTTONS_GPIO_BASE, RIGHT_BUTTON))
		{
			while(!MAP_GPIOPinRead(BUTTONS_GPIO_BASE, RIGHT_BUTTON))
			{
				ui32debounce++;
				if(ui32debounce >= 5000)
				{
					break;
				}
			}
		}
	}
	else if(ui8Mode == 3)
	{
	    //
	    // Combo Mode
	    //
	    if(!MAP_GPIOPinRead(BUTTONS_GPIO_BASE, RIGHT_BUTTON))
	    {
	        while(!MAP_GPIOPinRead(BUTTONS_GPIO_BASE, RIGHT_BUTTON))
	        {
	            ui32debounce++;
	            if(ui32debounce >= 100)
	            {
	                return true;
	            }
	            delay_ms(1);
	        }
	        return false;
	    }
	}

	if(ui32debounce >= 150)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool UpBtn(uint8_t ui8Mode)
{
	uint32_t ui32debounce = 0;

	if(ui8Mode == 0)
	{
		//
		// return Btn event until released.
		//
		if(!MAP_GPIOPinRead(BUTTONS_GPIO_BASE, UP_BUTTON))
		{
			while(!MAP_GPIOPinRead(BUTTONS_GPIO_BASE, UP_BUTTON))
			{
				ui32debounce++;
				if(ui32debounce >= 150)
				{
					ui32debounce = 150;
				}
			}
		}
	}
	else if(ui8Mode == 1)
	{
		//
		// Combo Mode
		//
		if(!MAP_GPIOPinRead(BUTTONS_GPIO_BASE, UP_BUTTON))
		{
			while(!MAP_GPIOPinRead(BUTTONS_GPIO_BASE, UP_BUTTON))
			{
				ui32debounce++;
				if(ui32debounce >= 300000)
				{
					break;
				}
			}
		}
	}
	else if(ui8Mode == 2)
	{
		//
		// Combo Mode
		//
		if(!MAP_GPIOPinRead(BUTTONS_GPIO_BASE, UP_BUTTON))
		{
			while(!MAP_GPIOPinRead(BUTTONS_GPIO_BASE, UP_BUTTON))
			{
				ui32debounce++;
				if(ui32debounce >= 5000)
				{
					break;
				}
			}
		}
	}
	else if(ui8Mode == 3){
		while(!MAP_GPIOPinRead(BUTTONS_GPIO_BASE, UP_BUTTON)){
			if(ui32debounce > 300){
				return true;
			}
			ui32debounce++;
			delay_ms(1);			
		}		
		return false;
	}
	else if(ui8Mode == 4){
		if(!MAP_GPIOPinRead(BUTTONS_GPIO_BASE, UP_BUTTON)){
			delay_ms(50);
			return true;
		}
		else{
			return false;
		}
	}

	if(ui32debounce >= 150)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool DownBtn(uint8_t ui8Mode)
{
	uint32_t ui32debounce = 0;

	if(ui8Mode == 0)
	{
		//
		// return Btn event until released.
		//
		if(!MAP_GPIOPinRead(BUTTONS_GPIO_BASE, DOWN_BUTTON))
		{
			while(!MAP_GPIOPinRead(BUTTONS_GPIO_BASE, DOWN_BUTTON))
			{
				ui32debounce++;
				if(ui32debounce >= 150)
				{
					ui32debounce = 150;
				}
			}
		}
	}
	else if(ui8Mode == 1)
	{
		//
		// Combo Mode
		//
		if(!MAP_GPIOPinRead(BUTTONS_GPIO_BASE, DOWN_BUTTON))
		{
			while(!MAP_GPIOPinRead(BUTTONS_GPIO_BASE, DOWN_BUTTON))
			{
				ui32debounce++;
				if(ui32debounce >= 300000)
				{
					break;
				}
			}
		}
	}
	else if(ui8Mode == 2)
	{
		//
		// Combo Mode
		//
		if(!MAP_GPIOPinRead(BUTTONS_GPIO_BASE, DOWN_BUTTON))
		{
			while(!MAP_GPIOPinRead(BUTTONS_GPIO_BASE, DOWN_BUTTON))
			{
				ui32debounce++;
				if(ui32debounce >= 5000)
				{
					break;
				}
			}
		}
	}
	else if(ui8Mode == 3){
		while(!MAP_GPIOPinRead(BUTTONS_GPIO_BASE, DOWN_BUTTON)){
			if(ui32debounce > 300){
				return true;
			}
			ui32debounce++;
			delay_ms(1);			
		}		
		return false;
	}
	else if(ui8Mode == 4){
		if(!MAP_GPIOPinRead(BUTTONS_GPIO_BASE, DOWN_BUTTON)){
			delay_ms(50);
			return true;
		}
		else{
			return false;
		}
	}

	if(ui32debounce >= 150)
	{
		return true;
	}
	else
	{
		return false;
	}
}

