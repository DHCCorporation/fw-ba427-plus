//*****************************************************************************
//
// Source File: st7565_lcm.h
// Description: Prototype for functions to configure ST7565 LCM Controller.
//
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 06/25/15     William.H      Created file.
// 10/05/15     William.H      Check in BT2020 project which branched from LCDFontMaker(Pre-BT2020) by modifed related GPIOs.
// 10/27/15     William.H      1. Add MoveInBufIdx parameter into ST7565SlideEffectBaseOnEdge() function.
//                             2. Add new API of st7565SlideUpBootupAnimWithNewImage() for bootup animation with DHC logo.
// 01/06/16     William.H      Create st7565drawLangSelMenu() function to process the drawing of language menu list.
// 01/21/16     William.H      Add ST7565DrawImageSinceXY_fromLeftEdge() func that provide the slide effect of screen based on left edge of LCD panel.
//                             Bitmap could be slided start from any Y-axis(irregular of one page) then move into panel from left to right.
//                             This func is used at bootup animation.
// 01/21/16     Vincent.T      New function st7565DrawUprHalfScrnCrankVAnimation();
// 02/03/16     Vincent.T      New function void ST7565Ent2NextLvlMenu7page();
// 03/02/16     Vincent.T      1. Add backlight color setting
//                             2. New function - Backlight color setting
// 03/30/16     Vincent.T      1. Add declaraction of st7565draw8x8Char();
// 04/07/16     Vicnent.T      1. Move st7565SendByte(); form .c to .h
// 11/17/16     Vincent.T      1. New Func. SetDisplayBuf() and ST7565DrawStringx16();
// 09/25/17		Jerry.W		   fix the error of ST7565Draw8x8Char
// 05/18/18     Jerry.W        remove double declared structure.
// 09/11/18     Henry.Y        st7565DrawUprHalfScrn() add parameter to replace global variable.
//                             ST7565HwInit applied new hierarchical architecture.
// 10/23/18     Jerry.w        add UTF8 string printf function.
// 03/20/19     Henry.Y        Add LCM setting to match HW bias1/9, \5x booster setup.
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/driver/st7565_lcm.h#7 $
// $DateTime: 2017/09/26 17:42:28 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#ifndef __ST7565_LCM_H__
#define __ST7565_LCM_H__
#define LCM_BOOSTER_CMD 1

#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/rom.h"
#include "inc/hw_memmap.h"
#include "uartstdio.h"
#include "smallfonts.h"
#include "../service/flashsvc.h"
#include "grlib/grlib.h"

// Pin Definitions
#define ST7565_CS_PORT                    SYSCTL_PERIPH_GPIOB     // Chip Select (1) <-> PB2
#define ST7565_CS_BASE                    GPIO_PORTB_BASE
#define ST7565_CS_PIN                     GPIO_PIN_2                

#define ST7565_RST_PORT                   SYSCTL_PERIPH_GPIOL     // Reset (2) <-> PL5
#define ST7565_RST_BASE                   GPIO_PORTL_BASE
#define ST7565_RST_PIN                    GPIO_PIN_5

#define ST7565_A0_PORT                    SYSCTL_PERIPH_GPIOL     // Data/Instruction selection (3) <-> PL4
#define ST7565_A0_BASE                    GPIO_PORTL_BASE
#define ST7565_A0_PIN                     GPIO_PIN_4

#define ST7565_WR_PORT                    SYSCTL_PERIPH_GPIOL     // Write (4) <-> PL3
#define ST7565_WR_BASE                    GPIO_PORTL_BASE
#define ST7565_WR_PIN                     GPIO_PIN_3

#if 0
#define ST7565_RD_PORT                    SYSCTL_PERIPH_GPIOE     // Read (5) <-> PE2
#define ST7565_RD_BASE                    GPIO_PORTE_BASE
#define ST7565_RD_PIN                     GPIO_PIN_2
#endif

#define ST7565_RD_PORT                    SYSCTL_PERIPH_GPIOL     // Read (5) <-> PL2
#define ST7565_RD_BASE                    GPIO_PORTL_BASE
#define ST7565_RD_PIN                     GPIO_PIN_2

#define ST7565_DATA_PORT                  SYSCTL_PERIPH_GPIOM     // Data Bus (Port M)
#define ST7565_DATA_BASE                  GPIO_PORTM_BASE
#define ST7565_D0_PIN                     GPIO_PIN_0              // D0 (6) <->PM0
#define ST7565_D1_PIN                     GPIO_PIN_1              // D1 (7) <-> PM1
#define ST7565_D2_PIN                     GPIO_PIN_2              // D2 (8) <-> PM2
#define ST7565_D3_PIN                     GPIO_PIN_3              // D3 (9) <-> PM3
#define ST7565_D4_PIN                     GPIO_PIN_4              // D4 (10) <-> PM4
#define ST7565_D5_PIN                     GPIO_PIN_5              // D5 (11) <-> PM5
#define ST7565_D6_PIN                     GPIO_PIN_6              // D6 (12) <-> PM6
#define ST7565_D7_PIN                     GPIO_PIN_7              // D7 (13) <-> PM7

// Commands
#define ST7565_CMD_DISPLAY_OFF            0xAE
#define ST7565_CMD_DISPLAY_ON             0xAF
#define ST7565_CMD_SET_DISP_START_LINE    0x40
#define ST7565_CMD_SET_PAGE               0xB0
#define ST7565_CMD_SET_COLUMN_UPPER       0x10
#define ST7565_CMD_SET_COLUMN_LOWER       0x00
#define ST7565_CMD_SET_ADC_NORMAL         0xA0
#define ST7565_CMD_SET_ADC_REVERSE        0xA1
#define ST7565_CMD_SET_DISP_NORMAL        0xA6
#define ST7565_CMD_SET_DISP_REVERSE       0xA7
#define ST7565_CMD_SET_ALLPTS_NORMAL      0xA4
#define ST7565_CMD_SET_ALLPTS_ON          0xA5
#define ST7565_CMD_SET_BIAS_9             0xA2 
#define ST7565_CMD_SET_BIAS_7             0xA3
#define ST7565_CMD_RMW                    0xE0
#define ST7565_CMD_RMW_CLEAR              0xEE
#define ST7565_CMD_INTERNAL_RESET         0xE2
#define ST7565_CMD_SET_COM_NORMAL         0xC0
#define ST7565_CMD_SET_COM_REVERSE        0xC8
#define ST7565_CMD_SET_POWER_CONTROL      0x28
#define ST7565_CMD_SET_RESISTOR_RATIO     0x20
#define ST7565_CMD_SET_VOLUME_FIRST       0x81
#define ST7565_CMD_SET_VOLUME_SECOND      0
#define ST7565_CMD_SET_STATIC_OFF         0xAC
#define ST7565_CMD_SET_STATIC_ON          0xAD
#define ST7565_CMD_SET_STATIC_REG         0x0
#define ST7565_CMD_SET_BOOSTER_FIRST      0xF8
#define ST7565_CMD_SET_BOOSTER_234        0
#define ST7565_CMD_SET_BOOSTER_5          1
#define ST7565_CMD_SET_BOOSTER_6          3
#define ST7565_CMD_NOP                    0xE3
#define ST7565_CMD_TEST                   0xF0

//
//
// Backlight color
//
//
#define LCD_BACKLIGHT_PORT					SYSCTL_PERIPH_GPIOK
#define LCD_BACKLIGHT_BASE					GPIO_PORTK_BASE
#define LCD_BACKLIGHT_Y						GPIO_PIN_7
#define LCD_BACKLIGHT_B						GPIO_PIN_6
#define LCD_BACKLIGHT_G						GPIO_PIN_5
#define LCD_BACKLIGHT_R						GPIO_PIN_4

//
// LCD contrast
//
#define LCDcontrast_max 33
#define LCDcontrast_min 13
#define bias9bter5x_LCDcontrast_max 45
#define bias9bter5x_LCDcontrast_min 34

typedef struct
{
    uint8_t u8Width;				/* Character width for storage         */
    uint8_t u8Height;				/* Character height for storage        */
    const uint8_t *au8FontTable;	/* Font table start address in memory  */
}
LANGMENUFONT_DEF;

typedef struct
{
	const char *string;	
	uint8_t x;
	uint8_t y;
}XY_DISPLAY;




/* RevrMode */
typedef enum {
	HORIZ = 0,	/* 0: Horizontal Reverse Mode */
	VERTICAL	/* 1: Vertical Reverse Mode */
} REVRMODE;

/* Slide Mode by Left or Right Edge Side. */
typedef enum {
	RIGHTEDGE = 0,	/* 0: Shift Based On Right Edge */
	LEFTEDGE		/* 1: Shift Based On Left Edge */		
} sEDGESIDE;

/* Moving-In buffer index of transition slide. */
typedef enum {
	BUFFER0 = 0,		/* 0: Moving-In buffer index 0 of transition slide. */
	BUFFER1				/* 1: Moving-In buffer index 1 of transition slide. */
} MOVEINBUFFER;



// Initialisation/Config Prototypes
extern void ST7565IOConfig( void );
extern void ST7565HwInit( uint8_t LCD_ratio );
extern void ST7565SetBrightness( uint8_t val );
void ST7565_V5_internal_resistor_ratio(uint8_t val);


// Drawing Prototypes
extern void ST7565V128x64x1PixelDraw(void *pvDisplayData, int32_t i32X, int32_t i32Y, uint32_t ui32Value);
extern void ST7565V128x64x1LineDrawH(void *pvDisplayData, int32_t i32X1, int32_t i32X2, int32_t i32Y, uint32_t ui32Value);
extern void ST7565V128x64x1LineDrawV(void *pvDisplayData, int32_t i32X, int32_t i32Y1, int32_t i32Y2, uint32_t ui32Value);
extern void ST7565V128x64x1LinePrintV(void *pvDisplayData, int32_t i32X, int32_t i32Y1, uint32_t len, uint8_t *Value, bool transparent);
extern void ST7565V128x64x1RectFill(void *pvDisplayData, const tRectangle *pRect, uint32_t ui32Value);
extern void ST7565V128x64x1Flush(void *pvDisplayData);
extern void ST7565RectReverse(const tRectangle *pRect, REVRMODE rMode);
extern void ST7565SlideEffectBaseOnEdge(uint16_t i16X, sEDGESIDE edge, MOVEINBUFFER MoveInBufIdx);
extern void ST7565DrawImageSinceXY(uint16_t ui16X, uint16_t srcY, uint16_t destY, MOVEINBUFFER MoveInBufIdx);
extern void ST7565DrawImageSinceXY_fromLeftEdge(uint16_t ui16X, uint16_t srcY, uint16_t destY, MOVEINBUFFER MoveInBufIdx);
extern void ST7565Ret2UpLvlMenu(uint16_t ui16X, MOVEINBUFFER MoveInBufIdx);
extern void ST7565Ent2NextLvlMenu(uint16_t ui16X, MOVEINBUFFER MoveInBufIdx);
extern void ST7565Draw8x8Char(uint16_t x, uint16_t y, char text, FONT_DEF font);
extern void ST7565Draw6x8Char(uint16_t x, uint16_t y, uint8_t text, FONT_DEF font);
extern void ST7565DrawLangSelMenu(uint16_t x, uint16_t y, LANGUAGE lang, LANGMENUFONT_DEF font);
extern void ST7565Draw16x16Char(uint16_t x, uint16_t y, uint8_t text, FONT_DEF font);
extern void ST7565DrawVirtualKybd(MOVEINBUFFER MoveInBufIdx);
extern void ST7565DrawTextEdit(MOVEINBUFFER MoveInBufIdx, uint8_t page);
extern void ST7565TextEditSlideUp(MOVEINBUFFER MoveInBufIdx);
extern void ST7565TextEditSlideDown(MOVEINBUFFER MoveInBufIdx);
extern void ST7565Clear8x8Char(uint8_t x, uint8_t y, MOVEINBUFFER MoveInBufIdx);
extern void ST7565DrawScreen(uint8_t *lcdScrnBuff, MOVEINBUFFER MoveInBufIdx);
extern void ST7565ClearScreen(MOVEINBUFFER MoveInBufIdx);
extern void ST7565Refresh(MOVEINBUFFER MoveInBufIdx);
extern void ST7565DrawPixel( uint8_t x, uint8_t y, MOVEINBUFFER MoveInBufIdx);
extern void ST7565ClearPixel( uint8_t x, uint8_t y );
extern uint8_t ST7565GetPixel( uint8_t x, uint8_t y );
extern void ST7565DrawString( uint16_t x, uint16_t y, char* text, FONT_DEF font, MOVEINBUFFER MoveInBufIdx);
extern void ST7565DrawScreenInvertType(uint8_t *lcdScrnBuff, MOVEINBUFFER MoveInBufId);
extern void ST7565SlideUpBootupAnimation(MOVEINBUFFER MoveInBufIdx, uint8_t shiftCnt);
extern void ST7565SlideUp(int16_t srcY, int16_t destY, MOVEINBUFFER MoveInBufIdx);
extern void ST7565SlideDown(int16_t srcY, int16_t destY);
extern void ST7565ShiftFrameBuffer( uint8_t pixels );
void st7565DrawUprHalfScrnCrankVAnimation(const uint8_t *buffer, uint8_t ui8Col);
void st7565DrawUprHalfScrnCrankVAnimationSpace(uint8_t ui8Col);
void ST7565Ent2NextLvlMenu7page(uint16_t ui16X, MOVEINBUFFER MoveInBufIdx);
void st7565draw8x8Char(uint16_t x, uint16_t y, uint8_t c, FONT_DEF font);
void SetDisplayBuf(uint8_t* ui8Src, uint8_t ui8D0, uint8_t ui8D1, uint8_t ui8D2, uint8_t ui8D3, uint8_t ui8D4, uint8_t ui8D5, uint8_t ui8D6, uint8_t ui8D7, uint8_t ui8D8, uint8_t ui8D9, uint8_t ui8DA, uint8_t ui8DB, uint8_t ui8DC, uint8_t ui8DD, uint8_t ui8DE, uint8_t ui8DF);
void ST7565DrawStringx16(uint16_t x, uint16_t y, uint8_t* text, FONT_DEF font);


enum{
    Align_Right,
    Align_Left,
    Align_Central,
};

void LCD_String(uint16_t x, uint16_t y, char* text);
void LCD_String_ext(uint16_t y, char* text, uint8_t align);
void LCD_String_ext2(uint16_t y, char* text, uint8_t align, uint8_t space);

enum{
    Arrow_enter = 0x01,
    Arrow_RL = 0x02,
    Arrow_up = 0x04,
    ARROW_UP_DOWN = 0x08,
};
void LCD_Arrow(uint8_t arr_flag);

void LCD_Select_Arrow(uint32_t y);

void LCD_SOC_SOH(uint16_t percent);

void ST7565printf5x8(const XY_DISPLAY* display);

//*****************************************************************************
//
//!	Forward declaration of ST7565V Send byte function.
//
//*****************************************************************************
void st7565SendByte(uint8_t byte);

//
//
// Backlight Color Setting
//
//
void ST7650BacklightIOConfig(void);
void ST7650BacklightInit(void);
void ST7650Yellow(void);
void ST7650Blue(void);
void ST7650Green(void);
void ST7650Red(void);
void ST7650Purple(void);
void ST7650Purple_blink(bool ON_OFF);

void ST7650BacklightSet(uint8_t duty);


#endif // __ST7565_LCM_H__

