//*****************************************************************************
//
// Source File: utilities.c
// Description: useful functions.
//
//   
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 03/02/18     Jerry.W        Created file.
// 12/07/18     Jerry.W        Add get_bit_reverse() for printer data processing.
// 03/20/19     Henry.Y        Add floating_point_rounding() for floating rounding process.
// =============================================================================
//
//
// $Header$
// $DateTime$
// $Author$
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#include "utilities.h"
#include <stdlib.h>
#include <string.h>
#include <math.h>


int32_t new_atoi(uint8_t * sting, uint8_t len){
	char buf[len+1];
	memset(buf, 0,len+1);
	strncpy(buf, (char*)sting, len);

	return atoi(buf);
}



void set_bit(void *array, uint16_t n, uint8_t value){
	uint8_t* temp_a = (uint8_t *)array;
	if(value) temp_a[n/8] |= 0x01 << (n%8);
	else temp_a[n/8] &= ~(0x01 << (n%8));
}

void set_bit_reverse(void *array, uint16_t n, uint8_t value){
    uint8_t* temp_a = (uint8_t *)array;
    if(value) temp_a[n/8] |= 0x80 >> (n%8);
    else temp_a[n/8] &= ~(0x80 >> (n%8));
}

uint8_t get_bit(void *array, uint16_t n){
	uint8_t* temp_a = (uint8_t *)array;
	uint16_t temp = temp_a[n/8];
	uint8_t result = (temp & (0x01<<(n%8)))? 1 : 0;

	return result;
}

uint8_t get_bit_reverse(void *array, uint16_t n){
	uint8_t* temp_a = (uint8_t *)array;
	uint8_t temp = temp_a[n/8];
	uint8_t result = (temp & (0x80>>(n%8)))? 1 : 0;

	return result;
}

double floating_point_rounding(float val, uint8_t decimal_place, tFPR process){
	if(decimal_place > 4) while(1);
	double fp;
	uint16_t x = (decimal_place)? pow(10,decimal_place):1;
	float comp = 0.1;
	comp /= x;
	switch(process){
	case fround:
		fp = round(val*x);
		fp /= (float)x;
		fp += comp;
		break;
	case ffloor:
		fp = floor(val*x);
		fp /= (float)x;
		fp += comp;
		break;
	case fceil:
		fp = ceil(val*x);
		fp /= (float)x;
		fp += comp;
		break;
	}

	return (float)fp;
}

