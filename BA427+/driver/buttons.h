//*****************************************************************************
//
// Source File: buttons.h
// Description: Prototypes for the buttons driver.
//
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 07/31/15     William.H      Created file.
// 10/05/15     William.H      Check in BT2020 project which branched from LCDFontMaker(Pre-BT2020) by modifed related GPIOs.
// 03/03/16     Vincent.T      1. Add button func.for Left/Select/Right event for EngMode.
// 11/17/16     Vincent.T      1. Add New Func. UpBtn() and DownBtn();
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/driver/buttons.h#4 $
// $DateTime: 2017/09/22 10:27:40 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#ifndef __BUTTONS_H__
#define __BUTTONS_H__

//*****************************************************************************
//
// Defines for the hardware resources used by the pushbuttons.
//
// The switches are on the following ports/pins:
//
// PF3 - Up
// PF4 - Down
// PF2 - Left
// PF1 - Right
// PF0 - Select/Wake
//
// The switches tie the GPIO to ground, so the GPIOs need to be configured
// with pull-ups, and a value of 0 means the switch is pressed.
//
//*****************************************************************************
#define BUTTONS_GPIO_PERIPH     SYSCTL_PERIPH_GPIOF
#define BUTTONS_GPIO_BASE       GPIO_PORTF_BASE

#define NUM_BUTTONS             5
#define UP_BUTTON               GPIO_PIN_3
#define DOWN_BUTTON             GPIO_PIN_4
#define LEFT_BUTTON             GPIO_PIN_2
#define RIGHT_BUTTON            GPIO_PIN_1
#define SELECT_BUTTON           GPIO_PIN_0

#define ALL_BUTTONS             (LEFT_BUTTON | RIGHT_BUTTON | UP_BUTTON |     \
                                 DOWN_BUTTON | SELECT_BUTTON)

//*****************************************************************************
//
// Useful macros for detecting button events.
//
//*****************************************************************************
#define BUTTON_PRESSED(button, buttons, changed)                              \
        (((button) & (changed)) && ((button) & (buttons)))

#define BUTTON_RELEASED(button, buttons, changed)                             \
        (((button) & (changed)) && !((button) & (buttons)))

//*****************************************************************************
//
// If building with a C++ compiler, make all of the definitions in this header
// have a C binding.
//
//*****************************************************************************
#ifdef __cplusplus
extern "C"
{
#endif

//*****************************************************************************
//
// Functions exported from buttons.c
//
//*****************************************************************************
extern void ButtonsInit(void); 
extern uint8_t ButtonsPoll(uint8_t *pui8Delta, 
                                 uint8_t *pui8Raw); 

//*****************************************************************************
//
// Mark the end of the C bindings section for C++ compilers.
//
//*****************************************************************************
#ifdef __cplusplus
}
#endif

//*****************************************************************************
//
// Prototypes for the globals exported by this driver.
//
//*****************************************************************************

//*****************************************************************************
//
// Defination for Left/Select/Right button event.
//
//*****************************************************************************
bool LeftBtn(uint8_t ui8Mode);
bool SelectBtn(uint8_t ui8Mode);
bool RightBtn(uint8_t ui8Mode);
bool UpBtn(uint8_t ui8Mode);
bool DownBtn(uint8_t ui8Mode);

#endif // __BUTTONS_H__
