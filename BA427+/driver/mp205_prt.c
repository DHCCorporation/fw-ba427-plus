//*****************************************************************************
//
// Source File: mp205_prt.c
// Description: Prototypes for the printer driver.
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 10/15/15     Vincent.T      mp205_prt.c -> GPIOs setting, enable, disable...
// 10/23/15     Vincent.T      Power on: high; power off: low.
// 11/03/15     Vincent.T      Delay function call rearrange
// 01/22/16     Vincnet.T      1. Move PRINTER_POWER_ON/OFF marco into mp205_prt.h
//                             2. Cancel charge Printer-C in MP205HWInit();(charge in StartAnimation();)
// 06/14/16     Vincent.T      1. #include "../service\battcoresvc.h"
//                             2. No need to pre-charge C for printer anymore.
//                             3. Modify MP205ThermalHeadControl(); and BA6845StepMotorControl();(add chech internal battery)
// 06/21/16     Vincent.T      1. Modify MP205ThermalHeadControl();(STB group setting)
//                             2. Modify BA6845StepMotorControl();(Skip delay)
// 08/10/16     Vincent.T      1. Modify printer SPI data transmission control.(-CS -> MANUAL)
//                             2. Modify MP205IOConfig() and MP205DataTransControl()(for 1.);
//                             3. Modify MP205ThermalHeadControl() and BA6845StepMotorControl() to expand printout times.
// 11/15/17     Jerry.W        1. Modify power low condition
// 09/11/18     Henry.Y        Use system driver interface and error flag to replace global variable.
// 11/19/18     Henry.Y        Adjust BA6845StepMotorControl().
// 12/26/18     Jerry.W        spread heating time for the case of using AA batteries to print.
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/driver/mp205_prt.c#1 $
// $DateTime: 2017/11/27 10:38:22 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//

#include "../service\battcoresvc.h"
#include "mp205_prt.h"
#include "inc/hw_ints.h"

#include "string.h"
#include "stdlib.h"
#include "inc/hw_types.h"
#include "driverlib/rom.h"
#include "driverlib/adc.h"
#include "driverlib/ssi.h"
#include "driverlib/adc.h"
#include "mem_protect.h"
#include "delaydrv.h"
#include "../service/SystemInfosvs.h"

static bool heat_section[6];


void MP205IOConfig(void)
{
	//Printer Power
	ROM_SysCtlPeripheralEnable(PRINTER_POWER_PORT);
	ROM_GPIOPinTypeGPIOOutput(PRINTER_POWER_BASE, PRINTER_POWER_PIN);
	//ROM_GPIOPadConfigSet(PRINTER_POWER_BASE, PRINTER_POWER_PIN, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_OD);

	//Photo Sensor
	//Enable ADC
	ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);
	ROM_ADCReferenceSet(ADC0_BASE, ADC_REF_EXT_3V);

	ROM_SysCtlPeripheralEnable(PRINTER_CO_PORT);
	ROM_GPIOPinTypeADC(PRINTER_CO_BASE, PRINTER_CO_PIN);

	//Printer STB
	ROM_SysCtlPeripheralEnable(PRINTER_STB_PORT);
	ROM_GPIOPinTypeGPIOOutput(PRINTER_STB_BASE, PRINTER_STB1_PIN | PRINTER_STB2_PIN | PRINTER_STB3_PIN | PRINTER_STB4_PIN | PRINTER_STB5_PIN | PRINTER_STB6_PIN);
	//ROM_GPIOPadConfigSet(PRINTER_STB_BASE, PRINTER_STB1_PIN | PRINTER_STB2_PIN | PRINTER_STB3_PIN | PRINTER_STB4_PIN | PRINTER_STB5_PIN | PRINTER_STB6_PIN, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD);

	//Step-Motor
	ROM_SysCtlPeripheralEnable(PRINTER_SM_PORT);
	ROM_GPIOPinTypeGPIOOutput(PRINTER_SM_BASE, PRINTER_IN21_PIN | PRINTER_IN11_PIN | PRINTER_IN12_22_PIN);
	//ROM_GPIOPadConfigSet(PRINTER_SM_BASE, PRINTER_IN21_PIN | PRINTER_IN11_PIN | PRINTER_IN12_22_PIN, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_OD);

	//Printer Data
	ROM_SysCtlPeripheralEnable(PRINTER_SSI_MODULE_PORT);
	ROM_SysCtlPeripheralEnable(PRINTER_SSI_PORT);

	//Step-Motor
	//ROM_GPIOPinTypeGPIOOutput(PRINTER_SSI_BASE, PRINTER_SSI_LAT_PIN);
	//ROM_GPIOPadConfigSet(PRINTER_SSI_BASE, PRINTER_SSI_LAT_PIN, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD);

	// -CS -> MANUAL
	ROM_SysCtlPeripheralEnable(PRINTER_SSI_PORT);
	ROM_GPIOPinTypeGPIOOutput(PRINTER_SSI_BASE, PRINTER_SSI_LAT_PIN);
	ROM_GPIOPinWrite(PRINTER_SSI_BASE, PRINTER_SSI_LAT_PIN, 0xFF);

	//ROM_GPIOPinConfigure(PRINTER_SSI_LAT);
	ROM_GPIOPinConfigure(PRINTER_SSI_CLK);
	ROM_GPIOPinConfigure(PRINTER_SSI_DI);
	//Configure the GPIO settings for the SSI pins.
	//-LAT -> PH5
	//CLK  -> PH4
	//DI   -> PH7
	ROM_GPIOPinTypeSSI(PRINTER_SSI_BASE, PRINTER_SSI_CLK_PIN | PRINTER_SSI_DI_PIN);
	//SSI module config
	//ROM_SSIConfigSetExpClk(PRINTER_SSI_MODULE_BASE, get_sys_clk(), SSI_FRF_MOTO_MODE_0, SSI_MODE_MASTER, 800000, 8);
	ROM_SSIConfigSetExpClk(PRINTER_SSI_MODULE_BASE, get_sys_clk(), SSI_FRF_MOTO_MODE_0, SSI_MODE_MASTER, 10000000, 8);

}

void MP205HWInit(void)
{
	/************************/
	/* VARIABLES DEFINITION */
	/************************/
	uint8_t *ui8DatBuf;
	ui8DatBuf = malloc(MP205_PRINTOUT_SIZE * sizeof(uint32_t));
	MP205IOConfig();
	//Disable the SSI module
	//ROM_SSIDisable(PRINTER_SSI_MODULE_BASE);
	//Enable SSI module
	//ROM_SSIEnable(PRINTER_SSI_MODULE_BASE);

	//Reset data buffer
	MP205BufferReset(ui8DatBuf, MP205_PRINTOUT_SIZE);
	//Transmit data to printer
	MP205DataTransControl(ui8DatBuf);
	//Printer Power-Off
	PRINTER_POWER_OFF;
	//STB OFF
	PRINTER_STB_OFF;
	//Release memory
	free(ui8DatBuf);

	//Charge C(for A-buffer)
	//PRINTER_POWER_ON;
	//2 sec
	//ROM_SysCtlDelay(get_sys_clk() / 3);	// 1 second
	//ROM_SysCtlDelay(get_sys_clk() / 3);	// 1 second
	PRINTER_POWER_OFF;
}

void MP205BufferReset(uint8_t *ui8DatBuf, uint8_t ui8BufLen)
{
	uint8_t ui8LoopIndex = 0;
	for(ui8LoopIndex = 0; ui8LoopIndex < ui8BufLen; ui8LoopIndex++)
	{
		ui8DatBuf[ui8LoopIndex] = 0x00;
	}
}

void MP205DataTransControl(uint8_t *ui8DatBuf)
{
	uint8_t ui8LoopIndex = 0;
	uint8_t ui8Data = 0;

	ROM_GPIOPinWrite(PRINTER_SSI_BASE, PRINTER_SSI_LAT_PIN, 0x00);
	ROM_SSIEnable(PRINTER_SSI_MODULE_BASE);
	memset(heat_section, 0, sizeof(heat_section));
	for(ui8LoopIndex = 0; ui8LoopIndex < MP205_PRINTOUT_SIZE; ui8LoopIndex++)
	{
		ui8Data = ui8DatBuf[ui8LoopIndex];
		ROM_SSIDataPut(PRINTER_SSI_MODULE_BASE, ui8Data);
		while(ROM_SSIBusy(PRINTER_SSI_MODULE_BASE));
		if(ui8Data){
			heat_section[ui8LoopIndex/8] = true;
		}
	}
	ROM_SSIDisable(PRINTER_SSI_MODULE_BASE);
	ROM_GPIOPinWrite(PRINTER_SSI_BASE, PRINTER_SSI_LAT_PIN, 0xFF);
}

bool MP205CoSensorDetect(void)
{
	uint32_t PaperAD = 0;
	ROM_ADCSequenceDisable(ADC0_BASE, 3);
	ROM_ADCSequenceConfigure(ADC0_BASE, 3, ADC_TRIGGER_PROCESSOR, 0);
	ROM_ADCSequenceStepConfigure(ADC0_BASE, 3, 0, PRINTER_CO_AD_CH | ADC_CTL_IE | ADC_CTL_END);
	ROM_ADCSequenceEnable(ADC0_BASE, 3);
	ROM_ADCIntClear(ADC0_BASE, 3);
	ROM_ADCProcessorTrigger(ADC0_BASE, 3);
	while(!ROM_ADCIntStatus(ADC0_BASE, 3, false));
	ROM_ADCSequenceDataGet(ADC0_BASE, 3, &PaperAD);
	//Paper Detected?
	if(PaperAD <= 655)
	{
		return true;	//Paper Detected
	}
	else
	{
		return false;	//No Paper
	}
}

void MP205ThermalHeadControl(uint8_t ui8STBGroup, float Delayms)
{
	float external_voltage;
	uint8_t STB_index = 0;
	bool heat_flag = true;
	float delay;
	bool skip;
	//Printer ON;
	PRINTER_POWER_ON;
	while(heat_flag)
	{

		skip = false;

		BattCoreScanVoltage(&external_voltage);
		if((external_voltage < 9)&&BattCoreChkIntBatFast())
		{
			//
			// Power Low
			//
			set_system_error(Power_low);
			break;
		}
		delay = Delayms;
HEAT:
		if(ui8STBGroup == STB_ALL)
		{
			PRINTER_STB_ON;
			heat_flag = false;
		}
		else if(ui8STBGroup == STB_12_34_56)
		{
			switch(STB_index)
			{
			case 0:
				if(heat_section[0] || heat_section[1]){
					PRINTER_STB_12;
				}
				else{
					skip = true;
				}
				break;
			case 1:
				if(heat_section[2] || heat_section[3]){
					PRINTER_STB_34;
				}
				else{
					skip = true;
				}
				break;
			case 2:
				if(heat_section[4] || heat_section[5]){
					PRINTER_STB_56;
				}
				else{
					skip = true;
				}
				heat_flag = false;
				break;
			default:
				//error, trap MCU
				PRINTER_POWER_OFF;
				PRINTER_STB_OFF;
				while(1);//debug
			}
		}
		else if(ui8STBGroup == STB_123_456)
		{
			switch(STB_index)
			{
			case 0:
				if(heat_section[0] || heat_section[1] || heat_section[2]){
					PRINTER_STB_123;
				}
				else{
					skip = true;
				}
				break;
			case 1:
				if(heat_section[3] || heat_section[4] || heat_section[5]){
					PRINTER_STB_456;
				}
				else{
					skip = true;
				}
				heat_flag = false;
				break;
			default:
				//error, trap MCU
				PRINTER_POWER_OFF;
				PRINTER_STB_OFF;
				while(1);//debug
			}
		}
		else if(ui8STBGroup == STB_1_2_3_4_5_6)
		{
			switch(STB_index)
			{
			case 0:
				if(heat_section[0]){
					PRINTER_STB_ON_1;
				}
				else{
					skip = true;
				}
				break;
			case 1:
				if(heat_section[1]){
					PRINTER_STB_ON_2;
				}
				else skip = true;
				break;
			case 2:
				if(heat_section[2]){
					PRINTER_STB_ON_3;
				}
				else skip = true;
				break;
			case 3:
				if(heat_section[3]){
					PRINTER_STB_ON_4;
				}
				else skip = true;
				break;
			case 4:
				if(heat_section[4]){
					PRINTER_STB_ON_5;
				}
				else skip = true;
				break;
			case 5:
				if(heat_section[5]){
					PRINTER_STB_ON_6;
				}
				else skip = true;
				heat_flag = false;
				break;
			default:
				//error, trap MCU
				PRINTER_POWER_OFF;
				PRINTER_STB_OFF;
				while(1);//debug
			}
		}
		if(skip){
			//nothing to be heated!!
		}
		else{
			if(delay > 1){
				delay_ms(1.2);
				delay -= 1;
				PRINTER_STB_OFF;
				delay_ms(0.5);
				goto HEAT;
			}
			delay_ms(delay+0.1);
		}
		STB_index++;
		//STB OFF
		PRINTER_STB_OFF;

	}
	//Printer STB OFF
	PRINTER_STB_OFF;
	//Printer OFF;
	PRINTER_POWER_OFF;
}

void BA6845StepMotorControl(uint8_t ui8SMPhase, float fSMTimens)
{
	static uint8_t Moter_Step = 0;
	Moter_Step = (Moter_Step + 1)%4;

	PRINTER_POWER_ON;

	/*if(BattCoreChkIntBatFast()){
		set_system_error(Power_low);
		PRINTER_POWER_OFF;
		return;
	}*/
	if(get_system_error()){
		PRINTER_POWER_OFF;
		return;
	}

	switch(Moter_Step)
	{
	case 0:
		SM_PHASE1_2;
		break;
	case 1:
		SM_PHASE2_3;
		break;
	case 2:
		SM_PHASE3_4;
		break;
	case 3:
		SM_PHASE4_1;
		break;
	default:
		PRINTER_POWER_OFF;
		break;
	}
	if(fSMTimens == 0)
	{
		//
		// SKIP
		//
	}
	else
	{
		ROM_SysCtlDelay(((get_sys_clk()/3)/1000) * fSMTimens);
	}
	//Printer OFF;
	PRINTER_POWER_OFF;
}
