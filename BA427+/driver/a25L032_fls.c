//*****************************************************************************
//
// Source File: a25L032_fls.c
// Description: Functions to Read/Write/Chip Erase/Deep Power Down of A25L032
//              Flash Drive.
//
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 06/24/15     William.H      Created file.
// 10/05/15     William.H      Check in BT2020 project which branched from LCDFontMaker(Pre-BT2020) by modifed related GPIOs.
// 09/11/18     Henry.Y        Add set_flash_channel.
// 11/19/18     Jerry          add A25L032BusyCheck() function.
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/driver/a25L032_fls.c#5 $
// $DateTime: 2017/09/22 10:27:40 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#include <stdbool.h>
#include <stdint.h>
#include "driverlib/gpio.h"
#include "driverlib/ssi.h"
#include "driverlib/sysctl.h"
#include "driverlib/rom.h"
#include "inc/hw_ssi.h"
#include "inc/hw_types.h"
#include "uartstdio.h"
#include "ssi_master.h"

#if 1//DEBUG_TEST
void ABC(const char *pcString, ...){}
#define UARTprintf ABC	//to avoid compiler warning.
#endif

//*****************************************************************************
// A25L032 Flash Instruction Set
//*****************************************************************************
#define WREN		0x06    // Write Enable
#define WRDI		0x04    // Write Disable
#define PAGEPROG	0x02    // initialize start of Page Program sequence
#define READ		0x03    // initialize start of Read sequence
#define CE			0xC7    // Chip Erase
#define SE			0x20    // Sector Erase 
#define RDSR1		0x05    // Read STATUS Register-1
#define RDSR2		0x35    // Read STATUS Register-2
#define WRSR		0x01	// Write STATUS Register
#define DP			0xB9    // Deep Power-down 
#define RES			0xAB    // Release from Deep Power-down

//*****************************************************************************
//
// The system clock frequency in Hz.
// Must be supplied by the application.
//
//*****************************************************************************

uint8_t channel = 0;

void set_flash_channel(uint8_t chan){
	channel = chan;
}


/* asserts the CS pin to the Flash */
static
void SELECT (void)
{
    if(channel == 0){
    GPIOPinWrite(FLASH_SSI_CS_PORT_BASE, FLASH_SSI_CS_PIN, 0);
}
    else{
    	GPIOPinWrite(FLASH_SSI_CS_PORT_BASE2, FLASH_SSI_CS_PIN2, 0);
    }
}

/* de-asserts the CS pin to the Flash */
static
void DESELECT (void)
{
	if(channel == 0){
    GPIOPinWrite(FLASH_SSI_CS_PORT_BASE, FLASH_SSI_CS_PIN, FLASH_SSI_CS_PIN);
}
	else{
		GPIOPinWrite(FLASH_SSI_CS_PORT_BASE2, FLASH_SSI_CS_PIN2, FLASH_SSI_CS_PIN2);
	}
}

void A25L032ChkWEL(void)
{
	uint32_t ui32Status;

	UARTprintf("A25L032ChkWEL() \n");
	
	do{ 	
		FlsSSISetMode(FLSSSI_WRITE);	
		SELECT();												// Chip Select Low - activate
		SSIDataPut(FLASH_SSI_BASE, RDSR1);						// Send "Read Status Register-1" instruction
		while(SSIBusy(FLASH_SSI_BASE));

		//
		// Set SSI Frame Format to SSI_FRF_MOTO_MODE_1. i.e. captured data on the clock falling edges.
		//
		FlsSSISetMode(FLSSSI_READ);

		// Send the DUMMY data to causes the clock transitions(wiggle) during "READ Status Register-2" period.
		SSIDataPut(FLASH_SSI_BASE, 0x00);						// Flash needs some time to receive, decode and execute instructions.
		SSIDataGet(FLASH_SSI_BASE, &ui32Status);				// Receive incoming data

		//
		// Since we are using 8-bit data, mask off the MSB.
		//
		ui32Status &= 0x00FF;

		//
		// Wait until SSI is done transferring all the data in the transmit FIFO.
		//
		while(SSIBusy(FLASH_SSI_BASE));		

		DESELECT();
	}while(!(ui32Status & 0x02));					// Write-Enable-Latch while status<bit1> set to '0'

	UARTprintf("ChkWEL Status = 0x%x.\n", ui32Status);

}

void A25L032WaitBusy(void)
{
	uint32_t ui32Status;

	UARTprintf("A25L032WaitBusy() \n");

	do{
		SELECT();												// Chip Select Low - activate

		//
		// Set SSI Frame Format to SSI_FRF_MOTO_MODE_0. i.e. captured data on the clock rising edges.
		//
		FlsSSISetMode(FLSSSI_WRITE);							// Really important!!!If not add this line to switch SET MODE, the 'RDSR1' OP code will not be executed by Flash drive!
		SSIDataPut(FLASH_SSI_BASE, RDSR1);						// Send "Read Status Register-1" instruction	
		while(SSIBusy(FLASH_SSI_BASE));
		
		//
		// Set SSI Frame Format to SSI_FRF_MOTO_MODE_1. i.e. captured data on the clock falling edges.
		//
		FlsSSISetMode(FLSSSI_READ);

		// Send the DUMMY data to causes the clock transitions(wiggle) during "READ Status Register-2" period.
		SSIDataPut(FLASH_SSI_BASE, 0x00);						// Flash needs some time to receive, decode and execute instructions.
		SSIDataGet(FLASH_SSI_BASE, &ui32Status);				// Receive incoming data		

		//
		// Since we are using 8-bit data, mask off the MSB.
		//
		ui32Status &= 0x00FF;

		//
		// Wait until SSI is done transferring all the data in the transmit FIFO.
		//
		while(SSIBusy(FLASH_SSI_BASE));		
		
		DESELECT();												// Chip Select High - deactivate		
	}while(ui32Status & 0x01);						//  write-in-progress while status<bit0> set to '1'

	UARTprintf("Busy Status = 0x%x.\n", ui32Status);
	
}

bool A25L032BusyCheck(void)
{
	uint32_t ui32Status;

	UARTprintf("A25L032WaitBusy() \n");

	SELECT();												// Chip Select Low - activate

	//
	// Set SSI Frame Format to SSI_FRF_MOTO_MODE_0. i.e. captured data on the clock rising edges.
	//
	FlsSSISetMode(FLSSSI_WRITE);							// Really important!!!If not add this line to switch SET MODE, the 'RDSR1' OP code will not be executed by Flash drive!
	SSIDataPut(FLASH_SSI_BASE, RDSR1);						// Send "Read Status Register-1" instruction
	while(SSIBusy(FLASH_SSI_BASE));

	//
	// Set SSI Frame Format to SSI_FRF_MOTO_MODE_1. i.e. captured data on the clock falling edges.
	//
	FlsSSISetMode(FLSSSI_READ);

	// Send the DUMMY data to causes the clock transitions(wiggle) during "READ Status Register-2" period.
	SSIDataPut(FLASH_SSI_BASE, 0x00);						// Flash needs some time to receive, decode and execute instructions.
	SSIDataGet(FLASH_SSI_BASE, &ui32Status);				// Receive incoming data

	//
	// Since we are using 8-bit data, mask off the MSB.
	//
	ui32Status &= 0x00FF;

	//
	// Wait until SSI is done transferring all the data in the transmit FIFO.
	//
	while(SSIBusy(FLASH_SSI_BASE));

	DESELECT();												// Chip Select High - deactivate

	return ui32Status & 0x01;

}

void A25L032ReadStatReg(void)
{
	uint32_t ui32StatReg1, ui32StatReg2;

	UARTprintf("A25L032ReadStatReg() \n");

	{	// Read Status Register 1
		SELECT();												// Chip Select Low - activate
	
		//
		// Set SSI Frame Format to SSI_FRF_MOTO_MODE_0. i.e. captured data on the clock rising edges.
		//
		FlsSSISetMode(FLSSSI_WRITE);
		SSIDataPut(FLASH_SSI_BASE, RDSR1);						// Send "Read Status Register-1" instruction
		while(SSIBusy(FLASH_SSI_BASE));
		
		//
		// Set SSI Frame Format to SSI_FRF_MOTO_MODE_1. i.e. captured data on the clock falling edges.
		//
		FlsSSISetMode(FLSSSI_READ);

		// Send the DUMMY data to causes the clock transitions(wiggle) during "READ Status Register-1" period.
		SSIDataPut(FLASH_SSI_BASE, 0x00);						// Flash needs some time to receive, decode and execute instructions.
		SSIDataGet(FLASH_SSI_BASE, &ui32StatReg1);				// Receive incoming data	

		//
		// Wait until SSI is done transferring all the data in the transmit FIFO.
		//
		while(SSIBusy(FLASH_SSI_BASE));

		UARTprintf("A25L032ReadStatReg(), ui32StatReg1 = 0x%x.\n", ui32StatReg1);
		
		DESELECT();												// Chip Select High - deactivate
	}
	

	{	// Read Status Register 2
		SELECT();												// Chip Select Low - activate
	
		//
		// Set SSI Frame Format to SSI_FRF_MOTO_MODE_0. i.e. captured data on the clock rising edges.
		//
		FlsSSISetMode(FLSSSI_WRITE);
		
		SSIDataPut(FLASH_SSI_BASE, RDSR2);						// Send "Read Status Register-2" instruction
		while(SSIBusy(FLASH_SSI_BASE));
		
		//
		// Set SSI Frame Format to SSI_FRF_MOTO_MODE_1. i.e. captured data on the clock falling edges.
		//
		FlsSSISetMode(FLSSSI_READ);

		// Send the DUMMY data to causes the clock transitions(wiggle) during "READ Status Register-2" period.
		SSIDataPut(FLASH_SSI_BASE, 0x00);						// Flash needs some time to receive, decode and execute instructions.
		SSIDataGet(FLASH_SSI_BASE, &ui32StatReg2);				// Receive incoming data	

		//
		// Wait until SSI is done transferring all the data in the transmit FIFO.
		//
		while(SSIBusy(FLASH_SSI_BASE));

		UARTprintf("A25L032ReadStatReg(), ui32StatReg2 = 0x%x.\n", ui32StatReg2);
		
		DESELECT();												// Chip Select High - deactivate
	}	
	
}

void A25L032WriteStatReg(void)
{
	UARTprintf("A25L032WriteStatReg() \n");
	
	//
	// Wait until Flash is not busy
	//
	A25L032WaitBusy();

	//
	// Set SSI Frame Format to SSI_FRF_MOTO_MODE_0. i.e. captured data on the clock rising edges.
	//
	FlsSSISetMode(FLSSSI_WRITE);

	//
	// Send "Write Enable" instruction
	//
	SELECT();														// Chip Select Low - activate		
	SSIDataPut(FLASH_SSI_BASE, WREN);								// Send "Write Enable" instruction
	while(SSIBusy(FLASH_SSI_BASE));
	//SysCtlDelay(2);													// Wait a few cycles.  In some cases, the WREN bit is not set immediately and this prevents us from dropping through the polling loop before the bit is set.
   	DESELECT();

	A25L032ChkWEL();												// Check Write Enable Latch(WEL) <bit1> in Status Register-1, must set to '1' after send WREN.

	//
	// Set SSI Frame Format to SSI_FRF_MOTO_MODE_0. i.e. captured data on the clock rising edges.
	//
	FlsSSISetMode(FLSSSI_WRITE);

	//
	// Send "Write Register" instruction
	//
	SELECT();												// Chip Select Low - activate
	SSIDataPut(FLASH_SSI_BASE, WRSR);						// Send "Write Status Register" instruction
	while(SSIBusy(FLASH_SSI_BASE));

	SSIDataPut(FLASH_SSI_BASE, 0x00); // Status Register-1
	SSIDataPut(FLASH_SSI_BASE, 0x00); // Status Register-2
	
	while(SSIBusy(FLASH_SSI_BASE)); 

	DESELECT();	
	
}

void A25L032ChipErase(void)
{

	UARTprintf("A25L032ChipErase() \n");
	
    //
	// Wait until Flash is not busy
	//
	A25L032WaitBusy();

	//
	// Set SSI Frame Format to SSI_FRF_MOTO_MODE_0. i.e. captured data on the clock rising edges.
	//
	FlsSSISetMode(FLSSSI_WRITE);

	//
	// Send "Write Enable" instruction
	//
	SELECT();														// Chip Select Low - activate
	SSIDataPut(FLASH_SSI_BASE, WREN);								// Send "Write Enable" instruction
	while(SSIBusy(FLASH_SSI_BASE));									// Really important!!! If not add this line, the 'Write Enable' instruction will not be executed in Flash.	
	//SysCtlDelay(2);													// Wait a few cycles.  In some cases, the WREN bit is not set immediately and this prevents us from dropping through the polling loop before the bit is set.
   	DESELECT();														// Chip Select High - deactivate

	A25L032ChkWEL();												// Check Write Enable Latch(WEL) <bit1> in Status Register-1, must set to '1' after send WREN.

	//
	// Set SSI Frame Format to SSI_FRF_MOTO_MODE_0. i.e. captured data on the clock rising edges.
	//
	FlsSSISetMode(FLSSSI_WRITE);

	//
	// Send "Chip Erase" instruction
	//
	SELECT();														// Chip Select Low - activate again after WREN cmd
	SSIDataPut(FLASH_SSI_BASE, CE);									// Send "Chip Erase" instruction
	while(SSIBusy(FLASH_SSI_BASE));									
	DESELECT();														// Chip Select High - deactivate

	A25L032WaitBusy();

	//SysCtlDelay((get_sys_clk()/3)*64);							// Delay 64 sec. Refer to A25L032 data sheet page.41 - tCE.
	
}

void A25L032SectorErase(uint32_t ui32Addr)
{	
	UARTprintf("A25L032SectorErase() \n");
    
	//
	// Wait until Flash is not busy
	//
	A25L032WaitBusy();
	
	//
	// Set SSI Frame Format to SSI_FRF_MOTO_MODE_0. i.e. captured data on the clock rising edges.
	//
	FlsSSISetMode(FLSSSI_WRITE);

	//
	// Send "Write Enable" instruction
	//
	SELECT();														// Chip Select Low - activate
	SSIDataPut(FLASH_SSI_BASE, WREN);								// Send "Write Enable" instruction
	while(SSIBusy(FLASH_SSI_BASE));									// Really important!!! If not add this line, the 'Write Enable' instruction will not be executed in Flash.
//	SysCtlDelay(2);													// Wait a few cycles.  In some cases, the WREN bit is not set immediately and this prevents us from dropping through the polling loop before the bit is set.
   	DESELECT();														// Chip Select High - deactivate

	A25L032ChkWEL();												// Check Write Enable Latch(WEL) <bit1> in Status Register-1, must set to '1' after send WREN.

	//
	// Set SSI Frame Format to SSI_FRF_MOTO_MODE_0. i.e. captured data on the clock rising edges.
	//
	FlsSSISetMode(FLSSSI_WRITE);
	
	//
	// Send "Sector Erase" instruction
	//
	SELECT();														// Chip Select Low - activate again after WREN cmd
	SSIDataPut(FLASH_SSI_BASE, SE);									// Send "Sector Erase" instruction
	while(SSIBusy(FLASH_SSI_BASE));

	//
	// Send "24-Bit Address"
	//
	SSIDataPut(FLASH_SSI_BASE, ((ui32Addr >> 16) & 0xFF));			// Send Address Bit: [b23 - b16]
	SSIDataPut(FLASH_SSI_BASE, ((ui32Addr >> 8) & 0xFF));			// Send Address Bit: [b15 - b8]
	SSIDataPut(FLASH_SSI_BASE, (ui32Addr & 0xFF));					// Send Address Bit: [b7 - b0]

	while(SSIBusy(FLASH_SSI_BASE));
	
	DESELECT();														// Chip Select High - deactivate


	UARTprintf("Sector Erase Complete!\n");
	
}

	

void A25L032PageProgram(uint32_t ui32PgAddr, uint8_t *pui8TxBuf, uint32_t ui32Len)
{
	uint32_t ui32addidx;	

	UARTprintf("A25L032PageProgram() \n");	

	//
	// Wait until Flash is not busy
	//
	A25L032WaitBusy();

	//
	// Set SSI Frame Format to SSI_FRF_MOTO_MODE_0. i.e. captured data on the clock rising edges.
	//	
	FlsSSISetMode(FLSSSI_WRITE);
	
	//
	// Send "Write Enable" instruction
	//
	SELECT();												// Chip Select Low - activate flash
	SSIDataPut(FLASH_SSI_BASE, WREN);						// Send "Write Enable" instruction	
	while(SSIBusy(FLASH_SSI_BASE));							// Really important!!! If not add this line, the 'Write Enable' instruction will not be executed in Flash.
	//SysCtlDelay(2);											// Wait a few cycles.  In some cases, the WREN bit is not set immediately and this prevents us from dropping through the polling loop before the bit is set.
	DESELECT();												// Chip Select High - deactivate flash

	A25L032ChkWEL();										// Check Write Enable Latch(WEL) <bit1> in Status Register-1, must set to '1' after send WREN.

	//
	// Set SSI Frame Format to SSI_FRF_MOTO_MODE_0. i.e. captured data on the clock rising edges.
	//
	FlsSSISetMode(FLSSSI_WRITE);

	//
	// Send "Page Program" instruction
	//
	SELECT();												// Chip Select Low - activate flash
	SSIDataPut(FLASH_SSI_BASE, PAGEPROG);					// Send "Page Program" instruction
	while(SSIBusy(FLASH_SSI_BASE));

	//
	// Send "24-Bit Address"
	//
	SSIDataPut(FLASH_SSI_BASE, ((ui32PgAddr >> 16) & 0xFF));// Send Address Bit: [b23 - b16]
	SSIDataPut(FLASH_SSI_BASE, ((ui32PgAddr >> 8) & 0xFF));	// Send Address Bit: [b15 - b8]
	SSIDataPut(FLASH_SSI_BASE, (ui32PgAddr & 0xFF));		// Send Address Bit: [b7 - b0]

	while(SSIBusy(FLASH_SSI_BASE));

	// write 1 byte at a time from array
	for( ui32addidx = 0; ui32addidx < ui32Len; ui32addidx++)
	{
		// New write cycle must be initiated if writing across 256-byte page
		if( (( ui32PgAddr + ui32addidx ) % 256 == 0) && (ui32addidx != 0) )
		{
			UARTprintf("Write over 256-bytes page!! THIS SHOULD NOT HAPPEN!\n");
			
			DESELECT();										// Chip Select High - deactivate flash
			
			// Wait until Flash is not busy			
			A25L032WaitBusy();

			//
			// Set SSI Frame Format to SSI_FRF_MOTO_MODE_0. i.e. captured data on the clock rising edges.
			//
			FlsSSISetMode(FLSSSI_WRITE);

			// Send "Write Enable" instruction
			SELECT();										// Chip Select Low - activate flash
			SSIDataPut(FLASH_SSI_BASE, WREN);				// Send "Write Enable" instruction
			while(SSIBusy(FLASH_SSI_BASE));
			//SysCtlDelay(2);									// Wait a few cycles.  In some cases, the WREN bit is not set immediately and this prevents us from dropping through the polling loop before the bit is set.
			DESELECT();										// Chip Select High - deactivate flash

			A25L032ChkWEL();								// Check Write Enable Latch(WEL) <bit1> in Status Register-1, must set to '1' after send WREN.

			//
			// Set SSI Frame Format to SSI_FRF_MOTO_MODE_0. i.e. captured data on the clock rising edges.
			//
			FlsSSISetMode(FLSSSI_WRITE);

			// Send "Page Program" instruction
			SELECT();										// Chip Select Low - activate flash
			SSIDataPut(FLASH_SSI_BASE, PAGEPROG);			// Send "Page Program" instruction
			while(SSIBusy(FLASH_SSI_BASE));

			// Send "24-Bit Address"
			SSIDataPut(FLASH_SSI_BASE, (((ui32PgAddr+ui32addidx) >> 16) & 0xFF));	// Send Address Bit: [b23 - b16]
			SSIDataPut(FLASH_SSI_BASE, (((ui32PgAddr+ui32addidx) >> 8) & 0xFF));	// Send Address Bit: [b15 - b8]
			SSIDataPut(FLASH_SSI_BASE, ((ui32PgAddr+ui32addidx) & 0xFF));			// Send Address Bit: [b7 - b0]

			while(SSIBusy(FLASH_SSI_BASE));
			
		}

		// Send the data using the "blocking" put function.  This function
		// will wait until there is room in the send FIFO before returning.
		// This allows you to assure that all the data you send makes it into
		// the send FIFO.
		SSIDataPut(FLASH_SSI_BASE, pui8TxBuf[ui32addidx]);

		// wait for the SSI TX fifo to be empty
		while(!(HWREG(FLASH_SSI_BASE + SSI_O_SR) & SSI_SR_TFE));		
	}

	//
	// Wait until SSI is done transferring all the data in the transmit FIFO.
	//
	while(SSIBusy(FLASH_SSI_BASE));

	DESELECT();												// Chip Select High - deactivate flash	
}


void A25L032ReadData(uint32_t ui32Addr, uint8_t *pui8RxBuf, uint32_t ui32Len)
{
	uint32_t ui32addidx;
	uint32_t buf;

	UARTprintf("A25L032ReadData() \n");

	//
	// Wait until Flash is not busy
	//
	A25L032WaitBusy();

	//
	// Set SSI Frame Format to SSI_FRF_MOTO_MODE_0. i.e. captured data on the clock rising edges.
	//
	FlsSSISetMode(FLSSSI_WRITE);

	//
	// Send "Read" instruction
	//
	SELECT();												// Chip Select Low - activate flash
	SSIDataPut(FLASH_SSI_BASE, READ);						// Send "Read" instruction
	while(SSIBusy(FLASH_SSI_BASE));

	//
	// Send "24-Bit Address"
	//
	SSIDataPut(FLASH_SSI_BASE, ((ui32Addr >> 16) & 0xFF));	// Send Address Bit: [b23 - b16]
	SSIDataPut(FLASH_SSI_BASE, ((ui32Addr >> 8) & 0xFF));	// Send Address Bit: [b15 - b8]
	SSIDataPut(FLASH_SSI_BASE, (ui32Addr & 0xFF));			// Send Address Bit: [b7 - b0]

	while(SSIBusy(FLASH_SSI_BASE)); // Really important!!!If not add this line, there will no data read from Flash!

	//
	// Set SSI Frame Format to SSI_FRF_MOTO_MODE_1. i.e. captured data on the clock falling edges.
	//
	FlsSSISetMode(FLSSSI_READ);

	//
	// Receive 1 byte at a time from SSI bus.
	//
	UARTprintf("Start to receive data from Rx FIFO.\n");

	for( ui32addidx = 0; ui32addidx < ui32Len; ui32addidx++)
	{
		// Send the DUMMY data to causes the clock transitions(wiggle) during READ period.
		SSIDataPut(FLASH_SSI_BASE, 0x00);

		//
		// Receive the data using the "blocking" Get function. This function
		// will wait until there is data in the receive FIFO before returning.
		//
		SSIDataGet(FLASH_SSI_BASE, &buf); // Type Conversion(or Type Cast) from (uint8_t *) to (uint32_t *)
		pui8RxBuf[ui32addidx] = (uint8_t)buf;															// Implicit conversions: No special syntax is required because the conversion is type safe and no data will be lost.

		//
		// Since we are using 8-bit data, mask off the MSB.
		//
		//pui32RxBuf[ui32addidx] &= 0x00FF;								// This is not neccessary since pui8RxBuf[] is uint8_t type for each element.
	}

	//
	// Wait until SSI is done transferring all the data in the transmit FIFO.
	//
	while(SSIBusy(FLASH_SSI_BASE));

	DESELECT();												// Chip Select High - deactivate flash
}

void A25L032DeepPowerDown(void)
{

	UARTprintf("A25L032DeepPowerDown() \n");
	
	//
	// Wait until Flash is not busy
	//
	A25L032WaitBusy();

	//
	// Set SSI Frame Format to SSI_FRF_MOTO_MODE_0. i.e. captured data on the clock rising edges.
	//
	FlsSSISetMode(FLSSSI_WRITE);

	//
	// Send "Deep Power-down(DP)" instruction
	//
	SELECT();												// Chip Select Low - activate flash
	SSIDataPut(FLASH_SSI_BASE, DP);							// Send "DP" instruction
	while(SSIBusy(FLASH_SSI_BASE));
	DESELECT();												// Chip Select High - deactivate flash
}

bool A25L032ReleseDPd(void)
{
	uint8_t i, j=0;
	uint32_t ui32Res;

	UARTprintf("A25L032ReleseDPd() \n");
	
	//
	// Wait until Flash is not busy
	//
	A25L032WaitBusy();

	//
	// Set SSI Frame Format to SSI_FRF_MOTO_MODE_0. i.e. captured data on the clock rising edges.
	//
	FlsSSISetMode(FLSSSI_WRITE);

	//
	// Send "Release from Deep Power-down(DP) and Read Electronic Signature(RES)" instruction
	//
	SELECT();												// Chip Select Low - activate flash
	SSIDataPut(FLASH_SSI_BASE, RES);						// Send "DP" instruction
	while(SSIBusy(FLASH_SSI_BASE));

	for(i = 0 ; i < 3 ; i++)
    {
    	// The DP instruction code is followed by 3 DUMMY bytes.
    	SSIDataPut(FLASH_SSI_BASE, 0x00);
	}

	//
	// Set SSI Frame Format to SSI_FRF_MOTO_MODE_1. i.e. captured data on the clock falling edges.
	//
	FlsSSISetMode(FLSSSI_READ);

	do{
		// Send the DUMMY data to causes the clock transitions(wiggle) during READ period.
		SSIDataPut(FLASH_SSI_BASE, 0x00);
		
		// Read "8-bit Electronic Signature"(RES)
		SSIDataGet(FLASH_SSI_BASE, &ui32Res);

		//
		// Since we are using 8-bit data, mask off the MSB.
		//
		ui32Res &= 0x00FF;

		if(ui32Res!= 0x15 && (++j == 3)) //re-try 3 times to read RES data
		{	
			UARTprintf("Release from Deep Power-down mode failed! \n");
			DESELECT();											// Chip Select High - deactivate flash
			return false;
		}		
	}while(ui32Res != 0x15);
	
	UARTprintf("Release from Deep Power-down mode success! \n");

	while(SSIBusy(FLASH_SSI_BASE));
	
	DESELECT();													// Chip Select High - deactivate flash
	
	return true;
	
}

