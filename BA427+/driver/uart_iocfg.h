//*****************************************************************************
//
// Source File: uart_iocfg.h
// Description: UART GPIO Pins Definition.
//				
//
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 07/03/15     William.H      Created file.
// 10/05/15     William.H      Check in BT2020 project which branched from LCDFontMaker(Pre-BT2020) by modifed related GPIOs.
// 03/22/16     Henry.Y        Layout "P-BT2100-VOO.b": change UART0 to UART7. 
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/driver/uart_iocfg.h#5 $
// $DateTime: 2017/09/22 10:27:40 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#ifndef __UART_IOCFG_H__
#define __UART_IOCFG_H__

#include "inc/hw_memmap.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"

//*****************************************************************************
//
//  UART Peripherals and I/O Pin Definition
//
//  Below defines the following UART peripherals and I/O signals.  You must
//  review these and change as needed for your own board:
//   - UART peripheral
//   - GPIO Port A peripheral (for UART pins)
//   - U0RX - PA0
//   - U0TX - PA1
//
//*****************************************************************************

/* UART port */
#define UART_MODULE_SYSCTL_PERIPH	SYSCTL_PERIPH_UART7
#define UART_PORT_SYSCTL_PERIPH		SYSCTL_PERIPH_GPIOC
#define UART_PORT_BASE				GPIO_PORTC_BASE
#define UART_BASE					UART7_BASE

/* U7RX pin */
#define U7RX_PIN					GPIO_PIN_4
#define U7RX_MUX_SEL				GPIO_PC4_U7RX

/* U7TX pin */
#define U7TX_PIN					GPIO_PIN_5
#define U7TX_MUX_SEL				GPIO_PC5_U7TX

extern void UARTHwInit(void);

#endif // __UART_IOCFG_H__
