//*****************************************************************************
//
// Source File: mem_protect.h
// Description: memory management function.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 01/04/18     Jerry.W        Created file.
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/driver/wifi_driver.h#2 $
// $DateTime: 2017/12/18 16:17:54 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#ifndef __PROTECT_MEM_H__
#define __PROTECT_MEM_H__


#undef malloc
#undef free

#define M_VARNAME(var) #var

#define MEM_LOG		0

void *prtect_malloc(size_t _size);
void protect_free(void* ptr);

void print_heap_usage();

void stack_init();
void get_stack_usage();

#if MEM_LOG
#define malloc(x) 	 prtect_malloc(x); UARTprintf("malloc(%d) at %s (%d)\n",x , __FILE__, __LINE__);
#define free(x)	UARTprintf("free(%s) at %s (%d)\n",M_VARNAME(x), __FILE__, __LINE__); protect_free(x);

#else
#define malloc(x) 	prtect_malloc(x);
#define free(x)		protect_free(x);

#endif



#endif
