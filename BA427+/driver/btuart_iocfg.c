//*****************************************************************************
//
// Source File: btuart_iocfg.c
// Description: Bluetooth module IOs and UART GPIO Pins Definition.
//				
//
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 01/07/16     William.H      Created file.
// 02/18/16     Vincent.T      1. Remove GPIOPadConfigSet(BT_CONN_STATUS_BASE, BT_CONN_STATUS_PIN, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD); due to new version of library didnt support this func.
// 04/06/16     Vincent.T      1. Modify BTResetIOConfig(); to enable Bluetooth power pin.
// 06/01/17     Henry          FW version: BT2100_WW_V01.a
//                             1. Modify BTResetIOConfig():set initial pin status to make sure BT off before pin configuration.
//                             2. Add BTDNIOConfig() in BTHwInit():set DHC device name I/O.
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/driver/btuart_iocfg.c#1 $
// $DateTime: 2017/11/27 10:38:22 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#include <stdbool.h>
#include <stdint.h>
#include "driverlib/uart.h"
#include "driverlib/rom.h"
#include "uartstdio.h"
#include "btuart_iocfg.h"
#include "delaydrv.h"



void BTResetIOConfig(void)
{
	//
	// Enable the peripherals of BT reset pin 
	//
	ROM_SysCtlPeripheralEnable(BT_RESET_PERIPHERAL);

	//
	//  Enable the peripherals of BT power pin
	//
	ROM_SysCtlPeripheralEnable(BT_POWER_PERIPHERAL);

	//
	// Config as output direction
	//
	ROM_GPIOPinTypeGPIOOutput(BT_RESET_BASE, BT_RESET_PIN);
	BT_RESET_LOW;	// Turn off bluetooth

	//
	// Config as output direction(power)
	//
	ROM_GPIOPinTypeGPIOOutput(BT_POWER_BASE, BT_POWER_PIN);	

	BT_POWER_OFF;
}

void BTDNIOConfig(void)
{
	//
	// Enable the peripherals of BT reset pin 
	//
	ROM_SysCtlPeripheralEnable(BT_DN_PERIPHERAL);

	//
	// Config as output direction
	//
	ROM_GPIOPinTypeGPIOOutput(BT_DN_BASE, BT_DN_PIN);

	DN_DHC;
}

void BTConStatIOConfig(void)
{
	//
	// Enable the peripherals of BT connection status pin 
	//
	ROM_SysCtlPeripheralEnable(BT_CONN_STATUS_PERIPHERAL);

	//
	// Config as input direction
	//
	ROM_GPIOPinTypeGPIOInput(BT_CONN_STATUS_BASE, BT_CONN_STATUS_PIN);

	GPIOPadConfigSet(BT_CONN_STATUS_BASE, BT_CONN_STATUS_PIN, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPU);
}

void BTUartIOConfig(void)
{
	//
	// Enable the peripherals that we will be redirecting.
	// The UART itself needs to be enabled, as well as the GPIO port
	// containing the pins that will be used.
	//
	ROM_SysCtlPeripheralEnable(BTUART_MODULE_SYSCTL_PERIPH);
	ROM_SysCtlPeripheralEnable(BTUART_PORT_SYSCTL_PERIPH);

	//
	// Configure the GPIO pin muxing for the UART function.
	// This is only necessary if your part supports GPIO pin function muxing.
	// Study the data sheet to see which functions are allocated per pin.
	//
    ROM_GPIOPinConfigure(U6RX_MUX_SEL);
    //ROM_GPIOPinConfigure(U6TX_MUX_SEL);

	//
	// Since GPIO P0 and P1 are used for the UART function, they must be
	// configured for use as a peripheral function (instead of GPIO).
	//
	//ROM_GPIOPinTypeUART(BTUART_PORT_BASE, U6RX_PIN | U6TX_PIN);

	//
    // Set the default UART configuration.
    //
	ROM_UARTConfigSetExpClk(BTUART_BASE, get_sys_clk(), UART_BAUDRATE_BT, UART_CONFIG);

	//
    // Configure and enable UART interrupts.
    //	
	ROM_UARTIntEnable(BTUART_BASE, UART_INT_RX | UART_INT_RT);	
	ROM_UARTIntEnable(BTUART_BASE, UART_INT_TX);

	//
    // Enable interrupts now that the application is ready to start.
    //
    ROM_IntEnable(BTUART_INT);
	
}

void BTHwInit(void)
{
	//
	// Config bluetooth device name I/O pin, must be initialized before power and reset pin configuration.
	//
	BTDNIOConfig();

	//
	// Config bluetooth reset I/O pin
	//
	BTResetIOConfig();
	
	//
	// Config bluetooth connection status pin
	//
	BTConStatIOConfig();

	//
	// Bluetooth UART6 I/O config
	//
	BTUartIOConfig();
}

