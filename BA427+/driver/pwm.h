//*****************************************************************************
//
// Source File: pwm.h
// Description: Prototypes for the pwm driver.
//
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 10/08/15     Vincent T.     pwm.h -> GPIOs setting, function declaraction
// 03/20/19     Henry.Y        increase PWM frequency from 100Hz to 400Hz.
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/driver/pwm.h#5 $
// $DateTime: 2017/09/22 10:27:40 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#ifndef __DRIVER_PWM_H__
#define __DRIVER_PWM_H__

#include <stdbool.h>
#include <stdint.h>
#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "inc/hw_sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/pwm.h"

// pwm frequency
#define PWM_DIV_64_FREQ 400

//PWM Module0
#define PWM_MODULE_PORT SYSCTL_PERIPH_PWM0
#define PWM_MODULE_BASE	PWM0_BASE
#define PWM_MOUDLE_PIN	GPIO_PG0_M0PWM4

//GPIO Setting(PD0)
#define PWM_PORT		SYSCTL_PERIPH_GPIOG
#define PWM_BASE		GPIO_PORTG_BASE
#define PWM_PIN			GPIO_PIN_0

//Function Declaraction
extern void PWMIOConfig(void);
extern void PWMHWInit(void);
extern void PWMEnable(void);
extern void PWMDisable(void);
void PWMDutySet(uint8_t ui8Duty);

#endif /* __DRIVER_PWM_H__ */
