//*****************************************************************************
//
// Source File: utilities.h
// Description: useful functions.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 03/02/18     Jerry.W	       Created file.
// 12/07/18     Jerry.W        Add get_bit_reverse() for printer data processing.
// =============================================================================
//
//
// $Header: //depot/Tester/pb/service.h#0 $
// $DateTime: 2017/09/26 17:08:19 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************


#ifndef SERVICE_UTILITIES_H_
#define SERVICE_UTILITIES_H_

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#define VARNAME(var) #var

int32_t new_atoi(uint8_t * sting, uint8_t len);

void set_bit(void *array, uint16_t n, uint8_t value);
void set_bit_reverse(void *array, uint16_t n, uint8_t value);

uint8_t get_bit(void *array, uint16_t n);
uint8_t get_bit_reverse(void *array, uint16_t n);

typedef enum{
	fround,
	ffloor,
	fceil	
}tFPR;
	
/*************************************************************************************************
 * @ Name		: floating_point_rounding
 * @ Brief 		: rounding input float variable.
 * @ Parameter 	: val -- input variable.
 * 				  decimal_place -- rounding to designated decimal place.
 * 				  process -- 
 * 				  fround: rounding;
 * 				  ffloor: truncate;
 * 				  fceil: round up
 * @ Return		: double type of rounding.
 *************************************************************************************************/
double floating_point_rounding(float val, uint8_t decimal_place, tFPR process);


#endif /* SERVICE_UTILITIES_H_ */
