//*****************************************************************************
//
// Source File: st7565_lcm.c
// Description: Function to configure ST7565 LCM Controller and draw LCD pixel 
//              for Character/String/Screen.
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 06/25/15     William.H      Created file.
// 10/01/15     William.H      Re-arrange/remove Global varaible's usage due to we've used stack to behave the manu level selection.
//                             Respective global varaible by the respective widget file to be maintained by their own.
// 10/05/15     William.H      Check in BT2020 project which branched from LCDFontMaker(Pre-BT2020) by modifed related GPIOs.
// 10/27/15     William.H      1. Add MoveInBufIdx parameter into ST7565SlideEffectBaseOnEdge() function.
//                             2. Add new API of st7565SlideUpBootupAnimWithNewImage() for bootup animation with DHC logo.
// 01/06/16     William.H      Create st7565drawLangSelMenu() function to process the drawing of language menu list.
// 01/21/16     William.H      Add ST7565DrawImageSinceXY_fromLeftEdge() func that provide the slide effect of screen based on left edge of LCD panel.
//                             Bitmap could be slided start from any Y-axis(irregular of one page) then move into panel from left to right.
//                             This func is used at bootup animation.  
// 02/02/16     Vincent.T      1. New function st7565DrawUprHalfScrnCrankVAnimation(); to draw Cranking Voltage Waveform;
// 02/03/16     Vincent.T      1. New function st7565FlipLeftInTransSlide7page(); to present sliding effect but only 7 pages.
//                             2. New function ST7565Ent2NextLvlMenu7page(); to present sliding effect but only 7 pages.
// 03/02/16     Vincent.T      1. Add backlight color func. body
// 04/07/16     Vincent.T      1. New global variable gui8LCDRRatio.(LCD V5 voltage regulator internal resistor ratio set)
//                             2. Move st7565SendByte(); form .c to .h
// 09/06/16     Vincent.T      1. Modify st7565draw6x8Char(); column[6] -> column[8]
// 11/15/16     Ian.C          1. Modify st7565draw6x8Char() to draw 7*8.
// 11/21/16     Vincent.T      1. New Func. SetDisplayBuf() and ST7565DrawStringx16(); to display content on Screen.
// 09/25/17		Jerry.W		   fix the error of ST7565Draw8x8Char
// 05/18/18     Jerry.W        add backlight blink function.
// 09/11/18     Henry.Y        Use system driver interface to replace global variable.
//                             st7565DrawUprHalfScrn() add parameter to replace global variable.
//                             ST7565HwInit applied new hierarchical architecture.
// 12/07/18     Jerry.W        ST7565V128x64x1LinePrintV() add transparent setting parameter.
// 03/20/19     Henry.Y        Add LCM setting to match HW bias1/9, \5x booster setup.
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/driver/st7565_lcm.c#7 $
// $DateTime: 2017/09/26 17:42:07 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#include "st7565_lcm.h"
#include "delaydrv.h"
#include "utilities.h"
#include "..\driver/pwm.h"

//*****************************************************************************
//
//! Translates a 24-bit RGB color to a display driver-specific color.
//!
//! \param c is the 24-bit RGB color.  The least-significant byte is the blue
//! channel, the next byte is the green channel, and the third byte is the red
//! channel.
//
//! This macro translates a 24-bit RGB color into a value that can be written
//! into the display's frame buffer in order to reproduce that color, or the
//! closest possible approximation of that color.
//
//! \return Returns the display-driver specific color.
//
//*****************************************************************************
#define DPYCOLORTRANSLATE(c)    ((((((c) & 0x00ff0000) >> 16) * 19661) + \
                                  ((((c) & 0x0000ff00) >> 8) * 38666) +  \
                                  (((c) & 0x000000ff) * 7209)) /         \
                                 (65536 * 128))

//*****************************************************************************
//
//!	Forward declaration of ST7565V Send byte function.
//
//*****************************************************************************
//void st7565SendByte(uint8_t byte);

//*****************************************************************************
//
//!	The ST7565V identify the data bus signals by combination of A0, /RD, /WR	
//!	signals.
//!	
//!	CMD(c) 		- Display Instructions. A0 = 0, /RD = 1, /WR = 0	
//!	DATA(d) 	- Display Data Write.   A0 = 1, /RD = 1, /WR = 0
//!	RCVDATA(r)	- Display Data Read.	A0 = 1, /RD = 0, /WR = 1
//
//*****************************************************************************
#define CMD(c)        do { ROM_GPIOPinWrite(ST7565_CS_BASE, ST7565_CS_PIN, ST7565_CS_PIN); ROM_GPIOPinWrite( ST7565_A0_BASE, ST7565_A0_PIN, 0x00 );          st7565SendByte( c ); } while (0);
#define DATA(d)       do { ROM_GPIOPinWrite(ST7565_CS_BASE, ST7565_CS_PIN, ST7565_CS_PIN); ROM_GPIOPinWrite( ST7565_A0_BASE, ST7565_A0_PIN, ST7565_A0_PIN ); st7565SendByte( d ); } while (0);
#define RCVDATA(r)    do { ROM_GPIOPinWrite(ST7565_CS_BASE, ST7565_CS_PIN, ST7565_CS_PIN); ROM_GPIOPinWrite( ST7565_A0_BASE, ST7565_A0_PIN, ST7565_A0_PIN ); st7565ReadByte( &r); } while (0);		

//*****************************************************************************
//
//!	Display Image Buffer.
//!	
//!	\_st7565buffer0
//!	\_st7565buffer1
//!	
//!	Two image buffers for transition effect of Sub-menus as well as 
//!	Virtual Keyboard v.s. Text Editor. Ex:
//!	
//!	Main Menu 			==> _st7565buffer0
//!	Setting Menu		==> _st7565buffer1
//!	Virtual Keyboard	==> _st7565buffer0
//!	Text Editor			==> _st7565buffer1
//
//*****************************************************************************
uint8_t _st7565buffer0[128*64/8] = {0};		// Totally [128*64/8] bytes in one full screen
uint8_t _st7565buffer1[128*64/8] = {0};		// Totally [128*64/8] bytes in one full screen

typedef enum{
	blue = 0,
	yellow,
	green,
	red	
}BACKLIGHT_COLOR;
static BACKLIGHT_COLOR light_color;

/**************************************************************************/
/* Private Methods                                                        */
/**************************************************************************/

/**************************************************************************/
/*! 
    @brief Renders the buffer contents

    @param[in]  buffer
                Pointer to the buffer containing the raw pixel data
*/
/**************************************************************************/
void st7565WriteBuffer(uint8_t *buffer) 
{
  uint8_t c, p;
  int pagemap[] = { 3, 2, 1, 0, 7, 6, 5, 4 };
  //int pagemap[] = { 0, 1, 2, 3, 4, 5, 6, 7 };

  for(p = 0; p < 8; p++) 
  {
    CMD(ST7565_CMD_SET_PAGE | pagemap[p]);
    CMD(ST7565_CMD_SET_COLUMN_LOWER | (0x0 & 0xf));
    CMD(ST7565_CMD_SET_COLUMN_UPPER | ((0x0 >> 4) & 0xf));
    CMD(ST7565_CMD_RMW);

	// One page has 128 bytes totally. One time for writing one Segment byte, 
	// i.e. write one Column Address(c).
    for(c = 0; c < 128; c++) 
    {
      DATA(buffer[(128*pagemap[p])+c]);
    }
  }
}

/**************************************************************************/
/*! 
    @brief Simulates an Data Bus Parallel Interface write using GPIO

    @param[in]  byte
                The byte to send
*/
/**************************************************************************/
void st7565SendByte(uint8_t byte)
{
  // Note: This code can be optimised to avoid the branches by setting
  // GPIO registers directly, but we'll leave it as is for the moment
  // for simplicity sake

  // Data Bus Pin Config as Output for Write data	
  ROM_GPIOPinTypeGPIOOutput(ST7565_DATA_BASE, ST7565_D0_PIN | ST7565_D1_PIN | ST7565_D2_PIN | ST7565_D3_PIN |  
  	                                          ST7565_D4_PIN | ST7565_D5_PIN | ST7565_D6_PIN | ST7565_D7_PIN );

  // Set CS low to activate chip
  ROM_GPIOPinWrite(ST7565_CS_BASE, ST7565_CS_PIN, 0x00);

  // Set WR low for Data Write operation
  ROM_GPIOPinWrite(ST7565_WR_BASE, ST7565_WR_PIN, 0x00);

  // Set RD high for Data Write operation
  ROM_GPIOPinWrite(ST7565_RD_BASE, ST7565_RD_PIN, ST7565_RD_PIN);   

  // Write Data Bus
  ROM_GPIOPinWrite(ST7565_DATA_BASE, ST7565_D0_PIN|ST7565_D1_PIN|ST7565_D2_PIN|ST7565_D3_PIN|
                                     ST7565_D4_PIN|ST7565_D5_PIN|ST7565_D6_PIN|ST7565_D7_PIN, byte);

  // Set WR high for Writing complete
  ROM_GPIOPinWrite(ST7565_WR_BASE, ST7565_WR_PIN, ST7565_WR_PIN);

  // Set CS high to deactivate chip select
  ROM_GPIOPinWrite(ST7565_CS_BASE, ST7565_CS_PIN, ST7565_CS_PIN);
}

/**************************************************************************/
/*! 
    @brief Simulates an Data Bus Parallel Interface read using GPIO

    @param[in]  byte
                The byte to read
*/
/**************************************************************************/
void st7565ReadByte(uint8_t *ui8RdData)
{  
  // Note: This code can be optimised to avoid the branches by setting
  // GPIO registers directly, but we'll leave it as is for the moment
  // for simplicity sake

  // Data Bus Pin Config as Input for Read data
  ROM_GPIOPinTypeGPIOInput(ST7565_DATA_BASE, ST7565_D0_PIN | ST7565_D1_PIN | ST7565_D2_PIN | ST7565_D3_PIN |  
  	                                         ST7565_D4_PIN | ST7565_D5_PIN | ST7565_D6_PIN | ST7565_D7_PIN );

  // Set CS low to activate chip
  ROM_GPIOPinWrite(ST7565_CS_BASE, ST7565_CS_PIN, 0x00);

  // Set RD low for Data Read operation
  ROM_GPIOPinWrite(ST7565_RD_BASE, ST7565_RD_PIN, 0x00);

  // Set WR high for Data Read operation
  ROM_GPIOPinWrite(ST7565_WR_BASE, ST7565_WR_PIN, ST7565_WR_PIN);
  
  // Dummy Read data 
  ROM_GPIOPinRead(ST7565_DATA_BASE, ST7565_D0_PIN|ST7565_D1_PIN|ST7565_D2_PIN|ST7565_D3_PIN|
                                    		ST7565_D4_PIN|ST7565_D5_PIN|ST7565_D6_PIN|ST7565_D7_PIN);
  // Set RD high for Data Reading complete
  ROM_GPIOPinWrite(ST7565_RD_BASE, ST7565_RD_PIN, ST7565_RD_PIN);
  
  // Set RD low for Data Read operation  
  ROM_GPIOPinWrite(ST7565_RD_BASE, ST7565_RD_PIN, 0x00);
  *ui8RdData = ROM_GPIOPinRead(ST7565_DATA_BASE, ST7565_D0_PIN|ST7565_D1_PIN|ST7565_D2_PIN|ST7565_D3_PIN|
                                    			ST7565_D4_PIN|ST7565_D5_PIN|ST7565_D6_PIN|ST7565_D7_PIN);

  ROM_GPIOPinWrite(ST7565_RD_BASE, ST7565_RD_PIN, ST7565_RD_PIN);

  // Set CS high to deactivate chip select
  ROM_GPIOPinWrite(ST7565_CS_BASE, ST7565_CS_PIN, ST7565_CS_PIN);
  
}

/**************************************************************************/
/*!
    @brief  Draws a single graphic character using the supplied font
*/
/**************************************************************************/
void st7565DrawChar(uint16_t x, uint16_t y, uint8_t c, FONT_DEF font, MOVEINBUFFER MoveInBufIdx)
{
  uint8_t col, column[10];


  // Check if the requested character is available
  if ((c >= font.u8FirstChar) && (c <= font.u8LastChar))
  {
    // Retrieve appropriate columns from font data
    for (col = 0; col < font.u8Width; col++)
    {
      column[col] = font.au8FontTable[((c - font.u8FirstChar) * font.u8Width) + col];	// Get first column of appropriate character
    }
  }
  else
  {    
    // Requested character is not available in this font ... send a space instead
    for (col = 0; col < font.u8Width; col++)
    {
      column[col] = 0xFF;	// Send solid space
    }
  }

  // Render each column
  uint16_t xoffset, yoffset;
  for (xoffset = 0; xoffset < font.u8Width; xoffset++)
  {
    for (yoffset = 0; yoffset < (font.u8Height + 1); yoffset++)
    {
      uint8_t bit = 0x00;
      bit = (column[xoffset] << (8 - (yoffset + 1)));	// Shift current row bit left
      bit = (bit >> 7);									// Shift current row bit right (results in 0x01 for black, and 0x00 for white)
      if (bit)
      {
        ST7565DrawPixel(x + xoffset, y + yoffset, MoveInBufIdx);
      }
    }
  }
}

/**************************************************************************/
/* Public Methods                                                         */
/**************************************************************************/

/**************************************************************************/
/*! 
    @GPIO Configuration of the ST7565 LCD display
*/
/**************************************************************************/
void ST7565IOConfig(void)
{
	//Backlight color config.
	ST7650BacklightIOConfig();

  // Chip Select Pin Peripheral Config
  ROM_SysCtlPeripheralEnable(ST7565_CS_PORT);
  ROM_GPIOPinTypeGPIOOutput(ST7565_CS_BASE, ST7565_CS_PIN);
  GPIOPadConfigSet(ST7565_CS_BASE, ST7565_CS_PIN, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD);

  // Reset Pin Peripheral Config 	
  ROM_SysCtlPeripheralEnable(ST7565_RST_PORT);
  ROM_GPIOPinTypeGPIOOutput(ST7565_RST_BASE, ST7565_RST_PIN);
  GPIOPadConfigSet(ST7565_RST_BASE, ST7565_RST_PIN, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD);

  // Data/Instruction Selection Pin Config
  ROM_SysCtlPeripheralEnable(ST7565_A0_PORT);
  ROM_GPIOPinTypeGPIOOutput(ST7565_A0_BASE, ST7565_A0_PIN);
  GPIOPadConfigSet(ST7565_A0_BASE, ST7565_A0_PIN, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD);

  // Write Pin Config
  ROM_SysCtlPeripheralEnable(ST7565_WR_PORT);
  ROM_GPIOPinTypeGPIOOutput(ST7565_WR_BASE, ST7565_WR_PIN);
  GPIOPadConfigSet(ST7565_WR_BASE, ST7565_WR_PIN, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD);

  // Read Pin Config
  ROM_SysCtlPeripheralEnable(ST7565_RD_PORT);
  ROM_GPIOPinTypeGPIOOutput(ST7565_RD_BASE, ST7565_RD_PIN);
  GPIOPadConfigSet(ST7565_RD_BASE, ST7565_RD_PIN, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD);

  // Data Bus Pins Config
  ROM_SysCtlPeripheralEnable(ST7565_DATA_PORT);
  ROM_GPIOPinTypeGPIOOutput(ST7565_DATA_BASE, ST7565_D0_PIN | ST7565_D1_PIN | ST7565_D2_PIN | ST7565_D3_PIN |  
  	                                          ST7565_D4_PIN | ST7565_D5_PIN | ST7565_D6_PIN | ST7565_D7_PIN );
  GPIOPadConfigSet(ST7565_DATA_BASE, ST7565_D0_PIN | ST7565_D1_PIN | ST7565_D2_PIN | ST7565_D3_PIN |  
  	                                     ST7565_D4_PIN | ST7565_D5_PIN | ST7565_D6_PIN | ST7565_D7_PIN, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD);  
}

/**************************************************************************/
/*! 
    @Hardware Initialises of the ST7565 LCD display
*/
/**************************************************************************/
void ST7565HwInit(uint8_t LCD_ratio)
{
  // Note: This can be optimised to set all pins to output and high
  // in two commands by manipulating the registers directly (assuming
  // that the pins are located in the same GPIO bank).  The code is left
  // as is for clarity sake in case the pins are not all located in the
  // same bank.
  ST7565IOConfig();

	//Backlight color config.
	ST7650BacklightInit();

  // Configure A0 pin to output and set high
  ROM_GPIOPinWrite(ST7565_A0_BASE, ST7565_A0_PIN, ST7565_A0_PIN);

  // Configure Reset pin and set high
  ROM_GPIOPinWrite(ST7565_RST_BASE, ST7565_RST_PIN, ST7565_RST_PIN);

  // Configure Chip select pin and set high
  ROM_GPIOPinWrite(ST7565_CS_BASE, ST7565_CS_PIN, ST7565_CS_PIN);

  // Reset
  ROM_GPIOPinWrite(ST7565_CS_BASE, ST7565_CS_PIN, 0x00);				// Set CS low
  ROM_GPIOPinWrite(ST7565_RST_BASE, ST7565_RST_PIN, 0x00);				// Set reset low
	SysCtlDelay((get_sys_clk()/3)/2);									// Wait 500mS
  ROM_GPIOPinWrite(ST7565_RST_BASE, ST7565_RST_PIN, ST7565_RST_PIN);	// Set reset high

  // Configure Display  
  CMD(LCM_BOOSTER_CMD? ST7565_CMD_SET_BIAS_9:ST7565_CMD_SET_BIAS_7);  // LCD Bias Select
  CMD(ST7565_CMD_SET_ADC_NORMAL);						// ADC Select
  CMD(ST7565_CMD_SET_COM_REVERSE);						// SHL Select. This is the setting against common sense! Why reverse?
  CMD(ST7565_CMD_SET_DISP_START_LINE);					// Initial Display Line
  CMD(ST7565_CMD_SET_POWER_CONTROL | 0x04);				// Turn on voltage converter (VC=1, VR=0, VF=0)
  SysCtlDelay((get_sys_clk()/3)/20);					// Wait 50mS
  CMD(ST7565_CMD_SET_POWER_CONTROL | 0x06);				// Turn on voltage regulator (VC=1, VR=1, VF=0)
  SysCtlDelay((get_sys_clk()/3)/20);					// Wait 50mS
  CMD(ST7565_CMD_SET_POWER_CONTROL | 0x07);				// Turn on voltage follower
  SysCtlDelay((get_sys_clk()/3)/100);				// Wait 10mS
  if(LCM_BOOSTER_CMD){
	  CMD(ST7565_CMD_SET_BOOSTER_FIRST);				//booster ratio \5x booster
	  CMD(ST7565_CMD_SET_BOOSTER_5);				//hw booster circuit \5x booster
	  SysCtlDelay((get_sys_clk()/3)/100);				// Wait 10mS
  }
  CMD(ST7565_CMD_SET_RESISTOR_RATIO | LCD_ratio);	// Set LCD operating voltage

  // Turn display on
  CMD(ST7565_CMD_DISPLAY_ON);
  CMD(ST7565_CMD_SET_ALLPTS_NORMAL);
  
}

//*****************************************************************************
//
//! Draws a pixel on the screen.
//!
//! \param pvDisplayData is a pointer to the driver-specific data for this
//! display driver.
//! \param lX is the X coordinate of the pixel.
//! \param lY is the Y coordinate of the pixel.
//! \param ulValue is the color of the pixel.
//!
//! This function sets the given pixel to a particular color.  The coordinates
//! of the pixel are assumed to be within the extents of the display.
//!
//! \return None.
//
//*****************************************************************************
void ST7565V128x64x1PixelDraw(void *pvDisplayData, int32_t i32X, int32_t i32Y,
                      uint32_t ui32Value)
{
	uint8_t temp=0, ui8Data=0;
		
	CMD(ST7565_CMD_SET_PAGE | (i32Y / 8));
	CMD(ST7565_CMD_SET_COLUMN_LOWER | (i32X & 0xf));
    CMD(ST7565_CMD_SET_COLUMN_UPPER | ((i32X >> 4) & 0xf));

	CMD(ST7565_CMD_RMW);

	RCVDATA(ui8Data);

	if(ui32Value)
	{
		//temp = pow(2,(i32Y % 8));
		temp = 0x01 << (i32Y % 8);
		ui8Data |= temp;		
	}
	else
	{
		//temp = pow(2,(i32Y % 8));
		temp = 0x01 << (i32Y % 8);
		ui8Data &= ~temp;
	}

	//
    // Write the pixel value.
    //
	DATA(ui8Data);	
}

//*****************************************************************************
//
//! Draws a horizontal sequence of pixels on the screen.
//!
//! \param pvDisplayData is a pointer to the driver-specific data for this
//! display driver.
//! \param i32X is the X coordinate of the first pixel.
//! \param i32Y is the Y coordinate of the first pixel.
//! \param i32X0 is sub-pixel offset within the pixel data, which is valid for 1
//! or 4 bit per pixel formats.
//! \param i32Count is the number of pixels to draw.
//! \param i32BPP is the number of bits per pixel; must be 1, 4, or 8 optionally
//! ORed with various flags unused by this driver.
//! \param pui8Data is a pointer to the pixel data.  For 1 and 4 bit per pixel
//! formats, the most significant bit(s) represent the left-most pixel.
//! \param pui8Palette is a pointer to the palette used to draw the pixels.
//!
//! This function draws a horizontal sequence of pixels on the screen, using
//! the supplied palette.  For 1 bit per pixel format, the palette contains
//! pre-translated colors; for 4 and 8 bit per pixel formats, the palette
//! contains 24-bit RGB values that must be translated before being written to
//! the display.
//!
//! \return None.
//
//*****************************************************************************
void ST7565V128x64x1PixelDrawMultiple(void *pvDisplayData, int32_t i32X, int32_t i32Y, int32_t i32X0,
                              int32_t i32Count, int32_t i32BPP,
                              const uint8_t *pui8Data,
                              const uint8_t *pui8Palette)
{
	uint8_t temp=0, ui8Data=0;
	uint32_t ui32Byte;

	CMD(ST7565_CMD_SET_PAGE | (i32Y / 8));

	//
    // Determine how to interpret the pixel data based on the number of bits
    // per pixel.
    //
    switch(i32BPP & 0xFF)
    {
    	//
        // The pixel data is in 1 bit per pixel format.
        //
        case 1:
        {
			//
            // Loop while there are more pixels to draw.
            //
            while(i32Count)
            {
                //
                // Get the next byte of image data.
                //
                ui32Byte = *pui8Data++;

                //
                // Loop through the pixels in this byte of image data.
                //
                for(; (i32X0 < 8) && i32Count; i32X0++, i32Count--)
                {
                	CMD(ST7565_CMD_SET_COLUMN_LOWER | ((i32X+i32X0) & 0xf));
    				CMD(ST7565_CMD_SET_COLUMN_UPPER | (((i32X+i32X0) >> 4) & 0xf));
					CMD(ST7565_CMD_RMW);
					RCVDATA(ui8Data);
					
                    //
                    // Draw this pixel in the appropriate color.
                    //
                    uint8_t ui8BPP = ((uint32_t *)pui8Palette)
                                            [(ui32Byte >> (7 - i32X0)) & 1];

                    if(ui8BPP)
					{
						//temp = pow(2,(i32Y % 8));
						temp = 0x01 << (i32Y % 8);
						ui8Data |= temp;
					}
					else
					{
						//temp = pow(2,(i32Y % 8));
						temp = 0x01 << (i32Y % 8);
						ui8Data &= ~temp;
					}
					
					//
    				// Write the pixel value.
    				//
					DATA(ui8Data);					
                }

                //
                // Start at the beginning of the next byte of image data.
                //
                i32X0 = 0;
            }

            //
            // The image data has been drawn.
            //
            break;	
        }
    }
}


//*****************************************************************************
//
//! Draws a horizontal line.
//!
//! \param pvDisplayData is a pointer to the driver-specific data for this
//! display driver.
//! \param i32X1 is the X coordinate of the start of the line.
//! \param i32X2 is the X coordinate of the end of the line.
//! \param i32Y is the Y coordinate of the line.
//! \param ui32Value is the color of the line.
//!
//! This function draws a horizontal line on the display.  The coordinates of
//! the line are assumed to be within the extents of the display.
//!
//! \return None.
//
//*****************************************************************************
void ST7565V128x64x1LineDrawH(void *pvDisplayData, int32_t i32X1, int32_t i32X2, int32_t i32Y,
                      uint32_t ui32Value)
{
	uint8_t temp=0, ui8Data=0;
	uint32_t ui32Hlen = 0, uIdx = 0;

	ui32Hlen = (i32X1 < i32X2) ? (i32X2 - i32X1) : (i32X1 - i32X2);
    ui32Hlen += 1;
	
	CMD(ST7565_CMD_SET_PAGE | (i32Y / 8));

	for(uIdx = 0; uIdx < ui32Hlen; uIdx++)
	{
		CMD(ST7565_CMD_SET_COLUMN_LOWER | ((i32X1+uIdx) & 0xf));
    	CMD(ST7565_CMD_SET_COLUMN_UPPER | (((i32X1+uIdx) >> 4) & 0xf));
		CMD(ST7565_CMD_RMW);
		RCVDATA(ui8Data);

		if(ui32Value)
		{
			//temp = pow(2,(i32Y % 8));
			temp = 0x01 << (i32Y % 8);
			ui8Data |= temp;
		}
		else
		{
			//temp = pow(2,(i32Y % 8));
			temp = 0x01 << (i32Y % 8);
			ui8Data &= ~temp;
		}

		//
    	// Write the pixel value.
    	//
		DATA(ui8Data);
	}
	
}

//*****************************************************************************
//
//! Draws a vertical line.
//!
//! \param pvDisplayData is a pointer to the driver-specific data for this
//! display driver.
//! \param i32X is the X coordinate of the line.
//! \param i32Y1 is the Y coordinate of the start of the line.
//! \param i32Y2 is the Y coordinate of the end of the line.
//! \param ui32Value is the color of the line.
//!
//! This function draws a vertical line on the display.  The coordinates of the
//! line are assumed to be within the extents of the display.
//!
//! \return None.
//
//*****************************************************************************
void
ST7565V128x64x1LineDrawV(void *pvDisplayData, int32_t i32X, int32_t i32Y1, int32_t i32Y2,
                      uint32_t ui32Value)
{
	uint8_t temp=0, ui8Data=0;
	uint32_t ui32Vlen = 0, uIdx = 0;

	ui32Vlen = (i32Y1 < i32Y2) ? (i32Y2 - i32Y1) : (i32Y1 - i32Y2);
    ui32Vlen += 1;	

	for(uIdx = 0; uIdx < ui32Vlen; uIdx++)
	{
		CMD(ST7565_CMD_SET_PAGE | ((i32Y1+uIdx) / 8));
		CMD(ST7565_CMD_SET_COLUMN_LOWER | (i32X & 0xf));
  		CMD(ST7565_CMD_SET_COLUMN_UPPER | ((i32X >> 4) & 0xf));		
		CMD(ST7565_CMD_RMW);
		RCVDATA(ui8Data);

		if(ui32Value)
		{
			//temp = pow(2, (i32Y1+uIdx) % 8);
			temp = 0x01 << ((i32Y1+uIdx) % 8);
			ui8Data |= temp;
		}
		else
		{
			//temp = pow(2, (i32Y1+uIdx) % 8);
			temp = 0x01 << ((i32Y1+uIdx) % 8);
			ui8Data &= ~temp;
		}
		
		//
    	// Write the pixel value.
    	//
		DATA(ui8Data);
	}
}

//*****************************************************************************
//
//! Draws a vertical line.
//!
//! \param pvDisplayData is a pointer to the driver-specific data for this
//! display driver.
//! \param i32X is the X coordinate of the line.
//! \param i32Y is the Y coordinate of the start of the line.
//! \param len is the length of the line.
//! \param Value is the data of the line.
//!
//! This function draws a vertical line on the display.  The coordinates of the
//! line are assumed to be within the extents of the display.
//!
//! \return None.
//
//*****************************************************************************
void
ST7565V128x64x1LinePrintV(void *pvDisplayData, int32_t i32X, int32_t i32Y, uint32_t len,
                      uint8_t *Value, bool transparent)
{
	uint8_t ui8Data=0, data_o;
	uint8_t page_len, i;
	uint32_t temp_len = 0;


	while(len != temp_len){

		page_len = 8 - (i32Y % 8);
		if(page_len > (len - temp_len)) page_len = len - temp_len;

		data_o = 0;

		for(i = 0; i != page_len; i++){
			if(get_bit(Value, temp_len+i)){
				set_bit(&data_o, i+(i32Y%8), 1);
			}
		}

		CMD(ST7565_CMD_SET_PAGE | (i32Y / 8));
		CMD(ST7565_CMD_SET_COLUMN_LOWER | (i32X & 0xf));
  		CMD(ST7565_CMD_SET_COLUMN_UPPER | ((i32X >> 4) & 0xf));
		CMD(ST7565_CMD_RMW);

		if(transparent){
			RCVDATA(ui8Data);
			ui8Data |= data_o;
		}
		else{
			if(page_len != 8){
				RCVDATA(ui8Data);
				for(i = 0 ; i < page_len ; i++){
					set_bit(&ui8Data, i+(i32Y%8), 0);
				}
				ui8Data |= data_o;
			}
			else{
				ui8Data = data_o;
			}
		}

		DATA(ui8Data);

		i32Y += page_len;
		temp_len += page_len;
	}
}


//*****************************************************************************
//
//! Fills a rectangle.
//!
//! \param pvDisplayData is a pointer to the driver-specific data for this
//! display driver.
//! \param pRect is a pointer to the structure describing the rectangle.
//! \param ui32Value is the color of the rectangle.
//!
//! This function fills a rectangle on the display.  The coordinates of the
//! rectangle are assumed to be within the extents of the display, and the
//! rectangle specification is fully inclusive (in other words, both i16XMin and
//! i16XMax are drawn, aint32_t with i16YMin and i16YMax).
//!
//! \return None.
//
//*****************************************************************************
void
ST7565V128x64x1RectFill(void *pvDisplayData, const tRectangle *pRect,
                     uint32_t ui32Value)
{
	unsigned int uY;

    for(uY = pRect->i16YMin; uY <= pRect->i16YMax; uY++)
    {
        ST7565V128x64x1LineDrawH(0, pRect->i16XMin, pRect->i16XMax, uY, ui32Value);
    }
}

//*****************************************************************************
//
//! Horizontal reverse.(Press Left/Right button to slide the Anti-White box)
//!
//!	\param ui16X1 is the X coordinate of the start of the line.
//!	\param ui16X2 is the X coordinate of the end of the line.
//!	\param ui16Y is the Y coordinate of the line.
//!
//!	This function reverse the specified display region of x-axis: ui16X1~ui16X2
//!	y-axis: one column(D0~D7), to program one rectangle at one time. To be used in
//! ex: Main Menu's horizontal slide the Anti-White box by Left/Right key press
//!		or Virtual Keyboard's Anti-White box by Up/Down/Left/Right key. 
//!
//!	\return None.
//
//*****************************************************************************
void st7565VLineDrawHReverse(uint16_t ui16X1, uint16_t ui16X2, uint16_t ui16Y)
{
	uint8_t ui8Data=0;
	uint32_t ui32Hlen = 0, uIdx = 0;

	ui32Hlen = (ui16X1 < ui16X2) ? (ui16X2 - ui16X1) : (ui16X1 - ui16X2);
    ui32Hlen += 1;	
	
	CMD(ST7565_CMD_SET_PAGE | (ui16Y));

	for(uIdx = 0; uIdx < ui32Hlen; uIdx++)
	{
		CMD(ST7565_CMD_SET_COLUMN_LOWER | ((ui16X1+uIdx) & 0xf));
    	CMD(ST7565_CMD_SET_COLUMN_UPPER | (((ui16X1+uIdx) >> 4) & 0xf));
		CMD(ST7565_CMD_RMW);
		RCVDATA(ui8Data);
		
		//
    	// Write the pixel value.
    	//
		DATA(~ui8Data); // To invert the whole column
	}
	
}

//*****************************************************************************
//
//! Vertical reverse.(Press Up/Down button to slide the Anti-White box)
//!
//!	\param ui16X1 is the X coordinate of the start of the line.
//!	\param ui16X2 is the X coordinate of the end of the line.
//!	\param ui16Y is the Y coordinate of the line.
//!
//!	This function reverse the specified display region of x-axis: ui16X1~ui16X2
//!	y-axis: ONLY one bit(ui16Y), to program one line at one time. To be used in
//!	ex: Main Menu's vertical slide the Anti-White box by Up/Down key press.
//!
//!	\return None.
//
//*****************************************************************************
void st7565VLineDrawVReverse(uint16_t ui16X1, uint16_t ui16X2, uint16_t ui16Y)
{
	uint8_t temp=0, ui8Data=0;
	uint32_t ui32Hlen = 0, uIdx = 0;

	ui32Hlen = (ui16X1 < ui16X2) ? (ui16X2 - ui16X1) : (ui16X1 - ui16X2);
    ui32Hlen += 1;
	
	CMD(ST7565_CMD_SET_PAGE | (ui16Y / 8));	

	for(uIdx = 0; uIdx < ui32Hlen; uIdx++)
	{
		CMD(ST7565_CMD_SET_COLUMN_LOWER | ((ui16X1+uIdx) & 0xf));
    	CMD(ST7565_CMD_SET_COLUMN_UPPER | (((ui16X1+uIdx) >> 4) & 0xf));
		CMD(ST7565_CMD_RMW);
		RCVDATA(ui8Data);

		//temp = pow(2,(ui16Y % 8));
		temp = 0x01<<(ui16Y % 8);
		ui8Data ^= temp; //	Toggle bit 'i32Y' only.
		
		//
    	// Write the pixel value.
    	//
		DATA(ui8Data);		
	}
	
}

//*****************************************************************************
//
//!	Rectangle Reversion.
//!
//!	\param pRect is a pointer to the structure describing the rectangle.
//!	\param rMode is the reversion type of main menu. (Left/Right or Up/Down)
//!
//!	This function reverse the specified display region in for example Main Menu.
//!	The coordinates of the rectangle are assumed to be within the extents of 
//!	the display, and the rectangle specification is fully inclusive (in other 
//! words, all region are drawn in i16XMin/i16XMax and i16YMin/i16YMax).
//!
//!	\return None.
//
//*****************************************************************************
void
ST7565RectReverse(const tRectangle *pRect, REVRMODE rMode)
{
	uint16_t uY;

	if(rMode == HORIZ)
	{
		//
		//	To program one column(D0~D7) then in N columns(i16XMin to i16XMax) by uY index. 
		//	==> Draw a rectangle box at one time.
		//
		for(uY = (pRect->i16YMin/8); uY <= (pRect->i16YMax/8); uY++)
		{
			st7565VLineDrawHReverse(pRect->i16XMin, pRect->i16XMax, uY);
		}	
	}
	else if(rMode == VERTICAL)
	{
		//
		//	To program one dot/bit(COM Output) then in N columns(i16XMin to i16XMax) by uY index. 
		//	==> Draw one line at one time.
		//
		for(uY = pRect->i16YMin; uY <= pRect->i16YMax; uY++)
    	{
        	st7565VLineDrawVReverse(pRect->i16XMin, pRect->i16XMax, uY);
    	}
	}

	
}

//*****************************************************************************
//
//!	Write Display Data Start from ui16X Column.
//!
//!	\param buffer is a pointer to the driver-specific data for this display driver.
//!	\param ui16X is the X coordinate of the start column of screen.
//!	\param edge is the panel edge side(Left or Right). Slide effect base on edge
//!				side.			
//!
//!	
//!	RIGHTEDGE ==> Write display data(col[0] to col[128-ui16X]) since column[ui16X] on panel. 
//! LEFTEDGE  ==> Write display data(col[ui16X] to col[128]) since column[0] on panel.
//!	For example:
//! 
//! (1).
//!	for(i=0; i<128; i++) // Screen move in from right edge. (right to left)
//!	{
//!		ST7565SlideEffectBaseOnEdge(127-i, RIGHTEDGE);	
//!	}
//! 
//! (2).
//!	for(i=0; i<128; i++) // Screen slide out by right edge. (left to right)
//!	{
//!		ST7565SlideEffectBaseOnEdge(i, RIGHTEDGE);
//!	}
//!
//!	(3).
//!	for(i=0; i<128; i++) // Screen move in from left edge. (left to right)
//!	{
//!		ST7565SlideEffectBaseOnEdge(127-i, LEFTEDGE);
//!	}
//!
//!	\return None.
//
//*****************************************************************************
void st7565WriteBufferSinceX(uint8_t *buffer, uint16_t ui16X, sEDGESIDE edge)
{
	int16_t c, p;
  	uint8_t pagemap[] = { 0, 1, 2, 3, 4, 5, 6, 7 };

  	for(p = 0; p < 8; p++) 
  	{
    	CMD(ST7565_CMD_SET_PAGE | p);

		if(edge == RIGHTEDGE)
		{
    		CMD(ST7565_CMD_SET_COLUMN_LOWER | (ui16X & 0xf));
    		CMD(ST7565_CMD_SET_COLUMN_UPPER | ((ui16X >> 4) & 0xf));
    		CMD(ST7565_CMD_RMW);

			// One page has 128 bytes totally. One time for writing one Segment byte, 
			// i.e. write one Column Address(c).
			// Write display data started from column[ui16X].
    		for(c = 0; c < (128-ui16X); c++) 
    		{
      			DATA(buffer[(128*pagemap[p])+c]); // Column address is incremented(+1) with each read/write command.
    		}
		}
		else if(edge == LEFTEDGE)
		{
			CMD(ST7565_CMD_SET_COLUMN_LOWER | (0x0 & 0xf));
    		CMD(ST7565_CMD_SET_COLUMN_UPPER | ((0x0 >> 4) & 0xf));
    		CMD(ST7565_CMD_RMW);

			// One page has 128 bytes totally. One time for writing one Segment byte, 
			// i.e. write one Column Address(c).
			// Write display data started from column[0].
			for(c = ui16X; c < 128; c++) 
    		{
      			DATA(buffer[(128*pagemap[p])+c]); // Column address is incremented(+1) with each read/write command.		
    		}
		}
		
	}
}

//*****************************************************************************
//
//!	Slide Effect Base On Left or Right Edge of LCD Panel.
//!
//!	\param ui16X is the X coordinate of the start column of screen.
//!	\param edge is the panel edge side(Left or Right). Slide effect base on edge
//!				side.			
//!
//!	This function provide the slide effect of screen. Based on edge side selection,
//!	screen can be moved in from right/left edge, or slided out by right/left edge.
//!
//!	\return None.
//
//*****************************************************************************
void ST7565SlideEffectBaseOnEdge(uint16_t ui16X, sEDGESIDE edge, MOVEINBUFFER MoveInBufIdx)
{
	if(MoveInBufIdx == BUFFER0)
{
	st7565WriteBufferSinceX(_st7565buffer0, ui16X, edge);
}
	else if(MoveInBufIdx == BUFFER1)
	{
		st7565WriteBufferSinceX(_st7565buffer1, ui16X, edge);
	}
}

// Draw Image starting from right edge, the picture has to locate at left side.
void st7565DrawImgSinceXY(uint16_t ui16X, uint16_t srcY, uint16_t destY, uint8_t *buffer)
{
	uint16_t c_orgi, c_new, p;
	uint8_t ui8DataCurPg=0;

	for(p = srcY/8; p <(destY/8+1); p++) 
  	{
  		if(p == srcY/8)
  		{
  		 	c_new=0;
			
  			for(c_orgi = ui16X; c_orgi < 128; c_orgi++)
  			{
  				CMD(ST7565_CMD_SET_PAGE | p);
				CMD(ST7565_CMD_SET_COLUMN_LOWER | (c_orgi & 0xf));
    			CMD(ST7565_CMD_SET_COLUMN_UPPER | ((c_orgi >> 4) & 0xf));
    			CMD(ST7565_CMD_RMW);
				RCVDATA(ui8DataCurPg);

				ui8DataCurPg = ui8DataCurPg << (8-srcY%8);
				ui8DataCurPg = ui8DataCurPg >> (8-srcY%8);				

				uint8_t bit=0x00;
				
				bit = buffer[(128*(p))+ c_new] >> (srcY%8);
				bit = bit << (srcY%8);

				ui8DataCurPg |= bit;

				DATA(ui8DataCurPg);

				c_new++;
  			}
  		}
		else if(p == destY/8)
		{
			c_new=0;

			for(c_orgi = ui16X; c_orgi < 128; c_orgi++)
  			{
  				CMD(ST7565_CMD_SET_PAGE | p);
				CMD(ST7565_CMD_SET_COLUMN_LOWER | (c_orgi & 0xf));
    			CMD(ST7565_CMD_SET_COLUMN_UPPER | ((c_orgi >> 4) & 0xf));
    			CMD(ST7565_CMD_RMW);
				RCVDATA(ui8DataCurPg);

				ui8DataCurPg = ui8DataCurPg >> (destY%8+1);
				ui8DataCurPg = ui8DataCurPg << (destY%8+1);

				uint8_t bit=0x00;

				bit = buffer[(128*(p))+ c_new] << (8-(destY%8+1));
				bit = bit >> (8-(destY%8+1));

				ui8DataCurPg |= bit;

				DATA(ui8DataCurPg);

				c_new++;
			}
		}
		else
		{
			CMD(ST7565_CMD_SET_PAGE | p);
			CMD(ST7565_CMD_SET_COLUMN_LOWER | (ui16X & 0xf));
    		CMD(ST7565_CMD_SET_COLUMN_UPPER | ((ui16X >> 4) & 0xf));
    		CMD(ST7565_CMD_RMW);

			for(c_new = 0; c_new < (128-ui16X); c_new++) 
    		{
      			DATA(buffer[(128*(p))+c_new]); // Column address is incremented(+1) with each read/write command.
    		}
		}
	}
}

void ST7565DrawImageSinceXY(uint16_t ui16X, uint16_t srcY, uint16_t destY, MOVEINBUFFER MoveInBufIdx)
{
	if(MoveInBufIdx == BUFFER0)
	{
		st7565DrawImgSinceXY(ui16X, srcY, destY, _st7565buffer0);
	}
	else if(MoveInBufIdx == BUFFER1)
	{
		st7565DrawImgSinceXY(ui16X, srcY, destY, _st7565buffer1);
	}
}

// Draw Image starting from left edge, the picture has to locate at right side.
void st7565DrawImgSinceXY_fromLeftEdge(uint16_t ui16X, uint16_t srcY, uint16_t destY, uint8_t *buffer)
{
	uint16_t c_orgi, c_new, p;
	uint8_t ui8DataCurPg=0;

	for(p = srcY/8; p <(destY/8+1); p++) 
  	{
  		if(p == srcY/8)
  		{
			CMD(ST7565_CMD_SET_PAGE | p);
			CMD(ST7565_CMD_SET_COLUMN_LOWER | (0x0 & 0xf));
    		CMD(ST7565_CMD_SET_COLUMN_UPPER | ((0x0 >> 4) & 0xf));
    		CMD(ST7565_CMD_RMW);
			
  			for(c_orgi = ui16X; c_orgi < 128; c_orgi++)
  			{  				
				RCVDATA(ui8DataCurPg);

				ui8DataCurPg = ui8DataCurPg << (8-srcY%8);
				ui8DataCurPg = ui8DataCurPg >> (8-srcY%8);				

				uint8_t bit=0x00;
				
				bit = buffer[(128*(p))+ c_orgi] >> (srcY%8);
				bit = bit << (srcY%8);

				ui8DataCurPg |= bit;

				DATA(ui8DataCurPg);
  			}
  		}
		else if(p == destY/8)
		{
			CMD(ST7565_CMD_SET_PAGE | p);
			CMD(ST7565_CMD_SET_COLUMN_LOWER | (0x0 & 0xf));
    		CMD(ST7565_CMD_SET_COLUMN_UPPER | ((0x0 >> 4) & 0xf));
    		CMD(ST7565_CMD_RMW);

			for(c_orgi = ui16X; c_orgi < 128; c_orgi++)
  			{
				RCVDATA(ui8DataCurPg);

				ui8DataCurPg = ui8DataCurPg >> (destY%8+1);
				ui8DataCurPg = ui8DataCurPg << (destY%8+1);

				uint8_t bit=0x00;

				bit = buffer[(128*(p))+ c_orgi] << (8-(destY%8+1));
				bit = bit >> (8-(destY%8+1));

				ui8DataCurPg |= bit;

				DATA(ui8DataCurPg);
			}
		}
		else
		{
			CMD(ST7565_CMD_SET_PAGE | p);
			CMD(ST7565_CMD_SET_COLUMN_LOWER | (0x0 & 0xf));
    		CMD(ST7565_CMD_SET_COLUMN_UPPER | ((0x0 >> 4) & 0xf));
    		CMD(ST7565_CMD_RMW);

			for(c_new = ui16X; c_new < 128; c_new++) 
    		{
      			DATA(buffer[(128*(p))+c_new]); // Column address is incremented(+1) with each read/write command.
    		}
		}
	}
}

//*****************************************************************************
//
//!	Slide Effect Base On Left Edge of LCD Panel, which handle in 
//! irregular(non-one page) screen process.
//!
//!	\param ui16X is the X coordinate of the start column of screen.
//!	\param srcY is the started Y coordinate of the slide screen.
//!	\param destY is the destination Y coordinate of the slide screen.
//!	\param MoveInBufIdx is enum number to indicate the Moving-In buffer index.
//!
//!	This function provide the slide effect of screen based on left edge of LCD panel.
//! Bitmap could be slided start from any Y-axis(irregular of one page) then move into
//!	panel from left to right.
//!
//!	\return None.
//
//*****************************************************************************
void ST7565DrawImageSinceXY_fromLeftEdge(uint16_t ui16X, uint16_t srcY, uint16_t destY, MOVEINBUFFER MoveInBufIdx)
{
	if(MoveInBufIdx == BUFFER0)
	{
		st7565DrawImgSinceXY_fromLeftEdge(ui16X, srcY, destY, _st7565buffer0);
	}
	else if(MoveInBufIdx == BUFFER1)
	{
		st7565DrawImgSinceXY_fromLeftEdge(ui16X, srcY, destY, _st7565buffer1);
	}
}

//*****************************************************************************
//
//!	Flip Right Effect in Slide Transition Based On Left Edge of LCD Panel.
//!
//!	\param bufferIn is a pointer to the "Moving-In" data for this display driver.
//!	\param bufferOut is a pointer to the "Sliding-Out" data for this display driver.
//!	\param ui16X is the X coordinate of the start column of screen.
//!
//!	This function provide the flip right effect of screen during transition slide. 
//! The 'bufferIn' has been choiced to move into LCD screen from left edge of panel.
//!	
//!	\return None.
//
//*****************************************************************************
void st7565FlipRightInTransSlide(uint8_t *bufferIn, uint8_t *bufferOut, uint16_t ui16X)
{
	uint16_t c, p;
	uint8_t pagemap[] = { 0, 1, 2, 3, 4, 5, 6, 7 };

	for(p = 0; p < 8; p++) 
  	{
  		CMD(ST7565_CMD_SET_PAGE | p);
		CMD(ST7565_CMD_SET_COLUMN_LOWER | (0x0 & 0xf));
    	CMD(ST7565_CMD_SET_COLUMN_UPPER | ((0x0 >> 4) & 0xf));
    	CMD(ST7565_CMD_RMW);

		for(c = ui16X; c < 128; c++) 
    	{    		
      		DATA(bufferIn[(128*pagemap[p])+c]); // Column address is incremented(+1) with each read/write command.		
		}	
  	
  		CMD(ST7565_CMD_SET_PAGE | p);
		CMD(ST7565_CMD_SET_COLUMN_LOWER | ((128-ui16X) & 0xf));
    	CMD(ST7565_CMD_SET_COLUMN_UPPER | (((128-ui16X) >> 4) & 0xf));

		for(c = 0; c < ui16X; c++) 
    	{
      		DATA(bufferOut[(128*pagemap[p])+c]); // Column address is incremented(+1) with each read/write command.
    	}
	}
	
}

//*****************************************************************************
//
//!	Return to Upper Level Menu with Flip Right Effect in Slide Transition.
//!
//!	\param ui16X is the X coordinate of the start column of screen.
//!	\param MoveInBufIdx is enum number to indicate the Moving-In buffer index.
//!	
//!	This function provide the Flip Right Effect when return to upper level menu via 
//! st7565FlipRightInTransSlide() sub-function. Because the new picture will move into 
//! panel from left edge, the ui16X should start operation by 127,126,...,1,0.
//!	
//! For exmaple: Virtual Keyboard slide out, Setting Menu move in.
//!				
//!        Setting Menu          LCD      Virtual Keyboard
//!   (_st7565buffer1, input)  |-----|(_st7565buffer0, output)
//!    ----------------------> |     |------------------------->
//!                            |-----|
//!
//!			// Clears the screen
//!			ST7565ClearScreen(BUFFER1); // _st7565buffer1 as input buffer
//!
//!			ST7565DrawString(0, 0, "1. LCD BackLight", Font_System5x8);   // 3x6 is UPPER CASE only
//!    		ST7565DrawString(0, 8, "2. Language Select", Font_System5x8);
//!    		ST7565DrawString(0, 16, "3. Clock Setting", Font_System5x8);
//!    		ST7565DrawString(0, 24, "4. Clear Memory", Font_System5x8);
//!			ST7565DrawString(0, 32, "5. Information", Font_System5x8);
//!			ST7565DrawString(0, 40, "6. Turn On Bluetooth", Font_System5x8);
//!
//!			uint8_t i;
//!			
//!			for(i=1; i<5; i++)
//!			{
//!				ST7565Ret2UpLvlMenu(128-i*32, BUFFER1);
//!				SysCtlDelay(SysCtlClockGet()/3/10);
//!			}
//!
//!	\return None.
//
//*****************************************************************************
void ST7565Ret2UpLvlMenu(uint16_t ui16X, MOVEINBUFFER MoveInBufIdx)
{
	if(MoveInBufIdx == BUFFER0)
	{
		st7565FlipRightInTransSlide(_st7565buffer0, _st7565buffer1, ui16X);
	}
	else if(MoveInBufIdx == BUFFER1)
	{
		st7565FlipRightInTransSlide(_st7565buffer1, _st7565buffer0, ui16X);
	}	
}

//*****************************************************************************
//
//!	Flip Left Effect in Slide Transition Based On Right Edge of LCD Panel.
//!
//!	\param bufferIn is a pointer to the "Moving-In" data for this display driver.
//!	\param bufferOut is a pointer to the "Sliding-Out" data for this display driver.
//!	\param ui16X is the X coordinate of the start column of screen.
//!
//!	This function provide the flip left effect of screen during transition slide. 
//! The 'bufferIn' has been choiced to move into LCD screen from right edge of panel.
//!	
//!	\return None.
//
//*****************************************************************************
void st7565FlipLeftInTransSlide(uint8_t *bufferIn, uint8_t *bufferOut, uint16_t ui16X)
{
	uint16_t c, p;
	uint8_t pagemap[] = { 0, 1, 2, 3, 4, 5, 6, 7 };

	for(p = 0; p < 8; p++) 
  	{
  		CMD(ST7565_CMD_SET_PAGE | p);
		CMD(ST7565_CMD_SET_COLUMN_LOWER | (0x0 & 0xf));
    	CMD(ST7565_CMD_SET_COLUMN_UPPER | ((0x0 >> 4) & 0xf));
    	CMD(ST7565_CMD_RMW);

		for(c = ui16X; c < 128; c++) 
    	{    		
      		DATA(bufferOut[(128*pagemap[p])+c]); // Column address is incremented(+1) with each read/write command.		
		}	
  	
  		CMD(ST7565_CMD_SET_PAGE | p);
		CMD(ST7565_CMD_SET_COLUMN_LOWER | ((128-ui16X) & 0xf));
    	CMD(ST7565_CMD_SET_COLUMN_UPPER | (((128-ui16X) >> 4) & 0xf));

		for(c = 0; c < ui16X; c++) 
    	{
      		DATA(bufferIn[(128*pagemap[p])+c]); // Column address is incremented(+1) with each read/write command.
    	}
	}
	
}

void st7565FlipLeftInTransSlide7page(uint8_t *bufferIn, uint8_t *bufferOut, uint16_t ui16X)
{
	uint16_t c, p;
	uint8_t pagemap[] = { 0, 1, 2, 3, 4, 5, 6, 7 };

	for(p = 0; p < 7; p++)
  	{
  		CMD(ST7565_CMD_SET_PAGE | p);
		CMD(ST7565_CMD_SET_COLUMN_LOWER | (0x0 & 0xf));
    	CMD(ST7565_CMD_SET_COLUMN_UPPER | ((0x0 >> 4) & 0xf));
    	CMD(ST7565_CMD_RMW);

		for(c = ui16X; c < 128; c++)
    	{
      		DATA(bufferOut[(128*pagemap[p])+c]); // Column address is incremented(+1) with each read/write command.
		}

  		CMD(ST7565_CMD_SET_PAGE | p);
		CMD(ST7565_CMD_SET_COLUMN_LOWER | ((128-ui16X) & 0xf));
    	CMD(ST7565_CMD_SET_COLUMN_UPPER | (((128-ui16X) >> 4) & 0xf));

		for(c = 0; c < ui16X; c++)
    	{
      		DATA(bufferIn[(128*pagemap[p])+c]); // Column address is incremented(+1) with each read/write command.
    	}
	}

}

//*****************************************************************************
//
//!	Enter to Next Level Menu with Flip Left Effect in Slide Transition.
//!
//!	\param ui16X is the X coordinate of the start column of screen.
//!	\param MoveInBufIdx is enum number to indicate the Moving-In buffer index.
//!	
//!	This function provide the Flip Left Effect when enter into next level menu via 
//! st7565FlipLeftInTransSlide() sub-function. Because the new picture will move into 
//! panel from right edge, the ui16X should start operation by 0,1,...,126,127.
//!	
//! For exmaple: Setting Menu slide out, Virtual Keyboard move in.
//!				
//!        Setting Menu          LCD      Virtual Keyboard
//!   (_st7565buffer1, Onput)  |-----|(_st7565buffer0, iutput)
//!   <----------------------  |     |<------------------------
//!                            |-----|
//!
//!			// Clears the screen//!			
//!			ST7565ClearScreen(BUFFER0); // _st7565buffer0 as input buffer
//!
//!			LcdDrawScreenBuffer(en_US, 12); // 'ABC' virtual keyboard
//!			ST7565DrawVirtualKybd(BUFFER0);
//!
//!			uint8_t i;
//!
//!			for(i=1; i<5; i++)
//!			{
//!				ST7565Ent2NextLvlMenu(i*32, BUFFER0);
//!				SysCtlDelay(SysCtlClockGet()/3/10);
//!			}
//!
//!	\return None.
//
//*****************************************************************************
void ST7565Ent2NextLvlMenu(uint16_t ui16X, MOVEINBUFFER MoveInBufIdx)
{
	if(MoveInBufIdx == BUFFER0)
	{
		st7565FlipLeftInTransSlide(_st7565buffer0, _st7565buffer1, ui16X);
	}
	else if(MoveInBufIdx == BUFFER1)
	{
		st7565FlipLeftInTransSlide(_st7565buffer1, _st7565buffer0, ui16X);
	}	
}

void ST7565Ent2NextLvlMenu7page(uint16_t ui16X, MOVEINBUFFER MoveInBufIdx)
{
	if(MoveInBufIdx == BUFFER0)
	{
		st7565FlipLeftInTransSlide7page(_st7565buffer0, _st7565buffer1, ui16X);
	}
	else if(MoveInBufIdx == BUFFER1)
	{
		st7565FlipLeftInTransSlide7page(_st7565buffer1, _st7565buffer0, ui16X);
	}
}

//*****************************************************************************
//
//! Draws a Single Graphic Character Directly onto LCD Using the Supplied Font.
//!
//!	\param x : Starting x co-ordinate
//! \param y : Starting y co-ordinate
//!	\param text : The signel character to render
//! \param font : Pointer to the FONT_DEF to use when drawing the string
//!
//! This function draw a single 8x8 size character with specified (x,y) co-ordinate.
//! It retrieve appropriate 8 columns from font data and render to LCD directly.
//!
//*****************************************************************************
void st7565draw8x8Char(uint16_t x, uint16_t y, uint8_t c, FONT_DEF font)
{
	uint8_t col, column[8] = {0,0,0,0,0,0,0,0};

	// Check if the requested character is available
  	if ((c >= font.u8FirstChar) && (c <= font.u8LastChar))
  	{
    	// Retrieve appropriate columns from font data
    	for (col = 0; col < font.u8Width; col++)
    	{
      		column[col] = font.au8FontTable[((c - font.u8FirstChar) * font.u8Width) + col];	// Get first column of appropriate character
    	}
  	}
  	else
  	{    
    	// Requested character is not available in this font ... send a space instead
    	for (col = 0; col < font.u8Width; col++)
    	{
      		column[col] = 0xFF;	// Send solid space
    	}
  	}

	//
	//	Render each column
	//
	CMD(ST7565_CMD_SET_PAGE | (y/8) );
	CMD(ST7565_CMD_SET_COLUMN_LOWER | (x & 0xf));
  	CMD(ST7565_CMD_SET_COLUMN_UPPER | ((x >> 4) & 0xf));
	CMD(ST7565_CMD_RMW);

	for(col = 0; col < 8; col++)
	{
		DATA(column[col]);
	}
	
}

/**************************************************************************/
//
//! Draws a Single 8x8 size Character Using the Supplied Font Data.
//!
//! \param x : Starting x co-ordinate
//! \param y : Starting y co-ordinate
//! \param text : The signel character to render
//! \param font : Pointer to the FONT_DEF to use when drawing the character
//!
//! The function draw a single 8x8 size character with specified (x,y)
//! co-ordinate. 
//!    
//! For example:    
//!    
//!    ST7565Draw8x8Char(x, y, "A", Font_System7x8);
//!
//! \return None.
//
/**************************************************************************/
void ST7565Draw8x8Char(uint16_t x, uint16_t y, char text, FONT_DEF font)
{	

    st7565draw8x8Char(x, y, text, font);

}

void st7565draw6x8Char(uint16_t x, uint16_t y, uint8_t c, FONT_DEF font)
{
	//uint8_t col, column[6] = {0,0,0,0,0,0};
	uint8_t col, column[8] = {0x00};
	uint8_t ui8LcdData=0;

	// Check if the requested character is available
  	if ((c >= font.u8FirstChar) && (c <= font.u8LastChar))
  	{
    	// Retrieve appropriate columns from font data
    	for (col = 0; col < font.u8Width; col++)
    	{
      		column[col] = font.au8FontTable[((c - font.u8FirstChar) * font.u8Width) + col];	// Get first column of appropriate character
    	}
  	}
  	else
  	{    
    	// Requested character is not available in this font ... send a space instead
    	for (col = 0; col < font.u8Width; col++)
    	{
      		column[col] = 0xFF;	// Send solid space
    	}
  	}

	if(y%8 == 0)
	{
		//
		//	Render each column
		//
		CMD(ST7565_CMD_SET_PAGE | (y/8) );
		CMD(ST7565_CMD_SET_COLUMN_LOWER | (x & 0xf));
  		CMD(ST7565_CMD_SET_COLUMN_UPPER | ((x >> 4) & 0xf));
		CMD(ST7565_CMD_RMW);

		for(col = 0; col < font.u8Width; col++)
		{
			DATA(column[col]);
		}
	}
	else
	{	
		//
		//	Write the character's column[0] to column[5]
		//
		for(col = 0; col < font.u8Width; col++)
		{
			//
			//  To write the upper portion character bits into the first(i.e. y/8) page.
			//
			CMD(ST7565_CMD_SET_PAGE | (y/8));
			CMD(ST7565_CMD_SET_COLUMN_LOWER | ((x+col) & 0xf));
  			CMD(ST7565_CMD_SET_COLUMN_UPPER | (((x+col) >> 4) & 0xf));
			CMD(ST7565_CMD_RMW);

			RCVDATA(ui8LcdData);

			//
			//	To keep the original data in this column.
			//
			ui8LcdData = ui8LcdData << (8-(y%8));
			ui8LcdData = ui8LcdData >> (8-(y%8));

			//
			//	To extract the 6x8 character bits in column buffer - column[col] according to y-axis,
			//	then to append into the same column buffer to write into LCD display. 	
			//
			ui8LcdData |= (column[col] << (y%8));

			//
			// Write the pixel value.
			//
			DATA(ui8LcdData);

			//
			// To write the rest portion character bits into the next page(i.e. (y/8)+1) page.
			//
			CMD(ST7565_CMD_SET_PAGE | (y/8 + 1));
			CMD(ST7565_CMD_SET_COLUMN_LOWER | ((x+col) & 0xf));
  			CMD(ST7565_CMD_SET_COLUMN_UPPER | (((x+col) >> 4) & 0xf));
			CMD(ST7565_CMD_RMW);

			RCVDATA(ui8LcdData);

			//
			//	To keep the original data in this column.
			//
			ui8LcdData = ui8LcdData >> (y%8);
			ui8LcdData = ui8LcdData << (y%8);

			//
			//	To extract the 6x8 character bits in column buffer - column[col] according to y-axis,
			//	then to append into the same column buffer to write into LCD display. 	
			//
			ui8LcdData |= (column[col] >> (8-(y%8)));

			//
			// Write the pixel value.
			//
			DATA(ui8LcdData);
		}
		
	}	
}

void ST7565Draw6x8Char(uint16_t x, uint16_t y, uint8_t text, FONT_DEF font)
{	
   	st7565draw6x8Char(x, y, text, font);
}

void st7565drawLangSelMenu(uint16_t x, uint16_t y, LANGUAGE lang, LANGMENUFONT_DEF font)
{
	uint8_t col, column[font.u8Width];
	uint8_t ui8LcdData=0;

	
    // Retrieve appropriate columns from font data
    for (col = 0; col < font.u8Width; col++)
    {
    	column[col] = font.au8FontTable[(lang * font.u8Width) + col];	// Get first column of appropriate character
    }  	

	if(y%8 == 0)
	{
		//
		//	Render each column
		//
		CMD(ST7565_CMD_SET_PAGE | (y/8) );
		CMD(ST7565_CMD_SET_COLUMN_LOWER | (x & 0xf));
  		CMD(ST7565_CMD_SET_COLUMN_UPPER | ((x >> 4) & 0xf));
		CMD(ST7565_CMD_RMW);

		for(col = 0; col < font.u8Width; col++)
		{
			DATA(column[col]);
		}
	}
	else
	{	
		//
		//	Write the character's column[0] to column[5]
		//
		for(col = 0; col < font.u8Width; col++)
		{
			//
			//  To write the upper portion character bits into the first(i.e. y/8) page.
			//
			CMD(ST7565_CMD_SET_PAGE | (y/8));
			CMD(ST7565_CMD_SET_COLUMN_LOWER | ((x+col) & 0xf));
  			CMD(ST7565_CMD_SET_COLUMN_UPPER | (((x+col) >> 4) & 0xf));
			CMD(ST7565_CMD_RMW);

			RCVDATA(ui8LcdData);

			//
			//	To keep the original data in this column.
			//
			ui8LcdData = ui8LcdData << (8-(y%8));
			ui8LcdData = ui8LcdData >> (8-(y%8));

			//
			//	To extract the 6x8 character bits in column buffer - column[col] according to y-axis,
			//	then to append into the same column buffer to write into LCD display. 	
			//
			ui8LcdData |= (column[col] << (y%8));

			//
			// Write the pixel value.
			//
			DATA(ui8LcdData);

			//
			// To write the rest portion character bits into the next page(i.e. (y/8)+1) page.
			//
			CMD(ST7565_CMD_SET_PAGE | (y/8 + 1));
			CMD(ST7565_CMD_SET_COLUMN_LOWER | ((x+col) & 0xf));
  			CMD(ST7565_CMD_SET_COLUMN_UPPER | (((x+col) >> 4) & 0xf));
			CMD(ST7565_CMD_RMW);

			RCVDATA(ui8LcdData);

			//
			//	To keep the original data in this column.
			//
			ui8LcdData = ui8LcdData >> (y%8);
			ui8LcdData = ui8LcdData << (y%8);

			//
			//	To extract the 6x8 character bits in column buffer - column[col] according to y-axis,
			//	then to append into the same column buffer to write into LCD display. 	
			//
			ui8LcdData |= (column[col] >> (8-(y%8)));

			//
			// Write the pixel value.
			//
			DATA(ui8LcdData);
		}
		
	}	
}

void ST7565DrawLangSelMenu(uint16_t x, uint16_t y, LANGUAGE lang, LANGMENUFONT_DEF font)
{
	st7565drawLangSelMenu(x, y, lang, font);
}

void st7565draw16x16Char(uint16_t x, uint16_t y, uint8_t c, FONT_DEF font)
{
	uint8_t col, column[32] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	uint8_t ui8LcdData=0;

	// Check if the requested character is available
  	if ((c >= font.u8FirstChar) && (c <= font.u8LastChar))
  	{
    	// Retrieve appropriate columns from font data
    	for (col = 0; col < (font.u8Width*2); col++)
    	{
      		column[col] = font.au8FontTable[((c - font.u8FirstChar) * (font.u8Width*2)) + col];	// Get first column of appropriate character
    	}
  	}
  	else
  	{    
    	// Requested character is not available in this font ... send a space instead
    	for (col = 0; col < font.u8Width; col++)
    	{
      		column[col] = 0xFF;	// Send solid space
    	}
  	}

	if(y%8 == 0)
	{
		//
		//	Render each column
		//
		CMD(ST7565_CMD_SET_PAGE | (y/8) );
		CMD(ST7565_CMD_SET_COLUMN_LOWER | (x & 0xf));
  		CMD(ST7565_CMD_SET_COLUMN_UPPER | ((x >> 4) & 0xf));
		CMD(ST7565_CMD_RMW);

		for(col = 0; col < font.u8Width; col++)
		{
			DATA(column[col]);
		}

		CMD(ST7565_CMD_SET_PAGE | ((y+8)/8) );
		CMD(ST7565_CMD_SET_COLUMN_LOWER | (x & 0xf));
  		CMD(ST7565_CMD_SET_COLUMN_UPPER | ((x >> 4) & 0xf));
		CMD(ST7565_CMD_RMW);

		for(col = font.u8Width; col < (font.u8Width*2); col++)
		{
			DATA(column[col]);
		}
	}
	else
	{	
		//
		//	Write the character's column[0] to column[15]
		//
		for(col = 0; col < font.u8Width; col++)
		{
			//
			//  To write the upper portion character bits into the first(i.e. y/8) page.
			//
			CMD(ST7565_CMD_SET_PAGE | (y/8));
			CMD(ST7565_CMD_SET_COLUMN_LOWER | ((x+col) & 0xf));
  			CMD(ST7565_CMD_SET_COLUMN_UPPER | (((x+col) >> 4) & 0xf));
			CMD(ST7565_CMD_RMW);

			RCVDATA(ui8LcdData);

			//
			//	To keep the original data in this column.
			//
			ui8LcdData = ui8LcdData << (8-(y%8));
			ui8LcdData = ui8LcdData >> (8-(y%8));

			//
			//	To extract the 16x16 character bits in column buffer - column[col] according to y-axis,
			//	then to append into the same column buffer to write into LCD display. 	
			//
			ui8LcdData |= (column[col] << (y%8));

			//
			// Write the pixel value.
			//
			DATA(ui8LcdData);

			//
			// To write the rest upper portion character bits into the next page(i.e. (y/8)+1) page.
			//
			CMD(ST7565_CMD_SET_PAGE | (y/8 + 1));
			CMD(ST7565_CMD_SET_COLUMN_LOWER | ((x+col) & 0xf));
  			CMD(ST7565_CMD_SET_COLUMN_UPPER | (((x+col) >> 4) & 0xf));

			//
			// Write the pixel value.
			//
			DATA((column[col] >> (8-(y%8))));
		}

		//
		// Write the character's column[16] to column[31]
		//
		for(col = font.u8Width; col < (font.u8Width*2); col++)
		{
			CMD(ST7565_CMD_SET_PAGE | (y/8 + 1));			
			CMD(ST7565_CMD_SET_COLUMN_LOWER | ((x+(col-16)) & 0xf));
			CMD(ST7565_CMD_SET_COLUMN_UPPER | (((x+(col-16)) >> 4) & 0xf));
			CMD(ST7565_CMD_RMW);

			RCVDATA(ui8LcdData);

			//
			//	To keep the original data in this column.
			//
			ui8LcdData = ui8LcdData << (8-(y%8));
			ui8LcdData = ui8LcdData >> (8-(y%8));

			//
			//	To extract the 16x16 character bits in column buffer - column[col] according to y-axis,
			//	then to append into the same column buffer to write into LCD display. 	
			//
			ui8LcdData |= (column[col] << (y%8));

			//
			// Write the pixel value.
			//
			DATA(ui8LcdData);

			//
			// To write the rest lower portion character bits into the next page(i.e. (y/8)+2) page.
			//
			CMD(ST7565_CMD_SET_PAGE | (y/8 + 2));
			CMD(ST7565_CMD_SET_COLUMN_LOWER | ((x+(col-16)) & 0xf));
  			CMD(ST7565_CMD_SET_COLUMN_UPPER | (((x+(col-16)) >> 4) & 0xf));
			CMD(ST7565_CMD_RMW);
			
			RCVDATA(ui8LcdData);

			//
			//	To keep the original data in this column.
			//
			ui8LcdData = ui8LcdData >> (y%8);
			ui8LcdData = ui8LcdData << (y%8);

			//
			//	To extract the 16x16 character bits in column buffer - column[col] according to y-axis,
			//	then to append into the same column buffer to write into LCD display. 	
			//
			ui8LcdData |= (column[col] >> (8-(y%8)));

			//
			// Write the pixel value.
			//
			DATA(ui8LcdData);			
		}
	}
	
}


void ST7565Draw16x16Char(uint16_t x, uint16_t y, uint8_t text, FONT_DEF font)
{	
   	st7565draw16x16Char(x, y, text, font);
}


/**************************************************************************/
//
//! Draws Lower Half Screen
//! 
//!	\param buffer is a pointer to the driver-specific data for this display driver.
//!	       It contains every virtual keyboard's bmp code.
//!
//! This fucntion draws lower half screen most used in virtual keyboard update.
//!
//! \return None.
//
/**************************************************************************/
void st7565DrawLowerHalfScrn(uint8_t *buffer)
{
	uint8_t c, p;	
	uint8_t pagemap[] = { 0, 1, 2, 3, 4, 5, 6, 7 };

	for(p = 4; p < 8; p++) 
	{
		CMD(ST7565_CMD_SET_PAGE | pagemap[p]);
		CMD(ST7565_CMD_SET_COLUMN_LOWER | (0x0 & 0xf));
		CMD(ST7565_CMD_SET_COLUMN_UPPER | ((0x0 >> 4) & 0xf));
		CMD(ST7565_CMD_RMW);

		// One page has 128 bytes totally. One time for writing one Segment byte, 
		// i.e. write one Column Address(c).
		for(c = 0; c < 128; c++) 
		{
			DATA(buffer[(128*pagemap[p])+c]);
		}
	}
}

/**************************************************************************/
//
//! Draws Virtual Keyboard - 'ABC','abc','123','#+='
//! 
//!	\param MoveInBufIdx is enum number to indicate the Moving-In buffer index.
//!
//! This fucntion draws virtual keyboards with specific move in buffer index.
//! It most uses BUFFER0 as virtual keyboard's image buffer. The virtual 
//! keyboard is drawn in lower half screen.
//! \return None.
//
/**************************************************************************/
void ST7565DrawVirtualKybd(MOVEINBUFFER MoveInBufIdx)
{
	if(MoveInBufIdx == BUFFER0)
	{
		st7565DrawLowerHalfScrn(_st7565buffer0);
	}
	else if(MoveInBufIdx == BUFFER1)
	{
		st7565DrawLowerHalfScrn(_st7565buffer1);
	}	
}

/**************************************************************************/
//
//! Draws Upper Half Screen.
//! 
//!	\param buffer is a pointer to the driver-specific data for this display driver.
//!        It contains user input's text content restrict in 128 characters image buffer.
//!
//! This fucntion draws text edit characters/numeric/symbols with specific 
//! move in buffer index. It most uses BUFFER1 as Text Edit's image buffer.
//! The Text Edit is drawn in upper half screen and total 128 characters distributed
//! in g_TextEditPage#0 and g_TextEditPage#1.
//!
//! \return None.
//
/**************************************************************************/
void st7565DrawUprHalfScrn(uint8_t *buffer, uint8_t page)
{
	uint8_t c, p;	

	for(p = 0; p < 4; p++) 
	{
		CMD(ST7565_CMD_SET_PAGE | p);
		CMD(ST7565_CMD_SET_COLUMN_LOWER | (0x0 & 0xf));
		CMD(ST7565_CMD_SET_COLUMN_UPPER | ((0x0 >> 4) & 0xf));
		CMD(ST7565_CMD_RMW);

		// One page has 128 bytes totally. One time for writing one Segment byte, 
		// i.e. write one Column Address(c).
		if(page == 0)
		{
			for(c = 0; c < 128; c++) 
			{
				DATA(buffer[(128*p)+c]);
			}
		}
		else // g_InfVkybdTextEditPage = 1
		{
			for(c = 0; c < 128; c++) 
			{
				DATA(buffer[(128*(p+4))+c]);
			}
		}
	}
}

/**************************************************************************/
//
//! Draws Text Edit region Based on User Input.
//! 
//!	\param MoveInBufIdx is enum number to indicate the Moving-In buffer index.
//!
//! This fucntion draws text edit characters/numeric/symbols with specific 
//! move in buffer index. It most uses BUFFER1 as Text Edit's image buffer.
//! The Text Edit is drawn in upper half screen and total 128 characters distributed
//! in g_InfVkybdTextEditPage#0 and g_InfVkybdTextEditPage#1.
//!
//! \return None.
//
/**************************************************************************/
void ST7565DrawTextEdit(MOVEINBUFFER MoveInBufIdx, uint8_t page)
{
	if(MoveInBufIdx == BUFFER0)
	{
		st7565DrawUprHalfScrn(_st7565buffer0, page);
	}
	else if(MoveInBufIdx == BUFFER1)
	{
		st7565DrawUprHalfScrn(_st7565buffer1, page);
	}	
}

/**************************************************************************/
// 
//! Draw Page Slide Up in Upeer Screen.
//!
//! \param buffer is a pointer to the driver-specific data for this display driver.
//!		   It contains user input's text content restrict in 128 characters image buffer.
//!
//! This function draws slide up effect in upper screen by 4 times graphics update:
//! (1). ImageBuffer Page1 --> LCD Page0
//!      ImageBuffer Page2 --> LCD Page1
//!		 ImageBuffer Page3 --> LCD Page2	
//!      ImageBuffer Page4 --> LCD Page3
//
//! (2). ImageBuffer Page2 --> LCD Page0
//!      ImageBuffer Page3 --> LCD Page1
//!      ImageBuffer Page4 --> LCD Page2
//!      ImageBuffer Page5 --> LCD Page3
//!
//! (3). ImageBuffer Page3 --> LCD Page0 
//!      ImageBuffer Page4 --> LCD Page1
//!      ImageBuffer Page5 --> LCD Page2
//!      ImageBuffer Page6 --> LCD Page3
//! 
//! (4). ImageBuffer Page4 --> LCD Page0
//!      ImageBuffer Page5 --> LCD Page1			
//!      ImageBuffer Page6 --> LCD Page2
//!      ImageBuffer Page7 --> LCD Page3
//!
//! \return None.
//
/**************************************************************************/
void st7565PageSlideUpInUpScrn(uint8_t *buffer)
{
	uint8_t slideIdx ,c, p;
	uint8_t pagemap[] = { 0, 1, 2, 3, 4, 5, 6, 7 };

	for(slideIdx = 1; slideIdx < 5; slideIdx++)
	{
		for(p = 0; p < 4; p++) 
		{
			CMD(ST7565_CMD_SET_PAGE | pagemap[p]);
			CMD(ST7565_CMD_SET_COLUMN_LOWER | (0x0 & 0xf));
			CMD(ST7565_CMD_SET_COLUMN_UPPER | ((0x0 >> 4) & 0xf));
			CMD(ST7565_CMD_RMW);

			// One page has 128 bytes totally. One time for writing one Segment byte, 
			// i.e. write one Column Address(c).
			for(c = 0; c < 128; c++) 
			{
				DATA(buffer[(128*pagemap[p+slideIdx])+c]);
			}			
		}

		SysCtlDelay(get_sys_clk()/3/10);
	}	
}

/**************************************************************************/
//
//! Text Edit Slide Up Effect when Input Character Number over 64 digits.
//! 
//!	\param MoveInBufIdx is enum number to indicate the Moving-In buffer index.
//!
//! This fucntion provide slide up effect for text edit when input is over 64
//! characters. It most uses BUFFER1 as Text Edit's image buffer.
//! 
//! \return None.
//
/**************************************************************************/
void ST7565TextEditSlideUp(MOVEINBUFFER MoveInBufIdx)
{
	if(MoveInBufIdx == BUFFER0)
	{
		st7565PageSlideUpInUpScrn(_st7565buffer0);
	}
	else if(MoveInBufIdx == BUFFER1)
	{
		st7565PageSlideUpInUpScrn(_st7565buffer1);
	}
}

/**************************************************************************/
// 
//! Draw Page Slide Down in Upeer Screen.
//!
//! \param buffer is a pointer to the driver-specific data for this display driver.
//!		   It contains user input's text content restrict in 128 characters image buffer.
//!
//! This function draws slide down effect in upper screen by 4 times graphics update:
//! (1). ImageBuffer Page3 --> LCD Page0
//!      ImageBuffer Page4 --> LCD Page1
//!		 ImageBuffer Page5 --> LCD Page2	
//!      ImageBuffer Page6 --> LCD Page3
//
//! (2). ImageBuffer Page2 --> LCD Page0
//!      ImageBuffer Page3 --> LCD Page1
//!      ImageBuffer Page4 --> LCD Page2
//!      ImageBuffer Page5 --> LCD Page3
//!
//! (3). ImageBuffer Page1 --> LCD Page0 
//!      ImageBuffer Page2 --> LCD Page1
//!      ImageBuffer Page3 --> LCD Page2
//!      ImageBuffer Page4 --> LCD Page3
//! 
//! (4). ImageBuffer Page0 --> LCD Page0
//!      ImageBuffer Page1 --> LCD Page1			
//!      ImageBuffer Page2 --> LCD Page2
//!      ImageBuffer Page3 --> LCD Page3
//!
//! \return None.
//
/**************************************************************************/
void st7565PageSlideDownInUpScrn(uint8_t *buffer)
{
	uint8_t slideIdx ,c, p;
	uint8_t pagemap[] = { 0, 1, 2, 3, 4, 5, 6, 7 };

	for(slideIdx = 1; slideIdx < 5; slideIdx++)
	{
		for(p = 0; p < 4; p++) 
		{
			CMD(ST7565_CMD_SET_PAGE | pagemap[p]);
			CMD(ST7565_CMD_SET_COLUMN_LOWER | (0x0 & 0xf));
			CMD(ST7565_CMD_SET_COLUMN_UPPER | ((0x0 >> 4) & 0xf));
			CMD(ST7565_CMD_RMW);

			// One page has 128 bytes totally. One time for writing one Segment byte, 
			// i.e. write one Column Address(c).
			for(c = 0; c < 128; c++) 
			{
				DATA(buffer[(128*pagemap[p+(4-slideIdx)])+c]);
			}			
		}

		SysCtlDelay(get_sys_clk()/3/10);
	}	
}

/**************************************************************************/
//
//! Text Edit Slide Down Effect when Input Character Number less than 64 digits.
//! 
//!	\param MoveInBufIdx is enum number to indicate the Moving-In buffer index.
//!
//! This fucntion provide slide down effect for text edit when input is less
//! than 64 characters. It most uses BUFFER1 as Text Edit's image buffer.
//! 
//! \return None.
//
/**************************************************************************/
void ST7565TextEditSlideDown(MOVEINBUFFER MoveInBufIdx)
{
	if(MoveInBufIdx == BUFFER0)
	{
		st7565PageSlideDownInUpScrn(_st7565buffer0);
	}
	else if(MoveInBufIdx == BUFFER1)
	{
		st7565PageSlideDownInUpScrn(_st7565buffer1);
	}
}

/**************************************************************************/
//
//! Clear 8x8 Character.
//! 
//!	\param x : Starting x co-ordinate.
//! \param y : Starting y co-ordinate.
//! \param MoveInBufIdx is enum number to indicate the Moving-In buffer index.
//!
//! This fucntion clear 8x8 character.
//! 
//! \return None.
//
/**************************************************************************/
void ST7565Clear8x8Char(uint8_t x, uint8_t y, MOVEINBUFFER MoveInBufIdx)
{
	int i;

	for(i=0; i<8; i++)
	{
		if(MoveInBufIdx == BUFFER0)
		{
			_st7565buffer0[(x+i) + (y/8)*128] = 0x00;
		}
		else if(MoveInBufIdx == BUFFER1)
		{
			_st7565buffer1[(x+i) + (y/8)*128] = 0x00;
		}
	}
}

/**************************************************************************/
//
//! Fill in One Page Data by (x,y) Coordinate into Image Buffer.
//! 
//! \param x : Starting x co-ordinate.
//! \param y : Starting y co-ordinate.
//!	\param lcdScrnBuff : is a pointer to the driver-specific data for this 
//!                      display driver. The screen data is extracted from
//!                      Flash Drive.		
//! \param MoveInBufIdx : is enum number to indicate the Moving-In buffer index.
//!
//! This fucntion fill in full one page data which read from Flash and store into
//! image buffer. The page data read from flash needs to be transcode to fit 
//! ST7565 LCM controller's electrical characteristics.
//! 
//! \return None.
//
/**************************************************************************/
void st7565DrawPage(uint16_t x, uint16_t y, uint8_t *LcdPgBuffer, MOVEINBUFFER MoveInBufIdx)
{	
	uint8_t col,row, column[128] = {0};
	
	for(col = 0; col < 128; col++)
	{
		for(row = 0; row < 8; row++)
		{
			uint8_t bit=0x00;
			bit = ( LcdPgBuffer[(col/8)+(row*16)] << (col % 8) );
			bit = (bit >> 7);
			column[col] |= (bit << row);
		}
	}
  
	// Render each column
	uint16_t xoffset, yoffset;
	for(xoffset = 0; xoffset < 128; xoffset++)
	{
		for(yoffset = 0; yoffset < 8; yoffset++)
		{
			uint8_t bit=0x00;
			bit = ( column[xoffset] << ( 8 - ( yoffset + 1 )) );
			bit = (bit >> 7);
			if( bit )
			{
				ST7565DrawPixel(x + xoffset, y + yoffset, MoveInBufIdx);
			}
		}
	}
	
}

/**************************************************************************/
//
//! Fill in Full Screen Data into Image Buffer.
//! 
//!	\param lcdScrnBuff : is a pointer to the driver-specific data for this 
//!                      display driver. The screen data is extracted from
//!                      Flash Drive.		
//! \param MoveInBufIdx : is enum number to indicate the Moving-In buffer index.
//!
//! This fucntion fill in full screen data which read from Flash and store into
//! image buffer.
//! 
//! \return None.
//
/**************************************************************************/
void ST7565DrawScreen(uint8_t *lcdScrnBuff, MOVEINBUFFER MoveInBufIdx)
{
	uint8_t ui8PgIdx;
	uint8_t ui8LcdPgBuff[128];
	uint16_t byteIdx;
	
	for (ui8PgIdx = 0; ui8PgIdx < 8; ui8PgIdx++) // ui8PgIdx = Page definition in ST7565. i.e. One screen has 8 pages (128 bytes/per page).
    {
    	for(byteIdx = 0; byteIdx < 128; byteIdx++)
		{
			// lcdScrnBuff[1024]    -> 1024 bytes for one LCD screen.
			// ui8LcdPgBuff[128]  	-> 128 bytes for one LCD page.
			// Array truncate to 128 bytes for ST7565's Page Screen update.
			ui8LcdPgBuff[byteIdx] = lcdScrnBuff[ui8PgIdx*128 + byteIdx];
		}
		
        st7565DrawPage(0, ui8PgIdx*8, ui8LcdPgBuff, MoveInBufIdx);
    }	
}

/**************************************************************************/
//
//! Fill in One Page Data by (x,y) Coordinate into Image Buffer(Invert Type).
//! 
//! \param x : Starting x co-ordinate.
//! \param y : Starting y co-ordinate.
//!	\param lcdScrnBuff : is a pointer to the driver-specific data for this 
//!                      display driver. The screen data is extracted from
//!                      Flash Drive.		
//! \param MoveInBufIdx : is enum number to indicate the Moving-In buffer index.
//!
//! This fucntion fill in full one page data which read from Flash and store into
//! image buffer. The page data read from flash needs to be transcode to fit 
//! ST7565 LCM controller's electrical characteristics.
//! 
//! \return None.
//
/**************************************************************************/
void st7565DrawPageInvertType(uint16_t x, uint16_t y, uint8_t *LcdPgBuffer, MOVEINBUFFER MoveInBufId)
{	
	uint8_t col,row, column[128] = {0};
	
	for(col = 0; col < 128; col++)
	{
		for(row = 0; row < 8; row++)
		{
			uint8_t bit=0x00;
			bit = ( LcdPgBuffer[(col/8)+row*16] << (col % 8) );
			bit = (bit >> 7);
			column[col] |= (bit << row);
		}
	}
  
	// Render each column
	uint16_t xoffset, yoffset;
	for(xoffset = 0; xoffset < 128; xoffset++)
	{
		for(yoffset = 0; yoffset < 8; yoffset++)
		{
			uint8_t bit=0x00;
			bit = ( column[xoffset] << ( 8 - ( yoffset + 1 )) );
			bit = (bit >> 7);
			if( !bit )
			{
				ST7565DrawPixel(x + xoffset, y + yoffset, MoveInBufId);
			}
		}
	}
	
}

/**************************************************************************/
//
//! Fill in Full Screen Data into Image Buffer(Invert Type).
//! 
//!	\param lcdScrnBuff : is a pointer to the driver-specific data for this 
//!                      display driver. The screen data is extracted from
//!                      Flash Drive.		
//! \param MoveInBufIdx : is enum number to indicate the Moving-In buffer index.
//!
//! This fucntion fill in full screen data which read from Flash and store into
//! image buffer.
//! 
//! \return None.
//
/**************************************************************************/
void ST7565DrawScreenInvertType(uint8_t *lcdScrnBuff, MOVEINBUFFER MoveInBufId)
{
	uint8_t ui8PgIdx;
	uint8_t ui8LcdPgBuff[128];
	uint16_t byteIdx;
	
	for (ui8PgIdx = 0; ui8PgIdx < 8; ui8PgIdx++) // ui8PgIdx = Page definition in ST7565. i.e. One screen has 8 pages (128 bytes/per page).
    {
    	for(byteIdx = 0; byteIdx < 128; byteIdx++)
		{
			// lcdScrnBuff[1024]    -> 1024 bytes for one LCD screen.
			// ui8LcdPgBuff[128]  	-> 128 bytes for one LCD page.
			// Array truncate to 128 bytes for ST7565's Page Screen update.
			ui8LcdPgBuff[byteIdx] = lcdScrnBuff[ui8PgIdx*128 + byteIdx];
		}
		
        st7565DrawPageInvertType(0, ui8PgIdx*8, ui8LcdPgBuff, MoveInBufId);
    }
}

void st7565SlideUpBootupAnimWithNewImage(uint8_t *buffer, uint8_t shiftCnt)
{
	uint8_t p, c;
	uint8_t ui8DataCurPg=0, ui8DataNexPg=0;

	for(p = 0; p < 8; p++)
	{
		if(p == 7)
		{
			CMD(ST7565_CMD_SET_PAGE | p);

			for(c=0; c<128; c++)
			{
				CMD(ST7565_CMD_SET_COLUMN_LOWER | (c & 0xf));
   				CMD(ST7565_CMD_SET_COLUMN_UPPER | ((c >> 4) & 0xf));
   				CMD(ST7565_CMD_RMW);

				RCVDATA(ui8DataCurPg);

				ui8DataCurPg = ui8DataCurPg >> 1; // Slide up one pixel at one time
						
				uint8_t bit=0x00, tmp=0x00;

				//tmp = pow(2, shiftCnt%8);
				tmp = 0x01 << ( shiftCnt%8);
				bit = (buffer[(128*(shiftCnt/8))+c] & tmp);
				bit = (bit << (8 - ((shiftCnt%8)+1))); // Shift the specific shift bit to MSB then append to page7.

				ui8DataCurPg |= bit;

				//
				// Write the pixel value.
				//
				DATA(ui8DataCurPg);
			}
		}
		else
		{
			CMD(ST7565_CMD_SET_PAGE | p);
					
			for(c=0; c<128; c++)
			{
				CMD(ST7565_CMD_SET_COLUMN_LOWER | (c & 0xf));
   				CMD(ST7565_CMD_SET_COLUMN_UPPER | ((c >> 4) & 0xf));
   				CMD(ST7565_CMD_RMW);

				RCVDATA(ui8DataCurPg);

			 	ui8DataCurPg = ui8DataCurPg >> 1; // Slide up one pixel at one time

				CMD(ST7565_CMD_SET_PAGE | (p+1));
				RCVDATA(ui8DataNexPg);

				ui8DataNexPg &= 0x01; // Slide up one pixel at one time
				ui8DataCurPg |= (ui8DataNexPg << 7); // Slide up one pixel at one time

				CMD(ST7565_CMD_SET_PAGE | (p));

				//
				// Write the pixel value.
				//
				DATA(ui8DataCurPg);
			}
		}
	}

	//
	// Shift Counter +1.
	// This bit shift counter is to calculate which bit in new image needs to move into MSB bit of page7.
	//
	//shiftCnt++;
	//}
}


void ST7565SlideUpBootupAnimation(MOVEINBUFFER MoveInBufIdx, uint8_t shiftCnt)
{
	if(MoveInBufIdx == BUFFER0)
	{
		st7565SlideUpBootupAnimWithNewImage(_st7565buffer0, shiftCnt);
	}
	else if(MoveInBufIdx == BUFFER1)
	{
		st7565SlideUpBootupAnimWithNewImage(_st7565buffer1, shiftCnt);
	}
}


void st7565SlideUpWithNewImage(int16_t srcY, int16_t destY, uint8_t *buffer)
{
	uint8_t shitPoint, p, c;
	uint8_t ui8DataCurPg=0, ui8DataNexPg=0, shiftCnt=0;
		
	for(shitPoint = srcY; shitPoint > destY; shitPoint--)
	{
		//
		// The shift point start from the non-page starting pixel. ex: COM1~COM7, or COM25~COM31...etc.
		//
		if(shitPoint%8 != 0)
		{
			for(p = shitPoint/8; p < 8; p++)
			{
				if(p == shitPoint/8)
				{
					CMD(ST7565_CMD_SET_PAGE | p);

					for(c=0; c<128; c++)
					{
						CMD(ST7565_CMD_SET_COLUMN_LOWER | (c & 0xf));
    					CMD(ST7565_CMD_SET_COLUMN_UPPER | ((c >> 4) & 0xf));
    					CMD(ST7565_CMD_RMW);

						RCVDATA(ui8DataCurPg);

						uint8_t bit=0x00;

						bit = ui8DataCurPg;
						ui8DataCurPg = ui8DataCurPg >> 1; // Slide up one pixel at one time

						if((shitPoint%8-2) >= 0)
						{
							bit = bit << (8-(shitPoint%8)+1); // Slide up one pixel at one time
							bit = bit >> (8-(shitPoint%8)+1); // Slide up one pixel at one time
							ui8DataCurPg = ui8DataCurPg >> (shitPoint%8)-1; // Slide up one pixel at one time
							ui8DataCurPg = ui8DataCurPg << (shitPoint%8)-1; // Slide up one pixel at one time
							ui8DataCurPg |= bit;

							CMD(ST7565_CMD_SET_PAGE | (p+1));
							RCVDATA(ui8DataNexPg);

							ui8DataNexPg &= 0x01; // Slide up one pixel at one time
							ui8DataCurPg |= (ui8DataNexPg << 7); // Slide up one pixel at one time
						}
						else if(shitPoint%8 == 1)
						{
							CMD(ST7565_CMD_SET_PAGE | (p+1));
							RCVDATA(ui8DataNexPg);

							ui8DataNexPg &= 0x01; // Slide up one pixel at one time
							ui8DataCurPg |= (ui8DataNexPg << 7); // Slide up one pixel at one time
						}

						CMD(ST7565_CMD_SET_PAGE | (p));

						//
						// Write the pixel value.
						//
						DATA(ui8DataCurPg);
					}
				}
				//
				// To process the last page which needs to append the new image into MSB of page7.
				//
				else if(p == 7)
				{
					CMD(ST7565_CMD_SET_PAGE | p);

					for(c=0; c<128; c++)
					{
						CMD(ST7565_CMD_SET_COLUMN_LOWER | (c & 0xf));
    					CMD(ST7565_CMD_SET_COLUMN_UPPER | ((c >> 4) & 0xf));
    					CMD(ST7565_CMD_RMW);

						RCVDATA(ui8DataCurPg);

						ui8DataCurPg = ui8DataCurPg >> 1; // Slide up one pixel at one time
						
						uint8_t bit=0x00, tmp=0x00;
						
						//tmp = pow(2, shiftCnt%8);
						tmp = 0x01 << (shiftCnt%8);
						bit = (buffer[(128*(shiftCnt/8))+c] & tmp);
						bit = (bit << (8 - ((shiftCnt%8)+1))); // Shift the specific shift bit to MSB then append to page7.

						ui8DataCurPg |= bit;

						//
						// Write the pixel value.
						//
						DATA(ui8DataCurPg);
					}
					
				}
				//
				// To process the middle of pages
				//
				else
				{
					CMD(ST7565_CMD_SET_PAGE | p);

					for(c=0; c<128; c++)
					{
						CMD(ST7565_CMD_SET_COLUMN_LOWER | (c & 0xf));
    					CMD(ST7565_CMD_SET_COLUMN_UPPER | ((c >> 4) & 0xf));
    					CMD(ST7565_CMD_RMW);

						RCVDATA(ui8DataCurPg);

						ui8DataCurPg = ui8DataCurPg >> 1; // Slide up one pixel at one time

						CMD(ST7565_CMD_SET_PAGE | (p+1));
						RCVDATA(ui8DataNexPg);

						ui8DataNexPg &= 0x01; // Slide up one pixel at one time
						ui8DataCurPg |= (ui8DataNexPg << 7); // Slide up one pixel at one time

						CMD(ST7565_CMD_SET_PAGE | (p));

						//
						// Write the pixel value.
						//
						DATA(ui8DataCurPg);
					}
				}
			}
		}
		//
		// The shift point start from the page starting pixel. ex: COM0, COM8, COM16, COM24...etc.
		// (shitPoint%8 = 0)
		//
		else
		{
			for(p = (shitPoint/8-1); p < 8; p++)
			{
				if(p == shitPoint/8-1)
				{
					CMD(ST7565_CMD_SET_PAGE | p);

					for(c=0; c<128; c++)
					{
						CMD(ST7565_CMD_SET_COLUMN_LOWER | (c & 0xf));
    					CMD(ST7565_CMD_SET_COLUMN_UPPER | ((c >> 4) & 0xf));
    					CMD(ST7565_CMD_RMW);

						RCVDATA(ui8DataCurPg);

						//ui8DataCurPg = ui8DataCurPg >> 1; // Slide up one pixel at one time

						CMD(ST7565_CMD_SET_PAGE | (p+1));
						RCVDATA(ui8DataNexPg);

						ui8DataNexPg &= 0x01; // Slide up one pixel at one time
						ui8DataCurPg |= (ui8DataNexPg << 7); // Slide up one pixel at one time

						CMD(ST7565_CMD_SET_PAGE | (p));

						//
						// Write the pixel value.
						//
						DATA(ui8DataCurPg);
					}
				}
				//
				// To process the last page which needs to append the new image into MSB of page7.
				//
				else if(p == 7)
				{
					CMD(ST7565_CMD_SET_PAGE | p);

					for(c=0; c<128; c++)
					{
						CMD(ST7565_CMD_SET_COLUMN_LOWER | (c & 0xf));
    					CMD(ST7565_CMD_SET_COLUMN_UPPER | ((c >> 4) & 0xf));
    					CMD(ST7565_CMD_RMW);

						RCVDATA(ui8DataCurPg);

						ui8DataCurPg = ui8DataCurPg >> 1; // Slide up one pixel at one time
						
						uint8_t bit=0x00, tmp=0x00;

						//tmp = pow(2, shiftCnt%8);
						tmp = 0x01 << (shiftCnt%8);
						bit = (buffer[(128*(shiftCnt/8))+c] & tmp);
						bit = (bit << (8 - ((shiftCnt%8)+1))); // Shift the specific shift bit to MSB then append to page7.

						ui8DataCurPg |= bit;

						//
						// Write the pixel value.
						//
						DATA(ui8DataCurPg);
					}
				}
				// 
				// To process the middle of pages
				//
				else
				{
					CMD(ST7565_CMD_SET_PAGE | p);
					
					for(c=0; c<128; c++)
					{
						CMD(ST7565_CMD_SET_COLUMN_LOWER | (c & 0xf));
    					CMD(ST7565_CMD_SET_COLUMN_UPPER | ((c >> 4) & 0xf));
    					CMD(ST7565_CMD_RMW);

						RCVDATA(ui8DataCurPg);

						ui8DataCurPg = ui8DataCurPg >> 1; // Slide up one pixel at one time

						CMD(ST7565_CMD_SET_PAGE | (p+1));
						RCVDATA(ui8DataNexPg);

						ui8DataNexPg &= 0x01; // Slide up one pixel at one time
						ui8DataCurPg |= (ui8DataNexPg << 7); // Slide up one pixel at one time

						CMD(ST7565_CMD_SET_PAGE | (p));

						//
						// Write the pixel value.
						//
						DATA(ui8DataCurPg);
					}
				}
			}			
		}

		//
		// Shift Counter +1.
		// This bit shift counter is to calculate which bit in new image needs to move into MSB bit of page7.
		//
		shiftCnt++;
	}
}

void ST7565SlideUp(int16_t srcY, int16_t destY, MOVEINBUFFER MoveInBufIdx)
{
	if(MoveInBufIdx == BUFFER0)
	{
		st7565SlideUpWithNewImage(srcY, destY, _st7565buffer0);
	}
	else if(MoveInBufIdx == BUFFER1)
	{
		st7565SlideUpWithNewImage(srcY, destY, _st7565buffer1);
	}
}

void st7565SlideDownWithoutNewImage(int16_t srcY, int16_t destY)
{
	uint8_t shitPoint, p, c;
	uint8_t ui8DataCurPg=0, ui8DataPrePg=0;
		
	for(shitPoint = srcY; shitPoint < destY; shitPoint++)
	{
		//
		// The shift point start from the non-Bit7 pixel. ex: COM0~COM6, or COM8~COM14...etc.
		//
		if(shitPoint%8 != 7)
		{
			for(p = 7; p >= (shitPoint/8); p--)
			{
				if(p == shitPoint/8)
				{
					CMD(ST7565_CMD_SET_PAGE | p);

					for(c=0; c<128; c++)
					{
						CMD(ST7565_CMD_SET_COLUMN_LOWER | (c & 0xf));
    					CMD(ST7565_CMD_SET_COLUMN_UPPER | ((c >> 4) & 0xf));
    					CMD(ST7565_CMD_RMW);

						RCVDATA(ui8DataCurPg);

						uint8_t bit=0x00;

						bit = ui8DataCurPg;
						ui8DataCurPg = ui8DataCurPg << 1; // Slide down one pixel at one time

						if(shitPoint%8 != 0)
						{
							//
							// Keep/extract the original data and remove other bits. To exclude/clean the starting shift point.
							//
							bit = bit << (8-(shitPoint%8)); // Slide down one pixel at one time. Clean the starting shift point.
							bit = bit >> (8-(shitPoint%8)); // Slide down one pixel at one time. Clean the starting shift point.

							//
							//	Extract shift down bits excluding the starting shift point.
							//
							ui8DataCurPg = ui8DataCurPg >> (shitPoint%8)+1; // Slide down one pixel at one time. Clean the starting shift point.
							ui8DataCurPg = ui8DataCurPg << (shitPoint%8)+1; // Slide down one pixel at one time. Clean the starting shift point.
							ui8DataCurPg |= bit;
						}
						//
						//	(shitPoint%8 = 0)
						//
						else
						{
							CMD(ST7565_CMD_SET_PAGE | (p-1));
							RCVDATA(ui8DataPrePg);

							//
							//	Clean the MSB bit of previous page. 
							//	(PS: Very Important!! If not add these column settings, the pixel will be mess up(right shift one pixel). 
							//		This is due to in RMW mode, display write cmd increases(+1) the column address automatically.)
							//
							CMD(ST7565_CMD_SET_COLUMN_LOWER | (c & 0xf));
    						CMD(ST7565_CMD_SET_COLUMN_UPPER | ((c >> 4) & 0xf));
    						CMD(ST7565_CMD_RMW);
							DATA(ui8DataPrePg & (~0x80));

							//
							//	Append the MSB bit of previous page to LSB bit of current page.
							//
							ui8DataPrePg &= 0x80; // Slide down one pixel at one time
							ui8DataCurPg |= (ui8DataPrePg >> 7); // Slide down one pixel at one time
							
						}

						CMD(ST7565_CMD_SET_PAGE | (p));

						//
						// Write the pixel value.
						// (PS: Very Important!! If not add these column settings, the pixel will be mess up(right shift one pixel). 
						//		This is due to in RMW mode, display write cmd increases(+1) the column address automatically.)
						//
						CMD(ST7565_CMD_SET_COLUMN_LOWER | (c & 0xf));
    					CMD(ST7565_CMD_SET_COLUMN_UPPER | ((c >> 4) & 0xf));
    					CMD(ST7565_CMD_RMW);						
						DATA(ui8DataCurPg);
					}
				}
				//
				// To process the rest of pages
				//
				else
				{
					CMD(ST7565_CMD_SET_PAGE | p);

					for(c=0; c<128; c++)
					{
						CMD(ST7565_CMD_SET_COLUMN_LOWER | (c & 0xf));
    					CMD(ST7565_CMD_SET_COLUMN_UPPER | ((c >> 4) & 0xf));
    					CMD(ST7565_CMD_RMW);

						RCVDATA(ui8DataCurPg);

						ui8DataCurPg = ui8DataCurPg << 1; // Slide down one pixel at one time

						CMD(ST7565_CMD_SET_PAGE | (p-1));
						RCVDATA(ui8DataPrePg);

						//
						//	Clean the MSB bit of previous page. 
						//
						//DATA(ui8DataPrePg & (~0x80));

						//
						//	Append the MSB bit of previous page to LSB bit of current page.
						//
						ui8DataPrePg &= 0x80; // Slide down one pixel at one time
						ui8DataCurPg |= (ui8DataPrePg >> 7); // Slide down one pixel at one time

						CMD(ST7565_CMD_SET_PAGE | (p));

						//
						// Write the pixel value.
						//
						DATA(ui8DataCurPg);
					}
				}
			}
		}
		//
		// The shift point start from the Bit7 pixel. ex: COM7, COM15, COM23, COM31...etc.
		// (shitPoint%8 = 7)
		//
		else
		{
			for(p = 7; p >= (shitPoint/8+1); p--)
			{
				if(p == shitPoint/8+1)
				{
					CMD(ST7565_CMD_SET_PAGE | p);

					for(c=0; c<128; c++)
					{
						CMD(ST7565_CMD_SET_COLUMN_LOWER | (c & 0xf));
    					CMD(ST7565_CMD_SET_COLUMN_UPPER | ((c >> 4) & 0xf));
    					CMD(ST7565_CMD_RMW);

						RCVDATA(ui8DataCurPg);

						ui8DataCurPg = ui8DataCurPg << 1; // Slide down one pixel at one time

						CMD(ST7565_CMD_SET_PAGE | (p-1));
						RCVDATA(ui8DataPrePg);

						//
						//	Clean the MSB bit of previous page. 
						//	(PS: Very Important!! If not add these column settings, the pixel will be mess up(right shift one pixel). 
						//		 This is due to in RMW mode, display write cmd increases(+1) the column address automatically.)
						//	
						CMD(ST7565_CMD_SET_COLUMN_LOWER | (c & 0xf));
    					CMD(ST7565_CMD_SET_COLUMN_UPPER | ((c >> 4) & 0xf));
    					CMD(ST7565_CMD_RMW);
						DATA(ui8DataPrePg & (~0x80));

						//
						//	Append the MSB bit of previous page to LSB bit of current page.
						//
						ui8DataPrePg &= 0x80; // Slide down one pixel at one time
						ui8DataCurPg |= (ui8DataPrePg >> 7); // Slide down one pixel at one time

						CMD(ST7565_CMD_SET_PAGE | (p));

						//
						// Write the pixel value.
						//	(PS: Very Important!! If not add these column settings, the pixel will be mess up(right shift one pixel). 
						//		 This is due to in RMW mode, display write cmd increases(+1) the column address automatically.)
						//
						CMD(ST7565_CMD_SET_COLUMN_LOWER | (c & 0xf));
    					CMD(ST7565_CMD_SET_COLUMN_UPPER | ((c >> 4) & 0xf));
    					CMD(ST7565_CMD_RMW);
						DATA(ui8DataCurPg);
					}
				}
				// 
				// To process the rest of pages
				//
				else
				{
					CMD(ST7565_CMD_SET_PAGE | p);
					
					for(c=0; c<128; c++)
					{
						CMD(ST7565_CMD_SET_COLUMN_LOWER | (c & 0xf));
    					CMD(ST7565_CMD_SET_COLUMN_UPPER | ((c >> 4) & 0xf));
    					CMD(ST7565_CMD_RMW);

						RCVDATA(ui8DataCurPg);

						ui8DataCurPg = ui8DataCurPg << 1; // Slide down one pixel at one time

						CMD(ST7565_CMD_SET_PAGE | (p-1));
						RCVDATA(ui8DataPrePg);

						//
						//	Clean the MSB bit of previous page. 
						//
						//DATA(ui8DataPrePg & (~0x80));

						//
						//	Append the MSB bit of previous page to LSB bit of current page.
						//
						ui8DataPrePg &= 0x80; // Slide down one pixel at one time
						ui8DataCurPg |= (ui8DataPrePg >> 7); // Slide down one pixel at one time

						CMD(ST7565_CMD_SET_PAGE | (p));

						//
						// Write the pixel value.
						//
						DATA(ui8DataCurPg);
					}
				}
			}			
		}		
	}
}


void ST7565SlideDown(int16_t srcY, int16_t destY)
{
	st7565SlideDownWithoutNewImage(srcY, destY);
}

//*****************************************************************************
//
//! Translates a 24-bit RGB color to a display driver-specific color.
//!
//! \param pvDisplayData is a pointer to the driver-specific data for this
//! display driver.
//! \param ui32Value is the 24-bit RGB color.  The least-significant byte is the
//! blue channel, the next byte is the green channel, and the third byte is the
//! red channel.
//!
//! This function translates a 24-bit RGB color into a value that can be
//! written into the display's frame buffer in order to reproduce that color,
//! or the closest possible approximation of that color.
//!
//! \return Returns the display-driver specific color.
//
//*****************************************************************************
uint32_t
ST7565V128x64x1ColorTranslate(void *pvDisplayData, uint32_t ui32Value)
{
    //
    // Translate from a 24-bit RGB color to a 3-3-2 RGB color.
    //
    return(ui32Value);
}

//*****************************************************************************
//
//! Flushes any cached drawing operations.
//!
//! \param pvDisplayData is a pointer to the driver-specific data for this
//! display driver.
//!
//! This functions flushes any cached drawing operations to the display.  This
//! is useful when a local frame buffer is used for drawing operations, and the
//! flush would copy the local frame buffer to the display.  Since no memory
//! based frame buffer is used for this driver, the flush is a no operation.
//!
//! \return None.
//
//*****************************************************************************
void
ST7565V128x64x1Flush(void *pvDisplayData)
{
    //
    // There is nothing to be done.
    //
}

//*****************************************************************************
//
//! The display structure that describes the driver for the ST7565V
//! controller.
//
//*****************************************************************************
tDisplay g_sST7565V128x64x1 =
{
    sizeof(tDisplay),
    0,
    128,
    64,
    ST7565V128x64x1PixelDraw,
    ST7565V128x64x1PixelDrawMultiple,
    ST7565V128x64x1LineDrawH,
    ST7565V128x64x1LineDrawV,
    ST7565V128x64x1RectFill,
    ST7565V128x64x1ColorTranslate,
    ST7565V128x64x1Flush
};

uint8_t BackLight_duty = 0;

/**************************************************************************/
/*! 
    @brief Sets the display brightness
*/
/**************************************************************************/
void ST7565SetBrightness(uint8_t val)
{
  CMD(ST7565_CMD_SET_VOLUME_FIRST);
  CMD(ST7565_CMD_SET_VOLUME_SECOND | (val & 0x3f));
}

void ST7565_V5_internal_resistor_ratio(uint8_t val)
{
	CMD(ST7565_CMD_SET_RESISTOR_RATIO | val);	// Set LCD operating voltage
	delay_ms(10);
}

/**************************************************************************/
/*! 
    @brief Clears the screen
*/
/**************************************************************************/
void ST7565ClearScreen(MOVEINBUFFER MoveInBufIdx) 
{
	if(MoveInBufIdx == BUFFER0)
  	{
		memset(&_st7565buffer0, 0x00, 128*64/8);
	}
	else if(MoveInBufIdx == BUFFER1)
	{
		memset(&_st7565buffer1, 0x00, 128*64/8);
	}
}

/**************************************************************************/
/*! 
    @brief Renders the contents of the pixel buffer on the LCD
*/
/**************************************************************************/
void ST7565Refresh(MOVEINBUFFER MoveInBufIdx)
{
	if(MoveInBufIdx == BUFFER0)
  	{
		st7565WriteBuffer(_st7565buffer0);
	}
	else if(MoveInBufIdx == BUFFER1)
	{
		st7565WriteBuffer(_st7565buffer1);
	}
}

/**************************************************************************/
/*! 
    @brief Draws a single pixel in image buffer

    @param[in]  x
                The x position (0..127)
    @param[in]  y
                The y position (0..63)
*/
/**************************************************************************/
void ST7565DrawPixel(uint8_t x, uint8_t y, MOVEINBUFFER MoveInBufIdx) 
{
  if ((x >= 128) || (y >= 64))
    return;

  // x is which column
  //_st7565buffer[x+ (y/8)*128] |= (1 << (7-(y%8)));
  if(MoveInBufIdx == BUFFER0)
  {
  	_st7565buffer0[x+ (y/8)*128] |= (1 << (y%8));
  }
  else if(MoveInBufIdx == BUFFER1)
  {
  	_st7565buffer1[x+ (y/8)*128] |= (1 << (y%8));
  }
}

/**************************************************************************/
/*! 
    @brief Clears a single pixel in image buffer

    @param[in]  x
                The x position (0..127)
    @param[in]  y
                The y position (0..63)
*/
/**************************************************************************/
void ST7565ClearPixel(uint8_t x, uint8_t y)
{
  if ((x >= 128) || (y >= 64))
    return;

  // x is which column
  _st7565buffer0[x+ (y/8)*128] &= ~(1 << (7-(y%8)));
}

/**************************************************************************/
/*! 
    @brief Gets the value (1 or 0) of the specified pixel from the buffer

    @param[in]  x
                The x position (0..127)
    @param[in]  y
                The y position (0..63)

    @return     1 if the pixel is enabled, 0 if disabled
*/
/**************************************************************************/
uint8_t ST7565GetPixel(uint8_t x, uint8_t y)
{
  if ((x >= 128) || (y >= 64)) return 0;
  return _st7565buffer0[x+ (y/8)*128] & (1 << (7-(y%8)));
}

/**************************************************************************/
/*!
    @brief  Draws a string using the supplied font data.

    @param[in]  x
                Starting x co-ordinate
    @param[in]  y
                Starting y co-ordinate
    @param[in]  text
                The string to render
    @param[in]  font
                Pointer to the FONT_DEF to use when drawing the string

    @section Example

    @code 

    #include "drivers/displays/bitmap/st7565/st7565.h"
    #include "drivers/displays/smallfonts.h"
    
    // Configure the pins and initialise the LCD screen
    st7565Init();

    // Enable the backlight
    st7565BLEnable();

    // Render some text on the screen with different fonts
    st7565DrawString(1, 1, "3X6 SYSTEM", Font_System3x6);   // 3x6 is UPPER CASE only
    st7565DrawString(1, 10, "5x8 System", Font_System5x8);
    st7565DrawString(1, 20, "7x8 System", Font_System7x8);

    // Refresh the screen to see the results
    st7565Refresh();

    @endcode
*/
/**************************************************************************/
void ST7565DrawString(uint16_t x, uint16_t y, char* text, FONT_DEF font, MOVEINBUFFER MoveInBufIdx)
{
  uint8_t l;
  for (l = 0; l < strlen(text); l++)
  {
    st7565DrawChar(x + (l * (font.u8Width + 1)), y, text[l], font, MoveInBufIdx);
  }
}

/**************************************************************************/
/*!
    @brief  Shifts the contents of the frame buffer up the specified
            number of pixels

    @param[in]  height
                The number of pixels to shift the frame buffer up, leaving
                a blank space at the bottom of the frame buffer x pixels
                high

    @section Example

    @code 

    #include "drivers/displays/bitmap/st7565/st7565.h"
    #include "drivers/displays/smallfonts.h"
    
    // Configure the pins and initialise the LCD screen
    st7565Init();

    // Enable the backlight
    st7565BLEnable();

    // Continually write some text, scrolling upward one line each time
    while (1)
    {
      // Shift the buffer up 8 pixels (adjust for font-height)
      st7565ShiftFrameBuffer(8);
      // Render some text on the screen with different fonts
      st7565DrawString(1, 56, "INSERT TEXT HERE", Font_System3x6);   // 3x6 is UPPER CASE only
      // Refresh the screen to see the results
      st7565Refresh();    
      // Wait a bit before writing the next line
      systickDelay(1000);
    }

    @endcode
*/
/**************************************************************************/
void ST7565ShiftFrameBuffer( uint8_t height )
{
  if (height == 0) return;
  if (height >= 64)
  {
    // Clear the entire frame buffer
    ST7565ClearScreen(BUFFER0);
    return;
  }

  // This is horribly inefficient, but at least easy to understand
  // In a production environment, this should be significantly optimised

  uint8_t y, x;
  for (y = 0; y < 64; y++)
  {
    for (x = 0; x < 128; x++)
    {
      if (63 - y > height)
      {
        // Shift height from further ahead in the buffer
        ST7565GetPixel(x, y + height) ? ST7565DrawPixel(x, y, BUFFER0) : ST7565ClearPixel(x, y);
      }
      else
      {
        // Clear the entire line
        ST7565ClearPixel(x, y);
      }
    }
  }
}

//
//
// *buffer: data source
// ui8Col: 0 ~ 15
//
void st7565DrawUprHalfScrnCrankVAnimation(const uint8_t *buffer, uint8_t ui8Col)
{
	uint8_t c, p;
	uint8_t pagemap[] = { 0, 1, 2, 3, 4, 5, 6, 7 };

	for(p = 0; p < 4; p++)
	{
		CMD(ST7565_CMD_SET_PAGE | pagemap[p]);
		CMD(ST7565_CMD_SET_COLUMN_LOWER | (0x0 & 0xf));
		CMD(ST7565_CMD_SET_COLUMN_UPPER | ((0x0 >> 4) & 0xf));
		CMD(ST7565_CMD_RMW);

		// One page has 128 bytes totally. One time for writing one Segment byte,
		// i.e. write one Column Address(c).

		for(c = 0; c < (8*(ui8Col + 1)); c++)
		{
			DATA(buffer[(128*pagemap[p])+c]);
		}
	}
}

void st7565DrawUprHalfScrnCrankVAnimationSpace(uint8_t ui8Col)
{
	uint8_t c, p;
	uint8_t pagemap[] = { 0, 1, 2, 3, 4, 5, 6, 7 };

	for(p = 0; p < 4; p++)
	{
		CMD(ST7565_CMD_SET_PAGE | pagemap[p]);
		CMD(ST7565_CMD_SET_COLUMN_LOWER | (0x0 & 0xf));
		CMD(ST7565_CMD_SET_COLUMN_UPPER | ((0x0 >> 4) & 0xf));
		CMD(ST7565_CMD_RMW);

		// One page has 128 bytes totally. One time for writing one Segment byte,
		// i.e. write one Column Address(c).

		for(c = 0; c < (8*(ui8Col + 1)); c++)
		{
			DATA(0x00);
		}
	}
}


/*******************************************************************************
* Function Name : SetDisplayBuf
* Description   : Set D0 ~ DF to text.
* Argument      : ui8Src: Point to data source
*               : D0 ~ DF: data
* return value  : None
*******************************************************************************/
void SetDisplayBuf(uint8_t* ui8Src, uint8_t ui8D0, uint8_t ui8D1, uint8_t ui8D2, uint8_t ui8D3, uint8_t ui8D4, uint8_t ui8D5, uint8_t ui8D6, uint8_t ui8D7, uint8_t ui8D8, uint8_t ui8D9, uint8_t ui8DA, uint8_t ui8DB, uint8_t ui8DC, uint8_t ui8DD, uint8_t ui8DE, uint8_t ui8DF)
{
	ui8Src[0] = ui8D0;
	ui8Src[1] = ui8D1;
	ui8Src[2] = ui8D2;
	ui8Src[3] = ui8D3;
	ui8Src[4] = ui8D4;
	ui8Src[5] = ui8D5;
	ui8Src[6] = ui8D6;
	ui8Src[7] = ui8D7;
	ui8Src[8] = ui8D8;
	ui8Src[9] = ui8D9;
	ui8Src[10] = ui8DA;
	ui8Src[11] = ui8DB;
	ui8Src[12] = ui8DC;
	ui8Src[13] = ui8DD;
	ui8Src[14] = ui8DE;
	ui8Src[15] = ui8DF;
	ui8Src[16] = '\0';
}

/*******************************************************************************
* Function Name : ST7565DrawStringx16
* Description   : Draws a Series Character Using the Supplied Font Data.
* Argument      : x: Starting x co-ordinate; y: Starting y co-ordinate
*               : text: Characters to render
*		: Pointer to the FONT_DEF to use when drawing the character
* return value  : None
*******************************************************************************/
void ST7565DrawStringx16(uint16_t x, uint16_t y, uint8_t* text, FONT_DEF font)
{
	uint8_t l;

	for (l = 0; l < strlen((char *)text); l++)
  	{
		ST7565Draw6x8Char(x + (l *  (1 + font.u8Width)), y, text[l], font);
  	}
}

void LCD_String(uint16_t x, uint16_t y, char* text){
    while(*text){
        ST7565Draw16x16Char(x, y, (*text), Font_System8x16);
        x += Font_System8x16.u8Width;
        text++;
    }
}

void LCD_String_ext(uint16_t y, char* text, uint8_t align){
    uint8_t leng = strlen(text);
    uint16_t start_x;
    switch(align){
    case Align_Right:
        start_x = 128-leng*8;
        break;
    case Align_Left:
        start_x = 0;
        break;
    case Align_Central:
        start_x = 64 - (leng*4);
        break;
    default:
        while(1){}
    }
    LCD_String(start_x, y, text);
}

void LCD_String_ext2(uint16_t y, char* text, uint8_t align, uint8_t space){
    uint8_t leng = strlen(text);
    uint16_t start_x;
    switch(align){
    case Align_Right:
        start_x = (128 - (leng*8) - (space*8));
        break;
    case Align_Left:
        start_x = (space*8);
        break;
    case Align_Central:
        start_x = 64 - (leng*4);
        break;
    default:
        while(1){}
    }
    LCD_String(start_x, y, text);
}

void LCD_Select_Arrow(uint32_t y){
    ST7565Draw16x16Char(128-8, y, 0x7f, Font_System8x16);
}

void LCD_Arrow(uint8_t arr_flag){
    uint8_t start = 128-8;
    if(arr_flag&Arrow_enter){
        ST7565Draw16x16Char(start, 0, 0x82, Font_System8x16);
        start -= 8;
    }
    if(arr_flag&Arrow_RL){
        ST7565Draw16x16Char(start, 0, 0x81, Font_System8x16);
        start -= 8;
    }
    if(arr_flag&Arrow_up){
        ST7565Draw16x16Char(start, 0, 0x80, Font_System8x16);
        start -= 8;
    }
    if(arr_flag&ARROW_UP_DOWN){
        ST7565Draw16x16Char(start, 0, '}', Font_System8x16);
        start -= 8;
    }
}

void ST7565printf5x8(const XY_DISPLAY* display){
	uint8_t l;
	uint8_t sLen = strlen(display->string);
	for(l = 0; l < sLen; l++){
		ST7565Draw6x8Char(display->x + l*6, display->y, display->string[l], Font_System5x8);
	}
}

void LCD_SOC_SOH(uint16_t percent){
    uint32_t i;
    uint16_t start = percent*(127-26-19-2)/100 + 19;

    ST7565V128x64x1LineDrawH(0, 18, 127-26, 40, 1);

    ST7565V128x64x1LineDrawV(0, 18, 40, 47, 1);
    ST7565V128x64x1LineDrawV(0, 19, 40, 47, 1);
    ST7565V128x64x1LineDrawV(0, 20, 40, 47, 1);
    ST7565V128x64x1LineDrawV(0, 127-26, 40, 47, 1);
    ST7565V128x64x1LineDrawV(0, 127-27, 40, 47, 1);
    ST7565V128x64x1LineDrawV(0, 127-28, 40, 47, 1);

    for(i = 4; i > 0 ; i--){
        ST7565V128x64x1LineDrawH(0, start - i +1, start + i, 40-i, 1);
    }

    LCD_String_ext(48, "0        100", Align_Central);
}




//
//
// Backlight Color Setting
//
//
void ST7650BacklightIOConfig(void)
{
	//Backlight IO config.
	ROM_SysCtlPeripheralEnable(LCD_BACKLIGHT_PORT);
	ROM_GPIOPinTypeGPIOOutput(LCD_BACKLIGHT_BASE, LCD_BACKLIGHT_R | LCD_BACKLIGHT_G | LCD_BACKLIGHT_B | LCD_BACKLIGHT_Y);
}

void ST7650BacklightInit(void)
{
	//ST7650Yellow();
	ST7650Blue();
	//ST7650Green();
	//ST7650Red();
}

void ST7650Yellow(void)
{
	light_color = yellow;
	ROM_GPIOPinWrite(LCD_BACKLIGHT_BASE, LCD_BACKLIGHT_Y, 0xFF);
	ROM_GPIOPinWrite(LCD_BACKLIGHT_BASE, LCD_BACKLIGHT_R | LCD_BACKLIGHT_G | LCD_BACKLIGHT_B, 0x00);
	PWMDutySet(BackLight_duty*0.8);
}

void ST7650Blue(void)
{
	light_color = blue;
	ROM_GPIOPinWrite(LCD_BACKLIGHT_BASE, LCD_BACKLIGHT_B, 0xFF);
	ROM_GPIOPinWrite(LCD_BACKLIGHT_BASE, LCD_BACKLIGHT_R | LCD_BACKLIGHT_G | LCD_BACKLIGHT_Y, 0x00);
	PWMDutySet(BackLight_duty);
}

void ST7650Green(void)
{
	light_color = green;
	ROM_GPIOPinWrite(LCD_BACKLIGHT_BASE, LCD_BACKLIGHT_G, 0xFF);
	ROM_GPIOPinWrite(LCD_BACKLIGHT_BASE, LCD_BACKLIGHT_R | LCD_BACKLIGHT_B | LCD_BACKLIGHT_Y, 0x00);
	PWMDutySet(BackLight_duty*0.60);
}

void ST7650Red(void)
{
	light_color = red;
	ROM_GPIOPinWrite(LCD_BACKLIGHT_BASE, LCD_BACKLIGHT_R, 0xFF);
	ROM_GPIOPinWrite(LCD_BACKLIGHT_BASE, LCD_BACKLIGHT_G | LCD_BACKLIGHT_B | LCD_BACKLIGHT_Y, 0x00);
	PWMDutySet(BackLight_duty*0.40);
}

void ST7650Purple(void)
{
	ROM_GPIOPinWrite(LCD_BACKLIGHT_BASE, LCD_BACKLIGHT_B | LCD_BACKLIGHT_Y, 0xFF);
	ROM_GPIOPinWrite(LCD_BACKLIGHT_BASE, LCD_BACKLIGHT_R | LCD_BACKLIGHT_G, 0x00);
	PWMDutySet(BackLight_duty*0.7);
}

void ST7650Purple_blink(bool ON_OFF)
{
	if(ON_OFF){
		ST7650Purple();
	}
	else{// recover backlight color
		switch(light_color){
			case yellow: ST7650Yellow(); break;
			case green: ST7650Green(); break;
			case red: ST7650Red(); break;
			case blue:
			default:
				ST7650Blue();
				break;
		}
	}
}

void ST7650BacklightSet(uint8_t duty){
	BackLight_duty = duty;
	BackLight_duty = BackLight_duty*4/10;
	PWMDutySet(BackLight_duty);
}

