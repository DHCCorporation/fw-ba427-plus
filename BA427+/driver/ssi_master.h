//*****************************************************************************
//
// Source File: ssi_master.h
// Description: Prototype for the function to configure SSI0 in SPI master
//              mode.
//
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 06/24/15     William.H      Created file.
// 10/05/15     William.H      Check in BT2020 project which branched from LCDFontMaker(Pre-BT2020) by modifed related GPIOs.
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/driver/ssi_master.h#5 $
// $DateTime: 2017/09/22 10:27:40 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#ifndef __SSI_MASTER_H__
#define __SSI_MASTER_H__

#include <stdbool.h>
#include <stdint.h>
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "inc/hw_memmap.h"

//*****************************************************************************
//
//  SSI Peripherals and I/O Pin Definition
//
//  Below defines the following SSI peripherals and I/O signals.  You must
//  review these and change as needed for your own board:
//   - SSI1 peripheral
//   - GPIO Port B peripheral (for SSI1 CLK/FSS pins)
//   - GPIO Port E peripheral (for SSI1 Tx/Rx pins)
//   - SSI1Clk - PB5
//   - SSI1Fss - PB4
//   - SSI1Rx  - PE5
//   - SSI1Tx  - PE4
//
//*****************************************************************************

/* SSI port */
#define FLASH_SSI_MODULE_SYSCTL_PERIPH			SYSCTL_PERIPH_SSI1
#define FLASH_SSI_PORT_CLK_SYSCTL_PERIPH		SYSCTL_PERIPH_GPIOB
#define FLASH_SSI_PORT_CLK_BASE					GPIO_PORTB_BASE

#define FLASH_SSI_PORT_DATA_SYSCTL_PERIPH		SYSCTL_PERIPH_GPIOE
#define FLASH_SSI_PORT_DATA_BASE				GPIO_PORTE_BASE

#define FLASH_SSI_BASE							SSI1_BASE

/* GPIO for SSI pins */
/* CLK pin */
#define FLASH_SSI_CLK_PORT_BASE					GPIO_PORTB_BASE
#define FLASH_SSI_CLK_PIN						GPIO_PIN_5
#define FLASH_SSI_CLK_MUX_SEL					GPIO_PB5_SSI1CLK

/* RX pin */
#define FLASH_SSI_RX_PORT_BASE					GPIO_PORTE_BASE
#define FLASH_SSI_RX_PIN						GPIO_PIN_5			//SSI1XDAT1 <-> PE5 (SSI1RX in Legacy SSI Mode).
#define FLASH_SSI_RX_MUX_SEL					GPIO_PE5_SSI1XDAT1

/* TX pin */
#define FLASH_SSI_TX_PORT_BASE					GPIO_PORTE_BASE
#define FLASH_SSI_TX_PIN						GPIO_PIN_4			//SSI1XDAT0 <-> PE4 (SSI1TX in Legacy SSI Mode).
#define FLASH_SSI_TX_MUX_SEL					GPIO_PE4_SSI1XDAT0

/* CS pin */
#define FLASH_SSI_CS_SYSCTL_PERIPH				SYSCTL_PERIPH_GPIOB
#define FLASH_SSI_CS_PORT_BASE					GPIO_PORTB_BASE
#define FLASH_SSI_CS_PIN						GPIO_PIN_4

#define FLASH_SSI_CS_PORT_BASE2					GPIO_PORTQ_BASE
#define FLASH_SSI_CS_PIN2						GPIO_PIN_0

/* Results of Disk Functions */
typedef enum {
	FLSSSI_WRITE = 0,			/* 0: MCU Write Data to Flash   */
	FLSSSI_READ					/* 1: MCU Read Data from Flash */
} FLSSSIMODE;


//*****************************************************************************
//
// Functions exported from ssi_master.c
//
//*****************************************************************************
extern void FlsSSIIntRxHandler(void);
extern void FlsSSIIOConfig(void);
extern void FlsSSISetMode(FLSSSIMODE mode);
extern void FlsSSIHwInit(void);

#endif // __SSI_MASTER_H__
