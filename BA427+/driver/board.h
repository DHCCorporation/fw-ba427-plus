/*
 * board.h
 *
 *  Created on: 2020�~2��27��
 *      Author: henry.yang
 */

#ifndef DRIVER_BOARD_H_
#define DRIVER_BOARD_H_

#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include "inc/hw_adc.h"
#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/sysctl.h"
#include "driverlib/adc.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"

typedef enum{
	hw_v00 = 0,
	hw_v01,
	hw_known = 0xFF,
}HWVer_t;
	
/*
 * Hardware version definition
 */
#define ADC_HWVer_V00 250//1+33k
#define ADC_HWVer_V01 500//2+33k


//*****************************************************************************
//
// - Hardware configuration
//
//*****************************************************************************
#define ADC_HWVER_PERIPHERAL		SYSCTL_PERIPH_GPIOE
#define ADC_HWVER_BASE				GPIO_PORTE_BASE
#define ADC_HWVER_PIN				GPIO_PIN_1
#define ADC_HWVER_CH				ADC_CTL_CH2

HWVer_t get_hardware_version(void);
uint8_t hardware_version_initial(void);


#endif /* DRIVER_BOARD_H_ */
