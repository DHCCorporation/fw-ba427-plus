//*****************************************************************************
//
// Source File: s35190a_clk.h
// Description: Prototype of the function to Read/Write S35190A Clock Drive.
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 11/10/15     Vincnet.T      Created file.
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/driver/s35190a_clk.h#5 $
// $DateTime: 2017/09/22 10:27:40 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#ifndef _S35190A_CLK_H_
#define _S35190A_CLK_H_


#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include "inc/hw_gpio.h"
#include "inc/hw_i2c.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/i2c.h"
#include "driverlib/pin_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/rom.h"


#define StatusReg1_Read  0x61	//Status register 1 access(Read)
#define StatusReg1_Write 0x60	//Status register 1 access(Write)
#define StatusReg2_Read  0x63	//Status register 1 access(Read)
#define StatusReg2_Write 0x62	//Status register 1 access(Write)
#define RealTime_Read    0x65  	//Status register 2 access(Read)
#define RealTime_Write   0x64	//Status register 2 access(Write)

//*******************************
//    S35190A Clock IC Driver
//*******************************

//**************
//   Data Bus
//**************
//-CS
#define S35190A_CS_PORT		SYSCTL_PERIPH_GPIOQ
#define S35190A_CS_BASE		GPIO_PORTQ_BASE
#define S35190A_CS_PIN		GPIO_PIN_4
//-CS High/Low
#define S35190A_CS_H	ROM_GPIOPinWrite(S35190A_CS_BASE, S35190A_CS_PIN, 0xFF);
#define S35190A_CS_L	ROM_GPIOPinWrite(S35190A_CS_BASE, S35190A_CS_PIN, 0x00);

//-SCK
//-SIO
#define S35190A_PORT		SYSCTL_PERIPH_GPIOL
#define S35190A_BASE		GPIO_PORTL_BASE
#define S35190A_SIO_PIN		GPIO_PIN_0
#define S35190A_SCK_PIN		GPIO_PIN_1

#define S35190A_MODULE_PORT	SYSCTL_PERIPH_I2C2
#define S35190A_MODULE_BASE	I2C2_BASE
#define S35190A_SIO			GPIO_PL0_I2C2SDA
#define S35190A_SCK			GPIO_PL1_I2C2SCL

//-SIO High/Low
#define S35190A_SIO_H	ROM_GPIOPinWrite(S35190A_BASE, S35190A_SIO_PIN, 0xFF);
#define S35190A_SIO_L	ROM_GPIOPinWrite(S35190A_BASE, S35190A_SIO_PIN, 0x00);

//-SCK High/Low
#define S35190A_SCK_H	ROM_GPIOPinWrite(S35190A_BASE, S35190A_SCK_PIN, 0xFF);
#define S35190A_SCK_L	ROM_GPIOPinWrite(S35190A_BASE, S35190A_SCK_PIN, 0x00);


//Function Declaraction
void CLK_S35190AIOConfig(void);
void CLK_S35190AHWInit(void);
void CLK_S35190ASend(uint8_t ui8Addr, uint8_t *pui8DatBuf, uint8_t ui8DatLen);
void CLK_S35190ARead(uint8_t ui8Addr, uint8_t *pui8DatBuf, uint8_t ui8DatLen);
void CLK_S35190AWriteByte(uint8_t ui8Byte);
uint8_t CLK_S35190AReadByte(void);
#endif /* _S35190A_CLK_H_ */
