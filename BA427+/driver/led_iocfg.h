//*****************************************************************************
//
// Source File: led_iocfg.h
// Description: temp board led control
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 07/27/21     David.W        Created file.
//
// ============================================================================
//
// Copyright (c) 2021 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#ifndef DRIVER_LED_IOCFG_H_
#define DRIVER_LED_IOCFG_H_

#include "inc/hw_memmap.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"

//*****************************************************************************
//
//  temp board LED ON/OFF I/O Pin Definition
//
//
//*****************************************************************************
#define TEMP_led_PERIPHERAL         SYSCTL_PERIPH_GPIOP
#define TEMP_led_BASE               GPIO_PORTP_BASE
#define TEMP_led_pin                GPIO_PIN_1


#define TEMP_led_ON               ROM_GPIOPinWrite(TEMP_led_BASE, TEMP_led_pin, 0XFF)
#define TEMP_led_OFF              ROM_GPIOPinWrite(TEMP_led_BASE, TEMP_led_pin, 0x00)

extern void Temp_LED_HwInit(void);


#endif /* DRIVER_LED_IOCFG_H_ */
