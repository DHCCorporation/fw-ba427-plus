//*****************************************************************************
//
// Source File: ssi_master.c
// Description: Function to configure the SSI Module/Peripherals/IOs nad Frame
//              format.
//
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 06/24/15     William.H      Created file.
// 10/05/15     William.H      Check in BT2020 project which branched from LCDFontMaker(Pre-BT2020) by modifed related GPIOs.
// 09/11/18     Henry.Y        Use system driver interface to replace global variable.
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/driver/ssi_master.c#5 $
// $DateTime: 2017/09/22 10:27:40 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#include <stdbool.h>
#include <stdint.h>
#include "driverlib/ssi.h"
#include "driverlib/rom.h"
#include "uartstdio.h"
#include "ssi_master.h"
#include "delaydrv.h"




//*****************************************************************************
//
// This is interrupt handler when SSI interrupt occurs.
//
//*****************************************************************************
void FlsSSIIntRxHandler(void)
{
	unsigned long ulInts;
	//
	// Read the asserted interrupt sources.
	//
	ulInts = SSIIntStatus(FLASH_SSI_BASE, true);
	SSIIntClear(FLASH_SSI_BASE, ulInts);

	if ( ulInts & SSI_RXFF)
	{
		//
		// Clear the asserted interrupt sources.
		//
		SSIIntClear(FLASH_SSI_BASE, ulInts);
		SSIIntDisable(FLASH_SSI_BASE, SSI_RXFF | SSI_RXTO | SSI_RXOR);

		UARTprintf("RX FIFO half full or more!!:\n  ");
	}
}


//*****************************************************************************
//
// Initialize  the SSI port and pins needed to talk to the flash.
//
//*****************************************************************************
void FlsSSIIOConfig(void)
{
	//
	// The SSI1 peripheral must be enabled for use.
	// Enable the peripherals used to drive the Flash on SSI.
	//
	SysCtlPeripheralEnable(FLASH_SSI_MODULE_SYSCTL_PERIPH);

	//
	// SSI1 is used with PortB[5:4] for SSI1CLK/SSI1FSS.
	//
	SysCtlPeripheralEnable(FLASH_SSI_PORT_CLK_SYSCTL_PERIPH);

	//
	// SSI1 is used with PortE[5:4] for SSI1Tx/SSI1Rx.
	//
	SysCtlPeripheralEnable(FLASH_SSI_PORT_DATA_SYSCTL_PERIPH);

	//
	// Configure the pin muxing for SSI1 functions on port B5[SSI1Clk], E5[SSI1Rx], and E4[SSI1Tx].
	// 
	GPIOPinConfigure(FLASH_SSI_CLK_MUX_SEL); // PB5 -> SSI1Clk
	GPIOPinConfigure(FLASH_SSI_RX_MUX_SEL);  // PE5 -> SSI1Rx
	GPIOPinConfigure(FLASH_SSI_TX_MUX_SEL);  // PE4 -> SSI1Tx

	//
	// The port B4[FSS (CS)] signal is directly driven to ensure that we can hold it low through a
	// complete transaction with the Flash.
	//
	GPIOPinTypeGPIOOutput(FLASH_SSI_CS_PORT_BASE, FLASH_SSI_CS_PIN); // PB4 -> SSI1Fss 
	GPIOPadConfigSet(FLASH_SSI_CS_PORT_BASE, FLASH_SSI_CS_PIN, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD);
	GPIOPinWrite(FLASH_SSI_CS_PORT_BASE, FLASH_SSI_CS_PIN, FLASH_SSI_CS_PIN); // Ensure CS is held high.

	//
	// Configure the GPIO settings for the SSI pins.  This function also gives
	// control of these pins to the SSI hardware. Consult the data sheet to
	// see which functions are allocated per pin.
	// The pins are assigned as follows:
	//      PB5 - SSI1CLK
	//      <PB4 - SSI1Fss>
	//      PE5 - SSI1Rx
	//      PE4 - SSI1Tx
	GPIOPinTypeSSI(FLASH_SSI_PORT_CLK_BASE, FLASH_SSI_CLK_PIN);
	GPIOPinTypeSSI(FLASH_SSI_PORT_DATA_BASE, FLASH_SSI_RX_PIN | FLASH_SSI_TX_PIN);
	
}

//*****************************************************************************
//
// The Freescale SPI interface is a four-wire interface where the SSInFss signal 
// behaves as a slave select. The main feature of the Freescale SPI format is 
// that the inactive state and phase of the SSInClk signal are programmable
// through the SPO and SPH bits in the SSICR0 control register.
//
// @ SPO Clock Polarity
//   0 - A steady state Low value is placed on the SSInClk pin.
//   1 - A steady state High value is placed on the SSInClk pin when data is 
//         not being transferred.
// @ SPH Phase Control Bit
//   0 - Data is captured on the first clock edge transition. (@ rising edge)
//   1 - Data is captured on the second clock edge transition.(@ falling edge)
//
// (Refer to tm4c1233h6pz.pdf datashhet page.945/page.955)
//
// #define SSI_FRF_MOTO_MODE_0     0x00000000
// 									Moto fmt, polarity(SPO) = 0, phase(SPH) = 0
// #define SSI_FRF_MOTO_MODE_1     0x00000002  
// 									Moto fmt, polarity(SPO) = 0, phase(SPH) = 1
//*****************************************************************************
void FlsSSISetMode(FLSSSIMODE mode)
{	
	uint32_t ui32buf[32];

	//
	// Disable the SSI module.
	//	

	SSIDisable(FLASH_SSI_BASE);
	
	switch(mode)
	{	
		//
		// Configure and enable the SSI port for SPI master mode.  Use SSI0,
		// system clock supply, idle clock level low and active low clock in
		// freescale SPI mode, master mode, 1MHz SSI frequency, and 8-bit data.
		// SSI_FRF_MOTO_MODE_0:
		// 	- SPO Clock Polarity(SPO) = 0;
		//  - SPH Phase Control Bit(SPH) = 0;
		//
		case FLSSSI_WRITE:
			SSIConfigSetExpClk(FLASH_SSI_BASE, get_sys_clk(), SSI_FRF_MOTO_MODE_0,
						SSI_MODE_MASTER, 20000000, 8);	//ui32BitRate = 200000	==> 20000000
			break;
		//
		// Configure and enable the SSI port for SPI master mode.  Use SSI0,
		// system clock supply, idle clock level low and active low clock in
		// freescale SPI mode, master mode, 1MHz SSI frequency, and 8-bit data.
		// SSI_FRF_MOTO_MODE_1:
		// 	- SPO Clock Polarity(SPO) = 0;
		//  - SPH Phase Control Bit(SPH) = 1;
		//	
		case FLSSSI_READ:
			SSIConfigSetExpClk(FLASH_SSI_BASE, get_sys_clk(), SSI_FRF_MOTO_MODE_1,
						SSI_MODE_MASTER, 20000000, 8); // ui32BitRate = 200000 ==> 20000000

			//
			// Enables individual SSI interrupt sources and Registers an interrupt handler
			// for the synchronous serial interface.
			// (Refer to tm4c1233h6pz.pdf page.963 - SSI Interrupt Mask (SSIIM))
			//
			// Remove SSI Int Enable & Register was due to resolve the FaultISR issue. But the root cause is Stack Overflow due to stack size is too small!
			//SSIIntEnable(FLASH_SSI_BASE, SSI_RXFF | SSI_RXTO | SSI_RXOR);
			//SSIIntRegister(FLASH_SSI_BASE, FlsSSIIntRxHandler);
			break;
		default:
			SSIConfigSetExpClk(FLASH_SSI_BASE, get_sys_clk(), SSI_FRF_MOTO_MODE_0,
						SSI_MODE_MASTER, 20000000, 8); // ui32BitRate = 200000 ==> 20000000
			break;
	}

	while(SSIDataGetNonBlocking(FLASH_SSI_BASE, &ui32buf[0])); // Really important!!! If not add this line, Read data will be delayed(shifted) by 4 bytes!
	
	//
	// Enable the SSI module.
	//
	SSIEnable(FLASH_SSI_BASE);	
	
}


void FlsSSIHwInit(void)
{
	uint32_t pui32DataRx[32];	
    
	FlsSSIIOConfig();
	
	FlsSSISetMode(FLSSSI_WRITE);	

	//
	// Read any residual data from the SSI port.  This makes sure the receive
	// FIFOs are empty, so we don't read any unwanted junk.  This is done here
	// because the SPI SSI mode is full-duplex, which allows you to send and
	// receive at the same time.  The SSIDataGetNonBlocking function returns
	// "true" when data was returned, and "false" when no data was returned.
	// The "non-blocking" function checks if there is any data in the receive
	// FIFO and does not "hang" if there isn't.
	//
	while(SSIDataGetNonBlocking(FLASH_SSI_BASE, &pui32DataRx[0]));	
	
}
