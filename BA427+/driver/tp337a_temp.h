//*****************************************************************************
//
// Source File: tp337a_temp.h
// Description: Prototype of the function to Read/Write TP-337A thermistor Drive.
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 12/01/15     Vincent.T      Created file.
// 05/27/16     Vincent.T      1. Add -5V pin definition
// 03/20/19     Henry.Y        +5V/-5V always on.
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/driver/tp337a_temp.h#5 $
// $DateTime: 2017/09/22 10:27:40 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#ifndef _TP337A_TEMP_H_
#define _TP337A_TEMP_H_

#include <stdbool.h>
#include <stdint.h>
#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "inc/hw_sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/sysctl.h"
#include "delaydrv.h"

//****************************************
//     Temperature Sensor: TP-337A
//****************************************

//GPIO define
//******************************
//IO
//TMEP_SWITCH: PJ0
//******************************
//AD
//TEMPERATURE(BODY):   PD5(AIN6)
//TEMPERATURE(OBJECT): PD4(AIN7)
//******************************
#define TEMP_PORT			SYSCTL_PERIPH_GPIOJ
#define TEMP_BASE			GPIO_PORTJ_BASE
#define TEMP_SWITCH			GPIO_PIN_0

#define TEMP_SENSOR_PORT	SYSCTL_PERIPH_GPIOD
#define TEMP_SENSOR_BASE	GPIO_PORTD_BASE
#define TEMP_BODY			GPIO_PIN_5
#define TEMP_BODY_AD_CH		ADC_CTL_CH7
#define TEMP_OBJ			GPIO_PIN_4
#define TEMP_OBJ_AD_CH		ADC_CTL_CH6

// -5V controller
#define TEMP_NEG5V_PORT	SYSCTL_PERIPH_GPION
#define TEMP_NEG5V_BASE	GPIO_PORTN_BASE
#define TEMP_NEG5V_PIN	GPIO_PIN_3
#define TEMP_NEG5V_PIN_ON	ROM_GPIOPinWrite(TEMP_NEG5V_BASE, TEMP_NEG5V_PIN, 0x00); delay_ms(2);
//#define TEMP_NEG5V_PIN_OFF	ROM_GPIOPinWrite(TEMP_NEG5V_BASE, TEMP_NEG5V_PIN, 0xFF);
#define TEMP_NEG5V_PIN_OFF	ROM_GPIOPinWrite(TEMP_NEG5V_BASE, TEMP_NEG5V_PIN, 0x00); // always on



//TEMP_SWITCH
#define TEMP_SWITCH_ON	ROM_GPIOPinWrite(TEMP_BASE, TEMP_SWITCH, 0xFF);
//#define TEMP_SWITCH_OFF	ROM_GPIOPinWrite(TEMP_BASE, TEMP_SWITCH, 0x00);
#define TEMP_SWITCH_OFF	ROM_GPIOPinWrite(TEMP_BASE, TEMP_SWITCH, 0xFF); // always on


//Function Declaraction
void TP337AIOConfig(void);
void TP337AHWInit(void);
uint16_t TP337ABodyTempAD(uint8_t ui8SampleNum);
uint16_t TP337AObjTempAD(uint8_t ui8SampleNum);


#endif /* _TP337A_TEMP_H_ */
