//*****************************************************************************
//
// Source File: s35190a_clk.c
// Description: Prototype of the function to Read/Write S35190A Clock Drive.
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 11/10/15     Vincnet.T      Created file.
// 09/11/18     Henry.Y        Use system driver interface to replace global variable.
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/driver/s35190a_clk.c#5 $
// $DateTime: 2017/09/22 10:27:40 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#include "s35190a_clk.h"
#include "delaydrv.h"




void CLK_S35190AIOConfig(void)
{
	//CS
	ROM_SysCtlPeripheralEnable(S35190A_CS_PORT);
	ROM_GPIOPinTypeGPIOOutput(S35190A_CS_BASE, S35190A_CS_PIN);

	//-SCK, SIO
	ROM_SysCtlPeripheralEnable(S35190A_PORT);
	ROM_GPIOPinTypeGPIOOutput(S35190A_BASE, S35190A_SIO_PIN | S35190A_SCK_PIN);

	//**************************************************
	//               Hardware Register Access
	//                    SIO: Open-Drain
	//**************************************************
	HWREG(GPIO_PORTL_BASE + GPIO_O_LOCK) |= (GPIO_PIN_0);
	HWREG(GPIO_PORTL_BASE + GPIO_O_ODR) |= (GPIO_PIN_0);
	HWREG(GPIO_PORTL_BASE + GPIO_O_PUR) &= ~(GPIO_PIN_0);
	HWREG(GPIO_PORTL_BASE + GPIO_O_DEN) &= ~(GPIO_PIN_0);
	HWREG(GPIO_PORTL_BASE + GPIO_O_PDR) &= ~(GPIO_PIN_0);

	S35190A_CS_L;
	S35190A_SCK_L;
}

void CLK_S35190AHWInit(void)
{
	uint8_t ui8DatBuf = 0;

	CLK_S35190AIOConfig();
	//Read Reg1
	CLK_S35190ARead(StatusReg1_Read, &ui8DatBuf, 1);

	//Check POC, BLD
	if((ui8DatBuf & 0x01) | (ui8DatBuf & 0x02))
	{
		//*******************************************
		//               Config. reg1
		//Set reset, 24H, SCO, SC1 1111 0000 -> 0xF0
		//*******************************************
		ui8DatBuf = 0xF0;
		CLK_S35190ASend(StatusReg1_Write, &ui8DatBuf, 1);
		//*******************************************
		//              Config. reg2
		//                0000 0000
		//*******************************************
		ui8DatBuf = 0x00;
		CLK_S35190ASend(StatusReg2_Write, &ui8DatBuf, 1);
	}
}

void CLK_S35190ASend(uint8_t ui8Addr, uint8_t *pui8DatBuf, uint8_t ui8DatLen)
{
	uint8_t ui8LoopIdx = 0;

	S35190A_CS_H;
	//Set Address, 0110 xxxx
	CLK_S35190AWriteByte(ui8Addr);
	while(ui8DatLen != 0)
	{
		CLK_S35190AWriteByte(pui8DatBuf[ui8LoopIdx]);
		ui8DatLen--;
		ui8LoopIdx++;
	}
	S35190A_CS_L;
}

void CLK_S35190ARead(uint8_t ui8Addr, uint8_t *pui8DatBuf, uint8_t ui8DatLen)
{
	uint8_t ui8LoopIdx = 0;

	S35190A_CS_H;
	//Set Address, 0110 xxxx
	CLK_S35190AWriteByte(ui8Addr);
	while(ui8DatLen != 0)
	{
		pui8DatBuf[ui8LoopIdx] = CLK_S35190AReadByte();
		ui8LoopIdx++;
		ui8DatLen--;
	}
	S35190A_CS_L;
}

void CLK_S35190AWriteByte(uint8_t ui8Byte)
{
	uint8_t ui8LoopIdx = 0;
	uint8_t ui8DatBuf = 0;
	//Output Direction
	ROM_GPIOPinTypeGPIOOutput(S35190A_BASE, S35190A_SIO_PIN);

	for(ui8LoopIdx = 0; ui8LoopIdx < 8; ui8LoopIdx++)
	{
		//******************
		//     SIO H/L
		//******************
		ui8DatBuf = ((ui8Byte >> (7- ui8LoopIdx)) & 0x01);
		if(ui8DatBuf >= 1)
		{
			S35190A_SIO_H;
		}
		else
		{
			S35190A_SIO_L;
		}
		//*****************
		//  -CLK Control
		//   L -> H -> L
		//*****************
		S35190A_SCK_H;
		ROM_SysCtlDelay(get_sys_clk() / 30000);
		S35190A_SCK_L;
		ROM_SysCtlDelay(get_sys_clk() / 30000);
	}
}

uint8_t CLK_S35190AReadByte(void)
{
	uint8_t ui8LoopIdx = 0;
	uint8_t ui8DatBuf = 0;
	uint8_t ui8Byte = 0;
	//Input Direction
	ROM_GPIOPinTypeGPIOInput(S35190A_BASE, S35190A_SIO_PIN);

	for(ui8LoopIdx = 0; ui8LoopIdx < 8; ui8LoopIdx++)
	{
		//**************
		//  Reset Data
		//**************
		ui8DatBuf = 0;
		//*************
		//  -CLK Low
		//*************
		S35190A_SCK_L;
		S35190A_SCK_H;
		ROM_SysCtlDelay(get_sys_clk() / 30000);
		ui8DatBuf = ROM_GPIOPinRead(S35190A_BASE, S35190A_SIO_PIN);
		ui8DatBuf = ui8DatBuf & 0x01;
		// H -> L
		ui8DatBuf = (ui8DatBuf << (7- ui8LoopIdx));
		ui8Byte = ui8Byte | ui8DatBuf;
		ROM_SysCtlDelay(get_sys_clk() / 30000);
		S35190A_SCK_L;
		ROM_SysCtlDelay(get_sys_clk() / 30000);
	}
	return ui8Byte;
}
