//*****************************************************************************
//
// Source File: delaydrv.h
// Description: printer function.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 02/21/18     Jerry.W	       Created file.
// =============================================================================
//
//
// $Header: //depot/Tester/pb/service.h#0 $
// $DateTime: 2017/09/26 17:08:19 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#ifndef DRIVER_DELAYDRV_H_
#define DRIVER_DELAYDRV_H_

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include "inc/hw_types.h"
#include "driverlib/rom.h"
#include "driverlib/sysctl.h"

void system_clock_init();

uint32_t get_sys_clk();

void delay_ms(float ms);

void delay_us(float us);



#endif /* DRIVER_DELAYDRV_H_ */
