//*****************************************************************************
//
// Source File: scanner_driver.h
// Description: Scanner hardware definition.
//
//
// Edit History:
//
// when 		who 		   what, where, why  
// -------- 	----------	   ------------------------------------------------
// 12/27/17 	Henry.Y 	   Create file.
// 09/11/18     Henry.Y        Add power switch pin definition.
// 02/24/20     Henry.Y        Scanner control modification.
// =============================================================================
//
//
// $Header$
// $DateTime$
// $Author$
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#ifndef DRIVER_SCANNER_DRIVER_H_
#define DRIVER_SCANNER_DRIVER_H_

#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "driverlib/sysctl.h"
#include "driverlib/uart.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "uartstdio.h"
#include "board.h"



//*****************************************************************************
//
//  Bluetooth UART Peripherals and I/O Pin Definition
//
//  Below defines the following UART peripherals and I/O signals.  You must
//  review these and change as needed for your own board:
//   - UART peripheral
//   - GPIO Port A peripheral (for UART pins)
//   - U0RX - PA0
//   - U0TX - PA1
//
//*****************************************************************************

/* UART port */
#define SCANNER_UART_MODULE_SYSCTL_PERIPH	SYSCTL_PERIPH_UART0
#define SCANNER_UART_PORT_SYSCTL_PERIPH	SYSCTL_PERIPH_GPIOA
#define SCANNER_UART_PORT_BASE			GPIO_PORTA_BASE
#define SCANNER_UART_BASE				UART0_BASE

#define U0RX_PIN					GPIO_PIN_0
#define U0RX_MUX_SEL				GPIO_PA0_U0RX

#define U0TX_PIN					GPIO_PIN_1
#define U0TX_MUX_SEL				GPIO_PA1_U0TX

/* Baud rate */
#define UART_BAUDRATE_SCANNER		115200

/* Data format for the port (number of data bits, number of stop bits, and parity). */
#define SCANNER_UART_CONFIG			(UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE)

/* UART6 interrupt */
#define SCANNER_UART_INT			INT_UART0


//*****************************************************************************
//
// power switch/PP0
//
//*****************************************************************************
#define SCANNER_POWER_PORT_SYSCTL_PERIPH SYSCTL_PERIPH_GPIOP
#define SCANNER_POWER_PORT_BASE GPIO_PORTP_BASE
#define SCANNER_POWER_PIN GPIO_PIN_0
#define SCANNER_P_ON ROM_GPIOPinWrite(SCANNER_POWER_PORT_BASE, SCANNER_POWER_PIN, 0x00);
#define SCANNER_P_OFF ROM_GPIOPinWrite(SCANNER_POWER_PORT_BASE, SCANNER_POWER_PIN, 0xFF);


//*****************************************************************************
//
// Beeper out/PJ1
//
//*****************************************************************************
#define SCANNER_BUZZER_PORT_SYSCTL_PERIPH SYSCTL_PERIPH_GPIOJ
#define SCANNER_BUZZER_PORT_BASE GPIO_PORTJ_BASE
#define SCANNER_BUZZER_PIN GPIO_PIN_1
#define BUZZER_ON ROM_GPIOPinWrite(SCANNER_BUZZER_PORT_BASE, SCANNER_BUZZER_PIN, 0xFF);
#define BUZZER_OFF ROM_GPIOPinWrite(SCANNER_BUZZER_PORT_BASE, SCANNER_BUZZER_PIN, 0x00);


//*****************************************************************************
//
//	Trigger/PB3
//
//*****************************************************************************
//
// TODO: Trigger pin definition
//
#define SCANNER_TRIGGER_PORT_SYSCTL_PERIPH SYSCTL_PERIPH_GPIOB
#define SCANNER_TRIGGER_PORT_BASE GPIO_PORTB_BASE
#define SCANNER_TRIGGER_PIN GPIO_PIN_3
#define TRIGGER_ON ROM_GPIOPinWrite(SCANNER_TRIGGER_PORT_BASE, SCANNER_TRIGGER_PIN, 0x00);
#define TRIGGER_OFF ROM_GPIOPinWrite(SCANNER_TRIGGER_PORT_BASE, SCANNER_TRIGGER_PIN, 0xFF);


void SCANNERHwInit(void);
void scanner_switch(uint8_t on_off);


#endif /* DRIVER_SCANNER_DRIVER_H_ */
