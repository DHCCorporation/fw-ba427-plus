//*****************************************************************************
//
// Source File: 25lc512_epr.c
// Description: Functions to Read/Write/Chip Erase/Deep Power Down of 25LC512
//              EEPROM IC Drive.
//
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 10/14/15     Vincent.T      Created file.
// 09/11/18     Henry.Y        Use system driver interface to replace global variable.
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/driver/25lc512_epr.c#5 $
// $DateTime: 2017/09/22 10:27:40 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************


#include "25lc512_epr.h"
#include <stdbool.h>
#include <stdint.h>
#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "driverlib/rom.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/ssi.h"
#include "delaydrv.h"
//External variables


void EPR_25LC512IOConfig(void)
{
	/************************/
	/* VARIABLES DEFINITION */
	/************************/
	// SSI1 MODULE
	ROM_SysCtlPeripheralEnable(EPR_25LC512_MOUDLE_PORT);
	ROM_SysCtlPeripheralEnable(EPR_25LC512_PORT_LAT);
	ROM_SysCtlPeripheralEnable(EPR_25LC512_PORT_B);
	ROM_SysCtlPeripheralEnable(EPR_25LC512_PORT_E);
	//SSI1 -FSS High
	ROM_GPIOPinTypeGPIOOutput(EPR_25LC512_BASE_B, EPR_25LC512_FSS_PIN);

	// -CS -> MANUAL
	ROM_SysCtlPeripheralEnable(EPR_25LC512_PORT_LAT);
	ROM_GPIOPinTypeGPIOOutput(EPR_25LC512_BASE_LAT, EPR_25LC512_CS_PIN);

	ROM_GPIOPinConfigure(EPR_25LC512_SCK);
	ROM_GPIOPinConfigure(EPR_25LC512_SO);
	ROM_GPIOPinConfigure(EPR_25LC512_SI);
	//Configure the GPIO settings for the SSI pins.
	//-CS	->	PQ0
	// SCK	->	PB5
	// SO	->	PE5
	// SI	->	PE4
	ROM_GPIOPinTypeSSI(EPR_25LC512_BASE_B, EPR_25LC512_SCK_PIN);
	ROM_GPIOPinTypeSSI(EPR_25LC512_BASE_E, EPR_25LC512_SI_PIN | EPR_25LC512_SO_PIN);
}

void EPR_25LC512HWInit(void)
{
	//GPIO Setting
	EPR_25LC512IOConfig();
	//Disable SSI1 Module
	ROM_SSIDisable(EPR_25LC512_MODULE_BASE);
	//-CS High to disable Ext. Epr. IC
	ROM_GPIOPinWrite(EPR_25LC512_BASE_LAT, EPR_25LC512_CS_PIN, 0xFF);
	//SSI1 -FSS High
	ROM_GPIOPinWrite(EPR_25LC512_BASE_B, EPR_25LC512_FSS_PIN, 0xFF);
	//Set Mode
	EPR_25LC512ModeSel(EPR_25LC512_WRITE);
	while(ROM_SSIBusy(EPR_25LC512_MODULE_BASE));
	//Enable SSI1 Module
	ROM_SSIEnable(EPR_25LC512_MODULE_BASE);

}

void EPR_25LC512ModeSel(uint8_t ui8Mode)
{
	uint32_t ui32Buf[32];
	ROM_SSIDisable(EPR_25LC512_MODULE_BASE);
	switch(ui8Mode)
	{
	case EPR_25LC512_WRITE:
		ROM_SSIConfigSetExpClk(EPR_25LC512_MODULE_BASE, get_sys_clk(), SSI_FRF_MOTO_MODE_0, SSI_MODE_MASTER, 2000000, 8);
		break;
	case EPR_25LC512_READ:
		ROM_SSIConfigSetExpClk(EPR_25LC512_MODULE_BASE, get_sys_clk(), SSI_FRF_MOTO_MODE_1, SSI_MODE_MASTER, 2000000, 8);
		break;
	default:
		//Trap MCU
		//ERROR
		while(1);
	}
	ROM_SSIEnable(EPR_25LC512_MODULE_BASE);
	while(ROM_SSIDataGetNonBlocking(EPR_25LC512_MODULE_BASE, &ui32Buf[0]));
	while(ROM_SSIBusy(EPR_25LC512_MODULE_BASE));
}

void EPR_25LC512WriteByte(uint8_t *ui8DataBuff, uint8_t ui8DataLen)
{
	uint8_t ui8LoopIndex = 0;
	for(ui8LoopIndex = 0; ui8LoopIndex < ui8DataLen; ui8LoopIndex++)
	{
		//Send Data
		ROM_SSIDataPut(EPR_25LC512_MODULE_BASE, ui8DataBuff[ui8LoopIndex]);
		while(ROM_SSIBusy(EPR_25LC512_MODULE_BASE));
	}
}

void EPR_25LC512ReadByte(uint8_t *ui8DataBuff, uint8_t ui8DataLen)
{
	uint8_t ui8LoopIndex = 0;
	uint32_t ui8Data = 0;
	for(ui8LoopIndex = 0; ui8LoopIndex < ui8DataLen; ui8LoopIndex++)
	{
		//Send data to get data
		ROM_SSIDataPut(EPR_25LC512_MODULE_BASE, 0x00);
		//Read data from FIFO
		ROM_SSIDataGet(EPR_25LC512_MODULE_BASE, &ui8Data);
		ui8DataBuff[ui8LoopIndex] = (uint8_t )ui8Data;
		while(ROM_SSIBusy(EPR_25LC512_MODULE_BASE));
	}
}
