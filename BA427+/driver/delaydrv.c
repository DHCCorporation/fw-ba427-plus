//*****************************************************************************
//
// Source File: delaydrv.c
// Description: printer function.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 02/21/18     Jerry.W	       Created file.
// =============================================================================
//
//
// $Header: //depot/Tester/pb/service.h#0 $
// $DateTime: 2017/09/26 17:08:19 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************


#include "delaydrv.h"

static uint32_t system_clk;

void system_clock_init(){
	system_clk = ROM_SysCtlClockFreqSet((SYSCTL_XTAL_16MHZ |
				SYSCTL_OSC_MAIN |
				SYSCTL_USE_PLL |
			SYSCTL_CFG_VCO_480), 50000000);
}
			
uint32_t get_sys_clk(){
	return system_clk;
}

void delay_ms(float ms){
	uint32_t temp;

	if(ms){
		temp = (system_clk*ms)/1000/3;
	SysCtlDelay(temp);
	}
}

void delay_us(float us){
	uint32_t temp;

	if(us){
		temp = (system_clk*us)/1000000/3;
	SysCtlDelay(temp);
	}
}
