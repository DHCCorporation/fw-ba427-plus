//*****************************************************************************
//
// Source File: a25L032_fls.h
// Description: Prototype of the function to Read/Write A25L032 Flash Drive.
//
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 06/24/15     William.H      Created file.
// 10/05/15     William.H      Check in BT2020 project which branched from LCDFontMaker(Pre-BT2020) by modifed related GPIOs.
// 09/11/18     Henry.Y        Add set_flash_channel.
// 11/19/18     Jerry          add A25L032BusyCheck() function.
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/driver/a25L032_fls.h#5 $
// $DateTime: 2017/09/22 10:27:40 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#ifndef __A25L032_FLS_H__
#define __A25L032_FLS_H__

//*****************************************************************************
//
// Functions exported from a25L032_fls.c
//
//*****************************************************************************
extern void A25L032WaitBusy(void);
extern bool A25L032BusyCheck(void);
extern void A25L032ReadStatReg(void);
extern void A25L032WriteStatReg(void);
extern void A25L032ChipErase(void);
extern void A25L032SectorErase(uint32_t ui32Addr);
extern void A25L032PageProgram(uint32_t ui32PgAddr, uint8_t *pui8Buf, uint32_t ui32Len);
extern void A25L032ReadData(uint32_t ui32Addr, uint8_t *pui32RxBuf, uint32_t ui32Len);
extern void A25L032DeepPowerDown(void);
extern bool A25L032ReleseDPd(void);
void set_flash_channel(uint8_t chan);

#endif // __A25L032_FLS_H__
