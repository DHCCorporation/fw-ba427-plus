//*****************************************************************************
//
// Source File: mp205_prt.h
// Description: Prototypes for the printer driver.
//
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 10/15/15     Vincent.T     mp205_prt.h -> GPIOs setting, enable, disable...
// 01/07/16     Vincent.T     Modified header format
// 01/22/16     Vincent.T     Add #define PRINTER_POWER_ON/OFF
// 06/14/16     Vincent.T     Add STB_1_2_3_4_5_6 in  enum of STB_GROUP
// 06/21/16     Vincent.T     1. Modify MP205ThermalHeadControl();(support float number heating time)
// 08/10/16     Vincent.T     1. Modify BA6845StepMotorControl();(support float step-motor pulse width)
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/driver/mp205_prt.h#5 $
// $DateTime: 2017/09/22 10:27:40 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//

#ifndef _EPM205_PTR_
#define _EPM205_PTR_

#include <stdbool.h>
#include <stdint.h>
#include "inc/hw_ssi.h"
#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "inc/hw_sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/sysctl.h"



/***********************/
/* PRINTER PART NUMBER */
/***********************/
#define MP205

/********/
/* ENUM */
/********/
typedef enum
{
	STB_ALL,
	STB_12_34_56,
	STB_123_456,
	STB_1_2_3_4_5_6
}STB_GROUP;

//Feed paper
#define FEED_PAPER_LENGTH 520

//Data buffer: 2-inch thermal printer(384-dots)
//(384/8) = 48
#define MP205_PRINTOUT_SIZE 48

//Power(PP5)
#define PRINTER_POWER_PORT	SYSCTL_PERIPH_GPIOP
#define PRINTER_POWER_BASE	GPIO_PORTP_BASE
#define PRINTER_POWER_PIN	GPIO_PIN_5

//Photo sensor(PD7)
#define PRINTER_CO_PORT		SYSCTL_PERIPH_GPIOD
#define PRINTER_CO_BASE		GPIO_PORTD_BASE
#define PRINTER_CO_PIN		GPIO_PIN_7
#define PRINTER_CO_AD_CH	ADC_CTL_CH4

//Printer STB
//STB1 -> PM2
//STB2 -> PM3
//STB3 -> PM4
//STB4 -> PM5
//STB5 -> PM6
//STB6 -> PM7
#define PRINTER_STB_PORT	SYSCTL_PERIPH_GPIOM
#define PRINTER_STB_BASE	GPIO_PORTM_BASE
#define PRINTER_STB1_PIN	GPIO_PIN_2
#define PRINTER_STB2_PIN	GPIO_PIN_3
#define PRINTER_STB3_PIN	GPIO_PIN_4
#define PRINTER_STB4_PIN	GPIO_PIN_5
#define PRINTER_STB5_PIN	GPIO_PIN_6
#define PRINTER_STB6_PIN	GPIO_PIN_7

//Step Motot
//IN11 -> PP3
//IN21 -> PP2
//IN12, IN22 -> PP4
#define PRINTER_SM_PORT		SYSCTL_PERIPH_GPIOP
#define PRINTER_SM_BASE		GPIO_PORTP_BASE
#define PRINTER_IN11_PIN 	GPIO_PIN_3
#define PRINTER_IN21_PIN	GPIO_PIN_2
#define PRINTER_IN12_22_PIN GPIO_PIN_4

//Data bus
//-LAT -> PA3
//CLK  -> PA2
//DI   -> PA4
//SSI Module
#define PRINTER_SSI_MODULE_PORT	SYSCTL_PERIPH_SSI0
#define PRINTER_SSI_MODULE_BASE	SSI0_BASE

#define PRINTER_SSI_PORT	SYSCTL_PERIPH_GPIOA
#define PRINTER_SSI_BASE	GPIO_PORTA_BASE

#define PRINTER_SSI_LAT		GPIO_PA3_SSI0FSS
#define PRINTER_SSI_LAT_PIN GPIO_PIN_3
#define PRINTER_SSI_CLK		GPIO_PA2_SSI0CLK
#define PRINTER_SSI_CLK_PIN GPIO_PIN_2
#define PRINTER_SSI_DI		GPIO_PA4_SSI0XDAT0
#define PRINTER_SSI_DI_PIN	GPIO_PIN_4

//Printer Power
#define PRINTER_POWER_ON	ROM_GPIOPinWrite(PRINTER_POWER_BASE, PRINTER_POWER_PIN, 0xFF);
#define PRINTER_POWER_OFF	ROM_GPIOPinWrite(PRINTER_POWER_BASE, PRINTER_POWER_PIN, 0x00);

//Printer Power
//#define PRINTER_POWER_ON	ROM_GPIOPinWrite(PRINTER_POWER_BASE, PRINTER_POWER_PIN, 0xFF);
//#define PRINTER_POWER_OFF	ROM_GPIOPinWrite(PRINTER_POWER_BASE, PRINTER_POWER_PIN, 0x00);

//Printer STB
#define PRINTER_STB_OFF		ROM_GPIOPinWrite(PRINTER_STB_BASE, PRINTER_STB1_PIN | PRINTER_STB2_PIN | PRINTER_STB3_PIN | PRINTER_STB4_PIN | PRINTER_STB5_PIN | PRINTER_STB6_PIN, 0x00);
#define PRINTER_STB_ON		ROM_GPIOPinWrite(PRINTER_STB_BASE, PRINTER_STB1_PIN | PRINTER_STB2_PIN | PRINTER_STB3_PIN | PRINTER_STB4_PIN | PRINTER_STB5_PIN | PRINTER_STB6_PIN, 0xFF);

#define PRINTER_STB_123		ROM_GPIOPinWrite(PRINTER_STB_BASE, PRINTER_STB1_PIN | PRINTER_STB2_PIN | PRINTER_STB3_PIN, 0xFF);
#define PRINTER_STB_456		ROM_GPIOPinWrite(PRINTER_STB_BASE, PRINTER_STB4_PIN | PRINTER_STB5_PIN | PRINTER_STB6_PIN, 0xFF);

#define PRINTER_STB_12		ROM_GPIOPinWrite(PRINTER_STB_BASE, PRINTER_STB1_PIN | PRINTER_STB2_PIN, 0xFF);
#define PRINTER_STB_34		ROM_GPIOPinWrite(PRINTER_STB_BASE, PRINTER_STB3_PIN | PRINTER_STB4_PIN, 0xFF);
#define PRINTER_STB_56		ROM_GPIOPinWrite(PRINTER_STB_BASE, PRINTER_STB5_PIN | PRINTER_STB6_PIN, 0xFF);

#define PRINTER_STB_ON_1 	ROM_GPIOPinWrite(PRINTER_STB_BASE, PRINTER_STB1_PIN, 0xFF);
#define PRINTER_STB_ON_2 	ROM_GPIOPinWrite(PRINTER_STB_BASE, PRINTER_STB2_PIN, 0xFF);
#define PRINTER_STB_ON_3 	ROM_GPIOPinWrite(PRINTER_STB_BASE, PRINTER_STB3_PIN, 0xFF);
#define PRINTER_STB_ON_4 	ROM_GPIOPinWrite(PRINTER_STB_BASE, PRINTER_STB4_PIN, 0xFF);
#define PRINTER_STB_ON_5	ROM_GPIOPinWrite(PRINTER_STB_BASE, PRINTER_STB5_PIN, 0xFF);
#define PRINTER_STB_ON_6 	ROM_GPIOPinWrite(PRINTER_STB_BASE, PRINTER_STB6_PIN, 0xFF);


//Printer Step-Motor
#define SM_IN11_HIGH 		ROM_GPIOPinWrite(PRINTER_SM_BASE, PRINTER_IN11_PIN, 0xFF);
#define SM_IN11_LOW 		ROM_GPIOPinWrite(PRINTER_SM_BASE, PRINTER_IN11_PIN, 0x00);
#define SM_IN21_HIGH 		ROM_GPIOPinWrite(PRINTER_SM_BASE, PRINTER_IN21_PIN, 0xFF);
#define SM_IN21_LOW			ROM_GPIOPinWrite(PRINTER_SM_BASE, PRINTER_IN21_PIN, 0x00);
#define SM_IN12_IN22_HIGH 	ROM_GPIOPinWrite(PRINTER_SM_BASE, PRINTER_IN12_22_PIN, 0xFF);
#define SM_IN12_IN22_LOW 	ROM_GPIOPinWrite(PRINTER_SM_BASE, PRINTER_IN12_22_PIN, 0x00);
#define SM_PHASE1_2			SM_IN11_HIGH; SM_IN21_HIGH; SM_IN12_IN22_HIGH;
#define SM_PHASE2_3			SM_IN11_LOW; SM_IN21_HIGH; SM_IN12_IN22_HIGH;
#define SM_PHASE3_4			SM_IN11_LOW; SM_IN21_LOW; SM_IN12_IN22_HIGH;
#define SM_PHASE4_1			SM_IN11_HIGH; SM_IN21_LOW; SM_IN12_IN22_HIGH;
#define SM_STOP 			SM_IN11_LOW; SM_IN21_LOW; SM_IN12_IN22_LOW;


//Function Declaraction
void MP205IOConfig(void);
void MP205HWInit(void);
void MP205BufferReset(uint8_t *ui8DatBuf, uint8_t ui8BufLen);
void MP205DataTransControl(uint8_t *ui8DatBuf);
bool MP205CoSensorDetect(void);
void MP205ThermalHeadControl(uint8_t ui8STBGroup, float Delayms);
void BA6845StepMotorControl(uint8_t ui8SMPhase, float fSMTimens);

#endif /* _EPM205_PTR_ */
