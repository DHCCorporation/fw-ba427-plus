//*****************************************************************************
//
// Source File: tp337a_temp.c
// Description: Prototype of the function to Read/Write TP-337A thermistor Drive.
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 12/01/15     Vincent.T      Created file.
// 05/27/16     Vincent.T      1. Add -5V controller
//                             2. -5V Off in TP337AHWInit();
// 09/11/18     Henry.Y        Use system driver interface to replace global variable.
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/driver/tp337a_temp.c#5 $
// $DateTime: 2017/09/22 10:27:40 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#include "tp337a_temp.h"
#include "inc/hw_types.h"
#include "driverlib/rom.h"
#include "driverlib/adc.h"
#include "delaydrv.h"




void TP337AIOConfig(void)
{
	//TEMP_SWITCH
	ROM_SysCtlPeripheralEnable(TEMP_PORT);
	ROM_GPIOPinTypeGPIOOutput(TEMP_BASE, TEMP_SWITCH);

	//Body and Object temperature
	//Enable ADC
	ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);
	ROM_ADCReferenceSet(ADC0_BASE, ADC_REF_EXT_3V);
	ROM_SysCtlPeripheralEnable(TEMP_SENSOR_PORT);
	ROM_GPIOPinTypeADC(TEMP_SENSOR_BASE, TEMP_BODY | TEMP_OBJ);

	//-5V controller
	ROM_SysCtlPeripheralEnable(TEMP_NEG5V_PORT);
	ROM_GPIOPinTypeGPIOOutput(TEMP_NEG5V_BASE, TEMP_NEG5V_PIN);
}

void TP337AHWInit(void)
{
	TP337AIOConfig();
	//Power Off
	TEMP_SWITCH_OFF;
	//
	// -5V OFF
	//
	//ROM_GPIOPinWrite(TEMP_NEG5V_BASE, TEMP_NEG5V_PIN, 0xFF);
	TEMP_NEG5V_PIN_OFF;
}

uint16_t TP337ABodyTempAD(uint8_t ui8SampleNum)
{
	uint8_t ui8LopIdx = 0;
	uint16_t ui16TempAD = 0;
	uint32_t ui32TempAD = 0;
	uint32_t ui32TempADSum = 0;
	ROM_ADCSequenceDisable(ADC0_BASE, 3);
	ROM_ADCSequenceConfigure(ADC0_BASE, 3, ADC_TRIGGER_PROCESSOR, 0);
	ROM_ADCSequenceStepConfigure(ADC0_BASE, 3, 0, TEMP_BODY_AD_CH | ADC_CTL_IE | ADC_CTL_END);
	ROM_ADCSequenceEnable(ADC0_BASE, 3);

	//Temperature Sensor on
	TEMP_SWITCH_ON;

	//
	// -5V ON
	//
	//ROM_GPIOPinWrite(TEMP_NEG5V_BASE, TEMP_NEG5V_PIN, 0x00);
	TEMP_NEG5V_PIN_ON;

	ROM_SysCtlDelay(get_sys_clk()/30);

	//Acc
	for(ui8LopIdx = 0; ui8LopIdx < ui8SampleNum; ui8LopIdx++)
	{
		ROM_ADCIntClear(ADC0_BASE, 3);
		ROM_ADCProcessorTrigger(ADC0_BASE, 3);
		while(!ROM_ADCIntStatus(ADC0_BASE, 3, false));
		ROM_ADCSequenceDataGet(ADC0_BASE, 3, &ui32TempAD);
		ui32TempADSum = ui32TempADSum + ui32TempAD;
	}

	//Temperature Sensor off
	TEMP_SWITCH_OFF;
	//
	// -5V OFF
	//
	//ROM_GPIOPinWrite(TEMP_NEG5V_BASE, TEMP_NEG5V_PIN, 0xFF);
	TEMP_NEG5V_PIN_OFF;

	//Average
	ui32TempADSum = ui32TempADSum / ui8SampleNum;
	ui16TempAD = ui32TempADSum;

	return ui16TempAD;
}

uint16_t TP337AObjTempAD(uint8_t ui8SampleNum)
{
	uint8_t ui8LopIdx = 0;
	uint16_t ui16TempAD = 0;
	uint32_t ui32TempAD = 0;
	uint32_t ui32TempADSum = 0;
	ROM_ADCSequenceDisable(ADC0_BASE, 3);
	ROM_ADCSequenceConfigure(ADC0_BASE, 3, ADC_TRIGGER_PROCESSOR, 0);
	ROM_ADCSequenceStepConfigure(ADC0_BASE, 3, 0, TEMP_OBJ_AD_CH | ADC_CTL_IE | ADC_CTL_END);
	ROM_ADCSequenceEnable(ADC0_BASE, 3);

	//Temperature Sensor on
	TEMP_SWITCH_ON;

	//
	// -5V ON
	//
	//ROM_GPIOPinWrite(TEMP_NEG5V_BASE, TEMP_NEG5V_PIN, 0x00);
	TEMP_NEG5V_PIN_ON;

	ROM_SysCtlDelay(get_sys_clk()/30);

	//Acc
	for(ui8LopIdx = 0; ui8LopIdx < ui8SampleNum; ui8LopIdx++)
	{
		ROM_ADCIntClear(ADC0_BASE, 3);
		ROM_ADCProcessorTrigger(ADC0_BASE, 3);
		while(!ROM_ADCIntStatus(ADC0_BASE, 3, false));
		ROM_ADCSequenceDataGet(ADC0_BASE, 3, &ui32TempAD);
		ui32TempADSum = ui32TempADSum + ui32TempAD;
	}

	//Temperature Sensor off
	TEMP_SWITCH_OFF;

	//
	// -5V OFF
	//
	//ROM_GPIOPinWrite(TEMP_NEG5V_BASE, TEMP_NEG5V_PIN, 0xFF);
	TEMP_NEG5V_PIN_OFF;

	//Average
	ui32TempADSum = ui32TempADSum / ui8SampleNum;
	ui16TempAD = ui32TempADSum;

	return ui16TempAD;
}
