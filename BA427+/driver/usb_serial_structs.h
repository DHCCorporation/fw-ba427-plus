//*****************************************************************************
//
// Source File: usb_serial_structs.h
// Description: Data structures defining this USB CDC device.
//				
//
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 07/02/15     William.H      Created file.
// 10/05/15     William.H      Check in BT2020 project which branched from LCDFontMaker(Pre-BT2020) by modifed related GPIOs.
// 03/08/16     Vincent.T      1. Add Vbus H/L Detection.
// 04/08/16     Vincent.T      1. UART0 -> UART7(due to PCB layout has been changed)
// 03/27/17     Vincent.T      1. Add Configure Extra I/O(AD) USB Connection Pin
//                             2. New function USBConnect() to detect USB connection
// 09/11/18     Henry.Y        Increase BUFFER_SIZE to 1024
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/driver/usb_serial_structs.h#1 $
// $DateTime: 2017/11/27 10:38:22 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#ifndef __USB_SERIAL_STRUCTS_H__
#define __USB_SERIAL_STRUCTS_H__

#include <stdbool.h>
#include <stdint.h>
#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "inc/hw_sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"
#include "driverlib/uart.h"
#include "driverlib/pin_map.h"

#include "usblib/usblib.h"
#include "usblib/usbcdc.h"
#include "usblib/device/usbdevice.h"
#include "usblib/device/usbdcdc.h"

#include "utils/ustdlib.h"
#include "uartstdio.h"
#include "delaydrv.h"

//*****************************************************************************
//
//  USB Peripherals and I/O Pin Definition
//
//  Below defines the following USB peripherals and I/O signals.  You must
//  review these and change as needed for your own board:
//   - USB peripheral
//   - GPIO Port L peripheral (for USB pins)
//   - USB_DM - PL7
//   - USB_DP - PL6
//
//*****************************************************************************

/* USB Vbus Port */
#define USB_VBUS_PERIPH				SYSCTL_PERIPH_GPIOB
#define USB_VBUS_BASE				GPIO_PORTB_BASE
#define USB_VBU_PIN					GPIO_PIN_1

/* USB port */
#define USB_MODULE_SYSCTL_PERIPH	SYSCTL_PERIPH_GPIOL
#define USB_PORT_BASE				GPIO_PORTL_BASE

/* USB_DM pin */
#define USB_DM_PIN					GPIO_PIN_7

/* USB_DP pin */
#define USB_DP_PIN					GPIO_PIN_6


//*****************************************************************************
//
// Configure Extra I/O(AD) USB Connection Pin
//
//*****************************************************************************
#define EXT_USB_AD_PERIPH			SYSCTL_PERIPH_GPIOK
#define EXT_USB_AD_BASE				GPIO_PORTK_BASE
#define EXT_USB_AD_PIN				GPIO_PIN_3
#define EXT_USB_AD_CH				ADC_CTL_CH19

//*****************************************************************************
//
// Configuration and tuning parameters.
//
//*****************************************************************************

//*****************************************************************************
//
// The system tick rate expressed both as ticks per second and a millisecond
// period.
//
//*****************************************************************************
#define SYSTICKS_PER_SECOND 100
#define SYSTICK_PERIOD_MS (1000 / SYSTICKS_PER_SECOND)

//*****************************************************************************
//
// The base address, peripheral ID and interrupt ID of the UART that is to
// be redirected.
//
//*****************************************************************************



//*****************************************************************************
//
// Flag indicating whether or not we are currently sending a Break condition.
//
//*****************************************************************************
static bool g_bSendingBreak = false;

//*****************************************************************************
//
// Flags used to pass commands from interrupt context to the main loop.
//
//*****************************************************************************
#define COMMAND_PACKET_RECEIVED 0x00000001
#define COMMAND_STATUS_UPDATE   0x00000002



//*****************************************************************************
//
// Global flag indicating that a USB configuration has been set.
//
//*****************************************************************************
//static volatile bool g_bUSBConfigured = false;

//*****************************************************************************
//
// Internal function prototypes.
//
//*****************************************************************************
static void USBUARTPrimeTransmit(uint32_t ui32Base);
static void CheckForSerialStateChange(const tUSBDCDCDevice *psDevice,
                                      int32_t i32Errors);
static void SetControlLineState(uint16_t ui16State);
static bool SetLineCoding(tLineCoding *psLineCoding);
static void GetLineCoding(tLineCoding *psLineCoding);
static void SendBreak(bool bSend);

//*****************************************************************************
//
// The error routine that is called if the driver library encounters an error.
//
//*****************************************************************************
#ifdef DEBUG
void
__error__(char *pcFilename, uint32_t ui32Line)
{
    while(1)
    {
    }
}
#endif

//*****************************************************************************
//
// The size of the transmit and receive buffers used for the redirected UART.
// This number should be a power of 2 for best performance.  256 is chosen
// pretty much at random though the buffer should be at least twice the size of
// a maxmum-sized USB packet.
//
//*****************************************************************************
//#define UART_BUFFER_SIZE 256
#define BUFFER_SIZE 1024


uint32_t RxHandler(void *pvCBData, uint32_t ui32Event,
                          uint32_t ui32MsgValue, void *pvMsgData);
uint32_t TxHandler(void *pvi32CBData, uint32_t ui32Event,
                          uint32_t ui32MsgValue, void *pvMsgData);
void USBIOConfig(void);
void USBHwInit(void);
bool USBConnect(void);
extern tUSBBuffer g_sTxBuffer;
extern tUSBBuffer g_sRxBuffer;
extern tUSBDCDCDevice g_sCDCDevice;
extern uint8_t g_pui8USBTxBuffer[];
extern uint8_t g_pui8USBRxBuffer[];

#endif // __USB_SERIAL_STRUCTS_H__
