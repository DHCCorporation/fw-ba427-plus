//*****************************************************************************
//
// Source File: battcore_iocfg.c
// Description: Battery Tester Core GPIO Configuration.
//				
//
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 10/05/15     William.H      Created file.
// 10/08/15     Henry.Y        Remove 8V ADC channel, hide CCA GPIO temporarily.
// 03/10/16     Vincent.T      1. Add Vload gpio func. initializing process.
// 03/21/16     Vincent.T      1. Add Pin defination of TEST LOAD PROTECT1 & TEST LOAD PROTECT2 initializing process
// 06/14/16     Vincent.T      1. Modify BattCoreHwInit();(add internal battery power switch initializing procedure)
// 12/02/16     Vincent.T      1. Modify BattCoreHwInit();(add Load Protect unlock procedure)
// 12/30/16     Vincent.T      1. Add Initializing process of ADC_VLOAD_PLUS_PIN and ADC_VLOAD_MINS_PIN
// 03/01/17     Henry.Y        Add internal voltage gpio switch IntBat_SW_OFF/IntBat_SW_ON.
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/driver/battcore_iocfg.c#5 $
// $DateTime: 2017/09/22 10:27:40 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************
#include <stdbool.h>
#include <stdint.h>
#include "inc/hw_memmap.h"
#include "inc/hw_adc.h"
#include "inc/hw_types.h"
#include "driverlib/rom.h"
#include "driverlib/sysctl.h"
#include "driverlib/adc.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "battcore_iocfg.h"

void BattCoreHwInit(void)
{
	//
	//	ADC Input Channel Initialization @ start
	//	{	
	//
    // Enable the ADC peripherals and the associated GPIO port
    //
    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);
	ROM_SysCtlPeripheralEnable(ADC_VOLTAGE_PERIPHERAL);		// CHANNEL FOR BAT/6/12/24/8V
	ROM_SysCtlPeripheralEnable(ADC_VOLTAGE_BAT_PERIPHERAL);	// CHANNEL FOR Internal BAT

	//
    // Configure the pins to be used as analog inputs.
    //
	ROM_GPIOPinTypeADC(ADC_VOLTAGE_BASE, ADC_6V_CH | ADC_12V_CH | ADC_24V_CH);	// CHANNEL FOR BAT/6/12/24
	ROM_GPIOPinTypeADC(ADC_VOLTAGE_BAT_BASE, ADC_BAT_CH);									// CHANNEL FOR Internal BAT

	//
    // Select the external reference for greatest accuracy.
    //
    ROM_ADCReferenceSet(ADC0_BASE, ADC_REF_EXT_3V);

	//
    // Disable ADC sequencers
    //
    ROM_ADCSequenceDisable(ADC0_BASE, 1);

	//
    // Initialize both ADC peripherals using sequencer 1 and processor trigger.
    //
    ROM_ADCSequenceConfigure(ADC0_BASE, 1, ADC_TRIGGER_PROCESSOR, 0);

	//
	//	The manner of changing the ADC speed has changed on TM4C devices from LM3S devices.  
	//	This change is not yet comprehended in TivaWare. As a result, the SysCtlADCSpeedSet function may not work correctly.  
	//	To ensure proper operation, remove any references to the SysCtlADCSpeedSet function and use the following instead.
	//
	//	(ps: http://e2e.ti.com/support/microcontrollers/tiva_arm/f/908/p/293858/1218708 or 
	//		 https://e2e.ti.com/support/microcontrollers/tiva_arm/f/908/t/331481)
	//	Original config code: SysCtlADCSpeedSet(SYSCTL_ADCSPEED_1MSPS);
	//
	HWREG(ADC0_BASE + ADC_O_PC) = (ADC_PC_SR_1M);

	//
	//	}
	//	ADC Input Channel Initialization @ end
	//

	//
	//	CCA Output Pins enable and configuration for Load-On test @ start
	//	{
	//	

	// Load-on 1
	ROM_SysCtlPeripheralEnable(CCA_load_1_PERIPHERAL);
	ROM_GPIOPinTypeGPIOOutput(CCA_load_1_BASE, CCA_load_1_pin | CCA_load_1_protect);	// Output Direction
	CCA_load_1_protect_ON;
	CCA_load_1_OFF;

	// Load-on 2
	ROM_SysCtlPeripheralEnable(CCA_load_2_PERIPHERAL);
	ROM_GPIOPinTypeGPIOOutput(CCA_load_2_BASE, CCA_load_2_pin|CCA_load_2_protect);	// Output Direction
	CCA_load_2_protect_ON;
	CCA_load_2_OFF;

	//
	//	}
	//	CCA Output Pins enable and configuration for Load-On test @ end
	//

	//
	//  Vload GPIO initializing process
	//
	ROM_SysCtlPeripheralEnable(ADC_VLOAD_PERIPHERAL);
	ROM_GPIOPinTypeADC(ADC_VLOAD_BASE, ADC_VLOAD_PLUS_PIN | ADC_VLOAD_MINS_PIN);

	// Internal battery power supply
	ROM_SysCtlPeripheralEnable(IntBat_SW_PERIPHERAL);
	ROM_GPIOPinTypeGPIOOutput(IntBat_SW_BASE, IntBat_SW_PIN);	// Output Direction
	IntBat_SW_OFF;

	//
	// Test Load Protection Circuit Initializing Process
	//
	ROM_SysCtlPeripheralEnable(TestLoad3_PERIPHERAL);
	ROM_GPIOPinTypeGPIOOutput(TestLoad3_BASE, TestLoad3_PIN | TestLoad3Protect_PIN);	// Output Direction

	//Test Load Protect ON
	//TestLoad3Protect_ON;
	//TestLoad3_OFF;

	//
	// Test Load Protect OFF
	//
	TestLoad3Protect_OFF;
	TestLoad3_ON;
}

