//*****************************************************************************
//
// Source File: am.c
// Description: Prototype for functions of amper meter driver.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 01/07/16     Vincent.T      Created file.
// 03/02/16     Vincent.T      1. Add -5V controller
// 05/27/16     Vincent.T      1. -5V off in AMHWInit();
// 06/14/16     Vincent.T      1. Modify AMGetAD();(ui32_t buf -> ui64_t buf)
// 02/06/17     Henry.Y        1. Hide UARTprintf in AMGetAD().
// ============================================================================

#include "am.h"
#include "inc/hw_types.h"
#include "driverlib/rom.h"
#include "driverlib/adc.h"
#include "uartstdio.h"

//*****************************************************************************
//
// The system clock frequency in Hz.
//
//*****************************************************************************


void AMIOConfig(void)
{
	UARTprintf("AMIOConfig()\n");

	//amper meter AD
	//Enable ADC0
	ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);
	ROM_ADCReferenceSet(ADC0_BASE, ADC_REF_EXT_3V);
	//Peripherial
	ROM_SysCtlPeripheralEnable(AM_PORT);
	//AD pin sel
	ROM_GPIOPinTypeADC(AM_BASE, AM_PIN);

	//-5V controller
	ROM_SysCtlPeripheralEnable(NEG5V_PORT);
	ROM_GPIOPinTypeGPIOOutput(NEG5V_BASE, NEG5V_PIN);
}

void AMHWInit(void)
{
	UARTprintf("AMHWInit()\n");
	AMIOConfig();
	//
	// -5V OFF
	//
	//ROM_GPIOPinWrite(NEG5V_BASE, NEG5V_PIN, 0xFF);
	AM_NEG5V_PIN_OFF;
}

void AMGetAD(float *pfDat, uint8_t ui8SampleNum)
{
	//UARTprintf("AMGetAD()\n");

	uint8_t ui8LopIdx = 0;
	uint32_t ui32AMTemp = 0;
	uint64_t ui64AMSum = 0;

	ROM_ADCSequenceDisable(ADC0_BASE, 3);
	ROM_ADCSequenceConfigure(ADC0_BASE, 3, ADC_TRIGGER_PROCESSOR, 0);
	ROM_ADCSequenceStepConfigure(ADC0_BASE, 3, 0, AM_CH | ADC_CTL_IE | ADC_CTL_END);
	ROM_ADCSequenceEnable(ADC0_BASE, 3);

	//Acc
	for(ui8LopIdx = 0; ui8LopIdx < ui8SampleNum; ui8LopIdx++)
	{
		ROM_ADCIntClear(ADC0_BASE, 3);
		ROM_ADCProcessorTrigger(ADC0_BASE, 3);
		while(!ROM_ADCIntStatus(ADC0_BASE, 3, false));
		ROM_ADCSequenceDataGet(ADC0_BASE, 3, &ui32AMTemp);
		ui64AMSum = ui64AMSum + ui32AMTemp;
	}

	//Average
	(*pfDat) = ((float)ui64AMSum / (float)ui8SampleNum);
}


