//*****************************************************************************
//
// Source File: wifi_driver.h
// Description: wifi module driver layer.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 12/19/17     Jerry.W        Created file.
// 11/20/18     Morgan         correct typo
// ============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_HMA/driver/wifi_driver.h#2 $
// $DateTime: 2017/12/18 16:17:54 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#ifndef __WIFI_DRIVER_H__
#define __WIFI_DRIVER_H__

#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include "driverlib/uart.h"
#include "driverlib/gpio.h"
#include "driverlib/rom.h"
#include "driverlib/pin_map.h"
#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "driverlib/sysctl.h"
#include "uartstdio.h"
#include "driverlib/interrupt.h"
#include "mem_protect.h"
#include "soft_timer.h"

typedef enum{
	module_no_response,
	unregistered_channel,
}Error_T;

//===========================================================================================//
//																							 //
//									UART hardware definition								 //
//																							 //
//===========================================================================================//
/* UART port */
#define WIFIUART_MODULE_SYSCTL_PERIPH	SYSCTL_PERIPH_UART4
#define WIFIUART_IOPORT_SYSCTL_PERIPH	SYSCTL_PERIPH_GPIOK
#define WIFIUART_IOPORT_BASE			GPIO_PORTK_BASE
#define WIFIUART_BASE					UART4_BASE

/* U0RX pin */
#define WIFI_RX_PIN						GPIO_PIN_0
#define WIFI_RX_MUX_SEL					GPIO_PK0_U4RX

/* U0TX pin */
#define WIFI_TX_PIN						GPIO_PIN_1
#define WIFI_TX_MUX_SEL					GPIO_PK1_U4TX

/* U0TX pin */
#define WIFI_RTS_PIN					GPIO_PIN_2
#define WIFI_RTS_MUX_SEL				GPIO_PK2_U4RTS

/* U0TX pin */
#define WIFI_CTS_PIN					GPIO_PIN_3
#define WIFI_CTS_MUX_SEL				GPIO_PK3_U4CTS

/* Baud rate */
#define WIFI_UART_BAUDRATE				115200*40

/* UART6 interrupt */
#define WIFI_BTUART_INT					INT_UART4

/* UART software buffer size */
#define WIFI_RX_BUFFER_SIZE				1024*6
#define WIFI_TX_BUFFER_SIZE				1024*10



//===========================================================================================//
//									Control I/O definition   								 //
//===========================================================================================//
/* Power Pin PH0 */
#define WIFIUART_POWER_IOPORT_SYSCTL_PERIPH		SYSCTL_PERIPH_GPIOH
#define WIFIUART_POWER_IOPORT_BASE				GPIO_PORTH_BASE
#define WIFI_POWER_PIN								GPIO_PIN_0


#define WIFIUART_CONTROL_RST_IOPORT_SYSCTL_PERIPH	SYSCTL_PERIPH_GPIOQ
#define WIFIUART_CONTROL_RST_IOPORT_BASE			GPIO_PORTQ_BASE
/* Reset_n Pin PQ3 */
#define WIFI_RESET_N_PIN						GPIO_PIN_3


#define WIFIUART_CONTROL_SLP_IOPORT_SYSCTL_PERIPH	SYSCTL_PERIPH_GPIOG
#define WIFIUART_CONTROL_SLP_IOPORT_BASE			GPIO_PORTG_BASE
/* config Pin PG1 */
#define WIFI_CONFIG_PIN							GPIO_PIN_1

/*************************************************************************************************
 * @ Name		: wifi_IO_config
 * @ Brief 		: initial IO
 *************************************************************************************************/
void wifi_driver_IO_config();



/*************************************************************************************************
 * @ Name		: wifi_init
 * @ Brief 		: initial driver layer data structure
 * @ Parameter 	: on_off -- 0 = off, 1 = on.
 *************************************************************************************************/
void wifi_driver_init(uint8_t on_off);



/*************************************************************************************************
 * @ Name		: wifi_driver_regist_com_cb
 * @ Brief 		: register communication channel.
 * @ Parameter 	: channel -- communication channel.
 * @ Parameter 	: com_cb -- callback function pointer to notice server layer of receiving command
 *************************************************************************************************/
bool wifi_driver_regist_com_cb(uint8_t channel,void (*com_cb)(uint8_t *data, uint16_t len));


/*************************************************************************************************
 * @ Name		: wifi_driver_regist_error_cb
 * @ Brief 		: register error handler.
 * @ Parameter 	: err_cb -- callback function pointer to notice server layer of error occurrence
 *************************************************************************************************/
void wifi_driver_regist_error_cb(void (*err_cb)(Error_T ));


/*************************************************************************************************
 * @ Name		: wifi_send_command
 * @ Brief 		: send command protocol
 * @ Parameter 	: cmmd -- command array pointer
 * 				  len -- command length
 * 				  chann -- communication channel
 * @ Return		: 0 -- success
 * 				  1 -- fail, has remained command to be sent
 *************************************************************************************************/
uint8_t wifi_driver_send_command(uint8_t chann, uint8_t * cmmd, uint16_t len);






#endif
