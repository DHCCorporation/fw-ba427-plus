//*****************************************************************************
//
// Source File: smallfonts.h
// Description: Prototype of Font type.
//
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 06/24/15     William.H      Created file.
// 10/05/15     William.H      Check in BT2020 project which branched from LCDFontMaker(Pre-BT2020) by modifed related GPIOs.
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/driver/smallfonts.h#5 $
// $DateTime: 2017/09/22 10:27:40 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************

#ifndef __SMALLFONTS_H__
#define __SMALLFONTS_H__

#include <stdbool.h>
#include <stdint.h>

typedef struct
{
    uint8_t u8Width;				/* Character width for storage         */
    uint8_t u8Height;				/* Character height for storage        */
    uint8_t u8FirstChar;			/* The first character available       */
    uint8_t u8LastChar;				/* The last character available        */	
    const uint8_t *au8FontTable;	/* Font table start address in memory  */
}
FONT_DEF;

extern const FONT_DEF Font_System3x6;
extern const FONT_DEF Font_System5x8;
extern const FONT_DEF Font_System7x8;
extern const FONT_DEF Font_8x8;
extern const FONT_DEF Font_8x8Thin;
extern const FONT_DEF Font_Digit16x16;
extern const FONT_DEF Font_System8x16;


extern const uint8_t au8FontSystem3x6[];
extern const uint8_t au8FontSystem5x8[];
extern const uint8_t au8FontSystem7x8[];
extern const uint8_t au8Font8x8[];
extern const uint8_t au8Font8x8Thin[];
extern const uint8_t au8Font16x16[];
extern const uint8_t au8FontSystem8x16[];

#endif // __SMALLFONTS_H__
