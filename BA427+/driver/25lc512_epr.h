//*****************************************************************************
//
// Source File: 25lc512_epr.h
// Description: Functions to Read/Write/Chip Erase/Deep Power Down of 25LC512
//              EEPROM IC Drive.
//
// Edit History:
//
// when         who            what, where, why  
// --------     ----------     ------------------------------------------------
// 10/14/15     Vincent.T      Created file.
// =============================================================================
//
//
// $Header: //depot/Tester/BT2100_Series/BT2100_FW/BT2100_WW_V02_plus/driver/25lc512_epr.h#5 $
// $DateTime: 2017/09/22 10:27:40 $
// $Author: Jerry $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************


#ifndef BT2100_WW_V00_L_DRIVER_25LC512_EPR_H_
#define BT2100_WW_V00_L_DRIVER_25LC512_EPR_H_

#include <stdbool.h>
#include <stdint.h>
#include "inc/hw_ssi.h"
#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/sysctl.h"

#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_ssi.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_epi.h"
#include "inc/hw_ints.h"
#include "inc/hw_nvic.h"
#include "inc/hw_uart.h"
#include "driverlib/debug.h"
#include "driverlib/fpu.h"
#include "driverlib/gpio.h"
#include "driverlib/rom.h"
#include "driverlib/rom_map.h"
#include "driverlib/interrupt.h"
#include "driverlib/pin_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/ssi.h"
#include "driverlib/uart.h"
#include "uartstdio.h"

/******************************/
/* EXTERNAL MEMORY IC         */
/* EEPROM IC: 25LC512         */
/******************************/

typedef enum
{
	EPR_25LC512_WRITE,
	EPR_25LC512_READ
}EXT_EPR_IC_CMD;

//Data bus
//-CS	->	PQ0
// SCK	->	PB5
// SI	->	PE4
// SO	->	PE5

//Module Setting
#define EPR_25LC512_MOUDLE_PORT	SYSCTL_PERIPH_SSI1
#define EPR_25LC512_MODULE_BASE SSI1_BASE
// -CS -> MANUAL
#define EPR_25LC512_SCK			GPIO_PB5_SSI1CLK
//#define EPR_25LC512_CS		GPIO_PB4_SSI1FSS
#define EPR_25LC512_SO			GPIO_PE4_SSI1XDAT0
#define EPR_25LC512_SI			GPIO_PE5_SSI1XDAT1

//GPIO SETTING
//-CS: PQ0
#define EPR_25LC512_PORT_LAT	SYSCTL_PERIPH_GPIOQ
#define EPR_25LC512_BASE_LAT	GPIO_PORTQ_BASE
#define EPR_25LC512_CS_PIN		GPIO_PIN_0

//GPIO SETTING
//-SCK: PB5
//-FSS: PB4
#define EPR_25LC512_PORT_B		SYSCTL_PERIPH_GPIOB
#define EPR_25LC512_BASE_B		GPIO_PORTB_BASE
#define EPR_25LC512_SCK_PIN		GPIO_PIN_5
#define EPR_25LC512_FSS_PIN		GPIO_PIN_4


//GPIO SETTING
//-SI : PE4
//-SO : PE5
#define EPR_25LC512_PORT_E		SYSCTL_PERIPH_GPIOE
#define EPR_25LC512_BASE_E		GPIO_PORTE_BASE
#define EPR_25LC512_SI_PIN		GPIO_PIN_4
#define EPR_25LC512_SO_PIN		GPIO_PIN_5

//Function Declaraction
void EPR_25LC512IOConfig(void);
void EPR_25LC512HWInit(void);
void EPR_25LC512ModeSel(uint8_t ui8Mode);
void EPR_25LC512WriteByte(uint8_t *ui8DataBuff, uint8_t ui8DataLen);
void EPR_25LC512ReadByte(uint8_t *ui8DataBuff, uint8_t ui8DataLen);

#endif /* BT2100_WW_V00_L_DRIVER_25LC512_EPR_H_ */
