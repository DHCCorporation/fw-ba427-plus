//*****************************************************************************
//
// Source File: inEEPROMdrv.c
// Description: internal EERPOM driver function.
//
//
// Edit History:
//
// when         who            what, where, why
// --------     ----------     ------------------------------------------------
// 02/26/18     Jerry.W	       Created file.
// 04/03/18     Henry.Y        EEPROM_word_write_safe(): add two times check and re-written.
// 04/11/18     Henry.Y        Fix logic issue in EEPROM_word_write_safe().
// 04/12/18     Henry.Y        fix logic error: EEPROM_bytes_read_safe().
// =============================================================================
//
//
// $Header: //depot/Tester/PB_Series/PB firmware/E1_V00/driver/inEEPROMdrv.c#5 $
// $DateTime: 2018/04/20 15:59:47 $
// $Author: HenryY $
//
//
// Copyright (c) 2015 DHC Specialty Corp.  All Rights Reserved.
// DHC Proprietary.
//
//*****************************************************************************


#include "inEEPROMdrv.h"

#include <stdbool.h>
#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "driverlib/rom.h"
#include "driverlib/eeprom.h"
#include "usb_serial_structs.h"
#include "delaydrv.h"
#include "uartstdio.h"

uint8_t EEPROM_word_write_safe(uint32_t data, uint32_t add, uint32_t mask){
	uint32_t buf, buf2;

	//UARTprintf("EEPROM save add : %08d, data : %08x, mask : %08x\n", add, data, mask);
	EEPROMRead(&buf, add, 4);

	buf &= ~mask;
	buf |= (data&mask);

/*
	while(!voltage_check_fase()){
		if(ct < 2){
			EEPROMProgram(&buf, add, 4);
			EEPROMRead(&buf2, add, 4);
			if(buf2 != buf) ct++;
			else return 0;
		}
		else{
			return 1;		
		}
	}
*/

	EEPROMProgram(&buf, add, 4);
	EEPROMRead(&buf2, add, 4);
	if(buf2 == buf){
		return 0;
	}else{
		return 2;
	}
}


uint8_t EEPROM_word_read_safe(uint32_t *data, uint32_t add){
	uint32_t buf, buf2;

	EEPROMRead(&buf, add, 4);
	//UARTprintf("EEPROM read add : %08d, data : %08x\n", add, buf);
	*data = buf;

/*
	if(!voltage_check_fase()) return 1;
*/

	EEPROMRead(&buf2, add, 4);
	if(buf2 == buf){
		return 0;
	}else{
		return 2;
	}

}



uint8_t EEPROM_bytes_write_safe(uint8_t *data, uint32_t add, uint32_t length){
	uint32_t buf, mask = 0;
	uint8_t temp, i, result;

	temp = (4 - add%4);
	if(temp){
		uint8_t *temp_buf = (uint8_t*)&buf;
		memcpy(temp_buf+4-temp, data , temp);

		for(i = 0; i < temp; i++){
			mask >>= 8;
			if(length) {
			mask |= byte4_mask;
				length--;
			}
		}

		result = EEPROM_word_write_safe(buf, add&0xFFFFFFFC, mask);
		if(result) return result;

		data += temp;
		add += temp;
	}
	mask = (byte4_mask|byte3_mask|byte2_mask|byte1_mask);
	while(length >= 4){
		memcpy(&buf, data , 4);
		result = EEPROM_word_write_safe(buf, add, mask);
		if(result) return result;

		length -= 4;
		data += 4;
		add += 4;
	}

	if(length){
		mask = 0;
		for(i = 0; i < length ; i++){
			mask <<= 8;
			mask |= byte1_mask;
		}
		memcpy(&buf, data , length);
		result = EEPROM_word_write_safe(buf, add, mask);
		if(result) return result;
	}

	return 0;
}

uint8_t EEPROM_bytes_read_safe(uint8_t *data, uint32_t add, uint32_t length){
	uint32_t buf;
	uint8_t temp, result;

	temp = (4 - add%4);

	if(temp){
		result = EEPROM_word_read_safe(&buf, add&0xFFFFFFFC);
		if(result) return result;


		uint8_t *temp_buf = (uint8_t*)&buf;
		memcpy(data, temp_buf+4-temp, (temp>length)? length:temp);

		data += temp;
		add += temp;
		length -= (temp>length)? length:temp;

	}

	while(length){
		temp = (length >= 4)? 4:length%4;
		result = EEPROM_word_read_safe(&buf, add);
		if(result) return result;

		memcpy(data, &buf, temp);

		data += temp;
		add += temp;
		length -= temp;
	}

	return 0;

}






